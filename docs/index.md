# Welcome to PRIDA
([return to plantmineralnutrition.net](http://plantmineralnutrition.net))

Building on the original 3D imaging framework, introduced in [Clark et al. (2011)](references), along with the updates described in [Pi&ntilde;eros et al. (2015)](references), PRIDA (Plant Root Imaging and Data Acquisition) is a Python[^1]-based software for 2D and 3D root system imaging and archiving.
The software is capable of controlling digital cameras for capturing 2D images (or image sequences) and microcontrollers for rotating plant systems for 3D analysis. The software saves all images, including experimental details and experimenter contact information, in a single hierarchical file (i.e., HDF5[^2]) for preservation.
The software's graphical user interface (GUI) allows users to search, edit and preview images and experiment details.
Data may be extracted from the HDF5 files for use in other software (e.g., Image Analysis, RootReader2D or RootReader3D).

PRIDA runs natively on Unix-like computers (including Linux[^3] and Mac OS X[^4]) with a highly portable version available for the Raspberry Pi[^5] single-board computer.
An explorer-only version is available that runs on Microsoft Windows[^6].

This software is still under development and new features are currently being added.
The first new development is a semi-automated camera calibration process, based on the methodology of [Clark et al. (2011)](references), in order to eliminate subjective calibration parameters, reduce the overall process time, and facilitate the reproducibility of experiments.

[^1]: Copyright &copy; 2001&ndash;2017, Python Software Foundation.

[^2]: Copyright &copy; 2006&ndash;2017, The HDF Group.

[^3]: Linux&reg; is the registered trademark of Linus Torvalds in the United States and other countries.

[^4]: Mac and OS X are trademarks of Apple Inc., registered in the United States and other countries.

[^5]: Raspberry Pi is the registered trademark of the Raspberry Pi Foundation in the United States and other countries.

[^6]: Windows is a registered trademark of Microsoft Corporation in the United States and/or other countries.
