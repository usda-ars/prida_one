### Principle Investigators
Miguel A. Piñeros[^1]
and
Leon V. Kochian[^2]

### Principle Developer
Tyler W. Davis[^1]

### Software Consultant
David J. Schneider[^2]

### Technical Support
- Jon E. Shaff[^1]
- Brandon G. Larson[^1]
- Eric J. Craft[^1]
- Pierre-Luc Pradier[^2]
- Bill Armstrong[^1]

### Research Assistants
- Zhigang Liu[^3] (2016&ndash;2017)
- Barbara Hufnagel[^3] (2015&ndash;2016)
- Ithipong "Billy" Assaranurak[^3] (2015)

### Undergraduate Interns
- Nathanael Shaw[^4] (2015&ndash;2016)
- Hoi Cheng[^5] (2015)

[^1]: USDA-Agricultural Research Service, Robert W. Holley Center for Agriculture & Health, 538 Tower Road, Ithaca, NY 14853, United States

[^2]: University of Saskatchewan, Global Institute for Food Security, 110 Gymnasium Place, Saskatoon, SK S7N 4J8, Canada

[^3]: Cornell University, School of Integrative Plant Science, Soil and Crop Sciences, 135 Plant Science Building, Ithaca, NY 14853, United States

[^4]: Rochester Institute of Technology, Kate Gleason College of Engineering, Department of Mechanical Engineering, James E. Gleason Building, 76 Lomb Memorial Drive, Rochester, NY 14623, United States

[^5]: Cornell University, Department of Computer Science, 402 Gates Hall, Ithaca, NY 14853, United States
