!!! note ""
    Updated for Version 1.5

Below are descriptions of the various controls of the software.
Click on the linked words on this page to navigate between sections and use your browser's back button to return to where you left off.

## Create
Clicking on the __Create__ button opens a popup window requesting a file name and location.
Click "Save" after typing in a file name (the appropriate file extension, **.hdf5**, will be added for you) and another popup window will appear requesting your name, contact details (i.e., email address) and a summary of your project or experiment.
You will be able to edit these fields again later (see [About](#about)).
Once you have entered your information, click "OK" to continue to an empty [Viewer Window](user_views/index.html#viewer).
You have successfully created a new project file.

## Open
Clicking on the __Open__ button (or using the keyboard shortcut **Ctrl + o** or **&#8984; + o**) opens a popup window requesting you to select an existing project file (**\*.hdf5**).
Either double-click or select a project file and click "Open" to bring you to the [Viewer Window](user_views/index.html#viewer).
You have successfully opened an existing project file.

## Search
Clicking on the __Search__ button opens the [File Search Window](user_views/index.html#file-search) where you can search for plant information across multiple project files.
Click on the "+" sign to add project files (\*.hdf5) to the _Search List_ and type keywords in the search bar to filter results in the _Plants List_.
Once you have found a project file you want to open, select it from the _Search List_ and click the "Explore" button to take you to the [Viewer Window](user_views/index.html#viewer) or, to exit, click on the "Main Menu" button to return to the [Main Menu Window](user_views/index.html#main-menu).

## Replace
When you start the program, you may receive a popup warning you that your configuration file is out-of-date.
Clicking on the __Replace__ button from the [Main Menu Window](user_views/index.html#main-menu) will create a new configuration file in your local Prida directory.
Upon closing and re-opening the software, the warning message will no longer show up.
You may also change the software configuration parameters to alter the behavior of the software (see [Configuration File](user_config)).

## Image
Available only in _2D Mode_ and _3D Mode_.

Clicking on the __Image__ button from the [Viewer Window](user_views/index.html#viewer) opens the [Input Session Window](user_views/index.html#input-session).
Here, give the plant you are imaging a unique identifier (PID) or select a PID from the dropdown list that appears when you start typing in the PID input box using the up and down arrow keys on your keyboard and press enter to autofill the _Plant Information_ fields.
The PLANTS Database ID and/or genus species input boxes are also set to autofill based on the contents of your dictionary file (see [Dictionary File](user_dictionary)) by highlighting them and pressing enter on your keyboard.
Continue completing the _Plant Information_ fields.

In the _Session Information_, the user and contact fields are autofilled based on the project file's author details.
The session date is also autofilled with the current day.
If you are in _2D Mode_ the "Number of Images" field will be autofilled with "1" (unchangeable).
If you are in _3D Mode_, the value you give to the number of images will automatically determine the required angles for rotating the plant (note some rounding may be necessary based on the minimum resolution of the motor).
The camera details (i.e., make, model, exposure, aperture and ISO) are autofilled based on the connected camera (if possible).

Once all of the _Plant Information_ and _Session Information_ fields are completed, click "OK" to begin imaging or "Cancel" to return to the [Viewer Window](user_views/index.html#viewer).
During imaging, the [Run Session Window](user_views/index.html#run-session) will open to display the progress and a preview of the image(s) being taken.
After all images have been captured, you will be returned to the [Viewer Window](user_views/index.html#viewer).
The new PID (or session under existing PID) can now be viewed in the _Spreadsheet Pane_.

## Calibrate
Clicking on the __Calibrate__ button from the [Viewer Window](user_views/index.html#viewer) with a calibration session selected in the _Spreadsheet Pane_ opens the [Calibration View](user_views/index.html#calibration) for processing a calibration rod image sequence.

## Import
Available only in _Explorer Mode_.

Clicking on the __Import__ button from the [Viewer Window](user_views/index.html#viewer) opens a popup asking you if your image files are stored in a directory.
Clicking "No" opens a file search window for you to open a single image file (e.g., JPG or TIF).
Clicking "Yes" opens a directory search window allowing you to open all image files within a given directory.
If you are importing multiple images from a directory, you may receive a warning message that indicates that one or more images do not match the others.
You may click "No" to stop the import (allowing you to check on the problem) or click "Yes" to import anyway (note: this may cause problems down the road when performing analysis).

Once you have opened a file or directory for importing, you will be guided to the [Input Session Window](user_views/index.html#input-session) to enter the plant and session information corresponding to the import image(s).
The "Number of Images" field will be automatically filled based on the number of images found.
Once you have entered all the information, clicking "OK" opens a popup asking you to confirm import.
Clicking "Cancel" returns you to the [Input Session Window](user_views/index.html#input-session) where you can make further edits to the input fields.
Clicking "OK" opens the [Run Session Window](user_views/index.html#run-session) and begins the image import.
When the import is finished, you will be returned to the main [Viewer Window](user_views/index.html#viewer).

## Extract
Clicking the __Extract__ button from the [Viewer Window](user_views/index.html#viewer) opens a popup requesting for an output directory where images and metadata from the current project file will be exported.
If you have nothing selected in the _Spreadsheet Pane_, the extract will export the entire project file's contents.
If you have selected a plant in the _Spreadsheet Pane_, the extract will export all the sessions associated with the plant.
If you have selected a session in the _Spreadsheet Pane_, the extract will export only the selected session.
After you have selected an output directory and clicked "Open," you are returned to the [Viewer Window](user_views/index.html#viewer).
A loading spinner and text are shown at the top of the window to indicate that the extract is still working.
Once the extract is complete, a popup notification will appear; click "OK" to dismiss.

In the output directory, the extracted files are in the same folder hierarchy as stored in the HDF5 project file.
In each PID folder are the individual session folders, under which are the folders for each dataset (i.e., the images).

The metadata associated with the plants and sessions are found in two CSV files:

* `[project file name]_pid-attrs.csv` &mdash; spreadsheet of plant (i.e., PIDs) attributes
* `[project file name]_sess-attrs.csv` &mdash; spreadsheet of plant session attributes

## Edit
Clicking the __Edit__ button from the [Viewer Window](user_views/index.html#viewer) with nothing selected in the _Spreadsheet Pane_ will open a notification popup warning you as much.
With either a plant or session selected in the _Spreadsheet Pane_, the __Edit__ button will direct you to the [Input Session Window](user_views/index.html#input-session).

If a plant was selected, only the _Plant Information_ input fields are active for editing.
Note that the plant identifier (PID) cannot be edited.
If a session was selected, only the _Session Information_ input fields are active for editing.
Note that the number of images cannot be edited.

When you are finished with your edits, click "OK" to save and return to the [Viewer Window](user_views/index.html#viewer).
Clicking "Cancel" will return you to the [Viewer Window](user_views/index.html#viewer) without saving any changes.

## Save
Clicking on __Save__ from the menu bar (`File -> Save`) or using the keyboard shortcut (e.g., **Ctrl + s** or **&#8984; + s**) opens a file dialog for creating a copy of the current HDF5 file using the current compression level (settable by either the [Configuration File](user_config) or from the menu: `Config -> Data -> Compression Level`), skipping sessions that have their "Exclude" attribute set to "True."
The default file name is the same as the current HDF5 file name with "\_comp" appended before the ".hdf5" file extension.
Click okay to begin the file copy.
The compression will take some time, especially for large project files, and is indicated by a spinner and text at the top of the [Viewer Window](user_views/index.html#viewer).

!!! warning
    There is no way to cancel a save operation once it has started. Use with caution!

A popup window will appear once the compressed copy has finished.
You can open and edit the new file in the same way as the original.
The benefit of the compressed copy is a smaller file size and the ability to exclude unwanted sessions (see _Exclude_ field in [Input Session Window](user_views/index.html#input-session)).

## About
Clicking on __About__ from the menu bar (`Help -> About` or `Prida -> About Prida`) opens a popup window displaying the current version of the software, the copyright notice, the URL to this README (click the hyperlink to open the webpage), and information regarding the project file, including: the author; the author's contact address; the software version used to create the project file; the total number of plants, sessions and images in the project file; and the project summary statement.

Clicking on the "Edit" button at the bottom of the popup window opens the project details popup (as mentioned in [Create](#create)) where you can edit the author, contact and project summary fields.
Click "OK" when you are done editing or the close box to dismiss and return to the project file's about popup.
Click "Close" to dismiss the popup and return to the [Viewer Window](user_views/index.html#viewer).
