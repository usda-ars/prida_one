Welcome and thank you for using the Plant Root Imaging and Data Acquisition (PRIDA) software.
This basic guide will help you through the use of this program.

Compiled binaries are available in [Downloads](../downloads).
If you are interested in the source code for this software, you can find it [here](https://bitbucket.org/usda-ars/prida_one/src).

Launching the software for the first time will create the following three directories and two text files:

| Type | Location | Description |
| :-- | :-- | :-- |
| directory | ~/Prida/ | The Prida parent directory |
| directory | ~/Prida/logs/ | Sub-directory for storing software operation log files |
| directory | ~/Prida/photos/ | Sub-directory for storing image files |
| text file | ~/Prida/dictionary.txt | Convenience file for genus/species naming |
| text file | ~/Prida/prida.config | Prida configuration file |

Please check that these files and folders were successfully created.
You will need access to the configuration file to enact important feature changes to the software.

The software will be started in "Explorer Mode" by default.
You will see "Explorer Mode" in the software's title bar.

To run the software in either "2D Mode" or "3D Mode", you will need:

- to be running a UNIX-like computer or virtual machine
- a USB-connected digital camera that is supported by the gPhoto2 library (check [here](http://gphoto.org/proj/libgphoto2/support.php))
- \(for 3D Mode only) a stepper motor with appropriate controller connected to your computer

Once you have ensured you are using the above items and that they are correctly connected, you will need to update the configuration file for either "2D" or "3D" mode before running the software.

1. Open the configuration file (located in ~/Prida/ directory) using your preferred text editor.
1. Edit the line that reads: `PRIDA.HARDWARE.IMAGING  MODE  'Explorer'` by changing 'Explorer' to either '2D' or '3D' (see [Configuration File](user_config)).
1. Start the program (you will either see the mode listed in the software's title bar or you will receive a message describing why it failed to start in the desired mode).
