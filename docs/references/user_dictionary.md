!!! note ""
    Updated for Version 1.5

The dictionary file (default location: ~/Prida/dictionary.txt) is a non-essential convenience file used for autofilling the "PLANTS Database ID" and "genus species" __Plant Information__ fields in the Input Session Window.

The following provides an example of the dictionary file format:

```
['CUSA4',]:garden cucumber (Cucumis sativus L.)
['ZEMA',]:corn (Zea mays L.)
['BRNA',]:rape (Brassica napus L.)
['ORSA',]:rice (Oryza sativa L.)
```

where the identifiers in the square brackets on the left are entries in the [USDA PLANTS Database](https://plants.usda.gov/), which is utilized in an effort to adopt a standard plant naming scheme.
Following the colon are the common and scientific names.

To assist you in editing and/or creating a new dictionary file, a USDA PLANTS Utility program has been developed, which can be found in [Downloads](../downloads).
