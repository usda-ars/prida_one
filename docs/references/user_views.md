!!! note ""
    Updated for Version 1.5

Below are descriptions of the various windows of the software.
Click on the linked words on this page to navigate between sections and use your browser's back button to return to where you left off.

## Main Menu
![Main Menu](../images/view_menu.jpg)

The Main Menu is the first screen you encounter with the software and has the following options available:

* [__Create__](user_controls/index.html#create) &mdash; creates a new project file
* [__Open__](user_controls/index.html#open) &mdash; opens an existing project file
* [__Search__](user_controls/index.html#search) &mdash; allows you to search across multiple files for specific plant or experiment details
* [__Replace__](user_controls/index.html#replace) &mdash; replaces the configuration file

## Viewer
![Viewer Sheet](../images/view_sheet.jpg)

The main Viewer has three panes: two on the left and one on the right.
The right pane is the _Spreadsheet Pane_, which displays the individual plants that have been imaged with their associated information.
Under each plant, defined by a unique plant identifier (PID), is a list of the plants imaging sessions (entries associated with each time the plant was imaged).

Above the _Spreadsheet Pane_ is the search bar.
Type space-delimited keywords to filter plant entries in the _Spreadsheet Pane_ based on matching plant attributes.
You may type in as many keywords to further filter the results.
Clearing the search bar will bring all entries back to the _Spreadsheet Pane_ view.

The top left pane is the _Session Pane_, which displays the information for a selected session and the bottom left pane is the _Thumbnail Pane_, which shows the thumbnail previews of all the images of a selected session.

Clicking on a thumbnail replaces the _Spreadsheet Pane_ with the _IDA Pane_, which contains a larger preview of the thumbnail image, and replaces the _Session Pane_ with the _Photo Pane_, which displays the image properties.
Clicking on the `...` button in the bottom of the _Photo Pane_ will open a popup window with a searchable list of EXIF tags (if available).
Clicking on the &#9664; and &#9654; buttons at the bottom left of the _IDA Pane_ will rotate the preview image (counter) clockwise.
Clicking on the __Back to Spreadsheet__ button returns you to the _Spreadsheet Pane_ and _Session Pane_ view.

![Viewer IDA](../images/view_ida.jpg)

Across the bottom of the Viewer window are five buttons:

* [__Image__](user_controls/index.html#image) &mdash; click to start a new imaging session (available on in _2D Mode_ or _3D Mode_)
* [__Calibrate__](user_controls/index.html#calibrate) &mdash; click to perform calibration
* [__Import__](user_controls/index.html#import) &mdash; click to import imaging sessions from file (available only in _Explorer Mode_)
* [__Extract__](user_controls/index.html#extract) &mdash; click to export selected plant images and information to file (or all plant images and information if none selected)
* [__Edit__](user_controls/index.html#edit) &mdash; click to edit the information of a selected plant or session

## File Search
![File Search](../images/view_search.jpg)

The File Search window has two panes: the _Search List_ on the left and the _Plants List_ on the right.

The _Search List_ shows all the project files that are currently being searched.
To add additional files to the _Search List_, click on the "__+__" sign button at the bottom of the _Search List_ pane.
You may select multiple HDF5 files to open at one time by using the "Ctrl + click" method.
To remove a project file from the _Search List_, click to highlight the file you want to remove and then click on the "__&ndash;__" sign button at the bottom of the _Search List_ pane to remove it.

The _Plants List_ pane shows all the plants (i.e., PIDs) from all the project files listed in the _Search List_.
The first column in the _Plants List_ (i.e., File) is the file name (i.e., File Name in the _Search List_) so you know to which file in the _Search List_ it belongs.

Above the _Plants List_ is the search bar, which may be used to filter plant entries in the _Plants List_.
The search filter is activated whenever you type (or press the delete key).
Once you have found the plant(s) of interest, use the File field to identify the plant's project file.
Click on the project file in the _Search List_ and click the __Explore__ button at the bottom of the window.
This will open the project file to the [Viewer Window](#viewer).

## Input Session
![Input Session](../images/view_input.jpg)

The Input Session Window consists of the _Plant Information_ pane on the left, the _Session Information_ pane on the right and "OK" and "Cancel" buttons located in the bottom right.

In the _Plant Information_ pane is a series of input fields for plant-specific information.
Similarly, the _Session Information_ pane consists of a series of input fields for session-specific information.
Additionally, at the bottom of the _Session Information_ pane is the _Image Crop_ tool, which may be enabled by selecting the checkbox.

The _Image Crop_ tool should only be used after imaging (accessible by selecting a session in the _Spreadsheet Pane_ of the Viewer and clicking the **Edit** button).
Checking the _Image Crop_ option box, changing the relative cropping values, and clicking "OK", applies the cropping option to all images (thumbnails, previews and exports) within the project file.
Simply uncheck the _Image Crop_ option box and click "OK" to return all images to their original view.

There are two selection boxes in the _Session Information_ pane: _Image Orientation_ and _Exclude_.
Changing the _Image Orientation_ value from "Original" (how it was captured from the camera during imaging or read from your computer during import) to either "Rotated Clockwise" or "Rotated Counter-Clockwise" is a convenient way to adjust how the thumbnails and image preview are displayed in the [Viewer Window](#viewer).

Changing the _Exclude_ value from "False" to "True" will result in the current session being skipped during a [Save](user_controls/index.html#save).
This provides a convenient way to remove unwanted data from your project file.

## Run Session
![Run Experiment](../images/view_run.jpg)

During either image acquisition or import, the Run Session Window displays a progress bar at the bottom of the window and an image preview in the pane on the right.
For large imaging sessions (3D Mode), the dialog on the left will change from "Acquiring Images ..." to "Saving to File ..." while the datasets are still being read into the HDF5 file after camera acquisition has finished.
Note that only the first image taken during an imaging session is previewed to reduce the imaging time.

## Calibration
![Calibration](../images/view_calib.jpg)

The Calibration view has the session properties displayed at the top left, including the session title, date and image count.
Beneath the session properties are the calibration tool tabs, including: _Load_, _Crop_, _Setup_, and _Run_.
Next to the calibration tool tabs is the _Image Display Area_.
Above the _Image Display Area_ is the dialog, which displays the current state of processing.
Below the _Image Display Area_ is the progress bar to show the progress of individual calibration processes.
