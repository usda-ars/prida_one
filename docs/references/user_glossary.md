### 2D Mode
This software mode allows users to create new and open existing project files; view, edit and export file contents; and capture single images from an attached USB digital camera

### 3D Mode
This software mode allows users to create new and open existing project files; view, edit and export file contents; and capture image [sequences](#sequence) from an attached USB digital camera

### Explorer Mode
This software mode allows users to create new and open existing project files; view, edit and export file contents; and import existing data to new sessions

### HDF5
The Hierarchical Data Format 5th standard&mdash;a container that consists of a folder and subfolders system (i.e., groups) for storing data and metadata (i.e., attributes)

### PID
The plant identifier; a unique name given to an individual plant, used for organizing [sessions](#session) within an [HDF5](#hdf5) [project file](#project-file)

### PRIDA
Plant Root Imaging and Data Acquisition

### Project File
Currently, it is intended that an individual project or experiment be incorporated into a single [HDF5](#hdf5) file

### Session
The imaging of a single object (e.g., plant root system or calibration rod), consisting of one or more photos and associated meta data

### Sequence
A series of multiple images taken during a single [session](#session) where each image is of a rotated view controlled by an attached motor and bearing system
