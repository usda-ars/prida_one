!!! attention "Updated for Version 1.5"
    Some configurable parameters may not be available in earlier versions of the software.

The configuration file allows you to change the software behavior without the need of reprogramming.
When the software starts, it checks for the existence of this file and may warn you if it is missing or out-of-date.
The configuration file is not required for running this software; however, without the configuration file, you will not be able to change the parameters listed below.

The configuration file is formatted with three columns:

```
PACKAGE.MODULE  PARAMETER  VALUE
```

The first column is the software notation (references the package-module structure used during the configuration process, please do not change).
The second column is the parameter's descriptive name; this is to help you identify the parameters.
The third column is the configuration value for the given parameter.
Tables 1 and 2 provide a list of the parameter names that may be configured along with a description of the parameter and possible value assignments.

__Table 1. Core Configurable Parameters.__

| Parameter Name | Description | Possible Values |
| :------------- | :---------- | :-------------- |
| DEGREES_PER_STEP | The number of degrees rotated during a single unit step made by the motor (degrees). | (float) non-negative and non-zero; default value: 1.8 |
| DRIVER | The name of the driver module to use for running the motor. | (string) 'Shield' (Arduino), 'HAT' (Raspberry Pi) or your own Python module |
| GEAR_RATIO | The ratio of the drive wheel diameter the bearing diameter. | (float) non-negative and non-zero; 1.0 (for magnetic drive), 10.0 (for large bearing) |
| IMAGE_DIR | The path to the directory where JPG images are stored before they are uploaded to the HDF5 file. | (string) default value: '~/Prida/photos' |
| MODE | The intended software mode (_Note:_ any errors during startup will default to Explorer Mode). | '2D' (camera required), '3D' (camera and motor required) or 'Explorer' (default) |
| PORT_NUMBER | The Shield or HAT port number connected to the stepper motor (Adafruit convention). | (integer) 1 or 2 |
| PRIDA_VERSION | The software version number used to create this configuration file (_Note:_ please do not change this value). | (string)
| SETTLING_TIME | The amount of time paused between the motor motion and image capture (seconds). | (float) non-negative; default value: 2.0 |
| SPEED |  Motor rotation speed (approximately in rotations-per-minute). | (float) non-negative and non-zero; default value: 2.0 |

__Table 2. Optional Configurable Parameters.__

| Parameter Name | Description | Possible Values |
| :------------- | :---------- | :-------------- |
| BLUE_LED_CHANNEL | The pin-out on the Adafruit Motor HAT from the PWM chip for controlling the blue LED. | (integer) 0, 1, 15, 16 |
| COMPRESS_LV | A value indicating the compression level (higher means more compressed) used during file save. | (integer) 1–9 |
| CREDENTIAL_DIR | The directory path where the email credential file for remote log archiving is located. | (string) default value: '~/Prida' |
| CREDENTIAL_FILENAME | The name of the credential file (username and password) for emailing archived logs. | (string) default value: 'credentials' |
| DICTIONARY_DIR | The directory path where the dictionary file is located. | (string) default value: '~/Prida' |
| DICTIONARY_FILENAME | The name of the dictionary file used to populate the gen. sp. and USDA PLANTS database keys (created using the USDA Plants Database Utility) | (string) default value: 'dictionary.txt' |
| GREEN_LED_CHANNEL | The pin-out on the Adafruit Motor HAT from the PWM chip for controlling the green LED. | (integer) 0, 1, 15, 16 |
| HAS_LED | A boolean flag indicating the presence or lack of a LED. | (bool) True or False |
| HAS_PIEZO | A boolean flag indicating the presence or lack of a piezo buzzer. | (bool) True or False |
| LED_TYPE | The type of LED (if) used. | (string) 'CC' (common cathode) or 'CA' (common anode) |
| LOG_DIR | The path to the directory where log files are saved. | (string) default value: '~/Prida/logs' |
| LOG_FILENAME | The name used for the log files. | (string) default value: 'prida.log' |
| LOG_LEVEL | The logging level used when saving to the log file. | (string) 'notset' (no logging), 'debug' (debugging messages and higher written), 'info' (info messages and higher written), 'warning' (warning messages and higher written), 'error' (error messages and higher written), 'critical' (only critical messages written; highest level) |
| MICROSTEPS_PER_STEP | Number of microsteps taken per each unit step. | (integer) 2, 4, 8 or 16 (higher values correspond to smoother motion, but have less turning power; _NOTE_ only implemented for the 'HAT' driver) |
| PWM_CHANNEL | The pin-out on the Adafruit Motor HAT from the PWM chip for controlling the piezo buzzer. | (integer) 0, 1, 15, 16 |
| RED_LED_CHANNEL | The pin-out on the Adafruit Motor HAT from the PWM chip for controlling the red LED. | (integer) 0, 1, 15, 16 |
| VOLUME | The piezo buzzer volume. | (float) 0.0-100.0 |
