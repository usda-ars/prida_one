The Prida '3D Mode' requires a driver for communicating with the stepper motor that controls the turntable/bearing for rotating the plant root system.
Two drivers are available for your convenience.
These drivers include the HAT class (i.e., 'prida/hardware/hat.py') for the [Adafruit DC  & Stepper Motor HAT](https://www.adafruit.com/product/2348) for the Raspberry Pi microcomputer and the Shield class (i.e., 'prida/hardware/shield.py') for the [Adafruit Motor/Stepper/Servo Shield](https://www.adafruit.com/product/1438) for the Arduino microcontroller.

Because the Arduino Shield works for all systems, this guide focuses on this driver.
For more information regarding the Raspberry Pi setup, see [here](https://bitbucket.org/usda-ars/prida_one/wiki/raspberry).

### Shield
<div class="image">
![Stepper Shield](../images/shield.jpg "Adafruit Motor Shield on Arduino Uno")
<div>Adafruit Motor/Stepper/Servo Shield on top of an Arduino Uno microcontroller.</div>
</div>

To begin, the following items need to be procured:

* Arduino Uno Rev. 3 microcontroller (available from [Arduino.cc](https://www.arduino.cc/en/Main/ArduinoBoardUno))
* USB 2.0 cable Type A/B (available from [Arduino.cc](https://store.arduino.cc/usa/usb-2-0-cable-type-a-b))
* Adafruit Motor/Stepper/Servo Shield v.2.3 (available from [Adafruit.com](https://www.adafruit.com/product/1438))
* Stepper motor (e.g., NEMA-23 available from [Pololo.com](https://www.pololu.com/product/1477))
* `Adafruit_Motor_Shield` Arduino Library (available on [Github.com](https://github.com/adafruit/Adafruit_Motor_Shield_V2_Library) or the [Prida repository](https://bitbucket.org/usda-ars/prida_one/src/master/utilities/Arduino/))
* `ams_serial` Arduino Sketch (available on the [Prida repository](https://bitbucket.org/usda-ars/prida_one/src/master/utilities/Arduino/))

You will also need to download and install the [Arduino IDE](https://www.arduino.cc/en/Main/Software) for your computer (available for Windows, Mac OS X and Linux).

!!! tip
    Make certain you have the most up-to-date Arduino IDE.
    This will ensure the latest drivers and libraries are installed on your computer.

Follow the [instructions](https://www.arduino.cc/en/Guide/Libraries) for installing the `Adafruit_Motor_Shield` library and add the `ams_serial` folder to your Sketches.

Follow the [instructions](https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/) for installing the Shield's headers and terminals.

!!! tip
    Make certain the terminals and headers are in the correct orientation before soldering!

Attach the Shield to your Arduino and connect the Arduino to your computer using the USB cable.

!!! tip
    On Windows, you may receive a message that your computer is installing the device driver for your Arduino Uno.
    Check to make certain the COM port number is COM1 or COM2, as higher COM port numbers may cause problems during upload.
    For more information on how to change your Arduino's COM port number, see this [stackoverflow](https://stackoverflow.com/questions/25718779/arduino-nano-avrdude-ser-opensystem-cant-open-device-com1the-system) article.

Open the Arduino IDE and make certain the Board (Tools -> Board) and Port (Tools -> Port) are correctly configured.
Open the `ams_serial` Sketch and click the checkmark (Sketch -> Verify) to compile and verify that the sketch is working.
If the Sketch compiles without error, click right-arrow (Sketch -> Upload) to program the Arduino.
If the program uploads without error, you may unplug the Arduino.
It is ready for use with Prida.

!!! tip
    To see more information during the Arduino code compilation and upload, open Preferences (File -> Preferences) and check the two boxes next to "Show verbose output during: ".

### Motor Test
First connect your stepper motor to the motor shield.

The Pololu NEMA-23 stepper has 6 color-coded leads (white, black, yellow, green, blue and red); however, for a bipolar connection, only four leads are used with the Adafruit Motor Shield (red, blue, green and black)&mdash;the center-tap yellow and white wires are left disconnected.
In order to turn the stepper motor, two pairs of leads (i.e., red with blue and green with black) must be matched together in the terminal blocks labeled M1 and M2 (port 1) or M3 and M4 (port 2); remember to skip over the center terminal (GND).

!!! tip
    Wiring the four NEMA-23 leads to the Adafruit motor shield port 1. <br />
    ![NEMA-23 Wiring](../images/adafruit_nema23.jpg "NEMA-23 wiring")

Power your shield.

The Pololu NEMA-23 stepper motor is rated at 8.6 V and 1.0 Amp per phase, which requires an AC/DC wall converter.
The Adafruit Motor Shield can run motors on 4.5&ndash;13.5 V at 1.2 A per bridge.

!!! attention
    Currently, the motor shield is powered using an AC/DC switching power adapter with a 5 V 2 A output, which is enough to provide 1 A per phase.
    If the green LED on the motor shield fades or blinks during operation, it means that it is being under powered.

With the Arduino plugged into your computer and the motor shield powered, you can try to run Prida in 3D Mode by setting the "MODE" to "3D" and "DRIVER" to "Shield" in the [Prida configuration file](user_config).
You will also need a USB-connected digital camera to begin in 3D mode.

To test the motor operation, at the [Menu Screen](user_views#main-menu), select the "Take Steps" option from the Motor menu (Motor -> Take Steps), enter a number of steps and click OK.
This should result in movement with your motor.

!!! tip
    If you successfully launched Prida in 3D mode but your motor does not move, make certain you have the motor shield powered and check that the port number in the configuration file matches the lead connections you made to the shield.
