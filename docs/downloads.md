Below are the executables of Prida, which were compiled using PyInstaller (v.3.1 and 3.2) with Python 3 (v.3.5.0 and 3.5.3) and Qt 5 (v.5.5.1 and 5.6.0) runtimes.
The executables are available for both Microsoft Windows (win32) and Apple macOS (Sierra).
A USDA PLANTS Database utility (PlantsDB) is also available for both Microsoft Windows (win32) and Apple macOS (Sierra).
After downloading an application, you must first unzip the file before you can use it.

!!! important "Copying and License"
    This software is a "United States Government Work" under the terms of the United States Copyright Act, was written as part of the authors' official duties as a United States Government employee, and cannot be copyrighted; therefore, Prida is freely available to the public for use.

    However, the Prida source code and subsequent binaries provided below include and/or reference copyrighted materials, namely:

    * Adafruit
    * Exifread (Prida v.1.5 and later)
    * LibRaw + RawPy (Prida v.1.5 and later)
    * libgphoto2 + GPhoto2
    * HDF5 + H5PY
    * NumPy
    * Qt 5/PyQt5 (Prida v.0.3 and later)
    * SciPy (Prida v.0.7 and later)

    Please see [COPYING and LICENSE](https://bitbucket.org/usda-ars/prida_one/wiki/license) for more information.

| File | Type | Version | Uploaded on | Size |
| :--- | :--- | :------ | :---------- | ---: |
<a onClick="ga('send', 'event', 'download', 'Prida', 'win32-py3.5-qt5.6_Prida-1.5.zip');" href="../downloads/win32-py3.5-qt5.6_Prida-1.5.zip">win32-py3.5-qt5.6_Prida-1.5.zip</a> | .EXE | 1.5 | 2017-06-12 | 111.5 MB |
<a onClick="ga('send', 'event', 'download', 'Prida', 'macOS-Sierra-py3.5-qt5.5_Prida-1.5.app.zip');" href="../downloads/macOS-Sierra-py3.5-qt5.5_Prida-1.5.app.zip">macOS-Sierra-py3.5-qt5.5_Prida-1.5.app.zip</a> | .app | 1.5 | 2017-06-12 | 49.5 MB |
<a onClick="ga('send', 'event', 'download', 'Prida', 'win32-py3.5-qt5.6_Prida-1.4.zip');" href="../downloads/win32-py3.5-qt5.6_Prida-1.4.zip">win32-py3.5-qt5.6_Prida-1.4.zip</a> | .EXE | 1.4 | 2016-07-12 | 108.3 MB |
<a onClick="ga('send', 'event', 'download', 'Prida', 'macOS-Sierra-py3.5-qt5.5_Prida-1.4.app.zip');" href="../downloads/macOS-Sierra-py3.5-qt5.5_Prida-1.4.app.zip">macOS-Sierra-py3.5-qt5.5_Prida-1.4.app.zip</a> | .app | 1.4 | 2016-07-12 | 48.1 MB |
<a onClick="ga('send', 'event', 'download', 'PlantsDB', 'win32-py3.5-qt5.6_PlantsDB-1.3.1.zip');" href="../downloads/win32-py3.5-qt5.6_PlantsDB-1.3.1.zip">win32-py3.5-qt5.6_PlantsDB-1.3.1.zip</a> | .EXE | 1.3.1 | 2017-06-13 | 17 MB |
<a onClick="ga('send', 'event', 'download', 'PlantsDB', 'macOS-Sierra-py3.5-qt5.5_PlantsDB-1.3.1.app.zip');" href="../downloads/macOS-Sierra-py3.5-qt5.5_PlantsDB-1.3.1.app.zip">macOS-Sierra-py3.5-qt5.5_PlantsDB-1.3.1.app.zip</a> | .app | 1.3.1 | 2017-06-13 | 18.9 MB |
