#!/usr/bin/python
#
# live_util.py
#
# VERSION: 0.0.0-dev
#
# LAST EDIT: 2017-04-26
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import io
import logging
import sys
import os

import numpy
from PIL import Image
import PIL.ImageQt as ImageQt
import PyQt5.uic as uic
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QMessageBox
import rawpy
from scipy.misc import imresize

from camera import Camera
from pridaperipheral import PRIDAPeripheralError


###############################################################################
# FUNCTIONS:
###############################################################################
def exit_app(my_app):
    """
    Gracefully exit a given QApplication.

    Name:     exit_app
    Inputs:   object, QApplication (my_app)
    Outputs:  None
    Features: Graceful exiting of QApplication
    """
    logging.info("exiting QApplication")
    if my_app.camera.current_status() == 0:
        my_app.camera.poweroff()
    my_app.quit()


def resource_path(relative_path):
    """
    Return absolute path to a given resource file---for PyInstaller.

    Name:     resource_path
    Inputs:   str, resource file with path (relative_path)
    Outputs:  str, resource file with a potentially modified path
    Features: Returns absolute path for a given resource, works for dev and
              for PyInstaller.
    """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = sys.path[0]

    return os.path.join(base_path, relative_path)


###############################################################################
# CLASSES:
###############################################################################
class LiveApp(QApplication):
    """
    Name:     LiveApp
    Features: This app is to test the gphoto2 LivePreview
    History:  Version 0.0.0
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     LiveApp.__init__
        Inputs:   None.
        Returns:  None.
        Features: App initialization
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # Check that UI file exists:
        self.live_ui = resource_path("live.ui")
        if not os.path.isfile(self.live_ui):
            raise IOError("Error! Missing required ui files!")

        # Load main window GUI to the QMainWindow:
        self.view = uic.loadUi(self.live_ui, self.base)
        self.base.setWindowTitle("Live Preview")

        # Connect the dialog buttons:
        self.view.buttonBox.rejected.connect(self.quit)
        self.view.buttonBox.accepted.connect(self.connect)

        # Set logger and at exit register:
        self.logger = logging.getLogger(__name__)
        atexit.register(exit_app, self)

        # Define a working directory:
        self.work_dir = os.path.expanduser("~")
        self.clearBaseImage("Preview")

        # Initialize a camera:
        self.camera = Camera()
        self.camera.path = self.work_dir

        self.view.statusBar.showMessage(
            "Welcome to LivePreview, click Retry to begin.", 2000)

        self.base.show()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def clearBaseImage(self, text=""):
        """
        Name:     IDA.clearBaseImage
        Inputs:   None.
        Outputs:  None.
        Features: Clears the IDA base image and all labels
        """
        self.logger.debug("clearing base image")
        self.view.label.clear()
        self.base_array = None
        self.cur_pixmap = None
        self.cur_qimage = None
        self.view.label.setText(text)

    def connect(self):
        """
        """
        if self.camera.current_status() != 0:
            try:
                self.camera.connect()
            except PRIDAPeripheralError as e:
                self.mayday(str(e))
            else:
                self.view.statusBar.showMessage("Camera connected!", 2000)

        self.preview()

    def mayday(self, msg):
        """
        Name:     LiveApp.mayday
        Inputs:   str, warning message (msg)
        Outputs:  None.
        Features: Opens a pop-up window displaying the warning text
        """
        QMessageBox.warning(self.base, "Warning", msg)

    def preview(self):
        """
        """
        if self.camera.current_status() == 0:
            try:
                camera_data = self.camera.preview()
            except Exception as e:
                self.mayday("Preview failed! %s" % str(e))
                self.camera.poweroff()
            else:
                img_quality = self.camera._get_atter_by_name('imagequality')
                if 'nef' in img_quality.lower():
                    raw = rawpy.imread(io.BytesIO(camera_data))
                    img = raw.postprocess()
                    self.setBaseImage(img)

    def resizeAndSet(self, img_array):
        """
        Name:     IDA.resizeAndSet
        Inputs:   numpy.ndarray, array for resizing and setting (img_array)
        Outputs:  None.
        Features: Resizes and sets ImageQt from base image
        """
        img_dim = len(img_array.shape)
        if img_dim == 3:
            try:
                self.logger.debug("checking dimensions of RGB image")
                h, w, _ = img_array.shape
            except:
                self.logger.exception("something is awry")
                raise
        elif img_dim == 2:
            try:
                self.logger.debug("checking dimensions on grayscale image")
                h, w = img_array.shape
            except:
                self.logger.exception("something is awry")
                raise
        else:
            self.logger.warning(
                "Can not handle image with dimension %d", img_dim)
            raise ValueError("Unsupported image dimension %d", img_dim)

        # Calculate the required size of image:
        self.logger.debug("resizing image")
        s_factor = numpy.array(
            [float(self.view.label.height())/h,
             float(self.view.label.width())/w]).min()
        self.logger.debug("IDA resize factor set to %f", s_factor)

        self.logger.debug("resizing image...")
        nd_array = imresize(img_array, s_factor)
        self.logger.debug("...resize complete")

        image = Image.fromarray(nd_array)
        self.logger.debug(
            "created PIL image from array using mode %s", image.mode)

        self.logger.debug("setting ImageQt...")
        qt_image = ImageQt.ImageQt(image)
        self.cur_qimage = qt_image
        self.logger.debug(
            "ImageQt is grayscale %s", qt_image.isGrayscale())

        self.logger.debug("setting QPixmap...")
        self.cur_pixmap = QPixmap.fromImage(self.cur_qimage)
        self.logger.debug("...setting Pixmap image")
        self.view.label.setPixmap(self.cur_pixmap)
        self.logger.debug("...pixmap set is complete")

    def setBaseImage(self, ndarray_full):
        """
        Name:     LiveApp.setBaseImage
        Inputs:   numpy.ndarray, image (ndarray_full)
        Outputs:  None.
        Features: Sets and saves base image for viewing
        Depends:  resizeAndSet
        """
        self.logger.debug("setting base array")
        img_dim = len(ndarray_full.shape)

        # Add binary image support:
        if ndarray_full.max() == 1 and img_dim == 2:
            self.logger.debug("found binary image!")
            self.logger.debug("scaling binary pixels for grayscale")
            ndarray_full[numpy.where(ndarray_full == 1)] *= 255
            self.logger.debug("pixel scaling complete")

        if img_dim == 2:
            try:
                self.logger.debug("checking dimensions on grayscale image")
                temp_array = numpy.zeros(
                    (ndarray_full.shape[0], ndarray_full.shape[1], 3))
                temp_array[:, :, 0] = numpy.copy(ndarray_full)
                temp_array[:, :, 1] = numpy.copy(ndarray_full)
                temp_array[:, :, 2] = numpy.copy(ndarray_full)
            except:
                self.logger.exception("something is awry")
                raise
            else:
                self.base_array = temp_array.astype('uint8')
                self.resizeAndSet(self.base_array)
        elif img_dim == 3:
            self.base_array = ndarray_full.astype('uint8')
            self.resizeAndSet(self.base_array)
        else:
            self.logger.warning(
                "Can not handle image with dimension %d", img_dim)
            raise ValueError("Unsupported image dimension %d", img_dim)

###############################################################################
# MAIN:
###############################################################################
if __name__ == "__main__":
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    # Instantiating logging handler and record format:
    root_handler = logging.StreamHandler()
    rec_format = "%(asctime)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%H%M%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)

    app = LiveApp()
    app.exec_()
