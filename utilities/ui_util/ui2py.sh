#!/bin/bash
#
# ui2py.sh
#
# LAST UPDATED: 2016-02-23
#
# ~~~~~~~~~~~~~~~~~~~~
# PUBLIC DOMAIN NOTICE
# ~~~~~~~~~~~~~~~~~~~~
# This software is a "United States Government Work" under the terms of the
# United States Copyright Act. It was written as part of the authors' official
# duties as a United States Government employee and thus cannot be copyrighted.
# This software/database is freely available to the public for use.
# The Department of Agriculture (USDA) and the U.S. Government have not placed
# any restriction on its use or reproduction.
#
# Although all reasonable efforts have been taken to ensure the accuracy and
# reliability of the software, the USDA and the U.S. Government do not and
# cannot warrant the performance or results that may be obtained by using this
# software. The USDA and the U.S. Government disclaim all warranties, express
# or implied, including warranties of performance, merchantability or fitness
# for any particular purpose. Please cite the authors in any work or product
# based on this material:
#
#    Tyler W. Davis
#    USDA-ARS Robert W. Holley Center for Agricutlure and Health
#    538 Tower Road, Ithaca, NY 14853
#
# ~~~~~~~~~~~~
# description:
# ~~~~~~~~~~~~
# This script is a part of the PRIDA project and is used to convert the Qt UI
# files into pyton scripts.
#
# ~~~~~~
# usage:
# ~~~~~~
# From the utilities directory, run:
#   `bash ui2py.sh ../working/`
# OR
# Copy script to ../working/prida directory and run:
#   `bash ui2py.sh`

# Case 1: no arguments sent, run locally
if [ "$#" -eq 0 ]; then
    find . -maxdepth 1 -type f -name "*.ui" | while IFS= read -r pathname; do
        base=$(basename "$pathname"); name=${base%.*}
        pyuic5 -o "${name}.py" "$base"
    done
    sed -i.bak s/custom/\.custom/ mainwindow.py
fi

# Case 2: one argument given, run on given directory (fully recursive!)
if [ "$#" -eq 1 ] && [ -d "$1" ]; then
    find "$1" -type f -name "*.ui" | while IFS= read -r pathname; do
        dir=$(dirname "$pathname"); base=$(basename "$pathname"); name=${base%.*}
        pyuic5 -o "${dir}/${name}.py" "$pathname"
    done
    find "$1" -type f -name mainwindow.py -exec sed -i.bak s/custom/\.custom/ {} \;
fi
