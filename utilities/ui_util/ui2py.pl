#!/usr/bin/perl
#
# ui2py.pl
#
# LAST UPDATED: 2016-04-18
#
# ~~~~~~~~~~~~~~~~~~~~
# PUBLIC DOMAIN NOTICE
# ~~~~~~~~~~~~~~~~~~~~
# This software is a "United States Government Work" under the terms of the
# United States Copyright Act. It was written as part of the authors' official
# duties as a United States Government employee and thus cannot be copyrighted.
# This software/database is freely available to the public for use.
# The Department of Agriculture (USDA) and the U.S. Government have not placed
# any restriction on its use or reproduction.
#
# Although all reasonable efforts have been taken to ensure the accuracy and
# reliability of the software, the USDA and the U.S. Government do not and
# cannot warrant the performance or results that may be obtained by using this
# software. The USDA and the U.S. Government disclaim all warranties, express
# or implied, including warranties of performance, merchantability or fitness
# for any particular purpose. Please cite the authors in any work or product
# based on this material:
#
#    Tyler W. Davis
#    USDA-ARS Robert W. Holley Center for Agricutlure and Health
#    538 Tower Road, Ithaca, NY 14853
#
# ~~~~~~~~~~~~
# description:
# ~~~~~~~~~~~~
# This script is a part of the PRIDA project and is used to convert the Qt UI
# files into pyton scripts.

# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# LOAD MODULES:
# ////////////////////////////////////////////////////////////////////////////
use warnings;
use strict;
use File::Basename;           # provides fileparse
use File::Find;               # provides find
use File::Spec::Functions;    # provides catfile

# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# MAIN:
# ////////////////////////////////////////////////////////////////////////////
my $numargs = $#ARGV;

if ($numargs == -1)
{
    find(\&ui2py, ".");
}
else
{
    my $path = $ARGV[0];
    find(\&ui2py, $path)
}

# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# SUBROUTINES:
# ////////////////////////////////////////////////////////////////////////////
sub ui2py
{
    my $file;        # file name passed to subroutine
    my $filepath;    # file with associated path passed to subroutine
    my $filename;    # same as file
    my $dir;         # same as $File::Find:dir
    my $suffix;      # file extension as defined in fileparse
    my $newfile;     # file with python extension
    my $newpath;     # new file with path
    my $command;     # pyuic5 command
    my $bakpath;     # backup file path
    my $readfh;        # reading file handle
    my $writefh;       # writing file handle
    my $line;          # lines to be written
    my @rawdata = ();  # raw file data

    # Get subroutine argument:
    $file = $_;

    # Process only UI files that exist and have content:
    if ((-e $file) && (-s $file) && ($file =~ /^.*\.ui$/))
    {
        $filepath = catfile($File::Find::dir, $file);
        ($filename, $dir, $suffix) = fileparse($filepath, ".ui");
        $newfile = $filename . ".py";
        $newpath = catfile($File::Find::dir, $newfile);

        $command = "pyuic5 -o " . $newpath . " " . $filepath;
        system($command);

        if ((-e $newpath) && (-s $newpath) && ($newpath =~ /mainwindow/))
        {
            # Define a backup file for mainwindow.py
            $bakpath = $newpath . ".bak";

            open $readfh, "<$newpath" or die $!;
            @rawdata = <$readfh>;
            close $readfh;

            rename $newpath, $bakpath;

            open $writefh, ">$newpath" or die $!;
            foreach $line (@rawdata)
            {
                $line =~ s/custom/\.custom/;
                print $writefh $line;
            }
            close $writefh;
        }
    }
}
