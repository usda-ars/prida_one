#!/usr/bin/python
#
# rr3dat_extractor.py
#
# VERSION: 1.0.0-dev
#
# LAST EDIT: 2016-06-16
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################

# NOTE: in Linux (or on Mac) you can create the list of RR3DAT files from the
#       command line by running the following:
#
#       cd <path-to-RSADrobo>
#       find `pwd` -type f -name *.rr3dat > ~/my_rr3dat_files.txt
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import os
import sys
import xml.etree.ElementTree as ET

###############################################################################
# MAIN:
###############################################################################
# Assign the path:
if len(sys.argv) == 2:
    my_file = str(sys.argv[1])
    if not os.path.isfile(my_file):
        raise ValueError("File does not exist")
        print("Please enter an actual directory path")
    else:
        out_file = os.path.join(os.path.expanduser("~"), "prida_rr3dat.out")
        if not os.path.isfile(out_file):
            f = open(out_file, 'w')
        else:
            print("Please remove or rename %s" % (out_file))
            raise ValueError("File already exists!")

        # Check path for files:
        with open(my_file) as rf:
            my_files = rf.read().splitlines()
            print("Found %d files" % (len(my_files)))
            f.write("File,Roll.deg,Tran.px,Pitch.deg,zt.m,Ir.mm_px\n")
            i = 0
            for my_file in my_files:
                i += 1
                print("Processing (%d/%d) %s" % (
                    i, len(my_files), os.path.basename(my_file)))
                tree = ET.parse(my_file)
                root = tree.getroot()
                pinfo = root.find("ProcessingInfo")
                recon = pinfo.find("Reconstruction")
                roll = recon.find("Roll")
                trans = recon.find("Trans")
                pitch = recon.find("Pitch")
                zt = recon.find("Dist2AOR")
                ir = recon.find("PixelSize")

                f.write("%s," % (my_file))
                if roll is not None:
                    if roll.attrib.get("units", "") == "deg":
                        f.write("%s," % (roll.text))
                    else:
                        print("roll units: %s" % (
                            roll.attrib.get("units", "")))
                        f.close()
                        raise ValueError("Roll units do not match!")
                else:
                    f.write(",")
                if trans is not None:
                    if trans.attrib.get("units", "") == "px":
                        f.write("%s," % (trans.text))
                    else:
                        print("trans units: %s" % (
                            trans.attrib.get("units", "")))
                        f.close()
                        raise ValueError("Trans units do not match")
                else:
                    f.write(",")
                if pitch is not None:
                    if pitch.attrib.get("units", "") == "deg":
                        f.write("%s," % (pitch.text))
                    else:
                        print("pitch units: %s" % (
                            pitch.attrib.get('units', '')))
                        f.close()
                        raise ValueError("Pitch units do not match!")
                else:
                    f.write(",")
                if zt is not None:
                    if zt.attrib.get("units", "") == "m":
                        f.write("%s," % (zt.text))
                    else:
                        print("zt units: %s" % (zt.attrib.get('units', '')))
                        f.close()
                        raise ValueError("zt units do not match!")
                else:
                    f.write(",")
                if ir is not None:
                    if ir.attrib.get("units", "") == "mm/px":
                        f.write("%s\n" % (ir.text))
                    else:
                        print("Ir units: %s" % (ir.attrib.get('units', '')))
                        f.close()
                        raise ValueError("Ir units do not match!")
                else:
                    f.write("\n")
        f.close()
else:
    print("Usage: python rr3dat_extractor.py input_file.txt")
