#!/usr/bin/python
#
# multiproc.py
#
# VERSION: 1.0.0-dev
#
# LAST EDIT: 2016-02-05
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################

from multiprocessing import Process, Queue, Event, current_process, Lock
from time import sleep
from random import randint
try:
    from Queue import Empty, Full
except ImportError:
    from queue import Empty, Full
import datetime


###############################################################################
# CLASS DEFINITION:
###############################################################################
class PridaSim(object):
    """
    Name:     PridaSim
    Feature:  Dummy app simulating a potential workflow for the prida
              app during imaging
    """

    def __init__(self):
        """
        Name:    PridaSim.__init__
        Feature: Constructs an instance of a PridaSim object
        Inputs:  None
        Outputs: None
        """
        # Initialize Variables
        self.start_time = datetime.datetime.now()
        self.progress = 0
        self.num_photos = 10

        # Instantiate Dummy Processes
        self.hard_proc = Process(target=self.run_hardware, name='HDW')
        self.data_proc = Process(target=self.run_data, name='DAT')
        self.gui_proc = Process(target=self.run_gui, name='GUI')

        # Instantiate 'thread-safe' Queues
        self.img_q = Queue(maxsize=0)
        self.saved_q = Queue(maxsize=0)  # maxsize = 0 indicates no maximum
        self.prog_q = Queue(maxsize=0)
        self.delay_q = Queue(maxsize=0)  # <-- for reporting "maximum" time

        # Instantiate Events of Interest
        self.camera_e = Event()
        self.motor_e = Event()
        self.hw_finished = Event()
        self.data_finished = Event()

        # Instaniate Hardware Lock
        self.hardware_lock = Lock()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Member Functions
    # /////////////////////////////////////////////////////////////////////////
    def capture_image(self, i):
        """
        Name:    PridaSim.capture_image
        Feature: Simulates capturing an image. Waits a small amount of time,
                 adds an item to the image queue and updates the progress
                 queue.
        Inputs:  int, current image number (i)
        Outputs: None
        """
        name = current_process().name
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: capturing image number %s' % (time, name, i))

        # Dummy wait for image capture
        delay = randint(1, 2)
        sleep(delay)

        # Add the value to the image queue
        self.img_q.put(i, block=True)
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: image number %s captured' % (time, name, i))

        # Add to the progress queue and set the move event
        self.prog_q.put(1)
        self.delay_q.put(delay)

    def move_motor(self):
        """
        Name:    PridaSim.move_motor
        Feature: Simulates moving the motor. Waits a small amount of time and
                 updates the progress queue.
        Inputs:  None
        Outputs: None
        """
        name = current_process().name
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: moving motor' % (time, name))

        # Dummy wait for motor movement
        delay = randint(1, 2)
        sleep(delay)
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: motor moved' % (time, name))

        # Add to the progress queue and set the move event
        self.prog_q.put(1)
        self.delay_q.put(delay)

    def run_camera(self):
        """
        Name:    PridaSim.run_camera
        Feature: Simulates the camera subprocess' run function. Waits for a
                 completed motor event, aquires the hardware lock, calls the
                 camera's capture image function, incriments the photos taken
                 counter and sets the camera event.
        Inputs:  None
        Outputs: None
        """
        # Initialize counter
        taken = 0
        # Enter camera loop
        while taken < self.num_photos:
            # wait for motor event
            self.motor_e.wait()
            # Aquire hardware lock
            with self.hardware_lock:
                # Capture image and manage events
                self.motor_e.clear()
                self.capture_image(taken)
                taken += 1
                self.camera_e.set()

    def run_data(self):
        """
        Name:    PridaSim.run_data
        Feature: Simulates the data process' run function. Calls the save image
                 function in a loop until both the hardware event is set and
                 the image queue is empty.
        Inputs:  None
        Outputs: None
        """
        name = current_process().name
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: Data START' % (time, name))

        # Loop the save function until all images are captured and saved
        while not (self.hw_finished.is_set() and self.img_q.empty()):
            self.save_image()

        # Set the data status event to indicate completion
        self.data_finished.set()
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: Data DONE' % (time, name))

    def run_gui(self):
        """
        Name:    PridaSim.run_gui
        Feature: Simulates the gui process' run function. Calls the update gui
                 function in a loop until both the data event is set and
                 the progress queue is empty.
        Inputs:  None
        Outputs: None
        """
        name = current_process().name
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: Gui START' % (time, name))

        # Loop the update gui function until all progress is displayed
        while not (self.data_finished.is_set() and self.prog_q.empty()):
            self.update_gui()
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: Gui DONE' % (time, name))

    def run_hardware(self):
        """
        Name:    PridaSim.run_hardware
        Feature: Simulates the hardware process' run function. Creates the
                 camera and motor subprocesses, starts them, and waits for the
                 camera to have finished before setting the hardware finished
                 event.
        Inputs:  None
        Outputs: None
        """
        name = current_process().name
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: Hardware START' % (time, name))

        # create subprocesses
        self.camera_proc = Process(target=self.run_camera, name='CAM')
        self.motor_proc = Process(target=self.run_motor, name='MTR')

        # Start the hardware loop
        self.camera_e.set()
        self.motor_proc.start()
        self.camera_proc.start()
        self.camera_proc.join()

        # Set the hardware status event to indicate completion
        self.hw_finished.set()
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: Hardware DONE' % (time, name))

    def run_motor(self):
        """
        Name:    PridaSim.run_motor
        Feature: Simulates the motor process' run function. Waits for the
                 camera even to be set, aquires the hardware lock, moves the
                 motor and sets the motor event.
        Inputs:  None
        Outputs: None
        """
        name = current_process().name
        # Enter motor loop
        while not self.hw_finished.is_set():
            # Wait for camera event
            if self.camera_e.wait(timeout=5):
                # Aquire hardware lock
                with self.hardware_lock:
                    # Move motor and manage events
                    self.camera_e.clear()
                    self.move_motor()
                    self.motor_e.set()
            else:
                # Alert user to timeout
                time = datetime.datetime.now() - self.start_time
                print('%s: %s: motor timeout, checking status' % (time, name))

    def save_image(self):
        """
        Name:    PridaSim.save_image
        Feature: Simulates saving an image. waits for an item in the image
                 queue, waits a small amount of time and adds the item to the
                 save queue and updates the progress queue.
        Inputs:  None
        Outputs: None
        """
        name = current_process().name
        time = datetime.datetime.now() - self.start_time
        print ('%s: %s: waiting to aquire image' % (time, name))

        # Get an item from the image queue when available
        try:
            item = self.img_q.get(block=True, timeout=5)
            time = datetime.datetime.now() - self.start_time
            print('%s: %s: image "%s" aquired' % (time, name, item))
            time = datetime.datetime.now() - self.start_time
            print('%s: %s: saving image "%s"' % (time, name, item))
        except Empty:
            time = datetime.datetime.now() - self.start_time
            print('%s: %s: no images to aquire, timeout' % (time, name))
        # Dummy wait for image save
        else:
            delay = randint(3, 6)
            sleep(delay)
            time = datetime.datetime.now() - self.start_time
            print('%s: %s: image "%s" saved' % (time, name, item))

            # Add the item to the saved queue and add to the progress queue
            self.saved_q.put(item)
            self.prog_q.put(1)
            self.delay_q.put(delay)

    def start(self):
        """
        Name:    PridaSim.start
        Feature: Starts the simulation. Calls each subprocess' start functions
                 and sets the events to start the imaging sequence.
        Inputs:  None
        Outputs: None
        """
        self.start_time = datetime.datetime.now()
        self.hard_proc.start()
        self.gui_proc.start()
        self.data_proc.start()

    def update_gui(self):
        """
        Name:    PridaSim.update_gui
        Feature: Removes an item from the progress queue and updates
                 the progress value
        Inputs:  None
        Outputs: None
        """
        name = current_process().name
        time = datetime.datetime.now() - self.start_time
        print('%s: %s: waiting for gui updates' % (time, name))

        # Update the progress value from the progress queue
        try:
            self.progress += self.prog_q.get(block=True, timeout=5)
        except Empty:
            time = datetime.datetime.now() - self.start_time
            print('%s: %s: no new updates, timeout' % (time, name))
        else:
            time = datetime.datetime.now() - self.start_time
            print('%s: %s: gui updated' % (time, name))
            time = datetime.datetime.now() - self.start_time
            print('%s: %s: progress val = %s' % (time, name, self.progress))


if __name__ == '__main__':
    app = PridaSim()
    app.start()
    app.gui_proc.join()
    end_time = datetime.datetime.now()
    elapsed_time = end_time - app.start_time
    print('\nTotal elapsed time: \n\t%s' % (elapsed_time))
    tot_delay = 0
    for i in range(app.delay_q.qsize()):
        tot_delay += app.delay_q.get()
    max_time = datetime.timedelta(seconds=tot_delay)  # assuming blocking
                                                      # functions on a single
                                                      # process
    saved_time = max_time - elapsed_time
    print('\nTotal time saved: \n\t%s' % (saved_time))
    print('\nImages saved: \n\t%s' % (app.saved_q.qsize()))