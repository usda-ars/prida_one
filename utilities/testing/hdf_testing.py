#!/usr/bin/python
#
# hdf_organizer.py
#
# written by Tyler W. Davis
# USDA-ARS, Robert W. Holley Center for Agriculture and Health
#
# 2015-06-10 -- created
# 2015-06-19 -- last updated
#
# ------------
# description:
# ------------
# This script sets up the HDF5 (Hierarchical Data Formal) files for storing 
# plant root images (a part of the PRIDA Project).
#
# The general workflow follows this pattern:
#   1. Create a class instance:
#      ``````````````````````
#      my_class = PRIDA_HDF()
#      ,,,,,,,,,,,,,,,,,,,,,,
#
#   2. Create new / open existing an HDF5 file:
#      a. Create new file:
#         ```````````````````````````
#         my_class.new_file(str, str)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Open existing file:
#         ````````````````````````````
#         my_class.open_file(str, str)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   3. Save user's details to the file (equivalent get functions available):
#      ```````````````````````````
#      my_class.set_root_user(str)
#      my_class.set_root_addr(str)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,, 
#
#   4. Create a new / open an existing experiment:
#      a. Create new experiment:
#         `````````````````````````
#         my_class.new_experiment()
#         ,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Open existing experiment:
#         `````````````````````````````
#         my_class.open_experiment(str)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   5. Set current experiment's details (equivalent get functions available):
#      ````````````````````````````````````````
#      my_class.set_exp_title(str)
#      my_class.set_exp_genus_species(str, str)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   6. Create new / open experiment rep:
#      a. Create new rep:
#         ``````````````````
#         my_class.new_rep()
#         ,,,,,,,,,,,,,,,,,,
#
#      b. Open existing rep:
#         ``````````````````````
#         my_class.open_rep(str)
#         ,,,,,,,,,,,,,,,,,,,,,,
#
#   7. Set rep details (equivalent get functions available):
#      ````````````````````````````````````
#      my_class.set_rep_date(datetime.date)
#      my_class.set_rep_number(int)
#      my_class.set_rep_images(int)
#      my_class.set_rep_cultivar(str)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
# -----
# todo:
# -----
# 
###############################################################################
## REQUIRED MODULES:
###############################################################################
import cv2
import datetime
import glob
import h5py
import numpy
import os.path

# For debugging purposes only:
from matplotlib import pyplot as plt

###############################################################################
## CLASSES
###############################################################################
class PRIDA_HDF:
    """
    Name:     PRIDA_HDF
    Features: This class handles the organization and storage of plant root
              images and their associated meta data into a readable/writeable 
              HDF5 (Hierarchical Data Format) file
    History:  VERSION 0.0.2
              - added cv2, datetime, glob, and os.path modules [15.06.11]
              - create_experiment & create_rep class functions [15.06.12]
              - created select_hdf_file function [15.06.16]
              - moved 'cultivar' from experiment to rep meta data [15.06.16]
              - set cur_exp/cur_rep to last values in existing file [15.06.16]
              - create_hdf and open_hdf separated into own functions [15.06.18]
              - individual set/get root group meta data functions [15.06.18]
              - updated create_experiment function [15.06.18]
              - individual set/get experiment meta data functions [15.06.18]
              - rename open_file / new_file functions [15.06.19]
              - new / open experiment functions [15.06.19]
              - set / get experiment and rep meta data functions [15.06.19]
              - get_datasets function [15.06.19]
              - get_image function [15.06.19]
    Ref:      https://www.hdfgroup.org/HDF5/
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization 
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     PRIDA_HDF.__init__
        Features: Opens existing or creates a new HDF5 file.
        Inputs:   None.
        Outputs:  None.
        """
        # Define class constants:
        self.TIMEFORMAT = "%Y-%m-%d"           # strftime format
        self.isopen = False
        self.cur_exp = None  # \  set to None type to catch errors when running
        self.cur_rep = None  # /  an experiment without creating an exp or rep
    #
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def get_datasets(self):
        """
        Name:     PRIDA_HDF.get_datasets
        Features: Returns a list of datasets in the current rep
        Inputs:   None.
        Outputs:  list, dataset names (str)
        """
        dsets = []
        if self.isopen:
            if self.cur_rep:
                for dset in self.hdfile[self.cur_rep].keys():
                    dsets.append(dset)
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not get datasets. HDF5 file not open!"
        #
        return dsets
    #
    def get_exp_genus_species(self):
        """
        Name:     PRIDA_HDF.get_exp_genus_species
        Features: Returns list of current experiment genus and species
        Inputs:   None.
        Outputs:  list, genus (str) and species (str)
        """
        exp_genus = ''
        exp_species = ''
        if self.isopen:
            if self.cur_exp:
                try:
                    exp_genus = self.hdfile[self.cur_exp].attrs["genus"]
                    exp_species = self.hdfile[self.cur_exp].attrs["species"]
                except:
                    # Experiment meta data not set!
                    exp_genus = 'N/A'
                    exp_species = 'N/A'
                finally:
                    exp_gen_spec = [exp_genus, exp_species]
                    return exp_gen_spec
            else:
                print "Error! No experiment selected!"
        else:
            print ("Error! Could not get experiment genus and species. "
                   "HDF5 file not open!")
    #
    def get_exp_title(self):
        """
        Name:     PRIDA_HDF.get_exp_title
        Features: Returns current experiment title
        Inputs:   None.
        Outputs:  str, experiment title (exp_title)
        """
        exp_title = ''
        if self.isopen:
            if self.cur_exp:
                try:
                    exp_title = str(self.hdfile[self.cur_exp].attrs["title"])
                except:
                    # Attribute not set!
                    exp_title = "N/A"
                finally:
                    return exp_title
            else:
                print "Error! No experiment selected!"
        else:
            print "Error! Could not get experiment title. HDF5 file not open!"
    #
    def get_image(self, iname):
        """
        Name:     PRIDA_HDF.get_image
        Features: Returns the dataset for a given image in current rep
        Inputs:   str, imange name (iname)
        Outputs:  numpy.ndarray
        """
        img = numpy.array([])
        if self.isopen:
            if self.cur_rep:
                if iname in self.hdfile[self.cur_rep]:
                    img = self.hdfile[self.cur_rep][iname][:,:]
                else:
                    print "Error! Could not find image in datasets."
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not get datasets. HDF5 file not open!"
        #
        return img
    #
    def get_rep_cultivar(self):
        """
        Name:     PRIDA_HDF.get_rep_cultivar
        Features: Returns the current rep cultivar
        Inputs:   None.
        Outputs:  str, cultivar of current rep (rcult)
        """
        rcult = ''
        if self.isopen:
            if self.cur_rep:
                try:
                    rcult = str(self.hdfile[self.cur_rep].attrs["cultivar"])
                except:
                    # Attribute not set!
                    rcult = "N/A"
                finally:
                    return rcult
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not get rep cultivar. HDF5 file not open!"
    #
    def get_rep_date(self):
        """
        Name:     PRIDA_HDF.get_rep_date
        Features: Returns the current rep date
        Inputs:   None.
        Outputs:  str, date of rep (rdate)
        """
        rdate = ''
        if self.isopen:
            if self.cur_rep:
                try:
                    rdate = str(self.hdfile[self.cur_rep].attrs["date"])
                except:
                    # Attribute not set!
                    rdate = 'N/A'
                finally:
                    return rdate
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not get rep date. HDF5 file not open!"
    #
    def get_rep_images(self):
        """
        Name:     PRIDA_HDF.get_rep_images
        Features: Returns the number of images to take for current rep
        Inputs:   None.
        Outputs:  int, number of rep (rtot)
        """
        rtot = 0
        if self.isopen:
            if self.cur_rep:
                try:
                    rtot = int(self.hdfile[self.cur_rep].attrs["total_images"])
                except ValueError:
                    # Attribute not a number!
                    rtot = -9999
                except:
                    # Attribute not set!
                    rtot = None
                finally:
                    return rtot
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not get number of images. HDF5 file not open!"
    #
    def get_rep_number(self):
        """
        Name:     PRIDA_HDF.get_rep_num
        Features: Returns the current rep number
        Inputs:   None.
        Outputs:  int, number of rep (rnum)
        """
        rnum = -1
        if self.isopen:
            if self.cur_rep:
                try:
                    rnum = int(self.hdfile[self.cur_rep].attrs["number"])
                except ValueError:
                    # Attribute not a number!
                    rnum = -9999
                except:
                    # Attribute not set!
                    rnum = None
                finally:
                    return rnum
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not get rep number. HDF5 file not open!"
    #
    def get_root_addr(self):
        """
        Name:     PRIDA_HDF.get_root_addr
        Features: Returns the root group user address (e.g., NetID)
        Inputs:   None.
        Outputs:  str, root group user address
        """
        root_addr = ''
        if self.isopen:
            try:
                # NOTE: addr should already be str, but in case it isn't this
                #       type cast should minimize issues downstream
                root_addr = str(self.hdfile.attrs['addr'])
            except:
                # Root user meta data not set!
                root_addr = 'N/A'
            finally:
                return root_addr
        else:
            print "Could not get root addr. HDF5 file not open!"
    #
    def get_root_user(self):
        """
        Name:     PRIDA_HDF.get_root_user
        Features: Returns the root group username
        Inputs:   None.
        Outputs:  str, root group username
        """
        root_user = ''
        if self.isopen:
            try:
                # NOTE: user should already be str, but in case it isn't this
                #       type cast should minimize issues downstream
                root_user = str(self.hdfile.attrs['user'])
            except:
                # Root user meta data not set!
                root_user = ''
            finally:
                return root_user
        else:
            print "Could not get root user. HDF5 file not open!"
    #
    def img_to_hdf(self, img, dname):
        """
        Name:     PRIDA_HDF.img_to_hdf
        Features: Saves image dataset to current HDF repository; currently set
                  with gzip compression.
        Inputs:   - numpy.ndarray, openCV image data array (img)
                  - str, image/dataset name (dname)
        """
        if self.cur_rep:
            ds_name = self.cur_rep + '/' + dname
            if ds_name in self.hdfile:
                print "WARNING: Overwriting dataset", self.cur_rep, dname
            #
            try:
                self.hdfile.create_dataset(name=ds_name, 
                                           data=img, 
                                           compression='gzip')
            except:
                print "PRIDA_HDF.img_to_hdf ERROR: could not create dataset",
                print dname
            else:
                # Save image dimensions (i.e., pixel y, pixel x, color bands):
                # NOTE: openCV reads images in BGR not RGB
                self.hdfile[ds_name].dims[0].label = 'pixels in y'
                self.hdfile[ds_name].dims[1].label = 'pixels in x'
                self.hdfile[ds_name].dims[2].label = 'BGR color bands'
        else:
            print "Error! Could not save dataset. No rep selected!"
    #
    def new_experiment(self):
        """
        Name:     PRIDA_HDF.new_experiment
        Features: Creates a new experiment (i.e., appends new experiment to the
                  list of existing experiments)
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            # Find the number of existing experiments:
            num_exp = len(self.hdfile.keys())
            #
            # Create the next experiment group by incrementing by one:
            ename = '/exp%02d' % (num_exp + 1)
            try:
                self.hdfile.create_group(ename)
            except:
                print "Error! Could not create new experiment!"
            else:
                self.cur_exp = ename
                self.cur_rep = None
        else:
            print "Could not create new experiment. HDF5 file not open!"
        #
    #
    def new_file(self, dname, fname):
        """
        Name:     PRIDA_HDF.new_file
        Features: Creates a new HDF5 file, overwriting an existing file if it
                  exists.
        Inputs:   - str, directory path (dname)
                  - str, filename (fname)
        Outputs:  None.
        """
        # Save directory and filename as class variables:
        self.dir = dname
        self.filename = fname
        my_hdf = dname + fname
        #
        # Create empty HDF file:
        if os.path.isfile(my_hdf):
            print "Warning! Overwriting existing file", fname, "...",
        else:
            print "Creating file", fname, "...",
        #
        try:
            self.hdfile = h5py.File(my_hdf, 'w')
        except:
            print "failed!"
            self.isopen = False
        else:
            print "success!"
            self.isopen = True
    #
    def new_rep(self):
        """
        Name:     PRIDA_HDF.new_rep
        Features: Creates a new repository group for storing images
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            if self.cur_exp:
                # Find the number of existing reps:
                num_rep = len(self.hdfile[self.cur_exp].keys())
                #
                # Create the next rep group by incrementing by one:
                rname = '%s/rep%02d' % (self.cur_exp, num_rep + 1)
                #
                try:
                    self.hdfile.create_group(rname)
                except:
                    print "Error! Could not create new rep!"
                else:
                    self.cur_rep = rname
            else:
                print "Error! Could not create new rep. No experiment selected!"
        else:
            print "Error! Could not create new rep. HDF5 file not open!"
    #
    def open_experiment(self, ename):
        """
        Name:     PRIDA_HDF.open_experiment
        Features: Opens an existing experiment (i.e., sets the current 
                  experiment flag)
        Inputs:   str, experiment group name
        Outputs:  None.
        """
        if self.isopen:
            if ename in self.hdfile:
                self.cur_exp = ename
            else:
                print "Error! Could not find experiment", ename
        else:
            print "Error! Could not open experiment. HDF5 file not open!"
        #
    #
    def open_file(self, dname, fname):
        """
        Name:     PRIDA_HDF.open_file
        Features: Opens an existing HDF5 file or creates new if file is not 
                  found.
        Inputs:   - str, directory path (dname)
                  - str, filename (fname)
        Outputs:  None.
        """
        # Save directory and filename as class variables:
        self.dir = dname
        self.filename = fname
        my_hdf = dname + fname
        #
        # Open existing HDF file:
        if os.path.isfile(my_hdf):
            print "Opening file", fname, "...",
        else:
            print "Could not find file! Creating", fname, "...",
        #
        try:
            # NOTE: 'a' flag opens existing or creates new
            self.hdfile = h5py.File(my_hdf, 'a')
        except:
            print "failed!"
            self.isopen = False
        else:
            print "success!"
            self.isopen = True
            self.cur_exp = None  # \  Set to None type by default for 
            self.cur_rep = None  # /  newly opened file.
    #
    def open_rep(self, rname):
        """
        Name:     PRIDA_HDF.open_experiment
        Features: Opens an existing rep (i.e., sets the current rep flag) from
                  within the current experiment
        Inputs:   str, rep group name
        Outputs:  None.
        """
        if self.isopen:
            if rname in self.hdfile[self.cur_exp]:
                self.cur_rep = "%s/%s" % (self.cur_exp, rname)
            else:
                print "Error! Could not find rep", rname
        else:
            print "Error! Could not open rep. HDF5 file not open!"
        #
    #
    def save(self):
        """
        Name:     PRIDA_HDF.save
        Features: Closes the HDF file, saving changes to disk
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile.close()
            except:
                pass # already closed
            finally:
                self.isopen = False
                self.cur_exp = None
                self.cur_rep = None
    #
    def save_image(self, ifile):
        """
        Name:     PRIDA_HDF.save_image
        Features: Saves image data as a dataset in the current rep
        Input:    str, image filename with path (ifile)
        Output:   None.
        Depends:  img_to_hdf()
        """
        if self.isopen:
            # NOTE: image name (iname) includes the original file extension
   	    iname = os.path.basename(ifile)
            img = cv2.imread(ifile)
            self.img_to_hdf(img, iname)
        else:
            print "Error! Could not save image. HDF5 file not open!"
    #
    def set_exp_genus_species(self, genus, species):
        """
        Name:     PRIDA_HDF.set_exp_genus_species
        Features: Sets current experiment genus and species
        Inputs:   - str, experiment genus (genus)
                  - str, experiment species (species)
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile[self.cur_exp].attrs.create(name="genus", 
                                                       data=genus)
                self.hdfile[self.cur_exp].attrs.create(name="species", 
                                                       data=species)
            except:
                print ("PRIDA_HDF.set_exp_genus_species ERROR: "
                       "could not create experiment attribute.")
        else:
            print ("Error! Could not set experiment genus and species. "
                   "HDF5 file not open!")
    #
    def set_exp_title(self, exp_title):
        """
        Name:     PRIDA_HDF.set_exp_title
        Features: Sets current experiment title
        Inputs:   str, experiment title (exp_title)
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile[self.cur_exp].attrs.create(name="title", 
                                                       data=exp_title)
            except:
                print ("PRIDA_HDF.set_exp_title ERROR: "
                       "could not create experiment attribute.")
        else:
            print "Error! Could not set experiment title. HDF5 file not open!"
    #
    def set_rep_cultivar(self, rcult):
        """
        Name:     PRIDA_HDF.set_rep_cultivar
        Features: Sets the current rep cultivar
        Inputs:   str, cultivar of current rep (rcult)
        Outputs:  None.
        """
        if self.isopen:
            if self.cur_rep:
                try:
                    self.hdfile[self.cur_rep].attrs.create(name="cultivar", 
                                                           data=rcult)
                except:
                    print ("PRIDA_HDF.set_rep_cultivar ERROR: "
                           "Could not create new attribute")
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not set rep cultivar. HDF5 file not open!"
    #
    def set_rep_date(self, rdate):
        """
        Name:     PRIDA_HDF.set_rep_date
        Features: Sets the current rep date
        Inputs:   datetime.date, date of rep (rdate)
        Outputs:  None.
        """
        # Set experiment's rep meta data:
        if self.isopen:
            if self.cur_rep:
                try:
                    self.hdfile[self.cur_rep].attrs.create(
                        name="date", 
                        data=rdate.strftime(self.TIMEFORMAT)
                    )
                except:
                    print ("PRIDA_HDF.set_rep_date ERROR: "
                           "Could not create new attribute")
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not set rep date. HDF5 file not open!"
    #
    def set_rep_images(self, rtot):
        """
        Name:     PRIDA_HDF.set_rep_images
        Features: Sets the number of images to take for current rep
        Inputs:   int, number of rep (rtot)
        Outputs:  None.
        """
        if self.isopen:
            if self.cur_rep:
                try:
                    self.hdfile[self.cur_rep].attrs.create(name="total_images", 
                                                           data=rtot)
                except:
                    print ("PRIDA_HDF.set_rep_images ERROR: "
                           "Could not create new attribute")
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not set number of images. HDF5 file not open!"
    #
    def set_rep_number(self, rnum):
        """
        Name:     PRIDA_HDF.set_rep_num
        Features: Sets the current rep number
        Inputs:   int, number of rep (rnum)
        Outputs:  None.
        """
        # Set experiment's rep meta data:
        if self.isopen:
            if self.cur_rep:
                try:
                    self.hdfile[self.cur_rep].attrs.create(name="number", 
                                                           data=rnum)
                except:
                    print ("PRIDA_HDF.set_rep_num ERROR: "
                           "Could not create new attribute")
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not set rep number. HDF5 file not open!"
    #
    def set_root_addr(self, root_addr):
        """
        Name:     PRIDA_HDF.set_root_addr
        Features: Sets the root group user address (e.g., NetID)
        Inputs:   str, root group user address (root_addr)
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile.attrs.create(name="addr", data=root_addr)
            except:
                print ("PRIDA_HDF.set_root_addr ERROR: "
                       "could not create root attribute.")
        else:
            print "Could not set root addr. HDF5 file not open!"
    #
    def set_root_user(self, root_user):
        """
        Name:     PRIDA_HDF.set_root_user
        Features: Sets the root group username
        Inputs:   str, root group username (root_user)
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile.attrs.create(name="user", data=root_user)
            except:
                print ("PRIDA_HDF.set_root_user ERROR: "
                       "could not create root attribute.")
        else:
            print "Could not set root user. HDF5 file not open!"
    #
    def show_image(self, iname):
        """
        Name:     PRIDA_HDF.show_image
        Features: Displays a given image using matplotlib
        Inputs:   str, imange name (iname)
        Outputs:  None.
        """
        if self.isopen:
            if self.cur_rep:
                if iname in self.hdfile[self.cur_rep]:
                    img = self.hdfile[self.cur_rep][iname]
                    #dims = [i.dims for i in img.dims]
                    #
                    fig = plt.figure()
                    ax1 = fig.add_subplot(111)
                    plt.setp(ax1.get_yticklabels(), rotation=0, fontsize=12)
                    plt.setp(ax1.get_xticklabels(), rotation=0, fontsize=12)
                    ax1.imshow(img[:,:])
                else:
                    print "Error! Could not find image in datasets."
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not plot image. HDF5 file not open!"
    #
###############################################################################
## FUNCTIONS
###############################################################################
def user_select_hdf(d):
    """
    Name:     user_select_hdf
    Features: Returns HDF file based on user selection
    Inputs:   str, directory path (d)
    Outputs:  str, hdf file w/ path (hdf_to_ret)
    
    NOT USED!
    """
    # Initialize return string:
    hdf_to_ret = ''
    #
    # Search for HDF files within the given directory
    my_hdf_files = glob.glob(d + '*.hdf5')
    #
    if my_hdf_files:
        num_files = len(my_hdf_files)
        #
        # Set looping boolean (in case wrong selection is made):
        done = False
        while not done:
            print "Found", num_files, "files.",
            print "Select file to process:"
            i = 1
            for hdf_file in my_hdf_files:
                print "%d. %s" % (i, os.path.basename(hdf_file))
                i += 1
            user_select = input('> ')
            user_select = int(user_select)
            if user_select > 0 and user_select <= num_files:
                hdf_to_ret = my_hdf_files[user_select - 1]
                print "You selected", os.path.basename(hdf_to_ret), 
                print "is this correct (y/n)?"
                is_done = raw_input("> ")
                if is_done.lower() == 'y':
                    done = True
                else:
                    done = False
                    print "Try again.",
    else:
        print "Found no HDF files. Check directory and try again."
    #
    return hdf_to_ret

###############################################################################
## MAIN:
###############################################################################
if __name__ == '__main__':
    #pass
    #
    # Configure directory, filename and meta data:
    my_dir = "/Users/twdavis/Desktop/temp/"
    my_file = "exp03.hdf5"
    img_dir = ('/Users/twdavis/Dropbox/Work/USDA/Projects/PRIDA/'
            'example_images/seq/')
    #
    #
    # WRITING NEW HDF FILE EXAMPLE:
    my_class = PRIDA_HDF()
    #
    try:
        ## 1. Create empty HDF file:
        my_class.new_file(my_dir, my_file)
        #my_class.open_file(my_dir, my_file)
    except:
        print "Could not create HDF file", my_file
    else:
        ## 2. Set the root group meta data:
        my_class.set_root_user('Tyler')
        my_class.set_root_addr('twd34')
        #
        # Check to see if they're set correctly:
        print my_class.get_root_user()
        print my_class.get_root_addr()
        #
        ## 3. Create a new experiment (subgroup of root):
        my_class.new_experiment()
        my_class.set_exp_title('My Experiment')
        my_class.set_exp_genus_species(genus='Carcharodon', 
                                    species='carcharias')
        #
        print my_class.get_exp_title()
        print my_class.get_exp_genus_species()
        #
        # Select existing experiment:
        my_class.open_experiment('exp02')
        #
        ## 4. Create a rep for your experiment (sub-subgroup for storing images):
        my_class.new_rep()
        my_class.set_rep_date(datetime.date.today())
        my_class.set_rep_number(231)
        my_class.set_rep_images(4)
        my_class.set_rep_cultivar('Taxus baccata variegata')
        #
        # Check to see if they're set correctly:
        print my_class.get_rep_date()
        print my_class.get_rep_number()
        print my_class.get_rep_images()
        print my_class.get_rep_cultivar()
        #
        # Select an existing rep:
        my_class.open_rep('rep02')
        #
        ## 5. Save images to current repository:
        my_images = glob.glob(img_dir + '*.jpg')
        for my_img in sorted(my_images):
   	    my_class.save_image(my_img)
        #
        # Show datasets saved in rep:
        for dset in my_class.get_datasets():
            print dset
        #
        ## 6. Save HDF file (flushes data from buffer to disk):
        my_class.save()
    #
    # READING EXISTING HDF FILE EXAMPLE:
    my_class = PRIDA_HDF()
    my_class.open_file(my_dir, my_file)
    #
    #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~ TESTING GROUNDS ~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    my_dir = "/Users/twdavis/Desktop/temp/"
    my_hdf = my_dir + "example.hdf5"
    my_tif = my_dir + "root_gel_ex02.tif"
    my_jpg = my_dir + "root_mesh_ex01.jpg"
    #
    #
    #
    # Open images w/ OpenCV as unsigned 8-bit arrays
    img_tif = cv2.imread(my_tif)
    img_jpg = cv2.imread(my_jpg)
    #
    #
    # Creates a blank HDF5 file:
    f = h5py.File(my_hdf, 'w')
    f.name     # '/' is the "root group" (unicode string)
    f.filename # name of the file on disk (unicode string)
    f.mode     # 'r+' read/write (NOTE: 'w' becomes 'r+')
    #
    #
    # Set root group attributes:
    f.attrs.create(name="Name", data="Tyler W. Davis")
    f.attrs.create(name="NetID", data="twd34")
    #
    #
    # Create some new groups:
    grp = f.create_group('subgroup')
    grp.name   # '/subgroup' (unicode string)
    grp.file   # the file instance in which the group resides
    grp.parent # group instance containing this group (e.g., "/" root)
    f.keys()   # list of subgroups/ datasets 
    #            NOTE: Python 3 returns objects instead of lists
    #
    #
    # Print the names of all subgroups (i.e., everything under "root")
    for name in f:
        print name
    #
    #
    # Create a dataset under subgroup:
    dset = grp.create_dataset("mydataset", (100,), dtype='f')
    dset.name   # mydataset
    dset.shape  # (100,)
    dset.dtype  # float32
    dset.size   # number of elements (i.e., 100)
    dset.file   # the file instance in which the data resides
    dset.parent # group instance containing this data
    "mydataset" in grp            # True
    grp.__contains__("mydataset") # True
    "subgroup/mydataset" in f     # True
    #
    #
    # Assign attributes
    # NOTE: these can be assigned to either groups or datasets
    #       * they can be created from any scalar or numpy array
    #       * each should be small (i.e., < 64k)
    #       * there is no slicing (entire attribute must be read)
    dset.attrs['temperature'] = 23.5
    dset.attrs.create(name="elevation", data=101.0, shape=(), dtype='f')
    dset.attrs.__contains__("temperature") # True
    dset.attrs.keys()
    dset.attrs.values()
    #
    #
    # Modify an attribute value (preserves shape and type)
    dset.attrs.modify("elevation", 200.25)
    #
    #
    # Initialize dataset to an existing numpy array:
    # NOTE: in "create_dataset," you can set compression flag, i.e.
    #       f.create_dataset('name', data=my_data, compression='gzip'),
    #       which results in compressed data when written to disk that is
    #       decompressed when read from disk
    my_array = numpy.array([i for i in xrange(16)])
    dset2 = f.create_dataset("myarray", data=my_array)
    dset2.resize((4,4)) # typical numpy resize behaviour
    dset2.name
    dset2.shape
    dset2.dtype
    dset2[:,0]          # numpy.ndarray of all rows, first column
    dset2[my_array > 5] # apply numpy boolean 'mask' arrays
    dset2.len()         # typical length behaviour 
    #
    #
    # Set data dimensions
    f['myarray'].dims[0].label = 'y'
    f['myarray'].dims[1].label = 'x'
    #
    #
    # Define region reference (e.g., image ROI) & save as data attribute:
    regref = dset2.regionref[1:3, 1:3]
    dset2.attrs['ROI'] = regref
    dset2[regref]                # returns the region you defined above
    dset2[dset2.attrs['ROI']]    # alt. method
    #
    #
    # Flush data buffers to disk:
    f.flush()