from Adafruit_MotorHAT import Adafruit_MotorHAT
import time
import atexit

mh = Adafruit_MotorHAT()

def turn_off_motors():
    mh.getMotor(1).run(mh.RELEASE)
    mh.getMotor(2).run(mh.RELEASE)
    mh.getMotor(3).run(mh.RELEASE)
    mh.getMotor(4).run(mh.RELEASE)


atexit.register(turn_off_motors)


def test_step(ipa_step, rpm, step_type):
    """
    Function to perform a single Inter-Photo-Angle (IPA) step. Takes the desired number of photos in the run 
    and calculates the IPAstep size, and converts the desired RPM value into the appropriate length wait time.
    Inputs: 
        ipa_step : integer - Integer value representing the number of IPA steps to move a single rotation
        rpm: float - Decimal number representing the rotational velocity of the system during a step
        step_type: integer - Integer value representing the stepping regieme to be used
    Outputs:
        None
    """
    #instantiate stepper object
    ms = mh.getStepper(200, 1)   # Steps/rev (unused), motor port #
    #user input error checking
    if ipa_step <= 0 or ipa_step > 200:
        print("Please select a valid IPA step value (0,200].")
        return None
    if rpm <= 0:
        print("RPM must be greater than zero.")
        return None
    if step_type == mh.SINGLE or step_type == mh.DOUBLE:
        steps_per_unit_step = 1
        base_sec_per_rot = 1.7
    elif step_type == mh.INTERLEAVE:
        steps_per_unit_step = 2
        base_sec_per_rot = 3.4
    elif step_type == mh.MICROSTEP:
        steps_per_unit_step = ms.MICROSTEPS
        base_sec_per_rot = 14
    else:
        print("Please select a valid stepping regieme.")
        return None
    try:
        #convert desired RPM to real, required value for calculating wait time
        real_rpm = 1.0 / (((60.0 / rpm) - base_sec_per_rot) / 60)
    except ZeroDivisionError:
        print("Enter a valid RPM value.")
    else:
        #begin stepping
        for i in xrange(200*steps_per_unit_step/ipa_step):
            ms.oneStep(mh.FORWARD, step_type)
            time.sleep(60.0 / (200*steps_per_unit_step*real_rpm))


def calibrate():
    ms = mh.getStepper(200, 1)   # Steps/rev (unused), motor port #
    for i in xrange(48):
        ms.oneStep(mh.FORWARD, mh.MICROSTEP)
        time.sleep(.1)


def full_rot(ipa_step, rpm, step_type):
    calibrate()
    for i in xrange(ipa_step):
        test_step(ipa_step,  rpm, step_type)
        time.sleep(.1)

def find_top_speed(step_type):
    #instantiate stepper object
    ms = mh.getStepper(200, 1)   # Steps/rev (unused), motor port #
    if step_type == mh.SINGLE or step_type == mh.DOUBLE:
        steps_per_unit_step = 1
    elif step_type == mh.INTERLEAVE:
        steps_per_unit_step = 2
    elif step_type == mh.MICROSTEP:
        steps_per_unit_step = ms.MIRCOSTEPS
    else:
        print("Please select a valid stepping regieme.")
        return None
    start_time = time.time()
    for i in xrange(200*steps_per_unit_step):
        ms.oneStep(mh.FORWARD, step_type)
    elapsed_time = time.time() - start_time
    return elapsed_time

def all_types_speed_test():
    print("Single Steps: %.2f" % find_top_speed(mh.SINGLE))
    print("Double Steps: %.2f" % find_top_speed(mh.DOUBLE))
    print("Interleave Steps: %.2f" % find_top_speed(mh.INTERLEAVE))
    print("Micro Steps: %.2f" % find_top_speed(mh.MICROSTEP))


def all_types_full_rot_test(ipa_step,rpm):
    full_rot(ipa_step,rpm,mh.SINGLE)
    full_rot(ipa_step,rpm,mh.DOUBLE)
    full_rot(ipa_step,rpm,mh.INTERLEAVE)
    full_rot(ipa_step,rpm,mh.MICROSTEP)

all_types_full_rot_test(40,10)
