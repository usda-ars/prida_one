#! /usr/bin/perl
#
# taxon_processing.pl
#
# 2015-11-17 -- created
# 2015-11-17 -- last updated
#
# This script matches the line number (first column) from the file
# taxonomy_result_species.txt to the taxonid in the file taxonomy_result_id.txt
#
# taxonomy_result_species.txt was created based on processing the file,
# taxonomy_result_summary.txt, an output from:
# http://www.ncbi.nlm.nih.gov/taxonomy/?term=txid3398[Subtree]
# filtered for entries that are species (i.e., not genus or subspecies)
# Its contents are in the following format:
#   <line_number>,<species name>\n
#
# taxonomy_result_id.txt was downloaded from:
# http://www.ncbi.nlm.nih.gov/taxonomy/?term=txid3398[Subtree]
# Its contents are in the following format:
#   <taxonid>\n
#
use strict ;
use warnings ;

my $spfile = "taxonomy_result_species.txt";
my $idfile = "taxonomy_result_id.txt";
my $line;
my @values;
my @taxonid;
my $lnum;
my $name;
my $id;

open IFILE, "<$idfile" or die $!;
@taxonid = <IFILE>;
close IFILE;

open SFILE, "<$spfile" or die $!;
while ($line = <SFILE>) {
    chomp($line);
    @values = split( ',', $line ) or die "Failed at $line\n$!";
    $lnum = int($values[0] || 0);
    $name = $values[1];
    $id = $taxonid[$lnum-1];
    chomp($id);
    print $id . "," . $name . "\n";
}
close SFILE;
