#!/usr/bin/python
#
# refraction_indexes.py
#
# VERSION: 1.0.0-dev
#
# LAST EDIT: 2016-06-24
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# For standard office space conditions (20 deg C, 30% humidity), refractivity
# of air varies between 1.000267 and 1.000274; std ~ 2.03e-6
#
#
# @TODO: for the deviation of n, take the square root of the sum of the squared
#        standard deviations, i.e.:
#          sig_n = eta.std()
#          sig_n_err = numpy.sqrt(numpy.power(eta_err, 2).sum())
#          sig_tot = numpy.sqrt(sig_n**2 + sig_n_err**2)
###############################################################################
# REQUIRED MODULES:
###############################################################################
import os

import matplotlib.pyplot as plt
import numpy


###############################################################################
# FUNCTIONS:
###############################################################################
def eta_h2o_cauchy(lam):
    """
    Name:     eta_h2o_cauchy
    Inputs:   float, wavelength, micrometers (lam)
    Outputs:  tuple, refractivity of distilled water and associated error
    Features: Calculates the refractivity of distilled water at 20 deg C
    Ref:      Kedenburg, S., M. Vieweg, T. Gissibl, and H. Giessen (2012)
              Linear refractive index and absorption measurements of nonlinear
              optical liquids in the visible and near-infrared spectral region,
              Optical Materials Express, 2(11), 1588--1611.
              Table 3; Eq. 14
    """
    # Table 3 values:
    c0 = 1.76880   # unitless
    c1 = 0.00237   # um^2
    c2 = 0.00087   # um^4
    c3 = -0.01651  # 1/um^2

    c0_err = 0.00134
    c1_err = 0.00093
    c2_err = 0.00017
    c3_err = 0.00048

    # Eq. 14
    n2 = c0 + (c1/lam)/lam + ((((c2/lam)/lam)/lam)/lam) + c3*lam*lam
    n_cauchy = numpy.sqrt(n2)

    # Partial derivatives:
    dndc0 = 1.0/(2.0*n_cauchy)
    dndc1 = dndc0/(lam*lam)
    dndc2 = dndc1/(lam*lam)
    dndc3 = dndc0*lam*lam

    # Uncertainty:
    n2_err = dndc0*c0_err**2 + dndc1*c1_err**2
    n2_err += dndc2*c2_err**2 + dndc3*c3_err**2
    n_err = numpy.sqrt(n2_err)

    return (n_cauchy, n_err)


def eta_h2o_sellmeier(lam):
    """
    Name:     eta_h2o_sellmeier
    Inputs:   wavelength, micrometers (lam)
    Outputs:  tuple, refractivity of distilled water and associated error
    Features: Calculates the refractivity of distilled water at 20 deg C
    Ref:      Kedenburg, S., M. Vieweg, T. Gissibl, and H. Giessen (2012)
              Linear refractive index and absorption measurements of nonlinear
              optical liquids in the visible and near-infrared spectral region,
              Optical Materials Express, 2(11), 1588--1611.
              Table 3; Eq. 12
    """
    # Table 3 values
    a1 = 0.75831  # unitless
    a2 = 0.08495  # unitless
    b1 = 0.01007  # 1/um^2
    b2 = 8.91377  # 1/um^2

    a1_err = 0.00082
    a2_err = 0.01912
    b1_err = 0.00027
    b2_err = 1.35076

    # Eq. 12
    n2 = 1.0 + a1*lam*lam/(lam*lam - b1) + a2*lam*lam/(lam*lam - b2)
    n_sell = numpy.sqrt(n2)

    # Partial derivatives
    dnda1 = lam*lam/(2.0*n_sell*(lam*lam - b1))
    dnda2 = lam*lam/(2.0*n_sell*(lam*lam - b2))
    dndb1 = a1*lam*lam/(2.0*n_sell*(lam*lam - b1)**2)
    dndb2 = a2*lam*lam/(2.0*n_sell*(lam*lam - b2)**2)

    # Uncertainty
    n2_err = dnda1*a1_err**2 + dnda2*a2_err**2
    n2_err += dndb1*b1_err**2 + dndb2*b2_err**2
    n_err = numpy.sqrt(n2_err)

    return (n_sell, n_err)


def eta_pmma(lam):
    """
    Name:     eta_pmma
    Inputs:   float, wavelength, micrometers (lam)
    Outputs:  float, refractivity index of PMMA
    Features: Calculates the refractivity index of poly(methyl methacrylate)
              PMMA at standard room conditions (i.e., 20.1 deg C, 53 per cent
              humidity)
    Ref:      Beadie, G., M. Brindza, R. A. Flynn, A. Rosenberg, and J. S.
              Shirk (2015) Refractive index measurements of poly(methyl
              methacrylate) (PMMA) from 0.4--1.6 um, Applied Optics, 54(31),
              F139--F143.
              Table 1; Eq. 1
    """
    # Table 1 values
    a0 = 2.1778
    a1 = 6.1209e-3
    a2 = -1.5004e-3
    a3 = 2.3678e-2
    a4 = -4.2137e-3
    a5 = 7.3417e-4
    a6 = -4.5042e-5

    # Eq. 1
    n2 = a0 + a1*lam*lam
    n2 += a2*lam*lam*lam*lam
    n2 += (a3/lam)/lam
    n2 += (((a4/lam)/lam)/lam)/lam
    n2 += (((((a5/lam)/lam)/lam)/lam)/lam)/lam
    n2 += (((((((a6/lam)/lam)/lam)/lam)/lam)/lam)/lam)/lam
    n = numpy.sqrt(n2)
    n_err = 1.8e-4

    return (n, n_err)


def eta_prop(patm, tc, xc, h, lam):
    """
    Name:     eta_prop
    Inputs:   - float, atmospheric pressure, Pa (patm)
              - float, atmospheric temperature, deg C (tc)
              - float, atmospheric CO2 concentration, ppm (xc)
              - float, humidity fraction, unitless (h)
              - float, wavelength, micrometers (lam)
    Outputs:  tuple, refractivity of moist air and associated error
    Features: Calculates the refractivity of moist air
    Depends:  - dry_air_density
              - eta_std_air_x
              - eta_std_vapor
              - moist_air_density
              - moist_air_molar_fraction
              - water_vapor_density
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
              Section 4; Eq. 5
    """
    naxs = eta_std_air_x(lam, xc)  # unitless
    xw = moist_air_molar_fraction(patm, tc, h)
    paxs = moist_air_density(101325.0, 15.0, xc, 0.0)
    pws = moist_air_density(1333.0, 20.0, xc, 1.0)
    pa = dry_air_density(patm, tc, xc, xw)
    pw = water_vapor_density(patm, tc, xw)
    nws = eta_std_vapor(lam)

    rhs = (pa/paxs)*(naxs - 1.0) + (pw/pws)*(nws - 1.0)
    nprop = rhs + 1
    n_err = 5e-8

    return (nprop, n_err)


def eta_std_vapor(lam):
    """
    Name:     eta_std_vapor
    Inputs:   float, wavelength, micrometers (lam)
    Outputs:  float, refractivity of water vapor, unitless
    Features: Calculates the refractivity of water vapor at standard conditions
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
              Section 2; Eq. 3
    """
    cf = 1.022
    w0 = 295.235  # 1/um^2
    w1 = 2.6422   # 1/um^2
    w2 = -0.03238  # 1/um^4
    w3 = 0.004028  # 1/um^6
    sigma = 1./lam
    rhs = (
        w0 + w1*sigma*sigma +
        w2*sigma*sigma*sigma*sigma +
        w3*sigma*sigma*sigma*sigma*sigma*sigma)
    rhs *= cf
    nws = rhs*1e-8
    nws += 1.0
    return nws


def eta_std_air(lam):
    """
    Name:     eta_std_air
    Inputs:   float, wavelength, micrometers (lam)
    Outputs:  float, index of refraction (nas)
    Features: Calculates the index of refraction for standard air (15 deg C;
              101.325 kPa; 0 per cent humidity; 450 ppm CO2) at a given
              wave number (i.e., the reciprocal of the wavelength)
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
              Section 2; Eq. 1
                  For standard air:
                      0.390 mm: 1.0002833
                      0.700 mm: 1.0002758
    """
    # Check input value validity:
    if lam < 0.360 or lam > 2.500:
        raise IndexError(
            "Only valid for wavelengths between 360 nm and 2500 nm!")

    # Define constants:
    k0 = 238.0185  # um^-2
    k1 = 5792105   # um^-2
    k2 = 57.362    # um^-2
    k3 = 167917    # um^-2

    # Define wave number, sigma (the reciprocal of wavelength):
    sigma = 1./lam
    sig2 = sigma*sigma

    # Calculate the right-hand side of Eq. (1):
    rhs = k1/(k0 - sig2)
    rhs += k3/(k2 - sig2)
    nas = 1.0e-8*rhs + 1.0
    return nas


def eta_std_air_x(lam, xc):
    """
    Name:     eta_std_air_x
    Inputs:   - wavelength, um (lam)
              - atm. CO2, ppm (xc)
    Outputs:  refractivity at a given wavelength and CO2 concentration
    Features: Calculates the refractivity of standard air at a given CO2
              concentration
    Depends:  - eta_std_air
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
              Section 2; Eq. 2
    """
    nas = eta_std_air(lam)
    naxs = (nas - 1.)*(1. + 0.534e-6*(xc - 450))
    naxs += 1.0
    return naxs


def dry_air_density(patm, tc, xc, xw):
    """
    Name:     dry_air_density
    Inputs:   - float, atmospheric pressure, Pa (patm)
              - float, air temperature, deg C (tc)
              - float, atmospheric CO2 concentration, ppm (xc)
              - float, moist air molar fraction, unitless (xw)
    Outputs:  float, dry air density, kg/m^3
    Features: Calculates the density of the dry air component of moist air
    Depends:  - dry_air_molar_mass
              - water_vapor_compressibility
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
              Appendix B; list item 8
    """
    kr = 8.314510     # universal gas constant, J/mol/K
    tk = tc + 273.15  # air temperature, K
    ma = dry_air_molar_mass(xc)  # kg/mol
    z = water_vapor_compressibility(patm, tc, xw)  # unitless
    rho_a = (patm*ma)*(1 - xw)/(z*kr*tk)  # kg/m^3
    return rho_a


def moist_air_density(patm, tc, xc, xw):
    """
    Name:     moist_air_density
    Inputs:   - float, atmospheric pressure, Pa (patm)
              - float, air temperature, deg C (tair)
              - float, moist air molar fraction, unitless (xw)
    Outputs:  float, moist air density, kg/m^3
    Features: Calculates the density of moist air
    Depends:  - dry_air_molar_mass
              - water_vapor_compressibility
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
              Section 3; Eq. 4
    """
    kr = 8.314510     # universal gas constant, J/mol/K
    kmw = 0.018015    # molar mass of water vapor, kg/mol
    tk = tc + 273.15  # air temperature, K
    ma = dry_air_molar_mass(xc)  # kg/mol
    z = water_vapor_compressibility(patm, tc, xw)    # unitless
    rho = (patm*ma)/(z*kr*tk)*(1 - xw*(1 - kmw/ma))  # kg/m^3
    return rho


def water_vapor_density(patm, tc, xw):
    """
    Name:     water_vapor_density
    Inputs:   - float, atmospheric pressure, Pa (patm)
              - float, air temperature, deg C (tair)
              - float, moist air molar fraction, unitless (xw)
    Outputs:  float, water vapor density, kg/m^3
    Features: Calculates the density of the water vapor component in air
    Depends:  - dry_air_molar_mass
              - water_vapor_compressibility
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
    """
    kr = 8.314510     # universal gas constant, J/mol/K
    kmw = 0.018015    # molar mass of water vapor, kg/mol
    tk = tc + 273.15  # air temperature, K
    z = water_vapor_compressibility(patm, tc, xw)  # unitless
    rho_w = (patm*kmw*xw)/(z*kr*tk)  # kg/m^3
    return rho_w


def enhancement_factor(patm, tc):
    """
    Name:     enhancement_factor
    Inputs:   - float, atmospheric pressure, Pa (patm)
              - float, air temperature, deg C (tc)
    Outputs:  float, enhancement factor, unitless (f)
    Features: Calculates the enhancement factor of water vapor in air
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
    """
    ka = 1.00062
    kb = 3.14e-8   # 1/Pa
    kg = 5.6e-7    # 1/deg C^2
    f = ka + kb*patm + kg*tc*tc
    return f


def sat_vap_press_water_vapor(tc):
    """
    Name:     sat_vap_press_water_vapor
    Inputs:   float, air temperature, deg C (tc)
    Outputs:  float, saturation vapor pressure for water vapor, Pa (svp)
    Features: Calculates the saturation vapor pressure of water vapor in air
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
    """
    ka = 1.2378847e-5   # 1/K^2
    kb = -1.9121316e-2  # 1/K
    kc = 33.93711047
    kd = -6.3431645e3   # K
    tk = tc + 273.15
    svp = numpy.exp(ka*tk*tk + kb*tk + kc + kd/tk)   # Pa
    return svp


def moist_air_molar_fraction(patm, tc, h):
    """
    Name:     moist_air_molar_fraction
    Inputs:   - float, atmospheric pressure, Pa (patm)
              - float, air temperature, deg C (tc)
              - float, humidity fraction, unitless (h)
    Outputs:  moist air molar fraction, unitless (xw)
    Features: Calculates the molar fraction of water vapor in moist air
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
    """
    f = enhancement_factor(patm, tc)      # unitless
    svp = sat_vap_press_water_vapor(tc)   # Pa
    xw = f*h*svp/patm                     # unitless
    return xw


def water_vapor_compressibility(patm, tc, xw):
    """
    Name:     water_vapor_compressibility
    Inputs:   - float, atmospheric pressure, Pa (patm)
              - float, air temperature, deg C (tc)
              - float, moist air molar fraction, unitless (xw)
    Outputs:  float, water vapor compressibility, unitless (z)
    Feature:  Calculates the compressibility of moist air
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
              Appendix A; Eq. 12.
    """
    a0 = 1.58123e-6  # K/Pa
    a1 = -2.9331e-8  # 1/Pa
    a2 = 1.1043e-10  # 1/K/Pa
    b0 = 5.707e-6    # K/Pa
    b1 = -2.051e-8   # 1/Pa
    c0 = 1.9898e-4   # K/Pa
    c1 = -2.376e-6   # 1/Pa
    kd = 1.83e-11    # K^2/Pa^2
    ke = -0.765e-8   # K^2/Pa^2
    tk = tc + 273.15
    z = 1
    z -= (patm/tk)*(
        a0 + a1*tc + a2*tc*tc + (b0 + b1*tc)*xw + (c0 + c1*tc)*xw*xw)
    z += (patm/tk)*(patm/tk)*(kd + ke*xw*xw)
    return z


def dry_air_molar_mass(xc):
    """
    Name:     dry_air_molar_mass
    Inputs:   float, ppm of CO2 (xc)
    Outputs:  float, dry air molar mass, kg/mol (ma)
    Features: Calculates the dry air molar mass for a given CO2 concentration
    Ref:      Ciddor, P. E. (1996) Refractive index of air: new equations for
              the visible and near infrared, Applied Optics, 35(9), 1566--1573
    """
    ma = 1e-3*(28.9635 + 12.011e-6*(xc - 400))
    return ma


def elv2pres(z):
    """
    Name:     EVAP.elv2pres
    Input:    float, elevation above sea level (z), m
    Output:   float, atmospheric pressure, Pa
    Features: Calculates atm. pressure for a given elevation
    Ref:      Allen et al. (1998)
    """
    kPo = 101325     # standard atmosphere, Pa (Allen, 1973)
    kTo = 288.15     # base temperature, K (Berberan-Santos et al., 1997)
    kL = 0.0065      # temperature lapse rate, K/m (Allen, 1973)
    kMa = 0.028963   # molecular weight of dry air, kg/mol (Tsilingiris, 2008)
    kG = 9.80665     # gravitational acceleration, m/s^2 (Allen, 1973)
    kR = 8.31447     # universal gas constant, J/mol/K (Moldover et al., 1988)

    p = kPo*(1.0 - kL*z/kTo)**(kG*kMa/(kR*kL))
    return p


###############################################################################
# MAIN:
###############################################################################
if __name__ == '__main__':
    # To plot flag:
    to_plot = False

    # Wavelengths of primary visible colors (assumed values)
    vis_lam = numpy.arange(0.4, 0.7003, 0.0003)
    print("Indexes of refraction for visible wavelengths (%0.3f--%0.3f)" % (
        vis_lam.min(), vis_lam.max()))

    # Validation of Table 1 in Ciddor (1996)
    if False:
        # Values from Table 1, Ciddor (1996)
        co2_ppm = 450.0  # ppm
        atm_p = 100.0e3  # Pa
        atm_t = 30.0     # deg C
        atm_h = 0.0      # unitless (0--1)
        my_lam = 0.633   # micrometers NOTE: visible light 0.390 to 0.700

        # NOTE: all values in Table 1 of Ciddor (1996) are reproducible.
        my_n, _ = eta_prop(atm_p, atm_t, co2_ppm, atm_h, my_lam)
        print("Phase refractivity is %f" % (1e8*(my_n - 1)))

    # Refractivity of air
    if True:
        # Local values
        co2_ppm = 405.0  # ppm
        elv = 123.0      # m
        atm_p = elv2pres(elv)  # Pa
        atm_t = 20.1     # room temperature, deg C
        atm_h = 0.53     # 53% relative; unitless (0--1)
        print("Atm. pressure: %0.3f" % (1e-3*atm_p))

        # Visible light range:
        eta_air = numpy.array([])
        eta_air_err = numpy.array([])

        for lam in vis_lam:
            n, n_err = eta_prop(atm_p, atm_t, co2_ppm, atm_h, lam)
            eta_air = numpy.append(eta_air, [n, ])
            eta_air_err = numpy.append(eta_air_err, [n_err, ])

        if len(eta_air) == len(eta_air_err):
            n_ave = eta_air.mean()
            n_ave_err = numpy.sqrt(
                numpy.power(eta_air_err, 2).sum()/len(eta_air_err))
            n_uncer = numpy.sqrt(eta_air.std()**2 + n_ave_err**2)
        else:
            raise ValueError("Arrays are of different size!")

        print("Ciddor formula (air):")
        print("  mean refractivity: %f +/- %e" % (n_ave, n_ave_err))
        print("  standard deviation: %e" % (eta_air.std()))
        print("  uncertainty: %e" % (n_uncer))

        if to_plot:
            lbounda = eta_air - eta_air_err
            ubounda = eta_air + eta_air_err
            ymin = lbounda.min()
            ymax = ubounda.max()

            fig = plt.figure()

            ax1 = fig.add_subplot(111)
            plt.setp(ax1.get_xticklabels(), rotation=0)
            plt.setp(ax1.get_yticklabels(), rotation=0)
            ax1.plot(vis_lam,
                     eta_air,
                     'k-',
                     linewidth=2.0,
                     label='Air (Ciddor, 1996)')
            ax1.fill_between(vis_lam,
                             lbounda,
                             ubounda,
                             facecolor='black',
                             alpha=0.25)
            ax1.set_ylabel('Index of Refraction, unitless')
            ax1.set_xlabel('Wavelength, $\mu$m')
            ax1.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=1, mode="expand", borderaxespad=0.)
            ax1.grid(True)
            plt.ylim([ymin, ymax])
            plt.xlim([0.4, 0.7])

            plt.show()

    # Refractivity of water
    if True:
        eta_ws = numpy.array([])
        eta_ws_err = numpy.array([])

        eta_wc = numpy.array([])
        eta_wc_err = numpy.array([])

        for lam in vis_lam:
            ns, ns_err = eta_h2o_sellmeier(lam)
            nc, nc_err = eta_h2o_cauchy(lam)
            eta_ws = numpy.append(eta_ws, [ns, ])
            eta_ws_err = numpy.append(eta_ws_err, [ns_err, ])
            eta_wc = numpy.append(eta_wc, [nc, ])
            eta_wc_err = numpy.append(eta_wc_err, [nc_err, ])

        if len(eta_ws) == len(eta_ws_err):
            ns_ave = eta_ws.mean()
            ns_ave_err = numpy.sqrt(
                numpy.power(eta_ws_err, 2).sum()/len(eta_ws_err))
            ns_uncer = numpy.sqrt(eta_ws.std()**2 + ns_ave_err**2)
        else:
            raise ValueError("Arrays are of different size!")

        if len(eta_wc) == len(eta_wc_err):
            nc_ave = eta_wc.mean()
            nc_ave_err = numpy.sqrt(
                numpy.power(eta_wc_err, 2).sum()/len(eta_wc_err))
            nc_uncer = numpy.sqrt(eta_wc.std()**2 + nc_ave_err**2)
        else:
            raise ValueError("Arrays are of different size!")

        print("Sellmeier formula (water):")
        print("  mean refractivity: %f +/- %e" % (ns_ave, ns_ave_err))
        print("  standard deviation: %f" % (eta_ws.std()))
        print("  uncertainty: %f" % (ns_uncer))

        print("Cauchy formula (water):")
        print("  mean refractivity: %f +/- %e" % (nc_ave, nc_ave_err))
        print("  standard deviation: %f" % (eta_wc.std()))
        print("  uncertainty: %f" % (nc_uncer))

        if to_plot:
            lboundc = eta_wc - eta_wc_err
            lbounds = eta_ws - eta_ws_err
            ymin = numpy.concatenate([lboundc, lbounds]).min()
            uboundc = eta_wc + eta_wc_err
            ubounds = eta_ws + eta_ws_err
            ymax = numpy.concatenate([uboundc, ubounds]).max()

            fig = plt.figure()

            ax1 = fig.add_subplot(111)
            plt.setp(ax1.get_xticklabels(), rotation=0)
            plt.setp(ax1.get_yticklabels(), rotation=0)
            ax1.plot(vis_lam,
                     eta_ws,
                     'k-',
                     linewidth=2.0,
                     label='Sellmeier formula')
            ax1.plot(vis_lam,
                     eta_wc,
                     'r--',
                     linewidth=2.0,
                     label='Cauchy formula')
            ax1.fill_between(vis_lam,
                             lbounds,
                             ubounds,
                             facecolor='black',
                             alpha=0.25)
            ax1.fill_between(vis_lam,
                             lboundc,
                             uboundc,
                             facecolor='red',
                             alpha=0.25)
            ax1.set_ylabel('Index of Refraction, unitless')
            ax1.set_xlabel('Wavelength, $\mu$m')
            ax1.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)
            ax1.grid(True)
            plt.ylim([ymin, ymax])
            plt.xlim([0.4, 0.7])

            plt.show()

    # Refractivity of acrylic
    if True:
        eta_a = numpy.array([])
        eta_a_err = numpy.array([])

        for lam in vis_lam:
            na, na_err = eta_pmma(lam)
            eta_a = numpy.append(eta_a, [na, ])
            eta_a_err = numpy.append(eta_a_err, [na_err, ])

        if len(eta_a) == len(eta_a_err):
            na_ave = eta_a.mean()
            na_ave_err = numpy.sqrt(
                numpy.power(eta_a_err, 2).sum()/len(eta_a_err))
            na_uncer = numpy.sqrt(eta_a.std()**2 + na_ave_err**2)
        else:
            raise ValueError("Arrays are of different size!")

        print("Beadie formula (acrylic):")
        print("  mean refractivity: %f +/- %e" % (na_ave, na_ave_err))
        print("  standard deviation: %f" % (eta_a.std()))
        print("  uncertainty: %f" % (na_uncer))

        if to_plot:
            lboundg = eta_a - eta_a_err
            uboundg = eta_a + eta_a_err
            ymin = lboundg.min()
            ymax = uboundg.max()

            fig = plt.figure()

            ax1 = fig.add_subplot(111)
            plt.setp(ax1.get_xticklabels(), rotation=0)
            plt.setp(ax1.get_yticklabels(), rotation=0)
            ax1.plot(vis_lam,
                     eta_a,
                     'k-',
                     linewidth=2.0,
                     label='PMMA (Beadie et al., 2015)')
            ax1.fill_between(vis_lam,
                             lboundg,
                             uboundg,
                             facecolor='black',
                             alpha=0.25)
            ax1.set_ylabel('Index of Refraction, unitless')
            ax1.set_xlabel('Wavelength, $\mu$m')
            ax1.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)
            ax1.grid(True)
            plt.ylim([ymin, ymax])
            plt.xlim([0.4, 0.7])

            plt.show()

    if True:
        # Define the lower and upper boundaries for each dataset:
        lbounda = eta_air - eta_air_err
        ubounda = eta_air + eta_air_err

        lboundc = eta_wc - eta_wc_err
        uboundc = eta_wc + eta_wc_err

        lbounds = eta_ws - eta_ws_err
        ubounds = eta_ws + eta_ws_err

        lboundg = eta_a - eta_a_err
        uboundg = eta_a + eta_a_err

        # Define the textbox style:
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

        # Begin figure:
        fig = plt.figure(figsize=(12, 12))

        # [1]
        ax1 = fig.add_subplot(311)
        plt.setp(ax1.get_xticklabels(), rotation=0, fontsize=16)
        plt.setp(ax1.get_yticklabels(), rotation=0, fontsize=16)
        ax1.plot(vis_lam,
                 eta_air,
                 'k-',
                 linewidth=1.5,
                 label='Air (Ciddor, 1996)')
        ax1.fill_between(vis_lam,
                         lbounda,
                         ubounda,
                         facecolor='lightgray',
                         edgecolor='lightgray',
                         alpha=0.25)
        ax1.set_ylabel('$n_a$, unitless', fontsize=18)
        ax1.xaxis.set_ticklabels([])
        ax1.grid(True)
        plt.xlim([0.4, 0.7])
        ax1.text(1.0,
                 1.1,
                 "a) Air (Ciddor, 1996)",
                 transform=ax1.transAxes,
                 verticalalignment='top',
                 horizontalalignment='right',
                 fontsize=18)

        # [2]
        ax2 = fig.add_subplot(312)
        plt.setp(ax2.get_xticklabels(), rotation=0, fontsize=16)
        plt.setp(ax2.get_yticklabels(), rotation=0, fontsize=16)
        ax2.plot(vis_lam,
                 eta_a,
                 'r-',
                 linewidth=1.5,
                 label='PMMA (Beadie et al., 2015)')
        ax2.fill_between(vis_lam,
                         lboundg,
                         uboundg,
                         facecolor='lightgray',
                         edgecolor="lightgray",
                         alpha=0.25)
        ax2.set_ylabel('$n_g$, unitless', fontsize=18)
        ax2.xaxis.set_ticklabels([])
        ax2.grid(True)
        plt.xlim([0.4, 0.7])
        ax2.text(1.0,
                 1.1,
                 "b) Acrylic (Beadie et al., 2015)",
                 transform=ax2.transAxes,
                 verticalalignment='top',
                 horizontalalignment='right',
                 fontsize=18)

        # [3]
        ax3 = fig.add_subplot(313)
        plt.setp(ax3.get_xticklabels(), rotation=0, fontsize=16)
        plt.setp(ax3.get_yticklabels(), rotation=0, fontsize=16)
        # ax3.plot(vis_lam,
        #         eta_ws,
        #         'c-',
        #         linewidth=2.0,
        #         label='Water (Sellmeier formula)')
        ax3.plot(vis_lam,
                 eta_wc,
                 'b-',
                 linewidth=2.0,
                 label='Water (Cauchy formula)')
        # ax3.fill_between(vis_lam,
        #                 lbounds,
        #                 ubounds,
        #                 facecolor='cyan',
        #                 edgecolor=None,
        #                 alpha=0.25)
        ax3.fill_between(vis_lam,
                         lboundc,
                         uboundc,
                         facecolor='lightgray',
                         edgecolor="lightgray",
                         alpha=0.25)
        ax3.set_ylabel('$n_w$, unitless', fontsize=18)
        ax3.set_xlabel('Wavelength, $\mu$m', fontsize=18)
        ax3.grid(True)
        plt.xlim([0.4, 0.7])
        ax3.text(1.0,
                 1.1,
                 "c) Water (Kedenburg et al., 2012)",
                 transform=ax3.transAxes,
                 verticalalignment='top',
                 horizontalalignment='right',
                 fontsize=18)

        plt.savefig(
            "%s" % (os.path.join(os.path.expanduser("~"),
                             "Desktop",
                             "figure_1a.eps")),
            format="eps")
        plt.show()
