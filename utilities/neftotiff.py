#!/usr/bin/python
#
# neftotiff.py
#
# VERSION: 0.0.0
#
# LAST EDIT: 2016-12-02
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################

# This script converts NEF raw images to TIFF format for previewing.

###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os
import re

import scipy.misc
import rawpy


###############################################################################
# FUNCTIONS:
###############################################################################
def find_raw_files_r(my_dir):
    """
    Name:     find_raw_files_r
    Inputs:   str, directory path (my_dir)
    Outputs:  list, absolute paths to UI files
    Features: Returns list of raw (NEF) files from a recursive directory search
    """
    raw_file_list = []
    for root, subdirs, files in os.walk(my_dir):
        for my_file in files:
            if re.match("^.*\.nef$", my_file):
                raw_file_list.append(os.path.join(root, my_file))
    return raw_file_list


def nef_to_tiff(nef_path, verbose=False):
    """
    Name:     nef_to_tiff
    Inputs:   - str, path to NEF file (nef_path)
              - [optional] bool, flag for logging statements (verbose)
    Outputs:  int, error identifier
              > -1 .... file already exists (skipping conversion)
              > 0 ..... file conversion successful
              > 1 ..... file conversion failed
    Features: Converts a NEF file to TIFF
    Depends:  export_to_file
    """
    nef_dir = os.path.dirname(nef_path)
    nef_basename = os.path.basename(nef_path)
    tif_basename = "%s.tiff" % (os.path.splitext(nef_basename)[0])
    tif_path = os.path.join(nef_dir, tif_basename)
    if os.path.isfile(tif_path):
        # File already exists, skip
        return -1
    else:
        try:
            # Use libraw image reader:
            raw = rawpy.imread(nef_path)
            img = raw.postprocess()
        except:
            if verbose:
                logging.exception("Failed to read NEF file")
            return 1
        else:
            try:
                scipy.misc.imsave(tif_path, img)
            except:
                if verbose:
                    logging.exception("Failed to save image")
                return 1
            else:
                if os.path.isfile(tif_path):
                    return 0
                else:
                    return 1


def perform_conversion(fline=49):
    """
    Name:     perform_conversion
    Inputs:   [optional] int, character fill line length (fline)
    Outputs:  None.
    Features: Converts NEF to TIFF
    """
    ck = "Checking for raw images"
    nef_list = find_raw_files_r(os.path.abspath("."))
    num_found = len(nef_list)
    if num_found > 0:
        msg = "(found %d files)" % (num_found)
        print("{} {} {}".format(
            ck, "."*(fline - len(ck) - len(msg) - 1), msg))

        for i in range(num_found):
            nef_file = nef_list[i]
            msg = "> converting (%d/%d) " % (i+1, num_found)

            ok = nef_to_tiff(nef_file, verbose=False)
            if ok == -1:
                msg0 = " EXISTS"
                print("{}{}{}".format(
                    msg, "."*(fline - len(msg) - len(msg0)), msg0))
            elif ok == 1:
                msg0 = " FAILED"
                print("{}{}{}".format(
                    msg, "."*(fline - len(msg) - len(msg0)), msg0))
            elif ok == 0:
                msg0 = " OK"
                print("{}{}{}".format(
                    msg, "."*(fline - len(msg) - len(msg0)), msg0))
            else:
                msg0 = " ERROR"
                print("{}{}{}".format(
                    msg, "."*(fline - len(msg) - len(msg0)), msg0))
    else:
        msg = "none found!"
        print("{} {} {}".format(
            ck, "."*(fline - len(ck) - len(msg) - 1), msg))


###############################################################################
# MAIN:
###############################################################################
if __name__ == '__main__':
    line_len = 57
    greeting = " NEF to TIF "
    ending = " done "
    gdots = int(0.5*(line_len - len(greeting)))
    edots = int(0.5*(line_len - len(ending)))
    print("{}{}{}".format("-"*gdots, greeting, "-"*gdots))

    perform_conversion(55)
    print("{}{}{}".format('-'*edots, ending, '-'*edots))
