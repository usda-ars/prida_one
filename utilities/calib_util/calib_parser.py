#!/usr/bin/python
#
# calib_parser.py
#
# VERSION: 1.0.0-dev
#
# LAST EDIT: 2016-05-16
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# NOTE:
#     THIS UTILITY IS NOT PYTHON 2 COMPATIBLE.
#
#
# Changelog:
# $log_start_tag$
#
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import datetime
import logging
import os


###############################################################################
# CLASS - CalImg
###############################################################################
class CalImg():
    """
    Object representing the sum total of information exported from a
    particular calibration image
    """

    # /////////////////////////////////////////////////////////////////////////
    # Attribute Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __init__(self):
        """
        Name:    CalImg.__init__
        Feature: Instantiate a calibration image and initialize its attributes
        Inputs:  None
        Outputs: None
        """
        self.__element_dict = dict()
        self.__iter_idx = None
        self.__keys = set()
        self._background = None
        self._column_dict = dict()
        self._element_count = None
        self._file_name = None
        self._hfprime_err = None
        self._hfprime_px = None
        self._img_count = None
        self._img_height = None
        self._img_width = None
        self._kernel_width = None
        self._otsu = None
        self._roi = ROI()
        self._roi._bottom = None
        self._roi._left = None
        self._roi._right = None
        self._roi._top = None
        self._roll_deg = None
        self._roll_err = None
        self._scale = None
        self._sess_date = None
        self._trans_err = None
        self._trans_px = None
        self.attr_dict = {
            'Name': 'file_name',
            'Date': 'sess_date',
            'ImgCount': 'img_count',
            'ImgHeight': 'img_height',
            'ImgWidth': 'img_width',
            'Scale': 'scale',
            'Background': 'background',
            'ROI_top': 'roi.top',
            'ROI_bottom': 'roi.bottom',
            'ROI_left': 'roi.left',
            'ROI_right': 'roi.right',
            'Otsu': 'otsu',
            'KernelWidth': 'kernel_width',
            'ElementCount': 'element_count',
            'Transl_px': 'trans_px',
            'TranslErr_px': 'trans_err',
            'Roll_deg': 'roll_deg',
            'RollErr_deg': 'roll_err',
            'HFprime_px': 'hfprime_px',
            'HFprimeErr_px': 'hfprime_err',
            }

    # /////////////////////////////////////////////////////////////////////////
    # Properties:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def background(self):
        """
        Name:    CalImg.background
        Feature: background attribute getter
        Inputs:  None
        Outputs: str, background color (self._background)
        """
        return self._background

    @background.setter
    def background(self, val):  # lint:ok
        """
        Name:    CalImg.background
        Feature: background attribute setter
        Inputs:  str, desired background color value (val)
        Outputs: None
        """
        if isinstance(val, str):
            if val in {'Black', 'White'}:
                self._background = val
            else:
                raise ValueError("background color limited to"
                                 " 'Black' or 'White'")
        else:
            raise TypeError('background color must be a string')

    @property
    def column_dict(self):
        """
        Name:    CalImg.column_dict
        Feature: column dictionary attribute getter
        Inputs:  None
        Outputs: Dict, dictionary of column information (self._colummn_dict)
        """
        return self._column_dict

    @column_dict.setter
    def column_dict(self, val):  # lint:ok
        """
        Name:    CalImg.column_dict
        Feature: column dictionary attribute setter
        Inputs:  Dict, column info dictionary (val)
        Outputs: None
        """
        if isinstance(val, dict):
            self._column_dict = val
        else:
            raise TypeError('column dict must be a dictionary object')

    @property
    def element_count(self):
        """
        Name:    CalImg.element_count
        Feature: element count attribute getter
        Inputs:  None
        Outputs: int, number of elements (self._element_count)
        """
        return self._element_count

    @element_count.setter
    def element_count(self, val):  # lint:ok
        """
        Name:    CalImg.element_count
        Feature: element count attribute setter
        Inputs:  int, number of elements (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val >= 0:
                self._element_count = val
            else:
                raise ValueError('element count must be positive')
        else:
            raise TypeError('element count must be a integer')

    @property
    def file_name(self):
        """
        Name:    CalImg.file_name
        Feature: file name getter
        Inputs:  None
        Outputs: str, file name (self._file_name)
        """
        return self._file_name

    @file_name.setter
    def file_name(self, val):  # lint:ok
        """
        Name:    CalImg.file_name
        Feature: file name setter
        Inputs:  str, file name (val)
        Outputs: None
        """
        if isinstance(val, str):
            self._file_name = val
        else:
            raise TypeError('file name must be a string')

    @property
    def hfprime_err(self):
        """
        Name:    CalImg.hfprime_err
        Feature: hfprime error value getter
        Inputs:  None
        Outputs: float, error in hfprime (self._hfprime_err)
        """
        return self._hfprime_err

    @hfprime_err.setter
    def hfprime_err(self, val):  # lint:ok
        """
        Name:    CalImg.hfprime_err
        Feature: hfprime error value setter
        Inputs:  float, error in hfprime (val)
        Outputs: None
        """
        if isinstance(val, (int, float)):
            self._hfprime_err = val
        else:
            raise TypeError('hfprime error must be a integer or float')

    @property
    def hfprime_px(self):
        """
        Name:    CalImg.hfprime_px
        Feature: hfprime pixel value getter
        Inputs:  None
        Outputs: float, hfprime pixel height (self._hfprime_px)
        """
        return self._hfprime_px

    @hfprime_px.setter
    def hfprime_px(self, val):  # lint:ok
        """
        Name:    CalImg.hfprime_px
        Feature: hfprime pixel value setter
        Inputs:  float, hfprime pixel height (val)
        Outputs; None
        """
        if isinstance(val, (int, float)):
            self._hfprime_px = val
        else:
            raise TypeError('hfprime value must be a integer')

    @property
    def img_count(self):
        """
        Name:    CalImg.img_count
        Feature: number of images getter
        Inputs:  None
        Outputs: int, image count (self._img_count)
        """
        return self._img_count

    @img_count.setter
    def img_count(self, val):  # lint:ok
        """
        Name:    CalImg.img_count
        Feature: number of images setter
        Inputs:  int, image count (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val >= 0:
                self._img_count = val
            else:
                raise ValueError('image count cannot be negative')
        else:
            raise TypeError('image count must be an integer value')

    @property
    def img_height(self):
        """
        Name:    CalImg.img_height
        Feature: image pixel height getter
        Inputs:  None
        Outputs: int, pixel height (self._img_height)
        """
        return self._img_height

    @img_height.setter
    def img_height(self, val):  # lint:ok
        """
        Name:    CalImg.img_height
        Feature: image pixel height setter
        Inputs:  int, pixel height (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val >= 0:
                self._img_height = val
            else:
                raise ValueError('image height cannot be negative')
        else:
            raise TypeError('image height must be an integer value')

    @property
    def img_width(self):
        """
        Name:    CalImg.img_width
        Feature: image pixel width getter
        Inputs:  None
        Outputs: int, pixel width (self._img_width)
        """
        return self._img_width

    @img_width.setter
    def img_width(self, val):  # lint:ok
        """
        Name:    CalImg.img_width
        Feature: image pixel width setter
        Inputs:  int, pixel width (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val >= 0:
                self._img_width = val
            else:
                raise ValueError('image width cannot be negative')
        else:
            raise TypeError('image width must be an integer value')

    @property
    def kernel_width(self):
        """
        Name:    CalImg.kernel_width
        Feature: median filter kernel width getter
        Inputs:  None
        Outputs: int, kernel width (self._kernel_width)
        """
        return self._kernel_width

    @kernel_width.setter
    def kernel_width(self, val):  # lint:ok
        """
        Name:    CalImg.kernel_width
        Feature: median filter kernel width setter
        Inputs:  int, kernel width (val)
        Outputs: None
        """
        if isinstance(val, int):
            if ((val > 0) and ((val % 2) != 0)):
                self._kernel_width = val
            else:
                raise ValueError('kernel width must be a positive odd value')
        else:
            raise TypeError('kernel width must be a integer')

    @property
    def otsu(self):
        """
        Name:    CalImg.otsu
        Feature: otsu threshold getter
        Inputs:  None
        Outputs: int, otsu threshold (self._otsu)
        """
        return self._otsu

    @otsu.setter
    def otsu(self, val):  # lint:ok
        """
        Name:    CalImg.otsu
        Feature: otsu threshold setter
        Inputs:  int, otsu threshold (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val in range(0, 256):
                self._otsu = val
            else:
                raise ValueError('otsu threshold must be a value between'
                                 ' 0 and 255 inclusive')
        else:
            raise TypeError('otsu threshold must be an integer')

    @property
    def roi(self):
        """
        Name:    CalImg.roi
        Feature: region of interest getter
        Inputs:  None
        Outputs: ROI, a region of interest object (self._roi)
        """
        return self._roi

    @roi.setter
    def roi(self, val):  # lint:ok
        """
        Name:    CalImg.roi
        Feature: region of interest setter
        Inputs:  ROI, a region of interest object (self._roi)
        Outputs: None
        """
        if isinstance(val, ROI):
            self._roi = val
        else:
            raise TypeError('region of interest must be an ROI object')

    @property
    def roll_deg(self):
        """
        Name:    CalImg.roll_deg
        Feature: roll value getter
        Inputs:  None
        Outputs: float, roll value in degrees (self._roll_deg)
        """
        return self._roll_deg

    @roll_deg.setter
    def roll_deg(self, val):  # lint:ok
        """
        Name:    CalImg.roll_deg
        Feature: roll value setter
        Inputs:  float, roll value in degrees (val)
        Outputs: None
        """
        if isinstance(val, (int, float)):
            self._roll_deg = val
        else:
            raise TypeError('roll value must be a integer or float')

    @property
    def roll_err(self):
        """
        Name:    CalImg.roll_err
        Feature: roll error getter
        Inputs:  None
        Outputs: float, roll error (self._roll_err)
        """
        return self._roll_err

    @roll_err.setter
    def roll_err(self, val):  # lint:ok
        """
        Name:    CalImg.roll_err
        Feature: roll error setter
        Inputs:  float, roll error (val)
        Outputs: None
        """
        if isinstance(val, (int, float)):
            self._roll_err = val
        else:
            raise TypeError('roll error must be a integer or float')

    @property
    def scale(self):
        """
        Name:    CalImg.scale
        Feature: scaling factor getter
        Inputs:  None
        Outputs: float, scaling factor (self._scale)
        """
        return self._scale

    @scale.setter
    def scale(self, val):  # lint:ok
        """
        Name:    CalImg.scale
        Feature: scaling factor setter
        Inputs:  float, scaling factor (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val in range(0, 101):
                self._scale = val
            else:
                raise ValueError('scaling factor must be a value between '
                                 '0 and 100 inclusive')
        else:
            raise TypeError('scaling factor must be an integer')

    @property
    def sess_date(self):
        """
        Name:    CalImg.sess_date
        Feature: imaging session date getter
        Inputs:  None
        Outputs: datetime, session date (self._sess_date)
        """
        return self._sess_date

    @sess_date.setter
    def sess_date(self, val):  # lint:ok
        """
        Name:    CalImg.sess_date
        Feature: imaging session date setter
        Inputs:  datetime, session date (val)
        Outputs: None
        """
        if isinstance(val, (datetime.datetime, datetime.date)):
            if val <= datetime.datetime.today():
                self._sess_date = val
            else:
                raise ValueError('session date cannot be in the future')
        else:
            raise TypeError('session date must be of type datetime')

    @property
    def trans_err(self):
        """
        Name:    CalImg.trans_err
        Feature: translation error value getter
        Inputs:  None
        Outputs: int, translation value error (self._trans_err)
        """
        return self._trans_err

    @trans_err.setter
    def trans_err(self, val):  # lint:ok
        """
        Name:    CalImg.trans_err
        Feature: translation error value setter
        Inputs:  int, translation value error (val)
        Outputs: None
        """
        if isinstance(val, (int, float)):
            self._trans_err = val
        else:
            raise TypeError('translation error must be a integer or float')

    @property
    def trans_px(self):
        """
        Name:    CalImg.trans_px
        Feature: pixel translation value getter
        Inputs:  None
        Outputs: int, pixel translation (self._trans_px)
        """
        return self._trans_px

    @trans_px.setter
    def trans_px(self, val):  # lint:ok
        """
        Name:    CalImg.trans_px
        Feature: pixel translation value setter
        Inputs:  int, pixel translation (val)
        Outputs: None
        """
        if isinstance(val, (int, float)):
            self._trans_px = val
        else:
            raise TypeError('translation value must be a integer')

    # /////////////////////////////////////////////////////////////////////////
    # Private Methods:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __contains__(self, val):
        """
        Name:    CalImg.__contains__
        Feature: returns True if the key of interest is a exisiting index of
                 the CalImg instance; else returns False.
        Inputs:  dict, the ring element of interest (val)
        Outputs: bool, value indicating if val is in values (my_bool)
        """
        my_bool = False
        if val in [self[i] for i in range(len(self))]:
            my_bool = True
        return my_bool

    def __delitem__(self, key):
        """
        Name:    CalImg.__delitem__
        Feature: deletes the value from a given index
        Inputs:  int, the index at which to delete its value (key)
        Outputs: None
        """
        if isinstance(key, int):
            if abs(key) < len(self.__keys):
                if key < 0:
                    my_key = len(self.__keys) + key
                else:
                    my_key = key
                self.__keys.remove(my_key)
                del self.__element_dict[my_key]
                for idx in self.__keys:
                    if idx > my_key:
                        self.__keys.remove(idx)
                        val = self.__element_dict.pop(idx)
                        self.__keys.add(idx - 1)
                        self.__element_dict[idx - 1] = val
            else:
                raise IndexError('image index out of range')
        else:
            raise TypeError('image indicies must be integers')

    def __eq__(self, value):
        """
        Name:    CalImg.__eq__
        Feature: method for determining equality of two CalImg objects
        Inputs:  any type, value to compare to (value)
        Outputs: bool, value indicating wether the objects are the same or not
        """
        return (hash(self) == hash(value))

    def __getitem__(self, key):
        """
        Name:    CalImg.__getitem__
        Feature: returns a value corresponding to a key passed as an index to
                 the CalImg instance.
        Inputs:  int, the index of the desired value (key)
        Outputs: the value stored at the given index
        """
        if isinstance(key, int):
            if abs(key) < len(self.__keys):
                if abs(key) in self.__keys:
                    if key < 0:
                        return self.__element_dict[len(self.__keys) + key]
                    else:
                        return self.__element_dict[key]
                else:
                    raise KeyError('key "{}" not in keys'.format(key))
            else:
                raise IndexError('image index out of range')
        else:
            raise TypeError('image indicies must be integers')

    def __hash__(self):
        """
        Name:    CalImg.__hash__
        Feature: generates a unique integer hashcode for representing a
              CalImg object.
        Inputs:  None
        Outputs: int, a unique hashcode
        """
        metadata = [self.column_dict[key]['value'] for key in self.column_dict]
        metadata += [e['size'] for e in self]
        metadata += [e['height'] for e in self]
        metadata += [e['width'] for e in self]
        return hash(tuple(metadata))

    def __iter__(self):
        """
        Name:    CalImg.__iter__
        Feature: initializes and returns an interable object for iterating
                 through the CalImg instance's ring elements.
        Inputs:  None
        Outputs: CalImg, an initalized CalImg iterable (self)
        """
        self.__iter_idx = -1
        return self

    def __len__(self):
        """
        Name:    CalImg.__len__
        Feature: returns the number of ring elements stored in the CalImg
                 object
        Inputs:  None
        Outputs: int, number of ring elements
        """
        return len(list(self.__keys))

    def __ne__(self, value):
        """
        Name:    CalImg.__ne__
        Feature: negation of the CalImg objects' equals method
        Inputs:  any type, value for comparision
        Outputs: bool, boolean indicating lack of equality
        """
        return not(self == value)

    def __next__(self):
        """
        Name:    CalImg.__next__
        Feature: behaviour for iterating through ring elements contained in
                 the CalImg instance.
        Inputs:  None
        Outputs: dict, dictionary of ring element data
        """
        self.__iter_idx += 1
        if self.__iter_idx < len(self.__keys):
            return self.__element_dict[self.__iter_idx]
        else:
            raise StopIteration

    def __setitem__(self, key, val):
        """
        Name:    CalImg.__setitem__
        Feature: sets a value to a specific indexable key of the CalImg
                 instance.
        Inputs:  - int, the index to set a value to (key)
                 - the desired new value (val)
        Outputs: None
        """
        if isinstance(key, int):
            if abs(key) <= len(self.__keys):
                if key < 0:
                    my_key = len(self.__keys) + key
                    self.__keys.add(my_key)
                    self.__element_dict.update({my_key: val})
                else:
                    self.__keys.add(key)
                    self.__element_dict.update({key: val})
            else:
                raise IndexError('image index out of range')
        else:
            raise TypeError('image indicies must be integers')

    # /////////////////////////////////////////////////////////////////////////
    # Behaviours:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def append(self, val):
        """
        Name:    CalImg.append
        Feature: appends a value to the end of the ring element indicies.
        Inputs:  a value to add to the CalImg (val)
        Outputs: None
        """
        new_key = len(self.__keys)
        self.__keys.add(new_key)
        self.__element_dict.update({new_key: val})

    def get_points(self):
        """
        Name:    CalImg.get_points
        Feature: returns a list of (x, y) coordinate pairs from the
                 calibration image elements
        Inputs:  None
        Outputs: list<tuple<int, int>>, list of coordinate pairs
        """
        return [(e['size'], e['height']) for e in self]

    def pop(self, idx=None):
        """
        Name:    CalImg.pop
        Feature: removes the value at a given index and returns it
        Inputs:  int, index to remove and return (idx)
        Outputs: value previously at the given index (my_val)
        """
        # check if idx is unset
        if idx is None:
            # default to last index
            idx = max(self.__keys)

        # check operation validity
        if not isinstance(idx, int):
            raise TypeError('index to pop must be of type Int')
        if abs(idx) >= len(self.__keys):
            raise IndexError('index out of bounds')

        # include reverse indexing support
        if idx < 0:
            my_idx = len(self.__keys) + idx
        else:
            my_idx = idx

        # pop value at key
        self.__keys.remove(my_idx)
        my_val = self.__element_dict.pop(my_idx)

        # adjust remaining indicies
        for key in self.__keys:
            if key > my_idx:
                self.__keys.remove(key)
                self.__keys.add(key - 1)
                val = self.__element_dict[key]
                del self.__element_dict[key]
                self.__element_dict[key - 1] = val
        return my_val

    def process_header(self, header):
        """
        Name:    CalImg.process_header
        Feature: initializes the column dictionary from a header list
        Inputs:  list<str>, list of column headers
        Outputs: None
        """
        my_column_dict = dict()
        for i in range(20):
            my_column_dict[i] = {
                'title': header[i],
                'name': self.attr_dict[header[i]],
                'value': None,
                }
        self.column_dict = my_column_dict

    def remove(self, idx):
        """
        Name:    CalImg.remove
        Feature: removes the value at the given index
        Inputs:  int, index to remove (idx)
        Outputs: None
        """
        # check operation validity
        if not isinstance(idx, int):
            raise TypeError('index to pop must be of type Int')
        if abs(idx) >= len(self.__keys):
            raise IndexError('index out of bounds')

        # include reverse indexing support
        if idx < 0:
            my_idx = len(self.__keys) + idx
        else:
            my_idx = idx

        # remove value at key
        self.__keys.remove(idx)
        del self.__element_dict[idx]

        # adjust remaining indicies
        for key in self.__keys:
            if key > my_idx:
                self.__keys.remove(key)
                self.__keys.add(key - 1)
                val = self.__element_dict[key]
                del self.__element_dict[key]
                self.__element_dict[key - 1] = val


###############################################################################
# CLASS - CalParser:
###############################################################################
class CalParser():
    """
    Object for coordinating the import of a Prida calibration data export file.

    Name:      CalParser
    History:   Version 1.0.0-dev
    """

    # /////////////////////////////////////////////////////////////////////////
    # Attribute Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __init__(self, my_file=None):
        """
        Name:    CalParser.__init__
        Feature: initializes an instance of a CalParser object
        Inputs:  None
        Outputs: None
        """
        self._cal_file = None
        if my_file is not None:
            self.cal_file = my_file
        self.imgs = []
        self.logger = logging.getLogger('CalParser')

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def cal_file(self):
        """
        Name:    self.cal_file
        Feature: calibration file property getter
        inputs:  None
        Outputs: Calibration file path
        """
        return self._cal_file

    @cal_file.setter
    def cal_file(self, val):  # lint:ok
        """
        Name:    self.cal_file
        Feature: calibration file property setter
        Inputs:  str, calibration file path (val)
        Outputs: None
        """
        if isinstance(val, str):
            if os.path.isfile(val):
                self._cal_file = val
            else:
                raise ValueError('cal file must be a path to a text file')
        else:
            raise TypeError('cal file must be a string')

    # /////////////////////////////////////////////////////////////////////////
    # Behaviour Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def get_all_points(self):
        """
        Name:    CalParser.get_all_points
        Feature: collects the (x, y) coordinates corresponding to each element
                 of each image in the target calibration file and returns them
                 as a list.
        Inputs:  None
        Outputs: list<tuple<int, int>>, list of (x, y) coordinate
                 pairs (my_points)
        """
        my_points = []
        for img in self.imgs:
            my_points += img.get_points()
        return my_points

    def parse_file(self):
        """
        Name:    CalParser.parse_file
        Feature: parses the calibration file located at self._cal_file and
                 adds all detected calibration image data to the imgs list as
                 CalImg objects.
        Inputs:  None
        Outputs: None
        """
        if self.cal_file is None:
            raise IOError('No file to parse! please set your cal file.')
        elif not os.path.isfile(self.cal_file):
            raise IOError('No file found at "{}".'.format(self.cal_file))
        self.logger.debug('parsing cal file')
        with open(self.cal_file, mode='r') as calib_file:
            header = True
            my_header = ''
            for line in calib_file:
                my_img = CalImg()
                if not header:
                    my_img.process_header(my_header)
                    my_cols = line.split(';')
                    self.logger.debug(
                        'parsing calibration image data from '
                        'file "{}"'.format(my_cols[0]))
                    my_img.file_name = my_cols[0]
                    my_img.column_dict[0]['value'] = str(my_cols[0])
                    my_img.sess_date = datetime.datetime.strptime(
                        my_cols[1], '%Y-%m-%d')
                    my_img.column_dict[1]['value'] = str(my_cols[1])
                    my_img.img_count = int(my_cols[2])
                    my_img.column_dict[2]['value'] = str(my_cols[2])
                    my_img.img_height = int(my_cols[3])
                    my_img.column_dict[3]['value'] = str(my_cols[3])
                    my_img.img_width = int(my_cols[4])
                    my_img.column_dict[4]['value'] = str(my_cols[4])
                    my_img.scale = int(my_cols[5])
                    my_img.column_dict[5]['value'] = str(my_cols[5])
                    my_img.background = str(my_cols[6])
                    my_img.column_dict[6]['value'] = str(my_cols[6])
                    my_img.roi.top = int(my_cols[7])
                    my_img.column_dict[7]['value'] = str(my_cols[7])
                    my_img.roi.bottom = int(my_cols[8])
                    my_img.column_dict[8]['value'] = str(my_cols[8])
                    my_img.roi.left = int(my_cols[9])
                    my_img.column_dict[9]['value'] = str(my_cols[9])
                    my_img.roi.right = int(my_cols[10])
                    my_img.column_dict[10]['value'] = str(my_cols[10])
                    my_img.otsu = int(my_cols[11])
                    my_img.column_dict[11]['value'] = str(my_cols[11])
                    my_img.kernel_width = int(my_cols[12])
                    my_img.column_dict[12]['value'] = str(my_cols[12])
                    my_img.element_count = int(my_cols[13])
                    my_img.column_dict[13]['value'] = str(my_cols[13])
                    my_img.trans_px = float(my_cols[14])
                    my_img.column_dict[14]['value'] = str(my_cols[14])
                    my_img.trans_err = float(my_cols[15])
                    my_img.column_dict[15]['value'] = str(my_cols[15])
                    my_img.roll_deg = float(my_cols[16])
                    my_img.column_dict[16]['value'] = str(my_cols[16])
                    my_img.roll_err = float(my_cols[17])
                    my_img.column_dict[17]['value'] = str(my_cols[17])
                    my_img.hfprime_px = float(my_cols[18])
                    my_img.column_dict[18]['value'] = str(my_cols[18])
                    my_img.hfprime_err = float(my_cols[19])
                    my_img.column_dict[19]['value'] = str(my_cols[19])
                    for i in range(20, len(my_cols)):
                        idx = int((i - 20) / 3)
                        atr = int((i - 20) % 3)
                        atrs = {0: 'size', 1: 'height', 2: 'width'}
                        if not (idx in range(len(my_img))):
                            my_img[idx] = dict()
                        my_img[idx].update({atrs[atr]: int(my_cols[i].strip())})
                    self.imgs.append(my_img)
                else:
                    self.logger.debug('processing header')
                    header = False
                    my_header = line.split(';')
        self.logger.info(
            'file at {} parsed, '.format(self.cal_file) +
            '{} elements collected'.format(my_cols[13]))

    def get_points_by_hash(self, hashval):
        """
        Name:    CalParser.get_points_by_name
        Feature: returns the list of coordinate tuples associated with a CalImg
                 with a specified hash value.
        Inputs:  int, hash value to get points from (hashval)
        Outputs: list<tuple<int, int>> list of coordinate tuples
        """
        hashes = {hash(self.imgs[i]): i for i in range(len(self.imgs))}
        return self.imgs[hashes[hashval]].get_points()

###############################################################################
# CLASS - ROI:
###############################################################################
class ROI():
    """
    Object representing a calibration image's Region of Interest.
    """

    # /////////////////////////////////////////////////////////////////////////
    # Attribute Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __init__(self):
        """
        Name:    ROI.__init__
        Feature: Instantiate a region of interest and initialize its attributes
        Inputs:  None
        Outputs: None
        """
        self._top = None
        self._bottom = None
        self._left = None
        self._right = None

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def top(self):
        """
        Name:    ROI.top
        Feature: top margin of the region of interest getter
        Inputs:  None
        Outputs: int, top margin (self.top)
        """
        return self._top

    @top.setter
    def top(self, val):  # lint:ok
        """
        Name:    ROI.top
        Feature: top margin of the region of interest setter
        Inputs:  int, top margin (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val >= 0:
                self._top = val
            else:
                raise ValueError('top margin must be a positive value')
        else:
            raise TypeError('top margin must be a integer')

    @property
    def bottom(self):
        """
        Name:    ROI.bottom
        Feature: bottom margin of the region of interest getter
        Inputs:  None
        Outputs: int, bottom margin (self._bottom)
        """
        return self._bottom

    @bottom.setter
    def bottom(self, val):  # lint:ok
        """
        Name:    ROI.bottom
        Feature: bottom margin of the region of interest setter
        Inputs:  int, bottom margin (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val >= 0:
                self._bottom = val
            else:
                raise ValueError('bottom margin must be a positive value')
        else:
            raise TypeError('bottom margin must be a integer')

    @property
    def left(self):
        """
        Name:    ROI.left
        Feature: left margin of the region of interest getter
        Inputs:  None
        Outputs: int, left margin (self._left)
        """
        return self._left

    @left.setter
    def left(self, val):  # lint:ok
        """
        Name:    ROI.left
        Feature: left margin of the region of interest setter
        Inputs:  int, left margin (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val >= 0:
                self._left = val
            else:
                raise ValueError('left margin must be a positive value')
        else:
            raise TypeError('left margin must be a integer')

    @property
    def right(self):
        """
        Name:    ROI.right
        Feature: right margin of the region of interest getter
        Inputs:  None
        Outputs: int, right margin (self._right)
        """
        return self._right

    @right.setter
    def right(self, val):  # lint:ok
        """
        Name:    ROI.right
        Feature: right margin of the region of interest setter
        Inputs:  int, right margin (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val >= 0:
                self._right = val
            else:
                raise ValueError('right margin must be a positive value')
        else:
            raise TypeError('right margin must be a integer')

###############################################################################
# MAIN:
###############################################################################
if __name__ == '__main__':
    # usage:
    my_file = ('/home/usda-ars/Drobo/RSADrobo/HDF5_Files/CalibrationImages/'
               'exports/prida-calib-export_1.4.0-dev.txt')
    my_parser = CalParser()
    my_parser.cal_file = my_file  # <-- set target file
    my_parser.parse_file()        # <-- parse target file
    tail, head = os.path.split(my_file)
    new_file = os.path.join(tail, 'example.txt')
    with open(new_file, 'w+') as my_file:
        for calimg in my_parser.imgs:
            my_file.write('\n\nFile Name: {}'.format(calimg.file_name))
            my_file.write('\nNumber of Elements: {}'.format(len(calimg)))
            i = 1
            for element in calimg:    # <-- calimg's are iterable
                #                       element data stored in dictionary form
                my_file.write(
                    '\n\tElement {} Size: {}'.format(i, element['size']))
                i += 1
            my_file.write('\nLast element details:')
            # supports indexing (and reverse indexing)
            my_file.write('\n\tElement Size: {}'.format(calimg[-1]['size']))
            my_file.write('\n\tElement Height: {}'.format(calimg[-1]['height']))
            my_file.write('\n\tElement Width: {}'.format(calimg[-1]['width']))
        my_data = my_parser.get_all_points()
        my_file.write('\n\nTotal Number of Rings: {}'.format(len(my_data)))
