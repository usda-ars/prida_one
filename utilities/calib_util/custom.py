#!/usr/bin/python
#
# custom.py
#
# VERSION: 1.0.0-dev
#
# LAST EDIT: 2016-05-24
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################

# For backend_qt5agg documentation, see:
# https://www.codatlas.com/github.com/matplotlib/matplotlib/master/lib/matplotlib/backends/backend_qt5agg.py
# https://fossies.org/dox/matplotlib-1.5.1/namespacematplotlib_1_1backends_1_1backend__agg.html

###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging

import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as Canvas
from matplotlib.figure import Figure
import numpy
from PyQt5.QtWidgets import QSizePolicy


###############################################################################
# CLASSES:
###############################################################################
class CalCanvas(Canvas):
    """
    A custom QWidget

    Name:    CalCanvas
    History: Version 0.0.1-dev
             - added regression and model fitness calculations [16.05.17]
             - added logger [16.05.17]
             - added alpha to scatter plot to show density [16.05.18]
             - added point count to plot text [16.05.18]
    Ref:     Based on the example of embedding matplotlib canvases into Qt
    http://matplotlib.org/examples/user_interfaces/embedding_in_qt4.html

    Copyright history:
            - Florent Rougon (2005); Darren Dale (2006); Jens H Nielsen (2015);
              BoxControL (2015)
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None, width=5, height=5, dpi=72):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        Canvas.__init__(self, self.fig)
        Canvas.setSizePolicy(self,
                             QSizePolicy.Expanding,
                             QSizePolicy.Expanding)

        self.setParent(parent)
        self.axes = self.fig.add_subplot(111)

        # Define properties for display box:
        self.props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

        # Initialize data arrays:
        self.x = numpy.array([])
        self.y = numpy.array([])
        self.x_max = 0.0
        self.y_max = 0.0
        self.m = 0.0
        self.txt = ""

        # Create class logger:
        self.logger = logging.getLogger(__name__)
        self.logger.info("CalCanvas initialized")

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def fitness_cod(self, fit, obs):
        """
        Name:     CalCanvas.fitness_cod
        Inputs:   - numpy.ndarray, fitted data (fit)
                  - numpy.ndarray, observed data (obs)
        Outputs:  float, coefficient of determination (rsqr)
        Features: Calculates the coefficient of determination based on the
                  ratio of regression sum of squares (SSR) to total sum of
                  squares (SST), where SSR = SST - SSE (error sum of squares)
        """
        sst = obs - obs.mean()
        sst = numpy.power(sst, 2.0)
        sst = sst.sum()

        sse = obs - fit
        sse = numpy.power(sse, 2.0)
        sse = sse.sum()

        rsqr = (sst - sse)/sst
        return rsqr

    def fitness_ioa(self, fit, obs):
        """
        Name:     CalCanvas.fitness_ioa
        Inputs:   - numpy.ndarray, fitted data (fit)
                  - numpy.ndarray, observed data (obs)
        Outputs:  float, index of agreement (ioa)
        Features: Calculates refined Willmott's index of agreement, which is
                  bounded between -1 and 1
        Ref:      Willmott et al. (2012) Int. J. Climatol. 32: 2088--2094.
                  doi: 10.1002/joc.2419
        """
        # Mean absolute error:
        mae = numpy.abs(fit - obs)
        smae = mae.sum()
        # Mean absolute deviation of observations:
        mao = numpy.abs(obs - obs.mean())
        smao = mao.sum()
        tsmo = 2.0*smao

        if smae <= tsmo:
            ioa = 1.0 - smae/tsmo
        else:
            ioa = tsmo/smae - 1.0
        return ioa

    def fitness_pearsonr(self, fit, obs):
        """
        Name:     CalCanvas.fitness_pearsonr
        Inputs:   - numpy.ndarray, fitted data (fit)
                  - numpy.ndarray, observed data (obs)
        Outputs:  float, Pearson's r (r)
        Features: Calculates Pearson's r (correlation coefficient)
        """
        n = float(len(obs))
        ssxy = (obs*fit).sum() - obs.sum()*fit.sum()/n
        ssx = numpy.power(fit, 2.0).sum() - numpy.power(fit.sum(), 2.0)/n
        sst = numpy.power(obs, 2.0).sum() - numpy.power(obs.sum(), 2.0)/n
        r = ssxy/numpy.power(ssx*sst, 0.5)
        return r

    def highlight_points(self, x, y):
        """
        Name:     CalCanvas.highlight_points
        Inputs:   - numpy.ndarray, abscissa data (x)
                  - numpy.ndarray, ordinate data (y)
        Outputs:  None.
        Features: Plots red dots over the all-data black dots, calculates the
                  regression parameters for the selection data, and updates
                  the legend
        Depends:  - model_fitness
                  - show_base_plot
                  - x_y_regression
        """
        slope = self.x_y_regression(x, y)

        # Calculate model fitness:
        if numpy.isfinite(slope):
            y_fit = x*slope
            my_rsq, my_r, my_ioa = self.model_fitness(y_fit, y)
        else:
            my_rsq = numpy.nan
            my_r = numpy.nan
            my_ioa = numpy.nan

        # Create plot text:
        plot_txt = ("$\\mathrm{%s}$ ($n=%d$)\n"
                    "$y=%0.2e\;x$\n"
                    "$R^2=%0.3f$\n$r=%0.3f$\n"
                    "$IA=%0.3f$") % (
                        "Selection", len(x), slope, my_rsq, my_r, my_ioa)

        # Re-draw the base plot:
        self.show_base_plot()

        # Plot over base points with selected data:
        self.axes.scatter(
            x, y, c='r', s=40, edgecolors='none', label="Selection", alpha=0.6)
        if numpy.isfinite(slope):
            x_data = numpy.arange(100)*(self.x_max/100.0)
            y_fit = slope*x_data

            # Remove points that are way off the plotting range:
            if y_fit.max() > self.y_max:
                good_idx = numpy.where(y_fit <= self.y_max)
                x_data = x_data[good_idx]
                y_fit = y_fit[good_idx]

            self.axes.plot(x_data, y_fit, '--r', label='Select fit')
            self.axes.legend(
                bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=4,
                mode="expand", borderaxespad=0., fontsize=14)
        else:
            self.axes.legend(
                bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=3,
                mode="expand", borderaxespad=0., fontsize=14)
        self.axes.text(0.80, 0.30, plot_txt, transform=self.axes.transAxes,
                       fontsize=14, verticalalignment='top', bbox=self.props)
        Canvas.draw(self)

    def model_fitness(self, fit, obs):
        """
        Name:     CalCanvas.model_fitness
        Inputs:   - numpy.ndarray, fitted data (fit)
                  - numpy.ndarray, observed data (obs)
        Outputs:  - float, coefficient of determination, (rsqr)
                  - float, correlation coefficient (r)
                  - float, index of agreement (ioa)
        Features: Returns model fitness parameters (rsqr, r, ioa)
        Depends:  - fitness_cod
                  - fitness_ioa
                  - fitness_pearsonr
        """
        # Calculate the R-square, correlation coeff., and index of agreement:
        if not isinstance(fit, numpy.ndarray):
            fit = numpy.array(fit)
        if not isinstance(obs, numpy.ndarray):
            obs = numpy.array(obs)

        if len(fit) == len(obs):
            rsqr = self.fitness_cod(fit, obs)
            r = self.fitness_pearsonr(fit, obs)
            ioa = self.fitness_ioa(fit, obs)
            return (rsqr, r, ioa)
        else:
            self.logger.error("Array length mismatch!")
            raise ValueError("The number of observations must match fit!")

    def set_base_plot(self, x, y):
        """
        Name:     CalCanvas.set_base_plot
        Inputs:   - numpy.ndarray, abscissa data (x)
                  - numpy.ndarray, ordinate data (y)
        Outputs:  None.
        Features: Saves original x & y data to class; calculates the slope,
                  max array values, and model fitness; and plots base data
        Depends:  - model_fitness
                  - show_base_plot
                  - x_y_regression
        """
        self.x = x
        self.y = y
        self.m = self.x_y_regression(x, y)
        self.x_max = numpy.ceil(x).max()
        self.y_max = numpy.ceil(y).max()

        # Calculate model fitness:
        if numpy.isfinite(self.m):
            y_fit = x*self.m
            my_rsq, my_r, my_ioa = self.model_fitness(y_fit, y)
        else:
            my_rsq = numpy.nan
            my_r = numpy.nan
            my_ioa = numpy.nan

        # Create plot text:
        self.txt = ("$\\mathrm{%s}$ ($n=%d$)\n"
                    "$y=%0.2e\;x$\n"
                    "$R^2=%0.3f$\n$r=%0.3f$\n"
                    "$IA=%0.3f$") % (
                        "All", len(x), self.m, my_rsq, my_r, my_ioa)

        self.show_base_plot()

    def show_base_plot(self):
        """
        Name:     CalCanvas.show_base_plot
        Inputs:   None.
        Outputs:  None.
        Features: Scatter plot of base data with legend and informative text
        """
        self.axes.hold(False)
        self.axes.scatter(
            self.x, self.y, c='k', s=40, edgecolors='none', label="All Points",
            alpha=0.3)
        self.axes.hold(True)

        if numpy.isfinite(self.m):
            x_data = numpy.arange(100)*(self.x_max/100.0)
            y_fit = self.m*x_data
            self.axes.plot(x_data, y_fit, '--k', label='All fit')
            self.axes.legend(
                bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=3,
                mode="expand", borderaxespad=0., fontsize=14)
        else:
            self.axes.legend(
                bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2,
                mode="expand", borderaxespad=0., fontsize=14)
        self.axes.set_xlabel('Element size (pixels)', fontsize=14)
        self.axes.set_ylabel('Element height (pixels)', fontsize=14)
        self.axes.text(0.05, 0.95, self.txt, transform=self.axes.transAxes,
                       fontsize=14, verticalalignment='top', bbox=self.props)
        self.axes.grid(True)
        Canvas.draw(self)

    def x_y_regression(self, x_array, y_array):
        """
        Name:     CalCanvas.x_y_regression
        Inputs:   - numpy.ndarray, abscissa data (x_array)
                  - numpy.ndarray, ordinate data (y_data)
        Outputs:  float, estimated slope (m_est)
        Features: Calculates the slope between two arrays, assumes y-intercept
                  is set at x=0; returns NaN if regression fails
        """
        # NOTE: force y-intercept to (0, 0) with numpy zeros
        x_data = numpy.vstack([x_array, numpy.zeros(len(x_array))]).T
        try:
            self.logger.debug("Calculating least squares regression ...")
            m_est = numpy.linalg.lstsq(x_data, y_array)[0][0]
        except:
            self.logger.exception("Regression failed!")
            m_est = numpy.nan
        else:
            self.logger.debug("Slope = %f", m_est)

        return m_est
