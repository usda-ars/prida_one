#!/usr/bin/python
#
# calib_util.py
#
# VERSION: 1.0.0-dev
#
# LAST EDIT: 2016-05-24
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# This program is a simple example of a Qt5 application embedding matplotlib
# canvases.
# http://matplotlib.org/examples/user_interfaces/embedding_in_qt4.html
#
# Copyright history:
#   2005 Florent Rougon
#   2006 Darren Dale
#   2015 Jens H Nielsen,
#   2015 BoxControL
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os
import sys

import numpy
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtWidgets import QAbstractItemView
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QErrorMessage
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QMainWindow

from calib import Ui_MainWindow
from calib_parser import CalParser


###############################################################################
# CLASSES:
###############################################################################
class PridaPlot(QApplication):
    """
    PridaPlot, a QApplication.

    Name:      PridaPlot
    History:   Version 1.0.0-dev
               - created [16.05.02]
               - fixed get points access by name [16.05.18]
               - added data to QStandardItems and access by hashes [16.05.19]
               - added status bar update for read lines [16.05.23]
               - created save file function [16.05.24]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        PridaPlot class initialization.

        Name:     PridaPlot.__init__
        Inputs:   None.
        Outputs:  None.
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()
        self.base.setAttribute(Qt.WA_DeleteOnClose)

        # Create a logger:
        self.logger = logging.getLogger(__name__)

        self.view = Ui_MainWindow()
        self.logger.debug("loading the main UI file")
        self.view.setupUi(self.base)

        # Create a class error message
        self.logger.debug("initializing QErrorMessage")
        self.error = QErrorMessage()

        # Create a class parser
        self.parser = CalParser()

        # Create the VIEWER sheet model (where PIDs and sessions are listed)
        self.logger.debug("creating sheet model")
        self.sheet_model = QStandardItemModel()
        self.view.sheet.setModel(self.sheet_model)
        self.view.sheet.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.sheet.collapsed.connect(lambda: self.resize_sheet())
        self.view.sheet.expanded.connect(lambda: self.resize_sheet())
        self.view.sheet.setAnimated(True)

        # Create selection models:
        self.logger.debug("creating selection model")
        self.sheet_select_model = self.view.sheet.selectionModel()
        self.sheet_select_model.selectionChanged.connect(self.highlight)

        # Assign connections:
        self.view.load_button.clicked.connect(self.load_file)
        self.view.save_button.clicked.connect(self.save_file)

        self.base.setWindowTitle("Prida - Calibration Analysis")
        self.base.statusBar().showMessage("PridaPlot v.1.0", 2000)

        self.base.show()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def highlight(self, selection):
        """
        Name:     PridaPlot.highlight
        Inputs:   QtSelection object (selection)
        Outputs:  None.
        Features: Gathers user selection by row, extracts row data, parses
                  coordinates into appropriate arrays and sends it off to be
                  plotted; shows base plot if user makes a deselection
        """
        if selection.indexes():
            my_idx = selection.indexes()[0]
            my_selection = self.sheet_model.itemFromIndex(my_idx).data()
            self.logger.debug("user selected %s", my_selection)
            my_points = self.parser.get_points_by_hash(my_selection)
            x_array = numpy.array([])
            y_array = numpy.array([])
            for point in my_points:
                w, h = point
                x_array = numpy.append(x_array, [w, ])
                y_array = numpy.append(y_array, [h, ])
            self.view.plot.highlight_points(x_array, y_array)
        else:
            self.logger.debug("user deselected")
            self.view.plot.show_base_plot()

    def load_file(self):
        """
        Name:     PridaPlot.load_file
        Inputs:   None.
        Outputs:  None.
        Features: Requests input file from user and attempts to parse
        Depends:  - mayday
                  - update_sheet_model
        """
        self.logger.info("Button Clicked")
        self.logger.debug("requesting input file")
        path = QFileDialog.getOpenFileName(self.base,
                                           "Select File",
                                           os.path.expanduser("~"),
                                           "Text files (*.txt *.csv)")[0]
        self.logger.debug("user selected file %s", path)
        try:
            self.parser.cal_file = path
        except (ValueError, TypeError) as e:
            self.mayday(e.args[0])
        else:
            try:
                self.parser.parse_file()
            except IOError as e:
                self.mayday(e.args[0])
            else:
                # Status update:
                self.base.statusBar().showMessage(
                    "Read %d lines" % (len(self.parser.imgs)), 4000)

                self.sheet_items = 20
                self.plot_all()

    def load_sheet_headers(self):
        """
        Name:     PridaPlot.load_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the sheet_model
        Depends:  resize_sheet
        """
        self.sheet_model.setColumnCount(self.sheet_items)
        if len(self.parser.imgs) > 0:
            col_dict = self.parser.imgs[0].column_dict
            for i in col_dict:
                exec('%s(%d, QStandardItem("%s"))' % (
                    "self.sheet_model.setHorizontalHeaderItem", i,
                    col_dict[i]['title']))
        self.resize_sheet()

    def mayday(self, msg):
        """
        Name:     PridaPlot.mayday
        Inputs:   str, notification message (msg)
        Outputs:  None.
        Features: Opens a popup window displaying the notification text
        """
        self.error.showMessage(msg)

    def plot_all(self):
        """
        Name:     PridaPlot.plot_all
        Inputs:   None.
        Outputs:  None.
        Features: Parses all data into representative arrays and sets the
                  base plot
        Depends:  update_sheet_model
        """
        self.update_sheet_model()
        all_points = self.parser.get_all_points()

        # Initialize x and y arrays:
        x_array = numpy.array([])
        y_array = numpy.array([])

        # Parse coordinates into representative arrays:
        for point in all_points:
            w, h = point
            x_array = numpy.append(x_array, [w, ])
            y_array = numpy.append(y_array, [h, ])

        self.view.plot.set_base_plot(x_array, y_array)

    def resize_sheet(self):
        """
        Name:     PridaPlot.resize_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the sheet
        """
        self.logger.debug("resizing sheet")
        for col in range(self.sheet_items):
            self.view.sheet.resizeColumnToContents(col)

    def save_file(self):
        """
        Name:     PridaPlot.save_file
        Inputs:   None.
        Outputs:  None.
        Features: Saves current canvas to PNG file
        Depends:  mayday
        """
        self.logger.info("Button Clicked")
        self.logger.debug("requesting output file")
        path = QFileDialog.getSaveFileName(self.base,
                                           "Enter PNG File Name",
                                           os.path.expanduser("~"),
                                           "PNG file (*.png)")[0]
        self.logger.debug("user entered %s", path)
        if path != "":
            try:
                self.base.statusBar().showMessage(
                    "Saving current plot to %s" % (path),
                    4000)
                self.view.plot.print_png(path)
            except:
                self.logger.exception("Failed to save image to %s", path)
                self.mayday("Image save failed!")
            else:
                if os.path.isfile(path):
                    self.mayday("Image save successful.")
                else:
                    self.mayday("Something went wrong with the file save.")
        else:
            self.logger.debug("No file path entered!")

    def update_sheet_model(self):
        """
        Name:     PridaPlot.update_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Loads all rows in the sheet
        Depends:  - load_sheet_headers
                  - resize_sheet
        """
        self.logger.debug("updating the sheet model")
        self.sheet_model.clear()
        self.load_sheet_headers()
        self.logger.debug("reading row data")
        for i in range(len(self.parser.imgs)):
            h = hash(self.parser.imgs[i])
            d = self.parser.imgs[i].column_dict
            # Fill the PID row with its ordered attributes:
            sheet_list = []
            self.logger.debug("reading column data for row %d", i)
            for j in sorted(list(d.keys())):
                my_attr = d[j]["value"]
                my_item = QStandardItem(my_attr)
                my_item.setData(h)
                sheet_list.append(my_item)
            self.sheet_model.appendRow(sheet_list)
        self.resize_sheet()


###############################################################################
# MAIN:
###############################################################################
if __name__ == '__main__':
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)

    # Instantiating logging handler and record format:
    root_handler = logging.FileHandler("calib.log")
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)

    # Create and launch QApplication:
    app = PridaPlot()
    sys.exit(app.exec_())
