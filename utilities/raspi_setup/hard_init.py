"""
hard_init.py
Auto-Run PRIDA Hardware Initializers

Nathanael Shaw
2015-07-21
"""
from imaging import Imaging

def main():
    i = Imaging('/home/pi/repo/prida/')
    i.motor.turn_off_motors()
    i.led.all_off()

if __name__ == "__main__":
    main()
