#!/usr/bin/python
#
# drobo.py
#
# VERSION: 0.0.2-dev
#
# LAST EDIT: 2016-10-04
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import errno
import logging
import sys
import subprocess
import os

import PyQt5.uic as uic
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QMessageBox


###############################################################################
# FUNCTIONS:
###############################################################################
def exit_app(my_app):
    """
    Gracefully exit a given QApplication.

    Name:     exit_app
    Inputs:   object, QApplication (my_app)
    Outputs:  None
    Features: Graceful exiting of QApplication
    """
    logging.info("exiting QApplication")
    my_app.quit()


def resource_path(relative_path):
    """
    Return absolute path to a given resource file---for PyInstaller.

    Name:     resource_path
    Inputs:   str, resource file with path (relative_path)
    Outputs:  str, resource file with a potentially modified path
    Features: Returns absolute path for a given resource, works for dev and
              for PyInstaller.
    """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = sys.path[0]

    return os.path.join(base_path, relative_path)


###############################################################################
# CLASSES:
###############################################################################
class DroboApp(QApplication):
    """
    Name:     DroboApp
    Features: This app mounts Drobo to a local Linux directory based on the
              supplied credentials. Note that in order for this app to work
              properly, the Linux system's /etc/fstab must have been edited
              with the following line:
                  //10.253.66.56/RSADrobo /home/archie/Drobo/RSADrobo cifs
                    users,credentials=/home/archie/Drobo/credfile.txt 0 0
              You must restart your computer after editing /etc/fstab before
              using this app.
    History:  Version 0.0.2
              - created mayday function for warning messages [16.10.04]
              - added UTF-8 encoding support to file IO [16.10.04]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    drobo_ip = "10.253.66.56"
    home_dir = os.path.expanduser("~")
    drobo_dir = os.path.join(home_dir, "Drobo")
    rsa_dir = os.path.join(drobo_dir, "RSADrobo")
    cred_file = os.path.join(drobo_dir, "credfile.txt")
    username = None
    password = None

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     DroboApp.__init__
        Inputs:   None.
        Returns:  None.
        Features: App initialization
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # Check that UI file exists:
        self.drobo_ui = resource_path("drobo.ui")
        if not os.path.isfile(self.drobo_ui):
            raise IOError("Error! Missing required ui files!")

        # Load main window GUI to the QMainWindow:
        self.view = uic.loadUi(self.drobo_ui, self.base)
        self.base.setWindowTitle("Connecting to Drobo")

        # Connect the dialog buttons:
        self.view.c_connect.clicked.connect(self.connect)
        self.view.c_exit.clicked.connect(self.cancel_input)

        # Set logger and at exit register:
        self.logger = logging.getLogger(__name__)
        atexit.register(exit_app, self)

        self.base.show()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def cancel_input(self):
        """
        Name:     DroboApp.cancel_input
        Inputs:   None.
        Returns:  None.
        Features: Exits the app.
        """
        self.logger.info("exiting")
        sys.exit()

    def check_directory(self):
        """
        Name:     DroboApp.check_directory
        Inputs:   None.
        Returns:  bool, directory exists
        Features: Checks for RSADrobo directory existence, if false, then
                  attempts to create it.
        Depends:  mkdir_p
        """
        # Check existence:
        if not os.path.isdir(self.rsa_dir):
            try:
                self.mkdir_p(self.rsa_dir)
            except:
                self.logger.warning("drobo directory not created")
                return False
            else:
                self.logger.info("drobo directory created")
                return True
        else:
            self.logger.info("drobo directory already exists")
            return True

    def connect(self):
        """
        Name:     DroboApp.connect
        Inputs:   None.
        Returns:  None.
        Features: Connects to Drobo server
        Depends:  - check_directory
                  - mayday
                  - mount_drobo
        """
        is_dir = self.check_directory()
        if is_dir:
            self.username = self.view.c_user.text()
            self.password = self.view.c_pass.text()

            if self.username and self.password and \
               not self.username.isspace() and \
               not self.password.isspace():
                    is_mnt = self.mount_drobo()
                    if is_mnt:
                        self.logger.info("drobo connected")
                        is_gone = self.rm_credfile()
                        if is_gone:
                            sys.exit()
                        else:
                            self.mayday(
                                ("Something went wrong deleting '%s'. "
                                 "Please check to make certain it has been "
                                 "removed.") % (self.cred_file))
            else:
                self.mayday("You forgot to enter your credentials.")
        else:
            self.mayday((
                "Directory %s does not exist or could not be created. "
                "Please create this directory before continuing.") % (
                    self.rsa_dir))

    def alt_connect(self):
        """
        Name:     DroboApp.connect
        Inputs:   None.
        Returns:  None.
        Features: For debugging puroposes only.
        Depends:  mayday
        """
        self.username = self.view.c_user.text()
        self.password = self.view.c_pass.text()

        if self.username and self.password and \
           not self.username.isspace() and not self.password.isspace():
                print("You entered: %s, %s" % (self.username, self.password))
        else:
            self.mayday("You forgot to enter your credentials.")

    def create_credfile(self):
        """
        Name:     DroboApp.create_credfile
        Inputs:   None.
        Returns:  bool, credential file created
        Features: Creates the credential file for connecting to the Drobo.
        """
        try:
            self.logger.info("writing credential file")
            f = open(self.cred_file, "w", encoding="utf-8")
            f.write("username=%s\n" % (self.username))
            f.write("password=%s" % (self.password))
            f.close()
        except:
            self.logger.warning("failed to create credential file")
            return False
        else:
            self.logger.info("credential created")
            return True

    def mayday(self, msg):
        """
        Name:     DroboApp.mayday
        Inputs:   str, warning message (msg)
        Outputs:  None.
        Features: Opens a pop-up window displaying the warning text
        """
        QMessageBox.warning(self.base, "Warning", msg)

    def mkdir_p(self, path):
        """
        Name:     DroboApp.mkdir_p
        Inputs:   str, directory path (path)
        Returns:  None.
        Features: Makes directories, including intermediate directories as
                  required (i.e., directory tree)
        Ref:      tzot (2009) "mkdir -p functionality in python,"
                  StackOverflow, Online:
                  http://stackoverflow.com/questions/600268/mkdir-p-
                  functionality-in-python
        """
        try:
            self.logger.debug("creating directory tree")
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                self.logger.debug("directory exists")
                pass
            else:
                self.logger.warning("failed to create directory!")
                raise

    def mount_drobo(self):
        """
        Name:     DroboApp.mount_drobo
        Inputs:   None.
        Returns:  bool, mount successful
        Features: Calls the system command to mount the Drobo.
        Depends:  create_credfile
        """
        is_cred = self.create_credfile()
        if is_cred:
            cmd_a = "mount.cifs"
            cmd_b = "//%s/RSADrobo" % (self.drobo_ip)
            cmd_c = "%s" % (self.rsa_dir)
            try:
                self.logger.info("calling subprocess")
                subprocess.call([cmd_a, cmd_b, cmd_c])
            except:
                self.logger.warning("failed to execute subprocess command")
                return False
            else:
                return True

    def rm_credfile(self):
        """
        Name:     DroboApp.rm_credfile
        Inputs:   None.
        Returns:  bool, credential file is gone.
        Features: Deletes the credential file.
        """
        try:
            os.remove(self.cred_file)
        except OSError:
            self.logger.debug("no credential file to delete")
            return True
        except:
            self.logger.warning("credential file not deleted")
            return False
        else:
            self.logger.info("credential file deleted")
            return True

###############################################################################
# MAIN:
###############################################################################
if __name__ == "__main__":
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    # Instantiating logging handler and record format:
    root_handler = logging.StreamHandler()
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)

    app = DroboApp()
    app.exec_()
