/*
# ams_serial.ino
#
# VERSION: 1.5.0-dev
#
# LAST EDIT: 2016-11-03
#
####################################################################
# PUBLIC DOMAIN NOTICE                                             #
####################################################################
# This software is a "United States Government Work" under the     #
# terms of the United States Copyright Act. It was written as      #
# part of the authors' official duties as a United States          #
# Government employee and thus cannot be copyrighted. This         #
# software/database is freely available to the public for use.     #
# The Department of Agriculture (USDA) and the U.S. Government     #
# have not placed any restriction on its use or reproduction.      #
#                                                                  #          #
# Although all reasonable efforts have been taken to ensure the    #
# accuracy and reliability of the software, the USDA and the U.S.  #
# Government do not and cannot warrant the performance or results  #
# that may be obtained by using this software. The USDA and the    #
# U.S. Government disclaim all warranties, express or implied,     #
# including warranties of performance, merchantability or fitness  #
# for any particular purpose.                                      #
#                                                                  #
# Please cite the authors in any work or product based on this     #
# material.                                                        #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw       #
#     Robert W. Holley Center for Agriculture and Health           #
#     USDA-Agricultural Research Service                           #
#     538 Tower Road, Ithaca NY, 14853                             #
####################################################################
*/

/*
 * Requires the Adafruit_MotorShield_V2 library, which may be
 * found here:
 * https://github.com/adafruit/Adafruit_Motor_Shield_V2_Library/releases
 *
 * NOTE: Microsteps per step are defined in Adafruit_MotorShield.h
 *       Default = 16
*/
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

//instantiate global variables
Adafruit_MotorShield ams = Adafruit_MotorShield();
Adafruit_StepperMotor *sm = ams.getStepper(200, 1);
int led;
byte myByte;

void setup(){
  Serial.begin(9600);
  while(!Serial){
    //wait until serial is ready
  }

  //setup motor shield object
  ams.begin();
  sm->setSpeed(1);

  //initialize counter
  led = 13;
  pinMode(led, OUTPUT);

  // beacon until you receive a call:
  while(Serial.available()<=0){
    Serial.write(0);
    delay(500);
  }
}

void loop(){
  /*
  read bytes:
  00000000 - release motor (0)
  00100000 - echo value(32)
  00111111 - return to beacon mode(63)
  010xxxxx - take step (65 - 88)
     ^^^^^
     ||||| -- single step bit ----- (1 - true,     0 - false)
     |||| --- double step bit ----- (1 - true,     0 - false)
     ||| ---- interleave step bit - (1 - true,     0 - false)
     || ----- microstep step bit -- (1 - true,     0 - false)
     | ------ direction bit ------- (1 - forwards, 0 - backwards)
    example:
      01000010 - take one double step backwards (66)
      01011000 - take one microstep forward (88)
  01111111 - calibrate motor (127)

  note - matchless values get echoed (ie echo is default case)
  note - step bits are mutally exclusive (ie a step cannot be more than one type)
  */
  if (Serial.available()){
    while (Serial.available()>0){
      myByte = Serial.read();
    }

    //process command
    switch (myByte) {
      case 0:
        sm->release();
        Serial.write(0);
        break;
      case 65:
        sm->onestep(BACKWARD, SINGLE);
        break;
      case 66:
        sm->onestep(BACKWARD, DOUBLE);
        break;
      case 68:
        sm->onestep(BACKWARD, INTERLEAVE);
        break;
      case 72:
        sm->onestep(BACKWARD, MICROSTEP);
        break;
      case 81:
        sm->onestep(FORWARD, SINGLE);
        break;
      case 82:
        sm->onestep(FORWARD, DOUBLE);
        break;
      case 84:
        sm->onestep(FORWARD, INTERLEAVE);
        break;
      case 88:
        sm->onestep(FORWARD, MICROSTEP);
        break;
      case 127:
        // calibrate
        sm->step(3, FORWARD, MICROSTEP);
        Serial.write(myByte);
        break;
      case 63:
        sm->release();
        Serial.write(0);
        break;
      default:
        Serial.write(myByte);
        delay(500);
        break;
    }
  } 
}
