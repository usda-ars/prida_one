#!/bin/bash
#
# move_images.sh
#
# 2015-11-10 -- created
# 2015-11-10 -- last updated
#
# ~~~~~~~~~~~~~~~~~~~~
# PUBLIC DOMAIN NOTICE
# ~~~~~~~~~~~~~~~~~~~~
# This software is a "United States Government Work" under the terms of the
# United States Copyright Act. It was written as part of the authors' official
# duties as a United States Government employee and thus cannot be copyrighted.
# This software/database is freely available to the public for use.
# The Department of Agriculture (USDA) and the U.S. Government have not placed
# any restriction on its use or reproduction.
#
# Although all reasonable efforts have been taken to ensure the accuracy and
# reliability of the software, the USDA and the U.S. Government do not and
# cannot warrant the performance or results that may be obtained by using this
# software. The USDA and the U.S. Government disclaim all warranties, express
# or implied, including warranties of performance, merchantability or fitness
# for any particular purpose. Please cite the authors in any work or product
# based on this material:
#
#    Tyler W. Davis
#    USDA-ARS Robert W. Holley Center for Agricutlure and Health
#    538 Tower Road, Ithaca, NY 14853
#
# ~~~~~~~~~~~~
# description:
# ~~~~~~~~~~~~
# This script moves JPG image files from individual sub-directories to a single
# output directory, such as done with the export function of the PRIDA software
#
# ~~~~~~
# usage:
# ~~~~~~
# Copy this script to your computer and open a terminal window.
# Locate this file in your terminal and run the following:
#    bash move_images <search directory> <output directory>
# Replace "<search directory>" with a folder location where sub-directories
# are storing image files else, the program will search the current working
# directory. Replace "<output directory>" with a folder location where you
# want all the image files to be located, else they will be moved to the
# current working directory.
#
# Case 1: no arguments sent, search local directory, move to local directory.
if [ "$#" -eq 0 ]; then
    find . -type f -name "*.jpg" -exec mv {} ./ \;
fi

# Case 2: one argument given, search directory and export to local directory.
if [ "$#" -eq 1 ] && [ -d "$1" ]; then
    find "$1" -type f -name "*.jpg" -exec mv {} ./ \;
fi

# Case 3: two arguments given, search first directory and export to second.
if [ "$#" -eq 2 ] && [ -d "$1" ] && [ -d "$2" ]; then
    find "$1" -type f -name "*.jpg" -exec mv {} "$2" \;
fi
