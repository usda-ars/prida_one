#!/usr/bin/python
#
# usda_plants_parser.py
#
# VERSION: 1.3.0-dev
#
# LAST EDIT: 2016-01-20
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES
###############################################################################
import os.path
import logging


###############################################################################
# FUNCTION DEFINITIONS
###############################################################################
def process_file(filepath):
    """
    Name:    process_file
    Feature: Takes the usda_plants database dump textfile and creates a prida
             dictionary file for the genus species suggestion test box
    Inputs:  None
    Outputs: None
    """
    if os.path.isfile(filepath):
        logging.debug('file found at %s' % (filepath))
        my_dict = {}
        first_line = True
        with open(filepath, 'r') as my_file:
            logging.debug('reading data from file')
            for line in my_file.readlines():
                if first_line:
                    first_line = False
                else:
                    my_txt = line.split('","')
                    my_key = my_txt[0][1:len(my_txt[0])]
                    sub_key = my_txt[1]
                    if my_txt[3] != '':
                        my_val = (my_txt[3] + ' (' + my_txt[2] + ')')
                    else:
                        my_val = ('(' + my_txt[2] + ')')
                    sub_dict = my_dict.get(my_key, dict())
                    sub_dict.update({sub_key: my_val})
                    my_dict[my_key] = sub_dict
        with open('dictionary.txt', 'w') as new_file:
            logging.debug('writing data to dictionary.txt')
            for my_key in my_dict.keys():
                for sub_key in my_dict[my_key].keys():
                    if sub_key:
                        new_file.write(
                            "['%s','%s']:%s\n" % (my_key,
                                                  sub_key,
                                                  my_dict[my_key][sub_key]))
                    else:
                        new_file.write(
                            "['%s',]:%s\n" % (my_key,
                                              my_dict[my_key][sub_key]))
    else:
        logging.info('could not find file at %s' % (filepath))


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    process_file('usda_plants.txt')

