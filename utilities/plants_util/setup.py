#!/usr/bin/python
#
# setup.py
#
# LAST EDIT: 2017-06-13
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################

# This script checks for required software dependencies and launches the
# PLANTS utility app.

###############################################################################
# REQUIRED MODULES:
###############################################################################
import os
import re
import shutil
import subprocess
import sys


###############################################################################
# FUNCTIONS:
###############################################################################
def check_pyqt5(tot=49):
    """
    Name:     check_pyqt5
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if PyQt5 package is available
    """
    passed = False
    err = None
    a = "Checking for PyQt5"
    try:
        import PyQt5.QtCore
    except ImportError:
        err = ("Python package PyQt5 is not installed! "
               "Please install PyQt (e.g., http://pyqt.sourceforge.net).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(PyQt5.QtCore.QT_VERSION_STR)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_python3(tot=49):
    """
    Name:     check_python3
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if the main version of Python is version 3
    """
    passed = False
    err = None
    a = "Checking for Python 3"
    if sys.version_info[0] == 3:
        passed = True
        b = "(running v.{}.{}.{})".format(
            sys.version_info[0], sys.version_info[1], sys.version_info[2])
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))
    else:
        err = ("Python 3 not running! "
               "Python 3 is required for running this software.")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))

    return (passed, err)


def check_pyuic():
    """Checks if PyUIC is installed and working"""
    try:
        if sys.version_info >= (3, 5):
            if os.name == 'nt':
                return subprocess.run(
                    ['pyuic5.bat', '-h'], stdout=subprocess.PIPE).returncode
            else:
                return subprocess.run(
                    ['pyuic5', '-h'], stdout=subprocess.PIPE).returncode
        else:
            if os.name == 'nt':
                return subprocess.call(
                    ['pyuic5.bat', '-h'], stdout=subprocess.PIPE)
            else:
                return subprocess.call(
                    ['pyuic5', '-h'], stdout=subprocess.PIPE)
    except:
        return -1


def find_ui_files_r(my_dir):
    """
    Name:     find_ui_files_r
    Inputs:   str, directory path (my_dir)
    Outputs:  list, absolute paths to UI files
    Features: Returns list of UI files from a recursive directory search
    """
    ui_file_list = []
    for root, subdirs, files in os.walk(my_dir):
        for my_file in files:
            if re.match("^.*ui$", my_file):
                ui_file_list.append(os.path.join(root, my_file))
    return ui_file_list


def fix_mainwindow(py_file, fline=49):
    """
    Name:     fix_mainwindow
    Inputs:   - str, Python file to fix (py_file)
              - [optional] int, character fill line length (fline)
    Outputs:  tuple, okay boolean and error string
    Features: Fixes the Python file created by pyuic for the relative import
              made in mainwindow.ui in Prida v1.3+
    """
    okay = True
    err_msg = ""
    msg = "> fixing mainwindow.py"
    bak_file = py_file + ".bak"
    if os.path.isfile(py_file):
        try:
            shutil.copyfile(py_file, bak_file)
        except:
            okay = False
            err_msg = "Failed to create copy of %s!" % (
                os.path.basename(py_file))
            print("{} {} FAILED".format(msg, "."*(fline - len(msg))))
        else:
            try:
                f = open(py_file, 'r', encoding="utf-8")
            except:
                okay = False
                err_msg = "Failed to read %s!" % os.path.basename(py_file)
                print("{} {} FAILED".format(msg, "."*(fline - len(msg))))
            else:
                data = f.readlines()
                f.close()

                try:
                    g = open(py_file, 'w', encoding="utf-8")
                except:
                    okay = False
                    err_msg = "Failed to write to %s!" % (
                        os.path.basename(py_file))
                    print("{} {} FAILED".format(msg, "."*(fline - len(msg))))
                else:
                    for line in data:
                        line = re.sub("custom", ".custom", line)
                        g.write(line)
                    g.close()
                    print("{} {} OK".format(msg, "."*(fline - len(msg))))
    else:
        okay = False
        err_msg = "%s does not exist!" % py_file
        print("{} {} FAILED".format(msg, "."*(fline - len(msg))))

    return (okay, err_msg)


def perform_checks(fline=49):
    """
    Name:     perform_checks
    Inputs:   [optional] int, character fill line length (fline)
    Outputs:  - bool, okay to procees flag
              - list, checks passed versus checks performed
              - list, error messages
    Features: Runs a series of checks for Prida software development
    Depends:  - check_python3
              - check_pyqt5
              - find_ui_files_r
    """
    passed = [0, 0]
    to_proceed = True
    err_msgs = []

    # CHECK: Python 3 requirement
    c_py3, e_py3 = check_python3(fline)
    passed[1] += 1
    if c_py3:
        passed[0] += 1
    else:
        to_proceed = False
    if e_py3 is not None:
        err_msgs.append(e_py3)

    # CHECK: Python PyQt5
    c_pyqt, e_pyqt = check_pyqt5(fline)
    passed[1] += 1
    if c_pyqt:
        passed[0] += 1
    else:
        to_proceed = False
    if e_pyqt is not None:
        err_msgs.append(e_pyqt)

    # CHECK: main.py existence
    ck10 = "Checking for plants_util.py"
    if os.path.isfile("plants_util.py"):
        passed[0] += 1
        print("{} {} OK".format(ck10, "."*(fline - len(ck10))))
    else:
        to_proceed = False
        err_msgs.append(
            "plants_util.py is missing! "
            "Please check that this setup.py file is "
            "in the correct directory.")
        print("{} {} FAILED".format(ck10, "."*(fline - len(ck10))))
    passed[1] += 1

    # CHECK: UI files:
    ck11 = "Checking for plants.ui"
    ui_list = find_ui_files_r(os.path.abspath("."))
    if len(ui_list) != 1:
        to_proceed = False
        err_msgs.append(
            "UI file are missing! "
            "Please check that this setup.py file is "
            "in the correct directory.")
        print("{} {} FAILED".format(ck11, "."*(fline - len(ck11))))
    else:
        passed[0] += 1
        print("{} {} OK".format(ck11, "."*(fline - len(ck11))))
    passed[1] += 1

    return (to_proceed, passed, err_msgs)


def print_messages(msg_list, char_count):
    """
    Name:     print_messages
    Inputs:   - list, error messages (msg_list)
              - int, character fill line length (char_count)
    Outputs:  None.
    Features: Prints formatted error messages
    """
    for i in range(len(msg_list)):
        # Break each error message into individual words:
        msg = msg_list[i].split(" ")

        # Split the error message into separate lines based on each
        # line's length
        out_lines = []
        line_num = 0
        out_lines.append("")
        for j in range(len(msg)):
            out_lines[line_num] += msg[j]
            count = len(out_lines[line_num])
            if count > char_count - 7:
                line_num += 1
                out_lines.append("")
            out_lines[line_num] += " "
        for k in range(len(out_lines)):
            if not out_lines[k].isspace():
                if k == 0:
                    print("{0:2}. {1:}".format(i + 1, out_lines[k]))
                else:
                    print("   {}".format(out_lines[k]))
    print("{}".format('-'*char_count))


def ui2py(my_files, fline=49):
    """
    Name:     ui2py
    Inputs:   - list, list of UI files (my_files)
              - [optional] int, character fill line length (fline)
    Outputs:  tuple, to procees boolean, list of passed versus performed
              operations, and list of error messages
    Features: Runs the Qt development tool on given list of UI files
    """
    passed = [0, 0]
    to_proceed = True
    err_msgs = []
    py35 = sys.version_info >= (3, 5)

    if check_pyuic() == 0:
        if len(my_files) > 0:
            for my_file in my_files:
                if os.name == "nt":
                    cmd_arg1 = "pyuic5.bat"
                else:
                    cmd_arg1 = "pyuic5"
                cmd_arg2 = "-o"
                cmd_arg3 = "%s.py" % (os.path.splitext(my_file)[0])
                cmd_arg4 = my_file
                msg = "> %s" % os.path.basename(my_file)
                try:
                    if py35:
                        subprocess.run(
                            [cmd_arg1, cmd_arg2, cmd_arg3, cmd_arg4],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            check=True)
                    else:
                        subprocess.check_call(
                            [cmd_arg1, cmd_arg2, cmd_arg3, cmd_arg4],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
                except:
                    to_proceed = False
                    err_msgs.append(
                        "pyuic failed to compile file %s "
                        "from directory %s" % (os.path.basename(my_file),
                                               os.path.dirname(my_file)))
                    print("{} {} FAILED".format(msg, "."*(fline - len(msg))))
                else:
                    passed[0] += 1
                    print("{} {} OK".format(msg, "."*(fline - len(msg))))
                    if re.match(".*mainwindow.*$", my_file):
                        passed[1] += 1
                        is_okay, err_msg = fix_mainwindow(cmd_arg3)
                        if not is_okay:
                            err_msgs.append(err_msg)
                        else:
                            passed[0] += 1
                passed[1] += 1
        else:
            passed[1] += 1
            to_proceed = False
            err_msgs.append("No UI files found!")
            print(" {} FAILED".format("."*fline))
    else:
        passed[1] += 1
        to_proceed = False
        err_msgs.append("pyuic not installed!")
        print(" {} FAILED".format("."*fline))

    return (to_proceed, passed, err_msgs)

###############################################################################
# MAIN:
###############################################################################
if __name__ == '__main__':
    # Define the main program module:
    mainfile = "plants_util.py"
    line_len = 57

    greeting = " PLANTS utility setup "
    ending = " end setup "
    gdots = int(0.5*(line_len - len(greeting)))
    edots = int(0.5*(line_len - len(ending)))
    print("{}{}{}".format("-"*gdots, greeting, "-"*gdots))

    # Perform system checks:
    to_proceed, num_passed, messages = perform_checks()

    if to_proceed:
        print("{}{}{}".format('-'*edots, ending, '-'*edots))
        print("Passed {}/{}".format(num_passed[0], num_passed[1]))
        print("")

        if num_passed[0] < num_passed[1]:
            print("Encountered the following warnings:")
            print_messages(messages, line_len)
            print("")

        # Allow user to pass "-y" argument to setup.py to run if successful:
        if "-y" in sys.argv:
            ans = "y"
        elif "-n" in sys.argv:
            ans = 'n'
        else:
            try:
                ans = raw_input("Start application? (Y/N): ")
            except NameError:
                try:
                    ans = input("Start application? (Y/N): ")
                except EOFError:
                    ans = ''
            finally:
                assert isinstance(ans, str)

        if ans.lower() == "y":
            if os.path.isfile(mainfile):
                with open(mainfile) as f:
                    code = compile(f.read(), mainfile, 'exec')
                    exec(code)
            else:
                print("... FAILED! %s not found." % mainfile)
    else:
        print("{}{}{}".format('-'*edots, ending, '-'*edots))
        print("Passed {}/{}".format(num_passed[0], num_passed[1]))
        print("")
        print("Encountered the following errors:")
        print_messages(messages, line_len)
