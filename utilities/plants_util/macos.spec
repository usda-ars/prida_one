# -*- mode: python -*-

block_cipher = None


my_path = './'
a = Analysis([my_path + 'plants_util.py'],
             pathex=['.'],
             binaries=None,
             datas=None,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
a.datas += [('plants.ui', my_path + 'plants.ui', 'DATA'),
            ('loading.gif', my_path + 'loading.gif', 'DATA'),
            ('icon.png', my_path + 'icon.png', 'DATA'),
            ('usda_plants.txt.gz', my_path + 'usda_plants.txt.gz', 'DATA')]
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='plants_db',
          debug=False,
          strip=False,
          upx=True,
          console=False,
          icon='icon.icns')
app = BUNDLE(exe,
             name='plants_db.app',
             icon='icon.icns',
             bundle_identifier=None,
             info_plist={
                 'CFBundleDevelopmentRegion': 'en_US',
                 'CFBundleShortVersionString': '1.3',
                 'CFBundleVersion': '1.3.1',
                 'CFBundleName': 'PLANTS Database Utility',
                 'CFBundleDisplayName': 'PLANTS DB',
                 'CFBundleSignature': '????',
                 'NSHighResolutionCapable': 'True',
                 'NSHumanReadableCopyright': 'This software is freely available to the public for use.'
                 })
