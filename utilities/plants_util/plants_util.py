#!/usr/bin/python
#
# plants_util.py
#
# VERSION: 1.3.1
#
# LAST EDIT: 2017-06-13
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import gzip
import logging
import sys
import os

import PyQt5.uic as uic
from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QMovie
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import QThread
from PyQt5.QtCore import QObject
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QMessageBox

__version__ = "1.3.1"


###############################################################################
# FUNCTIONS:
###############################################################################
def exit_app(my_app):
    """
    Name:     exit_app
    Inputs:   object, QApplication (my_app)
    Outputs:  None
    Features: Graceful exiting of QApplication
    """
    logging.info('exiting QApplication')
    my_app.quit()


def resource_path(relative_path):
    """
    Name:     resource_path
    Inputs:   str, resource file with path (relative_path)
    Outputs:  str, resource file with a potentially modified path
    Features: Returns absolute path for a given resource, works for dev and
              for PyInstaller.
    """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except AttributeError:
        base_path = sys.path[0]

    return os.path.join(base_path, relative_path)


###############################################################################
# CLASSES:
###############################################################################
class PlantsApp(QApplication):
    """
    Name:     PlantsApp
    Features: This app acts as a utility to add and remove USDA PLANTS entries
              to the Prida dictionary.txt file.
    History:  Version 1.3.1
              - added row count to plants database [17.06.13]
              - added check for empty database [17.06.13]
              Version 1.3.0
              - added main window icon [17.06.13]
              - created base directory class attribute [17.06.13]
              - added version to window title [17.06.13]
              - changed default dict path to Nonetype [17.06.13]
              - added check for dict path in save file [17.06.13]
              Version 1.2.1
              - removed QErrorMessage (self.error) [16.10.03]
              - created mayday and notice functions (QMessageBox) [16.10.03]
              - removed loading GIF as critical element [16.10.03]
              - created read file function [16.10.03]
              - updated file checking [16.10.03]
              - added UTF-8 file encoding in read/write [16.10.04]
              Version 1.1.0
              - moved utility functions to preamble [16.06.22]
              - swapped urllib test import to favor Python 3 [16.06.22]
              Version 1.0.0
              - connected search bar [16.01.25]
              - connected add to dict function [16.01.25]
              - connected remove from dict function [16.01.25]
              - remove duplicates between dictionary and database [16.01.25]
              - addressed UnicodeDecodeError when using Python 2 [16.01.25]
              - changed search to returnPressed event [16.01.26]
              - added threading [16.01.26]
              - changed sys.exit to self.quit [16.01.26]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     PlantsApp.__init__
        Inputs:   None.
        Returns:  None.
        Features: App initialization
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # Check that UI file exists:
        self.plants_ui = resource_path("plants.ui")
        if not os.path.isfile(self.plants_ui):
            raise IOError("Error! Missing required ui file!")

        # Check that loading spinner exists and create movie:
        self.load_gif = resource_path("loading.gif")
        if os.path.isfile(self.load_gif):
            self.movie = QMovie(self.load_gif)
        else:
            self.logger.warning("loading GIF not found")
            self.movie = QMovie(None)

        # Load main window GUI to the QMainWindow:
        self.view = uic.loadUi(self.plants_ui, self.base)
        self.base.setWindowTitle(
            "Prida PLANTS Database Utility %s" % __version__)

        # Load the main window icon
        icon_path = resource_path("icon.png")
        if os.path.isfile(icon_path):
            self.im_icon = QIcon(QPixmap(icon_path))
            self.setWindowIcon(self.im_icon)
        else:
            self.im_icon = None

        # Define base directory to search for dictionary file:
        home_dir = os.path.expanduser("~")
        prida_dir = os.path.join(home_dir, "Prida")
        if os.path.isdir(prida_dir):
            self.base_dir = prida_dir
        else:
            self.base_dir = home_dir

        # Set logger and at exit register:
        self.logger = logging.getLogger(__name__)
        atexit.register(exit_app, self)

        # Create a thread and worker object:
        self.thread = QThread()
        self.worker = Worker()
        self.worker.moveToThread(self.thread)
        self.worker.finished.connect(self.thread.quit)
        self.thread.started.connect(self.worker.load_usda_plants)
        self.thread.finished.connect(self.finish_loading)

        # Create sheet models and load their headers:
        self.logger.debug("creating sheet models")
        self.plants_sheet_model = QStandardItemModel()
        self.view.plants_sheet.setModel(self.plants_sheet_model)
        self.dict_sheet_model = QStandardItemModel()
        self.view.dict_sheet.setModel(self.dict_sheet_model)
        self.load_plants_sheet_headers()
        self.load_dict_sheet_headers()

        # Create selection models:
        self.logger.debug("creating selection models")
        self.dict_select_model = self.view.dict_sheet.selectionModel()
        self.plants_select_model = self.view.plants_sheet.selectionModel()

        # Connect the QPushButtons:
        self.logger.debug("connecting QPushButtons")
        self.view.cancel_button.clicked.connect(self.close_app)
        self.view.open_button.clicked.connect(self.open_file)
        self.view.new_button.clicked.connect(self.new_file)
        self.view.add_arrow.clicked.connect(self.add_to_dict)
        self.view.remove_arrow.clicked.connect(self.rm_from_dict)
        self.view.save_button.clicked.connect(self.save_file)
        self.view.load_button.clicked.connect(self.load_database)

        # Connect search bar:
        self.view.search.returnPressed.connect(self.update_sheet_models)

        # Initialize dictionary file path, USDA PLANT dictionary, user's plants
        # dictionary
        self.dict_isopen = False
        self.dict_path = None
        self.usda_plants = {}
        self.user_plants = {}

        # Show number of items in dictionary (empty)
        num_items = len(self.usda_plants)
        self.view.plantsLabel.setText(
            "USDA PLANTS Database (%d items)" % num_items)

        self.base.show()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def add_to_dict(self):
        """
        Name:     PlantsApp.add_to_dict
        Inputs:   None.
        Outputs:  None.
        Features: Adds selection to dictionary, removing it from the database
        Depends:  - mayday
                  - update_sheet_models
        """
        self.logger.info("Button Clicked")
        if self.dict_isopen:
            if self.plants_select_model.hasSelection():
                # Get the selection and put together the file name and path:
                self.logger.debug("gathering user selection")
                my_key1 = self.plants_sheet_model.itemFromIndex(
                    self.plants_select_model.selectedIndexes()[0])
                my_key2 = self.plants_sheet_model.itemFromIndex(
                    self.plants_select_model.selectedIndexes()[1])
                my_val = self.plants_sheet_model.itemFromIndex(
                    self.plants_select_model.selectedIndexes()[2])
                entry_key = ".".join([my_key1.text(), my_key2.text()])
                my_entry = (my_key1.text(), my_key2.text(), my_val.text())
                self.logger.info(entry_key)

                if entry_key in self.user_plants.keys():
                    self.logger.info("selection already exists in dictionary")
                else:
                    self.logger.debug("adding selection to dictionary")
                    self.user_plants[entry_key] = my_entry

                    self.logger.debug("removing selection from usda plants")
                    self.usda_plants.pop(entry_key, None)

                    self.logger.debug("updating sheet models")
                    search_terms = self.view.search.text()
                    self.update_sheet_models(search_terms)
            else:
                self.logger.warning("nothing selected!")
                self.mayday((
                    "No selection made. "
                    "Please make a selection from the USDA PLANTS database."))
        else:
            self.logger.warning("dictionary not open!")
            self.mayday((
                "No dictionary file opened. "
                "Please open a dictionary file or create a new one."))

    def close_app(self):
        """
        Name:     PlantsApp.close_app
        Inputs:   None.
        Returns:  None.
        Features: Exits the app.
        """
        self.logger.info("exiting")
        if self.thread.isRunning():
            self.logger.warning("thread running, exiting...")
            self.thread.quit()
        self.quit()

    def disable_buttons(self):
        """
        Name:     PlantsApp.disable_buttons
        Inputs:   None.
        Outputs:  None.
        Features: Disables buttons that could conflict during the loading of
                  PLANTS database
        """
        self.view.add_arrow.setEnabled(False)
        self.view.remove_arrow.setEnabled(False)
        self.view.load_button.setEnabled(False)
        self.view.new_button.setEnabled(False)
        self.view.open_button.setEnabled(False)
        self.view.save_button.setEnabled(False)

    def enable_buttons(self):
        """
        Name:     PlantsApp.enable_buttons
        Inputs:   None.
        Outputs:  None.
        Features: Enables buttons that were disabled during the loading of
                  PLANTS database
        """
        self.view.add_arrow.setEnabled(True)
        self.view.remove_arrow.setEnabled(True)
        self.view.load_button.setEnabled(True)
        self.view.new_button.setEnabled(True)
        self.view.open_button.setEnabled(True)
        self.view.save_button.setEnabled(True)

    def finish_loading(self):
        """
        Name:     PlantsApp.finish_loading
        Inputs:   None.
        Outputs:  None.
        Features: Ends loading animation, gathers database from worker, and
                  updates sheets
        Depends:  - enable_buttons
                  - update_sheet_models
        """
        # End movie, return database, enable buttons and update sheet:
        self.logger.info("finished loading")
        self.movie.stop()
        self.usda_plants = self.worker.plants_db
        if len(self.usda_plants) == 0:
            self.mayday("The database is empty!")
        self.enable_buttons()
        self.update_sheet_models()

    def load_database(self):
        """
        Name:     PlantsApp.load_database
        Inputs:   None.
        Outputs:  None.
        Features: Checks that dictionary is ready, starts thread download
                  thread and begins loading animation
        Depends:  mayday
        """
        # Send user plants to worker object:
        self.worker.user_db = self.user_plants
        if self.dict_isopen:
            # Start animation and thread:
            self.logger.debug("loading gif movie")
            self.view.plantsLabel.setMovie(self.movie)
            self.view.plantsLabel.show()
            self.movie.start()

            # Disable buttons that shouldn't be used right now:
            self.disable_buttons()

            self.logger.debug("staring thread")
            self.thread.start()
        else:
            self.logger.warning("dictionary not open!")
            self.mayday(
                "No dictionary file opened. "
                "Please open a dictionary file or create a new one.")

    def load_dict_sheet_headers(self):
        """
        Name:     PlantsApp.load_dict_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers for the sheet models
        Depends:  resize_dict_sheet
        """
        self.logger.debug("loading dict sheet headers")
        self.dict_sheet_model.setColumnCount(3)
        self.dict_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem("Key 1"))
        self.dict_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem("Key 2"))
        self.dict_sheet_model.setHorizontalHeaderItem(
            2, QStandardItem("Common name (Genus sp.)"))
        self.resize_dict_sheet()

    def load_plants_sheet_headers(self):
        """
        Name:     PlantsApp.load_plants_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers for the sheet models
        Depends:  resize_plants_sheet
        """
        self.logger.debug("loading plants sheet headers")
        self.plants_sheet_model.setColumnCount(3)
        self.plants_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem("Key 1"))
        self.plants_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem("Key 2"))
        self.plants_sheet_model.setHorizontalHeaderItem(
            2, QStandardItem("Common name (Genus sp.)"))
        self.resize_plants_sheet()

    def mayday(self, msg):
        """
        Name:     PlantsApp.mayday
        Inputs:   str, warning message (msg)
        Outputs:  None.
        Features: Opens a pop-up window displaying the warning text
        """
        QMessageBox.warning(self.base, "Warning", msg)

    def new_file(self):
        """
        Name:     PlantsApp.new_file
        Inputs:   None.
        Outputs:  None.
        Features: Prepares for a new dictionary file
        Depends:  - mayday
                  - read_file
        """
        self.logger.info("Button Clicked")
        self.logger.debug("requesting new file name from user")
        path = QFileDialog.getSaveFileName(None,
                                           "Create New File",
                                           self.base_dir)[0]
        if path != '':
            # Update base directory
            self.base_dir = os.path.dirname(path)

            if os.path.splitext(path)[1] == '':
                path += '.txt'
                self.dict_path = path
                self.view.dictLabel.setText(path)
                self.dict_isopen = True
                if os.path.isfile(path):
                    self.logger.info("Found existing dictinary file!")
                    self.read_file()
                else:
                    self.logger.debug("opening new file")
                    self.user_plants = {}
            elif os.path.splitext(path)[1] != '.txt':
                self.mayday(
                    "Only a plain text file (.txt) is allowed "
                    "to be a dictionary. Please enter a new name.")
            else:
                self.dict_path = path
                self.view.dictLabel.setText(path)
                self.dict_isopen = True
                if os.path.isfile(path):
                    self.logger.info("Found existing dictinary file!")
                    self.read_file()
                else:
                    self.logger.debug("opening new file")
                    self.user_plants = {}

    def notice(self, msg):
        """
        Name:     PlantsApp.notice
        Inputs:   str, notification message (msg)
        Outputs:  None.
        Features: Opens a pop-up window displaying the notification text
        """
        QMessageBox.information(self.base, "Notice", msg)

    def open_file(self):
        """
        Name:     PlantsApp.open_file
        Inputs:   None.
        Outputs:  None.
        Features: Opens an existing file
        Depends:  read_file
        """
        self.logger.info("Button Clicked")
        self.logger.debug("request existing file from user")
        path = QFileDialog.getOpenFileName(None,
                                           "Select File",
                                           self.base_dir,
                                           filter="*.txt")[0]
        if os.path.isfile(path):
            self.base_dir = os.path.dirname(path)
            self.logger.info("opening selected file %s", path)
            self.dict_path = path
            self.view.dictLabel.setText(path)
            self.read_file()

    def read_file(self):
        """
        Name:     PlantsApp.read_file
        Inputs:   None.
        Outputs:  None.
        Features: Reads an existing dictionary file
        Depends:  - mayday
                  - update_dict_sheet_model
        """
        try:
            f = open(self.dict_path, 'r', encoding='utf-8')
            my_dict = f.read().splitlines()
            f.close()
        except IOError:
            self.logger.info("dictionary file not found")
        else:
            my_idx = 0
            self.user_plants = {}
            self.dict_isopen = True

            self.logger.info("read file contents to dictionary list")
            for line in my_dict:
                my_items = line.split(":")
                if len(my_items) == 2:
                    try:
                        my_keys = eval("%s" % my_items[0])
                    except NameError:
                        self.logger.exception(
                            "check format on bad dictionary file entry '%s'",
                            line)
                        self.mayday(
                            ("Entry '%s' is not understood in dictionary. "
                             "Please check file format or create a "
                             "new dictionary file.") % line)
                        self.user_plants = {}
                        self.dict_isopen = False
                        self.update_dict_sheet_model()
                        self.view.dictLabel.setText("None")
                        break
                    else:
                        if len(my_keys) == 2:
                            my_key1 = str(my_keys[0])
                            my_key2 = str(my_keys[1])
                            my_entry = (str(my_keys[0]),
                                        str(my_keys[1]),
                                        str(my_items[1]))
                        elif len(my_keys) == 1:
                            my_key1 = str(my_keys[0])
                            my_key2 = ""
                            my_entry = (str(my_keys[0]), "", str(my_items[1]))
                        else:
                            raise ValueError("found too many keys in file")
                            self.logger.error(
                                "found too many keys in line %s", line)
                        entry_key = ".".join([my_key1, my_key2])
                        self.logger.info("saving entry %s", str(my_entry))
                        self.logger.info("using key %s", entry_key)
                        if entry_key not in self.user_plants.keys():
                            self.user_plants[entry_key] = my_entry
                        else:
                            self.logger.warning(
                                "%s key already exists", entry_key)
                        my_idx += 1
                        self.update_dict_sheet_model()
                else:
                    self.logger.warning("no keys found in line %s", line)
                    self.mayday((
                        "No database keys found in dictionary. "
                        "Please create a new dictionary file."))
                    self.user_plants = {}
                    self.dict_isopen = False
                    self.update_dict_sheet_model()
                    self.view.dictLabel.setText("None")
                    break

    def resize_dict_sheet(self):
        """
        Name:     PlantsApp.resize_dict_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the dictionary sheet
        """
        self.logger.debug("resizing")
        for col in range(3):
            self.view.dict_sheet.resizeColumnToContents(col)

    def resize_plants_sheet(self):
        """
        Name:     PlantsApp.resize_plants_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the plants sheet
        """
        self.logger.debug("resizing")
        for col in range(3):
            self.view.plants_sheet.resizeColumnToContents(col)

    def rm_from_dict(self):
        """
        Name:     PlantsApp.rm_from_dict
        Inputs:   None.
        Outputs:  None.
        Features: Removes entry from dictionary and returns it to the database
        Depends:  - mayday
                  - update_sheet_models
        """
        self.logger.info("Button Clicked")
        if self.dict_isopen:
            if self.dict_select_model.hasSelection():
                # Get the selection and put together the file name and path:
                self.logger.debug("gathering user selection")
                my_key1 = self.dict_sheet_model.itemFromIndex(
                    self.dict_select_model.selectedIndexes()[0])
                my_key2 = self.dict_sheet_model.itemFromIndex(
                    self.dict_select_model.selectedIndexes()[1])
                my_val = self.dict_sheet_model.itemFromIndex(
                    self.dict_select_model.selectedIndexes()[2])
                entry_key = ".".join([my_key1.text(), my_key2.text()])
                my_entry = (my_key1.text(), my_key2.text(), my_val.text())
                self.logger.info(entry_key)

                if entry_key in self.usda_plants.keys():
                    self.logger.info("selection already exists in dictionary")
                else:
                    self.logger.debug("removing selection from dictionary")
                    self.user_plants.pop(entry_key, None)

                    self.logger.debug("adding selection back to database")
                    self.usda_plants[entry_key] = my_entry

                    self.logger.debug("updating sheet models")
                    search_terms = self.view.search.text()
                    self.update_sheet_models(search_terms)
            else:
                self.logger.warning("nothing selected!")
                self.mayday((
                    "No selection made. "
                    "Please make a selection from your dictionary file."))
        else:
            self.logger.warning("dictionary not open!")
            self.mayday((
                "No dictionary file opened. "
                "Please open a dictionary file or create a new one."))

    def save_file(self):
        """
        Name:     PlantsApp.save_file
        Inputs:   None.
        Outputs:  None.
        Features: Saves the current user dictionary to file.
        Depends:  - close_app
                  - mayday
                  - notice
        """
        if self.dict_path is not None:
            if os.path.isfile(self.dict_path):
                self.logger.warning("overwriting existing file")
            try:
                f = open(self.dict_path, "w", encoding="utf-8")
                for key in self.user_plants.keys():
                    items = self.user_plants[key]
                    if items[1] == "":
                        my_line = "['%s',]:%s\n" % (items[0], items[2])
                    else:
                        my_line = "['%s','%s']:%s\n" % (items)
                    f.write(my_line)
            except:
                self.logger.exception("failed to open file for writing")
            else:
                f.close()
                self.notice("File saved.")
            finally:
                self.close_app()
        else:
            self.logger.warning("Cannot save; no file opened!")
            self.mayday("No file opened for saving!")

    def search(self, items, terms):
        """
        Name:     PlantsApp.search
        Inputs:   - list, PLANTS database items (items)
                  - list, search terms (terms)
        Outputs:  bool (found)
        Features: Searches the items and returns true if all terms are
                  found anywhere within the fields
        """
        found = True
        for term in terms:
            if not isinstance(term, str):
                term = str(term)
            found = found and any(term in item.lower() for item in items)
        return found

    def update_dict_sheet_model(self):
        """
        Name:     PlantsApp.update_dict_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Fills the rows in the dict sheet
        Depends:  - load_dict_sheet_headers
                  - resize_dict_sheet
        """
        self.logger.debug("updating sheet")
        self.dict_sheet_model.clear()
        self.load_dict_sheet_headers()
        for items in self.user_plants.values():
            item_list = []
            for item in items:
                item_list.append(QStandardItem(item))
            self.dict_sheet_model.appendRow(item_list)
        self.resize_dict_sheet()

    def update_sheet_models(self, search=''):
        """
        Name:     PlantsApp.update_sheet_models
        Inputs:   [optional] str, search string (search)
        Outputs:  None.
        Features: Filters both sheet models depending on the search terms.
        Depends:  - load_dict_sheet_headers
                  - load_plants_sheet_headers
                  - resize_dict_sheet
                  - resize_plants_sheet
                  - search
        """
        if search == "":
            search = self.view.search.text()
        terms = search.lower().split()

        # Start filtering dict sheet model:
        self.logger.debug("updating dict sheet")
        self.dict_sheet_model.clear()
        self.load_dict_sheet_headers()
        for items in self.user_plants.values():
            item_list = []
            if self.search(list(items), terms):
                for item in items:
                    item_list.append(QStandardItem(item))
                self.dict_sheet_model.appendRow(item_list)
        self.resize_dict_sheet()

        # Now filter plants sheet model:
        self.plants_sheet_model.clear()
        self.load_plants_sheet_headers()
        for items in self.usda_plants.values():
            item_list = []
            if self.search(list(items), terms):
                for item in items:
                    item_list.append(QStandardItem(item))
                self.plants_sheet_model.appendRow(item_list)
        self.resize_plants_sheet()
        num_items = self.plants_sheet_model.rowCount()
        self.view.plantsLabel.setText(
            "USDA PLANTS Database (%d items)" % num_items)


class Worker(QObject):
    """
    Name:     Worker
    Features: This class is a worker object used for threading the download of
              the USDA PLANTS database.
    History:  Version 1.3.0
              - update url to local gzip file [17.06.13]
              - removed url package dependency [17.06.13]
              Version 1.2.0
              - updated try-catch for evaluating lines [16.10.03]
              Version 1.1.0
              - updated url to dropbox (USDA PLANTS page not found) [16.06.22]
              Version 1.0.0
              - created load usda plants function [16.01.26]
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    finished = pyqtSignal()
    plants_file = resource_path("usda_plants.txt.gz")
    plants_db = {}
    user_db = {}

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def load_usda_plants(self):
        """
        Name:     Worker.load_usda_plants
        Inputs:   None.
        Outputs:  None.
        Features: Downloads USDA PLANTS database from the web, parses the
                  database fields, checks for existing fields in the user
                  dictionary, and saves to plants dictionary
        """
        logging.info("beginning")
        line_count = 0
        try:
            logging.debug("opening PLANTS database file")
            with gzip.open(self.plants_file, 'rb') as f:
                for line in f:
                    if line_count != 0:
                        try:
                            my_items = eval(line)
                        except SyntaxError:
                            logging.error(
                                "Could not evaluate line %d", line_count)
                        except:
                            logging.exception("Error on line %d", line_count)
                        else:
                            my_key1 = my_items[0]
                            my_key2 = my_items[1]
                            if my_items[3] != "":
                                my_val = "%s (%s)" % (my_items[3], my_items[2])
                            else:
                                my_val = "(%s)" % (my_items[2])
                            my_entry = (my_key1, my_key2, my_val)
                            entry_key = ".".join([my_key1, my_key2])

                            if entry_key in self.plants_db.keys():
                                logging.warning("duplicate key %s", entry_key)
                            elif entry_key in self.user_db.keys():
                                logging.info("skipping key %s", entry_key)
                            else:
                                logging.debug("adding key %s", entry_key)
                                self.plants_db[entry_key] = my_entry
                    line_count += 1
        except:
            logging.exception("failed to load PLANTS database")
            self.plants_db = {}
            self.finished.emit()
        else:
            logging.info("finished")
            f.close()
            self.finished.emit()


###############################################################################
# MAIN:
###############################################################################
if __name__ == "__main__":
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    # Instantiating logging handler and record format:
    root_handler = logging.StreamHandler()
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)

    app = PlantsApp()
    app.exec_()
