# -*- mode: python -*-

block_cipher = None


my_path = '.\\'
a = Analysis([my_path + 'plants_util.py'],
             pathex=['.'],
             binaries=None,
             datas=None,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
a.datas += [('plants.ui', my_path + 'plants.ui', 'DATA'),
            ('loading.gif', my_path + 'loading.gif', 'DATA'),
            ('icon.png', my_path + 'icon.png', 'DATA'),
            ('usda_plants.txt.gz', my_path + 'usda_plants.txt.gz', 'DATA')]

# To get rid of the popup warning message in Windows:
a.binaries -= [('.\pywintypes34.dll', None, None)]

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='plants_db.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False,
          icon='icon.ico')
