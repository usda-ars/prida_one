#!/usr/bin/python
#
# comp_test_data_parser.py
#
# VERSION: 1.2.0-dev
#
# LAST EDIT: 2015-12-07
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# Script for parsing, plotting and saving the results of the hdf5 dataset
# compression tests. Section one parses files resulting from the test sweeping
# across chunk dimensions with shuffle off on compresion option 2.
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import os
import re

import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt

mac = True
if mac:
    filepath = ('/Users/twdavis/Dropbox/Work/USDA/Projects/RootImaging/Prida/'
                'Data/comp_testing/pi_comp_test_w_opt_2.txt')
else:
    filepath = ('/home/nms604/Dropbox/Prida/Data/comp_testing/'
                'pi_comp_test_w_opt_2.txt')


def tuple_parser(tup_str):
    """
    Name:    tuple_parser
    Feature: takes a string representing a chunk dimension tuple and parses it
             as a tuple of integer values
    Inputs:  tuple, string representing chunk dimensions (tup_str)
    Outputs: 3 integer tuple of dimension values
    """
    x, y, z = (int(re.sub('[()]', '', term)) for term in tup_str.split(','))
    return x, y, z


# NEW DATA PARSER #############################################################
def new_tuple_parser(tup_str):
    """
    """
    x, y, z = (int(re.sub('[()]', '', term)) for term in tup_str.split(','))
    return x*y*z/1000.


def new_data_parser(file_name, img_name):
    """
    @TODO
    """
    if os.path.isfile(file_name):
        data = np.loadtxt(
            fname=file_name,
            dtype={'names': ('image_name',
                             'chunk_size_kb',
                             'time_s',
                             'file_size_mb'),
                   'formats': ('S35', 'f4', 'f4', 'f4')},
            delimiter=':',
            converters={0: np.str,
                        1: lambda s: new_tuple_parser(s),
                        2: np.float,
                        3: lambda s: float(s)/1e6},
            skiprows=1)
        img_indx = np.where(data['image_name'] == img_name)
        temp = data[img_indx]
        return np.copy(temp)


def plot_x_y(data, x_lab, y_lab, c_lab):
    """
    @TODO
    """
    my_x = data[x_lab]
    my_y = data[y_lab]
    # my_z = data[c_lab]
    my_z = (data[c_lab] - data[c_lab].min())/(
        data[c_lab].max() - data[c_lab].min())
    # colors = plt.cm.coolwarm(my_z)
    my_title = "Plot '%s' versus '%s' colored by '%s'" % (x_lab, y_lab, c_lab)

    fig1 = plt.figure(figsize=(16, 9), dpi=120)
    ax1 = fig1.add_subplot(111)
    ax1.scatter(my_x, my_y, s=100, c=my_z, edgecolors='face', alpha=0.5,
                cmap=plt.cm.rainbow)
    ax1.set_xlabel(x_lab, fontsize=14)
    ax1.set_ylabel(y_lab, fontsize=14)
    ax1.set_title(my_title, fontsize=16)
    ax1.grid(True)
    # ax1.text(0.75, 0.75,
    #         'RED < %.2fMb\n%.2fMb < GREEN < %.2fMb\n%.2fMb < BLUE' %
    #         (threshold * max_size, threshold * max_size,
    #          (1 - threshold) * max_size, (1 - threshold) * max_size),
    #         fontsize=my_font_size,
    #         transform=ax1.transAxes)
    plt.show()


###############################################################################
# SECTION ONE - XPS CHUNK SWEEP TEST
###############################################################################

def file_parser(my_file):
    """
    Name:    file_parser
    Feature: Takes a input datafile with specific formatting and outputs arrays
             for plotting the data.
    Inputs:  str, absolute file path to data file (my_file)
    Outputs: tuple of data arrays, color arrays and constant values
    """
    # Initialize arrays, set constant values
    threshold = 0.33
    image_names = []
    compression_option = []
    chunks = []
    times = []
    sizes = []
    x_dims = []
    y_dims = []
    areas = []
    shape_colors = []
    time_colors = []
    size_colors = []
    ALPHA = 0.5
    RED = (1.0, 0, 0, ALPHA)
    GREEN = (0, 1.0, 0, ALPHA)
    BLUE = (0, 0, 1.0, ALPHA)

    with open(my_file) as data_file:
        firstline = True
        for line in data_file.readlines():
            if firstline:
                # skip header line
                firstline = False
            else:
                a, b, c, d, e = line.split(':')

                # add values from current line to an array
                image_names.append(a)
                compression_option.append(int(b))
                chunks.append(tuple_parser(c))

                # calculate relative shape
                half_perim = tuple_parser(c)[0] + tuple_parser(c)[1]
                x_dims.append(tuple_parser(c)[0])
                rel_w = (tuple_parser(c)[0] / (1.0 * half_perim))
                y_dims.append(tuple_parser(c)[1])
                rel_h = (tuple_parser(c)[1] / (1.0 * half_perim))

                # build color arrays for shape graphs
                if abs(rel_w - rel_h) < threshold:
                    shape_colors.append(GREEN)
                elif (rel_w - rel_h) > threshold:
                    shape_colors.append(RED)
                elif (rel_w - rel_h) < -1 * threshold:
                    shape_colors.append(BLUE)

                # convert to ~kb
                areas.append(3 * ((tuple_parser(c)[0] *
                                   tuple_parser(c)[1]) / 1000.0))
                times.append(float(d))

                # convert to ~Mb
                sizes.append(int(e) / 1000000.0)

        # calculate constants from arrays
        max_time = max(times)
        min_time = min(times)
        max_size = max(sizes)
        min_size = min(sizes)

        # re-define color constants for next graphs
        ALPHA = 0.1
        RED = (1.0, 0, 0, ALPHA)
        GREEN = (0, 1.0, 0, ALPHA)
        BLUE = (0, 0, 1.0, ALPHA)

        # build color arrays for dim graphs
        for k in range(len(times)):
            my_time = (times[k] - min_time) / (1.0 * (max_time - min_time))
            if my_time < threshold:
                time_colors.append(RED)
            elif my_time > (1.0 - threshold):
                time_colors.append(BLUE)
            else:
                time_colors.append(GREEN)
            my_size = (sizes[k] - min_size) / (1.0 * (max_size - min_size))
            if my_size < threshold:
                size_colors.append(RED)
            elif my_size > (1.0 - threshold):
                size_colors.append(BLUE)
            else:
                size_colors.append(GREEN)

        # Convert to numpy arrays
        x_dims = np.array(x_dims)
        y_dims = np.array(y_dims)
        areas = np.array(areas)
        times = np.array(times)
        sizes = np.array(sizes)

        # return all arrays
        return (x_dims, y_dims, areas, times, sizes,
                shape_colors, time_colors, size_colors, threshold)


def make_plots(my_tuple):
    """
    Name:    make_plots
    Feature: plots the output of data parser onto six scatter plots across
             three figures with variable color points indicating values
             crossing specific thresholds. Saves the figures to a pdf file.
    Inputs:  tuple of data arrays, color arrays and constants (my_tuple)
    Outputs: None
    """
    pp = PdfPages('compression_testing_graphs.pdf')
    # parse input tuple
    x_dims = my_tuple[0]
    y_dims = my_tuple[1]
    areas = my_tuple[2]
    times = my_tuple[3]
    max_time = max(times)
    sizes = my_tuple[4]
    max_size = max(sizes)
    shape_colors = my_tuple[5]
    time_colors = my_tuple[6]
    size_colors = my_tuple[7]
    threshold = my_tuple[8]
    x_max = 4000.0
    y_max = 6000.0
    my_font_size = 8

    # begin figure 1, plot 1
    fig1 = plt.figure(figsize=(16, 9), dpi=120)
    ax1 = fig1.add_subplot(211)
    ax1.scatter(x_dims, times, c=size_colors, marker='.', edgecolors='face')
    ax1.set_xlim(0, x_max)
    ax1.set_ylim(min(times), max(times))
    ax1.set_xlabel('y chunk dimension (pixels)', fontsize=my_font_size)
    ax1.set_ylabel('time (s)', fontsize=my_font_size)
    ax1.set_title('Y vs. Time, Across File Sizes', fontsize=my_font_size)
    ax1.grid(True)
    ax1.text(0.75, 0.75,
             'RED < %.2fMb\n%.2fMb < GREEN < %.2fMb\n%.2fMb < BLUE' %
             (threshold * max_size, threshold * max_size,
              (1 - threshold) * max_size, (1 - threshold) * max_size),
             fontsize=my_font_size,
             transform=ax1.transAxes)

    # begin figure 1, plot 2
    ax2 = fig1.add_subplot(212)
    ax2.scatter(y_dims, times, c=size_colors, marker='.', edgecolors='face')
    ax2.set_xlim(0, y_max)
    ax2.set_ylim(min(times), max(times))
    ax2.set_xlabel('x chunk dimension (pixels)', fontsize=my_font_size)
    ax2.set_ylabel('time (s)', fontsize=my_font_size)
    ax2.set_title('X vs. Time, Across File Sizes', fontsize=my_font_size)
    ax2.grid(True)
    ax2.text(0.75, 0.75,
             'RED < %.2fMb\n%.2fMb < GREEN < %.2fMb\n%.2fMb < BLUE' %
             (threshold * max_size, threshold * max_size,
              (1 - threshold) * max_size, (1 - threshold) * max_size),
             fontsize=my_font_size,
             transform=ax2.transAxes)
    pp.savefig()
    plt.close()

    # begin figure 2, plot 1
    fig2 = plt.figure(figsize=(16, 9), dpi=120)
    ax3 = fig2.add_subplot(211)
    ax3.scatter(x_dims, sizes, c=time_colors, marker='.', edgecolors='face')
    ax3.set_xlim(0, x_max)
    ax3.set_ylim(min(sizes), max(sizes))
    ax3.set_xlabel('y chunk dimension (pixels)', fontsize=my_font_size)
    ax3.set_ylabel('size (Mb)', fontsize=my_font_size)
    ax3.set_title('Y vs. File Size, Across Times', fontsize=my_font_size)
    ax3.grid(True)
    ax3.text(0.75, 0.75,
             'RED < %.2fs\n%.2fs < GREEN < %.2fs\n%.2fs < BLUE' %
             (threshold * max_time, threshold * max_time,
              (1 - threshold) * max_time, (1 - threshold) * max_time),
             fontsize=my_font_size,
             transform=ax3.transAxes)

    # begin figure 2, plot 2
    ax4 = fig2.add_subplot(212)
    ax4.scatter(y_dims, sizes, c=time_colors, marker='.', edgecolors='face')
    ax4.set_xlim(0, y_max)
    ax4.set_ylim(min(sizes), max(sizes))
    ax4.set_xlabel('x chunk dimension (pixels)', fontsize=my_font_size)
    ax4.set_ylabel('size (Mb)', fontsize=my_font_size)
    ax4.set_title('X vs. File Size, Across Times', fontsize=my_font_size)
    ax4.grid(True)
    ax4.text(0.75, 0.75,
             'RED < %.2fs\n%.2fs < GREEN < %.2fs\n%.2fs < BLUE' %
             (threshold * max_time, threshold * max_time,
              (1 - threshold) * max_time, (1 - threshold) * max_time),
             fontsize=my_font_size,
             transform=ax4.transAxes)
    pp.savefig()
    plt.close()

    # begin figure 3, plot 1
    fig3 = plt.figure(figsize=(16, 9), dpi=120)
    ax5 = fig3.add_subplot(211)
    ax5.scatter(areas, times, c=shape_colors, marker='.', edgecolors='face')
    ax5.set_xlim(1, 1000)
    ax5.set_ylim(min(times), max(times))
    ax5.set_xlabel('chunk size (~kb)', fontsize=my_font_size)
    ax5.set_ylabel('time (s)', fontsize=my_font_size)
    ax5.set_title('Chunk Size vs. Time, Across Shapes', fontsize=my_font_size)
    ax5.grid(True)
    ax5.text(0.75, 0.75,
             '(2 * |w - h|) / P < %i%% == GREEN\n '
             '(2 * (w - h)) / P > %i%% == RED\n '
             '(2 * (w - h)) / P < %i%% == BLUE' %
             (threshold * 100.0, threshold * 100.0, threshold * -100.0),
             fontsize=my_font_size,
             transform=ax5.transAxes)

    # begin figure 3, plot 2
    ax6 = fig3.add_subplot(212)
    ax6.scatter(areas, sizes, c=shape_colors, marker='.', edgecolors='face')
    ax6.set_xlim(1, 1000)
    ax6.set_ylim(min(sizes), max(sizes))
    ax6.set_xlabel('chunk size (~kb)', fontsize=my_font_size)
    ax6.set_ylabel('size (Mb)', fontsize=my_font_size)
    ax6.set_title('Chunk Size vs File Size, Across Shapes',
                  fontsize=my_font_size)
    ax6.grid(True)
    ax6.text(0.75, 0.75,
             '(2 * |w - h|) / P < %i%% == GREEN\n '
             '(2 * (w - h)) / P > %i%% == RED\n '
             '(2 * (w - h)) / P < %i%% == BLUE' %
             (threshold * 100.0, threshold * 100.0, threshold * -100.0),
             fontsize=my_font_size,
             transform=ax6.transAxes)
    pp.savefig()
    plt.close()
    d = pp.infodict()
    d['Title'] = 'HDF5 File Dataset Compression Testing'
    d['Author'] = 'Nathanael M. Shaw'
    pp.close()


###############################################################################
# SECTION TWO - RPI CHUNK SWEEP VALIDATION TEST
###############################################################################
def parse_data_file(filepath):
    """
    Name:   parse_data_file
    Feature: Takes a input datafile with specific formatting and outputs arrays
             for plotting the data.
    Inputs:  str, absolute file path to data file (my_file)
    Outputs: tuple of data arrays, color arrays and constant values across images
    """
    data = np.loadtxt(
        fname=filepath,
        dtype={'names': ('image_name',
                         'chunk_size',
                         'time',
                         'file_size'),
               'formats': ('S35', np.object, np.float, np.int)},
        delimiter=':',
        converters={1: lambda s: tuple_parser(s)},
        skiprows=1)
    threshold = 0.33
    alpha = 0.25
    filenames = {name for name in data['image_name']}
    img_arrays = []
    for cur_name in filenames:
        x_dim = []
        y_dim = []
        areas = []
        shape_colors = []
        time_colors = []
        size_colors = []
        times = []
        sizes = []
        x_arrays = []
        y_arrays = []
        color_arrays = []
        for row_num in range(len(data)):
            if data['image_name'][row_num] == cur_name:
                x_val = data['chunk_size'][row_num][0]
                y_val = data['chunk_size'][row_num][1]
                x_dim.append(x_val)
                y_dim.append(y_val)
                areas.append(x_val * y_val)
                times.append(data['time'][row_num])
                sizes.append(data['file_size'][row_num])
        for index in range(len(times)):
            shape_colors.append(shape_color_picker(y_dim[index],
                                                   x_dim[index],
                                                   threshold,
                                                   alpha))
            time_colors.append(color_picker(times[index],
                                            times,
                                            threshold,
                                            alpha))
            size_colors.append(color_picker(sizes[index],
                                            sizes,
                                            threshold,
                                            alpha))
        x_arrays = (x_dim, y_dim, areas)
        y_arrays = (times, sizes)
        color_arrays = (shape_colors, time_colors, size_colors)
        img_arrays.append((cur_name, (x_arrays, y_arrays, color_arrays)))
    return img_arrays, threshold


def make_plots_from_file(filepath):
    """
    Name:    make_plots_from_file
    Feature: plots the output of data parser onto six scatter plots across
             three figures with variable color points indicating values
             crossing specific thresholds. Saves the figures to a pdf file.
    Inputs:  tuple of data arrays, color arrays and constants (my_tuple)
    Outputs: None
    """
    pp = PdfPages('compression_testing_graphs.pdf')
    my_font_size = 8
    images, threshold = parse_data_file(filepath)

    image1, image2 = images
    name1, array1 = image1
    x_array1, y_array1, color_array1 = array1
    x_dim1, y_dim1, chunk_size1 = x_array1
    time1, file_size1 = y_array1
    shape_colors1, time_colors1, size_colors1 = color_array1

    name2, array2 = image2
    x_array2, y_array2, color_array2 = array2
    x_dim2, y_dim2, chunk_size2 = x_array2
    time2, file_size2 = y_array2
    shape_colors2, time_colors2, size_colors2 = color_array2

    #//////////////////////////////////////////////////////////////////////////
    # IMAGE #1
    #\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    #img1, figure 1, plot 1
    fig1 = plt.figure(figsize=(16, 9), dpi=120)
    ax1 = fig1.add_subplot(211)
    ax1.scatter(x_dim1,
                time1,
                c=size_colors1,
                marker='.',
                edgecolors='face')
    ax1.set_xlim(0, max(x_dim1))
    ax1.set_ylim(min(time1), max(time1))
    ax1.set_xlabel('y chunk dimension (pixels)', fontsize=my_font_size)
    ax1.set_ylabel('time (s)', fontsize=my_font_size)
    ax1.set_title('%s: Y vs. Time, Across File Sizes' %
                  (name1), fontsize=my_font_size)
    ax1.grid(True)
    ax1.text(0.75, 0.75,
             'RED < %ib\n%ib < GREEN < %ib\n%ib < BLUE' %
 (((threshold * (max(file_size1) - min(file_size1))) + min(file_size1)),
  ((threshold * (max(file_size1) - min(file_size1))) + min(file_size1)),
  (((1 - threshold) * (max(file_size1) - min(file_size1))) + min(file_size1)),
  (((1 - threshold) * (max(file_size1) - min(file_size1))) + min(file_size1))),
             fontsize=my_font_size,
             transform=ax1.transAxes)

    #img1, figure 1, plot 2
    ax2 = fig1.add_subplot(212)
    ax2.scatter(y_dim1,
                time1,
                c=size_colors1,
                marker='.',
                edgecolors='face')
    ax2.set_xlim(0, max(y_dim1))
    ax2.set_ylim(min(time1), max(time1))
    ax2.set_xlabel('x chunk dimension (pixels)', fontsize=my_font_size)
    ax2.set_ylabel('time (s)', fontsize=my_font_size)
    ax2.set_title('%s: X vs. Time, Across File Sizes' %
                  (name1), fontsize=my_font_size)
    ax2.grid(True)
    ax2.text(0.75, 0.75,
             'RED < %ib\n%ib < GREEN < %ib\n%ib < BLUE' %
 (((threshold * (max(file_size1) - min(file_size1))) + min(file_size1)),
  ((threshold * (max(file_size1) - min(file_size1))) + min(file_size1)),
  (((1 - threshold) * (max(file_size1) - min(file_size1))) + min(file_size1)),
  (((1 - threshold) * (max(file_size1) - min(file_size1))) + min(file_size1))),
             fontsize=my_font_size,
             transform=ax2.transAxes)
    pp.savefig()
    plt.close()

    #img1, figure 2, plot 1
    fig2 = plt.figure(figsize=(16, 9), dpi=120)
    ax3 = fig2.add_subplot(211)
    ax3.scatter(x_dim1,
                file_size1,
                c=time_colors1,
                marker='.',
                edgecolors='face')
    ax3.set_xlim(0, max(x_dim1))
    ax3.set_ylim(min(file_size1), max(file_size1))
    ax3.set_xlabel('y chunk dimension (pixels)', fontsize=my_font_size)
    ax3.set_ylabel('size (bytes)', fontsize=my_font_size)
    ax3.set_title('%s: Y vs. File Size, Across Times' %
                  (name1), fontsize=my_font_size)
    ax3.grid(True)
    ax3.text(0.75, 0.75,
             'RED < %.2fs\n%.2fs < GREEN < %.2fs\n%.2fs < BLUE' %
             (((threshold * (max(time1) - min(time1))) + min(time1)),
              ((threshold * (max(time1) - min(time1))) + min(time1)),
              (((1 - threshold) * (max(time1) - min(time1))) + min(time1)),
              (((1 - threshold) * (max(time1) - min(time1))) + min(time1))),
             fontsize=my_font_size,
             transform=ax3.transAxes)

    #img1, figure 2, plot 2
    ax4 = fig2.add_subplot(212)
    ax4.scatter(y_dim1,
                file_size1,
                c=time_colors1,
                marker='.',
                edgecolors='face')
    ax4.set_xlim(0, max(y_dim1))
    ax4.set_ylim(min(file_size1), max(file_size1))
    ax4.set_xlabel('x chunk dimension (pixels)', fontsize=my_font_size)
    ax4.set_ylabel('size (bytes)', fontsize=my_font_size)
    ax4.set_title('%s: X vs. File Size, Across Times' %
                  (name1), fontsize=my_font_size)
    ax4.grid(True)
    ax4.text(0.75, 0.75,
             'RED < %.2fs\n%.2fs < GREEN < %.2fs\n%.2fs < BLUE' %
             (((threshold * (max(time1) - min(time1))) + min(time1)),
              ((threshold * (max(time1) - min(time1))) + min(time1)),
              (((1 - threshold) * (max(time1) - min(time1))) + min(time1)),
              (((1 - threshold) * (max(time1) - min(time1))) + min(time1))),
             fontsize=my_font_size,
             transform=ax4.transAxes)
    pp.savefig()
    plt.close()

    #img1, figure 3, plot 1
    fig3 = plt.figure(figsize=(16, 9), dpi=120)  # figsize=(16, 9), dpi=120
    ax5 = fig3.add_subplot(211)
    ax5.scatter(chunk_size1,
                time1,
                c=shape_colors1,
                marker='.',
                edgecolors='face')
    ax5.set_xlim(min(chunk_size1), max(chunk_size1))
    ax5.set_ylim(min(time1), max(time1))
    ax5.set_xlabel('chunk size (pixels$^{2}$)', fontsize=my_font_size)
    ax5.set_ylabel('time (s)', fontsize=my_font_size)
    ax5.set_title('%s: Chunk Size vs. Time, Across Shapes' %
                  (name1), fontsize=my_font_size)
    ax5.grid(True)
    ax5.text(0.75, 0.75,
             '(2 * |w - h|) / P < %i%% == GREEN\n '
             '(2 * (w - h)) / P > %i%% == RED\n '
             '(2 * (w - h)) / P < %i%% == BLUE' %
             (threshold * 100.0, threshold * 100.0, threshold * -100.0),
             fontsize=my_font_size,
             transform=ax5.transAxes)

    #img1, figure 3, plot 2
    ax6 = fig3.add_subplot(212)
    ax6.scatter(chunk_size1,
                file_size1,
                c=shape_colors1,
                marker='.',
                edgecolors='face')
    ax6.set_xlim(min(chunk_size1), max(chunk_size1))
    ax6.set_ylim(min(file_size1), max(file_size1))
    ax6.set_xlabel('chunk size (pixels$^{2}$)', fontsize=my_font_size)
    ax6.set_ylabel('size (bytes)', fontsize=my_font_size)
    ax6.set_title('%s: Chunk Size vs File Size, Across Shapes' %
                  (name1), fontsize=my_font_size)
    ax6.grid(True)
    ax6.text(0.75, 0.75,
             '(2 * |w - h|) / P < %i%% == GREEN\n '
             '(2 * (w - h)) / P > %i%% == RED\n '
             '(2 * (w - h)) / P < %i%% == BLUE' %
             (threshold * 100.0, threshold * 100.0, threshold * -100.0),
             fontsize=my_font_size,
             transform=ax6.transAxes)
    pp.savefig()
    plt.close()

    #//////////////////////////////////////////////////////////////////////////
    # IMAGE #2
    #\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    #img2, figure 4, plot 1
    fig4 = plt.figure(figsize=(16, 9), dpi=120)
    ax7 = fig4.add_subplot(211)
    ax7.scatter(x_dim2,
                time2,
                c=size_colors2,
                marker='.',
                edgecolors='face')
    ax7.set_xlim(0, max(x_dim2))
    ax7.set_ylim(min(time2), max(time2))
    ax7.set_xlabel('y chunk dimension (pixels)', fontsize=my_font_size)
    ax7.set_ylabel('time (s)', fontsize=my_font_size)
    ax7.set_title('%s: Y vs. Time, Across File Sizes' %
                  (name2), fontsize=my_font_size)
    ax7.grid(True)
    ax7.text(0.75, 0.75,
             'RED < %ib\n%ib < GREEN < %ib\n%ib < BLUE' %
 (((threshold * (max(file_size2) - min(file_size2))) + min(file_size2)),
  ((threshold * (max(file_size2) - min(file_size2))) + min(file_size2)),
  (((1 - threshold) * (max(file_size2) - min(file_size2))) + min(file_size2)),
  (((1 - threshold) * (max(file_size2) - min(file_size2))) + min(file_size2))),
             fontsize=my_font_size,
             transform=ax7.transAxes)

    #img2, figure 4, plot 1
    ax8 = fig4.add_subplot(212)
    ax8.scatter(y_dim2,
                time2,
                c=size_colors2,
                marker='.',
                edgecolors='face')
    ax8.set_xlim(0, max(y_dim2))
    ax8.set_ylim(min(time2), max(time2))
    ax8.set_xlabel('x chunk dimension (pixels)', fontsize=my_font_size)
    ax8.set_ylabel('time (s)', fontsize=my_font_size)
    ax8.set_title('%s: X vs. Time, Across File Sizes' %
                  (name2), fontsize=my_font_size)
    ax8.grid(True)
    ax8.text(0.75, 0.75,
             'RED < %ib\n%ib < GREEN < %ib\n%ib < BLUE' %
 (((threshold * (max(file_size2) - min(file_size2))) + min(file_size2)),
  ((threshold * (max(file_size2) - min(file_size2))) + min(file_size2)),
  (((1 - threshold) * (max(file_size2) - min(file_size2))) + min(file_size2)),
  (((1 - threshold) * (max(file_size2) - min(file_size2))) + min(file_size2))),
             fontsize=my_font_size,
             transform=ax8.transAxes)
    pp.savefig()
    plt.close()

    #img2, figure 5, plot 1
    fig5 = plt.figure(figsize=(16, 9), dpi=120)
    ax9 = fig5.add_subplot(211)
    ax9.scatter(x_dim2,
                file_size2,
                c=time_colors2,
                marker='.',
                edgecolors='face')
    ax9.set_xlim(0, max(x_dim2))
    ax9.set_ylim(min(file_size2), max(file_size2))
    ax9.set_xlabel('y chunk dimension (pixels)', fontsize=my_font_size)
    ax9.set_ylabel('size (bytes)', fontsize=my_font_size)
    ax9.set_title('%s: Y vs. File Size, Across Times' %
                  (name2), fontsize=my_font_size)
    ax9.grid(True)
    ax9.text(0.75, 0.75,
             'RED < %.2fs\n%.2fs < GREEN < %.2fs\n%.2fs < BLUE' %
             (((threshold * (max(time2) - min(time2))) + min(time2)),
              ((threshold * (max(time2) - min(time2))) + min(time2)),
              (((1 - threshold) * (max(time2) - min(time2))) + min(time2)),
              (((1 - threshold) * (max(time2) - min(time2))) + min(time2))),
             fontsize=my_font_size,
             transform=ax9.transAxes)

    #img 2, figure 5, plot 2
    ax10 = fig5.add_subplot(212)
    ax10.scatter(y_dim2,
                file_size2,
                c=time_colors2,
                marker='.',
                edgecolors='face')
    ax10.set_xlim(0, max(y_dim2))
    ax10.set_ylim(min(file_size2), max(file_size2))
    ax10.set_xlabel('x chunk dimension (pixels)', fontsize=my_font_size)
    ax10.set_ylabel('size (bytes)', fontsize=my_font_size)
    ax10.set_title('%s: X vs. File Size, Across Times' %
                  (name2), fontsize=my_font_size)
    ax10.grid(True)
    ax10.text(0.75, 0.75,
             'RED < %.2fs\n%.2fs < GREEN < %.2fs\n%.2fs < BLUE' %
             (((threshold * (max(time2) - min(time2))) + min(time2)),
              ((threshold * (max(time2) - min(time2))) + min(time2)),
              (((1 - threshold) * (max(time2) - min(time2))) + min(time2)),
              (((1 - threshold) * (max(time2) - min(time2))) + min(time2))),
             fontsize=my_font_size,
             transform=ax10.transAxes)
    pp.savefig()
    plt.close()

    #img2, figure 6, plot 1
    fig6 = plt.figure(figsize=(16, 9), dpi=120)
    ax11 = fig6.add_subplot(211)
    ax11.scatter(chunk_size2,
                time2,
                c=shape_colors2,
                marker='.',
                edgecolors='face')
    ax11.set_xlim(min(chunk_size2), max(chunk_size2))
    ax11.set_ylim(min(time2), max(time2))
    ax11.set_xlabel('chunk size (pixels$^{2}$)', fontsize=my_font_size)
    ax11.set_ylabel('time (s)', fontsize=my_font_size)
    ax11.set_title('%s: Chunk Size vs. Time, Across Shapes' %
                   (name2), fontsize=my_font_size)
    ax11.grid(True)
    ax11.text(0.75, 0.75,
             '(2 * |w - h|) / P < %i%% == GREEN\n '
             '(2 * (w - h)) / P > %i%% == RED\n '
             '(2 * (w - h)) / P < %i%% == BLUE' %
             (threshold * 100.0, threshold * 100.0, threshold * -100.0),
             fontsize=my_font_size,
             transform=ax11.transAxes)

    #img2, figure 6, plot 2
    ax12 = fig6.add_subplot(212)
    ax12.scatter(chunk_size2,
                file_size2,
                c=shape_colors2,
                marker='.',
                edgecolors='face')
    ax12.set_xlim(min(chunk_size2), max(chunk_size2))
    ax12.set_ylim(min(file_size2), max(file_size2))
    ax12.set_xlabel('chunk size (pixels$^{2}$)', fontsize=my_font_size)
    ax12.set_ylabel('size (bytes)', fontsize=my_font_size)
    ax12.set_title('%s: Chunk Size vs File Size, Across Shapes' %
                   (name2), fontsize=my_font_size)
    ax12.grid(True)
    ax12.text(0.75, 0.75,
             '(2 * |w - h|) / P < %i%% == GREEN\n '
             '(2 * (w - h)) / P > %i%% == RED\n '
             '(2 * (w - h)) / P < %i%% == BLUE' %
             (threshold * 100.0, threshold * 100.0, threshold * -100.0),
             fontsize=my_font_size,
             transform=ax12.transAxes)
    pp.savefig()
    plt.close()
    d = pp.infodict()
    d['Title'] = 'HDF5 File Dataset Compression Testing'
    d['Author'] = 'Nathanael M. Shaw'
    pp.close()


def shape_color_picker(wval, hval, threshold, alpha):
    """
    @TODO
    """
    RED = (1, 0, 0, alpha)
    BLUE = (0, 0, 1, alpha)
    GREEN = (0, 1, 0, alpha)
    wp = (1.0 * wval) / (1.0 * (wval + hval))
    hp = (1.0 * hval) / (1.0 * (wval + hval))
    if (wp - hp) < (-1 * threshold):
        return BLUE
    elif(wp - hp) > threshold:
        return RED
    else:
        return GREEN


def color_picker(my_val, my_dat, threshold, alpha):
    """
    @TODO
    """
    RED = (1, 0, 0, alpha)
    BLUE = (0, 0, 1, alpha)
    GREEN = (0, 1, 0, alpha)
    per_val = ((my_val - min(my_dat)) / (1.0 * (max(my_dat) - min(my_dat))))
    if per_val > (1 - threshold):
        return BLUE
    elif per_val < threshold:
        return RED
    else:
        return GREEN

if __name__ == '__main__':
    #//////////////////////////////////////////////////////////////////////////
    # SECTION ONE
    #\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    #make_plots(file_parser(filepath))

    #//////////////////////////////////////////////////////////////////////////
    # SECTION TWO
    #\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    make_plots_from_file(filepath)
