#!/usr/bin/python
#
# workers.py
#
# VERSION: 1.3.1
#
# LAST EDIT: 2016-03-01
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QMutexLocker
from PyQt5.QtCore import QObject
from PyQt5.QtGui import QPixmap

try:
    from .hardware.imaging import Imaging
    from .hardware.pridaperipheral import PRIDAPeripheralError
    hardware_mode = True
except ImportError:
    hardware_mode = False
from .hdf_organizer import PridaHDF
from .utilities import get_ctime


###############################################################################
# CLASSES:
###############################################################################
class HardWorker(QObject):
    """
    Name:     HardWorker
    Features: This class is a worker object used for threading hardware.
    History:  Version 1.3.1-dev
              - created [16.01.27]
              - updated run sequence with new signals [16.01.28]
              - reduced the number of properties [16.01.28]
              - removed reset count and get progress value functions [16.01.28]
              - created end sequence function [16.02.22]
              - added new save signal [16.02.22]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    finished = pyqtSignal()           # imaging sequence complete
    running = pyqtSignal(int, tuple)  # hardware moved - update GUI
    ready = pyqtSignal(tuple)         # photo taken event - save image

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None, is_enabled=hardware_mode, mutex=None):
        """
        Name:     HardWorker.__init__
        Inputs:   [optional] Qt parent object (parent)
        Returns:  None.
        Features: Class initialization
        """
        super(HardWorker, self).__init__(parent)

        # Create a worker logger:
        self.logger = logging.getLogger(__name__)
        self.logger.debug("hardware worker object initialized")

        # Create a mutex lock
        self.mutex = mutex

        # Initialize property values:
        self.is_enabled = is_enabled

        # Debugging messages
        if is_enabled:
            self.logger.debug("hardware mode enabled")
        else:
            self.logger.debug("disabled hardware model")

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def image_number(self):
        if self.is_enabled:
            return self.imaging.num_photos
        else:
            return None

    @image_number.setter
    def image_number(self, value):
        if self.is_enabled:
            self.logger.debug("setting number of images to %d", value)
            self.imaging.num_photos = value
        else:
            self.logger.warning(
                "failed to set number of images; hardware not enabled")

    @property
    def image_pid(self):
        if self.is_enabled:
            return self.imaging.cur_pid
        else:
            return None

    @image_pid.setter
    def image_pid(self, value):
        if self.is_enabled:
            self.logger.debug("setting current image PID to %s", value)
            self.imaging.cur_pid = value
        else:
            self.logger.warning(
                "failed to set image PID; hardware not enabled")

    @property
    def camera_make(self):
        if self.is_enabled:
            return self.imaging.camera.make
        else:
            return ""

    @property
    def camera_model(self):
        if self.is_enabled:
            return self.imaging.camera.model
        else:
            return ""

    @property
    def camera_exposure(self):
        if self.is_enabled:
            return self.imaging.camera.exposure_time
        else:
            return ""

    @property
    def camera_aperture(self):
        if self.is_enabled:
            return self.imaging.camera.aperture
        else:
            return ""

    @property
    def camera_iso_speed(self):
        if self.is_enabled:
            return self.imaging.camera.iso_speed
        else:
            return ""

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def create_imaging(self, my_parser=None, config_filepath=None):
        """
        Name:     HardWorker.create_imaging
        Inputs:   - [optional] function handle (my_parser)
                  - [optional] str, configuration file path (config_filepath)
        Outputs:  None.
        Features: Creates an Imaging class instance
        """
        if self.is_enabled:
            self.logger.debug("creating imaging object")
            try:
                self.imaging = Imaging(my_parser, config_filepath)
            except PRIDAPeripheralError:
                self.logger.exception('hardware error,' +
                                      ' booting to explorer mode.')
                self.is_enabled = False
        else:
            self.logger.warning(
                "imaging cannot be created; hardware is not enabled")

    def get_maximum_value(self):
        """
        Name:     HardWorker.get_maximum_value
        Inputs:   None.
        Outputs:  None.
        Features: Returns the maximum value used for the GUI progress bar or
                  zero if hardware is not enabled
        """
        if self.is_enabled:
            max_val = int(self.imaging.find_end_pos())
            max_val += int(10*self.imaging.num_photos)
            self.logger.debug("max value calculated as %d", max_val)
        else:
            self.logger.warning(
                "cannot calculate max value; hardware not enabled")
            max_val = 0

        return max_val

    def end_sequence(self):
        """
        Name:     HardWorker.end_sequence
        Inputs:   None.
        Outputs:  None.
        Features: Stops an photo capturing sequence; the next run sequence
                  should emit the finished signal
        """
        # For 5 s, try to get the mutex lock from run_sequence:
        self.logger.debug("attempting to gather mutex lock")
        got_it = self.mutex.tryLock(timeout=5000)
        if got_it:
            self.logger.debug("lock acquired, calling end sequence")
            self.imaging.end_sequence()
        else:
            self.logger.debug("failed to acquire lock, killing")
            self.imaging.end_sequence()

    def run_sequence(self):
        """
        Name:     HardWorker.run_sequence
        Inputs:   None.
        Outputs:  None.
        Features: Run imaging sequence loop, emitting appropriate signals to
                  indicate imaging is 1) running, 2) ready, or 3) finished
        """
        if self.is_enabled:
            keep_running = True
            locked = False
            while keep_running:
                if self.mutex is not None and not locked:
                    self.mutex.lock()
                    locked = True
                self.logger.debug("running imaging sequence")
                output_args = self.imaging.run_sequence()

                progress_val = self.imaging.ms_count
                progress_val += 10*self.imaging.taken
                self.logger.debug("returning progress value %d", progress_val)

                is_path = output_args[0]
                keep_running = self.imaging.is_running

                if is_path and self.imaging.is_running:
                    self.logger.debug("imaging is ready")
                    self.running.emit(progress_val, output_args)
                    self.ready.emit(output_args)
                    if self.mutex is not None:
                        locked = False
                        self.mutex.unlock()
                elif not is_path and self.imaging.is_running:
                    self.logger.debug("imaging is running")
                    self.running.emit(progress_val, output_args)
                elif not self.imaging.is_running:
                    self.logger.debug("imaging has completed")
                    self.finished.emit()
                    if self.mutex is not None:
                        locked = False
                        self.mutex.unlock()
                else:
                    self.logger.warning("unknown state encountered, finishing")
                    self.finished.emit()
                    if self.mutex is not None:
                        locked = False
                        self.mutex.unlock()
        else:
            self.logger.warning("failed to run sequence; hardware not enabled")


class DataWorker(QObject):
    """
    Name:     DataWorker
    Features: This class is a worker object used for threading data management.
    History:  Version 1.3.1-dev
              - created [16.01.27]
              - added new properties and functions [16.01.28]
              - added session attribute [16.02.22]
              - updated create session function [16.02.22]
              - added finished signal to compress file [16.02.23]
              - created output_dir attribute [16.02.24]
              - edited how extract to file works [16.02.24]
              - created report and importing signals [16.02.25]
              - created import images function [16.02.25]
              - created stop function with abort property [16.02.25]
              - added QMutexLocker without success [16.02.25]
              - created close search file [16.03.01]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    isbusy = pyqtSignal(str)      # for starting file IO
    donebusy = pyqtSignal()       # for ending file IO
    report = pyqtSignal(str)      # for error messages to Prida app
    importing = pyqtSignal(int, tuple, bool)  # import images - update GUI
    finished = pyqtSignal()       # to end importing
    data_closed = pyqtSignal()    # signals data is closed

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None, mutex=None):
        """
        Name:     DataWorker.__init__
        Inputs:   [optional] Qt parent object (parent)
        Returns:  None.
        Features: Class initialization
        """
        super(DataWorker, self).__init__(parent)

        # Create a worker logger:
        self.logger = logging.getLogger(__name__)
        self.logger.debug("data worker object initialized")

        # Initialize variables
        self.mutex = mutex      # mutex lock
        self.session = None     # current session path
        self.output_dir = None  # where exports should be saved
        self.img_files = []     # list of images for importing
        self._abort = False     # whether to abort image saving

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def abort(self):
        return self._abort

    @abort.setter
    def abort(self, value):
        self._abort = value

    @property
    def about(self):
        return self.hdf.get_about()

    @about.setter
    def about(self, value):
        self.hdf.set_root_about(value)

    @property
    def basename(self):
        return self.hdf.basename()

    @property
    def pid_list(self):
        if self.hdf.isopen:
            return self.hdf.list_pids()
        else:
            return []

    @property
    def root_user(self):
        return str(self.hdf.get_root_user())

    @root_user.setter
    def root_user(self, value):
        self.hdf.set_root_user(value)

    @property
    def root_addr(self):
        return str(self.hdf.get_root_addr())

    @root_addr.setter
    def root_addr(self, value):
        self.hdf.set_root_addr(value)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def close_file(self):
        """
        Name:     DataWorker.close_file
        Inputs:   None.
        Outputs:  None.
        Features: Closes HDF file
        """
        self.logger.debug("closing HDF file")
        self.hdf.close()
        self.data_closed.emit()

    def close_search_file(self):
        """
        Name:     DataWorker.close_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Closes HDF file during inter-file searching
        """
        self.logger.debug("closing HDF file")
        self.hdf.close()

    def compress_file(self):
        """
        Name:     DataWorker.compress_file
        Inputs:   None.
        Outputs:  isbusy, donebusy and report signals
        Features: Creates a compressed copy of the currently opened HDF file
                  and emits the appropriate message for GUI popup
        """
        # Alert Prida that we have started compressing
        self.isbusy.emit("Compressing file...")

        err_msg = ""
        if self.hdf.isopen:
            err_code = self.hdf.compress()
            if err_code == 0:
                err_msg = "Successfully created compressed HDF file."
            elif err_code == 1:
                err_msg = ("Failed to create compressed HDF file. Check that "
                           "you have write privileges and try again.")
            elif err_code == -1:
                err_msg = ("An error was encountered during writing to the "
                           "compressed HDF file. Try re-opening this file and "
                           "running compression again.")
            elif err_code == 9999:
                err_msg = ("Failed to create compressed HDF file. The file "
                           "already exists. Please rename the existing file "
                           "and try again.")
            else:
                err_msg = ("An unknown error was encountered during "
                           "compression. Try re-opening this file and running "
                           "the compression again.")
        else:
            err_msg = ("Failed to create compressed HDF file. No open HDF "
                       "file was found. Please open an existing HDF file and "
                       "try again.")

        self.donebusy.emit()
        self.report.emit(err_msg)

    def create_hdf(self, my_parser=None, config_filepath=None):
        """
        Name:     DataWorker.create_hdf
        Inputs:   - [optional] function handle (my_parser)
                  - [optional] str, configuration file path (config_filepath)
        Outputs:  None.
        Features: Creates an PridaHDF class instance
        """
        self.logger.debug("creating HDF object")
        self.hdf = PridaHDF(my_parser, config_filepath)

    def create_session(self, pid, pid_attrs, session_attrs):
        """
        Name:     DataWorker.create_session
        Inputs:   - str, plant ID (pid)
                  - dict, plant ID metadata (pid_attrs)
                  - dict, session metadata (session_attrs)
        Outputs:  str, session path
        Features: Creates a new project session for a given PID, saves the
                  PID and session metadata and saves/returns the session path
        """
        self.session = self.hdf.create_session(pid, pid_attrs, session_attrs)
        return self.session

    def extract_to_file(self):
        """
        Name:     DataWorker.extract_to_file
        Inputs:   None.
        Outputs:  isbusy, donebusy, and report signals
        Features: Extracts datasets and metadata at and below a given HDF level
        """
        self.isbusy.emit("Extracting to file...")

        if os.path.isdir(self.output_dir):
            # Extract attribute files:
            try:
                self.logger.debug(
                    "extracting attributes to %s", self.output_dir)
                self.hdf.extract_attrs(self.output_dir)
            except IOError:
                self.logger.warning(
                    "failed to export attributes to %s", self.output_dir)
            except ValueError:
                self.logger.warning(
                    "failed to export attributes; file already exists")
            except:
                self.logger.exception("caught unknown error exception")
            else:
                self.logger.debug("attributes extraction successful")

            # Extract datasets:
            try:
                self.logger.debug("extracting datasets to %s", self.output_dir)
                self.hdf.extract_datasets(self.session, self.output_dir)
            except IOError:
                self.logger.warning("failed to extract datasets")
                err_msg = ("Failed to extract datasets to %s. "
                           "Please check output directory privileges "
                           "and try again.") % (self.output_dir)
            except:
                self.logger.exception("caught unknown error exception")
                err_msg = ("An unexpected error occurred during dataset "
                           "extraction. Please check that output "
                           "directory %s exists and that you have write "
                           "privileges.") % (self.output_dir)
            else:
                self.logger.debug("datasets extraction successful")
                err_msg = ("Successfully extracted datasets "
                           "to %s") % (self.output_dir)
        else:
            err_msg = "Failed to extract, output directory does not exist!"

        self.donebusy.emit()
        self.report.emit(err_msg)

    def get_attr(self, attr, obj_path):
        """
        Name:     DataWorker.get_attr
        Inputs:   - str, attribute name (attr)
                  - str, object path (obj_path)
        Outputs:  str, attribute value
        Features: Returns a requested attribute from a given object
        """
        return self.hdf.get_attr(attr, obj_path)

    def get_dataset(self, ds_path):
        """
        Name:     DataWorker.get_dataset
        Inputs:   str, dataset path (ds_path)
        Outputs:  ndarray, dataset
        Features: Returns a dataset (i.e., array) for a given dataset path
        """
        return self.hdf.get_dataset(ds_path)

    def get_image_session(self, img_path):
        """
        Name:     DataWorker.get_image_session
        Inputs:   str, image path (img_path)
        Outputs:  str, session path
        Features: Returns the session name for a given image path
        """
        return self.hdf.get_img_session(img_path)

    def import_images(self):
        """
        Name:     DataWorker.import_images
        Inputs:   None.
        Outputs:  None.
        Features: Imports images to current session; signals importing to Prida
                  to update the GUI and finished when complete.
        """
        if len(self.img_files) > 0:
            for file_idx in range(len(self.img_files)):
                file_path = self.img_files[file_idx]
                img = QPixmap(file_path)
                a_date = get_ctime(file_path).strftime("%Y-%m-%d")
                a_time = get_ctime(file_path).strftime("%H.%M.%S")
                my_t = (True, file_path, a_date, a_time, str(file_idx),
                        str(img.height()), str(img.width()), "0")
                try:
                    self.logger.debug("saving image %s", file_path)
                    self.save_image(my_t)
                except:
                    self.logger.exception(
                        "failed to save dataset %s", self.session)
                else:
                    self.importing.emit(file_idx, my_t, True)
                    self.logger.debug("saved image %s", file_path)
            self.finished.emit()

    def list_objects(self, obj_path):
        """
        Name:     DataWorker.list_objects
        Inputs:   str, object path (obj_path)
        Outputs:  list, member object names
        Features: Returns a list of member objects for a given object path
        """
        return self.hdf.list_objects(obj_path)

    def list_sessions(self, pid):
        """
        Name:     DataWorker.list_sessions
        Inputs:   str, plant ID (pid)
        Outputs:  list, session names
        Features: Returns a list of sessions for a given plant ID
        """
        return self.hdf.list_sessions(pid)

    def open_file(self, file_path):
        """
        Name:     DataWorker.open_file
        Inputs:   str, file path (file_path)
        Outputs:  None.
        Features: Opens a new HDF file
        """
        self.logger.debug("opening new HDF file %s", file_path)
        self.hdf.close()
        self.hdf.open_file(file_path)

    def save_file(self):
        """
        Name:     DataWorker.save_file
        Inputs:   None.
        Outputs:  None.
        Features: Saves/flushes HDF data to file
        """
        self.logger.debug("saving HDF file")
        self.hdf.save()

    def save_image(self, t):
        """
        Name:     DataWorker.save_image
        Inputs:   - tuple (t)
        Outputs:  None.
        Features: Saves photo and attributes to HDF
        """
        self.logger.debug("gathering image metadata")
        # is_path = t[0]
        img_path = t[1]
        datestamp = t[2]
        timestamp = t[3]
        anglestamp = t[4]
        image_height = t[5]
        image_width = t[6]
        orientation = t[7]
        basename = os.path.basename(img_path)
        name = os.path.splitext(basename)[0]
        a_path = os.path.join(self.session, name, basename)
        if os.name is 'nt':
            a_path = a_path.replace(os.sep, '/')

        if self.mutex is not None:
            locker = QMutexLocker(self.mutex)

        if not self.abort:
            try:
                self.logger.info("saving image %s to HDF file", img_path)
                self.hdf.save_image(self.session, img_path)
            except:
                self.logger.exception("failed to save image %s", img_path)
            else:
                try:
                    self.logger.debug(
                        "saving session %s attributes", self.session)
                    self.set_attr("Date", datestamp, a_path)
                    self.set_attr("Time", timestamp, a_path)
                    self.set_attr("Angle", anglestamp, a_path)
                    self.set_attr("Height", image_height, a_path)
                    self.set_attr("Width", image_width, a_path)
                    self.set_attr("Orientation", orientation, a_path)
                except:
                    self.logger.exception(
                        "failed to set session %s attributes", self.session)
            finally:
                self.logger.debug("flushing to HDF file")
                self.save_file()
        else:
            self.logger.debug("found abort signal!")

        if self.mutex is not None:
            locker.unlock()

    def set_attr(self, attr_name, attr_val, obj_path):
        """
        Name:     DataWorker.set_attr
        Inputs:   - str, attribute name (attr_name)
                  - str, attribute value (attr_val)
                  - str, HDF object path (obj_path)
        Outputs:  None.
        Features: Saves a value to a given attribute of a given object
        """
        self.logger.debug("setting attribute %s", attr_name)
        self.hdf.set_attr(attr_name, attr_val, obj_path)

    def stop(self):
        """
        Features: Sets abort to True
        """
        if self.mutex is not None:
            locker = QMutexLocker(self.mutex)
        self.logger.info("setting abort trigger to True")
        self.abort = True
        if self.mutex is not None:
            locker.unlock()
