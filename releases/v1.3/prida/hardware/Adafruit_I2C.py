#!/usr/bin/python
#
# Adafruit_I2C.py
#
# VERSION: 1.3.1
#
# LAST EDIT: 2016-03-01
#
###############################################################################
# ADAFRUIT PYTHON LIBRARY FOR DC + STEPPER MOTOR HAT                          #
###############################################################################
# Python library for interfacing with the Adafruit Motor HAT for Raspberry Pi #
# to control DC motors with speed control and Stepper motors with single,     #
# double, interleave and microstepping.                                       #
#                                                                             #
# Designed specifically to work with the Adafruit Motor Hat                   #
#                                                                             #
# ----> https://www.adafruit.com/product/2348                                 #
#                                                                             #
# Adafruit invests time and resources providing this open source code, please #
# support Adafruit and open-source hardware by purchasing products from       #
# Adafruit!                                                                   #
#                                                                             #
# Written by Limor Fried for Adafruit Industries.                             #
# MIT license, all text above must be included in any redistribution          #
###############################################################################
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import re
import smbus
import logging


###############################################################################
# CLASSES:
###############################################################################
class Adafruit_I2C(object):

    @staticmethod
    def getPiRevision():
        """
        Name:     Adafruit_I2C.getPiRevision
        Inputs:   None.
        Outputs:  None.
        Features: Gets the version number of the Raspberry Pi board
        """
        # Revision list available at:
        # http://elinux.org/RPi_HardwareHistory#Board_Revision_History
        try:
            with open('/proc/cpuinfo', 'r') as infile:
                for line in infile:
                    # Match a line of the form
                    # "Revision : 0002" while ignoring extra
                    # info in front of the revsion
                    # (like 1000 when the Pi was over-volted).
                    match = re.match('Revision\s+:\s+.*(\w{4})$', line)
                    if match and match.group(1) in ['0000', '0002', '0003']:
                        # Return revision 1 if
                        # revision ends with 0000, 0002 or 0003.
                        return 1
                    elif match:
                        # Assume revision 2 if
                        # revision ends with any other 4 chars.
                        return 2
                # Couldn't find the revision,
                # assume revision 0 like older code for compatibility.
                return 0
        except:
            return 0

    @staticmethod
    def getPiI2CBusNumber():
        """
        Name:     Adafruit_I2C.getPiI2CBusNumber
        Inputs:   None.
        Outputs:  None.
        Features: Gets the I2C bus number /dev/i2c
        Depends:  getPiRevision
        """
        return 1 if Adafruit_I2C.getPiRevision() > 1 else 0

    def __init__(self, address, busnum=-1, debug=False):
        self.logger = logging.getLogger(__name__)
        self.logger.debug('start.')
        self.address = address
        # By default, the correct I2C bus is auto-detected using /proc/cpuinfo
        # Alternatively, you can hard-code the bus version below:
        # self.bus = smbus.SMBus(0); # Force I2C0 (early 256MB Pi's)
        # self.bus = smbus.SMBus(1); # Force I2C1 (512MB Pi's)
        self.bus = smbus.SMBus(
            busnum if busnum >= 0 else Adafruit_I2C.getPiI2CBusNumber())
        self.debug = debug
        self.logger.debug('complete.')

    def reverseByteOrder(self, data):
        """
        Name:     Adafruit_I2C.reverseByteOrder
        Input:    int | long (data)
        Output:   int | long (val)
        Features: Reverses the byte order of an int (16-bit)
                  or long (32-bit) value
        """
        self.logger.debug('called')
        # Courtesy Vishal Sapre
        byteCount = len(hex(data)[2:].replace('L', '')[::2])
        val = 0
        for i in range(byteCount):
            val = (val << 8) | (data & 0xff)
            data >>= 8
        return val

    def errMsg(self):
        self.logger.error("Error accessing 0x%02X:" +
                          " Check your I2C address" % self.address)
        return -1

    def write8(self, reg, value):
        """
        Name:     Adafruit_I2C.write8
        Inputs:   - register (reg)
                  - value (value)
        Outputs:  None.
        Features: Writes an 8-bit value to the specified register/address
        """
        try:
            self.bus.write_byte_data(self.address, reg, value)
            # self.logger.debug("I2C: Wrote 0x%02X to register 0x%02X"
            #                   % (value, reg))
        except IOError:
            return self.errMsg()

    def write16(self, reg, value):
        """
        Name:     Adafruit_I2C.write16
        Inputs:   - register (reg)
                  - value (value)
        Outputs:  None.
        Features: Writes a 16-bit value to the specified register/address pair
        """
        try:
            self.bus.write_word_data(self.address, reg, value)
            # self.logger.debug((("I2C: Wrote 0x%02X to register pair" +
            #                     " 0x%02X,0x%02X") % (value, reg, reg + 1)))
        except IOError:
            return self.errMsg()

    def writeRaw8(self, value):
        """
        Name:     Adafruit_I2C.writeRaw8
        Inputs:   value
        Outputs:  None.
        Features: Writes an 8-bit value on the bus
        """
        try:
            self.bus.write_byte(self.address, value)
            # self.logger.debug("I2C: Wrote 0x%02X" % value)
        except IOError:
            return self.errMsg()

    def writeList(self, reg, list):
        """
        Name:     Adafruit_I2C.writeList
        Features: Writes an array of bytes using I2C format
        """
        try:
            # self.logger.debug("I2C: Writing list to register 0x%02X:" % reg)
            # self.logger.debug(list)
            self.bus.write_i2c_block_data(self.address, reg, list)
        except IOError:
            return self.errMsg()

    def readList(self, reg, length):
        """
        Name:     Adafruit_I2C.readList
        Features: Read a list of bytes from the I2C device
        """
        try:
            results = self.bus.read_i2c_block_data(self.address, reg, length)
            # self.logger.debug((("I2C: Device 0x%02X returned" +
            #         " the following from reg 0x%02X") %
            #        (self.address, reg)))
            # self.logger.debug(results)
            return results
        except IOError:
            return self.errMsg()

    def readU8(self, reg):
        """
        Name:     Adafruit_I2C.readU8
        Features: Read an unsigned byte from the I2C device
        """
        try:
            result = self.bus.read_byte_data(self.address, reg)
            # self.logger.debug((("I2C: Device 0x%02X returned" +
            #                     " 0x%02X from reg 0x%02X") %
            #                     (self.address, result & 0xFF, reg)))
            return result
        except IOError:
            return self.errMsg()

    def readS8(self, reg):
        """
        Name:     Adafruit_I2C.readS8
        Features: Reads a signed byte from the I2C device
        """
        try:
            result = self.bus.read_byte_data(self.address, reg)
            if result > 127:
                result -= 256
            # self.logger.debug(("I2C: Device 0x%02X returned" +
            #                   " 0x%02X from reg 0x%02X" %
            #                  (self.address, result & 0xFF, reg)))
            return result
        except IOError:
            return self.errMsg()

    def readU16(self, reg, little_endian=True):
        """
        Name:     Adafruit_I2C.readU16
        Features: Reads an unsigned 16-bit value from the I2C device
        """
        try:
            result = self.bus.read_word_data(self.address, reg)
            # Swap bytes if using big endian
            # because read_word_data assumes little
            # endian on ARM (little endian) systems.
            if not little_endian:
                result = ((result << 8) & 0xFF00) + (result >> 8)
            # self.logger.debug(("I2C: Device 0x%02X returned" +
            #       " 0x%04X from reg 0x%02X") %
            #      (self.address, result & 0xFFFF, reg))
            return result
        except IOError:
            return self.errMsg()

    def readS16(self, reg, little_endian=True):
        """
        Name:     Adafruit_I2C.readS16
        Features: Reads a signed 16-bit value from the I2C device
        """
        # self.logger.debug('start.')
        try:
            result = self.readU16(reg, little_endian)
            if result > 32767:
                result -= 65536
            # self.logger.debug('complete.')
            return result
        except IOError:
            return self.errMsg()


###############################################################################
# MAIN:
###############################################################################
if __name__ == '__main__':
    try:
        bus = Adafruit_I2C(address=0)
        logging.info("Adafruit_I2C:Default I2C bus is accessible")
    except:
        logging.warning("Adafruit_I2C:Error accessing default I2C bus")
