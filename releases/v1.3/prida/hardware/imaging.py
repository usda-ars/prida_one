#!/usr/bin/python
#
# imaging.py
#
# VERSION: 1.3.1
#
# LAST EDIT: 2016-03-01
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
#
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import time
import atexit
import datetime
import logging
import os

from .hat import HAT
from .camera import Camera
from .led import LED
from .piezo import Piezo
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# FUNCTIONS:
###############################################################################
def exit_sequence(my_imaging):
    """
    Name:    Imaging.exit_sequence
    Feature: Disables all hardware before application close
    Inputs:  Imaging, the instance of Imaging to cleanup (my_imaging)
    Outputs: None
    """
    logging.info('start.')
    my_imaging.hat.turn_off_motors()
    my_imaging.led.all_off()
    logging.info('complete.')


###############################################################################
# CLASSES:
###############################################################################
class Imaging(object):
    """
    Aggregate imaging hardware class for PRIDA.
    """
    _hat = None     # Attribute name for 'static' object controlling the hat
    _camera = None  # Attribute name for 'static' object controlling the camera
    _led = None     # Attribute name for 'static' object controlling the led
    _piezo = None   # Attribute name for 'static' object controlling the piezo

    def __init__(self, my_parser=None, config_filepath=None):
        """
        Initializes hardware states.
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('Instantiating imaging.')
        if Imaging.__dict__['_hat'] is None:
            setattr(Imaging, '_hat', HAT(my_parser, config_filepath))
        if Imaging.__dict__['_camera'] is None:
            setattr(Imaging, '_camera', Camera(my_parser, config_filepath))
        if Imaging.__dict__['_led'] is None:
            setattr(Imaging, '_led', LED(my_parser, config_filepath))
        if Imaging.__dict__['_piezo'] is None:
            setattr(Imaging, '_piezo', Piezo(my_parser, config_filepath))
        self._num_photos = 1         # Total to collect
        self._taken = 0              # Number of current photo
        self._position = 0           # Current position of the motor in degrees
        self._is_running = False     # indicates if system is mid DAQ
        self._is_error = False       # indicates system error
        self._ms_count = 0
        self._gear_ratio = 1         # 4.9 for use with RM101 setup
        self._led.green_on()         # led indicates ready for DAQ
        self.hat._sm._build_microstep_curve()
        self._ms_end_pos = int(self.hat.microsteps *
                               self.hat.step_res * self.gear_ratio)
        self._cur_pid = None
        self.attr_dict = {'GEAR_RATIO': 'gear_ratio',
                          }
        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('Imaging instantiated.')
        atexit.register(exit_sequence, self)

    @property
    def ms_end_pos(self):
        """
        Name:    Imaging.ms_end_pos
        Feature: Returns the end position of the motor for the current sequence
                 in number of microsteps
        Inputs:  None
        Outputs: Int, end position in microsteps (self._ms_end_pos)
        """
        return self._ms_end_pos

    @ms_end_pos.setter
    def ms_end_pos(self, value):
        """
        Name:    Imaging.ms_end_pos
        Feature: Forbids setting the end position of the motor for the current
                 sequence in number of microsteps
        Inputs:  dtype, user value specified for assignment (value)
        Outputs: None
        """
        self.logger.error(
            'setting the microstep end position manualy is forbidden.')
        raise AttributeError(
            'setting the microstep end position manualy is forbidden.')

    @property
    def cur_pid(self):
        """
        Name:    Imaging.cur_pid
        Feature: Returns the current pid
        Inputs:  None
        Outputs: Str, the current PID (self._cur_pid)
        """
        return self._cur_pid

    @cur_pid.setter
    def cur_pid(self, value):
        """
        Name:    Imaging.cur_pid
        Feature: Sets the current PID
        Inputs:  Str, the new PID (value)
        Outputs: None
        """
        if isinstance(value, str):
            self._cur_pid = value
        else:
            self.logger.error('current pid must be a string.')
            raise TypeError('current pid must be a string.')

    @property
    def ms_count(self):
        """
        Name:    Imaging.ms_count
        Feature: Returns the current microstep position
        Inputs:  None
        Outputs: Int, current motor position in microsteps (self._ms_count)
        """
        return self._ms_count

    @ms_count.setter
    def ms_count(self, value):
        """
        Name:    Imaging.ms_count
        Feature: sets the current microstep position
        Inputs:  Int, the new position in microsteps (value)
        Outputs: None
        """
        if isinstance(value, int):
            if value >= 0:
                self._ms_count = value
            else:
                self.logger.error(
                    'microstep count must be a positive interger value.'
                    )
                raise ValueError(
                    'microstep count must be a positive interger value.'
                    )
        else:
            self.logger.error('microstep count must be an integer.')
            raise TypeError('microstep count must be an integer.')

    @property
    def gear_ratio(self):
        """
        Name:    Imaging.gear_ratio
        Feature: Returns the setup's gear ratio
        Inputs:  None
        Outputs: Float, the gear ratio (self._gear_ratio)
        """
        return self._gear_ratio

    @gear_ratio.setter
    def gear_ratio(self, value):
        """
        Name:    Imaging.gear_ratio
        Feature: Sets the system's gear ratio
        Inputs:  Float, the new gear ratio (value)
        Outputs: None
        """
        if isinstance(value, float) or isinstance(value, int):
            if value >= 0:
                self._gear_ratio = value
            else:
                self.logger.error('gear ratio must be a positive value.')
                raise ValueError('gear ratio must be a positive value.')
        else:
            self.logger.error('gear ratio must be a numeric value.')
            raise TypeError('gear ratio must be a numeric value.')

    @property
    def hat(self):
        """
        Name:    Imaging.hat
        Feature: Return _hat data member (getter)
        Inputs:  None
        Outputs: HAT, an Adafruit_MotorHAT abstraction (self._hat)
        """
        return self._hat

    @hat.setter
    def hat(self, value):
        """
        Name:    Imaging.hat
        Feature: _hat attribute setter
        Inputs:  HAT, an Adafruit_MotorHAT abstraction (value)
        Outputs: None
        """
        if isinstance(value, HAT()):
            self._hat = value
        else:
            self.logger.error('_hat attribute must be a HAT object.')
            raise TypeError('_hat attribute must be a HAT object.')

    @property
    def camera(self):
        """
        Name:    Imaging.camera
        Feature: Return camera data member
        Inputs:  None
        Outputs: Camera, a gphoto2 camera object abstraction
                 (self._camera)
        """
        return self._camera

    @camera.setter
    def camera(self, value):
        """
        Name:    Imaging.camera
        Feature: _camera attribute setter
        Inputs:  Camera, an gphoto2 camera object abstraction (value)
        Outputs: None
        """
        if isinstance(value, Camera()):
            self._camera = value
        else:
            self.logger.error('_camera attribute must be a Camera' +
                              ' object.')
            raise TypeError('_camera attribute must be a Camera object.')

    @property
    def led(self):
        """
        Name:    Imaging.led
        Feature: Return led data member
        Inputs:  None
        Outputs: LED, object representing a multi-colored LED that
                 inherits from the Adafruit_MotorHAT class (self._led)
        """
        return self._led

    @led.setter
    def led(self, value):
        """
        Name:    Imaging.led
        Feature: _led attribute setter
        Inputs:  LED, object representing a multi-colored LED that
                 inherits from the Adafruit_MotorHAT class (value)
        Outputs: None
        """
        if isinstance(value, LED()):
            self._led = value
        else:
            self.logger.error('_led attribute must be a LED object.')
            raise TypeError('_led attribute must be a LED object.')

    @property
    def piezo(self):
        """
        Name:    Imaging.piezo
        Feature: Return piezo data member
        Inputs:  None
        Outputs: Piezo, object representing a piezo-buzzer that inherits
                 from the Adafruit_MotorHAT class (self._piezo)
        """
        return self._piezo

    @piezo.setter
    def piezo(self, value):
        """
        Name:    Imaging.piezo
        Feature: _piezo attribute setter
        Inputs:  Piezo, object representing a piezo-buzzer that
                 inherits from the Adafruit_MotorHAT class (value)
        Outputs: None
        """
        if isinstance(value, Piezo()):
            self._piezo = value
        else:
            self.logger.error('_piezo attribute must be a Piezo object.')
            raise TypeError('_piezo attribute must be a Piezo object.')

    @property
    def is_running(self):
        """
        Name:    Imaging.is_running
        Feature: Return is_running data member
        Inputs:  None
        Outputs: Boolean, value representing system state, indicating
                 whether or not the system is currently in a imaging
                 sequence (self._is_running)
        """
        return self._is_running

    @property
    def is_error(self):
        """
        Name:    Imaging.is_error
        Feature: Return is_error data member
        Inputs:  None
        Outputs: Boolean, value representing system state, indicating
                 whether or not the hardware controls have encountered
                 an error (self._is_error)
        """
        return self._is_error

    @property
    def num_photos(self):
        """
        Name:    Imaging.num_photos
        Feature: Get the number of photos in a single expiriment.
        Inputs:  None
        Outputs: int, number of photos to be taken (self._num_photos)
        """
        return self._num_photos

    @property
    def taken(self):
        """
        Name:    Imaging.taken
        Feature: Get the number of photos in a single expiriment.
        Inputs:  None
        Outputs: int, number of photos that have been taken (self._taken)
        """
        return self._taken

    @is_running.setter
    def is_running(self, state):
        """
        Name:    Imaging.is_running
        Feature: Set is_running data member to boolean value.
        Inputs:  Boolean, desired system state (state)
        Outputs: None
        """
        if isinstance(state, bool):
            self._is_running = state
        else:
            self.logger.error('_is_running must be a boolean.')
            raise TypeError('_is_running must be a boolean.')

    @is_error.setter
    def is_error(self, state):
        """
        Name:    Imaging.is_error
        Feature: Set is_error data member to boolean value.
        Inputs:  Boolean, desired system state (state)
        Outputs: None
        """
        if isinstance(state, bool):
            self._is_error = state
        else:
            self.logger.error('_is_error must be a boolean.')
            raise TypeError('_is_error must be a boolean.')

    @num_photos.setter
    def num_photos(self, value):
        """
        Name:    Imaging.num_photos
        Feature: Set the number of photos in a single expiriment.
        Inputs:  int, number of photos to be taken (value)
        Outputs: None
        """
        if isinstance(value, int):
            if value > 0:
                self._num_photos = value
            else:
                self.logger.error('Number of photos must be a positive value.')
                raise ValueError('Number of photos must be a positive value.')
        else:
            self.logger.error('Number of photos must be a integer.')
            raise TypeError('Number of photos must be a integer.')

    @taken.setter
    def taken(self, value):
        """
        Name:    Imaging.taken
        Feature: Set the number of photos in a single expiriment.
        Inputs:  int, number of photos that have been taken (value)
        Outputs: None
        """
        if isinstance(value, int):
            if value >= 0:
                self._taken = value
            else:
                self.logger.error(
                    'Number of photos taken may not be negative.')
                raise ValueError('Number of photos taken may not be negative.')
        else:
            self.logger.error(
                'Number of photos taken must be an integer value.')
            raise TypeError('Number of photos taken must be an integer value.')

    @property
    def position(self):
        """
        Name:    Imaging.position
        Feature: Indicates the current position of the motor during the imaging
                 sequence.
        Inputs:  None
        Outputs: Float, current motor position in degrees
        """
        return self._position

    @position.setter
    def position(self, value):
        """
        Name:    Imaging.position
        Feature: Updates the current position of the motor during the imaging
                 sequence.
        Inputs:  Float, updated motor position value
        Outputs: None
        """
        if isinstance(value, int) or isinstance(value, float):
            if value >= 0:
                self._position = (value % 360)
            else:
                self.logger.error(
                    'Motor position cannot be a negative number.')
                raise ValueError('Motor position cannot be a negative number.')
        else:
            self.logger.error('Motor position must be a number [degrees].')
            raise TypeError('Motor position must be a number [degrees].')

    def reset_count(self):
        """
        Name:    Imaging.reset_count
        Feature: Resets the number of images taken counter
        Inputs:  None
        Outputs: None
        """
        self.taken = 0

    def reset_position(self):
        """
        Name:    Imaging.reset_position
        Feature: Resets the position indicators values
        Inputs:  None
        Outputs: None
        """
        self.position = 0
        self.ms_count = 0

    def reset(self):
        """
        Name:    Imaging.reset
        Feature: Resets the imaging hardware
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('Resetting all counters...')
        self.reset_count()
        self.reset_position()
        self.logger.debug('Turning off the motor(s)...')
        self.hat.turn_off_motors()
        self.check_status()
        self.logger.debug('complete.')

    def run_sequence(self):
        """
        Name:    Imaging.run_sequence
        Feature: Attempts to determine where in the sequence the system is,
                 and then either step the motor, take an image, or end the
                 sequence as appropriate.
        Inputs:  None
        Outputs: tuple(bool, string, string, string, string), is the returned
                 filename a valid filename and the filename of the image
                 (is_filename, filename, date, time, angle)
        """
        if self.is_error and self.is_running:
            self.logger.exception('Error detected! ' +
                                  'Please address and try again.')
            self.end_sequence()
            raise PRIDAPeripheralError(('Error detected! ' +
                                        'Please address and try again.'))
        elif self.is_error:
            try:
                self._check_error()
            except:
                self.logger.exception('Error not resolved! ' +
                                      'Please address and try again.')
                self.end_sequence()
                raise PRIDAPeripheralError(('Error not resolved! ' +
                                            'Please address and try again.'))
            else:
                self.is_error = False
                self.logger.info('Error resolved, beginning sequence.')
        if not self.is_running:
            self.hat.calibrate()
            self.is_running = True
            self.check_status()
        if (self.taken < self.num_photos and
                self.ms_count < self.find_next_pos()):
            try:
                self.step_motor()
            except:
                self.is_error = True
                self.check_status()
                self.logger.exception('Motor Error!')
            else:
                return (False, '', '', '', '', '', '', '')
        elif ((self.taken < self.num_photos) and
              (self.ms_count >= self.find_next_pos())):
            try:
                filename = self.take_photo()
                str_args = filename.split('_')
                str_args[3] = os.path.splitext(str_args[3])[0]
                #if self.taken == (self.num_photos - 1):
                    #self.piezo.beep()
                height = self.camera.image_height
                width = self.camera.image_width
                orientation = self.camera.orientation
            except:
                self.is_error = True
                self.check_status()
                self.logger.exception('Camera Error!')
            else:
                return (True,
                        filename,
                        str_args[1],
                        str_args[2],
                        str_args[3],
                        height,
                        width,
                        orientation)
        else:
            self.end_sequence()
            return (False, '', '', '', '', '', '', '')

    def end_sequence(self):
        """
        Name:    Imaging.end_sequence
        Feature: Tries stopping the system.
                 If successful, turn off the motors and indicate ready
                 status. If it fails, indicate error status.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start cleanup')
        if self.is_running:
            self.is_running = False
        try:
            self.reset()
            self.hat.turn_off_motors()
        except:
            self.logger.exception('unexpected hardware error!')
            self.is_error = True
        finally:
            if not self.is_error:
                for i in range(3):
                    self.piezo.beep()
                    time.sleep(0.5)
            else:
                self.check_status()
                self.piezo.beep(3, 1, 1)
            self.check_status()
            self.logger.info('imaging sequence complete.')

    def check_status(self):
        """
        Name:    Imaging.check_status
        Feature: Sets the LED status indicator to the appropriate color.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('detecting system status')
        if self.is_error:
            self.led.yellow_on()
        elif self.is_running:
            self.led.red_on()
        else:
            self.led.green_on()
        self.logger.debug('adjusted led')

    def _check_error(self):
        """
        Name:    Imaging._check_error
        Feature: Checks if an error is still present in the system
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        if self.is_error is True:
            self.logger.info('attempting to determine origin...')
            try:
                self.hat.calibrate()
                self.hat.turn_off_motors()
            except:
                self.logger.error('cannot communicate with motor.')
                raise PRIDAPeripheralError('Motor problem!')
            try:
                self.camera.connect()
                self.camera.capture('test')
            except:
                self.logger.error('cannot communicate with camera.')
                raise PRIDAPeripheralError('Camera problem!')
            else:
                self.logger.info('no hardware errors detected.')
                self.is_error = False
                self.check_status()
            self.logger.debug('complete.')

    def find_next_pos(self):
        """
        Name:    Imaging.find_next_pos
        Feature: Calculates the next position value at which the camera must
                 take a photo
        Inputs:  None
        Outputs: Int, position value in number of microsteps
        """
        pos_list = [i * (360.0 / self.num_photos)
                    for i in range(self.num_photos)]
        unit_step = 360.0 / self.hat.step_res
        for index in range(len(pos_list)):
            pos_b = pos_list[index]
            pos_m = (pos_b * self.gear_ratio)
            num_unit_steps = int((pos_m) / unit_step)
            min_angle = num_unit_steps * unit_step
            remaining_dist = pos_m - min_angle
            take_step = int((remaining_dist / unit_step) + 0.5)
            if take_step:
                pos_list[index] = int((num_unit_steps + 1)
                                      * self.hat.microsteps)
            else:
                pos_list[index] = int((num_unit_steps) * self.hat.microsteps)
        res = pos_list[self.taken]
        return res

    def find_end_pos(self):
        """
        Name:    Imaging.find_end_pos
        Feature: Calculates the end position value at which the camera must
                 take a photo (in microsteps)
        Inputs:  None
        Outputs: Int, position value in number of microsteps
        """
        pos_b = 360.0 * (1.0 - (1.0 / self.num_photos))
        unit_step = 360.0 / self.hat.step_res
        pos_m = (pos_b * self.gear_ratio)
        num_unit_steps = int((pos_m) / unit_step)
        min_angle = num_unit_steps * unit_step
        remaining_dist = pos_m - min_angle
        take_step = int((remaining_dist / unit_step) + 0.5)
        if take_step:
            tmp = int((num_unit_steps + 1) * self.hat.microsteps)
        else:
            tmp = int((num_unit_steps) * self.hat.microsteps)
        return tmp

    def step_motor(self):
        """
        Name:    Imaging.step_motor
        Feature: Indexes the motor a single microstep,
                 updates the current position and number of microsteps.
        Inputs:  None
        Outputs: None
        """
        self.hat.single_step()
        self.position += (1.0 / self.gear_ratio) * (
            360.0 / (self.hat.step_res * self.hat.microsteps))
        self.ms_count += 1

    def take_photo(self):
        """
        Name:    Imaging.take_photo
        Feature: Captures a photo, updates the number of photos taken and
                 returns the image filename.
        Inputs:  None
        Outputs: Str, captured image file name (filename)
        """
        self.logger.debug('start.')
        name = '_'.join([self.cur_pid,
                         datetime.datetime.now().strftime('%Y-%m-%d_%H.%M.%S'),
                         '%06.2f' % self.position])
        self.logger.info('photo name - %s' % (name))
        filename = self.camera.capture(name, verbose=False)
        self.taken += 1
        self.logger.debug('complete.')
        return filename
