#!/usr/bin/python
#
# Prida.py
#
# VERSION: 1.3.1-r1
#
# LAST EDIT: 2016-03-15
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import glob
import logging
import logging.handlers
import os
import sys

import numpy
import scipy.misc
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QAbstractItemView
from PyQt5.QtWidgets import QCompleter
from PyQt5.QtWidgets import QErrorMessage
from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QMovie
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QStringListModel
from PyQt5.QtCore import QThread
from PyQt5.QtCore import QSize
from PyQt5.QtCore import QDate
from PyQt5.QtCore import QMutex
from PyQt5.QtCore import Qt
import PIL.ImageQt as ImageQt
import PIL.Image as Image

from . import __version__
from .utilities import get_ctime
from .utilities import resource_path
from .utilities import read_exif_tags
from .workers import DataWorker
from .workers import HardWorker
from .mainwindow import Ui_MainWindow
from .about import Ui_About
from .input_user import Ui_Dialog


###############################################################################
# GLOBAL VARIABLES:
###############################################################################
# PAGE NUMBER ENUMS FOR mainwindow.ui
MAIN_MENU = 0
VIEWER = 1
INPUT_EXP = 2
RUN_EXP = 3
FILE_SEARCH = 4
ANALYSIS = 5

# PAGE NUMBER ENUMS FOR SHEET/IDA
SHEET_VIEW = 0
IDA_VIEW = 1


###############################################################################
# FUNCTIONS:
###############################################################################
def exit_app(my_app):
    """
    Name:     exit_app
    Inputs:   object, QApplication (my_app)
    Outputs:  None
    Features: Graceful exiting of QApplication
    """
    logging.info('exiting QApplication')
    my_app.shutdown()


###############################################################################
# CLASSES:
###############################################################################
class Prida(QApplication):
    """
    Prida, a QApplication.

    Name:      Prida
    History:   Version 1.3.1-r1
               - moved view change in import session [16.01.20]
               - added USDA PLANTS and image orient. metadata fields [16.01.20]
               - created usda_plants dict [16.01.20]
               - created build plants db function [16.01.20]
               - added USDA PLANTS auto-completion model [16.01.20]
               - updated the update dict auto model function [16.01.20]
               - connected autofill functions for dict and plants [16.01.20]
               - image orientation handled via combo box [16.01.20]
               - added checks for img orient None [16.01.21]
               - added checks for empty dataset arrays [16.01.21]
               - changed greeter to IDA [16.01.21]
               - created img types tuple for data import [16.01.21]
               - fixed image naming to handle tif files [16.01.21]
               - fixed if else statement in show ida [16.01.26]
               - created end progress and update and save functions [16.01.27]
               - added worker objects [16.01.27]
               - updated update progress function [16.01.27]
               - hardware mode is now Prida class variable [16.01.28]
               - session path is now a Prida class variable [16.01.28]
               - created shutdown function [16.01.28]
               - version number is now a Prida class variable [16.01.28]
               - doc strings updated [16.01.28]
               - created a mutex lock for worker classes [16.02.18]
               - removed TimeCriticalPriority from hardware thread [16.02.18]
               - connected new abort button to end sequence [16.02.22]
               - created data thread [16.02.22]
               - compress hdf now captures data's finished signal [16.02.23]
               - created two new QLabels for spinner [16.02.23]
               - created spinner start and stop functions [16.02.23]
               - created new QErrorMessage for exporting to file [16.02.23]
               - threaded export to file function [16.02.24]
               - checking for greeter and loading images [16.02.25]
               - new threaded import data feature [16.02.25]
               - fixed back to sheet in import session [16.02.25]
               - added default value to exclude field [16.02.25]
               - added PID reset to set defaults [16.02.25]
               - added pyqt signals [16.02.26]
               - added data close search file [16.03.01]
               - improved meta data handling [16.03.15]
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    # dictionaries
    pid_attrs = {}
    session_attrs = {}
    usda_plants = {}

    # signals
    close_data = pyqtSignal()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, dict_filepath, my_parser=None, config_filepath=None):
        """
        Prida class initialization.

        Name:     Prida.__init__
        Inputs:   None.
        Outputs:  None.
        Depends:  - load_session_sheet_headers     - new_hdf
                  - new_session                    - open_hdf
                  - resize_sheet                   - show_ida
                  - to_sheet                       - update_progress
                  - update_session_sheet_model     - update_sheet_model
                  - set_defaults                   - about
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # Create a logger for Prida:
        self.logger = logging.getLogger(__name__)

        # Create a class error message
        self.logger.debug("initializing QErrorMessage")
        self.error = QErrorMessage()

        # Create the Qt mutex
        self.mutex = QMutex()

        # Create worker objects:
        self.logger.debug("creating data worker")
        self.data = DataWorker(mutex=self.mutex)
        self.data.create_hdf(my_parser, config_filepath)

        self.logger.debug("creating data thread")
        self.data_thread = QThread()
        self.data.moveToThread(self.data_thread)
        self.data_thread.start()

        self.logger.debug("creating hardware worker")
        self.hardware = HardWorker(mutex=self.mutex)

        # Defining analysis mode
        self.analysis_mode = True

        # Initialize PID and session attribute dictionaries:
        self.logger.debug("initializing PID and session attributes")
        self.init_pid_attrs()
        self.init_session_attrs()
        self.sheet_items = len(self.pid_attrs)
        self.session_sheet_items = len(self.session_attrs)

        # NOTE: mainwindow.ui depends on class definitions from custom.py
        self.main_ui = Ui_MainWindow()
        self.user_ui = Ui_Dialog()
        self.about_ui = Ui_About()
        self.gs_dict = dict_filepath

        # Load main window GUI to the QMainWindow:
        self.logger.debug("loading the main UI file")
        self.main_ui.setupUi(self.base)
        self.view = self.main_ui

        # Check that the necessary image files exist:
        self.logger.debug("checking for required resources")
        if os.path.isfile(resource_path("greeter.jpg")):
            self.greeter = resource_path("greeter.jpg")
            self.logger.debug("setting the greeter image")
            self.view.welcome.setBaseImage(scipy.misc.imread(self.greeter))
        else:
            self.logger.warning("greeter JPG not found")

        if os.path.isfile(resource_path("loading.gif")):
            self.spinner = resource_path("loading.gif")
        else:
            self.logger.warning("loading GIF not found")
            self.spinner = None
        self.movie = QMovie(self.spinner)

        # Create the VIEWER sheet model (where PIDs and sessions are listed)
        self.logger.debug("creating sheet model")
        self.sheet_model = QStandardItemModel()
        self.view.sheet.setModel(self.sheet_model)
        self.view.sheet.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.sheet.collapsed.connect(lambda: self.resize_sheet())
        self.view.sheet.expanded.connect(lambda: self.resize_sheet())
        self.view.sheet.setAnimated(True)

        # Create the VIEWER session sheet model (for session properties)
        self.logger.debug("creating session sheet model")
        self.session_sheet_model = QStandardItemModel()
        self.view.session_sheet.setModel(self.session_sheet_model)
        self.view.session_sheet.setRootIsDecorated(False)
        self.load_session_sheet_headers()

        # Create FILE_SEARCH sheet model (where files + PIDs are listed)
        self.logger.debug("creating search sheet model")
        self.search_sheet_model = QStandardItemModel()
        self.view.file_search_sheet.setModel(self.search_sheet_model)
        self.load_file_search_sheet_headers()

        # Create the FILE_SEARCH file browse model (for displaying files)
        self.logger.debug("creating file sheet model")
        self.file_sheet_model = QStandardItemModel()
        self.view.file_browse_sheet.setModel(self.file_sheet_model)
        self.load_file_browse_sheet_headers()

        # Create VIEWER image sheet model (for thumbnail previewer):
        self.logger.debug("creating image sheet model")
        self.img_sheet_model = QStandardItemModel()
        self.view.img_select.setModel(self.img_sheet_model)
        self.view.img_select.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.img_select.setIconSize(QSize(100, 100))

        # Turn off the RUN_EXP abort button (TODO in v1.4)
        self.view.abortButton.setEnabled(False)

        # Determine if safe to boot into hardware mode:
        if self.hardware.is_enabled:
            self.logger.debug("creating hardware's imaging")
            self.hardware.create_imaging(my_parser, config_filepath)

        self.hardware_mode = self.hardware.is_enabled
        self.logger.info("Hardware Mode == %s", self.hardware_mode)

        # Set HARDWARE MODE options:
        if self.hardware_mode:
            # Hardware Mode Decorator:
            self.prida_title = "Prida %s" % (__version__)
            self.base.setWindowTitle(self.prida_title)
            self.logger.warning("Hardware Mode Enabled")

            # Spawn a hardware thread if mode is enabled:
            self.logger.debug("creating hardware thread")
            self.hardware_thread = QThread()
            self.hardware_thread.started.connect(self.hardware.run_sequence)
            self.hardware_thread.finished.connect(self.end_progress)

            self.hardware.finished.connect(self.hardware_thread.quit)
            self.hardware.running.connect(self.update_progress)
            self.hardware.ready.connect(self.data.save_image)
            self.hardware.moveToThread(self.hardware_thread)

            # Disable "Import Data" button (only allow "Create Session")
            # and set VIEWER action for "OK" and "Create Session" buttons:
            self.view.import_data.setEnabled(False)
            self.view.c_buttons.accepted.connect(self.new_session)
            self.view.create_session.clicked.connect(
                lambda: self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
            )

            # Set default values based on imaging's camera
            self.session_attrs[8]["def_val"] = self.hardware.camera_make
            self.session_attrs[9]["def_val"] = self.hardware.camera_model
            self.session_attrs[10]["def_val"] = self.hardware.camera_exposure
            self.session_attrs[11]["def_val"] = self.hardware.camera_aperture
            self.session_attrs[12]["def_val"] = self.hardware.camera_iso_speed
        else:
            # Explorer Mode Decorator:
            self.prida_title = "Prida %s - Explorer Mode" % (__version__)
            self.base.setWindowTitle(self.prida_title)
            self.logger.warning("Preview Mode Enabled")

            # Disable "Create Session" button, set VIEWER action for
            # "Import Data" button & INPUT_EXP action for "OK" button,
            # import data to file dialog and connections:
            self.view.create_session.setEnabled(False)
            self.view.import_data.clicked.connect(self.import_data)
            self.view.c_buttons.accepted.connect(self.import_session)
            self.import_file_dialog = QErrorMessage()
            self.import_file_dialog.accepted.connect(self.data.import_images)
            self.data.importing.connect(self.update_progress)
            self.data.finished.connect(self.end_progress)

        # Set ANALYSIS MODE options:
        if self.analysis_mode:
            # Connect analysis button:
            self.logger.info("Analysis Mode Enabled")
            self.view.perform_analysis.clicked.connect(self.analysis)

            # Create ANALYSIS session sheet:
            self.logger.debug("creating analysis sheet model")
            self.analysis_sheet_model = QStandardItemModel()
            self.view.a_session.setModel(self.analysis_sheet_model)
            self.view.a_session.setEditTriggers(
                QAbstractItemView.NoEditTriggers)
            self.load_analysis_sheet_headers()

            # Creating ANALYSIS session sheet selection model:
            self.analysis_select_model = self.view.a_session.selectionModel()
            self.analysis_select_model.selectionChanged.connect(
                self.update_thumb_sheet_model
            )

            # Create ANALYSIS thumb sheet:
            self.logger.debug("creating analysis thumb sheet model")
            self.thumb_sheet_model = QStandardItemModel()
            self.view.a_thumb.setModel(self.thumb_sheet_model)
            self.view.a_thumb.setEditTriggers(QAbstractItemView.NoEditTriggers)
            self.view.a_thumb.setIconSize(QSize(125, 125))

            # Creating ANALYSIS thumb sheet selection model:
            self.thumb_select_model = self.view.a_thumb.selectionModel()
            self.thumb_select_model.selectionChanged.connect(self.show_preview)

            # Set ANALYSIS actions:
            self.view.a_menu.clicked.connect(self.back_to_menu)
            self.close_data.connect(self.data.close_file)
            self.data.data_closed.connect(self.back_to_main)
            self.view.a_about.clicked.connect(self.about)
            self.view.a_save.clicked.connect(self.back_to_sheet)
            self.view.a_undo.clicked.connect(self.analyze_undo)
            self.view.a_gray.clicked.connect(self.analyze_gray)
            self.view.a_thresh.clicked.connect(self.analyze_binary)
            self.view.a_invert.clicked.connect(self.tool_not_enabled)
            self.view.a_rLeft.clicked.connect(
                self.view.a_preview.rotateBaseImageLeft)
            self.view.a_rRight.clicked.connect(
                self.view.a_preview.rotateBaseImageRight)
            self.view.a_search.textEdited.connect(
                self.update_analysis_sheet_model)
        else:
            self.logger.warning("Analysis Mode Disabled")
            self.view.perform_analysis.setEnabled(False)

        # Set MAIN_MENU actions for "New," "Open," & "Search" buttons:
        self.view.new_hdf.clicked.connect(self.new_hdf)
        self.view.open_hdf.clicked.connect(self.open_hdf)
        self.view.search_hdf.clicked.connect(
            lambda: self.view.stackedWidget.setCurrentIndex(FILE_SEARCH)
        )

        # Set VIEWER actions for "Main Menu," "About," and "Edit" buttons
        # and for edited search text:
        self.view.aboutButton.clicked.connect(self.about)
        self.view.make_edit.clicked.connect(self.edit_field)
        self.view.menuButton.clicked.connect(self.back_to_menu)
        self.view.search.textEdited.connect(self.update_sheet_model)

        # Export data to file dialog and connections:
        self.export_file_dialog = QErrorMessage()
        self.export_file_dialog.accepted.connect(self.data.extract_to_file)
        self.view.export_data.clicked.connect(self.export)

        # Connect compression and return signal
        self.view.saveButton.clicked.connect(self.data.compress_file)
        self.data.isbusy.connect(self.start_spinner)
        self.data.donebusy.connect(self.stop_spinner)
        self.data.report.connect(self.mayday)

        # Set IDA_SHEET actions for "Back to Spreadsheet," "Rotate Left," and
        # "Rotate Right" buttons:
        self.view.to_sheet.clicked.connect(self.to_sheet)
        self.view.rotateLeft.clicked.connect(self.view.ida.rotateBaseImageLeft)
        self.view.rotateRight.clicked.connect(
            self.view.ida.rotateBaseImageRight)

        # Set INPUT_EXP action for "Cancel" button and update the image
        # orientation and exclude combo boxes:
        self.view.c_buttons.rejected.connect(self.cancel_input)
        self.update_orient_combo()
        self.update_exclude_combo()

        # Create selection models for VIEWER sheet and img_sheet and for
        # FILE_SELECT file_browser_sheet:
        self.logger.debug("creating selection models")
        self.sheet_select_model = self.view.sheet.selectionModel()
        self.img_sheet_select_model = self.view.img_select.selectionModel()
        self.file_select_model = self.view.file_browse_sheet.selectionModel()

        # Set selection model actions for selection changes:
        self.img_sheet_select_model.selectionChanged.connect(self.show_ida)
        self.sheet_select_model.selectionChanged.connect(
            self.update_session_sheet_model
        )

        # Set FILE_SEARCH actions for "Main Menu," "+/-," and "Explore" buttons
        # and action for when search text is edited:
        self.view.menuButton2.clicked.connect(self.back_to_menu)
        self.view.add_file.clicked.connect(self.add_search_file)
        self.view.rm_file.clicked.connect(self.rm_search_file)
        self.view.explore_file.clicked.connect(self.explore_file)
        self.view.f_search.textEdited.connect(self.update_file_search_sheet)

        # Create a auto-completion model for PIDs and set action for edited
        # PID text:
        self.logger.debug("creating PID auto-completion model")
        self.pid_completer = QCompleter()
        self.view.c_pid.setCompleter(self.pid_completer)
        self.pid_auto_model = QStringListModel()
        self.pid_completer.setModel(self.pid_auto_model)
        self.pid_completer.activated.connect(self.autofill_pid)

        # Create an auto-completion model for dictionary fields:
        self.logger.debug("creating dictionary auto-completion model")
        self.dict_completer = QCompleter()
        self.view.c_gen_sp.setCompleter(self.dict_completer)
        self.dict_auto_model = QStringListModel()
        self.dict_completer.setModel(self.dict_auto_model)
        self.dict_completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.dict_completer.activated.connect(self.autofill_dict)

        # Create an auto-completion model for USDA PLANTS:
        self.logger.debug("creating USDA PLANTS auto-completion model")
        self.plants_completer = QCompleter()
        self.view.c_usda_plants.setCompleter(self.plants_completer)
        self.plants_auto_model = QStringListModel()
        self.plants_completer.setModel(self.plants_auto_model)
        self.plants_completer.activated.connect(self.autofill_plants)

        # Build the USDA PLANTS database and update the auto-completion models:
        self.build_plants_db()
        self.update_dict_auto_model()

        # Initialize the search dictionaries, list of PID metadata, list of
        # thumbnail images, session create/edit boolean, input field defaults,
        # whether an image is being previewed, and the current image:
        self.search_files = {}
        self.search_data = {}
        self.ps_list = {}
        self.data_list = []
        self.img_list = []
        self.img_types = (".jpg", ".JPG", ".jpeg", ".tif", ".tiff")
        self.editing = False
        self.preview = False
        self.current_img = None        # not currently used

        # Set exit register:
        atexit.register(exit_app, self)

        # Parse configuration file and check Prida version:
        self.attr_dict = {}
        err_str = ''
        conf_ver = None
        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        elif my_parser is not None:
            try:
                my_user = os.path.expanduser('~')
                conf_file = os.path.join(my_user, 'Prida', 'prida.config')
                if os.path.isfile(conf_file):
                    with open(conf_file, 'r') as my_config_file:
                        for line in my_config_file:
                            if 'PRIDA_VERSION' in line:
                                words = line.split()
                                if len(words) == 3:
                                    conf_ver = eval(words[2])
                else:
                    err_str = ('Configuration file missing!\n'
                               'Please make sure your configuration file is'
                               ' located in your local prida directory.'
                               '\nAssuming program defaults.')
            except:
                err_str = ('Configuration file parsing error!\n'
                           'Please check that your configuration file'
                           ' follows the proper formatting and is located in'
                           ' your local prida directory.'
                           '\nAssuming program defaults.')
                self.logger.exception(err_str)
            if conf_ver is not None:
                if conf_ver < __version__:
                    err_str = ('Configuration file out of date!\n'
                               'Please make sure your configuration file is'
                               ' correctly formatted and fully up to date.\n'
                               'Assuming program defaults.')
                elif conf_ver > __version__:
                    err_str = ('Configuration file version mis-match!\n'
                               'Please update your configuration file'
                               ' to match your version of Prida (%s).\n'
                               'Assuming program defaults.' % (__version__))
                else:
                    err_str = None
        else:
            err_str = ('Configuration file parsing error.\n'
                       'Please check that your configuration file follows the'
                       ' proper formatting and is located in your local'
                       ' prida directory.\nAssuming program defaults.')

        self.base.show()
        if err_str:
            self.mayday(err_str)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def about(self):
        """
        Name:     Prida.about
        Inputs:   None.
        Outputs:  None.
        Features: Produces the file's About popup dialog box
        """
        self.logger.info("Button Clicked")
        self.logger.debug("loading UI file")
        self.about_dialog = QDialog(self.base)
        self.about_ui.setupUi(self.about_dialog)
        popup = self.about_ui

        self.logger.debug("getting about info")
        overview = self.data.about

        # Set popup values to default string if about fails:
        self.logger.debug("assigning UI fields")
        popup.author_about.setText(overview.get("Author", "Unknown"))
        popup.contact_about.setText(overview.get("Contact", "Unknown"))
        popup.plants_about.setText(overview.get("Plants", "?"))
        popup.sessions_about.setText(overview.get("Sessions", "?"))
        popup.photos_about.setText(overview.get("Photos", "?"))
        popup.summary_about.setText(overview.get("Summary", "?"))
        self.about_dialog.setWindowTitle("About: %s" % (self.data.basename))
        if self.about_dialog.exec_():
            self.about_dialog.repaint()

    def add_search_file(self):
        """
        Name:     Prida.add_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Adds user-defined HDF5 files to search files dictionary and
                  saves the files' PID attributes to search data dictionary
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.info("Button Clicked")
        self.logger.debug("requesting search files")
        items = QFileDialog.getOpenFileNames(None,
                                             "Select one or more files",
                                             os.path.expanduser("~"),
                                             "HDF5 files (*.hdf5)")
        paths = items[0]
        if paths:
            for path in paths:
                self.logger.debug("processing selection %s", path)
                f_name = os.path.basename(path)
                f_name = os.path.splitext(f_name)[0]
                f_path = os.path.dirname(path)
                if f_name not in self.search_files:
                    # Add file to file_sheet_model:
                    self.logger.debug("adding search file to list")
                    self.search_files[f_name] = f_path
                    f_list = [QStandardItem(f_name), QStandardItem(f_path)]
                    self.file_sheet_model.appendRow(f_list)

                    self.logger.debug("opening search file")
                    self.data.open_file(path)

                    # Add file's data to search_sheet_model:
                    self.logger.debug("reading search file PID attributes")
                    for pid in self.data.pid_list:
                        # Create a unique dictionary key for each file's PIDs:
                        my_key = "%s.%s" % (f_name, pid)

                        # Initialize the sheet's row data:
                        my_data = [f_name, pid]
                        for i in self.pid_attrs.keys():
                            my_attr = self.pid_attrs[i]["key"]
                            my_attr_val = self.data.get_attr(
                                my_attr, os.path.join('/', str(pid)))

                            # Append PID attribute to row:
                            my_data.append(my_attr_val)

                        # Save search file's PID attributes:
                        self.search_data[my_key] = my_data

                    self.logger.debug("closing search file")
                    self.data.close_search_file()
            self.logger.debug("clearing FILE_SEARCH search bar")
            self.view.f_search.clear()
            self.update_file_search_sheet()
            self.update_file_browse_sheet()

    def analysis(self):
        """
        Name:     Prida.analysis
        Inputs:   None.
        Outputs:  None.
        Features: Changes view to ANALYSIS
        Depends:  - update_analysis_sheet_model
        """
        self.logger.info("Button Clicked")
        self.update_analysis_sheet_model()
        self.view.a_preview.clearBaseImage()
        self.view.a_label.clear()
        self.thumb_sheet_model.clear()
        self.img_list = []
        self.view.stackedWidget.setCurrentIndex(ANALYSIS)

    def analyze_gray(self):
        """
        Name:     Prida.analyze_gray
        Inputs:   None.
        Outputs:  None.
        Features: Processes IDA base image to grayscale and sets display label
        """
        self.logger.info("Button Clicked")
        my_label = self.view.a_preview.grayscale()
        self.view.a_label.setText(my_label)

    def analyze_binary(self):
        """
        Name:     Prida.analyze_binary
        Inputs:   None.
        Outputs:  None.
        Features: Processes IDA base image to binary and sets display label
        """
        self.logger.info("Button Clicked")
        my_label = self.view.a_preview.binary()
        self.view.a_label.setText(my_label)

    def analyze_undo(self):
        """
        Name:     Prida.analyze_undo
        Inputs:   None.
        Outputs:  None.
        Features: Resets the preview image to original
        """
        self.view.a_preview.resetBaseImage()
        self.view.a_label.setText("original image")

    def autofill_dict(self, value=''):
        """
        Name:     Prida.autofill_dict
        Inputs:   [optional] str, Genus species (value)
        Outputs:  None.
        Features: Autofills the USDA PLANTS ID for a given Genus sp.
        Depends:  set_input_field
        """
        for k, v in self.usda_plants.items():
            if value == v:
                try:
                    self.set_input_field("c_usda_plants", "QLineEdit", str(k))
                except:
                    self.logger.exception(
                        "could not set QLineEdit c_usda_plants")

    def autofill_pid(self, pid=''):
        """
        Name:     Prida.autofill_pid
        Inputs:   [optional] str, plant ID (pid)
        Outputs:  None.
        Features: Autofills the PID metadata if PID exists
        Depends:  set_input_field
        """
        if pid in self.data.pid_list:
            self.logger.debug("performing auto-completion")
            p_path = "/%s" % (pid)
            for j in self.pid_attrs:
                f_name = self.pid_attrs[j]["qt_val"]
                f_type = self.pid_attrs[j]["qt_type"]
                f_value = self.data.get_attr(self.pid_attrs[j]["key"], p_path)
                self.set_input_field(f_name, f_type, f_value)

    def autofill_plants(self, key=''):
        """
        Name:     Prida.autofill_plants
        Inputs:   [optional] str, USDA PLANTS ID (key)
        Outputs:  None.
        Features: Autofills the Genus species for a given USDA PLANTS ID
        Depends:  set_input_field
        """
        if key in self.usda_plants.keys():
            try:
                self.set_input_field(
                    "c_gen_sp", "QLineEdit", str(self.usda_plants[key]))
            except:
                self.logger.exception("could not set QLineEdit c_gen_sp")

    def back_to_main(self):
        """
        Name:     Prida.back_to_main
        Inputs:   None
        Outputs:  None
        Features: Returns to the menu screen, closing the HDF5 file handle and
                  clearing session and sheet data.
        Depends:  - clear_session_sheet_model
                  - init_pid_attrs
                  - init_session_attrs
                  - update_file_browse_sheet
                  - update_file_search_sheet
                  - update_sheet_model
        @TODO:    connect to data close file with a signal
        """
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.init_pid_attrs()
        self.init_session_attrs()
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.editing = False
        self.update_sheet_model()
        self.update_file_browse_sheet()
        self.update_file_search_sheet()
        self.logger.info("view changed to MAIN_MENU")
        self.view.stackedWidget.setCurrentIndex(MAIN_MENU)
        self.base.setWindowTitle(self.prida_title)

    def back_to_menu(self):
        """
        Name:     Prida.back_to_menu
        Inputs:   None
        Outputs:  None
        Features: Signals data to close.
        """
        self.logger.info("Button Clicked")
        self.close_data.emit()

    def back_to_sheet(self):
        """
        Name:     Prida.back_to_sheet
        Inputs:   None
        Outputs:  None
        Features: Returns the current view to VIEWER cleaning up all the sheets
        Depends:  - clear_session_sheet_model
                  - update_sheet_model
                  - update_thumb_sheet_model
        """
        self.logger.debug("returning to VIEWER")
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.update_sheet_model()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.logger.info("view changed to SHEET_VIEW")
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.logger.info("view changed to VIEWER")
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def build_plants_db(self):
        """
        Name:     Prida.build_plants_db
        Inputs:   None.
        Outputs:  None.
        Features: Reads the dictionary file and parses contents into the
                  usda_plants dictionary; if no USDA PLANTS IDs found,
                  assumes generic numbering
        """
        unk_idx = 0
        try:
            with open(self.gs_dict) as f:
                my_dict = f.read().splitlines()
        except IOError:
            self.logger.warning("dictionary file not found")
        else:
            self.logger.info("read file contents to dictionary list")
            for item in my_dict:
                my_items = item.split(":")
                if len(my_items) == 2:
                    try:
                        my_keys = eval("%s" % my_items[0])
                    except NameError:
                        self.logger.exception(
                            "bad dictionary file entry '%s', check formatting",
                            item)
                    else:
                        for key in my_keys:
                            self.logger.debug("adding '%s' to database", key)
                            self.usda_plants[str(key)] = str(my_items[1])
                else:
                    self.logger.debug(
                        "USDA PLANTS ID not found, adding '%s' to database",
                        unk_idx)
                    self.usda_plants[str(unk_idx)] = item
                    unk_idx += 1

    def cancel_input(self):
        """
        Name:     Prida.cancel_input
        Inputs:   None
        Outputs:  None
        Features: Exits INPUT_EXP, resetting editing boolean, enabling all
                  QLineEdit fields, and returns to VIEWER
        Depends:  enable_all_attrs
        """
        self.logger.info("Button Clicked")
        self.editing = False
        self.enable_all_attrs()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.logger.info("view changed to SHEET_VIEW")
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.logger.info("view changed to VIEWER")
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def clear_session_sheet_model(self):
        """
        Name:     Prida.clear_session_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Clears session sheet model
        Depends:  resize_session_sheet
        """
        self.logger.debug("clearing session sheet")
        for i in range(self.session_sheet_items):
            self.session_sheet_model.setItem(i, 1, QStandardItem(''))
        #
        self.resize_session_sheet()

    def disable_pid_attrs(self):
        """
        Name:     Prida.disable_pid_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with PID attributes
        """
        if self.editing:
            self.logger.debug("disabling INPUT_EXP fields")
            self.view.c_pid.setEnabled(False)
            self.view.c_num_images.setEnabled(False)
            for k in self.pid_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.pid_attrs[k]["qt_val"]))

    def disable_session_attrs(self):
        """
        Name:     Prida.disable_session_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with session attributes
        """
        if self.editing:
            self.logger.debug("disabling INPUT_EXP fields")
            self.view.c_pid.setEnabled(False)
            for k in self.session_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.session_attrs[k]["qt_val"]))

    def edit_field(self):
        """
        Name:     Prida.edit_field
        Inputs:   None
        Outputs:  None
        Features: Performs INPUT_EXP field disabling based on the selection in
                  the sheet model, applies appropriate meta data to INPUT_EXP
                  fields, sets editing boolean to True and changes current
                  view to INPUT_EXP
        Depends:  - disable_pid_attrs
                  - disable_session_attrs
                  - set_defaults
                  - set_input_field
        """
        self.logger.info("Button Clicked")
        if self.sheet_select_model.hasSelection():
            self.logger.debug("set editing to True")
            self.editing = True
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session, disable PID attrs:
                self.disable_pid_attrs()

                # Get session attrs & set them to INPUT_EXP:
                pid = my_selection.parent().text()
                session = my_selection.text()
                p_path = "/%s" % (pid)
                s_path = "/%s/%s" % (pid, session)

                self.logger.debug("setting INPUT_EXP fields for PID")
                self.set_input_field("c_pid", "QLineEdit", str(pid))
                for j in self.pid_attrs:
                    f_name = self.pid_attrs[j]["qt_val"]
                    f_type = self.pid_attrs[j]["qt_type"]
                    f_value = self.data.get_attr(self.pid_attrs[j]["key"],
                                                 p_path)
                    self.set_input_field(f_name, f_type, f_value)

                self.logger.debug("setting INPUT_EXP fields for session")
                for k in self.session_attrs:
                    f_name = self.session_attrs[k]["qt_val"]
                    f_type = self.session_attrs[k]["qt_type"]
                    f_value = self.data.get_attr(self.session_attrs[k]["key"],
                                                 s_path)
                    self.set_input_field(f_name, f_type, f_value)
            else:
                # Selected PID, disable session attrs:
                self.disable_session_attrs()

                # Get PID attrs & set them to INPUT_EXP:
                pid = my_selection.text()
                s_path = "/%s" % (pid)

                self.set_defaults()
                self.logger.debug("setting INPUT_EXP fields for PID")
                self.set_input_field("c_pid", "QLineEdit", str(pid))
                for k in self.pid_attrs:
                    f_name = self.pid_attrs[k]["qt_val"]
                    f_type = self.pid_attrs[k]["qt_type"]
                    f_value = self.data.get_attr(self.pid_attrs[k]["key"],
                                                 s_path)
                    self.set_input_field(f_name, f_type, f_value)
            self.logger.info("view changed to INPUT_EXP")
            self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
        else:
            err_msg = "Nothing selected!"
            self.mayday(err_msg)

    def enable_all_attrs(self):
        """
        Name:     Prida.enable_all_attrs
        Inputs:   None
        Outputs:  None
        Features: Enables all INPUT_EXP fields
        """
        self.logger.debug("enabling all INPUT_EXP fields")
        self.view.c_pid.setEnabled(True)
        for k in self.pid_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.pid_attrs[k]["qt_val"]))

        for j in self.session_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.session_attrs[j]['qt_val']))

    def explore_file(self):
        """
        Name:     Prida.explore_file
        Inputs:   None.
        Outputs:  None.
        Features: Opens the sheet VIEW with the currently selected file from
                  the search file browser
        Depends:  - get_data
                  - update_sheet_model
        """
        self.logger.info("Button Clicked")
        if self.file_select_model.hasSelection():
            self.logger.debug("gathering selection details")
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0]
            )
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            self.logger.debug("opening selected file")
            self.data.open_file(f_path)
            self.logger.debug("gathering HDF file data")
            self.get_data()
            self.update_sheet_model()
            if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                self.logger.info("view changed to SHEET_VIEW")
                self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
            self.logger.info("view changed to VIEWER")
            self.view.stackedWidget.setCurrentIndex(VIEWER)

            # Set session user and contact defaults:
            self.logger.debug("saving session defaults")
            self.session_attrs[0]["def_val"] = self.data.root_user
            self.session_attrs[1]["def_val"] = self.data.root_addr
            self.logger.debug("updating window title")
            self.base.setWindowTitle(
                "%s: %s" % (self.prida_title, self.data.basename))
        else:
            err_msg = "Nothing selected!"
            self.mayday(err_msg)

    def export(self):
        """
        Name:     Prida.export
        Inputs:   None.
        Outputs:  None.
        Features: Exports the images from a given selection to a user-defined
                  output directory recreating the HDF5 folder structure, else
                  exports all images and meta data
        Depends:  get_sheet_model_selection
        """
        self.logger.info("Button Clicked")
        self.logger.debug("gathering user selection")
        if self.sheet_select_model.hasSelection():
            self.logger.debug("getting user selection")
            s_path = self.get_sheet_model_selection()
        else:
            self.logger.debug("no selection, exporting from root")
            s_path = "/"

        self.logger.debug("requesting output directory from user")
        path = QFileDialog.getExistingDirectory(None,
                                                "Select an output directory:",
                                                os.path.expanduser("~"),
                                                QFileDialog.ShowDirsOnly)
        self.data.session = s_path
        self.data.output_dir = path
        self.export_file_dialog.showMessage("Click OK to begin export.")

    def get_data(self):
        """
        Name:     Prida.get_data
        Inputs:   None.
        Outputs:  None.
        Features: Creates a list of attribute dictionaries for each PID in
                  the HDF5 file.
        """
        self.logger.debug("resetting data list")
        self.data_list = []

        self.logger.debug("reading PID attributes")
        for pid in self.data.pid_list:
            d = {}
            d["pid"] = pid
            for i in self.pid_attrs.keys():
                my_attr = self.pid_attrs[i]["key"]
                d[my_attr] = self.data.get_attr(my_attr,
                                                os.path.join('/', str(pid)))
            self.data_list.append(d)

    def get_input_field(self, field_name, field_type):
        """
        Name:     Prida.get_input_field
        Inputs:   - str, Qt field name (field_name)
                  - str, Qt field type (field_type)
        Outputs:  str, current input text (field_value)
        Features: Get the current text from a Qt input box
        Raises:   NameError
        """
        err_msg = "could not get %s %s value" % (field_type, field_name)

        if field_type == "QComboBox":
            try:
                field_value = eval("self.view.%s.currentText()" % (field_name))
            except:
                self.logger.exception(err_msg)
                raise NameError(err_msg)
            else:
                self.logger.debug("read %s from %s %s" % (
                    field_value, field_type, field_name))
        elif (field_type == "QDateEdit" or
              field_type == "QLineEdit" or
              field_type == "QSpinBox"):
            # All three fields use text() getter
            try:
                field_value = eval("self.view.%s.text()" % (field_name))
            except:
                self.logger.exception(err_msg)
                raise NameError(err_msg)
            else:
                self.logger.info("read %s from %s %s" % (
                    field_value, field_type, field_name))
        else:
            self.logger.error(err_msg)
            raise NameError(err_msg)

        return str(field_value)

    def get_sheet_model_selection(self):
        """
        Name:     Prida.get_sheet_model_selection
        Inputs:   None
        Outputs:  str, HDF5 group path based on selection (s_path)
        Features: Returns sheet model selection path for HDF5 file
        """
        if self.sheet_select_model.hasSelection():
            self.logger.debug("gathering user selection")
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session
                pid = my_selection.parent().text()
                session = my_selection.text()
                s_path = "/%s/%s" % (pid, session)
                self.logger.debug("returning session %s", s_path)
            else:
                # Selected PID
                pid = my_selection.text()
                s_path = "/%s" % (pid)
                self.logger.debug("returning PID %s", s_path)

            return s_path

    def import_data(self):
        """
        Name:     Prida.import_data
        Inputs:   None.
        Outputs:  None.
        Features: Reads a user-defined directory for images, saves images to
                  search_files dictionary, attempts to extract image exif tags
                  and defines them in INPUT_EXP view
        Depends:  - read_exif_tags
                  - set_defaults
                  - set_input_field
        """
        self.logger.info("Button Clicked")
        self.logger.debug("requesting directory from user")
        path = QFileDialog.getExistingDirectory(
            None,
            "Select directory containing images:",
            os.path.expanduser("~"),
            QFileDialog.ShowDirsOnly
        )
        if os.path.isdir(path):
            # Populate default values and overwrite where possible:
            self.set_defaults()

            # Currently filters search for only jpg and tif image files
            self.logger.debug("searching directory for images")
            my_files = []
            for img_type in self.img_types:
                s_path = os.path.join(path, "*%s" % (img_type))
                my_files += glob.glob(s_path)
            files_found = len(my_files)
            self.logger.info("importing %d files", files_found)

            if files_found > 0:
                # Reset file names for each processing:
                self.logger.debug("resetting search file dictionary")
                self.search_files = {}

                self.logger.debug("reading images found")
                for my_file in my_files:
                    try:
                        self.logger.debug("opening image %s", my_file)
                        img = Image.open(my_file)
                    except:
                        self.logger.exception("failed to open %s", my_file)
                        raise IOError("Error! Could not open image")
                    else:
                        self.search_files[my_file] = 0
                        exif = read_exif_tags(img)

                        # If DateTime is empty, try last modified time:
                        if exif["DateTime"] == "":
                            try:
                                self.logger.debug(
                                    "trying to read file modification date")
                                my_dt = get_ctime(my_file).strftime("%Y-%m-%d")
                            except:
                                self.logger.debug("no datetime exif tag")
                            else:
                                exif["DateTime"] = my_dt

                exif["NumPhotos"] = str(files_found)

                # Save the exif tag values to INPUT_EXP fields:
                self.save_exif_tags(exif)
                self.logger.info("view changed to INPUT_EXP")
                self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
            else:
                self.mayday("No images found in directory.")
                self.logger.warning("no images found in %s", path)

    def save_exif_tags(self, exif):
        """
        Name:     Prida.save_exif_tags
        Inputs:   dictionary, exif tags (exif)
        Outputs:  None.
        Features: Save exif tag values to INPUT_EXP fields
        Depends:  set_input_field
        """
        # Pseudo-intelligent search for session_attr index:
        tag_attr = {
            "Make": [k for k, v in self.session_attrs.items()
                     if v["key"] == "cam_make"],
            "Model": [k for k, v in self.session_attrs.items()
                      if v["key"] == "cam_model"],
            "ShutterSpeedValue": [k for k, v in self.session_attrs.items()
                                  if v["key"] == "cam_shutter"],
            "ApertureValue": [k for k, v in self.session_attrs.items()
                              if v["key"] == "cam_aperture"],
            "ISOSpeedRatings": [k for k, v in self.session_attrs.items()
                                if v["key"] == "cam_exposure"],
            "DateTime": [k for k, v in self.session_attrs.items()
                         if v["key"] == "date"],
            "NumPhotos": [k for k, v in self.session_attrs.items()
                          if v["key"] == "num_img"]
        }

        for tag in exif:
            if tag in tag_attr:
                j = tag_attr[tag][0]
                f_name = self.session_attrs[j]["qt_val"]
                f_type = self.session_attrs[j]["qt_type"]
                f_val = exif[tag]
                self.logger.debug("setting %s %s to %s" % (
                    f_type, f_name, f_val))
                self.set_input_field(f_name, f_type, f_val)

    def import_session(self):
        """
        Name:     Prida.import_session
        Inputs:   None.
        Outputs:  None.
        Features: Creates a new session based on imported data; checks that
                  images were found (search_files dictionary); opens popup
                  for beginning import (accept signal connected to data)
        Depends:  - back_to_sheet           - (editing) save_edits
                  - get_data                - (editing) enable_all_attrs
                  - get_input_field         - (editing) set_defaults
                  - update_sheet_model
        TODO:     * dataset attribute orientation not saved
        """
        self.logger.info("Button Clicked")
        if self.editing:
            self.logger.info("Saving Edits")
            self.save_edits()
            self.logger.debug("set editing to False")
            self.editing = False
            self.enable_all_attrs()
            self.set_defaults()
            self.get_data()
            self.back_to_sheet()
        else:
            # Create PID metadata dictionary:
            self.logger.debug("building PID dictionary")
            pid_dict = {}
            for i in self.pid_attrs.keys():
                f_key = self.pid_attrs[i]["key"]
                f_name = self.pid_attrs[i]["qt_val"]
                f_type = self.pid_attrs[i]["qt_type"]
                f_val = self.get_input_field(f_name, f_type)
                self.pid_attrs[i]["def_val"] = f_val
                pid_dict[f_key] = f_val

            # Create session metadata dictionary:
            self.logger.debug("building session dictionary")
            session_dict = {}
            for j in self.session_attrs.keys():
                f_key = self.session_attrs[j]["key"]
                f_name = self.session_attrs[j]["qt_val"]
                f_type = self.session_attrs[j]["qt_type"]
                f_val = self.get_input_field(f_name, f_type)
                self.session_attrs[j]["def_val"] = f_val
                session_dict[f_key] = f_val

            # Create new session:
            self.logger.info("Saving New Session")
            s_path = self.data.create_session(
                str(self.view.c_pid.text()), pid_dict, session_dict)

            # Set progress bar min and max values based on files found:
            files_found = sorted(list(self.search_files.keys()))
            num_files = len(files_found)
            if num_files > 0:
                self.view.c_progress.setMinimum(0)
                self.view.c_progress.setMaximum(num_files - 1)

                # Set appropriate data variables for processing:
                self.data.abort = False
                self.data.img_files = files_found
                self.data.session = s_path

                self.logger.info("view changed to RUN_EXP")
                self.view.stackedWidget.setCurrentIndex(RUN_EXP)

                self.import_file_dialog.showMessage(
                    "Click OK to begin importing %d images." % (num_files))
            else:
                err_msg = "No images found for importing."
                self.mayday(err_msg)
                self.get_data()
                self.back_to_sheet()

    def init_pid_attrs(self):
        """
        Name:     Prida.init_pid_attrs
        Inputs:   None.
        Outputs:  None.
        Features: Initializes the PID attribute dictionary
        """
        self.logger.debug('initializing dictionary')
        self.pid_attrs = {
            1: {'title': 'USDA PLANTS',
                'key': 'usda_plants',
                'qt_val': 'c_usda_plants',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            2: {'title': 'Genus Species',
                'key': 'gen_sp',
                'qt_val': 'c_gen_sp',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            3: {'title': 'Line',
                'key': 'line',
                'qt_val': 'c_line',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            4: {'title': 'Rep Num',
                'key': 'rep_num',
                'qt_val': 'c_rep_num',
                'def_val': '',
                'qt_type': 'QSpinBox'},
            5: {'title': 'Tub ID',
                'key': 'tubid',
                'qt_val': 'c_tubid',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            6: {'title': 'Germination Date',
                'key': 'germdate',
                'qt_val': 'c_germdate',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            7: {'title': 'Transplant Date',
                'key': 'transdate',
                'qt_val': 'c_transdate',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            8: {'title': 'Treatment',
                'key': 'treatment',
                'qt_val': 'c_treatment',
                'def_val': 'Control',
                'qt_type': 'QLineEdit'},
            9: {'title': 'Media',
                'key': 'media',
                'qt_val': 'c_media',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            10: {'title': 'Tub Size',
                 'key': 'tubsize',
                 'qt_val': 'c_tubsize',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            11: {'title': 'Nutrient',
                 'key': 'nutrient',
                 'qt_val': 'c_nutrient',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            12: {'title': 'Growth Temp (Day)',
                 'key': 'growth_temp_day',
                 'qt_val': 'c_growth_temp_day',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            13: {'title': 'Growth Temp (Night)',
                 'key': 'growth_temp_night',
                 'qt_val': 'c_growth_temp_night',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            14: {'title': 'Lighting Conditions',
                 'key': 'growth_light',
                 'qt_val': 'c_growth_light',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            15: {'title': 'Watering schedule',
                 'key': 'water_sched',
                 'qt_val': 'c_water_sched',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            16: {'title': 'Damage',
                 'key': 'damage',
                 'qt_val': 'c_damage',
                 'def_val': 'No',
                 'qt_type': 'QLineEdit'},
            17: {'title': 'Plant Notes',
                 'key': 'notes',
                 'qt_val': 'c_plant_notes',
                 'def_val': 'N/A',
                 'qt_type': 'QLineEdit'}
        }

    def init_session_attrs(self):
        """
        Name:     Prida.init_session_attrs
        Inputs:   None.
        Outputs:  None.
        Features: Initializes session attribute dictionary
        """
        self.logger.debug('initializing dictionary')
        self.session_attrs = {
            0: {'title':  'Session User',
                'key': 'user',
                'qt_val': 'c_session_user',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            1: {'title': 'Session Email',
                'key': 'addr',
                'qt_val': 'c_session_addr',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            2: {'title': 'Session Title',
                'key': 'number',
                'qt_val': 'c_session_number',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            3: {'title': 'Session Date',
                'key': 'date',
                'qt_val': 'c_session_date',
                'def_val': '',
                'qt_type': 'QDateEdit'},
            4: {'title': 'Rig Name',
                'key': 'rig',
                'qt_val': 'c_session_rig',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            5: {'title': 'Image Count',
                'key': 'num_img',
                'qt_val': 'c_num_images',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            6: {'title': 'Plant Age',
                'key': 'age_num',
                'qt_val': 'c_plant_age',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            7: {'title': 'Notes',
                'key': 'notes',
                'qt_val': 'c_session_notes',
                'def_val': 'N/A',
                'qt_type': 'QLineEdit'},
            8: {'title': 'Camera Make',
                'key': 'cam_make',
                'qt_val': 'c_cam_make',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            9: {'title': 'Camera Model',
                'key': 'cam_model',
                'qt_val': 'c_cam_model',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            10: {'title': 'Exposure Time',
                 'key': 'cam_shutter',
                 'qt_val': 'c_cam_shutter',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            11: {'title': 'Aperture',
                 'key': 'cam_aperture',
                 'qt_val': 'c_cam_aperture',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            12: {'title': 'ISO',
                 'key': 'cam_exposure',
                 'qt_val': 'c_cam_exposure',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            13: {'title': 'Orientation',
                 'key': 'img_orient',
                 'qt_val': 'c_img_orient',
                 'def_val': '',
                 'qt_type': 'QComboBox'},
            14: {'title': 'Exclude',
                 'key': 'exclude',
                 'qt_val': 'c_exclude',
                 'def_val': 'False',
                 'qt_type': 'QComboBox'}
        }

    def load_analysis_sheet_headers(self):
        """
        Name:     Prida.load_analysis_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the analysis sheet model
        Depends:  resize_analysis_sheet
        """
        self.logger.debug('loading headers')
        self.analysis_sheet_model.setColumnCount(2)
        self.analysis_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('PID'))
        self.analysis_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Session'))
        self.resize_analysis_sheet()

    def load_file_browse_sheet_headers(self):
        """
        Name:     Prida.load_file_browse_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the file_sheet_model
        Depends:  resize_file_browse_sheet
        """
        self.logger.debug('loading headers')
        self.file_sheet_model.setColumnCount(2)
        self.file_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File Name'))
        self.file_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Path'))
        self.resize_file_browse_sheet()

    def load_file_search_sheet_headers(self):
        """
        Name:     Prida.load_file_search_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the search_sheet_model
        Depends:  resize_search_sheet
        """
        self.logger.debug('loading headers')
        self.search_sheet_model.setColumnCount(self.sheet_items + 1)
        self.search_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File'))
        self.search_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('PID'))
        for i in self.pid_attrs:
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.search_sheet_model.setHorizontalHeaderItem", (i + 1),
                self.pid_attrs[i]['title']))
        self.resize_search_sheet()

    def load_session_sheet_headers(self):
        """
        Name:     Prida.load_session_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the session_sheet_model
        Depends:  resize_session_sheet
        """
        self.logger.debug('loading headers')
        self.session_sheet_model.setColumnCount(2)
        self.session_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('Property'))
        self.session_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Value'))
        #
        # Define session sheet rows in column 1:
        for i in range(self.session_sheet_items):
            exec('%s(%d, 0, QStandardItem("%s"))' % (
                "self.session_sheet_model.setItem", i,
                self.session_attrs[i]['title']))
        self.resize_session_sheet()

    def load_sheet_headers(self):
        """
        Name:     Prida.load_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the sheet_model
        Depends:  resize_sheet
        """
        self.logger.debug('load_sheet_headers:loading headers')
        self.sheet_model.setColumnCount(self.sheet_items)
        self.sheet_model.setHorizontalHeaderItem(0, QStandardItem('PID'))
        for i in self.pid_attrs.keys():
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.sheet_model.setHorizontalHeaderItem", i,
                self.pid_attrs[i]['title']))
        self.resize_sheet()

    def mayday(self, msg):
        """
        Name:     Prida.mayday
        Inputs:   str, notification message (msg)
        Outputs:  None.
        Features: Opens a popup window displaying the notification text
        """
        self.error.showMessage(msg)
        self.error.exec_()

    def new_hdf(self):
        """
        Name:     Prida.new_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Gets user information, gets HDF5 file name, creates a new
                  HDF5 file, stores user information, and goes to VIEWER page
        Depends:  - get_data
                  - update_sheet_model
        """
        self.logger.info('Button Clicked')
        self.logger.debug('loading UI file')
        self.user_dialog = QDialog(self.base)
        self.user_ui.setupUi(self.user_dialog)
        popup = self.user_ui
        if self.user_dialog.exec_():
            self.logger.debug('requesting new file name from user')
            path = QFileDialog.getSaveFileName(None,
                                               "Save File",
                                               os.path.expanduser("~"))[0]
            if path != '':
                if path[-5:] != '.hdf5':
                    path += '.hdf5'
                self.logger.debug("opening new file")
                self.data.open_file(path)

                self.logger.debug("saving about info to file")
                sess_user = str(popup.user.text())
                sess_addr = str(popup.email.text())
                sess_about = str(popup.about.toPlainText())
                self.data.root_user = sess_user
                self.data.root_addr = sess_addr
                self.data.about = sess_about

                self.logger.debug("saving file")
                self.data.save_file()
                self.get_data()
                self.update_sheet_model()
                if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                    self.logger.info('view changed to SHEET_VIEW')
                    self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
                self.logger.info('view changed eto VIEWER')
                self.view.stackedWidget.setCurrentIndex(VIEWER)

                # Set session user and contact default values:
                self.logger.debug('saving session defaults')
                self.session_attrs[0]['def_val'] = self.data.root_user
                self.session_attrs[1]['def_val'] = self.data.root_addr
                self.logger.debug('updating window title')
                self.base.setWindowTitle(
                    "%s: %s" % (self.prida_title, self.data.basename)
                )
                self.set_defaults()

    def new_session(self):
        """
        Name:     Prida.new_session
        Inputs:   None.
        Outputs:  None
        Features: Creates HDF5 session, resets the progress bar, enters the
                  RUN_EXP screen, and begins a session
        """
        self.logger.info('Button Clicked')
        if self.editing:
            self.logger.info('Saving Edits')
            self.save_edits()
            self.logger.debug('set editing to False')
            self.editing = False
            self.enable_all_attrs()
            self.set_defaults()
            self.get_data()
            self.back_to_sheet()
        else:
            if self.hardware_mode:
                # Create the PID metadata dictionary:
                self.logger.debug('building PID dictionary')
                pid_dict = {}
                for i in self.pid_attrs.keys():
                    f_key = self.pid_attrs[i]['key']
                    f_name = self.pid_attrs[i]['qt_val']
                    f_type = self.pid_attrs[i]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    self.pid_attrs[i]['def_val'] = f_val
                    pid_dict[f_key] = f_val

                # Create the session metadata dictionary:
                self.logger.debug('buidling session dictionary')
                session_dict = {}
                for j in self.session_attrs.keys():
                    f_key = self.session_attrs[j]['key']
                    f_name = self.session_attrs[j]['qt_val']
                    f_type = self.session_attrs[j]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    self.session_attrs[j]['def_val'] = f_val
                    session_dict[f_key] = f_val

                # Create a new session:
                self.session_path = self.data.create_session(
                    str(self.view.c_pid.text()), pid_dict, session_dict)
                self.logger.info("created new session %s", self.session_path)

                # Update hardware:
                self.logger.debug("updating hardware")
                self.hardware.image_number = int(self.view.c_num_images.text())
                self.hardware.image_pid = str(self.view.c_pid.text())

                # Prepare for RUN_EXP image capture and preview:
                self.logger.debug("preparing progress bar")
                progress_max = self.hardware.get_maximum_value()
                self.view.c_preview.clear()
                self.view.c_progress.setMinimum(0)
                self.view.c_progress.setMaximum(progress_max)

                self.logger.info("view changed to RUN_EXP")
                self.view.stackedWidget.setCurrentIndex(RUN_EXP)

                self.logger.info("starting hardware thread")
                self.hardware_thread.start()

    def open_hdf(self):
        """
        Name:     Prida.open_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Opens an existing HDF5 file and goes to VIEWER page
        Depends:  - get_data
                  - back_to_sheet
                  - set_defaults
                  - update_pid_auto_model
        """
        self.logger.info('Button Clicked')
        self.logger.debug('request existing file from user')
        path = QFileDialog.getOpenFileName(None,
                                           "Select File",
                                           os.path.expanduser("~"),
                                           filter='*.hdf5')[0]
        if os.path.isfile(path):
            self.logger.debug("opening file %s", path)
            self.data.open_file(path)
            self.get_data()
            self.update_pid_auto_model()
            self.back_to_sheet()

            # Set session user and contact defaults:
            self.logger.debug('saving session defaults')
            self.session_attrs[0]['def_val'] = self.data.root_user
            self.session_attrs[1]['def_val'] = self.data.root_addr
            self.logger.debug('updating window title')
            self.base.setWindowTitle(
                "%s: %s" % (self.prida_title, self.data.basename)
            )
            self.set_defaults()

    def resize_analysis_sheet(self):
        """
        Name:     Prida.resize_analysis_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the analysis session sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.a_session.resizeColumnToContents(col)

    def resize_file_browse_sheet(self):
        """
        Name:     Prida.resize_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_browse_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.file_browse_sheet.resizeColumnToContents(col)

    def resize_search_sheet(self):
        """
        Name:     Prida.resize_search_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_search_sheet
        """
        self.logger.debug('resizing')
        for col in range(self.sheet_items + 2):
            self.view.file_search_sheet.resizeColumnToContents(col)

    def resize_session_sheet(self):
        """
        Name:     Prida.resize_session_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the session_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.session_sheet.resizeColumnToContents(col)

    def resize_sheet(self):
        """
        Name:     Prida.resize_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the sheet
        """
        self.logger.debug("resizing")
        for col in range(self.sheet_items + 1):
            self.view.sheet.resizeColumnToContents(col)

    def rm_search_file(self):
        """
        Name:     Prida.rm_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Removes a file from the file browse view and its
                  associated PID rows from the file search view
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.info("Button Clicked")
        if self.file_select_model.hasSelection():
            # Get the selection and put together the file name and path:
            self.logger.debug("gathering user selection")
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0])
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            # Remove file from search_files:
            self.logger.debug("deleting file %s", f_name)
            del self.search_files[s_name]

            # Remove file's PIDs from file_search_sheet:
            self.logger.debug("opening selected file")
            self.data.open_file(f_path)

            self.logger.debug("deleting PIDs from search data")
            for pid in self.data.pid_list:
                my_key = "%s.%s" % (s_name, pid)
                if my_key in self.search_data:
                    del self.search_data[my_key]
            self.logger.debug("closing selected file")
            self.data.close_search_file()
            self.view.f_search.clear()
            self.update_file_browse_sheet()
            self.update_file_search_sheet()
        else:
            self.logger.warning("nothing selected!")
            err_msg = "No selection made. Please select a file to remove."
            self.mayday(err_msg)

    def save_edits(self):
        """
        Name:     Prida.save_edits
        Inputs:   None
        Outputs:  None
        Features: Saves new attributes to HDF5 file
        Depends:  get_sheet_model_selection
        """
        if self.editing:
            if self.sheet_select_model.hasSelection():
                self.logger.debug("gathering user selection")
                s_path = self.get_sheet_model_selection()

                self.logger.debug(
                    "saving attributes from enabled INPUT_EXP fields")
                for i in self.pid_attrs.keys():
                    f_key = self.pid_attrs[i]["key"]
                    f_name = self.pid_attrs[i]["qt_val"]
                    f_type = self.pid_attrs[i]["qt_type"]
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        self.pid_attrs[i]["def_val"] = f_val
                        self.data.set_attr(f_key, f_val, s_path)

                for j in self.session_attrs.keys():
                    f_key = self.session_attrs[j]["key"]
                    f_name = self.session_attrs[j]["qt_val"]
                    f_type = self.session_attrs[j]["qt_type"]
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        self.session_attrs[j]["def_val"] = f_val
                        self.data.set_attr(f_key, f_val, s_path)
                self.logger.debug("saving file")
                self.data.save_file()

    def search(self, pid_meta, terms):
        """
        Name:     Prida.search
        Inputs:   - list, PID meta data values (pid_meta)
                  - list, search terms (terms)
        Outputs:  bool (found)
        Features: Searches the pid metafields and returns true if all terms are
                  found anywhere within the fields
        """
        found = True
        for term in terms:
            found = found and any(term in m_field for m_field in pid_meta)
        return found

    def set_defaults(self):
        """
        Name:     Prida.set_defaults
        Inputs:   None.
        Outputs:  None.
        Features: Sets GUI text default values
        """
        self.logger.debug("resetting INPUT_EXP PID default fields")
        self.set_input_field("c_pid", "QLineEdit", "")
        for i in self.pid_attrs.keys():
            f_name = self.pid_attrs[i]["qt_val"]
            f_type = self.pid_attrs[i]["qt_type"]
            f_val = self.pid_attrs[i]["def_val"]
            try:
                self.set_input_field(f_name, f_type, f_val)
            except:
                self.set_input_field(f_name, f_type, '')

        self.logger.debug("resetting INPUT_EXP session default fields")
        for j in self.session_attrs.keys():
            f_name = self.session_attrs[j]["qt_val"]
            f_type = self.session_attrs[j]["qt_type"]
            f_val = self.session_attrs[j]["def_val"]
            try:
                self.set_input_field(f_name, f_type, f_val)
            except:
                self.set_input_field(f_name, f_type, '')

    def set_input_field(self, field_name, field_type, field_value):
        """
        Name:     Prida.set_input_field
        Inputs:   - str, Qt input widget name (field_name)
                  - str, Qt input widget type (field_type)
                  - str, input value (field_value)
        Outputs:  None
        Features: Sets Qt input field

        @TODO: change empty string to None type
        """
        err_msg = "set_input_field:could not set Qt %s, %s, to value %s" % (
            field_type, field_name, field_value)

        if field_type == "QComboBox":
            # Takes an integer, find it!
            if field_value == '':
                index = 0
            else:
                index = eval('self.view.%s.findText("%s", %s)' % (
                    field_name,
                    field_value,
                    "Qt.MatchFixedString")
                )
            try:
                self.logger.debug("setting QComboBox index to %d", index)
                exec("self.view.%s.setCurrentIndex(%d)" % (
                    field_name,
                    index)
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        elif field_type == "QDateEdit":
            # Takes a QDate object
            if field_value == '':
                try:
                    self.logger.debug("setting QDateEdit to current date")
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        "QDate.fromString",
                        QDate.currentDate().toString("yyyy-MM-dd"))
                    )
                except:
                    self.logger.exception(err_msg)
                    raise ValueError(err_msg)
            else:
                try:
                    self.logger.debug("setting QDate edit to %s", field_value)
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        "QDate.fromString",
                        field_value)
                    )
                except:
                    self.logger.exception(err_msg)
                    raise ValueError(err_msg)
        elif field_type == "QLineEdit":
            # Takes a string
            try:
                self.logger.debug("setting QLineEdit to %s", field_value)
                exec('self.view.%s.setText("%s")' % (
                    field_name,
                    field_value)
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        elif field_type == "QSpinBox":
            # Takes an integer
            if field_value == '':
                field_value = 0
            try:
                self.logger.debug("setting QSpinBox to %d", int(field_value))
                exec("self.view.%s.setValue(%d)" % (
                    field_name,
                    int(field_value))
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        else:
            self.logger.error(err_msg)
            raise ValueError(err_msg)

    def show_ida(self, selection):
        """
        Name:     Prida.show_ida
        Input:    obj (selection)
        Output:   None.
        Features: Switches to IDA page and sets IDA base image
        """
        self.logger.debug("clearing IDA base image")
        self.view.ida.clearBaseImage()
        self.logger.debug("IDA base image cleared")
        self.logger.debug("checking selection indexes %s", selection.indexes())
        if selection.indexes():
            self.logger.info("view changed to IDA_VIEW")
            self.view.stacked_sheets.setCurrentIndex(IDA_VIEW)

            self.logger.debug("gathering user selection")
            img_path = self.img_sheet_model.itemFromIndex(
                selection.indexes()[0]).data()

            self.logger.debug("checking image orientation")
            s_path = self.data.get_image_session(img_path)
            if s_path is not None:
                img_orient = self.data.get_attr("img_orient", s_path)
            else:
                img_orient = "Original"
            self.logger.info("#101# %s", img_orient)

            self.logger.debug("gathering image data for selection")
            ndarray_full = self.data.get_dataset(img_path)
            if ndarray_full.size != 0:
                if img_orient and img_orient != "Original":
                    if img_orient == "Rotated Clockwise":
                        self.logger.debug(
                            "rotating image 90 degrees clockwise")
                        ndarray_full = numpy.rot90(ndarray_full, 3)
                    elif img_orient == "Rotated Counter-Clockwise":
                        self.logger.debug(
                            "rotating image 90 degrees counter-clockwise")
                        ndarray_full = numpy.rot90(ndarray_full)
                    else:
                        self.logger.warning(
                            "failed to match orientation %s", img_orient)

                self.logger.debug("setting IDA base image to selection")
                self.view.ida.setBaseImage(ndarray_full)
            else:
                self.logger.warning("no data retrieved from %s", img_path)

    def show_preview(self, selection):
        """
        Name:     Prida.show_preview
        Input:    obj (selection)
        Output:   None.
        Features: Updates ANALYSIS IDA image preview; handles image orientation
        """
        self.logger.debug("clearing IDA base image")
        self.view.a_preview.clearBaseImage()
        self.view.a_label.clear()
        if selection.indexes():
            self.logger.debug("gathering user selection")
            img_path = self.thumb_sheet_model.itemFromIndex(
                selection.indexes()[0]).data()

            self.logger.debug("checking image orientation")
            s_path = self.data.get_image_session(img_path)
            if s_path is not None:
                img_orient = self.data.get_attr("img_orient", s_path)
            else:
                img_orient = "Original"
            self.logger.info("#102# %s", img_orient)

            self.logger.info("gathering data for image %s", img_path)
            ndarray_full = self.data.get_dataset(img_path)
            if ndarray_full.size != 0:
                if img_orient and img_orient != "Original":
                    if img_orient == "Rotated Clockwise":
                        self.logger.debug(
                            "rotating image 90 degrees clockwise")
                        ndarray_full = numpy.rot90(ndarray_full, 3)
                    elif img_orient == "Rotated Counter-Clockwise":
                        self.logger.debug(
                            "rotating image 90 degrees counter-clockwise")
                        ndarray_full = numpy.rot90(ndarray_full)
                    else:
                        self.logger.warning(
                            "failed to match orientation %s", img_orient)

                self.logger.debug("set IDA base image to selection")
                self.view.a_preview.setBaseImage(ndarray_full)
                self.view.a_label.setText("original image")
            else:
                self.logger.warning("no data read from %s", img_path)

    def shutdown(self):
        """
        Name:     Prida.shutdown
        Inputs:   None.
        Outputs:  None.
        Features: Shutsdown all open processes and threads
        """
        try:
            self.data.close_file()
            self.data_thread.quit()
            if self.hardware_mode:
                self.hardware_thread.quit()
        except:
            self.logger.warning("failed to shutdown")
        else:
            self.logger.info("app is shutting down")
        finally:
            self.quit()

    def start_spinner(self, msg):
        """
        Name:     Prida.start_spinner
        Inputs:   str, spinner's message text (msg)
        Outputs:  None.
        Features: Starts the spinner GIF and with associated text
        """
        self.view.spinner_txt.setText(msg)
        self.view.spinner_gif.setMovie(self.movie)
        self.view.spinner_gif.show()
        self.movie.start()

    def stop_spinner(self):
        """
        Name:     Prida.stop_spinner
        Inputs:   None.
        Outputs:  None.
        Features: Stops the spinner GIF and clears the associated text
        """
        self.movie.stop()
        self.view.spinner_txt.setText("")
        self.view.spinner_gif.clear()

    def to_sheet(self):
        """
        Name:     Prida.to_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Switches from IDA to sheet view
        """
        self.logger.info("Button Clicked")
        self.logger.debug("clearing img_select")
        self.view.img_select.clearSelection()
        self.logger.info("view changed to SHEET_VIEW")
        self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)

    def tool_not_enabled(self):
        """
        @TODO
        """
        self.logger.warning("tool not enabled!")
        err_msg = "This tool has not be enabled yet."
        self.mayday(err_msg)

    def update_analysis_sheet_model(self, search=''):
        """
        Name:     Prida.update_analysis_sheet_model
        Inputs:   [optional] str (search)
        Outputs:  None
        Features: Displays all PIDs and sessions or just those that match the
                  search terms if search terms are given; saves the session
                  path to the PID QStandardItem
        Depends:  - load_analysis_sheet_headers
                  - update_ps_list
        """
        self.logger.debug("updating sheet")
        self.analysis_sheet_model.clear()
        self.load_analysis_sheet_headers()
        self.update_ps_list()
        terms = search.lower().split()

        for s_path in self.ps_list:
            ps = self.ps_list[s_path]
            self.logger.debug("unpacking %s", s_path)
            pid, session = ps
            self.logger.debug("found PID %s and session %s" % (pid, session))
            if self.search([i.lower() for i in ps], terms):
                pid_item = QStandardItem(pid)
                pid_item.setData(s_path)
                session_item = QStandardItem(session)
                pid_list = [pid_item, session_item]
                self.analysis_sheet_model.appendRow(pid_list)
        self.resize_analysis_sheet()

    def update_dict_auto_model(self):
        """
        Name:     Prida.update_dict_auto_model
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QStringListModel for the dictionary file's
                  contents
        """
        self.plants_auto_model.setStringList(list(self.usda_plants.keys()))
        self.dict_auto_model.setStringList(
            list(set(self.usda_plants.values())))

    def update_exclude_combo(self):
        """
        Name:     Prida.update_exclude_combo
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QComboBox with exclude options
        """
        self.logger.debug("setting the QComboBox values for c_exclude")
        my_options = ["False",
                      "True"]
        self.view.c_exclude.insertItems(0, my_options)

    def update_file_browse_sheet(self):
        """
        Name:     Prida.update_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Reloads the rows in the file_sheet_model
        Depends:  - load_file_browse_sheet_headers
                  - resize_file_browse_sheet
        """
        self.logger.debug("updating sheet")
        self.file_sheet_model.clear()
        self.load_file_browse_sheet_headers()
        for f_name in self.search_files:
            f_path = self.search_files[f_name]
            f_list = [QStandardItem(f_name), QStandardItem(f_path)]
            self.file_sheet_model.appendRow(f_list)
        self.resize_file_browse_sheet()

    def update_file_search_sheet(self, search=''):
        """
        Name:     Prida.update_file_search_sheet
        Inputs:   [optional] str, search string (search)
        Outputs:  None.
        Features: Reloads the rows in the search_sheet_model, filtered for
                  search terms
        Depends:  - load_file_search_sheet_headers
                  - resize_search_sheet
                  - search
        """
        self.logger.debug("updating sheet")
        self.search_sheet_model.clear()
        self.load_file_search_sheet_headers()
        terms = search.lower().split()
        for i in self.search_data:
            pid_meta = []
            for j in self.search_data[i]:
                pid_meta.append(j.lower())
            if self.search(pid_meta, terms):
                d_list = []
                for k in self.search_data[i]:
                    d_list.append(QStandardItem(k))
                self.search_sheet_model.appendRow(d_list)
        self.resize_search_sheet()

    def update_orient_combo(self):
        """
        Name:     Prida.update_orient_combo
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QComboBox with orientation options
        """
        self.logger.debug("setting the QComboBox values for c_img_orient")
        my_options = ["Original",
                      "Rotated Counter-Clockwise",
                      "Rotated Clockwise"]
        self.view.c_img_orient.insertItems(0, my_options)

    def update_pid_auto_model(self):
        """
        Name:     Prida.update_pid_auto_model
        Inputs:   None
        Outputs:  None
        Features: Updates the QStringListModel with HDF5 file's current PIDs
        """
        self.logger.debug("assigning PIDs to list")
        self.pid_auto_model.setStringList(self.data.pid_list)

    def update_progress(self, prog_val, t, show_all=False):
        """
        Name:     Prida.update_progress
        Inputs:   - int, progress value (prog_val)
                  - tuple, imaging output arguments (t)
                  - bool, show all preview images (show_all)
        Outputs:  None.
        Features: Threading slot.
                  Updates the progress bar during imaging, creates a preview
                  of the first image taken, and saves the image along with
                  associated metadata to HDF5 file
        """
        self.logger.debug("updating progress bar")
        self.view.c_progress.setValue(prog_val)

        self.logger.debug("gathering image metadata")
        is_path = t[0]
        file_path = t[1]

        if show_all:
            self.preview = False

        self.logger.debug("setting image preview")
        if is_path and not self.preview:
            try:
                image = QPixmap(file_path)
            except:
                self.logger.exception("QPixmap failed for file %s", file_path)
                self.preview = False
            else:
                self.logger.debug("image preview set")
                self.view.c_preview.setPixmap(image.scaledToHeight(300))
                self.preview = True
        else:
            self.logger.debug("skipping image, preview already set")

    def end_progress(self):
        """
        Name:     Prida.end_progress
        Inputs:   None.
        Outputs:  None.
        Features: Clears preview image, gatherings data, and returns to sheet
                  view.
        """
        self.logger.debug("imaging done")
        self.view.c_preview.clear()
        self.preview = False
        self.get_data()
        self.back_to_sheet()

    def update_ps_list(self):
        """
        Name:     Prida.update_ps_list
        Inputs:   None.
        Outputs:  None.
        Features: Creates a dictionary of session path keys and pid/session
                  name list values
        """
        self.logger.debug("resetting ps list")
        self.ps_list = {}
        for pid in self.data.pid_list:
            for session in self.data.list_sessions(pid):
                s_path = "/%s/%s" % (pid, session)
                s_name = self.data.get_attr("number", s_path)
                self.logger.debug("saving session path '%s'", s_path)
                if s_name and not s_name.isspace():
                    # Use user-given session name if available
                    self.ps_list[s_path] = [pid, s_name]
                    self.logger.debug("using session name '%s'", s_name)
                else:
                    # Otherwise, just use session group name
                    self.ps_list[s_path] = [pid, session]

    def update_session_sheet_model(self, selection):
        """
        Name:     Prida.update_session_sheet_model
        Inputs:   obj (selection)
        Outputs:  None.
        Features: Updates session sheet model with selected session's data,
                  creates the thumbnails and tags the full image paths, and
                  clears session sheet model if no sessions are selected;
                  handles image orientation
        Depends:  - clear_session_sheet_model
                  - resize_session_sheet
        """
        try:
            self.logger.debug("gathering user selection")
            my_session = self.sheet_model.itemFromIndex(selection.indexes()[0])
        except IndexError:
            # You made a de-selection (ctrl+click):
            self.logger.debug("encountered de-selection")
            self.clear_session_sheet_model()
            self.img_sheet_model.clear()
            self.img_list = []
            self.resize_session_sheet()
        else:
            if (my_session.parent() is not None):
                # Session selection
                self.logger.debug("session selected")
                pid = my_session.parent().text()
                session = my_session.text()
                s_path = "/%s/%s" % (pid, session)
                self.logger.debug("loading session fields")
                for i in range(self.session_sheet_items):
                    exec('%s(%d, 1, QStandardItem("%s"))' % (
                        "self.session_sheet_model.setItem", i,
                        self.data.get_attr(
                            self.session_attrs[i]['key'], s_path)
                        )
                    )
                self.img_sheet_model.clear()
                self.img_list = []
                img_counter = 0

                self.logger.debug("gathering image orientation")
                img_orient = self.data.get_attr("img_orient", s_path)

                self.logger.debug("reading session datasets")
                thumb_path = ""
                image_path = ""
                for obj in self.data.list_objects(s_path):
                    img_counter += 1
                    image_datasets = self.data.list_objects(
                        "%s/%s" % (s_path, obj))
                    for img_name in image_datasets:
                        if ".thumb" in img_name:
                            thumb_path = "%s/%s/%s" % (s_path,
                                                       obj,
                                                       img_name)
                        else:
                            for img_type in self.img_types:
                                if img_type in img_name:
                                    image_path = "%s/%s/%s" % (s_path,
                                                               obj,
                                                               img_name)

                    self.logger.debug("getting data for %s", thumb_path)
                    ndarray = self.data.get_dataset(thumb_path)
                    if ndarray.size != 0:
                        if img_orient and img_orient != "Original":
                            if img_orient == "Rotated Clockwise":
                                self.logger.debug(
                                    "rotating image 90 degrees clockwise")
                                ndarray = numpy.rot90(ndarray, 3)
                            elif img_orient == "Rotated Counter-Clockwise":
                                self.logger.debug(
                                    ("rotating image 90 degrees "
                                     "counter-clockwise"))
                                ndarray = numpy.rot90(ndarray)
                            else:
                                self.logger.debug(
                                    "failed to match orientation %s",
                                    img_orient)
                        image = Image.fromarray(ndarray)
                        qt_image = ImageQt.ImageQt(image)
                        self.img_list.append(qt_image)
                        pix = QPixmap.fromImage(qt_image)
                        self.logger.debug("adding thumb to image list")
                        item = QStandardItem(str(img_counter))
                        item.setIcon(QIcon(pix))
                        item.setData(image_path)
                        self.img_sheet_model.appendRow(item)
            else:
                # PID selection
                self.logger.debug("PID selected")
                self.logger.debug("clearing session and image sheets")
                self.clear_session_sheet_model()
                self.img_sheet_model.clear()
                self.img_list = []
            self.resize_session_sheet()

    def update_sheet_model(self, search=''):
        """
        Name:     Prida.update_sheet_model
        Inputs:   [optional] str (search)
        Features: Displays all PIDs in the sheet or the PIDs that match the
                  search terms if search terms are given
        Depends:  - load_sheet_headers
                  - resize_sheet
                  - search
        """
        self.logger.debug("updating sheet")
        self.update_pid_auto_model()
        self.sheet_model.clear()
        self.load_sheet_headers()
        terms = search.lower().split()
        for d in self.data_list:
            pid_meta = []
            for key in d:
                pid_meta.append(d[key].lower())
            if self.search(pid_meta, terms):
                pid_item = QStandardItem(d["pid"])

                # Create session rows under each PID:
                for session in self.data.list_sessions(d["pid"]):
                    pid_item.appendRow(QStandardItem(session))

                # Fill the PID row with its attributes:
                pid_list = [pid_item, ]
                for i in sorted(self.pid_attrs.keys()):
                    my_attr = d[self.pid_attrs[i]["key"]]
                    pid_list.append(QStandardItem(my_attr))
                self.sheet_model.appendRow(pid_list)
        self.resize_sheet()

    def update_thumb_sheet_model(self, selection):
        """
        Name:     Prida.update_thumb_sheet_model
        Inputs:   obj (selection)
        Outputs:  None.
        Features: Updates thumb sheet model with icons from the selected
                  session's datasets, saves the full image paths as data, and
                  clears thumb sheet model if no selection is made; handles
                  image orientation
        """
        # Clear the image preview:
        self.view.a_preview.clearBaseImage()
        self.view.a_label.clear()

        if selection.indexes():
            # Retrieve session path (created in update analysis sheet model)
            self.logger.debug("gathering user selection")
            s_path = self.analysis_sheet_model.itemFromIndex(
                selection.indexes()[0]).data()

            # Reset view and lists:
            self.thumb_sheet_model.clear()
            self.img_list = []
            img_counter = 0

            self.logger.debug("gathering image orientation")
            img_orient = self.data.get_attr("img_orient", s_path)
            self.logger.info("#104# %s", img_orient)

            self.logger.debug("reading '%s' datasets", s_path)
            for obj in self.data.list_objects(s_path):
                image_datasets = self.data.list_objects(
                    "%s/%s" % (s_path, obj))
                thumb_path = ""
                image_path = ""
                for img_name in image_datasets:
                    if ".thumb" in img_name:
                        thumb_path = "%s/%s/%s" % (s_path,
                                                   obj,
                                                   img_name)
                    else:
                        for img_type in self.img_types:
                            if img_type in img_name:
                                image_path = "%s/%s/%s" % (s_path,
                                                           obj,
                                                           img_name)

                self.logger.debug("getting data for %s", thumb_path)
                ndarray = self.data.get_dataset(thumb_path)
                if ndarray.size != 0:
                    if img_orient and img_orient != "Original":
                        if img_orient == "Rotated Clockwise":
                            self.logger.debug(
                                "rotating image 90 degrees clockwise")
                            ndarray = numpy.rot90(ndarray, 3)
                        elif img_orient == "Rotated Counter-Clockwise":
                            self.logger.debug(
                                "rotating image 90 deg counter-clockwise")
                            ndarray = numpy.rot90(ndarray)
                        else:
                            self.logger.warning(
                                "failed to match orientation %s",
                                img_orient)
                    image = Image.fromarray(ndarray)
                    qt_image = ImageQt.ImageQt(image)
                    self.img_list.append(qt_image)
                    pix = QPixmap.fromImage(qt_image)

                    self.logger.debug(
                        "adding image icon to thumb sheet model")
                    img_counter += 1
                    item = QStandardItem("%03d" % (img_counter))
                    item.setIcon(QIcon(pix))
                    item.setData(image_path)
                    self.thumb_sheet_model.appendRow(item)
                else:
                    self.logger.warning("no data read from %s", thumb_path)
        else:
            # You made a de-selection (ctrl+click):
            self.logger.debug("encountered de-selection")
            self.thumb_sheet_model.clear()
            self.img_list = []
