#!/usr/bin/python
#
# email_organizer.py
#
# VERSION 1.3.1
#
# LAST EDIT: 2016-03-01
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import datetime
import getpass
import glob
import logging
import os.path
import socket
import smtplib
import zipfile
import tempfile
from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


###############################################################################
# CLASSES:
###############################################################################
class PridaMail:
    """
    Name:     PridaMail
    Features: This class handles the compression and emailing of log files
    History:  VERSION 1.1.2-dev
              - converted to class [15.12.02]
              - created set log dir function [15.12.02]
              - fixed paths in zipfile [15.12.02]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, log_dir='.'):
        """
        Name:     PridaMail.__init__
        Features: Initializes the class.
        Inputs:   [optional] str, directory where log files are saved (log_dir)
        """
        # Create a logger for PridaMail:
        self.logger = logging.getLogger(__name__)
        self.valid = False
        self.log_dir = log_dir

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def archive_log(self, log_file):
        """
        Name:     PridaMail.archive_log
        Inputs:   str, filepath and name for Prida log file
        Outputs:  None.
        Features: Searches for local log files, attempts to email them or
                  create a local archive
        Depends:  - send_file_zipped
                  - zip_file
        """
        self.logger.debug('searching for log files')
        log_name = os.path.join(self.log_dir, log_file)
        my_files = glob.glob(''.join([log_name, '.*']))
        if my_files:
            self.logger.debug('%d log files found', len(my_files))
            if self.valid:
                for logfile in my_files:
                    try:
                        self.logger.debug('preparing to email %s', logfile)
                        self.send_file_zipped(
                            logfile, ['usda.ars.rsa@gmail.com'])
                    except:
                        # Email failed, archive locally
                        self.logger.warning('email failed--archiving locally')
                        zf_name = self.zip_file(logfile)

                        # Check for successful zip before removing file
                        if os.path.isfile(zf_name):
                            os.remove(logfile)
                    else:
                        # Email success, delete local file
                        self.logger.debug('email sent')
                        self.logger.info('deleting log file %s', logfile)
                        os.remove(logfile)
            else:
                self.logger.info('emailer is not validated--no email sent')
        else:
            self.logger.info('no log files found--no email sent')

    def get_file(self, path):
        """
        Name:     PridaMail.get_file
        Inputs:   str, path to text file (path)
        Outputs:  str (data)
        Features: Returns first line of file as a string
        """
        # Check filepath
        if os.path.isfile(path):
            self.logger.debug('file found at %s', path)
            with open(path) as my_file:
                return my_file.read().splitlines()
        else:
            self.logger.warning('file not found at %s, check file path', path)
            return 1

    def send_file_zipped(self, attachment, to):
        """
        Name:     PridaMail.send_file_zipped
        Inputs:   - str, filename (attachment)
                  - list, list of recipient email addresses (to)
        Outputs:  None.
        Features: Sends an SMTP e-mail with zipped text file attachment
        Ref:      "How can I compress a folder and email the compressed file in
                  Python?," StackOverflow, Online: http://stackoverflow.com/
                  questions/169362/how-can-i-compress-a-folder-and-email-the-
                  compressed-file-in-python. Accessed 23 Nov 2015.
        """
        if os.path.isfile(attachment):
            if self.valid:
                try:
                    self.logger.debug('creating temporary zip file')
                    zf = tempfile.TemporaryFile(prefix='mail', suffix='.zip')
                    my_zip = zipfile.ZipFile(zf, 'w')
                    my_zip.write(attachment,
                                 arcname=os.path.basename(attachment))
                    my_zip.close()
                    zf.seek(0)

                    self.logger.debug('creating email message')
                    my_time = datetime.datetime.now().strftime(
                        '%Y-%m-%d %H:%M:%S')
                    themsg = MIMEMultipart('related')
                    themsg['Subject'] = 'Prida.log %s' % my_time
                    themsg['To'] = ', '.join(to)
                    themsg['From'] = self.sender
                    themsg.preamble = 'USDA-ARS Prida log file.\n'

                    msgAlternative = MIMEMultipart('alternative')
                    themsg.attach(msgAlternative)

                    self.logger.debug('creating email body text')
                    body_user = getpass.getuser()
                    body_fqdn = socket.getfqdn()
                    body = "A log file has been generated by %s at %s." % (
                        body_user, body_fqdn)
                    msgText = MIMEText(body, 'plain')
                    msgAlternative.attach(msgText)

                    msg = MIMEBase('application', 'zip')
                    msg.set_payload(zf.read())
                    encoders.encode_base64(msg)
                    msg.add_header('Content-Disposition', 'attachment',
                                   filename=''.join(
                                       [os.path.basename(attachment), '.zip']))
                    themsg.attach(msg)
                    themsg = themsg.as_string()

                    self.logger.debug('preparing to send email')
                    try:
                        smtp = smtplib.SMTP("smtp.gmail.com:587")
                        smtp.ehlo()
                        smtp.starttls()
                        # Requires turning on
                        #   google.com/settings/security/lesssecureapps
                        smtp.login(self.sender, self.cred)
                        smtp.sendmail(self.sender, to, themsg)
                        smtp.close()
                    except:
                        self.logger.error('email send failed')
                        raise
                    else:
                        self.logger.info('email logging complete')
                except:
                    self.logger.error('message failed--no email sent')
                    raise
                else:
                    pass
            else:
                self.logger.warning('emailing not validated--no email sent')
                raise IOError('email not validated')
        else:
            self.logger.warning('no log file found at %s', attachment)
            raise IOError('log file not found')

    def set_log_dir(self, log_dir):
        """
        Name:     PridaMail.set_log_dir
        Inputs:   str, log directory (log_dir)
        Outputs:  None.
        Features: Assigns log directory
        """
        self.log_dir = log_dir

    def validate(self, path='credentials'):
        """
        Name:     PridaMail.validate
        Inputs:   [optional] str, validation file (path)
        Outputs:  None.
        Features: Validates email credentials
        Depends:  get_file
        """
        if os.path.isfile(path):
            self.logger.debug('found file for validation')
            temp = self.get_file(path)
            if temp != 1:
                try:
                    sender, cred = temp
                except:
                    self.logger.warning('failed to validate')
                    self.logger.warning('check contents of file at %s', path)
                    self.valid = False
                    self.sender = None
                    self.cred = None
                else:
                    self.valid = True
                    self.sender = sender
                    self.cred = cred
        else:
            self.logger.info('failed to validate---file not found')
            self.valid = False

    def zip_file(self, my_file):
        """
        Name:     PridaMail.zip_file
        Inputs:   str, log file name (my_file)
        Outputs:  str, zip file name (zf_name)
        Features: Creates a local archive for a given file
        """
        my_time = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        zf_name = "%s.%s.zip" % (my_file, my_time)
        zf = zipfile.ZipFile(zf_name, mode='w')
        try:
            self.logger.debug('archiving log file')
            zf.write(my_file)
        except:
            self.logger.error('zipping failed for log file %s', my_file)
        finally:
            zf.close()
            return zf_name

###############################################################################
# MAIN:
###############################################################################
if __name__ == '__main__':
    my_class = PridaMail()
    my_class.validate()
    my_class.send_file_zipped('prida.log', ['usda.ars.rsa@gmail.com'])
