#!/usr/bin/python
#
# custom.py
#
# VERSION: 1.3.1
#
# LAST EDIT: 2016-03-01
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging

import numpy
import scipy.misc
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPainter
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QColor
import PIL.ImageQt as ImageQt
import PIL.Image as Image

from .utilities import resource_path


###############################################################################
# CLASSES:
###############################################################################
class InteractiveLabel(QLabel):
    """
    Name:     InteractiveLabel
    Features: Click and drag and selection widget
    History:  Version 1.0.1-dev
              - added image filename as default init parameter [15.10.16]
              - PEP8 style fixes [15.11.18]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, stuff, ifile='greeter.jpg'):
        """
        Name:     InteractiveLabel.__init__
        Inputs:   - QtWidgets.QWidget, (stuff)
                  - str, greeter image file (ifile)
        Features: Initializes an InteractiveLabel.
                  * press_pos is the stored mouse press location
                  * clean_img is last image before mouse press
                  * base_img is the original image
                  * active_img is the image currently being used
                  * is_pressed indicates mouse pressed status
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('start.')
        QLabel.__init__(self)
        self.press_pos = None
        self.greeter = resource_path(ifile)
        self.clean_img = QPixmap(self.greeter).scaledToHeight(300)
        self.base_img = QPixmap(self.greeter).scaledToHeight(300)
        self.active_img = None
        self.is_pressed = False
        self.painter = QPainter()
        self.logger.debug('complete.')

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def mousePressEvent(self, event):
        """
        Name:     InteractiveLabel.mousePressEvent
        Input:    (event)
        Output:   None.
        Features: On left mouse click, the mouse location is saved and the
                  pressed status is set to true
        """
        self.logger.debug('start.')
        if event.button() == Qt.LeftButton:
            self.press_pos = event.pos()
            self.is_pressed = True
        self.logger.debug('complete.')

    def mouseMoveEvent(self, event):
        """
        Name:     InteractiveLabel.mouseMoveEvent
        Input:    (event)
        Output:   None.
        Features: While mouse click is held down, reset the active image as the
                  clean image, then draw the new image and set the active image
                  to the label
        """
        self.logger.debug('start.')
        if self.is_pressed:
            self.active_img = self.clean_img.copy()
            self.painter.begin(self.active_img)
            self.painter.fillRect(self.press_pos.x(),
                                  self.press_pos.y(),
                                  event.x() - self.press_pos.x(),
                                  event.y() - self.press_pos.y(),
                                  QColor(128, 128, 255, 128))
            self.painter.end()
            self.setPixmap(self.active_img)
        self.logger.debug('complete.')

    def mouseReleaseEvent(self, event):
        """
        Name:     InteractiveLabel.mouseReleaseEvent
        Features: When left mouse click is released, set the active image as
                  the new clean image. When right mouse click is released, set
                  the shown and clean image to the base image.
        """
        self.logger.debug('start.')
        if event.button() == Qt.LeftButton:
            self.logger.debug("Start: " + str(self.press_pos))
            self.logger.debug("End: " + str(event.pos()))
            if self.is_pressed:
                if (event.x() > self.pixmap().size().width() or
                        event.y() > self.pixmap().size().height() or
                        event.x() < 0 or event.y() < 0):
                    self.setPixmap(self.clean_img)
                else:
                    self.clean_img = self.active_img
                self.is_pressed = False
        else:
            self.setPixmap(self.base_img)
            self.clean_img = self.base_img
        self.logger.debug('complete.')


class IDA(QLabel):
    """
    Name:     IDA (Image Display Area)
    Features: The label that displays images in rescalable size
    History:  Version 1.2.0-dev
              - addressed portrait images in resizeAndSet [15.10.16]
              - created rotate base image left and right functions [15.11.19]
              - added grayscale function [15.12.31]
              - attempted to show grayscale images [15.12.31]
              - using QImage with Format_Indexed8 for grayscale [16.01.04]
              - created resetBaseImage function [16.01.06]
              - added Otsu's threshold to display label
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    num_gray_methods = 4
    gray_methods = {0: "original image",
                    1: "grayscale method 1/3: average",
                    2: "grayscale method 2/3: luminance",
                    3: "grayscale method 3/3: Saravanan (2010)"}

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, arg):
        """
        Name:     IDA.__init__
        Inputs:   (arg)
        Outputs:  None.
        Features: Initializes IDA
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('initializing IDA class')

        QLabel.__init__(self)
        self.gray_count = 0     # index
        self.bin_thresh = 0     # binary image threshold value
        self.base_array = None  # numpy array of original color image
        self.base_img = None    # numpy array of (un)processed image
        self.cur_image = None   # ImageQt.ImageQt

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def clearBaseImage(self):
        """
        Name:     IDA.clearBaseImage
        Inputs:   None.
        Outputs:  None.
        Features: Clears the IDA base image and all labels
        """
        self.logger.debug(
            "clearing base image, base array, and resetting counter")
        self.base_img = None
        self.base_array = None
        self.cur_image = None
        self.gray_count = 0
        self.bin_thresh = 0
        self.clear()

    def increment_graycount(self):
        """
        Name:     IDA.increment_graycount
        Inputs:   None.
        Outputs:  None.
        Features: Increments the grayscale counter by one; loops back to zero
                  when total number of grayscale methods exceeded
        """
        self.gray_count += 1
        if self.gray_count >= self.num_gray_methods:
            self.gray_count = 0
        elif self.gray_count < 0:
            self.gray_count = self.num_gray_methods - 1

    def grayscale(self):
        """
        Name:     IDA.grayscale
        Inputs:   None.
        Outputs:  str, display label (r_message)
        Features: Iterates through the grayscale image processing methods and
                  saves to the base image as duplicate grayscale color bands
        Depends:  - increment_graycount
                  - to_grayscale
                  - resizeAndSet
        """
        if self.base_array is not None:
            self.increment_graycount()
            self.logger.debug("grayscale method counter = %d", self.gray_count)
            self.logger.debug("iterating base image to next grayscale")
            r_message = self.to_grayscale()
            self.resizeAndSet()
            return r_message
        else:
            self.gray_count = 0
            self.logger.warning("base image not set")
            return ""

    def to_grayscale(self):
        """
        Name:     IDA.to_grayscale
        Inputs:   None.
        Outputs:  str, display label (r_message)
        Features: Sets base image to original or grayscale image based on the
                  grayscale counter
        Depends:  - gray_ave
                  - gray_luminance
                  - gray_saravanan
        """
        if self.base_array is not None:
            if self.gray_count == 0:
                self.base_img = self.base_array
                r_message = self.gray_methods[0]
            elif self.gray_count == 1:
                self.base_img = self.gray_ave(self.base_array)
                r_message = self.gray_methods[1]
            elif self.gray_count == 2:
                self.base_img = self.gray_luminance(self.base_array)
                r_message = self.gray_methods[2]
            elif self.gray_count == 3:
                self.base_img = self.gray_saravanan(self.base_array)
                r_message = self.gray_methods[3]
            else:
                self.logger.warning(
                    "counter index out of range %d", self.gray_count)
                self.base_img = self.base_array
                r_message = "original image"
            return r_message
        else:
            return ""

    def gray_ave(self, nd_array):
        """
        Name:     IDA.gray_ave
        Inputs:   numpy.ndarray, hxwxb color image array (nd_array)
        Outputs:  numpy.ndarray, hxwxb grayscale image array (my_gray)
        Features: Converts color image to grayscale based on the arithmetic
                  average of the color bands; note the grayscale image has the
                  same depth as the original color image
        """
        self.logger.info("using average method")
        try:
            _, _, b = nd_array.shape
        except:
            self.logger.warning("failed to unpack color bands")
            self.logger.warning("image is probably already grayscale")
        else:
            if b != 3:
                self.logger.warning("image not color! Found %d bands", b)
            else:
                my_gray = numpy.copy(nd_array)
                R = nd_array[:, :, 0]
                G = nd_array[:, :, 1]
                B = nd_array[:, :, 2]
                A = ((1.0*R) + (1.0*G) + (1.0*B))/3.0
                my_gray[:, :, 0] = A
                my_gray[:, :, 1] = A
                my_gray[:, :, 2] = A
                return my_gray

    def gray_luminance(self, nd_array):
        """
        Name:     IDA.gray_luminance
        Inputs:   numpy.ndarray, hxwxb color image array (nd_array)
        Outputs:  numpy.ndarray, hxwxb grayscale image array (my_gray)
        Features: Converts color image to grayscale based on a weighted sum
                  of the color bands as a calculation of luminance
        """
        self.logger.info("using luminance method")
        try:
            _, _, b = nd_array.shape
        except:
            self.logger.warning("failed to unpack color bands")
            self.logger.warning("image is probably already grayscale")
        else:
            if b != 3:
                self.logger.warning("image not color! Found %d bands", b)
            else:
                my_gray = numpy.copy(nd_array)
                R = nd_array[:, :, 0]
                G = nd_array[:, :, 1]
                B = nd_array[:, :, 2]
                Y = (0.299*R) + (0.587*G) + (0.114*B)
                my_gray[:, :, 0] = Y
                my_gray[:, :, 1] = Y
                my_gray[:, :, 2] = Y
                return my_gray

    def gray_saravanan(self, nd_array):
        """
        Name:     IDA.gray_saravanan
        Inputs:   numpy.ndarray, hxwxb color image array (nd_array)
        Outputs:  numpy.ndarray, hxwxb grayscale image array (my_gray)
        Features: Converts color image to grayscale based on Saravanan (2010);
                  note that the grayscale image is the same depth as the
                  original color image
        Ref:      Saravanan, C. 2010. Color image to grayscale image
                  conversion. In 2nd Intl. Computer Engineering and
                  Applications, 196--199.
        """
        self.logger.info("using Saravanan (2010) grayscale method")
        try:
            _, _, b = nd_array.shape
        except:
            self.logger.warning("failed to unpack color bands")
            self.logger.warning("image is probably already grayscale")
        else:
            if b != 3:
                self.logger.warning("image not color! Found %d bands", b)
            else:
                my_gray = numpy.copy(nd_array)
                R = nd_array[:, :, 0]
                G = nd_array[:, :, 1]
                B = nd_array[:, :, 2]
                Y = (0.299*R) + (0.587*G) + (0.114*B)
                U = (B - Y)*0.565
                V = (R - Y)*0.713
                UV = U + V
                R4 = (1.0*R)/3.0
                G4 = (1.0*G)/3.0
                B4 = (1.0*B)/3.0
                I1 = (R4 + G4 + B4 + UV)*0.25
                my_gray[:, :, 0] = I1*3.0
                my_gray[:, :, 1] = I1*3.0
                my_gray[:, :, 2] = I1*3.0
                return my_gray

    def binary(self):
        """
        Name:     IDA.binary
        Inputs:   None.
        Outputs:  str, display label (r_message)
        Features: Sets the base image to binary image based on the current
                  grayscale method
        Depends:  - check_binary        - increment_graycount
                  - otsu                - resetBaseImage
                  - resizeAndSet        - to_grayscale
        """
        if self.base_img is not None:
            self.increment_graycount()
            self.logger.debug("grayscale method counter = %d", self.gray_count)
            self.logger.debug("iterating base image to next grayscale")
            self.to_grayscale()
            self.resizeAndSet()
            if self.cur_image.isGrayscale():
                try:
                    my_gray = self.check_binary(self.base_img)
                    my_otsu = self.otsu(my_gray)
                except:
                    self.logger.exception("binary conversion failed!")
                    self.resetBaseImage()
                else:
                    self.logger.info("creating binary image")
                    my_binary = numpy.copy(self.base_img)
                    my_binary[:, :, 0] = my_otsu
                    my_binary[:, :, 1] = my_otsu
                    my_binary[:, :, 2] = my_otsu

                    self.logger.info("setting binary image as base")
                    self.base_img = my_binary
                    r_message = "Otsu threshold %d using %s" % (
                        self.bin_thresh,
                        self.gray_methods[self.gray_count]
                    )
                    self.resizeAndSet()
                    return r_message
            else:
                return self.gray_methods[self.gray_count]
        else:
            self.gray_count = 0
            self.logger.warning("base image not set")

    def check_binary(self, nd_array):
        """
        Name:     IDA.check_binary
        Inputs:   numpy.ndarray, grayscale image array (nd_array)
        Outputs:  numpy.ndarray, two-dimensional integer array (my_array)
        Features: Returns a two-dimensional grayscale array that has been
                  value checked for binary processing
        """
        max_val = 255
        min_val = 0

        # Check image shape:
        try:
            h, w, b = nd_array.shape
        except:
            self.logger.warning("failed to unpack color bands")
            try:
                h, w = nd_array.shape
            except:
                self.logger.error("failed to read array shape")
            else:
                self.logger.info("unpacked grayscale image")

                # Check grayscale values:
                my_array = nd_array.astype("uint8")
                my_max = my_array.max()
                my_min = my_array.min()
                if my_max > max_val:
                    self.logger.error("out of range value found %d", my_max)
                    raise IOError("Out of range value found in image array")
                elif my_min < min_val:
                    self.logger.error("out of range value found %d", my_min)
                    raise IOError("Out of range value found in image array")
                else:
                    return my_array
        else:
            if b == 3:
                self.logger.info("unpacked RGB image, assuming grayscale")

                # Check grayscale values:
                my_array = nd_array[:, :, 0].astype("uint8")
                my_max = my_array.max()
                my_min = my_array.min()
                if my_max > max_val:
                    self.logger.error("out of range value found %d", my_max)
                    raise IOError("Out of range value found in image array")
                elif my_min < min_val:
                    self.logger.error("out of range value found %d", my_min)
                    raise IOError("Out of range value found in image array")
                else:
                    return my_array
            else:
                self.logger.error("unpacked %d color bands?!", b)

    def otsu(self, nd_array):
        """
        Name:     IDA.otsu
        Inputs:   numpy.ndarray, two-dimensional integer array (nd_array)
        Outputs:  numpy.ndarray, two-dimensional integer array (my_binary)
        Features: Returns a binary image based on the Otsu (1979) method
        Ref:      Otsu, N. (1979) A threshold selection method from gray-level
                  histograms, IEEE Transactions on Systems, Man, and
                  Cybernetics, 9(1), 62--66.
        """
        # Otsu's max and min level values:
        l_max = 255
        l_min = 0

        # Otsu's number of pixels at each level, n, total number of pixels, N,
        # and normalized probability distribution, p
        n = numpy.bincount(nd_array.astype("uint8").ravel(), minlength=256)
        N = n.sum()
        p = n.astype("f4")/N

        # Otsu's mean pixel value at each level, mu:
        mu = [i*p[i] for i in range(len(p))]
        mu = numpy.array(mu)

        # Otsu's probabilities of class occurrence, wk [Eq. 6], class mean
        # values, uk [Eq. 7], and the total mean level of the original
        # image, ut [Eq. 8]:
        wk = p.cumsum()
        uk = mu.cumsum()
        ut = mu.sum()

        # Calculate the between-class variances, vb [Eq. 18]:
        vb = ut*wk[:-1]
        vb -= uk[:-1]
        vb = numpy.power(vb, 2.0)
        denom = 1. - wk[:-1]
        denom *= wk[:-1]
        try:
            vb /= denom
        except:
            self.logger.error("encountered division error")
            raise
        else:
            # Search for maximum between-class variance, i.e., threshold value:
            k = numpy.where(vb == vb.max())[0]
            if len(k) == 0:
                self.logger.error("failed to find maximum value")
                raise ValueError("No maximum value found in Otsu method")
            elif len(k) > 1:
                self.logger.warning("found %d maximum values", len(k))
                self.logger.warning("using first occurrence")
            thresh = k[0]
            self.bin_thresh = thresh
            self.logger.info("Otsu threshold set at %d", int(thresh))

            # Find the foreground and background pixel values:
            my_binary = numpy.copy(nd_array).astype("f4")
            fore_idx = numpy.where(my_binary > thresh)
            back_idx = numpy.where(my_binary <= thresh)

            # Set foreground white and background black:
            my_binary[fore_idx] /= my_binary[fore_idx]
            my_binary[fore_idx] *= l_max
            my_binary[back_idx] *= l_min

            return my_binary.astype("uint8")

    def resetBaseImage(self):
        """
        Name:     IDA.resetBaseImage
        Inputs:   None.
        Outputs:  None.
        Features: Resets base image to base array and grayscale counter
        """
        if self.base_array is not None:
            self.logger.debug("resetting base image")
            self.base_img = self.base_array
            self.gray_count = 0
            self.resizeAndSet()

    def resizeAndSet(self):
        """
        Name:     IDA.resizeAndSet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes and sets ImageQt from base image
        """
        # Calculate the image aspect ratio:
        try:
            h, w, _ = self.base_img.shape
        except:
            self.logger.warning("something is awry")
        else:
            self.logger.info("resizing image")
            ratio = (1.0*w)/h
            self.logger.debug("IDA resize ratio set to %f", ratio)

            # Calculate the scaling factor for the image:
            height = float(self.width())/ratio
            self.logger.debug("IDA required height is %f", height)
            if height > self.height():
                self.logger.debug("required height is larger than available")
                width = float(self.height())*ratio
                self.logger.debug("IDA required width is %f", width)
                s_factor = float(width)/w
            else:
                s_factor = float(height)/h
            self.logger.debug("IDA resize factor set to %f", s_factor)

            self.logger.debug("resizing image...")
            nd_array = scipy.misc.imresize(self.base_img, s_factor, "bilinear")
            self.logger.debug("...resize complete")

            image = Image.fromarray(nd_array)
            self.logger.debug(
                "created PIL image from array using mode %s", image.mode)

            self.logger.debug("setting ImageQt...")
            qt_image = ImageQt.ImageQt(image)
            self.cur_image = qt_image
            self.logger.debug(
                "ImageQt is grayscale %s", qt_image.isGrayscale())

            self.logger.debug("setting QPixmap...")
            pix = QPixmap.fromImage(self.cur_image)
            self.setPixmap(pix)
            self.logger.debug("...pixmap set is complete")

    def resizeEvent(self, e):
        """
        Name:     IDA.resizeEvent
        Inputs:   event (e)
        Outputs:  None.
        Features: Upon resize events, resize and set image
        Depends:  resizeAndSet
        """
        self.logger.debug('start.')
        if self.base_img is not None:
            self.resizeAndSet()
        self.logger.debug('complete.')

    def rotateBaseImageLeft(self):
        """
        Name:     IDA.rotateBaseImageLeft
        Inputs:   None.
        Outputs:  None.
        Features: Rotates base image 90 degrees counter-clockwise
        Depends:  resizeAndSet
        """
        self.logger.info('Button Clicked')
        if self.base_img is not None:
            self.logger.debug('rotating')
            self.base_img = numpy.rot90(self.base_img)
            self.resizeAndSet()

    def rotateBaseImageRight(self):
        """
        Name:     IDA.rotateBaseImageRight
        Inputs:   None.
        Outputs:  None.
        Features: Rotates base image 90 degrees clockwise
        Depends:  resizeAndSet
        """
        self.logger.info("Button Clicked")
        if self.base_img is not None:
            self.logger.debug("rotating")
            self.base_img = numpy.rot90(self.base_img, 3)
            self.resizeAndSet()

    def setBaseImage(self, ndarray_full):
        """
        Name:     IDA.setBaseImage
        Inputs:   numpy.ndarray, image (ndarray_full)
        Outputs:  None.
        Features: Sets and saves base image for viewing
        Depends:  resizeAndSet
        """
        self.logger.debug("setting base image")
        self.base_img = ndarray_full
        self.base_array = numpy.copy(ndarray_full)
        self.resizeAndSet()
