#!/usr/bin/python
#
# Prida.py
#
# VERSION: 1.2.2
#
# LAST EDIT: 2016-07-19
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import glob
import logging
import logging.handlers
import os
import sys
try:
    # Python 2
    from urllib import urlretrieve
except:
    # Python 3
    from urllib.request import urlretrieve

import PyQt5.uic as uic
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QAbstractItemView
from PyQt5.QtWidgets import QCompleter
from PyQt5.QtWidgets import QErrorMessage
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QStringListModel
from PyQt5.QtCore import QThread
from PyQt5.QtCore import QSize
from PyQt5.QtCore import QDate
from PyQt5.QtCore import Qt
import PIL.ImageQt as ImageQt
import PIL.Image as Image
import zipfile

from email_organizer import PridaMail
from hdf_organizer import PridaHDF
from utilities import get_ctime
from utilities import resource_path
from utilities import read_exif_tags
from utilities import conf_parser
from global_conf import GlobalConf
from global_conf import CustomFilter

###############################################################################
# DETECT ANALYSIS MODE:
###############################################################################
analysis_mode = True

###############################################################################
# DETECT HARDWARE MODE:
###############################################################################
try:
    from hardware.imaging import Imaging
    hardware_mode = True
except ImportError:
    # MODE 1 INDICATES ERROR LIKELY FROM NONEXISTENCE OF HARDWARE
    hardware_mode = False

###############################################################################
# GLOBAL VARIABLES:
###############################################################################
# PRIDA VERSION
PRIDA_VERSION = "Prida 1.2.2"
PRIDA_VERSION_EXPL = PRIDA_VERSION + " - Explorer Mode"

# PAGE NUMBER ENUMS FOR mainwindow.ui
MAIN_MENU = 0
VIEWER = 1
INPUT_EXP = 2
RUN_EXP = 3
FILE_SEARCH = 4
ANALYSIS = 5

# PAGE NUMBER ENUMS FOR SHEET/IDA
SHEET_VIEW = 0
IDA_VIEW = 1

# FILE & SESSION PATHS FOR IMAGES AND HDF FILE
filepath = ''
session_path = ''
is_path = False


###############################################################################
# FUNCTIONS:
###############################################################################
def create_config_file(prida_dir, file_name):
    """
    Name:     create_config_file
    Inputs:   str, config file name with path (file_path)
    Outputs:  None.
    Features: Create a default configuration file at given location.
    """
    config_txt = (
        "HARDWARE.PIEZO      PWM_CHANNEL             0\n"
        "HARDWARE.PIEZO      VOLUME                  100.0\n"
        "HARDWARE.CAMERA     IMAGE_DIR              '%s'\n"
        "HARDWARE.LED        RED_LED_CHANNEL         15\n"
        "HARDWARE.LED        GREEN_LED_CHANNEL       14\n"
        "HARDWARE.LED        BLUE_LED_CHANNEL        1\n"
        "HARDWARE.LED        LED_TYPE                'CC'\n"
        "HARDWARE.IMAGING    GEAR_RATIO              1.0\n"
        "HARDWARE.HAT        RPM                     4.0\n"
        "HARDWARE.HAT        DEGREES_PER_STEP        1.8\n"
        "HARDWARE.HAT        MICROSTEPS_PER_STEP     4\n"
        "HARDWARE.HAT        PORT_NUMBER             1\n"
        "GLOBAL_CONF         LOG_DIR                 '%s'\n"
        "GLOBAL_CONF         CREDENTIAL_DIR          '%s'\n"
        "GLOBAL_CONF         DICTIONARY_DIR          '%s'\n"
        "GLOBAL_CONF         LOG_FILENAME            'prida.log'\n"
        "GLOBAL_CONF         CREDENTIAL_FILENAME     'crendentials'\n"
        "GLOBAL_CONF         DICTIONARY_FILENAME     'dictionary.txt'\n"
        "GLOBAL_CONF         LOG_LEVEL               'info'\n"
        "HDF_ORGANIZER       COMPRESS_LV             6\n"
        % (os.path.join(prida_dir, 'photos'),
           os.path.join(prida_dir, "logs"),
           prida_dir, prida_dir)
    )
    file_path = os.path.join(prida_dir, file_name)
    try:
        with open(file_path, 'w') as my_file:
            my_file.write(config_txt)
    except:
        raise IOError("Can not write to config file.")


def create_dictionary(prida_dir, file_name, online=False):
    """
    Name:     create_dictionary
    Inputs:   - str, prida directory (prida_dir)
              - str, path to and name of output text file (file_path)
              - [optional] bool, whether to download from internet (online)
    Outputs:  None.
    Features: Creates a dictionary file for the genus species suggestion text
              box based on the USDA PLANTS database
    Depends:  write_default_dictionary
    Ref:      USDA, NRCS. 2015. The PLANTS Database (http://plants.usda.gov).
              National Plant Data Team, Greensboro, NC 27401-4901 USA
    """
    # The USDA PLANTS dictionary file address:
    dict_file = ("https://dl.dropboxusercontent.com"
                 "/u/468281225/Prida/dictionary.txt.zip")
    file_path = os.path.join(prida_dir, file_name)

    if online:
        my_path, my_filename = os.path.split(file_path)
        try:
            logging.debug("retrieving zip archive from url")
            urlretrieve(dict_file, ''.join([file_path, '.zip']))
            with zipfile.ZipFile(''.join([file_path, '.zip'])) as my_zip:
                logging.debug("extracting from local zip archive")
                my_zip.extract(my_filename, path=my_path)
            if os.path.isfile(file_path):
                logging.debug(
                    "extraction successful, removing local zip archive")
                os.remove(''.join([file_path, '.zip']))
        except:
            logging.exception("failed to write usda-plants database to file")
            write_default_dictionary(file_path)
    else:
        write_default_dictionary(file_path)


def write_default_dictionary(file_path):
    """
    Name:     write_default_dictionary
    Inputs:   str, path to and name of output text file (file_path)
    Outputs:  None.
    Features: Creates a default dictionary file for the genus species
              suggestion text box based on the USDA PLANTS database
    """
    # Default dictionary file:
    diction_txt = (
        "[BRNA,]:Canola (Brassica napus)\n"
        "[CUSA4,]:Cucumber (Cucumis sativus)\n"
        "[GLMA4,]:Soybean (Glycine max)\n"
        "[ORSA,]:Rice (Oryza sativa)\n"
        "[SOLY2,]:Tomato (Solanum lycopersicum)\n"
        "[SOBI2,]:Sorghum (Sorghum bicolor)\n"
        "[ZEMA,]:Corn (Zea mays)"
    )

    try:
        logging.debug("writing default dictionary")
        with open(file_path, 'w') as my_file:
            my_file.write(diction_txt)
    except:
        logging.exception("failed to write default dictionary file")


def create_local_prida(prida_dir,
                       conf_file="prida.config",
                       dict_file="dictionary.txt"):
    """
    Name:     create_local_prida
    Inputs:   - str, prida directory (prida_dir)
              - [optional] str, configuration file name (conf_file)
              - [optional] str, dictionary file name (dict_file)
    Outputs:  int, return code
              > 1 ...... create directory failed
              > -1 ..... create configuration file/dictionary failed
              > 9999 ... directory already exists
    Features: Checks for prexistance of local Prida directory, creates one if
              one does not already exists, and returns a value representing
              the result.
    """
    if not os.path.isdir(prida_dir):
        try:
            os.mkdir(prida_dir)
            os.mkdir(os.path.join(prida_dir, "logs"))
            os.mkdir(os.path.join(prida_dir, "photos"))
        except:
            # Failed to create Prida directory
            return 1
        else:
            try:
                create_config_file(prida_dir, conf_file)
                create_dictionary(prida_dir, dict_file)
            except:
                # Failed to create configuration/dictionary files
                return -1
            else:
                return 0
    else:
        # Directory already exists!
        return 9999


def exit_app(my_app):
    """
    Name:     exit_app
    Inputs:   object, QApplication (my_app)
    Outputs:  None
    Features: Graceful exiting of QApplication
    """
    logging.info('exiting QApplication')
    my_app.quit()


def exit_log(file_handler):
    """
    Name:     exit_log
    Inputs:   RotatingFileHandler (file_handler)
    Outputs:  None.
    Features: Roll over a RotatingFileHandler to a new log file
    """
    file_handler.doRollover()
    logging.info('Rolling over to new log file')


###############################################################################
# CLASSES:
###############################################################################
class HardwareThread(QThread):
    """
    Name:     HardwareThread
    Features: Thread for running hardware
    History:  Version 1.2.1
              - changed attrs_path to a_path [15.11.02]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def run(self):
        """
        Name:     HardwareThread.run
        Inputs:   None.
        Outputs:  None.
        Features: Run imaging sequence and save image and metadata to HDF file.
        Depends:  - filepath (str, global var)
                  - is_path (bool, global var)
                  - hdf (obj, global var)
        """
        global filepath, is_path
        if hardware_mode:
            output_args = imaging.run_sequence()
            is_path = output_args[0]
            filepath = output_args[1]
            datestamp = output_args[2]
            timestamp = output_args[3]
            anglestamp = output_args[4]
            anglestamp = os.path.splitext(anglestamp)[0]
            basename = os.path.basename(filepath)
            name = os.path.splitext(basename)[0]
            a_path = os.path.join(session_path, name, basename)
            if is_path:
                try:
                    hdf.save_image(session_path, filepath)
                except:
                    logging.exception("failed to create dataset %s", filepath)
                else:
                    try:
                        hdf.set_attr("Date", datestamp, a_path)
                        hdf.set_attr("Time", timestamp, a_path)
                        hdf.set_attr("Angle", anglestamp, a_path)
                        hdf.set_attr(
                            "Height", imaging.camera.image_height, a_path)
                        hdf.set_attr(
                            "Width", imaging.camera.image_width, a_path)
                        hdf.set_attr(
                            "Orientation", imaging.camera.orientation, a_path)
                    except:
                        logging.exception("failed to save datset attributes")
                    hdf.save()


class Prida(QApplication):
    """
    Prida, a QApplication.

    Name:      Prida
    History:   Version 1.2.1-r1
               - PEP8 style fixes [15.11.02]
               - cleared RUN_EXP image between sessions [15.11.03]
               - added HDF5 filename to Prida window title and About [15.11.18]
               - added multifile selection in file search [15.11.18]
               - added defaults to session user and email attrs [15.11.18]
               - added defaults to all pid & session attrs [15.11.18]
               - created a Prida logger [15.11.18]
               - created init functions for pid & session attrs [15.11.18]
               - started PID autofill function [15.11.18]
               - moved set defaults to open and new hdf [15.11.19]
               - connected rotate left and right IDA buttons [15.11.19]
               - created log messages [15.11.19]
                 + view changes and button clicks assigned at 'info' level
               - created read and save exif tag functions [15.11.19]
               - addressed some PEP8 style fixes [15.11.28]
               - added at exit register to app.quit() [15.12.07]
               - created class attribute preview [15.12.10]
               - preview only occurs for first image taken [15.12.10]
               - genus species Qt field changed to QLineEdit [15.12.11]
               - added auto compteter for genus species field [15.12.11]
               - added search for global dictionary file [15.12.11]
               - reinstated preview of first image taken [15.12.21]
               - added QErrorMessage [15.12.22]
               - added analysis tools [15.12.30]
               - created analyze gray [16.01.05]
               - clear IDA preview on de-selection [16.01.05]
               - created analyze binary [16.01.05]
               - added QErrorMessages to export [16.01.06]
               - added QErrorMessage to edit field [16.01.06]
               - connected undo analysis button [16.01.06]
               - created compress HDF file function [16.01.06]
               - moved read exif tags function to utilities [16.01.07]
               - added image preview during import [16.01.07]
               - handle empty arrays in update session sheet model [16.01.11]
               - changed dictionary auto model to case insensitive [16.01.11]
               - changed PID autofill [16.01.11]
                 + completes on mouse-click or return on suggestion (not tab)
               - image date and time attributes saved on import [16.01.13]
               - updated meta data handling [16.03.15]
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    pid_attrs = {}
    session_attrs = {}

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Prida class initialization.

        Name:     Prida.__init__
        Inputs:   None.
        Outputs:  None.
        Depends:  - load_session_sheet_headers     - new_hdf
                  - new_session                    - open_hdf
                  - resize_sheet                   - show_ida
                  - to_sheet                       - update_progress
                  - update_session_sheet_model     - update_sheet_model
                  - set_defaults                   - about
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # Create a logger for Prida:
        self.logger = logging.getLogger(__name__)

        # Create a class error message
        self.logger.debug("initializing QErrorMessage")
        self.error = QErrorMessage()

        # Initialize PID and session attribute dictionaries:
        self.logger.debug("initializing PID and session attributes")
        self.init_pid_attrs()
        self.init_session_attrs()
        self.sheet_items = len(self.pid_attrs)
        self.session_sheet_items = len(self.session_attrs)

        # Check that the necessary files exist:
        # NOTE: mainwindow.ui depends on class definitions from custom.py
        self.logger.debug("checking for required resources")
        self.main_ui = resource_path("mainwindow.ui")
        self.user_ui = resource_path("input_user.ui")
        self.about_ui = resource_path("about.ui")
        self.greeter = resource_path("greeter.jpg")
        self.gs_dict = os.path.join(dict_dir, dict_filename)

        if (not os.path.isfile(self.main_ui) or
                not os.path.isfile(self.user_ui) or
                not os.path.isfile(self.about_ui)):
            self.logger.error("missing required ui files!")
            raise IOError("Error! Missing required ui files!")
        if not os.path.isfile(self.greeter):
            self.logger.error("missing welcome screen image!")
            raise IOError("Error! Missing welcome screen image!")
        if not os.path.isfile(self.gs_dict):
            self.logger.warning("missing dictionary file!")

        # Load main window GUI to the QMainWindow:
        self.logger.debug("loading the main UI file")
        self.view = uic.loadUi(self.main_ui, self.base)

        # Set the welcome image and icon:
        self.logger.debug("setting the greeter image")
        self.view.welcome.setPixmap(QPixmap(self.greeter).scaledToHeight(300))

        # Create the VIEWER sheet model (where PIDs and sessions are listed)
        self.logger.debug("creating sheet model")
        self.sheet_model = QStandardItemModel()
        self.view.sheet.setModel(self.sheet_model)
        self.view.sheet.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.sheet.collapsed.connect(lambda: self.resize_sheet())
        self.view.sheet.expanded.connect(lambda: self.resize_sheet())
        self.view.sheet.setAnimated(True)

        # Create the VIEWER session sheet model (for session properties)
        self.logger.debug("creating session sheet model")
        self.session_sheet_model = QStandardItemModel()
        self.view.session_sheet.setModel(self.session_sheet_model)
        self.view.session_sheet.setRootIsDecorated(False)
        self.load_session_sheet_headers()

        # Create FILE_SEARCH sheet model (where files + PIDs are listed)
        self.logger.debug("creating search sheet model")
        self.search_sheet_model = QStandardItemModel()
        self.view.file_search_sheet.setModel(self.search_sheet_model)
        self.load_file_search_sheet_headers()

        # Create the FILE_SEARCH file browse model (for displaying files)
        self.logger.debug("creating file sheet model")
        self.file_sheet_model = QStandardItemModel()
        self.view.file_browse_sheet.setModel(self.file_sheet_model)
        self.load_file_browse_sheet_headers()

        # Create VIEWER image sheet model (for thumbnail previewer):
        self.logger.debug("creating image sheet model")
        self.img_sheet_model = QStandardItemModel()
        self.view.img_select.setModel(self.img_sheet_model)
        self.view.img_select.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.img_select.setIconSize(QSize(100, 100))

        if hardware_mode:
            # Hardware Mode Decorator:
            self.prida_title = PRIDA_VERSION
            self.base.setWindowTitle(PRIDA_VERSION)
            self.logger.warning("Hardware Mode Enabled")

            # Spawn a hardware thread if mode is enabled:
            self.logger.debug("creating hardware thread")
            self.hardware_thread = HardwareThread()
            self.hardware_thread.finished.connect(self.update_progress)

            # Disable "Import Data" button (only allow "Create Session")
            # and set VIEWER action for "OK" and "Create Session" buttons:
            self.view.import_data.setEnabled(False)
            self.view.c_buttons.accepted.connect(self.new_session)
            self.view.create_session.clicked.connect(
                lambda: self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
            )

            # Set default values based on imaging's camera
            self.session_attrs[8]["def_val"] = imaging.camera.make
            self.session_attrs[9]["def_val"] = imaging.camera.model
            self.session_attrs[10]["def_val"] = imaging.camera.exposure_time
            self.session_attrs[11]["def_val"] = imaging.camera.aperture
            self.session_attrs[12]["def_val"] = imaging.camera.iso_speed
        else:
            # Explorer Mode Decorator:
            self.prida_title = PRIDA_VERSION_EXPL
            self.base.setWindowTitle(PRIDA_VERSION_EXPL)
            self.logger.warning("Preview Mode Enabled")

            # Disable "Create Session" button, set VIEWER action for
            # "Import Data" button & INPUT_EXP action for "OK" button:
            self.view.create_session.setEnabled(False)
            self.view.import_data.clicked.connect(self.import_data)
            self.view.c_buttons.accepted.connect(self.import_session)

        if analysis_mode:
            # Connect analysis button:
            self.logger.warning("Analysis Mode Enabled")
            self.view.perform_analysis.clicked.connect(self.analysis)

            # Create ANALYSIS session sheet:
            self.logger.debug("creating analysis sheet model")
            self.analysis_sheet_model = QStandardItemModel()
            self.view.a_session.setModel(self.analysis_sheet_model)
            self.view.a_session.setEditTriggers(
                QAbstractItemView.NoEditTriggers)
            self.load_analysis_sheet_headers()

            # Creating ANALYSIS session sheet selection model:
            self.analysis_select_model = self.view.a_session.selectionModel()
            self.analysis_select_model.selectionChanged.connect(
                self.update_thumb_sheet_model
            )

            # Create ANALYSIS thumb sheet:
            self.logger.debug("creating analysis thumb sheet model")
            self.thumb_sheet_model = QStandardItemModel()
            self.view.a_thumb.setModel(self.thumb_sheet_model)
            self.view.a_thumb.setEditTriggers(QAbstractItemView.NoEditTriggers)
            self.view.a_thumb.setIconSize(QSize(125, 125))

            # Creating ANALYSIS thumb sheet selection model:
            self.thumb_select_model = self.view.a_thumb.selectionModel()
            self.thumb_select_model.selectionChanged.connect(self.show_preview)

            # Set ANALYSIS actions:
            self.view.a_menu.clicked.connect(self.back_to_menu)
            self.view.a_about.clicked.connect(self.about)
            self.view.a_save.clicked.connect(self.back_to_sheet)
            self.view.a_undo.clicked.connect(self.analyze_undo)
            self.view.a_gray.clicked.connect(self.analyze_gray)
            self.view.a_thresh.clicked.connect(self.analyze_binary)
            self.view.a_invert.clicked.connect(self.tool_not_enabled)
            self.view.a_rLeft.clicked.connect(
                self.view.a_preview.rotateBaseImageLeft)
            self.view.a_rRight.clicked.connect(
                self.view.a_preview.rotateBaseImageRight)
            self.view.a_search.textEdited.connect(
                self.update_analysis_sheet_model)
        else:
            self.logger.warning("Analysis Mode Disabled")
            self.view.perform_analysis.setEnabled(False)

        # Set MAIN_MENU actions for "New," "Open," & "Search" buttons:
        self.view.new_hdf.clicked.connect(self.new_hdf)
        self.view.open_hdf.clicked.connect(self.open_hdf)
        self.view.search_hdf.clicked.connect(
            lambda: self.view.stackedWidget.setCurrentIndex(FILE_SEARCH)
        )

        # Set VIEWER actions for "Export Data," "Main Menu," "About," and
        # "Edit" buttons and for edited search text;
        self.view.aboutButton.clicked.connect(self.about)
        self.view.export_data.clicked.connect(self.export)
        self.view.make_edit.clicked.connect(self.edit_field)
        self.view.menuButton.clicked.connect(self.back_to_menu)
        self.view.saveButton.clicked.connect(self.compress_hdf)
        self.view.search.textEdited.connect(self.update_sheet_model)

        # Set IDA_SHEET actions for "Back to Spreadsheet," "Rotate Left," and
        # "Rotate Right" buttons:
        self.view.to_sheet.clicked.connect(self.to_sheet)
        self.view.rotateLeft.clicked.connect(self.view.ida.rotateBaseImageLeft)
        self.view.rotateRight.clicked.connect(
            self.view.ida.rotateBaseImageRight)

        # Set INPUT_EXP action for "Cancel" button:
        self.view.c_buttons.rejected.connect(self.cancel_input)

        # Create selection models for VIEWER sheet and img_sheet and for
        # FILE_SELECT file_browser_sheet:
        self.logger.debug("creating selection models")
        self.sheet_select_model = self.view.sheet.selectionModel()
        self.img_sheet_select_model = self.view.img_select.selectionModel()
        self.file_select_model = self.view.file_browse_sheet.selectionModel()

        # Set selection model actions for selection changes:
        self.img_sheet_select_model.selectionChanged.connect(self.show_ida)
        self.sheet_select_model.selectionChanged.connect(
            self.update_session_sheet_model
        )

        # Set FILE_SEARCH actions for "Main Menu," "+/-," and "Explore" buttons
        # and action for when search text is edited:
        self.view.menuButton2.clicked.connect(self.back_to_menu)
        self.view.add_file.clicked.connect(self.add_search_file)
        self.view.rm_file.clicked.connect(self.rm_search_file)
        self.view.explore_file.clicked.connect(self.explore_file)
        self.view.f_search.textEdited.connect(self.update_file_search_sheet)

        # Create a auto-completion model for PIDs and set action for edited
        # PID text:
        self.logger.debug("creating PID auto-completion model")
        self.pid_completer = QCompleter()
        self.view.c_pid.setCompleter(self.pid_completer)
        self.pid_auto_model = QStringListModel()
        self.pid_completer.setModel(self.pid_auto_model)
        self.pid_completer.activated.connect(self.autofill_pid)

        # Create a auto-completion model for dictionary fields:
        self.logger.debug("creating dictionary auto-completion model")
        self.dict_completer = QCompleter()
        self.view.c_gen_sp.setCompleter(self.dict_completer)
        self.dict_auto_model = QStringListModel()
        self.dict_completer.setModel(self.dict_auto_model)
        self.dict_completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.update_dict_auto_model()

        # Initialize the search dictionaries, list of PID metadata, list of
        # thumbnail images, session create/edit boolean, input field defaults,
        # whether an image is being previewed, and the current image:
        self.search_files = {}
        self.search_data = {}
        self.ps_list = {}
        self.data_list = []
        self.img_list = []
        self.editing = False
        self.preview = False
        self.current_img = None        # not currently used

        # Set exit register:
        atexit.register(exit_app, self)

        self.base.show()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def about(self):
        """
        Name:     Prida.about
        Inputs:   None.
        Outputs:  None.
        Features: Produces the file's About popup dialog box
        """
        self.logger.info("Button Clicked")
        self.logger.debug("loading UI file")
        popup = uic.loadUi(self.about_ui)

        self.logger.debug("getting about info")
        overview = hdf.get_about()

        # Set popup values to default string if get_about fails:
        self.logger.debug("assigning UI fields")
        popup.author_about.setText(overview.get("Author", "Unknown"))
        popup.contact_about.setText(overview.get("Contact", "Unknown"))
        popup.plants_about.setText(overview.get("Plants", "?"))
        popup.sessions_about.setText(overview.get("Sessions", "?"))
        popup.photos_about.setText(overview.get("Photos", "?"))
        popup.summary_about.setText(overview.get("Summary", "?"))
        popup.setWindowTitle("About: %s" % (hdf.basename()))
        if popup.exec_():
            popup.repaint()

    def add_search_file(self):
        """
        Name:     Prida.add_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Adds user-defined HDF5 files to search files dictionary and
                  saves the files' PID attributes to search data dictionary
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.info("Button Clicked")
        self.logger.debug("requesting search files")
        items = QFileDialog.getOpenFileNames(None,
                                             "Select one or more files",
                                             os.path.expanduser("~"),
                                             "HDF5 files (*.hdf5)")
        paths = items[0]
        if paths:
            for path in paths:
                self.logger.debug("processing selection %s", path)
                f_name = os.path.basename(path)
                f_name = os.path.splitext(f_name)[0]
                f_path = os.path.dirname(path)
                if f_name not in self.search_files:
                    # Add file to file_sheet_model:
                    self.logger.debug("adding search file")
                    self.search_files[f_name] = f_path
                    f_list = [QStandardItem(f_name), QStandardItem(f_path)]
                    self.file_sheet_model.appendRow(f_list)

                    if hdf.isopen:
                        self.logger.debug("closing open HDF5 file")
                        hdf.close()

                    # Add file's data to search_sheet_model:
                    self.logger.debug("opening search file")
                    hdf.open_file(path)
                    self.logger.debug("reading PID attributes")
                    for pid in hdf.list_pids():
                        # Create a unique dictionary key for each file's PIDs:
                        my_key = "%s.%s" % (f_name, pid)
                        # Initialize the sheet's row data:
                        my_data = [f_name, pid]
                        for i in self.pid_attrs.keys():
                            my_attr = self.pid_attrs[i]["key"]
                            my_attr_val = hdf.get_attr(
                                my_attr, os.path.join('/', str(pid)))
                            # Append PID attribute to row:
                            my_data.append(my_attr_val)
                        # Save search file's PID attributes:
                        self.search_data[my_key] = my_data
                    self.logger.debug("closing search file")
                    hdf.close()
            self.logger.debug("clearing FILE_SEARCH search bar")
            self.view.f_search.clear()
            self.update_file_search_sheet()
            self.update_file_browse_sheet()

    def analysis(self):
        """
        Name:     Prida.analysis
        Inputs:   None.
        Outputs:  None.
        Features: Changes view to ANALYSIS
        Depends:  - update_analysis_sheet_model
        """
        self.logger.info("Button Clicked")
        self.update_analysis_sheet_model()
        self.view.a_preview.clearBaseImage()
        self.view.a_label.clear()
        self.thumb_sheet_model.clear()
        self.img_list = []
        self.view.stackedWidget.setCurrentIndex(ANALYSIS)

    def analyze_gray(self):
        """
        Name:     Prida.analyze_gray
        Inputs:   None.
        Outputs:  None.
        Features: Processes IDA base image to grayscale and sets display label
        """
        self.logger.info("Button Clicked")
        my_label = self.view.a_preview.grayscale()
        self.view.a_label.setText(my_label)

    def analyze_binary(self):
        """
        Name:     Prida.analyze_binary
        Inputs:   None.
        Outputs:  None.
        Features: Processes IDA base image to binary and sets display label
        """
        self.logger.info("Button Clicked")
        my_label = self.view.a_preview.binary()
        self.view.a_label.setText(my_label)

    def analyze_undo(self):
        """
        Name:     Prida.analyze_undo
        Inputs:   None.
        Outputs:  None.
        Features: Resets the preview image to original
        """
        self.view.a_preview.resetBaseImage()
        self.view.a_label.setText("original image")

    def autofill_pid(self, pid=''):
        """
        Name:     Prida.autofill_pid
        Inputs:   [optional] str, plant ID (pid)
        Outputs:  None.
        Features: Autofills the PID metadata if PID exists
        """
        if pid in hdf.list_pids():
            self.logger.debug("performing auto-completion")
            p_path = "/%s" % (pid)
            for j in self.pid_attrs:
                f_name = self.pid_attrs[j]["qt_val"]
                f_type = self.pid_attrs[j]["qt_type"]
                f_value = hdf.get_attr(self.pid_attrs[j]["key"], p_path)
                self.set_input_field(f_name, f_type, f_value)

    def back_to_menu(self):
        """
        Name:     Prida.back_to_menu
        Inputs:   None
        Outputs:  None
        Features: Returns to the menu screen, closing the HDF5 file handle and
                  clearing session and sheet data.
        Depends:  - clear_session_sheet_model
                  - init_pid_attrs
                  - init_session_attrs
                  - update_file_browse_sheet
                  - update_file_search_sheet
                  - update_sheet_model
        """
        self.logger.info("Button Clicked")
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.init_pid_attrs()
        self.init_session_attrs()
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.editing = False
        self.update_sheet_model()
        self.update_file_browse_sheet()
        self.update_file_search_sheet()
        self.logger.info("view changed to MAIN_MENU")
        self.view.stackedWidget.setCurrentIndex(MAIN_MENU)
        self.base.setWindowTitle(self.prida_title)
        hdf.close()

    def back_to_sheet(self):
        """
        Name:     Prida.back_to_sheet
        Inputs:   None
        Outputs:  None
        Features: Returns the current view to VIEWER cleaning up all the sheets
        Depends:  - clear_session_sheet_model
                  - update_sheet_model
                  - update_thumb_sheet_model
        """
        self.logger.debug("returning to VIEWER")
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.update_sheet_model()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.logger.info("view changed to SHEET_VIEW")
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.logger.info("view changed to VIEWER")
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def cancel_input(self):
        """
        Name:     Prida.cancel_input
        Inputs:   None
        Outputs:  None
        Features: Exits INPUT_EXP, resetting editing boolean, enabling all
                  QLineEdit fields, and returns to VIEWER
        Depends:  enable_all_attrs
        """
        self.logger.info("Button Clicked")
        self.editing = False
        self.enable_all_attrs()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.logger.info("view changed to SHEET_VIEW")
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.logger.info("view changed to VIEWER")
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def clear_session_sheet_model(self):
        """
        Name:     Prida.clear_session_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Clears session sheet model
        Depends:  resize_session_sheet
        """
        self.logger.debug("clearing session sheet")
        for i in range(self.session_sheet_items):
            self.session_sheet_model.setItem(i, 1, QStandardItem(''))
        #
        self.resize_session_sheet()

    def compress_hdf(self):
        """
        Name:     Prida.compress_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Creates a compressed copy of the existing HDF file
        """
        if hdf.isopen:
            err_code = hdf.compress()
            if err_code == 0:
                self.error.showMessage(
                    "Successfully created compressed HDF file.")
                self.error.exec_()
            elif err_code == 1:
                self.error.showMessage(
                    ("Failed to create compressed HDF file. Check that you "
                     "have write privileges and try again."))
                self.error.exec_()
            elif err_code == -1:
                self.error.showMessage(
                    ("An error was encountered during writing to the "
                     "compressed HDF file. Try re-opening this file and "
                     "running compression again."))
                self.error.exec_()
            elif err_code == 9999:
                self.error.showMessage(
                    ("Failed to create compressed HDF file. The file already "
                     "exists. Please rename the existing file and try again."))
                self.error.exec_()
            else:
                self.error.showMessage(
                    ("An unknown error was encountered during compression. "
                     "Try re-opening this file and running the compression "
                     "again."))
                self.error.exec_()
        else:
            self.error.showMessage(
                ("Failed to create compressed HDF file. No open HDF file was "
                 "found. Please open an existing HDF file and try again."))
            self.error.exec_()

    def disable_pid_attrs(self):
        """
        Name:     Prida.disable_pid_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with PID attributes
        """
        if self.editing:
            self.logger.debug("disabling INPUT_EXP fields")
            self.view.c_pid.setEnabled(False)
            self.view.c_num_images.setEnabled(False)
            for k in self.pid_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.pid_attrs[k]["qt_val"]))

    def disable_session_attrs(self):
        """
        Name:     Prida.disable_session_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with session attributes
        """
        if self.editing:
            self.logger.debug("disabling INPUT_EXP fields")
            self.view.c_pid.setEnabled(False)
            for k in self.session_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.session_attrs[k]["qt_val"]))

    def edit_field(self):
        """
        Name:     Prida.edit_field
        Inputs:   None
        Outputs:  None
        Features: Performs INPUT_EXP field disabling based on the selection in
                  the sheet model, applies appropriate meta data to INPUT_EXP
                  fields, sets editing boolean to True and changes current
                  view to INPUT_EXP
        Depends:  - disable_pid_attrs
                  - disable_session_attrs
                  - set_defaults
                  - set_input_field
        """
        self.logger.info("Button Clicked")
        if self.sheet_select_model.hasSelection():
            self.logger.debug("set editing to True")
            self.editing = True
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session, disable PID attrs:
                self.disable_pid_attrs()

                # Get session attrs & set them to INPUT_EXP:
                pid = my_selection.parent().text()
                session = my_selection.text()
                p_path = "/%s" % (pid)
                s_path = "/%s/%s" % (pid, session)

                self.logger.debug("setting INPUT_EXP fields for PID")
                self.set_input_field("c_pid", "QLineEdit", str(pid))
                for j in self.pid_attrs:
                    f_name = self.pid_attrs[j]["qt_val"]
                    f_type = self.pid_attrs[j]["qt_type"]
                    f_value = hdf.get_attr(self.pid_attrs[j]["key"], p_path)
                    self.set_input_field(f_name, f_type, f_value)

                self.logger.debug("setting INPUT_EXP fields for session")
                for k in self.session_attrs:
                    f_name = self.session_attrs[k]["qt_val"]
                    f_type = self.session_attrs[k]["qt_type"]
                    f_value = hdf.get_attr(
                        self.session_attrs[k]["key"], s_path)
                    self.set_input_field(f_name, f_type, f_value)
            else:
                # Selected PID, disable session attrs:
                self.disable_session_attrs()

                # Get PID attrs & set them to INPUT_EXP:
                pid = my_selection.text()
                s_path = "/%s" % (pid)

                self.set_defaults()
                self.logger.debug("setting INPUT_EXP fields for PID")
                self.set_input_field("c_pid", "QLineEdit", str(pid))
                for k in self.pid_attrs:
                    f_name = self.pid_attrs[k]["qt_val"]
                    f_type = self.pid_attrs[k]["qt_type"]
                    f_value = hdf.get_attr(self.pid_attrs[k]["key"], s_path)
                    self.set_input_field(f_name, f_type, f_value)
            self.logger.info("view changed to INPUT_EXP")
            self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
        else:
            self.error.showMessage("Nothing selected!")
            self.error.exec_()

    def enable_all_attrs(self):
        """
        Name:     Prida.enable_all_attrs
        Inputs:   None
        Outputs:  None
        Features: Enables all INPUT_EXP fields
        """
        self.logger.debug("enabling all INPUT_EXP fields")
        self.view.c_pid.setEnabled(True)
        for k in self.pid_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.pid_attrs[k]["qt_val"]))

        for j in self.session_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.session_attrs[j]['qt_val']))

    def explore_file(self):
        """
        Name:     Prida.explore_file
        Inputs:   None.
        Outputs:  None.
        Features: Opens the sheet VIEW with the currently selected file from
                  the search file browser
        Depends:  - get_data
                  - update_sheet_model
        """
        self.logger.info("Button Clicked")
        if self.file_select_model.hasSelection():
            self.logger.debug("gathering selection details")
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0]
            )
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            if hdf.isopen:
                self.logger.debug("closing open HDF5 file")
                hdf.close()

            self.logger.debug("opening selected file")
            hdf.open_file(f_path)
            self.get_data()
            self.update_sheet_model()
            if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                self.logger.info("view changed to SHEET_VIEW")
                self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
            self.logger.info("view changed to VIEWER")
            self.view.stackedWidget.setCurrentIndex(VIEWER)

            # Set session user and contact defaults:
            self.logger.debug("saving session defaults")
            self.session_attrs[0]["def_val"] = str(hdf.get_root_user())
            self.session_attrs[1]["def_val"] = str(hdf.get_root_addr())
            self.logger.debug("updating window title")
            self.base.setWindowTitle(
                "%s: %s" % (self.prida_title, hdf.basename()))

    def export(self):
        """
        Name:     Prida.export
        Inputs:   None.
        Outputs:  None.
        Features: Exports the images from a given selection to a user-defined
                  output directory recreating the HDF5 folder structure, else
                  exports all images and meta data
        Depends:  get_sheet_model_selection
        """
        self.logger.info("Button Clicked")
        self.logger.debug("gathering user selection")
        if self.sheet_select_model.hasSelection():
            self.logger.debug("getting user selection")
            s_path = self.get_sheet_model_selection()
        else:
            self.logger.debug("no selection, exporting from root")
            s_path = "/"

        self.logger.debug("requesting output diretory from user")
        path = QFileDialog.getExistingDirectory(None,
                                                "Select an output directory:",
                                                os.path.expanduser("~"),
                                                QFileDialog.ShowDirsOnly)
        if os.path.isdir(path):
            try:
                hdf.extract_attrs(path)
            except IOError:
                self.logger.warning("failed to export attributes to %s", path)
            except ValueError:
                self.logger.warning(
                    "failed to export attributes; file already exists")
            except:
                self.logger.exception("caught unknown error exception")
            else:
                self.logger.debug("attributes extracted to %s", path)

            try:
                hdf.extract_datasets(s_path, path)
            except IOError:
                self.logger.warning("failed to export datasets to %s", path)
                self.error.showMessage(
                    ("Failed to extract datasets to %s. Please check output "
                     "directory privileges and try again.") % (path)
                )
                self.error.exec_()
            except:
                self.logger.exception("caught unknown error exception")
                self.error.showMessage(
                    ("An unexpected error occurred during dataset extraction. "
                     "Please check that output directory %s exists and "
                     "that you have write privileges.") % (path)
                )
                self.error.exec_()
            else:
                self.logger.debug("datasets extracted to %s", path)
                self.error.showMessage(
                    "Successfully extracted datasets to %s" % path)
                self.error.exec_()

    def get_data(self):
        """
        Name:     Prida.get_data
        Inputs:   None.
        Outputs:  None.
        Features: Creates a list of attribute dictionaries for each PID in
                  the HDF5 file.
        """
        self.logger.debug("resetting data list")
        self.data_list = []

        self.logger.debug("reading PID attributes")
        for pid in hdf.list_pids():
            d = {}
            d["pid"] = pid
            for i in self.pid_attrs.keys():
                my_attr = self.pid_attrs[i]["key"]
                d[my_attr] = hdf.get_attr(my_attr, os.path.join('/', str(pid)))
            self.data_list.append(d)

    def get_input_field(self, field_name, field_type):
        """
        Name:     Prida.get_input_field
        Inputs:   - str, Qt field name (field_name)
                  - str, Qt field type (field_type)
        Outputs:  str, current input text (field_value)
        Features: Get the current text from a Qt input box
        Raises:   NameError
        """
        err_msg = "could not get %s %s value" % (field_type, field_name)

        if field_type == "QComboBox":
            try:
                field_value = eval("self.view.%s.currentText()" % (field_name))
            except:
                self.logger.exception(err_msg)
                raise NameError(err_msg)
            else:
                self.logger.info("read %s from %s %s" % (
                    field_value, field_type, field_name))
        elif (field_type == "QDateEdit" or
              field_type == "QLineEdit" or
              field_type == "QSpinBox"):
            # All three fields use text() getter
            try:
                field_value = eval("self.view.%s.text()" % (field_name))
            except:
                self.logger.exception(err_msg)
                raise NameError(err_msg)
            else:
                self.logger.info("read %s from %s %s" % (
                    field_value, field_type, field_name))
        else:
            self.logger.error(err_msg)
            raise NameError(err_msg)

        return field_value

    def get_sheet_model_selection(self):
        """
        Name:     Prida.get_sheet_model_selection
        Inputs:   None
        Outputs:  str, HDF5 group path based on selection (s_path)
        Features: Returns sheet model selection path for HDF5 file
        """
        if self.sheet_select_model.hasSelection():
            self.logger.debug("gathering user selection")
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session
                self.logger.debug("returning session")
                pid = my_selection.parent().text()
                session = my_selection.text()
                s_path = "/%s/%s" % (pid, session)
            else:
                # Selected PID
                self.logger.debug("returning PID")
                pid = my_selection.text()
                s_path = "/%s" % (pid)

            return s_path

    def import_data(self):
        """
        Name:     Prida.import_data
        Inputs:   None.
        Outputs:  None.
        Features: Reads a user-defined directory for images, saves images to
                  search_files dictionary, attempts to extract image exif tags
                  and defines them in INPUT_EXP view
        Depends:  - read_exif_tags
                  - set_defaults
                  - set_input_field
        @TODO:    * save dataset attributes (i.e., image height, width, and
                    orientation)
        """
        self.logger.info("Button Clicked")
        self.logger.debug("requesting directory from user")
        path = QFileDialog.getExistingDirectory(
            None,
            "Select directory containing images:",
            os.path.expanduser("~"),
            QFileDialog.ShowDirsOnly
        )
        if os.path.isdir(path):
            # Populate default values and overwrite where possible:
            self.set_defaults()

            # Currently filters search for only jpg and tif image files
            self.logger.debug("searching directory for images")
            s_path = os.path.join(path, "*[(.jpg)(.tif)]")
            my_files = glob.glob(s_path)
            files_found = len(my_files)
            self.logger.info("importing %d files", files_found)

            if files_found > 0:
                # Reset file names for each processing:
                self.logger.debug("resetting search file dictionary")
                self.search_files = {}
                self.logger.debug("reading images found")
                for my_file in my_files:
                    try:
                        self.logger.debug("opening image %s", my_file)
                        img = Image.open(my_file)
                    except:
                        self.logger.exception("failed to open %s", my_file)
                        raise IOError("Error! Could not open image")
                    else:
                        self.search_files[my_file] = 0
                        exif = read_exif_tags(img)

                        # If DateTime is empty, try last modified time:
                        if exif["DateTime"] == "":
                            try:
                                self.logger.debug(
                                    "trying to read file modification date")
                                my_dt = get_ctime(my_file).strftime("%Y-%m-%d")
                            except:
                                self.logger.debug("no datetime exif tag")
                            else:
                                exif["DateTime"] = my_dt

                exif["NumPhotos"] = str(files_found)
                self.save_exif_tags(exif)
                self.logger.info("view changed to INPUT_EXP")
                self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
            else:
                self.logger.warning("no images found in %s", path)

    def save_exif_tags(self, exif):
        """
        Name:     Prida.save_exif_tags
        Inputs:   dictionary, exif tags (exif)
        Outputs:  None.
        Features: Save exif tag values to INPUT_EXP fields
        Depends:  set_input_field
        """
        # Pseudo-intelligent search for session_attr index:
        tag_attr = {
            "Make": [k for k, v in self.session_attrs.items()
                     if v["key"] == "cam_make"],
            "Model": [k for k, v in self.session_attrs.items()
                      if v["key"] == "cam_model"],
            "ShutterSpeedValue": [k for k, v in self.session_attrs.items()
                                  if v["key"] == "cam_shutter"],
            "ApertureValue": [k for k, v in self.session_attrs.items()
                              if v["key"] == "cam_aperture"],
            "ISOSpeedRatings": [k for k, v in self.session_attrs.items()
                                if v["key"] == "cam_exposure"],
            "DateTime": [k for k, v in self.session_attrs.items()
                         if v["key"] == "date"],
            "NumPhotos": [k for k, v in self.session_attrs.items()
                          if v["key"] == "num_img"]
        }

        for tag in exif:
            if tag in tag_attr:
                j = tag_attr[tag][0]
                f_name = self.session_attrs[j]["qt_val"]
                f_type = self.session_attrs[j]["qt_type"]
                f_val = exif[tag]
                self.logger.debug("setting %s %s to %s" % (
                    f_type, f_name, f_val))
                self.set_input_field(f_name, f_type, f_val)

    def import_session(self):
        """
        Name:     Prida.import_session
        Inputs:   None.
        Outputs:  None.
        Features: Creates a new session based on imported data; previews
                  images as they are imported
        Depends:  - back_to_sheet           - (editing) save_edits
                  - get_data                - (editing) enable_all_attrs
                  - get_input_field         - (editing) set_defaults
                  - update_sheet_model
        """
        self.logger.info("Button Clicked")
        if self.editing:
            self.logger.info("Saving Edits")
            self.save_edits()
            self.logger.debug("set editing to False")
            self.editing = False
            self.enable_all_attrs()
            self.set_defaults()
        else:
            # Create PID metadata dictionary:
            self.logger.debug("building PID dictionary")
            pid_dict = {}
            for i in self.pid_attrs.keys():
                f_key = self.pid_attrs[i]["key"]
                f_name = self.pid_attrs[i]["qt_val"]
                f_type = self.pid_attrs[i]["qt_type"]
                f_val = self.get_input_field(f_name, f_type)
                self.pid_attrs[i]["def_val"] = f_val
                pid_dict[f_key] = f_val

            # Create session metadata dictionary:
            self.logger.debug("building session dictionary")
            session_dict = {}
            for j in self.session_attrs.keys():
                f_key = self.session_attrs[j]["key"]
                f_name = self.session_attrs[j]["qt_val"]
                f_type = self.session_attrs[j]["qt_type"]
                f_val = self.get_input_field(f_name, f_type)
                self.session_attrs[j]["def_val"] = f_val
                session_dict[f_key] = f_val

            # Create new session:
            self.logger.info("Saving New Session")
            s_path = hdf.create_session(
                str(self.view.c_pid.text()), pid_dict, session_dict)

            # Save images to HDF5 and flush memory to file:
            files_found = sorted(list(self.search_files.keys()))
            self.view.c_progress.setMinimum(0)
            self.view.c_progress.setMaximum(len(files_found) - 1)
            self.view.stackedWidget.setCurrentIndex(RUN_EXP)
            for file_idx in range(len(files_found)):
                self.view.c_progress.setValue(file_idx)
                file_path = files_found[file_idx]
                img = QPixmap(file_path)
                self.view.c_preview.setPixmap(img.scaledToHeight(300))
                self.logger.debug("saving image %s", file_path)
                try:
                    hdf.save_image(s_path, file_path)
                except:
                    self.logger.exception("failed to save dataset %s", s_path)
                else:
                    basename = os.path.basename(file_path)
                    name = os.path.splitext(basename)[0]
                    a_path = os.path.join(s_path, name, basename)
                    a_date = get_ctime(file_path).strftime("%Y-%m-%d")
                    a_time = get_ctime(file_path).strftime("%H.%M.%S")

                    try:
                        hdf.set_attr("Height", str(img.height()), a_path)
                        hdf.set_attr("Width", str(img.width()), a_path)
                        hdf.set_attr("Date", a_date, a_path)
                        hdf.set_attr("Time", a_time, a_path)
                    except:
                        self.logger.exception(
                            "failed to save attributes to %s", s_path)
                    hdf.save()

        # Return to sheet:
        self.get_data()
        self.back_to_sheet()

    def init_pid_attrs(self):
        """
        Name:     Prida.init_pid_attrs
        Inputs:   None.
        Outputs:  None.
        Features: Initializes the PID attribute dictionary
        """
        self.logger.debug('initializing dictionary')
        self.pid_attrs = {
            1: {'title': 'Genus Species',
                'key': 'gen_sp',
                'qt_val': 'c_gen_sp',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            2: {'title': 'Line',
                'key': 'line',
                'qt_val': 'c_line',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            3: {'title': 'Rep Num',
                'key': 'rep_num',
                'qt_val': 'c_rep_num',
                'def_val': '',
                'qt_type': 'QSpinBox'},
            4: {'title': 'Tub ID',
                'key': 'tubid',
                'qt_val': 'c_tubid',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            5: {'title': 'Germination Date',
                'key': 'germdate',
                'qt_val': 'c_germdate',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            6: {'title': 'Transplant Date',
                'key': 'transdate',
                'qt_val': 'c_transdate',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            7: {'title': 'Treatment',
                'key': 'treatment',
                'qt_val': 'c_treatment',
                'def_val': 'Control',
                'qt_type': 'QLineEdit'},
            8: {'title': 'Media',
                'key': 'media',
                'qt_val': 'c_media',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            9: {'title': 'Tub Size',
                'key': 'tubsize',
                'qt_val': 'c_tubsize',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            10: {'title': 'Nutrient',
                 'key': 'nutrient',
                 'qt_val': 'c_nutrient',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            11: {'title': 'Growth Temp (Day)',
                 'key': 'growth_temp_day',
                 'qt_val': 'c_growth_temp_day',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            12: {'title': 'Growth Temp (Night)',
                 'key': 'growth_temp_night',
                 'qt_val': 'c_growth_temp_night',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            13: {'title': 'Lighting Conditions',
                 'key': 'growth_light',
                 'qt_val': 'c_growth_light',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            14: {'title': 'Watering schedule',
                 'key': 'water_sched',
                 'qt_val': 'c_water_sched',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            15: {'title': 'Damage',
                 'key': 'damage',
                 'qt_val': 'c_damage',
                 'def_val': 'No',
                 'qt_type': 'QLineEdit'},
            16: {'title': 'Plant Notes',
                 'key': 'notes',
                 'qt_val': 'c_plant_notes',
                 'def_val': 'N/A',
                 'qt_type': 'QLineEdit'}
        }

    def init_session_attrs(self):
        """
        Name:     Prida.init_session_attrs
        Inputs:   None.
        Outputs:  None.
        Features: Initializes session attribute dictionary
        """
        self.logger.debug('initializing dictionary')
        self.session_attrs = {
            0: {'title':  'Session User',
                'key': 'user',
                'qt_val': 'c_session_user',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            1: {'title': 'Session Email',
                'key': 'addr',
                'qt_val': 'c_session_addr',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            2: {'title': 'Session Title',
                'key': 'number',
                'qt_val': 'c_session_number',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            3: {'title': 'Session Date',
                'key': 'date',
                'qt_val': 'c_session_date',
                'def_val': '',
                'qt_type': 'QDateEdit'},
            4: {'title': 'Rig Name',
                'key': 'rig',
                'qt_val': 'c_session_rig',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            5: {'title': 'Image Count',
                'key': 'num_img',
                'qt_val': 'c_num_images',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            6: {'title': 'Plant Age',
                'key': 'age_num',
                'qt_val': 'c_plant_age',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            7: {'title': 'Notes',
                'key': 'notes',
                'qt_val': 'c_session_notes',
                'def_val': 'N/A',
                'qt_type': 'QLineEdit'},
            8: {'title': 'Camera Make',
                'key': 'cam_make',
                'qt_val': 'c_cam_make',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            9: {'title': 'Camera Model',
                'key': 'cam_model',
                'qt_val': 'c_cam_model',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            10: {'title': 'Exposure Time',
                 'key': 'cam_shutter',
                 'qt_val': 'c_cam_shutter',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            11: {'title': 'Aperture',
                 'key': 'cam_aperture',
                 'qt_val': 'c_cam_aperture',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            12: {'title': 'ISO',
                 'key': 'cam_exposure',
                 'qt_val': 'c_cam_exposure',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
        }

    def load_analysis_sheet_headers(self):
        """
        Name:     Prida.load_analysis_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the analysis sheet model
        Depends:  resize_analysis_sheet
        """
        self.logger.debug('loading headers')
        self.analysis_sheet_model.setColumnCount(2)
        self.analysis_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('PID'))
        self.analysis_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Session'))
        self.resize_analysis_sheet()

    def load_file_browse_sheet_headers(self):
        """
        Name:     Prida.load_file_browse_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the file_sheet_model
        Depends:  resize_file_browse_sheet
        """
        self.logger.debug('loading headers')
        self.file_sheet_model.setColumnCount(2)
        self.file_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File Name'))
        self.file_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Path'))
        self.resize_file_browse_sheet()

    def load_file_search_sheet_headers(self):
        """
        Name:     Prida.load_file_search_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the search_sheet_model
        Depends:  resize_search_sheet
        """
        self.logger.debug('loading headers')
        self.search_sheet_model.setColumnCount(self.sheet_items + 1)
        self.search_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File'))
        self.search_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('PID'))
        for i in self.pid_attrs:
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.search_sheet_model.setHorizontalHeaderItem", (i + 1),
                self.pid_attrs[i]['title']))
        self.resize_search_sheet()

    def load_session_sheet_headers(self):
        """
        Name:     Prida.load_session_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the session_sheet_model
        Depends:  resize_session_sheet
        """
        self.logger.debug('loading headers')
        self.session_sheet_model.setColumnCount(2)
        self.session_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('Property'))
        self.session_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Value'))
        #
        # Define session sheet rows in column 1:
        for i in range(self.session_sheet_items):
            exec('%s(%d, 0, QStandardItem("%s"))' % (
                "self.session_sheet_model.setItem", i,
                self.session_attrs[i]['title']))
        self.resize_session_sheet()

    def load_sheet_headers(self):
        """
        Name:     Prida.load_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the sheet_model
        Depends:  resize_sheet
        """
        self.logger.debug('load_sheet_headers:loading headers')
        self.sheet_model.setColumnCount(self.sheet_items)
        self.sheet_model.setHorizontalHeaderItem(0, QStandardItem('PID'))
        for i in self.pid_attrs.keys():
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.sheet_model.setHorizontalHeaderItem", i,
                self.pid_attrs[i]['title']))
        self.resize_sheet()

    def new_hdf(self):
        """
        Name:     Prida.new_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Gets user information, gets HDF5 file name, creates a new
                  HDF5 file, stores user information, and goes to VIEWER page
        Depends:  - get_data
                  - update_sheet_model
        """
        self.logger.info('Button Clicked')
        self.logger.debug('loading UI file')
        popup = uic.loadUi(self.user_ui)
        if popup.exec_():
            self.logger.debug('requesting new file name from user')
            path = QFileDialog.getSaveFileName(None,
                                               "Save File",
                                               os.path.expanduser("~"))[0]
            if path != '':
                if path[-5:] != '.hdf5':
                    path += '.hdf5'
                self.logger.debug('opening new file')
                hdf.open_file(path)
                self.logger.debug('saving about info to file')
                sess_user = str(popup.user.text())
                sess_addr = str(popup.email.text())
                sess_about = str(popup.about.toPlainText())
                hdf.set_root_user(sess_user)
                hdf.set_root_addr(sess_addr)
                hdf.set_root_about(sess_about)
                self.logger.debug('saving file')
                hdf.save()
                self.get_data()
                self.update_sheet_model()
                if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                    self.logger.info('view changed to SHEET_VIEW')
                    self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
                self.logger.info('view changed eto VIEWER')
                self.view.stackedWidget.setCurrentIndex(VIEWER)

                # Set session user and contact default values:
                self.logger.debug('saving session defaults')
                self.session_attrs[0]['def_val'] = str(hdf.get_root_user())
                self.session_attrs[1]['def_val'] = str(hdf.get_root_addr())
                self.logger.debug('updating window title')
                self.base.setWindowTitle(
                    "%s: %s" % (self.prida_title, hdf.basename())
                )
                self.set_defaults()

    def new_session(self):
        """
        Name:     Prida.new_session
        Inputs:   None.
        Outputs:  None
        Features: Creates HDF5 session, resets the progress bar, enters the
                  RUN_EXP screen, and begins a session
        """
        global session_path

        self.logger.info('Button Clicked')
        if self.editing:
            self.logger.info('Saving Edits')
            self.save_edits()
            self.logger.debug('set editing to False')
            self.editing = False
            self.enable_all_attrs()
            self.set_defaults()
            self.get_data()
            self.back_to_sheet()
        else:
            if hardware_mode:
                # Create the PID metadata dictionary:
                self.logger.debug('building PID dictionary')
                pid_dict = {}
                for i in self.pid_attrs.keys():
                    f_key = self.pid_attrs[i]['key']
                    f_name = self.pid_attrs[i]['qt_val']
                    f_type = self.pid_attrs[i]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    self.pid_attrs[i]['def_val'] = f_val
                    pid_dict[f_key] = f_val

                # Create the session metadata dictionary:
                self.logger.debug('buidling session dictionary')
                session_dict = {}
                for j in self.session_attrs.keys():
                    f_key = self.session_attrs[j]['key']
                    f_name = self.session_attrs[j]['qt_val']
                    f_type = self.session_attrs[j]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    self.session_attrs[j]['def_val'] = f_val
                    session_dict[f_key] = f_val

                # Create a new session:
                self.logger.info(':Saving New Session')
                session_path = hdf.create_session(str(self.view.c_pid.text()),
                                                  pid_dict,
                                                  session_dict)

                # Update imaging class:
                self.logger.debug('updating imaging class')
                imaging.cur_pid = str(self.view.c_pid.text())
                imaging.num_photos = int(self.view.c_num_images.text())

                # Prepare for RUN_EXP image capture and preview:
                self.logger.debug('preparing progress bar')
                self.view.c_preview.clear()
                self.view.c_progress.setMinimum(0)
                self.view.c_progress.setMaximum(int(imaging.find_end_pos()) +
                                                int(10 * imaging.num_photos))
                self.logger.info('view changed to RUN_EXP')
                self.view.stackedWidget.setCurrentIndex(RUN_EXP)
                self.logger.debug('start hardware thread')
                self.hardware_thread.start(QThread.TimeCriticalPriority)

    def open_hdf(self):
        """
        Name:     Prida.open_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Opens an existing HDF5 file and goes to VIEWER page
        Depends:  - get_data
                  - back_to_sheet
                  - set_defaults
                  - update_pid_auto_model
        """
        self.logger.info('Button Clicked')
        self.logger.debug('request existing file from user')
        path = QFileDialog.getOpenFileName(None,
                                           "Select File",
                                           os.path.expanduser("~"),
                                           filter='*.hdf5')[0]
        if os.path.isfile(path):
            self.logger.debug('opening selected file')
            hdf.open_file(path)
            self.get_data()
            self.update_pid_auto_model()
            self.back_to_sheet()

            # Set session user and contact defaults:
            self.logger.debug('saving session defaults')
            self.session_attrs[0]['def_val'] = str(hdf.get_root_user())
            self.session_attrs[1]['def_val'] = str(hdf.get_root_addr())
            self.logger.debug('updating window title')
            self.base.setWindowTitle(
                "%s: %s" % (self.prida_title, hdf.basename())
            )
            self.set_defaults()

    def resize_analysis_sheet(self):
        """
        Name:     Prida.resize_analysis_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the analysis session sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.a_session.resizeColumnToContents(col)

    def resize_file_browse_sheet(self):
        """
        Name:     Prida.resize_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_browse_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.file_browse_sheet.resizeColumnToContents(col)

    def resize_search_sheet(self):
        """
        Name:     Prida.resize_search_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_search_sheet
        """
        self.logger.debug('resizing')
        for col in range(self.sheet_items + 2):
            self.view.file_search_sheet.resizeColumnToContents(col)

    def resize_session_sheet(self):
        """
        Name:     Prida.resize_session_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the session_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.session_sheet.resizeColumnToContents(col)

    def resize_sheet(self):
        """
        Name:     Prida.resize_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the sheet
        """
        self.logger.debug("resizing")
        for col in range(self.sheet_items + 1):
            self.view.sheet.resizeColumnToContents(col)

    def rm_search_file(self):
        """
        Name:     Prida.rm_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Removes a file from the file browse view and its
                  associated PID rows from the file search view
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.info("Button Clicked")
        if self.file_select_model.hasSelection():
            # Get the selection and put together the file name and path:
            self.logger.debug("gathering user selection")
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0])
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            # Remove file from search_files:
            self.logger.debug("deleting file %s", f_name)
            del self.search_files[s_name]

            if hdf.isopen:
                self.logger.debug("closing open HDF5 file")
                hdf.close()

            # Remove file's PIDs from file_search_sheet:
            self.logger.debug("opening selected file")
            hdf.open_file(f_path)

            self.logger.debug("deleting PIDs from search data")
            for pid in hdf.list_pids():
                my_key = "%s.%s" % (s_name, pid)
                if my_key in self.search_data:
                    del self.search_data[my_key]
            self.logger.debug("closing selected file")
            hdf.close()
            self.view.f_search.clear()
            self.update_file_browse_sheet()
            self.update_file_search_sheet()
        else:
            self.logger.warning("nothing selected!")
            self.error.showMessage(
                "No selection made. Please select a file to remove.")
            self.error.exec_()

    def save_edits(self):
        """
        Name:     Prida.save_edits
        Inputs:   None
        Outputs:  None
        Features: Saves new attributes to HDF5 file
        Depends:  get_sheet_model_selection
        """
        if self.editing:
            if self.sheet_select_model.hasSelection():
                self.logger.debug("gathering user selection")
                s_path = self.get_sheet_model_selection()

                self.logger.debug(
                    "saving attributes from enabled INPUT_EXP fields")
                for i in self.pid_attrs.keys():
                    f_key = self.pid_attrs[i]["key"]
                    f_name = self.pid_attrs[i]["qt_val"]
                    f_type = self.pid_attrs[i]["qt_type"]
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        self.pid_attrs[i]["def_val"] = f_val
                        hdf.set_attr(f_key, f_val, s_path)
                for j in self.session_attrs.keys():
                    f_key = self.session_attrs[j]["key"]
                    f_name = self.session_attrs[j]["qt_val"]
                    f_type = self.session_attrs[j]["qt_type"]
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        self.session_attrs[j]["def_val"] = f_val
                        hdf.set_attr(f_key, f_val, s_path)
                self.logger.debug("saving file")
                hdf.save()

    def search(self, pid_meta, terms):
        """
        Name:     Prida.search
        Inputs:   - list, PID meta data values (pid_meta)
                  - list, search terms (terms)
        Outputs:  bool (found)
        Features: Searches the pid metafields and returns true if all terms are
                  found anywhere within the fields
        """
        found = True
        for term in terms:
            found = found and any(term in m_field for m_field in pid_meta)
        return found

    def set_defaults(self):
        """
        Name:     Prida.set_defaults
        Inputs:   None.
        Outputs:  None.
        Features: Sets GUI text default values
        """
        self.logger.debug("assigning PID INPUT_EXP fields")
        for i in self.pid_attrs.keys():
            f_name = self.pid_attrs[i]["qt_val"]
            f_type = self.pid_attrs[i]["qt_type"]
            f_val = self.pid_attrs[i]["def_val"]
            try:
                self.set_input_field(f_name, f_type, f_val)
            except:
                self.set_input_field(f_name, f_type, '')

        self.logger.debug("assigning session INPUT_EXP fields")
        for j in self.session_attrs.keys():
            f_name = self.session_attrs[j]["qt_val"]
            f_type = self.session_attrs[j]["qt_type"]
            f_val = self.session_attrs[j]["def_val"]
            try:
                self.set_input_field(f_name, f_type, f_val)
            except:
                self.set_input_field(f_name, f_type, '')

    def set_input_field(self, field_name, field_type, field_value):
        """
        Name:     Prida.set_input_field
        Inputs:   - str, Qt input widget name (field_name)
                  - str, Qt input widget type (field_type)
                  - str, input value (field_value)
        Outputs:  None
        Features: Sets Qt input field

        @TODO: change empty string to None type
        """
        err_msg = "set_input_field:could not set Qt %s, %s, to value %s" % (
            field_type, field_name, field_value)

        if field_type == "QComboBox":
            # Takes an integer, find it!
            if field_value == '':
                index = 0
            else:
                index = eval('self.view.%s.findText("%s", %s)' % (
                    field_name,
                    field_value,
                    "Qt.MatchFixedString")
                )
            try:
                self.logger.debug("setting QComboBox index to %d", index)
                exec("self.view.%s.setCurrentIndex(%d)" % (
                    field_name,
                    index)
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        elif field_type == "QDateEdit":
            # Takes a QDate object
            if field_value == '':
                try:
                    self.logger.debug("setting QDateEdit to current date")
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        "QDate.fromString",
                        QDate.currentDate().toString("yyyy-MM-dd"))
                    )
                except:
                    self.logger.exception(err_msg)
                    raise ValueError(err_msg)
            else:
                try:
                    self.logger.debug("setting QDate edit to %s", field_value)
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        "QDate.fromString",
                        field_value)
                    )
                except:
                    self.logger.exception(err_msg)
                    raise ValueError(err_msg)
        elif field_type == "QLineEdit":
            # Takes a string
            try:
                self.logger.debug("setting QLineEdit to %s", field_value)
                exec('self.view.%s.setText("%s")' % (
                    field_name,
                    field_value)
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        elif field_type == "QSpinBox":
            # Takes an integer
            if field_value == '':
                field_value = 0
            try:
                self.logger.debug("setting QSpinBox to %d", int(field_value))
                exec("self.view.%s.setValue(%d)" % (
                    field_name,
                    int(field_value))
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        else:
            self.logger.error(err_msg)
            raise ValueError(err_msg)

    def show_ida(self, selection):
        """
        Name:     Prida.show_ida
        Input:    obj (selection)
        Output:   None.
        Features: Switches to IDA page and sets IDA base image
        """
        self.view.ida.clearBaseImage()
        if selection.indexes():
            self.logger.info("view changed to IDA_VIEW")
            self.view.stacked_sheets.setCurrentIndex(IDA_VIEW)
            self.logger.debug("gathering user selection")
            img_path = self.img_sheet_model.itemFromIndex(
                selection.indexes()[0]).data()
            self.logger.debug("get image data for selection")
            ndarray_full = hdf.get_dataset(img_path)
            self.logger.debug("set IDA base image to selection")
            self.view.ida.setBaseImage(ndarray_full)

    def show_preview(self, selection):
        """
        Name:     Prida.show_preview
        Input:    obj (selection)
        Output:   None.
        Features: Updates ANALYSIS IDA image preview
        """
        self.view.a_preview.clearBaseImage()
        self.view.a_label.clear()
        if selection.indexes():
            self.logger.debug("gathering user selection")
            img_path = self.thumb_sheet_model.itemFromIndex(
                selection.indexes()[0]).data()
            self.logger.debug("gathering image data from selection")
            ndarray_full = hdf.get_dataset(img_path)
            self.logger.debug("set IDA base image to selection")
            self.view.a_preview.setBaseImage(ndarray_full)
            self.view.a_label.setText("original image")

    def to_sheet(self):
        """
        Name:     Prida.to_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Switches from IDA to sheet view
        """
        self.logger.info("Button Clicked")
        self.view.img_select.clearSelection()
        self.logger.info("view changed to SHEET_VIEW")
        self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)

    def tool_not_enabled(self):
        """
        @TODO
        """
        self.logger.warning("tool not enabled!")
        self.error.showMessage(
            "This tool has not be enabled yet.")
        self.error.exec_()

    def update_analysis_sheet_model(self, search=''):
        """
        Name:     Prida.update_analysis_sheet_model
        Inputs:   [optional] str (search)
        Features: Displays all PIDs and sessions or just those that match the
                  search terms if search terms are given; saves the session
                  path to the PID QStandardItem
        Depends:  - load_analysis_sheet_headers
                  - update_ps_list
        """
        self.logger.debug("updating sheet")
        self.analysis_sheet_model.clear()
        self.load_analysis_sheet_headers()
        self.update_ps_list()
        terms = search.lower().split()

        for s_path in self.ps_list:
            ps = self.ps_list[s_path]
            self.logger.debug("unpacking %s", s_path)
            pid, session = ps
            self.logger.debug("found PID %s and session %s" % (pid, session))
            if self.search([i.lower() for i in ps], terms):
                pid_item = QStandardItem(pid)
                pid_item.setData(s_path)
                session_item = QStandardItem(session)
                pid_list = [pid_item, session_item]
                self.analysis_sheet_model.appendRow(pid_list)
        self.resize_analysis_sheet()

    def update_dict_auto_model(self):
        """
        Name:     Prida.update_dict_auto_model
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QStringListModel with dictionary file's contents
        """
        try:
            with open(self.gs_dict) as f:
                my_dict = f.read().splitlines()
        except:
            self.logger.debug("dictionary list set to empty")
            my_dict = []
        else:
            self.logger.debug("read file contents to dictionary list")
        finally:
            self.dict_auto_model.setStringList(my_dict)

    def update_file_browse_sheet(self):
        """
        Name:     Prida.update_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Reloads the rows in the file_sheet_model
        Depends:  - load_file_browse_sheet_headers
                  - resize_file_browse_sheet
        """
        self.logger.debug("updating sheet")
        self.file_sheet_model.clear()
        self.load_file_browse_sheet_headers()
        for f_name in self.search_files:
            f_path = self.search_files[f_name]
            f_list = [QStandardItem(f_name), QStandardItem(f_path)]
            self.file_sheet_model.appendRow(f_list)
        self.resize_file_browse_sheet()

    def update_file_search_sheet(self, search=''):
        """
        Name:     Prida.update_file_search_sheet
        Inputs:   [optional] str, search string (search)
        Outputs:  None.
        Features: Reloads the rows in the search_sheet_model, filtered for
                  search terms
        Depends:  - load_file_search_sheet_headers
                  - resize_search_sheet
                  - search
        """
        self.logger.debug("updating sheet")
        self.search_sheet_model.clear()
        self.load_file_search_sheet_headers()
        terms = search.lower().split()
        for i in self.search_data:
            pid_meta = []
            for j in self.search_data[i]:
                pid_meta.append(j.lower())
            if self.search(pid_meta, terms):
                d_list = []
                for k in self.search_data[i]:
                    d_list.append(QStandardItem(k))
                self.search_sheet_model.appendRow(d_list)
        self.resize_search_sheet()

    def update_pid_auto_model(self):
        """
        Name:     Prida.update_pid_auto_model
        Inputs:   None
        Outputs:  None
        Features: Updates the QStringListModel with HDF5 file's current PIDs
        """
        if hdf.isopen:
            self.logger.debug("assigning PIDs to list")
            self.pid_auto_model.setStringList(hdf.list_pids())
        else:
            self.logger.debug("reset to empty list")
            self.pid_auto_model.setStringList([])

    def update_progress(self):
        """
        Name:     Prida.update_progress
        Inputs:   None.
        Outputs:  None.
        Features: Updates the progress bar, sets first captured image to
                  preview, and decides the completion status of the session
        Depends:  - clear_session_sheet_model
                  - get_data
                  - update_sheet_model
        """
        self.logger.debug("update progress bar")
        self.view.c_progress.setValue(imaging.ms_count + (10 * imaging.taken))
        if is_path:
            if not self.preview:
                self.logger.debug("set preview image")
                image = QPixmap(filepath)
                self.view.c_preview.setPixmap(image.scaledToHeight(300))
                self.preview = True
            else:
                self.logger.debug("preview image already set")
        if not imaging.is_running:
            self.logger.debug("imaging done")
            imaging.reset_count()
            self.view.c_preview.clear()
            self.preview = False
            self.get_data()
            self.back_to_sheet()
        else:
            self.logger.debug("start hardware thread")
            self.hardware_thread.start(QThread.TimeCriticalPriority)

    def update_ps_list(self):
        """
        Name:     Prida.update_ps_list
        Inputs:   None.
        Outputs:  None.
        Features: Creates a dictionary of session path keys and pid/session
                  name list values
        """
        if hdf.isopen:
            self.logger.debug("resetting ps list")
            self.ps_list = {}
            for pid in hdf.list_pids():
                for session in hdf.list_sessions(pid):
                    s_path = "/%s/%s" % (pid, session)
                    s_name = hdf.get_attr("number", s_path)
                    self.logger.debug("saving session path '%s'", s_path)
                    if s_name and not s_name.isspace():
                        # Use user-given session name if available
                        self.ps_list[s_path] = [pid, s_name]
                        self.logger.debug("using session name '%s'", s_name)
                    else:
                        # Otherwise, just use session group name
                        self.ps_list[s_path] = [pid, session]

    def update_session_sheet_model(self, selection):
        """
        Name:     Prida.update_session_sheet_model
        Inputs:   obj (selection)
        Outputs:  None.
        Features: Updates session sheet model with selected session's data,
                  creates the thumbnails and tags the full image paths, and
                  clears session sheet model if no sessions are selected
        Depends:  - clear_session_sheet_model
                  - resize_session_sheet
        """
        try:
            self.logger.debug("gathering user selection")
            my_session = self.sheet_model.itemFromIndex(selection.indexes()[0])
        except IndexError:
            # You made a de-selection (ctrl+click):
            self.logger.debug("encountered de-selection")
            self.clear_session_sheet_model()
            self.img_sheet_model.clear()
            self.img_list = []
            self.resize_session_sheet()
        else:
            if (my_session.parent() is not None):
                # Session selection
                self.logger.debug("session selected")
                pid = my_session.parent().text()
                session = my_session.text()
                s_path = "/%s/%s" % (pid, session)
                self.logger.debug("loading session fields")
                for i in range(self.session_sheet_items):
                    exec('%s(%d, 1, QStandardItem("%s"))' % (
                        "self.session_sheet_model.setItem", i,
                        hdf.get_attr(self.session_attrs[i]['key'], s_path))
                    )
                self.img_sheet_model.clear()
                self.img_list = []
                img_counter = 0
                if hdf.isopen:
                    self.logger.debug("reading session datasets")
                    for obj in hdf.list_objects(s_path):
                        img_counter += 1
                        thumb_path = "%s/%s/%s.thumb" % (s_path, obj, obj)
                        image_path = "%s/%s/%s.jpg" % (s_path, obj, obj)
                        self.logger.debug("getting data for %s", thumb_path)
                        ndarray = hdf.get_dataset(thumb_path)
                        if ndarray.size != 0:
                            image = Image.fromarray(ndarray)
                            qt_image = ImageQt.ImageQt(image)
                            self.img_list.append(qt_image)
                            pix = QPixmap.fromImage(qt_image)
                            self.logger.debug(
                                "saving thumb to image sheet model")
                            item = QStandardItem(str(img_counter))
                            item.setIcon(QIcon(pix))
                            item.setData(image_path)
                            self.img_sheet_model.appendRow(item)
            else:
                # PID selection
                self.logger.debug("PID selected")
                self.logger.debug("clearing session and image sheets")
                self.clear_session_sheet_model()
                self.img_sheet_model.clear()
                self.img_list = []
            self.resize_session_sheet()

    def update_sheet_model(self, search=''):
        """
        Name:     Prida.update_sheet_model
        Inputs:   [optional] str (search)
        Features: Displays all PIDs in the sheet or the PIDs that match the
                  search terms if search terms are given
        Depends:  - load_sheet_headers
                  - resize_sheet
                  - search
        """
        self.logger.debug("updating sheet")
        self.update_pid_auto_model()
        self.sheet_model.clear()
        self.load_sheet_headers()
        terms = search.lower().split()
        for data in self.data_list:
            pid_meta = []
            for key in data:
                pid_meta.append(data[key].lower())
            if self.search(pid_meta, terms):
                pid_item = QStandardItem(data["pid"])

                # Create session rows under each PID:
                for session in hdf.list_sessions(data["pid"]):
                    pid_item.appendRow(QStandardItem(session))

                # Fill the PID row with its attributes:
                pid_list = [pid_item, ]
                for i in sorted(self.pid_attrs.keys()):
                    my_attr = data[self.pid_attrs[i]["key"]]
                    pid_list.append(QStandardItem(my_attr))
                self.sheet_model.appendRow(pid_list)
        self.resize_sheet()

    def update_thumb_sheet_model(self, selection):
        """
        Name:     Prida.update_thumb_sheet_model
        Inputs:   obj (selection)
        Outputs:  None.
        Features: Updates thumb sheet model with icons from the selected
                  session's datasets, saves the full image paths as data, and
                  clears thumb sheet model if no selection is made
        """
        # Clear the image preview:
        self.view.a_preview.clearBaseImage()
        self.view.a_label.clear()

        if selection.indexes():
            # Retrieve session path (created in update analysis sheet model)
            self.logger.debug("gathering user selection")
            s_path = self.analysis_sheet_model.itemFromIndex(
                selection.indexes()[0]).data()

            # Reset view and lists:
            self.thumb_sheet_model.clear()
            self.img_list = []
            img_counter = 0
            if hdf.isopen:
                self.logger.debug("reading '%s' datasets", s_path)
                for obj in hdf.list_objects(s_path):
                    thumb_path = "%s/%s/%s.thumb" % (s_path, obj, obj)
                    image_path = "%s/%s/%s.jpg" % (s_path, obj, obj)

                    self.logger.debug("getting data for %s", thumb_path)
                    ndarray = hdf.get_dataset(thumb_path)
                    image = Image.fromarray(ndarray)
                    qt_image = ImageQt.ImageQt(image)
                    self.img_list.append(qt_image)
                    pix = QPixmap.fromImage(qt_image)

                    self.logger.debug("adding image icon to thumb sheet model")
                    img_counter += 1
                    item = QStandardItem("%03d" % (img_counter))
                    item.setIcon(QIcon(pix))
                    item.setData(image_path)
                    self.thumb_sheet_model.appendRow(item)
        else:
            # You made a de-selection (ctrl+click):
            self.logger.debug("encountered de-selection")
            self.thumb_sheet_model.clear()
            self.img_list = []


class StreamToLogger(object):
    """
    Name:     StreamToLogger
    Features: Logs stdout and stderr to log file
    History:  Version 1.0.1-dev
              - Based off of the Stream to Logger class written by Ferry
                Boender for logging stdout and stderr to a log file using the
                sys and logging python modules.
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        # self.line_buf = ''

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def write(self, buf):
        """
        Name:    StreamToLogger.write
        Feature: Writes the buffer to a log file. Used to make logging emulate
                 a file-like object.
        Inputs:  Str, buffer to write to the log (buf)
        Outputs: None
        """
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(self):
        """
        Name:    StreamToLogger.flush
        Feature: Dummy function to have logging emulate a file like object.
                 Actual flushing of the buffer handled within logging itself.
        Inputs:  None
        Outputs: None
        """
        pass

###############################################################################
# MAIN:
###############################################################################
if __name__ == "__main__":
    # Create a root logger and temporary buffer logger
    root_logger = logging.getLogger()
    temp_logger = logging.getLogger('temp')
    temp_logger.setLevel(logging.DEBUG)

    # Set up a 66 kB memory handler for buffering initial logs
    mem_handler = logging.handlers.MemoryHandler(
        (66 * 1024),
        flushLevel=logging.CRITICAL,
        target=logging.NullHandler())
    temp_logger.addHandler(mem_handler)

    # Configure global variables; if config file doesn't exist, uses
    # GlobalConf default values
    temp_logger.debug('Instantiating global conf...')
    config = GlobalConf()
    temp_logger.debug('Configuring global variables...')
    prida_dir = config.prida_dir
    conf_dir = config.conf_dir
    cred_dir = config.cred_dir
    log_dir = config.log_dir
    dict_dir = config.dict_dir
    conf_filename = config.conf_filename
    cred_filename = config.cred_filename
    log_filename = config.log_filename
    dict_filename = config.dict_filename
    log_level = "logging.%s" % (config.log_level.upper())

    # Set root logger level
    root_logger.setLevel(eval(log_level))

    # Check for pre-existance of logging directory
    temp_logger.debug('Checking for pre-existance of logging directory...')
    if os.path.isdir(log_dir) and os.access(log_dir, os.W_OK):
        # Logging directory exists and is writeable
        temp_logger.debug("Logging directory found at '%s'" % (log_dir))
        log_path = os.path.join(log_dir, log_filename)
        my_mail = PridaMail(log_dir)

        # Instantiating logging file handler
        temp_logger.debug('Instantiating logging file handler...')
        root_handler = logging.handlers.RotatingFileHandler(
            log_path, backupCount=9)

    elif os.path.isdir(prida_dir) and os.access(prida_dir, os.W_OK):
        # Reassign logging directory
        log_dir = prida_dir
        temp_logger.debug(
            ("Writing to logging directory failed"
             ", writing to '%s'") % (log_dir))
        log_path = os.path.join(log_dir, log_filename)
        my_mail = PridaMail(log_dir)
        # Instantiating logging file handler
        temp_logger.debug('Instantiating logging file handler...')
        root_handler = logging.handlers.RotatingFileHandler(
            log_path, backupCount=9)

    else:
        # No writeable logging directory found, logging to stream
        temp_logger.info(
            'No writeable logging directory found, logging to stream.')
        root_handler = logging.StreamHandler()
        my_mail = PridaMail()

    # Estblishing record format
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt='%Y-%m-%d %H:%M:%S')
    root_handler.setFormatter(formatter)

    # Instantiating logging filter to check the records in the buffer loggers,
    # add to handler, and register rollover at exit
    temp_logger.debug('Instantiating logging filter...')
    root_filter = CustomFilter(eval(log_level))
    temp_logger.debug('Sending filter to root handler...')
    root_handler.addFilter(root_filter)
    if os.path.isdir(log_dir):
        temp_logger.debug('Registering file handler rollover...')
        atexit.register(exit_log, root_handler)

    # Send logging handler to root logger
    temp_logger.debug('Sending logging handler to root logger...')
    root_logger.addHandler(root_handler)

    # To preserve record order, collect log records from GlobalConf and
    # temp handler now if logging to file
    if os.path.isdir(log_dir):
        temp_logger.debug('Collecting buffered log records...')
        config.collect_log_records(root_handler)
        mem_handler.setTarget(root_handler)
        mem_handler.flush()

    # Create a standard output logger
    stdout_logger = logging.getLogger("STDOUT")
    sl = StreamToLogger(stdout_logger, logging.INFO)
    sys.stdout = sl

    # Create a standard error logger
    stderr_logger = logging.getLogger("STDERR")
    sl = StreamToLogger(stderr_logger, logging.ERROR)
    sys.stderr = sl

    # # Archive log files (if existing)
    # cred_file = os.path.join(cred_dir, cred_filename)
    # my_mail.validate(cred_file)
    # if os.path.isfile(log_path):
    #    fh.doRollover()
    # my_mail.archive_log(log_path)

    # Attempt to create a local Prida directory:
    pridadir_code = create_local_prida(prida_dir, conf_filename, dict_filename)
    if pridadir_code == 0:
        # Success, delete any local logs and set new logging directory
        my_mail.set_log_dir(log_dir)
        root_handler.flush()
        root_logger.removeHandler(root_handler)

        log_path = os.path.join(log_dir, log_filename)
        fh = logging.handlers.RotatingFileHandler(log_path, backupCount=9)
        fh.setFormatter(formatter)
        root_logger.addHandler(fh)
        root_logger.info("Created local Prida directory")

        root_logger.debug("Collecting previous buffered log records...")
        config.collect_log_records(fh)
        mem_handler.setTarget(fh)
        mem_handler.flush()
        root_logger.debug(
            "...finished recording previous buffered log records")
    elif pridadir_code == 9999:
        root_logger.info("Local Prida directory already exists")
    elif pridadir_code == 1:
        root_logger.warning("Failed to create local Prida directory")
        root_logger.debug('Collecting buffered log records...')
        config.collect_log_records(root_handler)
        mem_handler.setTarget(root_handler)
        mem_handler.flush()
        root_logger.debug(
            "...finished recording previous buffered log records")
    elif pridadir_code == -1:
        root_logger.warning("Failed to create config and/or dictionary file")
        temp_logger.debug('Collecting buffered log records...')
        config.collect_log_records(root_handler)
        mem_handler.setTarget(root_handler)
        mem_handler.flush()
        root_logger.debug(
            "...finished recording previous buffered log records")

    hdf = PridaHDF(conf_parser, os.path.join(conf_dir, conf_filename))
    root_logger.info("Instantiated PridaHDF()")

    root_logger.info("Hardware Mode == %s" % (hardware_mode))
    if hardware_mode:
        imaging = Imaging(conf_parser, os.path.join(conf_dir, conf_filename))
        root_logger.info("Instantiated Imaging()")

    root_logger.debug("initializing Prida application")
    app = Prida()
    app.exec_()
    if hardware_mode:
        app.hardware_thread.wait()
    hdf_closed = hdf.close()

    root_logger.info('Closed PridaHDF()')
    my_mail.archive_log(log_path)
    sys.exit()
