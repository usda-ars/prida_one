#!/usr/bin/python
#
# Adafruit_PWM_Servo_Driver.py
#
# VERSION: 1.2.1
#
# LAST EDIT: 2016-01-13
#
###############################################################################
# ADAFRUIT PYTHON LIBRARY FOR DC + STEPPER MOTOR HAT                          #
###############################################################################
# Python library for interfacing with the Adafruit Motor HAT for Raspberry Pi #
# to control DC motors with speed control and Stepper motors with single,     #
# double, interleave and microstepping.                                       #
#                                                                             #
# Designed specifically to work with the Adafruit Motor Hat                   #
#                                                                             #
# ----> https://www.adafruit.com/product/2348                                 #
#                                                                             #
# Adafruit invests time and resources providing this open source code, please #
# support Adafruit and open-source hardware by purchasing products from       #
# Adafruit!                                                                   #
#                                                                             #
# Written by Limor Fried for Adafruit Industries.                             #
# MIT license, all text above must be included in any redistribution          #
###############################################################################
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# e48e914: Nathanael Shaw - 2015-12-22 14:28:57
#     update changelogs
# 2c229fb: twdavis - 2015-12-10 17:56:58
#     Updated logging messages. Added new LED class attribute for current color.
# b737ac8: Nathanael Shaw - 2015-12-02 11:23:58
#     relative imports hotfix
# e627560: Nathanael Shaw - 2015-12-01 19:08:44
#     add hardware module
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import time
import logging
import numpy
from .Adafruit_I2C import Adafruit_I2C


###############################################################################
# CLASSES:
###############################################################################
class PWM:
    # Registers/etc.
    __MODE1 = 0x00
    __MODE2 = 0x01
    __SUBADR1 = 0x02
    __SUBADR2 = 0x03
    __SUBADR3 = 0x04
    __PRESCALE = 0xFE
    __LED0_ON_L = 0x06
    __LED0_ON_H = 0x07
    __LED0_OFF_L = 0x08
    __LED0_OFF_H = 0x09
    __ALL_LED_ON_L = 0xFA
    __ALL_LED_ON_H = 0xFB
    __ALL_LED_OFF_L = 0xFC
    __ALL_LED_OFF_H = 0xFD

    # Bits
    __RESTART = 0x80
    __SLEEP = 0x10
    __ALLCALL = 0x01
    __INVRT = 0x10
    __OUTDRV = 0x04

    general_call_i2c = Adafruit_I2C(0x00)

    @classmethod
    def softwareReset(cls):
        """
        Sends a software reset (SWRST) command to all the servo drivers on
        the bus
        """
        cls.general_call_i2c.writeRaw8(0x06)        # SWRST

    def __init__(self, address=0x40, debug=False):
        self.logger = logging.getLogger(__name__)
        self.logger.debug('start.')
        self.i2c = Adafruit_I2C(address)
        self.i2c.debug = debug
        self.address = address
        self.debug = debug
        self.logger.debug("Reseting PCA9685 MODE1 " +
                          "(without SLEEP) and MODE2")
        self.setAllPWM(0, 0)
        self.i2c.write8(self.__MODE2, self.__OUTDRV)
        self.i2c.write8(self.__MODE1, self.__ALLCALL)
        time.sleep(0.005)                             # wait for oscillator

        mode1 = self.i2c.readU8(self.__MODE1)
        mode1 = mode1 & ~self.__SLEEP                 # wake up (reset sleep)
        self.i2c.write8(self.__MODE1, mode1)
        time.sleep(0.005)                             # wait for oscillator
        self.logger.debug('complete.')

    def setPWMFreq(self, freq):
        "Sets the PWM frequency"
        prescaleval = 25000000.0    # 25MHz
        prescaleval /= 4096.0       # 12-bit
        prescaleval /= float(freq)
        prescaleval -= 1.0
        self.logger.debug(("setPWMFreq:Setting PWM frequency to %d Hz" % freq))
        self.logger.debug(("setPWMFreq:Estimated pre-scale: %d" % prescaleval))
        prescale = numpy.floor(prescaleval + 0.5)
        self.logger.debug(("setPWMFreq:Final pre-scale: %d" % prescale))

        oldmode = self.i2c.readU8(self.__MODE1)
        newmode = (oldmode & 0x7F) | 0x10             # sleep
        self.i2c.write8(self.__MODE1, newmode)        # go to sleep
        self.i2c.write8(self.__PRESCALE, int(numpy.floor(prescale)))
        self.i2c.write8(self.__MODE1, oldmode)
        time.sleep(0.005)
        self.i2c.write8(self.__MODE1, oldmode | 0x80)

    def setPWM(self, channel, on, off):
        "Sets a single PWM channel"
        # self.logger.debug('start.')
        self.i2c.write8(self.__LED0_ON_L + 4 * channel, on & 0xFF)
        self.i2c.write8(self.__LED0_ON_H + 4 * channel, on >> 8)
        self.i2c.write8(self.__LED0_OFF_L + 4 * channel, off & 0xFF)
        self.i2c.write8(self.__LED0_OFF_H + 4 * channel, off >> 8)
        # self.logger.debug('complete.')

    def setAllPWM(self, on, off):
        "Sets a all PWM channels"
        self.logger.debug('start.')
        self.i2c.write8(self.__ALL_LED_ON_L, on & 0xFF)
        self.i2c.write8(self.__ALL_LED_ON_H, on >> 8)
        self.i2c.write8(self.__ALL_LED_OFF_L, off & 0xFF)
        self.i2c.write8(self.__ALL_LED_OFF_H, off >> 8)
        self.logger.debug('complete.')
