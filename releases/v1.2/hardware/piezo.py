#!/usr/bin/python
#
# piezo.py
#
# VERSION: 1.2.1
#
# LAST EDIT: 2016-01-13
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 1045bfe: Nathanael Shaw - 2016-01-04 11:43:10
#     add conf param type and value checking
# 1b87533: Nathanael Shaw - 2015-12-23 15:17:26
#     hotfix
# e48e914: Nathanael Shaw - 2015-12-22 14:28:57
#     update changelogs
# 76a746e: Nathanael Shaw - 2015-12-22 14:23:26
#     update docstrings and cleanup
# 2ac67e4: Nathanael Shaw - 2015-12-22 12:23:59
#     pass func handle in lieu of rel import
# 8efbde4: Nathanael Shaw - 2015-12-22 12:02:53
#     move conf_parser to utilities.py
# 70e1bde: twdavis - 2015-12-10 18:30:04
#     more logging statement updates
# 1947123: Nathanael Shaw - 2015-12-04 14:41:03
#     update config parse with try except
# 6482ee8: Nathanael Shaw - 2015-12-03 17:27:04
#     add filepath passing bewteen objects
# 1bf8599: Nathanael Shaw - 2015-12-03 14:34:54
#     add hardware object self configuring from file
# 4279d18: Nathanael Shaw - 2015-12-03 13:52:34
#     getting closer
# 0a1b0da: Nathanael Shaw - 2015-12-03 13:31:27
#     more debug logs
# c88e1bc: Nathanael Shaw - 2015-12-03 13:12:37
#     hotfix
# ae4178c: Nathanael Shaw - 2015-12-03 13:03:02
#     hotfix
# e1bd0f9: Nathanael Shaw - 2015-12-03 12:50:06
#     add conf debug log records
# 61fd0a4: Nathanael Shaw - 2015-12-03 12:45:58
#     hotfix
# 35a805e: Nathanael Shaw - 2015-12-03 12:31:49
#     add self config behav. to objects
# ad3a146: Nathanael Shaw - 2015-12-02 09:37:28
#     attempt relative import fix
# e627560: Nathanael Shaw - 2015-12-01 19:08:44
#     add hardware module
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import time
import logging

from .Adafruit_MotorHAT import Adafruit_MotorHAT
from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class Piezo(Adafruit_MotorHAT, PRIDAPeripheral):

    def __init__(self, my_parser=None, config_filepath='.'):
        logging.debug('start initializing piezo.')
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self.logger = logging.getLogger(__name__)
        self._CHANNEL = 0
        self._VOLUME = 1.0
        self.attr_dict = {'PWM_CHANNEL': 'channel',
                          'VOLUME': 'volume',
                          }
        if my_parser is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('complete.')

    @property
    def channel(self):
        """
        Name:    Piezo.channel
        Feature: Returns PWM channel controlling the piezo buzzer.
        Inputs:  None
        Outputs: Integer, channel number assigned to controlling the
                 piezo buzzer (self._CHANNEL)
        """
        return self._CHANNEL

    @property
    def volume(self):
        """
        Name:    Imaging.volume
        Feature: Returns float value representing percent of maximum
                 volume
        Inputs:  None
        Outputs: Float, value representing currently assigned piezo
                 buzzer volume (self._VOLUME)
        """
        return 100 * self._VOLUME

    @channel.setter
    def channel(self, ch):
        """
        Name:    Piezo.channel
        Feature: Set the PWM channel controlling the piezo buzzer.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if isinstance(ch, int):
            if ch in {0, 1, 14, 15}:
                self._CHANNEL = ch

    @volume.setter
    def volume(self, volume):
        """
        Name:    Piezo.volume
        Feature: Set piezo volume by duty cycle.
        Inputs:  int, percent of maximum volume (volume)
        Outputs: None
        """
        if isinstance(volume, int) or isinstance(volume, float):
            if volume < 0:
                volume = 0
            elif volume > 100:
                volume = 100
            self._VOLUME = volume / 100.0

    def beep(self, num_beeps=3, beep_time=0.05, rest_time=0.1):
        """
        Name:    Piezo.beep
        Feature: Temporarily raise the 'piezo' channel. Default is three
                 short beeps in quick succession.
        Inputs:  - int, desired number of beeps (num_beeps)
                 - float, length of each beep in seconds (beep_time)
                 - float, length of silence between beeps (rest_time)
        Outputs: None
        """
        self.logger.debug('called')
        for i in range(num_beeps):
            # turn buzzer on
            self._pwm.setPWM(self._CHANNEL, 0, int(2048 * self._VOLUME))
            # sustain note
            time.sleep(beep_time)
            # turn buzzer off
            self._pwm.setPWM(self._CHANNEL, 0, 4096)
            # rest
            time.sleep(rest_time)

    def connect(self):
        self.logger.debug('start.')
        try:
            self.beep(2)
        except:
            self.logger.error('could not connect to piezo.')
            raise PRIDAPeripheralError('Could not connect to piezo!')
        self.logger.debug('complete.')

    def poweroff(self):
        self.logger.debug('called')
        self._pwm.setPWM(self._CHANNEL, 0, 0)

    def current_status(self):
        self.logger.debug('called')
        super(self.current_status())
