#!/usr/bin/python
#
# motor.py
#
# VERSION: 1.2.1
#
# LAST EDIT: 2016-01-13
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 1045bfe: Nathanael Shaw - 2016-01-04 11:43:10
#     add conf param type and value checking
# 1b87533: Nathanael Shaw - 2015-12-23 15:17:26
#     hotfix
# e48e914: Nathanael Shaw - 2015-12-22 14:28:57
#     update changelogs
# 76a746e: Nathanael Shaw - 2015-12-22 14:23:26
#     update docstrings and cleanup
# 2ac67e4: Nathanael Shaw - 2015-12-22 12:23:59
#     pass func handle in lieu of rel import
# 8efbde4: Nathanael Shaw - 2015-12-22 12:02:53
#     move conf_parser to utilities.py
# fb6b2af: twdavis - 2015-12-10 16:39:03
#     General clean up, style fixes, and debug message alterations
# 1947123: Nathanael Shaw - 2015-12-04 14:41:03
#     update config parse with try except
# 6482ee8: Nathanael Shaw - 2015-12-03 17:27:04
#     add filepath passing bewteen objects
# 1bf8599: Nathanael Shaw - 2015-12-03 14:34:54
#     add hardware object self configuring from file
# 4279d18: Nathanael Shaw - 2015-12-03 13:52:34
#     getting closer
# 0a1b0da: Nathanael Shaw - 2015-12-03 13:31:27
#     more debug logs
# c88e1bc: Nathanael Shaw - 2015-12-03 13:12:37
#     hotfix
# ae4178c: Nathanael Shaw - 2015-12-03 13:03:02
#     hotfix
# e1bd0f9: Nathanael Shaw - 2015-12-03 12:50:06
#     add conf debug log records
# 61fd0a4: Nathanael Shaw - 2015-12-03 12:45:58
#     hotfix
# 35a805e: Nathanael Shaw - 2015-12-03 12:31:49
#     add self config behav. to objects
# 15c343e: Nathanael Shaw - 2015-12-02 10:18:32
#     permit edits cleanup hotfix
# 541f9f6: Nathanael Shaw - 2015-12-02 10:16:29
#     permit edits cleanup hotfix
# ad3a146: Nathanael Shaw - 2015-12-02 09:37:28
#     attempt relative import fix
# e627560: Nathanael Shaw - 2015-12-01 19:08:44
#     add hardware module
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import time
import logging

from .Adafruit_MotorHAT import Adafruit_MotorHAT
from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class HAT(Adafruit_MotorHAT, PRIDAPeripheral):

    def __init__(self, my_parser=None, config_filepath='.'):
        """
        Initialize data memebers. Inherit attributes and
        behaviours from the Adafruit_MotorHAT object.
        Note: currently assumes that system is always using
        microstepping.
        """
        logging.debug('start initializing hat.')
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self.logger = logging.getLogger(__name__)
        self._rpm = 2.0           # Approximate rotational velocity
        self._step_res = 200      # Number of units steps per rotation
        self._motor_port_num = 1  # Motor HAT Port Number In Use
        self._sm = self.getStepper(self.step_res, self.motor_port_num)
        #                        ^ Adafruit Stepper Motor Object
        #                       \ / Minimum Time Elapsed In a Rotation
        self._base_rot_time = 1.75 * self._sm.MICROSTEPS
        self.attr_dict = {'RPM': 'rpm',
                          'DEGREES_PER_STEP': 'degrees',
                          'MICROSTEPS_PER_STEP': 'microsteps',
                          'PORT_NUMBER': 'motor_port_num',
                          }
        if my_parser is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('complete.')

    @property
    def microsteps(self):
        """
        Name:    Motor.microsteps
        Feature: Returns number of microsteps per unit step
        Inputs:  None
        Outputs: Int, number of microsteps per unit step (self.microsteps)
        """
        return self._sm.MICROSTEPS

    @microsteps.setter
    def microsteps(self, value):
        """
        Name:    Motor.microsteps
        Feature: Sets the number of microsteps per unit step
        Inputs:  Int, number of microsteps per unit step (value)
        Outputs: None
        """
        if isinstance(value, int):
            if value in {4, 8, 12, 16}:
                self._sm.MICROSTEPS = value
                self._sm._build_microstep_curve()
            else:
                self.logger.error(
                    'number of microsteps per step must be a multiple of four.'
                    )
                raise ValueError(
                    'number of microsteps per step must be a multiple of four.'
                    )
        else:
            self.logger.error(
                'number of microsteps per step must be an integer.'
                )
            raise TypeError(
                'number of microsteps per step must be an integer.'
                )

    @property
    def degrees(self):
        """
        Name:    Motor.degrees
        Feature: Returns Step Resolution in terms of degrees per unit step.
        Inputs:  None
        Outputs: Float, degree representation of step resolution
                 (Motor.degrees)
        """
        return 360.0 / self.step_res

    @degrees.setter
    def degrees(self, value):
        """
        Name:    Motor.degrees
        Feature: Sets Step Resolution in terms of degrees per unit step.
        Inputs:  Float, desired degree representation of step resolution
                 (Motor.degrees)
        Outputs: None
        """
        if isinstance(value, int) or isinstance(value, float):
            if 0 < value < 360:
                if (360.0 / value) % 1 == 0:
                    self.step_res = int(360 / value)
                else:
                    self.logger.error('degrees attribute' +
                                      ' must evenly divide 360.')
                    raise ValueError('Degrees attribute' +
                                     ' must evenly divide 360.')
            else:
                self.logger.error('degrees must be a value between 0 and 360.')
                raise ValueError('degrees must be a value between 0 and 360.')
        else:
            self.logger.error('degrees must be a integer or float.')
            raise TypeError('degrees must be a integer or float.')

    @property
    def rpm(self):
        """
        Name:    Motor.rpm
        Feature: Returns the approximate RPM value at which the motor is
                 to spin
        Inputs:  None
        Outputs: Float, value approximating currently assigned motor
                 rotational speed (self._rpm)
        """
        return self._rpm

    @property
    def step_res(self):
        """
        Name:    Motor.step_res
        Feature: Returns the number of unit steps within a single full
                 rotation at which the motor is currently configured
        Inputs:  None
        Outputs: Integer, number of unit steps per full rotation
                 (self._step_res)
        """
        return self._step_res

    @property
    def motor_port_num(self):
        """
        Name:    Motor.motor_port_num
        Feature: Returns the port number which the hat is currently
                 controlling
        Inputs:  None
        Outputs: Integer, the number representing the in-use motorHAT
                 port (self._motor_port_num)
        """
        return self._motor_port_num

    @property
    def base_rot_time(self):
        """
        Name:    Motor.base_rot_time
        Feature: Returns the minimum amount of time at which the motor
                 can complete a full rotation with a microstepping
                 regieme
        Inputs:  None
        Outputs: Float, minimum number of seconds for a full rotation
                 (self._base_rot_time)
        """
        return self._base_rot_time

    @rpm.setter
    def rpm(self, rot_per_min):
        """
        Name:    Motor.rpm
        Feature: Set approximate rotations per minute value for the
                 motor.
        Inputs:  float, desired rpm value (rot_per_min)
        Outputs: None
        """
        if isinstance(rot_per_min, float) or isinstance(rot_per_min, int):
            if rot_per_min > 0:
                self._rpm = rot_per_min
            else:
                self.logger.error('RPM requires a non-negative value.')
                raise ValueError('RPM requires a non-negative value.')
        else:
            self.logger.error('RPM must be a number.')
            raise TypeError('RPM must be a number.')

    @step_res.setter
    def step_res(self, sr):
        """
        Name:    Motor.step_res
        Feature: Set stepper motor step resolution. Not currently
                 supported. Data member added to reduce presence of
                 'magic numbers.'
        Inputs:  - int, desired 'step resolution,' or the number of
                   steps in a full rotation, defining the length of a
                   unit step (sr)
        Outputs: None
        """
        if isinstance(sr, int):
            if sr > 0:
                self._step_res = sr
            else:
                self.logger.error('step resolution must be non-negative.')
                raise ValueError('Step resolution must be non-negative')
        else:
            self.logger.error('step resolution must be an integer.')
            raise TypeError('Step resolution must be an integer')

    @motor_port_num.setter
    def motor_port_num(self, num):
        """
        Name:    Motor.motor_port_num
        Feature: Set stepper motor port number. Not currently supported.
                 Data member added to reduce presence of 'magic numbers.'
        Inputs:  - int, desired port number to be used on the HAT (num)
        Outputs: None
        """
        if isinstance(num, int):
            if num in {1, 2}:
                self._motor_port_num = num
            else:
                self.logger.error('motor port number restricted to 1 or 2.')
                raise ValueError('Motor port number restricted to 1 or 2.')
        else:
            self.logger.error('motor port number must be an integer.')
            raise TypeError('Motor port number must be an integer.')

    @base_rot_time.setter
    def base_rot_time(self, base_time):
        """
        Name:    Motor.base_rot_time
        Feature: Set stepper motor base rotation time (in seconds)
                 During full speed operation. Not currently supported.
                 Data member added to reduce presence of 'magic numbers.'
        Inputs:  - int, minmum rotation time in seconds (base_time)
        Outputs: None
        """
        if isinstance(base_time, int) or isinstance(base_time, float):
            if base_time > 0:
                self._base_rot_time = base_time
            else:
                self.logger.error('base rotation time' +
                                  ' must be non-negative.')
                raise ValueError('Base rotation time must be non-negative.')
        else:
            self.logger.error('base rotation time must be a number.')
            raise TypeError('Base rotation time must be a number.')

    def turn_off_motors(self):
        """
        Name:    Motor.turn_off_motors
        Feature: Disable all motors attached to the HAT.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('called')
        self.getMotor(1).run(self.RELEASE)
        self.getMotor(2).run(self.RELEASE)
        self.getMotor(3).run(self.RELEASE)
        self.getMotor(4).run(self.RELEASE)

    def calibrate(self):
        """
        Name:    Motor.calibrate
        Feature: Take three initial unit steps.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('called')
        for i in range(3 * self._sm.MICROSTEPS):
            self._sm.oneStep(self.FORWARD, self.MICROSTEP)
            time.sleep(.1)

    def step(self, num_photos):
        """
        Name:    Motor.step
        Feature: Take a single Inter-Photo-Angle (IPA) step.
                 Take the desired number of photos in the run and
                 calculate the IPA step size. Convert the desired RPM
                 value into the appropriate length wait time.
        Inputs:  Integer, number of photos to be taken (num_photos)
        Outputs: None
        """
        self.logger.debug('start.')
        # data member error checking
        if self.rpm <= 0:
            self.logger.error('RPM must be greater than zero.')
            raise ValueError("RPM must be greater than zero.")
        try:
            # convert RPM to req'd value for calculating wait time
            real_rpm = (1.0 / (((60.0 / self.rpm) -
                               self.base_rot_time) / 60.0))
            if real_rpm <= 0:
                raise ZeroDivisionError()
        except ZeroDivisionError:
            self.logger.error('set a valid RPM value.')
            raise AttributeError("Set a valid RPM value.")
        else:
            # begin stepping
            for i in range((self.step_res * self._sm.MICROSTEPS) /
                           num_photos):
                self._sm.oneStep(self.FORWARD, self.MICROSTEP)
                time.sleep(60.0 / (self.step_res *
                                   self._sm.MICROSTEPS * real_rpm))
        self.logger.debug('complete.')

    def single_step(self):
        """
        Name:    motor.single_step
        Feature: Takes a single microstep
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        # data member error checking
        if self.rpm <= 0:
            self.logger.error('RPM value must be greater than zero.')
            raise ValueError("RPM must be greater than zero.")
        try:
            # convert RPM to req'd value for calculating wait time
            real_rpm = (1.0 / (((60.0 / self.rpm) -
                               self.base_rot_time) / 60.0))
            if real_rpm <= 0:
                raise ZeroDivisionError()
        except ZeroDivisionError:
            self.logger.error('set a valid RPM value before' +
                              ' attempting a step.')
            raise AttributeError("Set a valid RPM value" +
                                 " before attempting a step.")
        else:
            self._sm.oneStep(self.FORWARD, self.MICROSTEP)
            time.sleep(60.0 / (self.step_res * self._sm.MICROSTEPS * real_rpm))
        self.logger.debug('complete.')

    def connect(self):
        self.logger.debug('called')
        try:
            self.calibrate()
        except:
            raise PRIDAPeripheralError('Could not connect to motor!')

    def poweroff(self):
        self.logger.debug('called')
        self.turn_off_motors()

    def current_status(self):
        self.logger.debug('called')
        super(self.current_status())
