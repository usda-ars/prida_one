#!/usr/bin/python
#
# camera.py
#
# VERSION: 1.2.2
#
# LAST EDIT: 2016-07-19
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 7a70dbc: Nathanael Shaw - 2016-01-12 10:51:13
#     Address #58 and #59. handle whitespace, escapes
# 1045bfe: Nathanael Shaw - 2016-01-04 11:43:10
#     add conf param type and value checking
# ea29eb9: Nathanael Shaw - 2015-12-24 10:50:46
#     add path property getter and setter
# 1b87533: Nathanael Shaw - 2015-12-23 15:17:26
#     hotfix
# e48e914: Nathanael Shaw - 2015-12-22 14:28:57
#     update changelogs
# 76a746e: Nathanael Shaw - 2015-12-22 14:23:26
#     update docstrings and cleanup
# 2ac67e4: Nathanael Shaw - 2015-12-22 12:23:59
#     pass func handle in lieu of rel import
# 8efbde4: Nathanael Shaw - 2015-12-22 12:02:53
#     move conf_parser to utilities.py
# 93bd47a: Nathanael Shaw - 2015-12-21 16:27:40
#     Address #48. hotfix
# c045110: Nathanael Shaw - 2015-12-21 16:25:04
#     Address #48. Remove orientation apostophe.
# 0036c58: Nathanael Shaw - 2015-12-21 16:17:33
#     Address #48. Fix width property
# 7f02b97: Nathanael Shaw - 2015-12-21 16:14:32
#     Address #48. Return values not widgets
# 26f405e: Nathanael Shaw - 2015-12-21 16:12:57
#     Address #48. Add missing return.
# bba735f: Nathanael Shaw - 2015-12-21 16:08:12
#     Address #48. Fix typo.
# 77705fb: Nathanael Shaw - 2015-12-21 16:02:17
#     Address #48. get attr by name or label
# 466eac3: Nathanael Shaw - 2015-12-14 12:06:41
#     fix typo
# 278825d: twdavis - 2015-12-10 18:06:39
#     updated logging statements
# d261751: twdavis - 2015-12-10 17:27:12
#     More updates/fixes
# 1947123: Nathanael Shaw - 2015-12-04 14:41:03
#     update config parse with try except
# 6482ee8: Nathanael Shaw - 2015-12-03 17:27:04
#     add filepath passing bewteen objects
# 1bf8599: Nathanael Shaw - 2015-12-03 14:34:54
#     add hardware object self configuring from file
# 4279d18: Nathanael Shaw - 2015-12-03 13:52:34
#     getting closer
# 0a1b0da: Nathanael Shaw - 2015-12-03 13:31:27
#     more debug logs
# c88e1bc: Nathanael Shaw - 2015-12-03 13:12:37
#     hotfix
# ae4178c: Nathanael Shaw - 2015-12-03 13:03:02
#     hotfix
# e1bd0f9: Nathanael Shaw - 2015-12-03 12:50:06
#     add conf debug log records
# 61fd0a4: Nathanael Shaw - 2015-12-03 12:45:58
#     hotfix
# 35a805e: Nathanael Shaw - 2015-12-03 12:31:49
#     add self config behav. to objects
# ad3a146: Nathanael Shaw - 2015-12-02 09:37:28
#     attempt relative import fix
# e627560: Nathanael Shaw - 2015-12-01 19:08:44
#     add hardware module
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import os
import os.path
import logging

import gphoto2 as gp

from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class Camera(PRIDAPeripheral):

    # camera_set = {}
    # available = {}
    logger = logging.getLogger(__name__)

    def __init__(self, my_parser=None, config_filepath='.'):
        """
        Initialize data members, connect the camera.
        """
        self.logger.debug('start initializing camera.')
        PRIDAPeripheral.__init__(self)
        self._path = '%s' % os.path.join(
            os.path.dirname(config_filepath), 'photos')
        self.context = None
        self.camera = None
        self.config = None
        self._summary = ''
        #self._attr_dict = {}
        self.label_list = []
        self.name_list = []
        self.connect()
        self.attr_dict = {'IMAGE_DIR': 'path',
                          }
        if my_parser is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('complete.')

    @property
    def path(self):
        """
        @TODO
        """
        return self._path

    @path.setter
    def path(self, val):
        """
        @TODO
        """
        if isinstance(val, basestring):
            if os.path.isdir(val):
                self._path = val
            else:
                raise ValueError('"%s" is not a valid directory' % (val))
        else:
            raise TypeError('path attribute must be a string')

    def connect(self):
        """
        Name:    Camera.connect
        Feature: Attempts to connect a camera via gphoto2
        Inputs:  None
        Outputs: None
        """
        try:
            if not self.context:
                self.context = gp.Context()
                self.camera = gp.Camera()
                self.config = self.camera.get_config(self.context)
                self._find_all_children(self.config)
            else:
                self.logger.warning('Camera already connected.')
        except:
            self.logger.exception('Could not connect to camera.')
            raise PRIDAPeripheralError("Could not connect to camera.")

    def _find_all_children(self, camera_widget):
        """
        Name:    Camera._find_all_children
        Feature: recursive search to accumulate all camera attribute names
                 and labels
        Input:   CameraWidget*, a gphoto2 Camera Widget (camera_widget)
        Output:  None
        """
        num_children = camera_widget.count_children()
        if num_children == 0:
            self.label_list.append(camera_widget.get_label())
            self.name_list.append(camera_widget.get_name())
        else:
            for i in range(num_children):
                self._find_all_children(camera_widget.get_child(i))

    def poweroff(self):
        """
        Name:    Camera.poweroff
        Feature: Prepares the camera for manual shutdown.
                 Overrides PRIDAPeripheral.poweroff.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        err = gp.check_result(gp.gp_camera_exit(self.camera, self.context))
        if err >= 1:
            self.logger.error('could not disconnect camera for poweroff.')
            raise PRIDAPeripheralError('Could not disconnect' +
                                       ' camera for poweroff!')
        else:
            self.context = None
            self.camera = None
            self.config = None
            self._summary = ''
            self._attr_dict = {}
            self.logger.info('Camera disconnected. Safe to power down.')
        self.logger.debug('complete.')

    def current_status(self):
        """
        Name:    Camera.current_status
        Feature: Identifies the current status of the camera.
                 Overrides PRIDAPeripheral.check_status.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('called')
        super(self.current_status())

    def get_full_path(self):
        """
        Name:    Camera.get_full_path
        Feature: Returns a string representing the filepath to the
                 working directory
        Inputs:  None
        Outputs: String, path to working directory (self.path)
        """
        self.logger.debug('called.')
        return self.path

    def set_path(self, p):
        """
        Name:    Camera.set_path
        Feature: Sets the path to the working directory
        Inputs:  String, path to save images to (p)
        Outputs: None
        """
        self.logger.debug("setting path to '%s'", p)
        self.path = p

    def capture(self, name, verbose=False):
        """
        Name:    Camera.capture
        Feature: Captures an image with the connected camera
        Inputs:  - String, name of the image to be saved (name)
                 - Bool, enable verbose output (verbose)
        Outputs: String, absolute file path of captured image (filename)
        """
        self.logger.debug('start.')
        file_path = gp.check_result(gp.gp_camera_capture(self.camera,
                                                         gp.GP_CAPTURE_IMAGE,
                                                         self.context))
        filename = os.path.join(self.path, str(name) + ".jpg")
        self.logger.debug(('Copying image to: ' + filename))
        camera_file = gp.check_result(
            gp.gp_camera_file_get(self.camera,
                                  file_path.folder,
                                  file_path.name,
                                  gp.GP_FILE_TYPE_NORMAL,
                                  self.context)
            )
        gp.check_result(gp.gp_file_save(camera_file, filename))
        gp.check_result(gp.gp_camera_exit(self.camera, self.context))
        self.logger.debug('complete.')
        return filename

    @property
    def exposure_time(self):
        """
        Name:    Camera.exposure_time
        Feature: Property describing camera exposure time or "shutter speed"
        Inputs:  None
        Outputs: Float, duration during image capture of open shutter
                 (self.exposure_time)
        """
        self.logger.debug('returning camera exposure time')
        return self._attr_getter('Shutter Speed').strip('s')

    @property
    def focal_length(self):
        """
        Name:    Camera.focal_length
        Feature: Returns the focal length of the camera (in mm)
        Inputs:  None
        Outputs: Integer representing the focal length
        """
        self.logger.debug('returning camera focal length')
        return self._attr_getter('Focal Length')

    @property
    def orientation(self):
        """
        Name:    Camera.orientation
        Feature: Returns angle value corresponding to camera position (using
                 standard polar coordinates)
        Inputs:  None
        Outputs: Integer, the angular position of the camera
        """
        self.logger.debug('returning camera orientation')
        return self._attr_getter('orientation').strip("'")

    @property
    def iso_speed(self):
        """
        Name:    Camera.iso_speed
        Feature: Returns integer representing camera iso speed
        Inputs:  None
        Outputs: String, the manufacturer and device model of the camera
        """
        self.logger.debug('returning camera ISO speed')
        return self._attr_getter('iso')

    @property
    def aperture(self):
        """
        Name:    Camera.aperture
        Feature: Returns integer representing camera aperture (if determinable)
        Inputs:  None
        Outputs: Integer, the camera aperture (if known)
        """
        self.logger.debug('returning camera aperture')
        apt_at_min = self._attr_getter(
            'Maximum Aperture at Focal Length Maximum')
        apt_at_max = self._attr_getter(
            'Maximum Aperture at Focal Length Minimum')
        if apt_at_min == apt_at_max:
            return apt_at_min
        else:
            val = min([apt_at_min, apt_at_max])
            return 'Unknown. Less than %s.' % (val)

    @property
    def image_size(self):
        """
        Name:    Camera.image_size
        Feature: Returns camera image resolution
        Inputs:  None
        Outputs: List (int), the pixel height and width of the images
        """
        self.logger.debug('returning image_size')
        img_size = self._attr_getter('Image Size')
        img_size = img_size.split('x')
        return [img_size[0], img_size[1]]

    @property
    def image_height(self):
        """
        Name:    Camera.image_height
        Feature: Returns height dimension of the image resolution
        Inputs:  None
        Outputs: Integer, the image height in pixels
        """
        self.logger.debug('returning image height')
        tmp = self.image_size
        if int(self.orientation) in {90, 270}:
            tmp.sort()
            return tmp.pop()
        else:
            tmp.sort(reverse=True)
            return tmp.pop()

    @property
    def image_width(self):
        """
        Name:    Camera.image_width
        Feature: Returns width dimension of the image resolution
        Inputs:  None
        Outputs: Integer, the image width in pixels
        """
        self.logger.debug('returning image width')
        tmp = self.image_size
        if int(self.orientation) in {0, 180}:
            tmp.sort()
            return tmp.pop()
        else:
            tmp.sort(reverse=True)
            return tmp.pop()

    @property
    def summary(self):
        """
        Name:    Camera.summary
        Feature: Returns string containing gphoto2 camera summary data
        Inputs:  None
        Outputs: String, the manufacturer and device model of the camera
        """
        self.logger.debug('returning camera summary')
        return ' '.join([self.make, self.model])

    @property
    def make(self):
        """
        Name:    Camera.make
        Feature: Returns string containing camera manufacturer
        Inputs:  None
        Outputs: String, the manufacturer of the camera
        """
        self.logger.debug('returning camera make')
        return self._attr_getter('Camera Manufacturer')

    @property
    def model(self):
        """
        Name:    Camera.model
        Feature: Returns string containing camera model
        Inputs:  None
        Outputs: String, the model of the camera
        """
        self.logger.debug('returning camera model')
        return self._attr_getter('Camera Model')

    def _attr_getter(self, attr):
        """
        Name:    Camera._attr_getter
        Feature: takes a attribute name or label string and calls the
                 appropriate getter function to return the desired attribute
        Inputs:  str, attribute name or label as defined by gphoto2 (attr)
        Outputs: the attribute's value (val)
        """
        if attr.lower() == attr:
            val = self._get_atter_by_name(attr)
        else:
            val = self._get_atter_by_label(attr)
        return val

    def _get_atter_by_label(self, label):
        """
        Name:    Camera._get_attr_by_label
        Feature: Gets a gphoto2 camera attribute by its label
        Inputs:  str, the attributes label (label)
        Outputs: the attribute's value
        """
        if label in self.label_list:
            return self.config.get_child_by_label(label).get_value()
        else:
            self.logger.error('%s not a valid camera attriute label.' % (label))
            raise PRIDAPeripheralError(
                '%s not a valid camera attriute label.' % (label)
                )

    def _get_atter_by_name(self, name):
        """
        Name:    Camera._get_attr_by_name
        Feature: Gets a gphoto2 camera attribute by its name
        Inputs:  str, the attributes name (name)
        Outputs: the attribute's value
        """
        if name in self.name_list:
            return self.config.get_child_by_name(name).get_value()
        else:
            self.logger.error('%s not a valid camera attriute name.' % (name))
            raise PRIDAPeripheralError(
                '%s not a valid camera attriute name.' % (name)
                )
