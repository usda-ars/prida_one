#!/usr/bin/python
#
# global_conf.py
#
# VERSION: 1.2.1
#
# LAST EDIT: 2016-01-13
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 35c865b: Nathanael Shaw - 2016-01-11 17:37:27
#     make readonly attrs raise AttributeErrors
# e4d06ac: twdavis - 2016-01-07 16:26:38
#     Moved read exif tags function to utilities.py
# d7e32b2: twdavis - 2016-01-07 12:18:37
#     added some comments
# 149c416: twdavis - 2016-01-07 11:38:13
#     use hidden value
# dd0c9be: twdavis - 2016-01-07 11:35:13
#     updated logging statements
# 448412b: twdavis - 2016-01-07 11:20:14
#     Addresses #22. Added logging statements.
# 930d7b0: twdavis - 2016-01-07 10:55:34
#     Addresses #2. Python 2/3 type conflict.
# 9e56d33: Nathanael Shaw - 2016-01-06 13:36:28
#     add prida dir to globalconf
# a2286fb: Nathanael Shaw - 2016-01-06 12:14:01
#     fix typo
# 04fd519: Nathanael Shaw - 2016-01-06 12:11:55
#     Merge branch 'master' of bitbucket.org:usda-ars/prida_one
# 17735ad: Nathanael Shaw - 2016-01-06 12:10:29
#     add conf dir and filename atrs; forbid setting
# 8c5663a: twdavis - 2016-01-06 11:45:24
#     style fixes
# 5e5fd64: twdavis - 2016-01-04 13:55:17
#     hotfix
# 19ec97e: Nathanael Shaw - 2016-01-04 13:34:38
#     add docstrings
# 1045bfe: Nathanael Shaw - 2016-01-04 11:43:10
#     add conf param type and value checking
# e48e914: Nathanael Shaw - 2015-12-22 14:28:57
#     update changelogs
# 76a746e: Nathanael Shaw - 2015-12-22 14:23:26
#     update docstrings and cleanup
# 2ac67e4: Nathanael Shaw - 2015-12-22 12:23:59
#     pass func handle in lieu of rel import
# 8efbde4: Nathanael Shaw - 2015-12-22 12:02:53
#     move conf_parser to utilities.py
# e8142f7: Nathanael Shaw - 2015-12-22 11:10:07
#     add custom filter to global conf
# 5a043ea: Nathanael Shaw - 2015-12-22 10:03:20
#     undo delete of self keyword for assignment
# 25cbff4: Nathanael Shaw - 2015-12-22 09:45:44
#     add logging to fail cases
# b6f31cb: Nathanael Shaw - 2015-12-21 09:01:08
#     minor updates
# cf5148b: Nathanael Shaw - 2015-12-17 12:57:42
#     remove conf dir and conf name from config parser
# 095d501: Nathanael Shaw - 2015-12-17 12:48:21
#     fix logger
# 39d460e: Nathanael Shaw - 2015-12-16 12:06:54
#     update main conf
# baf8e11: Nathanael Shaw - 2015-12-16 11:57:35
#     add GlobalConf object for handlng global vars
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import logging.handlers
import os

from utilities import conf_parser

# Type handing for Python 2/3 compatibility
# Ref: Kelly, R. "Preparing PyEnchant for Python 3.0." Available online:
#      http://www.rfk.id.au/blog/entry/preparing-pyenchant-for-python-3/
try:
    unicode = unicode
except NameError:
    # 'unicode' is undefined, must be Python 3
    str = str
    unicode = str
    bytes = bytes
    basestring = (str, bytes)
else:
    # 'unicode' exists, must be Python 2
    str = str
    unicode = unicode
    bytes = str
    basestring = basestring


###############################################################################
# CLASSES:
###############################################################################
class GlobalConf(object):

    # Initialize Prida Global vars. as Data Members
    def __init__(self):
        # set default values
        my_user = os.path.expanduser('~')
        self._prida_dir = os.path.join(my_user, 'Prida')
        # Initialize memory handler for log record buffering
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        self.handler = logging.handlers.MemoryHandler(
            (66 * 1024),
            flushLevel=logging.CRITICAL,
            target=logging.NullHandler())
        self.logger.addHandler(self.handler)
        self._conf_dir = self._prida_dir
        self._cred_dir = self._prida_dir
        self._log_dir = self._prida_dir
        self._dict_dir = self._prida_dir
        self._conf_filename = 'prida.config'
        self._cred_filename = 'credentials'
        self._log_filename = 'prida.log'
        self._dict_filename = 'dictionary.txt'
        self._log_level = 'info'
        self.attr_dict = {
            'LOG_DIR': 'log_dir',
            'CREDENTIAL_DIR': 'cred_dir',
            'DICTIONARY_DIR': 'dict_dir',
            'LOG_FILENAME': 'log_filename',
            'CREDENTIAL_FILENAME': 'cred_filename',
            'DICTIONARY_FILENAME': 'dict_filename',
            'LOG_LEVEL': 'log_level',
            }
        config_filepath = (os.path.join(self._conf_dir, self._conf_filename))
        self.logger.info('configuring global variables')
        conf_parser(self, __name__, config_filepath)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def collect_log_records(self, target_handler):
        """
        Name:    GlobalConf.collect_log_records
        Feature: Sets the memory handlers target and flushes collected records
                 from buffer to target handler
        Inputs:  logging.Handler, An instance of a Handler object
                 (target_handler)
        Outputs: None
        """
        self.handler.setTarget(target_handler)
        self.handler.flush()

    @property
    def conf_dir(self):
        """
        Name:    global_conf.conf_dir
        Feature: conf_dir getter
        Inputs:  None
        Outputs: Str, conf_dir
        """
        self.logger.debug("%s", self._conf_dir)
        return self._conf_dir

    @conf_dir.setter
    def conf_dir(self, value):
        """
        Name:    global_conf.conf_dir
        Feature: conf_dir setter
        Inputs:  Str, new conf_dir value
        Outputs: None
        """
        self.logger.error(
            "changing the configuration file directory is not permitted")
        raise AttributeError(
            "changing the configuration file directory is not permitted")

    @property
    def cred_dir(self):
        """
        Name:    global_conf.cred_dir
        Feature: cred_dir getter
        Inputs:  None
        Outputs: Str, cred_dir
        """
        self.logger.debug("%s", self._cred_dir)
        return self._cred_dir

    @cred_dir.setter
    def cred_dir(self, value):
        """
        Name:    global_conf.cred_dir
        Feature: cred_dir setter
        Inputs:  Str, new cred_dir value
        Outputs: None
        """
        if isinstance(value, basestring):
            if os.path.isdir(value):
                self.logger.debug("set to %s", value)
                self._cred_dir = value
            else:
                self.logger.error(
                    "credential directory must be a pre-existing directory")
                raise ValueError(
                    "credential directory must be a pre-existing directory")
        else:
            self.logger.error("credential directory must be a string")
            raise TypeError("credential directory must be a string")

    @property
    def dict_dir(self):
        """
        Name:    global_conf.dict_dir
        Feature: dict_dir getter
        Inputs:  None
        Outputs: Str, dict_dir
        """
        self.logger.debug("%s", self._dict_dir)
        return self._dict_dir

    @dict_dir.setter
    def dict_dir(self, value):
        """
        Name:    global_conf.dict_dir
        Feature: dict_dir setter
        Inputs:  Str, new dict_dir value
        Outputs: None
        """
        if isinstance(value, basestring):
            if os.path.isdir(value):
                self.logger.debug("set to %s", value)
                self._dict_dir = value
            else:
                self.logger.error(
                    "dictionary directory must be a pre-existing directory")
                raise ValueError(
                    "dictionary directory must be a pre-existing directory")
        else:
            self.logger.error("dictionary directory must be a string")
            raise TypeError("dictionary directory must be a string")

    @property
    def log_dir(self):
        """
        Name:    global_conf.log_dir
        Feature: log_dir getter
        Inputs:  None
        Outputs: Str, log_dir
        """
        self.logger.debug("%s", self._log_dir)
        return self._log_dir

    @log_dir.setter
    def log_dir(self, value):
        """
        Name:    global_conf.log_dir
        Feature: log_dir setter
        Inputs:  Str, new log_dir value
        Outputs: None
        """
        if isinstance(value, basestring):
            if os.path.isdir(value):
                self.logger.debug("set to %s", value)
                self._log_dir = value
            else:
                self.logger.error(
                    "logging directory must be a pre-existing directory")
                raise ValueError(
                    "logging directory must be a pre-existing directory")
        else:
            self.logger.error("logging directory must be a string")
            raise TypeError("logging directory must be a string")

    @property
    def prida_dir(self):
        """
        Name:    global_conf.prida_dir
        Feature: prida_dir getter
        Inputs:  None
        Outputs: Str, prida_dir
        """
        self.logger.debug("%s", self._prida_dir)
        return self._prida_dir

    @prida_dir.setter
    def prida_dir(self, value):
        """
        Name:    global_conf.prida_dir
        Feature: prida_dir setter
        Inputs:  Str, new prida_dir value
        Outputs: None
        """
        self.logger.error("changing the prida directory is forbidden")
        raise AttributeError("changing the prida directory is forbidden")

    @property
    def conf_filename(self):
        """
        Name:    global_conf.conf_filename
        Feature: conf_filename getter
        Inputs:  None
        Outputs: Str, conf_filename
        """
        self.logger.debug("%s", self._conf_filename)
        return self._conf_filename

    @conf_filename.setter
    def conf_filename(self, value):
        """
        Name:    global_conf.conf_filename
        Feature: conf_filename setter
        Inputs:  Str, new conf_filename value
        Outputs: None
        """
        self.logger.error(
            "changing the configuration file name is not permitted")
        raise AttributeError(
            "changing the configuration file name is not permitted")

    @property
    def cred_filename(self):
        """
        Name:    global_conf.cred_filename
        Feature: cred_filename getter
        Inputs:  None
        Outputs: Str, cred_filename
        """
        self.logger.debug("%s", self._cred_filename)
        return self._cred_filename

    @cred_filename.setter
    def cred_filename(self, value):
        """
        Name:    global_conf.cred_filename
        Feature: cred_filename setter
        Inputs:  Str, new cred_filename value
        Outputs: None
        """
        if isinstance(value, basestring):
            self.logger.debug("set to %s", value)
            self._cred_filename = value
        else:
            self.logger.error("credential file name must be a string")
            raise TypeError("credential file name must be a string")

    @property
    def log_filename(self):
        """
        Name:    global_conf.log_filename
        Feature: log_filename getter
        Inputs:  None
        Outputs: Str, log_filename
        """
        self.logger.debug("%s", self._log_filename)
        return self._log_filename

    @log_filename.setter
    def log_filename(self, value):
        """
        Name:    global_conf.log_filename
        Feature: log_filename setter
        Inputs:  Str, new log_filename value
        Outputs: None
        """
        if isinstance(value, basestring):
            self.logger.debug("set to %s", value)
            self._log_filename = value
        else:
            self.logger.error("logging file name must be a string")
            raise TypeError("logging file name must be a string")

    @property
    def dict_filename(self):
        """
        Name:    global_conf.dict_filename
        Feature: dict_filename getter
        Inputs:  None
        Outputs: Str, dict_filename
        """
        self.logger.debug("%s", self._dict_filename)
        return self._dict_filename

    @dict_filename.setter
    def dict_filename(self, value):
        """
        Name:    global_conf.dict_filename
        Feature: dict_filename setter
        Inputs:  Str, new dict_filename value
        Outputs: None
        """
        if isinstance(value, basestring):
            self.logger.debug("set to %s", value)
            self._dict_filename = value
        else:
            self.logger.error("dictionary file name must be a string")
            raise TypeError("dictionary file name must be a string")

    @property
    def log_level(self):
        """
        Name:    global_conf.log_level
        Feature: log_level getter
        Inputs:  None
        Outputs: Str, log_level
        """
        self.logger.debug("%s", self._log_level)
        return self._log_level

    @log_level.setter
    def log_level(self, value):
        """
        Name:    global_conf.log_level
        Feature: log_level setter
        Inputs:  Str, log_level
        Outputs: None
        """
        if isinstance(value, basestring):
            opts = ('debug', 'info', 'warning', 'error', 'critical')
            if value.lower() in opts:
                self.logger.debug("set to %s", value)
                self._log_level = value
            else:
                self.logger.error("'%s' not a valid log level" % value)
                raise ValueError("'%s' is not a valid log level" % value)
        else:
            self.logger.error("log level must be a string")
            raise TypeError("log level must be a string")


class CustomFilter(logging.Filter):

    def __init__(self, myLevel=logging.DEBUG):
        # Create custom filter instance
        super(CustomFilter, self).__init__()
        self.myLevel = myLevel

    def filter(self, record):
        """
        @OVERRIDES logging.Filter.filter
        Name:    CustomFilter.filter
        Feature: selects logs for emitting based on their log level.
        Inputs:  LogRecord, an instance of the LogRecord class (record)
        Outputs: bool, boolean indicating to handler whether or not to emit
                 record
        """
        if record.levelno >= self.myLevel:
            return True
        else:
            return False
