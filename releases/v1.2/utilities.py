#!/usr/bin/python
#
# utilities.py
#
# VERSION 1.2.1
#
# LAST EDIT: 2016-01-13
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 04d776a: Nathanael Shaw - 2016-01-12 12:18:07
#     address #58 and #59. remove prida_path func
# 7a70dbc: Nathanael Shaw - 2016-01-12 10:51:13
#     Address #58 and #59. handle whitespace, escapes
# 65f7d27: twdavis - 2016-01-11 18:16:38
#     Added ctime getter for missing DateTime exif tags and added height and width dataset attributes to import
# a5cbc5d: twdavis - 2016-01-11 13:36:38
#     Addresses #56. PridaHDF configurable compression level.
# e4d06ac: twdavis - 2016-01-07 16:26:38
#     Moved read exif tags function to utilities.py
# 6fe011a: twdavis - 2016-01-07 12:28:12
#     hotfix
# 18f95dd: twdavis - 2016-01-07 12:27:20
#     Addresses #22. Added valid line check to config file.
# d7e32b2: twdavis - 2016-01-07 12:18:37
#     added some comments
# dd0c9be: twdavis - 2016-01-07 11:35:13
#     updated logging statements
# 8c5663a: twdavis - 2016-01-06 11:45:24
#     style fixes
# e48e914: Nathanael Shaw - 2015-12-22 14:28:57
#     update changelogs
# 8efbde4: Nathanael Shaw - 2015-12-22 12:02:53
#     move conf_parser to utilities.py
# b6f31cb: Nathanael Shaw - 2015-12-21 09:01:08
#     minor updates
# b5ca1bf: Nathanael Shaw - 2015-12-15 15:29:59
#     add initial utilities.py draft
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import datetime
import os.path
import sys
import time

import PIL.ExifTags as ExifTags


###############################################################################
# FUNCTIONS:
###############################################################################
def get_ctime(file_path):
    """
    Name:     get_ctime
    Inputs:   str, file path (file_path)
    Outputs:  datetime.datetime object (my_date)
    Features: Returns datetime object of file's *creation* time
    Note:     Creation time is not actual file creation time on *nix systems,
              rather, it refers to the last time the inode data changed
    """
    try:
        my_date = datetime.datetime.strptime(
            time.ctime(os.path.getctime(file_path)),
            "%a %b %d %H:%M:%S %Y")
    except OSError:
        my_date = ""
    finally:
        return my_date


def resource_path(relative_path):
    """
    Name:     resource_path
    Inputs:   str, resource file with path (relative_path)
    Outputs:  str, resource file with a potentially modified path
    Features: Returns absolute path for a given resource, works for dev and
              for PyInstaller.
    """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except AttributeError:
        base_path = sys.path[0]

    return os.path.join(base_path, relative_path)


def read_exif_tags(img):
    """
    Name:     read_exif_tags
    Inputs:   PIL.Image (img)
    Outputs:  dict, exif tag values (my_tags)
    Features: Returns dictionary of image exif tags
    """
    # Define Exif Tags:
    my_tags = {"Make": "",                 # Exif Tag (IFD0) 271
               "Model": "",                # Exif Tag (IFD0) 272
               "Orientation": "",          # Exif Tag (IFD0) 274
               "DateTime": "",             # Exif Tag (IFD0) 306
               "ISOSpeedRatings": "",      # Exif Tag (SubIFD) 34855
               "ShutterSpeedValue": "",    # Exif Tag (SubIFD) 37377
               "ApertureValue": ""}        # Exif Tag (SubIFD) 37378
    try:
        exif = {ExifTags.TAGS[k]: v for k, v in img._getexif().items()
                if k in ExifTags.TAGS}
    except:
        # Unable to gather exif tags from image
        exif = {}
    else:
        for tag in my_tags:
            try:
                my_attr = exif.get(tag, "")
            except:
                my_attr = ""
            else:
                # Handle tuple values:
                if isinstance(my_attr, tuple):
                    # Try reading first entry:
                    try:
                        my_attr = str(my_attr[0])
                    except:
                        my_attr = ""
                else:
                    try:
                        my_attr = str(my_attr)
                    except:
                        my_attr = ""

                if tag == "DateTime":
                    try:
                        # Parse date from datetime:
                        im_date = my_attr.split(' ')[0]
                        my_attr = im_date.replace(":", "-")
                    except:
                        my_attr = ""
                my_tags[tag] = my_attr

    return my_tags


def conf_parser(obj_inst, obj_name, config_file):
    """
    Name:    conf_parser
    Feature: Parses config file for relevant entries and assigns object
             attributes the user defined values found in the file
    Inputs:  - obj, instance of object to configure (obj_inst)
             - str, name of script calling the parser (obj_name)
             - str, absolute path to config file (config_file)
    Outputs: None
    """
    # Track whether we found a matching entry in the config file:
    found_match = False
    try:
        with open(config_file, 'r') as my_conf:
            for line in my_conf.readlines():
                words = line.split()

                # Check config file line formating
                if not len(words) == 3:
                    obj_inst.logger.error(
                        'config file poorly conditioned, check formatting.')
                    raise IOError(
                        'config file poorly conditioned, check formatting.')

                # Only access valid lines that match script/module name:
                if obj_name.upper() == words[0]:
                    found_match = True

                    # Check class attribute dictionary for valid entry:
                    if words[1] in obj_inst.attr_dict.keys():

                        # Double check that class has the attribute variable:
                        if hasattr(obj_inst, obj_inst.attr_dict[words[1]]):
                            try:

                                # Check if OS is windows and
                                # the attribute is a directory:
                                if os.name is 'nt' and '_DIR' in words[1]:

                                    # Replace escape characters:
                                    words[2] = words[2].replace(os.sep, '/')

                                # Set attribute value:
                                exec("obj_inst.%s = %s" % (
                                    obj_inst.attr_dict[words[1]], words[2]))
                                obj_inst.logger.debug("%s.%s = %s" % (
                                    obj_name,
                                    obj_inst.attr_dict[words[1]],
                                    words[2]))
                            except (ValueError, TypeError, AttributeError):
                                obj_inst.logger.exception(
                                    "failed to set %s.%s to %s" % (
                                        obj_name,
                                        obj_inst.attr_dict[words[1]],
                                        words[2]))
                            else:
                                obj_inst.logger.debug("%s.%s = %s" % (
                                    obj_name,
                                    obj_inst.attr_dict[words[1]],
                                    words[2]))
                        else:
                            obj_inst.logger.warning(
                                "%s does not have attribute %s" % (
                                    obj_name, obj_inst.attr_dict[words[1]]))
                    else:
                        obj_inst.logger.warning(
                            "%s is not an configurable attribute of %s" % (
                                words[1], obj_name))
                else:
                    obj_inst.logger.debug("ignoring %s", line.rstrip())
    except IOError:
        obj_inst.logger.exception(
            "Could not find config file at '%s'! Using defaults.", config_file)
    else:
        if not found_match:
            obj_inst.logger.warning("%s not found in config file!", obj_name)
