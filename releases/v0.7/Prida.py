#!/usr/bin/python
#
# Prida.py
#
# VERSION: 0.7.0-dev
#
# LAST EDIT: 2015-10-21
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import sys
import os
import glob

import PyQt5.uic as uic
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QAbstractItemView
#from PyQt5.QtWidgets import QErrorMessage # @TODO
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QThread
from PyQt5.QtCore import QSize
from PyQt5.QtCore import QDate  # used in session_attrs (as string)
import PIL.ImageQt as ImageQt
import PIL.Image as Image
import PIL.ExifTags as ExifTags

from hdf_organizer import PridaHDF

try:
    from imaging import Imaging
    hardware_mode = True
except ImportError:
    # MODE 1 INDICATES ERROR LIKELY FROM NONEXISTENCE OF HARDWARE
    print("Imaging hardware not set up")
    hardware_mode = False

###############################################################################
## GLOBAL VARIABLES:
###############################################################################
# PRIDA VERSION
PRIDA_VERSION = 'Prida 0.7.0-dev'
PRIDA_VERSION_EXPL = PRIDA_VERSION + ' - Explorer Mode'

#PAGE NUMBER ENUMS FOR mainwindow.ui
MAIN_MENU = 0
VIEWER = 1
INPUT_EXP = 2
RUN_EXP = 3
FILE_SEARCH = 4

#PAGE NUMBER ENUMS FOR SHEET/IDA
SHEET_VIEW = 0
IDA_VIEW = 1

# FILE & SESSION PATHS FOR IMAGES AND HDF FILE
filepath = ''
session_path = ''
is_path = False


###############################################################################
## FUNCTIONS:
###############################################################################
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = sys.path[0]

    return os.path.join(base_path, relative_path)


###############################################################################
## CLASSES:
###############################################################################
class HardwareThread(QThread):
    """
    Name:     HardwareThread
    Features: Thread for running hardware
    History:  Version 0.7.0-dev
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def run(self):
        """
        Name:     HardwareThread.run
        Inputs:   None.
        Outputs:  None.
        Features: Run sequence and save image to HDF file
        """
        global filepath, is_path
        if hardware_mode:
            output_args = imaging.run_sequence()
            is_path = output_args[0]
            filepath = output_args[1]
            datestamp = output_args[2]
            datestamp = os.path.basename(datestamp)
            timestamp = output_args[3]
            anglestamp = output_args[4]
            anglestamp = os.path.splitext(anglestamp)[0]
            basename = os.path.basename(filepath)
            name = os.path.splitext(basename)[0]
            attr_path = os.path.join(session_path, name, basename)
            if is_path:
                hdf.save_image(session_path, filepath)
                hdf.set_attr('Date', datestamp, attr_path)
                hdf.set_attr('Time', timestamp, attr_path)
                hdf.set_attr('Angle', anglestamp, attr_path)


class Prida(QApplication):
    """
    Name:      Prida
    Features:  Prida main class
    History:   Version 0.7.0-dev
               - added new meta data field getters for INPUT_EXP [15.09.24]
               - set default field values in INPUT_EXP [15.09.24]
               - added QtCore.QDate for setting session date [15.09.24]
               - set session user and addr for new/open HDF5 file [15.09.24]
               - moved clear session & image sheets to back-to-menu [15.09.24]
               - fixed merge error with hardware thread spawning [15.09.29]
               - added additional fields to session sheet model [15.09.30]
               - abstracted pid_attrs and session_attrs [15.09.30]
               - updated new_session using new dictionaries [15.09.30]
               - added sheet_items variable [15.09.30]
               - updated get_data to search over known keys [15.10.01]
               - solved problem with missing meta data displaying [15.10.01]
               - created a set-defaults function [15.10.01]
               - added check for reseting IDA view for new/open file [15.10.01]
               - pushButton renamed menuButton (mainwindow.ui) [15.10.13]
               - added about ui [15.10.13]
               - created about function [15.10.14]
               - created export function [15.10.14]
               - added button disabling for hardware mode [15.10.15]
               - created FILE_SEARCH view [15.10.15]
               - created file_browse_sheet [15.10.15]
               - created file_search_sheet [15.10.15]
               - created add_search_file & rm_search_file functions [15.10.15]
               - linked FILE_SEARCH explorer button [15.10.16]
               - created import_data function [15.10.16]
               - created import_session function [15.10.16]
               - added error handling on exif tag import [15.10.19]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    pid_attrs = {
        1: {'title': 'Genus Species', 'key': 'gen_sp',
            'qt_val': 'c_gen_sp', 'qt_get': 'currentText()',
            'has_default': False},
        2: {'title': 'Line', 'key': 'line',
            'qt_val': 'c_line', 'qt_get': 'text()',
            'has_default': False},
        3: {'title': 'Rep Num', 'key': 'rep_num',
            'qt_val': 'c_rep_num', 'qt_get': 'text()',
            'has_default': False},
        4: {'title': 'Tub ID', 'key': 'tubid',
            'qt_val': 'c_tubid', 'qt_get': 'text()',
            'has_default': False},
        5: {'title': 'Germination Date', 'key': 'germdate',
            'qt_val': 'c_germdate', 'qt_get': 'text()',
            'has_default': False},
        6: {'title': 'Transplant Date', 'key': 'transdate',
            'qt_val': 'c_transdate', 'qt_get': 'text()',
            'has_default': False},
        7: {'title': 'Treatment', 'key': 'treatment',
            'qt_val': 'c_treatment', 'qt_get': 'text()',
            'has_default': True,
            'def_val': "'Control'", 'qt_set': 'setText'},
        8: {'title': 'Media', 'key': 'media',
            'qt_val': 'c_media', 'qt_get': 'text()',
            'has_default': False},
        9: {'title': 'Tub Size', 'key': 'tubsize',
            'qt_val': 'c_tubsize', 'qt_get': 'text()',
            'has_default': False},
        10: {'title': 'Nutrient', 'key': 'nutrient',
             'qt_val': 'c_nutrient', 'qt_get': 'text()',
             'has_default': False},
        11: {'title': 'Growth Temp (Day)', 'key': 'growth_temp_day',
             'qt_val': 'c_growth_temp_day', 'qt_get': 'text()',
             'has_default': False},
        12: {'title': 'Growth Temp (Night)', 'key': 'growth_temp_night',
             'qt_val': 'c_growth_temp_night', 'qt_get': 'text()',
             'has_default': False},
        13: {'title': 'Lighting Conditions', 'key': 'growth_light',
             'qt_val': 'c_growth_light', 'qt_get': 'text()',
             'has_default': False},
        14: {'title': 'Watering schedule', 'key': 'water_sched',
             'qt_val': 'c_water_sched', 'qt_get': 'text()',
             'has_default': False},
        15: {'title': 'Damage', 'key': 'damage',
             'qt_val': 'c_damage', 'qt_get': 'text()',
             'has_default': True,
             'def_val': "'No'", 'qt_set': 'setText'},
        16: {'title': 'Plant Notes', 'key': 'notes',
             'qt_val': 'c_plant_notes', 'qt_get': 'text()',
             'has_default': True, 'def_val': "'N/A'", 'qt_set': 'setText'}
    }
    sheet_items = len(pid_attrs.keys())
    #
    # Define the first number of attrs that display in session sheet (i.e., 14)
    session_sheet_items = 14
    session_attrs = {
        0: {'title': 'Session ID', 'key': 'number',
            'qt_val': 'c_session_number', 'qt_get': 'text()',
            'has_default': False},
        1: {'title': 'Session Date', 'key': 'date', 'qt_val': 'c_session_date',
            'qt_get': 'text()',
            'has_default': True,
            'def_val': 'QDate.currentDate()', 'qt_set': 'setDate'},
        2: {'title': 'Rig Name', 'key': 'rig',
            'qt_val': 'c_session_rig', 'qt_get': 'text()',
            'has_default': False},
        3: {'title': 'Image Count', 'key': 'num_img',
            'qt_val': 'c_num_images', 'qt_get': 'text()',
            'has_default': False},
        4: {'title': 'Plant Age', 'key': 'age_num',
            'qt_val': 'c_plant_age', 'qt_get': 'text()',
            'has_default': False},
        5: {'title': 'Notes', 'key': 'notes',
            'qt_val': 'c_session_notes',  'qt_get': 'text()',
            'has_default': True,
            'def_val': "'N/A'", 'qt_set': 'setText'},
        6: {'title': 'Camera Make', 'key': 'cam_make',
            'qt_val': 'c_cam_make', 'qt_get': 'text()',
            'has_default': True,
            'def_val': 'imaging.camera.make', 'qt_set': 'setText'},
        7: {'title': 'Camera Model', 'key': 'cam_model',
            'qt_val': 'c_cam_model', 'qt_get': 'text()',
            'has_default': True,
             'def_val': 'imaging.camera.model', 'qt_set': 'setText'},
        8: {'title': 'Exposure Time', 'key': 'cam_shutter',
            'qt_val': 'c_cam_shutter', 'qt_get': 'text()',
            'has_default': True,
            'def_val': 'imaging.camera.exposure_time', 'qt_set': 'setText'},
        9: {'title': 'Aperture', 'key': 'cam_aperture',
            'qt_val': 'c_cam_aperture', 'qt_get': 'text()',
            'has_default': True,
            'def_val': 'imaging.camera.aperture', 'qt_set': 'setText'},
        10: {'title': 'ISO', 'key': 'cam_exposure',
             'qt_val': 'c_cam_exposure', 'qt_get': 'text()',
             'has_default': True,
             'def_val': 'imaging.camera.iso_speed', 'qt_set': 'setText'},
        11: {'title': 'Orientation', 'key': 'cam_orientation',
             'qt_val': 'c_cam_orientation', 'qt_get': 'text()',
             'has_default': True,
             'def_val': 'imaging.camera.orientation', 'qt_set': 'setText'},
        12: {'title': 'Height', 'key': 'cam_height',
             'qt_val': 'c_cam_height', 'qt_get': 'text()',
             'has_default': True,
             'def_val': 'imaging.camera.image_height', 'qt_set': 'setText'},
        13: {'title': 'Width', 'key': 'cam_width',
             'qt_val': 'c_cam_width', 'qt_get': 'text()',
             'has_default': True,
             'def_val': 'imaging.camera.image_width', 'qt_set': 'setText'},
        14: {'title':  'Session User', 'key': 'user',
             'qt_val': 'c_session_user', 'qt_get': 'text()',
             'has_default': False},
        15: {'title': 'Session Email', 'key': 'addr',
             'qt_val': 'c_session_addr', 'qt_get': 'text()',
             'has_default': False}
    }

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     Prida.__init__
        Inputs:   None.
        Outputs:  None.
        Depends:  - load_session_sheet_headers     - new_hdf
                  - new_session                    - open_hdf
                  - resize_sheet                   - show_ida
                  - to_sheet                       - update_progress
                  - update_session_sheet_model     - update_sheet_model
                  - set_defaults                   - about
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # Check that the necessary files exist:
        # NOTE: mainwindow.ui depends on class definitions from custom.py
        self.main_ui = resource_path('mainwindow.ui')
        self.user_ui = resource_path('input_user.ui')
        self.about_ui = resource_path('about.ui')
        self.gs_dict = resource_path('dictionary.txt')
        self.greeter = resource_path('greeter.jpg')

        if not os.path.isfile(self.main_ui) or not os.path.isfile(self.user_ui):
            raise IOError("Error! Missing required ui files!")
        if not os.path.isfile(self.gs_dict):
            raise IOError("Error! Missing dictionary file!")
        if not os.path.isfile(self.greeter):
            raise IOError("Error! Missing welcome screen image!")

        # Load main window GUI to the QMainWindow:
        self.view = uic.loadUi(self.main_ui, self.base)

        # Set the greeter image:
        self.view.welcome.setPixmap(QPixmap(self.greeter).scaledToHeight(300))

        # Create the VIEWER sheet model (where PIDs and sessions are listed)
        self.sheet_model = QStandardItemModel()
        self.view.sheet.setModel(self.sheet_model)
        self.view.sheet.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # Maximize/minimize window functionality (?)
        self.view.sheet.collapsed.connect(lambda: self.resize_sheet())
        self.view.sheet.expanded.connect(lambda: self.resize_sheet())
        self.view.sheet.setAnimated(True)

        # Create the VIEWER session sheet model (for session properties)
        self.session_sheet_model = QStandardItemModel()
        self.view.session_sheet.setModel(self.session_sheet_model)
        self.view.session_sheet.setRootIsDecorated(False)
        self.load_session_sheet_headers()

        # Create FILE_SEARCH sheet model (where files + PIDs are listed)
        self.search_sheet_model = QStandardItemModel()
        self.view.file_search_sheet.setModel(self.search_sheet_model)
        self.load_file_search_sheet_headers()

        # Create the FILE_SEARCH file browse model (for displaying files)
        self.file_sheet_model = QStandardItemModel()
        self.view.file_browse_sheet.setModel(self.file_sheet_model)
        self.load_file_browse_sheet_headers()

        # Populate INPUT_EXP Genus Species dropdown menu:
        for line in open(self.gs_dict).read().splitlines():
            self.view.c_gen_sp.addItem(line)

        # Set default values in INPUT_EXP:
        # NOTE: this may be moved to new file or open file functions
        self.set_defaults()

        if hardware_mode:
            # Hardware Mode Decorator:
            self.base.setWindowTitle(PRIDA_VERSION)

            # Spawn a hardware thread if mode is enabled:
            self.hardware_thread = HardwareThread()
            self.hardware_thread.finished.connect(self.update_progress)

            # Disable "Import Data" button (only allow "Create Session")
            # and set VIEWER action for "OK" and "Create Session" buttons:
            self.view.import_data.setEnabled(False)
            self.view.c_buttons.accepted.connect(self.new_session)
            self.view.create_session.clicked.connect(
                lambda: self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
            )
        else:
            # Explorer Mode Decorator:
            self.base.setWindowTitle(PRIDA_VERSION_EXPL)

            # Disable "Create Session" button, set VIEWER action for
            # "Import Data" button & INPUT_EXP action for "OK" button:
            self.view.create_session.setEnabled(False)
            self.view.import_data.clicked.connect(self.import_data)
            self.view.c_buttons.accepted.connect(self.import_session)

        # Set MAIN_MENU actions for "New," "Open," & "Search" buttons:
        self.view.new_hdf.clicked.connect(self.new_hdf)
        self.view.open_hdf.clicked.connect(self.open_hdf)
        self.view.search_hdf.clicked.connect(
            lambda: self.view.stackedWidget.setCurrentIndex(FILE_SEARCH)
        )

        # Set VIEWER actions for "Export Data," "Main Menu," "About," and
        # "Back to Spreadsheet" (IDA) buttons and for edited search text;
        # also disable "Perform Analysis" button until further notice:
        self.view.export_data.clicked.connect(self.export)
        self.view.menuButton.clicked.connect(self.back_to_menu)
        self.view.aboutButton.clicked.connect(self.about)
        self.view.to_sheet.clicked.connect(self.to_sheet)
        self.view.search.textEdited.connect(self.update_sheet_model)
        self.view.perform_analysis.setEnabled(False)

        # Set INPUT_EXP action for "Cancel" button:
        self.view.c_buttons.rejected.connect(
            lambda: self.view.stackedWidget.setCurrentIndex(VIEWER)
        )

        # Create VIEWER image sheet model (for thumbnail previewer):
        self.img_list = []
        self.img_sheet_model = QStandardItemModel()
        self.view.img_select.setModel(self.img_sheet_model)
        self.view.img_select.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.img_select.setIconSize(QSize(100, 100))

        # Create selection models for VIEWER sheet and img_sheet and for
        # FILE_SELECT file_browser_sheet:
        self.sheet_select_model = self.view.sheet.selectionModel()
        self.img_sheet_select_model = self.view.img_select.selectionModel()
        self.file_select_model = self.view.file_browse_sheet.selectionModel()

        # Set selection model actions for selection changes:
        self.img_sheet_select_model.selectionChanged.connect(self.show_ida)
        self.sheet_select_model.selectionChanged.connect(
            self.update_session_sheet_model
        )

        # Set FILE_SEARCH actions for "Main Menu," "+/-," and "Explore" buttons
        # and action for when search text is edited:
        self.view.menuButton2.clicked.connect(self.back_to_menu)
        self.view.add_file.clicked.connect(self.add_search_file)
        self.view.rm_file.clicked.connect(self.rm_search_file)
        self.view.explore_file.clicked.connect(self.explore_file)
        self.view.f_search.textEdited.connect(self.update_file_search_sheet)

        # Initialize the search dictionaries, list of PID metadata and
        # current image (not currently used):
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.current_img = None

        self.base.show()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def about(self):
        """
        Name:     Prida.about
        Inputs:   None.
        Outputs:  None.
        Features: Produces the file popup dialog box
        """
        popup = uic.loadUi(self.about_ui)
        overview = hdf.get_about()

        # Set popup values to default string if get_about fails:
        popup.author_about.setText(overview.get('Author', 'Unknown'))
        popup.contact_about.setText(overview.get('Contact', 'Unknown'))
        popup.plants_about.setText(overview.get('Plants', '?'))
        popup.sessions_about.setText(overview.get('Sessions', '?'))
        popup.photos_about.setText(overview.get('Photos', '?'))
        popup.summary_about.setText(overview.get('Summary', '?'))
        if popup.exec_():
            popup.repaint()

    def add_search_file(self):
        """
        Name:     Prida.add_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Adds user-defined HDF5 file to search files dictionary and
                  saves the file's PID attributes to search data dictionary
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        path = QFileDialog.getOpenFileName(filter='*.hdf5')[0]
        if path != '':
            f_name = os.path.basename(path)
            f_name = os.path.splitext(f_name)[0]
            f_path = os.path.dirname(path)
            if f_name not in self.search_files:
                # Add file to file_sheet_model:
                self.search_files[f_name] = f_path
                f_list = [QStandardItem(f_name), QStandardItem(f_path)]
                self.file_sheet_model.appendRow(f_list)

                # Add file's data to search_sheet_model:
                if hdf.isopen:
                    hdf.close()
                hdf.open_file(path)
                for pid in hdf.list_pids():
                    my_key = "%s.%s" % (f_name, pid)
                    my_data = [f_name, pid]
                    for i in self.pid_attrs.keys():
                        my_attr = self.pid_attrs[i]['key']
                        my_attr_val = hdf.get_attr(my_attr,
                                                   os.path.join('/', str(pid)))
                        my_data.append(my_attr_val)
                    self.search_data[my_key] = my_data
                hdf.close()
            self.update_file_search_sheet()
            self.update_file_browse_sheet()

    def back_to_menu(self):
        """
        Name:     Prida.back_to_menu
        Inputs:   None
        Outputs:  None
        Features: Returns to the menu screen, closing the HDF5 file handle and
                  clearing session and sheet data.
        Depends:  - clear_session_sheet_model
                  - update_file_browse_sheet
                  - update_file_search_sheet
                  - update_sheet_model
        """
        hdf.close()
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.update_sheet_model()
        self.update_file_browse_sheet()
        self.update_file_search_sheet()
        self.view.stackedWidget.setCurrentIndex(MAIN_MENU)

    def clear_session_sheet_model(self):
        """
        Name:     Prida.clear_session_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Clears session sheet model
        Depends:  resize_session_sheet
        """
        for i in range(self.session_sheet_items):
            self.session_sheet_model.setItem(i, 1, QStandardItem(''))
        #
        self.resize_session_sheet()

    def explore_file(self):
        """
        Name:     Prida.explore_file
        Inputs:   None.
        Outputs:  None.
        Features: Opens the sheet VIEW with the currently selected file from
                  the search file browser
        Depends:  - get_data
                  - update_sheet_model
        """
        if self.file_select_model.hasSelection():
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0]
            )
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)
            if hdf.isopen:
                hdf.close()
            hdf.open_file(f_path)
            self.get_data()
            self.update_sheet_model()
            if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                    self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
            self.view.stackedWidget.setCurrentIndex(VIEWER)

            # Set session user and contact defaults:
            self.view.c_session_user.setText(str(hdf.get_root_user()))
            self.view.c_session_addr.setText(str(hdf.get_root_addr()))

    def export(self):
        """
        Name:     Prida.export
        Inputs:   None.
        Outputs:  None.
        Features: Exports the images from a given selection to a user-defined
                  output directory recreating the HDF5 folder structure, else
                  exports all images and meta data
        """
        if self.sheet_select_model.hasSelection():
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                pid = my_selection.parent().text()
                session = my_selection.text()
                s_path = "/%s/%s" % (pid, session)
            else:
                pid = my_selection.text()
                s_path = "/%s" % (pid)
        else:
            s_path = '/'

        path = QFileDialog.getExistingDirectory(None,
                                                'Select an output directory:',
                                                os.path.expanduser("~"),
                                                QFileDialog.ShowDirsOnly)
        if os.path.isdir(path):
            try:
                hdf.extract_attrs(path)
            except IOError as e:
                attr_error_msg = "I/O Error(%s): %s" % (e.errno, e.strerror)
                print(attr_error_msg)
                #error_dialog = QErrorMessage()
                #error_dialog.showMessage(attr_error_msg)

            try:
                hdf.extract_datasets(s_path, path)
            except IOError as e:
                dset_error_msg = "I/O Error(%s): %s" % (e.errno, e.strerror)
                print(dset_error_msg)
                #error_dialog = QErrorMessage()
                #error_dialog.showMessage(dset_error_msg)

    def get_data(self):
        """
        Name:     Prida.get_data
        Inputs:   None.
        Outputs:  None.
        Features: Creates a list of attribute dictionaries for each PID in
                  the HDF5 file.
        """
        self.data_list = []
        for pid in hdf.list_pids():
            d = {}
            d['pid'] = pid
            for i in self.pid_attrs.keys():
                my_attr = self.pid_attrs[i]['key']
                d[my_attr] = hdf.get_attr(my_attr, os.path.join('/', str(pid)))
            self.data_list.append(d)

    def import_data(self):
        """
        Name:     Prida.import_data
        Inputs:   None.
        Outputs:  None.
        Features: Reads a user-defined directory for images, attempts to extract
                  image exif tags and defines them in the INPUT_EXP view
        """
        path = QFileDialog.getExistingDirectory(
            None,
            'Select directory containing images:',
            os.path.expanduser("~"),
            QFileDialog.ShowDirsOnly
        )
        if os.path.isdir(path):
            # Currently filters search for only jpg and tif image files
            s_path = os.path.join(path, '*[(.jpg)(.tif)]')
            my_files = glob.glob(s_path)
            files_found = len(my_files)
            print("Importing %d files" % (files_found))
            if files_found > 0:
                # Save image names for processing:
                self.search_files = {}
                for my_file in my_files:
                    self.search_files[my_file] = 0
                    img = Image.open(my_file)
                    try:
                        exif = { ExifTags.TAGS[k]: v for k, v in img._getexif().items() if k in ExifTags.TAGS }
                    except AttributeError:
                        exif = {}
                    finally:
                        im_make = str(exif.get('Make', '<Undefined>'))
                        im_model = str(exif.get('Model', '<Undefined>'))
                        im_speed = str(exif.get('ShutterSpeedValue', '<Undefined>'))
                        im_aper = str(exif.get('ApertureValue', '<Undefined>'))

                    # In Python 2 these are tuples, in Python 3 they are not:
                    try:
                        im_height = str(
                            exif.get('ExifImageHeight', ('<Undefined>',))[0]
                        )
                    except TypeError:
                        im_height = str(
                            exif.get('ExifImageHeight', '<Undefined>')
                        )
                    try:
                        im_width = str(
                            exif.get('ExifImageWidth', ('<Undefined>',))[0]
                        )
                    except TypeError:
                        im_width = str(
                            exif.get('ExifImageWidth', '<Undefined>')
                        )
                    try:
                        im_iso = str(
                            exif.get('ISOSpeedRatings', ('<Undefined>',))[0]
                        )
                    except TypeError:
                        im_iso = str(
                            exif.get('ISOSpeedRatings', '<Undefined>')
                        )
                    try:
                        im_orient = str(
                            exif.get('Orientation', ('<Undefined>',))[0]
                        )
                    except TypeError:
                        im_orient = str(exif.get('Orientation', '<Undefined>'))
                    try:
                        im_datetime = str(
                            exif.get('DateTimeOriginal', ('<Undefined>',))[0]
                        )
                    except TypeError:
                        im_datetime = str(
                            exif.get('DateTimeOriginal', '<Undefined>')
                        )
                    try:
                        im_date = im_datetime.split(' ')[0]
                        im_qdate = QDate.fromString(im_date, 'yyyy:MM:dd')
                    except:
                        im_qdate = QDate.currentDate()

                # Save defaults for INPUT_EXP:
                self.view.c_num_images.setText(str(files_found))
                self.view.c_cam_make.setText(im_make)
                self.view.c_cam_model.setText(im_model)
                self.view.c_cam_width.setText(im_width)
                self.view.c_cam_height.setText(im_height)
                self.view.c_cam_orientation.setText(im_orient)
                self.view.c_session_date.setDate(im_qdate)
                self.view.c_cam_exposure.setText(im_iso)
                self.view.c_cam_shutter.setText(im_speed)
                self.view.c_cam_aperture.setText(im_aper)
                self.view.stackedWidget.setCurrentIndex(INPUT_EXP)

    def import_session(self):
        """
        Name:     Prida.import_session
        Inputs:   None.
        Outputs:  None.
        Features: Creates a new session based on imported data
        Depends:  - get_data
                  - update_sheet_model
        """
        pid_dict = {}
        for i in self.pid_attrs.keys():
            exec("pid_dict['%s'] = str(self.view.%s.%s)" % (
                self.pid_attrs[i]['key'],
                self.pid_attrs[i]['qt_val'],
                self.pid_attrs[i]['qt_get']
                )
            )
        session_dict = {}
        for j in self.session_attrs.keys():
            exec("session_dict['%s'] = str(self.view.%s.%s)" % (
                self.session_attrs[j]['key'],
                self.session_attrs[j]['qt_val'],
                self.session_attrs[j]['qt_get']
                )
            )
        s_path = hdf.create_session(str(self.view.c_pid.text()),
                                    pid_dict,
                                    session_dict)
        # @TODO: progress bar?
        for filepath in sorted(list(self.search_files.keys())):
            hdf.save_image(s_path, filepath)

        # Return to sheet:
        self.get_data()
        self.update_sheet_model()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def load_file_browse_sheet_headers(self):
        """
        Name:     Prida.load_file_browse_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the file_sheet_model
        Depends:  resize_file_browse_sheet
        """
        #
        self.file_sheet_model.setColumnCount(2)
        self.file_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File Name')
        )
        self.file_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Path')
        )
        self.resize_file_browse_sheet()

    def load_file_search_sheet_headers(self):
        """
        Name:     Prida.load_file_search_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the search_sheet_model
        Depends:  resize_search_sheet
        """
        self.search_sheet_model.setColumnCount(self.sheet_items + 1)
        self.search_sheet_model.setHorizontalHeaderItem(0, QStandardItem('File'))
        self.search_sheet_model.setHorizontalHeaderItem(1, QStandardItem('PID'))
        for i in self.pid_attrs:
            eval("%s(%d, QStandardItem('%s'))" % (
                "self.search_sheet_model.setHorizontalHeaderItem", (i + 1),
                self.pid_attrs[i]['title']
                )
            )
        self.resize_search_sheet()

    def load_session_sheet_headers(self):
        """
        Name:     Prida.load_session_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the session_sheet_model
        Depends:  resize_session_sheet
        """
        # Define session sheet column headers:
        self.session_sheet_model.setColumnCount(2)
        self.session_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('Property')
        )
        self.session_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Value')
        )
        #
        # Define session sheet rows in column 1:
        for i in range(self.session_sheet_items):
            eval("%s(%d, 0, QStandardItem('%s'))" % (
                "self.session_sheet_model.setItem", i,
                self.session_attrs[i]['title']
                )
            )
        self.resize_session_sheet()

    def load_sheet_headers(self):
        """
        Name:     Prida.load_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the sheet_model
        Depends:  resize_sheet
        """
        self.sheet_model.setColumnCount(self.sheet_items)
        self.sheet_model.setHorizontalHeaderItem(0, QStandardItem('PID'))
        for i in self.pid_attrs.keys():
            eval("%s(%d, QStandardItem('%s'))" % (
                "self.sheet_model.setHorizontalHeaderItem", i,
                self.pid_attrs[i]['title']
                )
            )
        self.resize_sheet()

    def new_hdf(self):
        """
        Name:     Prida.new_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Gets user information, gets HDF5 file name, creates a new
                  HDF5 file, stores user information, and goes to VIEWER page
        Depends:  - get_data
                  - update_sheet_model
        """
        popup = uic.loadUi(self.user_ui)
        if popup.exec_():
            path = QFileDialog.getSaveFileName()[0]
            if path != '':
                if path[-5:] != '.hdf5':
                    path += '.hdf5'
                hdf.open_file(path)
                sess_user = str(popup.user.text())
                sess_addr = str(popup.email.text())
                sess_about = str(popup.about.toPlainText())
                hdf.set_root_user(sess_user)
                hdf.set_root_addr(sess_addr)
                hdf.set_root_about(sess_about)
                self.get_data()
                self.update_sheet_model()
                if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                    self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
                self.view.stackedWidget.setCurrentIndex(VIEWER)

                # Set session user and contact default values:
                self.view.c_session_user.setText(sess_user)
                self.view.c_session_addr.setText(sess_addr)

    def new_session(self):
        """
        Name:     Prida.new_session
        Inputs:   None.
        Outputs:  None
        Features: Creates HDF5 session, resets the progress bar, enters the
                  RUN_EXP screen, and begins a session
        """
        global session_path
        if hardware_mode:
            pid_dict = {}
            for i in self.pid_attrs.keys():
                exec("pid_dict['%s'] = str(self.view.%s.%s)" % (
                    self.pid_attrs[i]['key'],
                    self.pid_attrs[i]['qt_val'],
                    self.pid_attrs[i]['qt_get']
                    )
                )
            session_dict = {}
            for j in self.session_attrs.keys():
                exec("session_dict['%s'] = str(self.view.%s.%s)" % (
                    self.session_attrs[j]['key'],
                    self.session_attrs[j]['qt_val'],
                    self.session_attrs[j]['qt_get']
                    )
                )
            session_path = hdf.create_session(str(self.view.c_pid.text()),
                                              pid_dict,
                                              session_dict)
            imaging.num_photos = int(self.view.c_num_images.text())
            self.view.c_progress.setMinimum(0)
            self.view.c_progress.setMaximum(int(imaging.ms_end_pos))
            self.view.stackedWidget.setCurrentIndex(RUN_EXP)
            self.hardware_thread.start(QThread.TimeCriticalPriority)

    def open_hdf(self):
        """
        Name:     Prida.open_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Opens an existing HDF5 file and goes to VIEWER page
        Depends:  - get_data
                  - update_sheet_model
        """
        path = QFileDialog.getOpenFileName(filter='*.hdf5')[0]
        if os.path.isfile(path):
            hdf.open_file(path)
            self.get_data()
            self.update_sheet_model()
            if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                    self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
            self.view.stackedWidget.setCurrentIndex(VIEWER)

            # Set session user and contact defaults:
            self.view.c_session_user.setText(str(hdf.get_root_user()))
            self.view.c_session_addr.setText(str(hdf.get_root_addr()))

    def resize_file_browse_sheet(self):
        """
        Name:     Prida.resize_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_browse_sheet
        """
        for col in range(2):
            self.view.file_browse_sheet.resizeColumnToContents(col)

    def resize_search_sheet(self):
        """
        Name:     Prida.resize_search_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_search_sheet
        """
        for col in range(self.sheet_items + 2):
            self.view.file_search_sheet.resizeColumnToContents(col)

    def resize_session_sheet(self):
        """
        Name:     Prida.resize_session_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the session_sheet
        """
        for col in range(2):
            self.view.session_sheet.resizeColumnToContents(col)

    def resize_sheet(self):
        """
        Name:     Prida.resize_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the sheet
        """
        for col in range(self.sheet_items + 1):
            self.view.sheet.resizeColumnToContents(col)

    def rm_search_file(self):
        """
        Name:     Prida.rm_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Removes a file from the file browse view and its
                  associated PID rows from the file search view
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        if self.file_select_model.hasSelection():
            # Get the selection and put together the file name and path:
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0]
            )
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            # Remove file from search_files:
            del self.search_files[s_name]

            # Remove file's PIDs from file_search_sheet:
            if hdf.isopen:
                hdf.close()
            hdf.open_file(f_path)
            for pid in hdf.list_pids():
                my_key = "%s.%s" % (s_name, pid)
                if my_key in self.search_data:
                    del self.search_data[my_key]
            hdf.close()
            self.update_file_browse_sheet()
            self.update_file_search_sheet()
        else:
            print('Nothing selected!')

    def search(self, pid_meta, terms):
        """
        Name:     Prida.search
        Inputs:   - list, PID meta data values (pid_meta)
                  - list, search terms (terms)
        Outputs:  bool (found)
        Features: Searches the pid metafields and returns true if all terms are
                  found anywhere within the fields
        """
        found = True
        for term in terms:
            found = found and any(term in meta_field for meta_field in pid_meta)
        return found

    def set_defaults(self):
        """
        Name:     Prida.set_defaults
        Inputs:   None.
        Outputs:  None.
        Features: Sets GUI text default values
        """
        for i in self.pid_attrs.keys():
            if self.pid_attrs[i]['has_default']:
                eval("self.view.%s.%s(%s)" % (
                    self.pid_attrs[i]['qt_val'],
                    self.pid_attrs[i]['qt_set'],
                    self.pid_attrs[i]['def_val']
                    )
                )
        for j in self.session_attrs.keys():
            if self.session_attrs[j]['has_default']:
                try:
                    eval("self.view.%s.%s(%s)" % (
                        self.session_attrs[j]['qt_val'],
                        self.session_attrs[j]['qt_set'],
                        self.session_attrs[j]['def_val']
                        )
                    )
                except NameError:
                    # E.g., imaging not named if not hardware_mode
                    print("Warning! Setting Preview Mode default value.")
                    eval("self.view.%s.%s(%s)" % (
                        self.session_attrs[j]['qt_val'],
                        self.session_attrs[j]['qt_set'],
                        '"<Undefined>"'
                        )
                    )
                except:
                    print("Warning! Setting unknown parameter default value.")
                    eval("self.view.%s.%s(%s)" % (
                        self.session_attrs[j]['qt_val'],
                        self.session_attrs[j]['qt_set'],
                        '"<Unset>"'
                        )
                    )

    def show_ida(self, selection):
        """
        Name:     Prida.show_ida
        Input:    obj (selection)
        Output:   None.
        Features: Switches to IDA page and sets IDA base image
        """
        print(selection.indexes())
        if selection.indexes():
            self.view.stacked_sheets.setCurrentIndex(IDA_VIEW)
            img_path = self.img_sheet_model.itemFromIndex(selection.indexes()[0]).data()
            ndarray_full = hdf.get_dataset(img_path)
            self.view.ida.setBaseImage(ndarray_full)

    def to_sheet(self):
        """
        Name:     Prida.to_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Switches from IDA to sheet view
        """
        self.view.img_select.clearSelection()
        self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)

    def update_file_browse_sheet(self):
        """
        Name:     Prida.update_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Reloads the rows in the file_sheet_model
        Depends:  - load_file_browse_sheet_headers
                  - resize_file_browse_sheet
        """
        self.file_sheet_model.clear()
        self.load_file_browse_sheet_headers()
        for f_name in self.search_files:
            f_path = self.search_files[f_name]
            f_list = [QStandardItem(f_name), QStandardItem(f_path)]
            self.file_sheet_model.appendRow(f_list)
        self.resize_file_browse_sheet()

    def update_file_search_sheet(self, search=''):
        """
        Name:     Prida.update_file_search_sheet
        Inputs:   [optional] str, search string (search)
        Outputs:  None.
        Features: Reloads the rows in the search_sheet_model, filtered for
                  search terms
        Depends:  - load_file_search_sheet_headers
                  - resize_search_sheet
                  - search
        """
        self.search_sheet_model.clear()
        self.load_file_search_sheet_headers()
        terms = search.lower().split()
        for i in self.search_data:
            pid_meta = []
            for j in self.search_data[i]:
                pid_meta.append(j.lower())
            if self.search(pid_meta, terms):
                d_list = []
                for k in self.search_data[i]:
                    d_list.append(QStandardItem(k))
                self.search_sheet_model.appendRow(d_list)
        self.resize_search_sheet()

    def update_progress(self):
        """
        Name:     Prida.update_progress
        Inputs:   None.
        Outputs:  None.
        Features: Updates the progress bar, sets captured image to preview, and
                  decides the completion status of the session
        Depends:  - clear_session_sheet_model
                  - get_data
                  - update_sheet_model
        """
        self.view.c_progress.setValue(imaging.ms_count)
        if is_path:
            image = QPixmap(filepath)
            self.view.c_preview.setPixmap(image.scaledToHeight(300))
        if not imaging.is_running:
            imaging.reset_count()
            self.get_data()
            self.update_sheet_model()
            self.clear_session_sheet_model()
            self.view.stackedWidget.setCurrentIndex(VIEWER)
        else:
            self.hardware_thread.start(QThread.TimeCriticalPriority)

    def update_session_sheet_model(self, selection):
        """
        Name:     Prida.update_session_sheet_model
        Inputs:   obj (selection)
        Outputs:  None.
        Features: Updates session sheet model with selected session's data,
                  creates the thumbnails and tags the full image paths, and
                  clears session sheet model if no sessions are selected
        Depends:  - clear_session_sheet_model
                  - resize_session_sheet
        """
        my_session = self.sheet_model.itemFromIndex(selection.indexes()[0])
        if (my_session.parent() is not None):
            pid = my_session.parent().text()
            session = my_session.text()
            s_path = "/%s/%s" % (pid, session)
            for i in range(self.session_sheet_items):
                eval("%s(%d, 1, QStandardItem('%s'))" % (
                    "self.session_sheet_model.setItem", i,
                    hdf.get_attr(self.session_attrs[i]['key'], s_path)
                    )
                )
            self.img_sheet_model.clear()
            self.img_list = []
            for obj in hdf.list_objects(s_path):
                thumb_path = "%s/%s/%s.thumb" % (s_path, obj, obj)
                image_path = "%s/%s/%s.jpg" % (s_path, obj, obj)
                ndarray = hdf.get_dataset(thumb_path)
                image = Image.fromarray(ndarray)
                qt_image = ImageQt.ImageQt(image)
                self.img_list.append(qt_image)
                pix = QPixmap.fromImage(qt_image)
                item = QStandardItem(obj)
                item.setIcon(QIcon(pix))
                item.setData(image_path)
                self.img_sheet_model.appendRow(item)
        else:
            self.clear_session_sheet_model()
            self.img_sheet_model.clear()
            self.img_list = []
        self.resize_session_sheet()

    def update_sheet_model(self, search=''):
        """
        Name:     Prida.update_sheet_model
        Inputs:   [optional] str (search)
        Features: Displays all PIDs in the sheet or the PIDs that match the
                  search terms if search terms are given
        Depends:  - load_sheet_headers
                  - resize_sheet
                  - search
        """
        self.sheet_model.clear()
        self.load_sheet_headers()
        terms = search.lower().split()
        for data in self.data_list:
            pid_meta = []
            for key in data:
                pid_meta.append(data[key].lower())
            if self.search(pid_meta, terms):
                pid_item = QStandardItem(data['pid'])

                # Create session rows under each PID:
                for session in hdf.list_sessions(data['pid']):
                    pid_item.appendRow(QStandardItem(session))

                # Fill the PID row with its attributes:
                pid_list = [pid_item,]
                for i in sorted(self.pid_attrs.keys()):
                    my_attr = data[self.pid_attrs[i]['key']]
                    pid_list.append(QStandardItem(my_attr))
                self.sheet_model.appendRow(pid_list)
        self.resize_sheet()

###############################################################################
## MAIN:
###############################################################################
if __name__ == '__main__':
    hdf = PridaHDF()
    if hardware_mode:
        imaging = Imaging()
    app = Prida()
    app.exec_()
    if hardware_mode:
        app.hardware_thread.wait()
    hdf.close()
    sys.exit()
