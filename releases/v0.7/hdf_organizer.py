#!/usr/bin/python
#
# hdf_organizer.py
#
# VERSION: 0.7.0-dev
#
# LAST EDIT: 2015-10-21
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# ------------
# description:
# ------------
# This script sets up the HDF5 (Hierarchical Data Formal) files for storing
# plant root images and analysis (a part of the PRIDA Project).
#
# The general workflow follows this pattern:
#   1. Create a class instance:
#      ``````````````````````
#      my_class = PridaHDF()
#      ,,,,,,,,,,,,,,,,,,,,,,
#
#   2. Create new / open existing an HDF5 file:
#      a. Create new file:
#         ``````````````````````````````````````````````
#         my_class.new_file(str directory, str filename)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Open existing file:
#         ````````````````````````````````
#         my_class.open_file(str filename)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   3. List PIDs:
#      a. abstracted:
#         ````````````````````
#         my_class.list_pids()
#         ,,,,,,,,,,,,,,,,,,,,
#
#      b. explicit:
#         ``````````````````````````
#         my_class.list_objects('/')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   4. User's details:
#      a. Save HDF5 file details:
#         ````````````````````````````
#         my_class.set_root_user(str)
#         my_class.set_root_addr(str)
#         my_class.set_root_about(str)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Check details:
#         `````````````````````````
#         my_class.get_root_user()
#         my_class.get_root_addr()
#         my_class.get_root_about()
#         ,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   5. Create a new session:
#      ````````````````````````````````````````````````````````````````````
#      session_path = my_class.create_session(str pid, dict pid, dict sess)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      where the PID meta data dictionary looks like this:
#      ````````````````````````````````````````````````````````````````````
#      dict = {
#          "gen_sp"            : str, Genus species
#          "line"              : str, cultivar/line
#          "media"             : str, growing media (e.g., hydroponic, gel)
#          "germdate"          : str, germination date (YYYY-MM-DD)
#          "transdate"         : str, transplant date (YYYY-MM-DD)
#          "rep_num"           : int, replication number
#          "treatment"         : str, treatment type
#          "tubsize"           : str, size of tub used for growing plant
#          "tubid"             : str, tub indentifier
#          "nutrient"          : str, nutrient solution info
#          "growth_temp_day"   : float, daytime growing temp., deg. C
#          "growth_temp_night" : float nighttime growing temp., deg. C
#          "growth_light"      : str, lighting conditions
#          "water_sched"       : str, watering schedule
#          "notes"             : str, additional notes
#      }
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#
#      and where the session meta data dictionary looks like this:
#      ```````````````````````````````````````````````````````````````
#      dict = {
#          "user"         : str, session user's name
#          "addr"         : str, session user's email address
#          "date"         : str, session date (YYYY-MM-DD)
#          "number"       : int, session number
#          "img_taken"    : int, total number of images taken
#          "age_num"      : float, plant age
#          "age_unit"     : str, unit of plant age (e.g., days, weeks)
#          "cam_shutter"  : str, camera shutter speed
#          "cam_aperture" : str, camera aperature setting
#          "cam_exposure" : str, camera exposure time
#          "notes"        : str, additional notes
#      }
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   6. Set existing PID attributes
#      NOTE: dictionary keys that already exist will have their values
#            re-written and keys that do not exist will be added to the
#            dictionary!
#      `````````````````````````````````````
#      my_class.set_pid_attrs(str pid, dict)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   7. Set existing session attributes
#      NOTE: dictionary keys that already exist will have their values
#            re-written and keys that do not exist will be added to the
#            dictionary!
#      ``````````````````````````````````````````````````
#      my_class.set_session_attrs(str session_path, dict)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   8. Retrieve PID attrs:
#      ````````````````````````
#      my_class.get_pid_attrs()
#      ,,,,,,,,,,,,,,,,,,,,,,,,
#
#   9. List PID sessions:
#      a. sorted:
#         `````````````````````````````````
#         my_class.list_sessions(str 'PID')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. unsorted:
#         `````````````````````````````````
#         my_class.list_objects(str 'PID')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#  10. Retrieve session attrs:
#      ````````````````````````````````````````````
#      my_class.get_session_attrs(str session_path)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#  11. Save/list/extract datasets:
#      a. Save image to session:
#         ```````````````````````````````````````````
#         my_class.save_image(str session, str image)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. List images in a session:
#         `````````````````````````````````````````
#         my_class.list_objects(str '/PID/Session')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      c. Extract images to directory:
#         ````````````````````````````````````````````````````````````````
#         my_class.extract_datasets(str session, str output, str extension)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      d. Extract PID and session attributes to CSV file:
#         ````````````````````````````````````````````
#         my_class.extract_attrs(str output_location)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#  12. Save file (flush data to disk):
#      ```````````````
#      my_class.save()
#      ,,,,,,,,,,,,,,,
#
#  13. Close file (flush data to disk and close file handle):
#      ````````````````
#      my_class.close()
#      ,,,,,,,,,,,,,,,,
#
#
# -----
# todo:
# -----
# 00. extract dataset level 0 should skip parent directory tree
# 01. Add compression, use gzip with shuffle=True and chucks=0.1*shape
#     also, try-catch image shape for monochrome images (img_to_hdf)
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import errno
import os
import re

import h5py
import numpy
import scipy.misc

try:
    import cv2
    import scipy.ndimage
    analysis_mode = True
except ImportError:
    print("Image analysis mode unavailable.")
    analysis_mode = False

###############################################################################
## CLASSES
###############################################################################
class PridaHDF:
    """
    Name:     PridaHDF
    Features: This class handles the organization and storage of plant root
              images and their associated meta data into a readable/writeable
              HDF5 (Hierarchical Data Format) file
    History:  VERSION 0.7.0-dev
              - Python 2/3 print syntax [15.09.22]
              - raise IOErrors for actual errors [15.09.22]
              - created analysis_mode [15.09.22]
              - replaced cv2 with scipy.misc [15.09.22]
              - replaced dict iteritems() with keys() function [15.09.22]
              - added check for strings in get attrs functions [15.09.22]
              - added typecasting of strings in set_attrs function [15.09.23]
              - added check in get_attr to decode unicode strings [15.09.24]
              - changed val to empty string if not found [15.09.30]
              - changed val to '<Undefined>' if attr not found [15.10.01]
              - updated "RGB" color index in dataset attr [15.10.01]
              - updated extract_datasets function name [15.10.01]
              - added check for numpy.ndarray in attrs [15.10.01]
                * dataset DIMENSION_LABELS are a numpy array
              - added find datasets function [15.10.06]
              - added new class variable datasets [15.10.06]
              - changed datasets to dict [15.10.09]
              - added find datasets to open file [15.10.13]
              - created get about function [15.10.14]
              - updated member_path in find & extract datasets [15.10.16]
              - reset datasets dict on close [15.10.16]
    Ref:      https://www.hdfgroup.org/HDF5/
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    UNDEFINED = "<Undefined>"

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     PridaHDF.__init__
        Features: Initialize empty class.
        Inputs:   None.
        Outputs:  None.
        """
        # Define class constants:
        self.isopen = False                # HDF5 file handler
        self.pids = {}                     # PIDs are keys w/ session counter
        self.datasets = {}                 # dict of all datasets (i.e., photos)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def close(self):
        """
        Name:     PridaHDF.close
        Features: Saves changes to disk and closes HDF5 file handle, no further
                  reading or writing can be done.
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile.close()
            except:
                pass # already closed
            finally:
                self.isopen = False
                self.pids = {}
                self.datasets = {}

    def create_session(self, pid, pid_attrs, session_attrs):
        """
        Name:     PridaHDF.create_session
        Features: Creates a new session under an existing (or newly created)
                  plant ID (PID) and sets the associated attributes to each
        Inputs:   - str, plant ID (pid)
                  - dict, PID attribute dictionary
                  - dict, session attribute dictionary
        Outputs:  str, new session path (session_path)
        Depends:
        """
        if self.isopen:
            session_num = self.get_session_number(pid)
            session_name = "Session-%d" % (session_num)
            session_path = "/%s/%s" % (pid, session_name)
            old_pid = self.pid_exists(pid)
            #
            # Create new session:
            try:
                self.hdfile.create_group(session_path)
            except:
                err_msg = "Error! Could not create session %s" % (session_path)
                raise IOError(err_msg)
            else:
                self.set_session_attrs(session_path, session_attrs)
                #
                # Set PID attributes and update dictionary:
                if old_pid:
                    self.pids[pid].append(session_num)
                else:
                    self.pids[pid] = [session_num,]
                    self.set_pid_attrs(pid, pid_attrs)
                return session_path
        else:
            raise IOError("Could not create new session. HDF5 file not open!")

    def delete_object(self, obj_parent, obj_name):
        """
        Name:     PridaHDF.delete_object
        Features: Deletes a given object from a given HDF5 parent object's
                  member list; it does NOT reduce file size!
        Inputs:   - str, object's parent w/ path (obj_parent)
                  - str, group/dataset object name (obj_name)
        Outputs:  None.
        Depends:  refresh_pids
        """
        if self.isopen:
            if obj_parent[-1:] != '/':
                obj_parent += "/"
            obj_path = "%s%s" % (obj_parent, obj_name)
            if obj_path in self.hdfile:
                try:
                    del self.hdfile[obj_parent][obj_name]
                except:
                    err_msg = ("PridaHDF.delete_dataset error. "
                               "Could not delte dataset: %s") % (obj_path)
                    raise IOError(err_msg)
                else:
                    self.refresh_pids()
            else:
                err_msg = "Error! Could not find object %s" % (obj_path)
                raise IOError(err_msg)
        else:
            raise IOError("Error! Could not delete object. HDF5 file not open!")

    def extract_attrs(self, opath):
        """
        Name:     PridaHDF.extract_attrs
        Features: Extracts PID and session attributes to two CSV files
                  a given directory
        Inputs:   str, full output path to directory where CSV files are to be
                  saved (opath)
        Outputs:  None.
        Depends:  - get_pid_attrs
                  - get_session_attrs
                  - list_objects
        """
        if self.isopen:
            # Create output files for saving attributes:
            bname = os.path.splitext(os.path.basename(self.hdfile.filename))[0]
            pids_fname = "%s_%s.%s" % (bname, "pid-attrs", "csv")
            sess_fname = "%s_%s.%s" % (bname, "sess-attrs", "csv")
            pids_file = os.path.join(opath, pids_fname)
            sess_file = os.path.join(opath, sess_fname)
            if os.path.isfile(pids_file) or os.path.isfile(sess_file):
                raise IOError("Cannot save file. File already exists!")
            else:
                try:
                    pFile = open(pids_file, 'w')
                    sFile = open(sess_file, 'w')
                except IOError:
                    err_msg = ("PridaHDF.extract_attrs error. "
                               "Could not open PID or session attribute files "
                               "for writing")
                    raise IOError(err_msg)
                except:
                    raise IOError("PridaHDF.extract_attrs unknown error!")
                else:
                    # Accumulate PID and session attribute keys:
                    pid_attr_keys = []
                    sess_attr_keys = []
                    for pid in self.pids:
                        pDict = self.get_pid_attrs(pid)
                        for k in pDict:
                            pid_attr_keys.append(k)
                        #
                        for session in self.list_objects(pid):
                            sPath = "/%s/%s" % (pid, session)
                            sDict = self.get_session_attrs(sPath)
                            for j in sDict:
                                sess_attr_keys.append(j)
                    pid_attr_keys = sorted(list(set(pid_attr_keys)))
                    sess_attr_keys = sorted(list(set(sess_attr_keys)))
                    #
                    # Build the headerline for PID attrs:
                    pid_headerline = 'PID,'
                    for k in pid_attr_keys:
                        pid_headerline += k
                        pid_headerline += ","
                    pid_headerline = pid_headerline.rstrip(",")
                    pid_headerline += "\n"
                    pFile.write(pid_headerline)
                    #
                    # Build the headerline for session attrs:
                    sess_headerline = 'PID,Session,'
                    for j in sess_attr_keys:
                        sess_headerline += j
                        sess_headerline += ','
                    sess_headerline = sess_headerline.rstrip(",")
                    sess_headerline += "\n"
                    sFile.write(sess_headerline)
                    #
                    # Write each PID attribute line:
                    for pid in self.pids:
                        pDict = self.get_pid_attrs(pid)
                        pid_write_line = str(pid)
                        pid_write_line += ","
                        for k in pid_attr_keys:
                            if k in pDict:
                                v = str(pDict[k])
                            else:
                                v = ''
                            v += ','
                            pid_write_line += v
                        pid_write_line = pid_write_line.rstrip(',')
                        pid_write_line += '\n'
                        pFile.write(pid_write_line)
                    #
                    # Write each session attribute line:
                    for pid in self.pids:
                        for session in self.list_objects(pid):
                            sess_write_line = str(pid)
                            sess_write_line += ","
                            sess_write_line += session
                            sess_write_line += ","
                            sPath = "/%s/%s" % (pid, session)
                            sDict = self.get_session_attrs(sPath)
                            for j in sess_attr_keys:
                                if j in sDict:
                                    u = str(sDict[j])
                                else:
                                    u = ''
                                u += ','
                                sess_write_line += u
                            sess_write_line = sess_write_line.rstrip(',')
                            sess_write_line += "\n"
                            sFile.write(sess_write_line)
                    #
                finally:
                    pFile.close()
                    sFile.close()
        #
        else:
            raise IOError("Error! Could not get attributes. HDF5 file not open!")

    def extract_datasets(self, ipath, opath, img_ext='.jpg', level=0):
        """
        Name:     PridaHDF.extract_datasets
        Features: Extracts image datasets from a given HDF5 file hierarchy to
                  a given directory
        Inputs:   - str, HDF5 input object with associated path (ipath)
                  - str, absolute path to output directory (opath)
                  - [optional] str, image file extension (img_ext)
                  - [optional] int, number of levels deep (level)
        Outputs:  None.
        """
        if self.isopen:
            if ipath in self.hdfile:
                obj_members = self.list_objects(ipath)
                if obj_members == 0:
                    # Found dataset!
                    member_ext = os.path.splitext(ipath)[1]
                    if member_ext == img_ext:
                        out_path = os.path.join(opath, ipath[1:])
                        out_dir = os.path.dirname(out_path)
                        print(("Saving %s at level %i") % (out_path, level))
                        self.mkdir_p(out_dir)
                        dset = self.get_dataset(ipath)
                        try:
                            scipy.misc.imsave(out_path, dset)
                        except:
                            err_msg = ("Could not extract dataset "
                                       "to %s") % (out_path)
                            raise IOError(err_msg)
                else:
                    # Keep digging!
                    level += 1
                    for member in obj_members:
                        if ipath[-1] != '/':
                            ipath += '/'
                        member_path = "%s%s" % (ipath, member)
                        self.extract_datasets(member_path, opath, img_ext, level)
            else:
                err_msg = ("PridaHDF.extract_datasets error! "
                           "Object '%s' does not exist.") % (ipath)
                raise IOError(err_msg)
        else:
            raise IOError("Error! Could not get datasets. HDF5 file not open!")

    def find_datasets(self, ipath='/', img_ext='.jpg'):
        """
        """
        if self.isopen:
            if ipath in self.hdfile:
                obj_members = self.list_objects(ipath)
                if obj_members == 0:
                    # Found dataset!
                    member_ext = os.path.splitext(ipath)[1]
                    if member_ext == img_ext:
                        if ipath not in self.datasets:
                            self.datasets[ipath] = 0
                else:
                    # Keep digging!
                    for member in obj_members:
                        if ipath[-1] != '/':
                            ipath += '/'
                        member_path = "%s%s" % (ipath, member)
                        self.find_datasets(member_path, img_ext)
            else:
                err_msg = ("PridaHDF.find_datasets error! "
                           "Object '%s' does not exist.") % (ipath)
                raise IOError(err_msg)
        else:
            raise IOError("Error! Could not find datasets. HDF5 file not open!")


    def get_about(self):
        """
        Name:     PridaHDF.get_about
        Features: Returns dictionary of about strings
        Inputs:   None.
        Outputs:  dict, about strings (my_overview)
        """
        if self.isopen:
            my_author = "%s" % (self.get_root_user())
            my_contact = "%s" % (self.get_root_addr())
            my_summary = "%s" % (self.get_root_about())
            my_plants = "%d plants" % (len(list(self.pids.keys())))
            my_sessions = "%d sessions" % (numpy.array(
                [len(self.pids[i]) for i in self.pids]).sum())
            my_photos = "%d images" % (len(list(self.datasets.keys())))

            my_overview = {"Author": my_author,
                           "Contact": my_contact,
                           "Summary": my_summary,
                           "Plants": my_plants,
                           "Sessions": my_sessions,
                           "Photos": my_photos}
            return my_overview

    def get_attr(self, attr, group_path):
        """
        Name:     PridaHDF.get_attr
        Features: Returns the given attribute for the given path
        Inputs:   - str, attribute name (attr)
                  - str, group path (group_path)
        Outputs:  str, attribute value (val)
        """
        if self.isopen:
            if group_path in self.hdfile:
                try:
                    tmp = self.hdfile[group_path].attrs[attr]
                    #
                    if isinstance(tmp, str):
                        val = tmp
                    elif isinstance(tmp, numpy.ndarray):
                        val = tmp
                    else:
                        try:
                            val = tmp.decode('UTF-8')
                        except:
                            val = tmp
                except KeyError:
                    print(("Warning! '%s' has no attribute '%s'") %
                          (group_path, attr))
                    val = self.UNDEFINED
                except:
                    print(("Warning! Attribute '%s' could not be retrieved "
                           "from path '%s'!") % (attr, group_path))
                    val = self.UNDEFINED
            else:
                print("Warning! Path not defined!")
                val = self.UNDEFINED
        else:
            print("Warning! Could not get attribute. HDF5 file not open!")
            val = self.UNDEFINED
        #
        return val

    def get_pid_attrs(self, pid):
        """
        Name:     PridaHDF.get_pit_attrs
        Features: Returns dictionary of plant ID group attributes
        Inputs:   str, plant ID (pid)
        Outputs:  dict, PID attributes (attrs_dict)
        Depends:  pid_exists
        """
        attr_dict = {}
        if self.isopen:
            if self.pid_exists(pid):
                for key in self.hdfile[pid].attrs.keys():
                    val = self.hdfile[pid].attrs[key]
                    if isinstance(val, str):
                        attr_dict[key] = val
                    else:
                        attr_dict[key] = val.decode('UTF-8')
            else:
                err_msg = ("Error! Could not retrieve attributes "
                           "from PID: %s") % (pid)
                raise IOError(err_msg)
        else:
            raise IOError("Error! Could not read PID attributes. HDF5 file not open!")
        #
        return attr_dict

    def get_dataset(self, ds_path):
        """
        Name:     PridaHDF.get_dataset
        Features: Returns a given dataset for a given session
        Inputs:   str, dataset path (ds_path)
        Outputs:  numpy.ndarray
        """
        dset = numpy.array([])
        if self.isopen:
            if ds_path in self.hdfile:
                dset = self.hdfile[ds_path][:,:]
            else:
                print("Warning! Could not find image in dataset %s" % (ds_path))
        else:
            print("Warning! Could not get datasets. HDF5 file not open!")
        #
        return dset

    def get_root_about(self):
        """
        Name:     PridaHDF.get_root_about
        Features: Returns the root group about attribute
        Inputs:   None.
        Outputs:  str, root group about message (attr)
        Depends:  get_attr
        """
        if self.isopen:
            attr = self.get_attr("about", "/")
        else:
            print("Warning! Could not get root about. HDF5 file not open!")
            attr = self.UNDEFINED
        #
        return attr

    def get_root_addr(self):
        """
        Name:     PridaHDF.get_root_addr
        Features: Returns the root group user address (e.g., NetID)
        Inputs:   None.
        Outputs:  str, root group user address (attr)
        Depends:  get_attr
        """
        if self.isopen:
            attr = self.get_attr("addr", "/")
        else:
            print("Warning! Could not get root addr. HDF5 file not open!")
            attr = self.UNDEFINED
        #
        return attr

    def get_root_user(self):
        """
        Name:     PridaHDF.get_root_user
        Features: Returns the root group username
        Inputs:   None.
        Outputs:  str, root group username (attr)
        Depends:  get_attr
        """
        if self.isopen:
            attr = self.get_attr("user", "/")
        else:
            print("Warning! Could not get root user. HDF5 file not open!")
            attr = self.UNDEFINED
        #
        return attr

    def get_session_attrs(self, session_path):
        """
        Name:     PridaHDF.get_session_attrs
        Features: Returns dictionary of plant ID session attributes
        Inputs:   str, session path (session_path)
        Outputs:  dict, session attributes (attrs_dict)
        """
        attr_dict = {}
        if self.isopen:
            if session_path in self.hdfile:
                for key in self.hdfile[session_path].attrs.keys():
                    val = self.hdfile[session_path].attrs[key]
                    if isinstance(val, str):
                        attr_dict[key] = val
                    elif isinstance(val, numpy.ndarray):
                        attr_dict[key] = val
                    else:
                        attr_dict[key] = val.decode('UTF-8')
            else:
                err_msg = ("Error! Could not retrieve attributes from session: "
                           "%s") % (session_path)
                raise IOError(err_msg)
        else:
            raise IOError("Error! Could not read session attributes. HDF5 file not open!")
        #
        return attr_dict

    def get_session_number(self, pid):
        """
        Name:     PridaHDF.get_session_number
        Features: Returns the value for a new session number
        Inputs:   str, plant ID (pid)
        Outputs:  None.
        Depends:  refresh_pids
        """
        self.refresh_pids()
        #
        try:
            session_max_val = max(self.pids[pid])
        except ValueError:
            session_num = 1
        except KeyError:
            session_num = 1
        except:
            print("Warning! Unexpected problem with get_session_name.")
            session_num = 1
        else:
            session_num = session_max_val + 1
        finally:
            return session_num

    def img_to_hdf(self, img, ds_path):
        """
        Name:     PridaHDF.img_to_hdf
        Features: Saves image dataset to current HDF repository; currently set
                  without compression, but with chunks
        Inputs:   - numpy.ndarray, openCV image data array (img)
                  - str, dataset name with path (ds_path)
        """
        if ds_path in self.hdfile:
            print("Warning! Overwriting dataset %s" % (ds_path))
        if img is None:
            err_msg = "PridaHDF.img_to_hdf ERROR: no image data sent."
            raise IOError(err_msg)
        else:
            try:
                x, y, z = img.shape # NOTE: issue will arise with monochrome
                if (x % 16 == 0 & y % 16 == 0):
                    my_chunks = (int(x/16), int(y/16), int(z))
                else:
                    my_chunks = (int(x/10), int(y/10), int(z))
                #
                self.hdfile.create_dataset(name=ds_path,
                                           data=img,
                                           chunks=my_chunks)
            except:
                err_msg = ("PridaHDF.img_to_hdf ERROR: "
                           "could not create dataset %s") % (ds_path)
                raise IOError(err_msg)
            else:
                # Save image dimensions (i.e., pixel y, pixel x, color bands):
                # NOTE: openCV reads images in BGR
                #       scipy.misc reads images in RGB
                self.hdfile[ds_path].dims[0].label = 'pixels in y'
                self.hdfile[ds_path].dims[1].label = 'pixels in x'
                self.hdfile[ds_path].dims[2].label = 'RGB color bands'

    def list_objects(self, parent_path):
        """
        Name:     PridaHDF.list_objects
        Features: Returns a list of HDF5 objects under a given parent
        Inputs:   str, HDF5 path to parent object
        Outputs:  list, HDF5 objects
        """
        rlist = []
        if self.isopen:
            if parent_path in self.hdfile:
                try:
                    self.hdfile[parent_path].keys()
                except:
                    # Dataset has no members
                    rlist = 0
                else:
                    for obj in self.hdfile[parent_path].keys():
                        rlist.append(obj)
            else:
                print("Warning! Object %s does not exist!" % (parent_path))
        else:
            print("Warning! Could not get list objects. HDF5 file not open!")
        #
        return rlist

    def list_pids(self):
        """
        Name:     PridaHDF.list_pids
        Features: Returns a list of current PIDs
        Inputs:   None.
        Outputs:  list, PID names
        Depends:  list_objects
        """
        pid_list = self.list_objects('/')
        return sorted(pid_list)

    def list_sessions(self, pid):
        """
        Name:     PridaHDF.list_sessions
        Features: Returns a list of sorted sessions for a given PID
        Inputs:   str, plant ID (pid)
        Outputs:  list, session names
        Depends:  list_objects
        """
        rlist = []
        if self.pid_exists(pid):
            temp = {}
            for session in self.list_objects(pid):
                try:
                    temp_val = re.search('\D+(\d+)', session).group(1)
                    temp_val = int(temp_val)
                except:
                    print("Warning! There was a problem listing "
                          "session %s" % (session))
                else:
                    temp[temp_val] = session
            #
            for session_num in sorted(temp.keys()):
                rlist.append(temp[session_num])
        else:
            print("Warning! PID is empty.")
        #
        return rlist

    def mkdir_p(self, path):
        """
        Name:     PridaHDF.mkdir_p
        Features: Makes directories, including intermediate directories as
                  required (i.e., directory tree)
        Inputs:   str, directory path (path)
        Outputs:  None.
        Ref:      tzot (2009) "mkdir -p functionality in python," StackOverflow,
                  Online: http://stackoverflow.com/questions/600268/mkdir-p-
                  functionality-in-python
        """
        try:
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    def new_file(self, dir_path, hdf_name):
        """
        Name:     PridaHDF.new_file
        Features: Creates a new HDF5 file, overwriting an existing file if it
                  exists.
        Inputs:   - str, directory path (dir_path)
                  - str, HDF5 filename (hdf_name)
        Outputs:  None.
        """
        # String handling:
        if dir_path[-1:] != "/":
            dir_path += "/"
        if hdf_name[-5:] != ".hdf5":
            hdf_name += ".hdf5"
        #
        # Save directory and filename as class variables:
        self.dir = dir_path
        self.filename = hdf_name
        my_hdf = dir_path + hdf_name
        #
        # Create empty HDF file:
        if os.path.isfile(my_hdf):
            print("Warning! That file already exits! Exiting...")
        else:
            out_msg = ("Creating file %s ..." % (hdf_name))
            try:
                self.hdfile = h5py.File(my_hdf, 'w')
            except:
                print("%s failed!" % out_msg)
                self.isopen = False
            else:
                print("%s success!" % out_msg)
                self.isopen = True

    def open_file(self, hdf_path):
        """
        Name:     PridaHDF.open_file
        Features: Opens an existing HDF5 file or creates new if file is not
                  found.
        Inputs:   str, HDF5 filename with path (hdf_path)
        Outputs:  None.
        Depends:  - refresh_pids
                  - find_datasets
        """
        # Open existing HDF file:
        if os.path.isfile(hdf_path):
            out_msg = ("Opening file %s ..." % (os.path.basename(hdf_path)))
        else:
            out_msg = ("Could not find file! "
                       "Creating %s ...") % (os.path.basename(hdf_path))
        #
        try:
            # NOTE: 'a' flag opens existing or creates new
            self.hdfile = h5py.File(hdf_path, 'a')
        except:
            print("%s failed!" % out_msg)
            self.isopen = False
        else:
            print("%s success!" % out_msg)
            self.isopen = True
            #
            # Populate PID & dataset dictionaries:
            self.refresh_pids()
            self.find_datasets()

    def pid_exists(self, pid):
        """
        Name:     PridaHDF.pid_exists
        Features: Checks to see if PID already exists in HDF5 file
        Inputs:   str, plant ID (pid)
        Outputs:  bool
        """
        if pid in self.pids.keys():
            return True
        else:
            return False

    def refresh_pids(self):
        """
        Name:     PridaHDF.refresh_pids
        Features: Refreshes the class dictionary with current plant IDs
        Inputs:   None.
        Outputs:  None.
        """
        self.pids = {}
        for pid in self.hdfile.keys():
            self.pids[pid] = []
            for session in self.hdfile[pid].keys():
                try:
                    # Search for digits following non-digits
                    session_num = re.search('\D+(\d+)', session).group(1)
                    session_num = int(session_num)
                except:
                    err_msg = ("Error! Could not extract number from "
                               "session %s") % (session)
                    raise IOError(err_msg)
                else:
                    self.pids[pid].append(session_num)

    def save(self):
        """
        Name:     PridaHDF.save
        Features: Flushes changes to the HDF file.
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile.flush()
            except:
                raise IOError("Could not flush HDF5 file!")

    def save_image(self, session_path, img_path, make_thumb=True):
        """
        Name:     PridaHDF.save_image
        Features: Saves image data as a dataset to a given session
        Input:    - str, session path (session_path)
                  - str, input image name with path (img_path)
                  - [optional] bool, flag for thumbnail creation (make_thumb)
        Output:   None.
        Depends:  img_to_hdf
        """
        if self.isopen:
            # NOTE: img_name includes the original file extension required for
            #       extraction (see extract_datasets)
            try:
                img_name = os.path.basename(img_path)
                img_base = os.path.splitext(img_name)[0]
            except:
                err_msg = ("PridaHDF.save_image error! "
                           "Could not process image: %s") % (img_path)
                raise IOError(err_msg)
            else:
                if not os.path.isfile(img_path):
                    err_msg = ("PridaHDF.save_image error! "
                               "Image '%s' does not exist!") % (img_path)
                    raise IOError(err_msg)
                else:
                    if session_path[-1:] != '/':
                        session_path += "/"
                    ds_path = "%s%s/%s" % (session_path, img_base, img_name)
                    img = scipy.misc.imread(img_path)
                    self.img_to_hdf(img, ds_path)
                    if make_thumb:
                        self.save_thumb(img, ds_path)
        else:
            raise IOError("Error! Could not save image. HDF5 file not open!")

    def save_thumb(self, img, ds_path):
        """
        Name:     PridaHDF.save_thumb
        Features: Saves a thumbnail version of given image to same dataset
                  group scaled to a width of 250 pixels
        Inputs:   - numpy.ndarray, openCV image data array (img)
                  - str, dataset name with path (ds_path)
        """
        if ds_path in self.hdfile:
            if img is not None:
                try:
                    (height, width, depth) = img.shape
                except:
                    # Maybe greyscaled image?
                    print("Warning! Encountered greyscale image in save_thumb")
                    (height, width) = img.shape
                s_factor = 250.0/width
                #
                try:
                    thumb_path = "%s.thumb" % (os.path.splitext(ds_path)[0])
                    thumb = scipy.misc.imresize(img, s_factor, 'bilinear')
                    self.hdfile.create_dataset(name=thumb_path, data=thumb)
                except:
                    err_msg = ("PridaHDF.save_thumb ERROR: "
                               "could not create dataset %s") % (thumb_path)
                    raise IOError(err_msg)

    def set_attr(self, attr_name, attr_val, obj_path):
        """
        Name:     PridaHDF.set_attr
        Features: Sets given attribute to given path
        Inputs:   - str, attribute name (attr_name)
                  - [dtype], attribute value (attr_val)
                  - str, path to group/dataset object (obj_path)
        Outputs:  None.
        """
        if self.isopen:
            try:
                # Typecast strings to data:
                if isinstance(attr_val, str):
                    attr_val = attr_val.encode('utf-8')

                self.hdfile[obj_path].attrs.create(name=attr_name,
                                                   data=attr_val)
            except:
                err_msg = ("Error! Could not set value '%s' to attribute '%s' "
                           "for path '%s'") % (attr_val, attr_name, obj_path)
                raise IOError(err_msg)
        else:
            raise IOError("Error! Could not set attribute. HDF5 file not open!")

    def set_pid_attrs(self, pid, attrs_dict):
        """
        Name:     PridaHDF.set_pit_attrs
        Features: Sets plant ID group attributes
        Inputs:   - str, plant ID (pid)
                  - dict, PID attributes (attrs_dict)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if self.pid_exists(pid):
                for key in attrs_dict.keys():
                    val = attrs_dict[key]
                    try:
                        self.set_attr(key, val, pid)
                    except:
                        err_msg = "Error! Could not set PID attr %s" % (key)
                        raise IOError(err_msg)
            else:
                err_msg = "Error! Could not attribute PID: %s" % (pid)
                raise IOError(err_msg)
        else:
            raise IOError("Error! Could not set PID attributes. HDF5 file not open!")

    def set_root_about(self, attr):
        """
        Name:     PridaHDF.set_root_about
        Features: Sets the root group about attribute (e.g., short experiment
                  description)
        Inputs:   str, root group about message (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.set_attr("about", attr, "/")
        else:
            raise IOError("Error! Could not set root about. HDF5 file not open!")

    def set_root_addr(self, attr):
        """
        Name:     PridaHDF.set_root_addr
        Features: Sets the root group user address (e.g., NetID)
        Inputs:   str, root group user address (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.set_attr("addr", attr, "/")
        else:
            raise IOError("Error! Could not set root addr. HDF5 file not open!")

    def set_root_user(self, attr):
        """
        Name:     PridaHDF.set_root_user
        Features: Sets the root group username
        Inputs:   str, root group username (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.set_attr("user", attr, "/")
        else:
            raise IOError("Error! Could not set root user. HDF5 file not open!")

    def set_session_attrs(self, session_path, attrs_dict):
        """
        Name:     PridaHDF.set_pit_attrs
        Features: Sets session attributes
        Inputs:   - str, session path (session_path)
                  - dict, session attributes (attrs_dict)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if session_path in self.hdfile:
                for key in attrs_dict.keys():
                    val = attrs_dict[key]
                    try:
                        self.set_attr(key, val, session_path)
                    except:
                        err_msg = "Error! Could not set session attr %s" % (key)
                        raise IOError(err_msg)
            else:
                err_msg = "Error! Could not attribute session %s" % (session_path)
                raise IOError(err_msg)

    #    < -------- TESTING FUNCTIONS FOR DISPLAYING IMAGE DATA -------- >

    def show_thumb(self, session_path, base_name, rot=0):
        """
        Name:     PridaHDF.show_thumb
        Features: Displays a thumb image using openCV
        Inputs:   - str, session path (session_path)
                  - str, dataset base name (base_name)
                  - [optional] int, rotation angle (rot)
        Outputs:  None.
        Depends:  - get_dataset
        """
        if analysis_mode:
            if self.isopen:
                if session_path[-1:] != '/':
                    session_path += "/"
                ds_path = "%s%s/%s.thumb" % (session_path, base_name, base_name)
                if ds_path in self.hdfile:
                    img = self.get_dataset(ds_path)
                    cv2.namedWindow('Image', cv2.WINDOW_AUTOSIZE)
                    #
                    # Rotation:
                    if rot != 0:
                        img = scipy.ndimage.interpolation.rotate(img, rot)
                    #
                    cv2.imshow("Image", img)
                    cv2.moveWindow("Image", 0, 0)
                    cv2.waitKey(0)
                    cv2.destroyWindow("Image")
                else:
                    err_msg = "Error! Could not show image: %s" % (ds_path)
                    raise IOError(err_msg)
            else:
                raise IOError("Error! Could not plot image. HDF5 file not open!")
    #
