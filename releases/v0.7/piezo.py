#!/usr/bin/python
#
# piezo.py
#
# VERSION: 0.7.0-dev
#
# LAST EDIT: 2015-10-21
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 36d81c1: Nathanael Shaw - 2015-10-01 16:23:21
#     version 0.6.1-dev
# 9f50386: Nathanael Shaw - 2015-09-30 14:19:10
#     update changelogs
# 42dd0fd: Nathanael Shaw - 2015-09-28 12:18:45
#     correct typo
# d42ec29: Nathanael Shaw - 2015-09-28 12:05:04
#     remove module level vars from scripts
# 9c28bfe: Nathanael Shaw - 2015-09-28 11:57:19
#     title hotfix
# b089bd6: Nathanael Shaw - 2015-09-28 11:36:52
#     add open-source license disclaimers
# 48c37c3: Nathanael Shaw - 2015-09-24 16:37:28
#     Update changelogs for new repo
# d978041: twdavis - 2015-09-22 13:22:25
#     Addresses #3. Python 2/3 compatability.
# 44a5255: Nathanael Shaw - 2015-09-21 17:38:57
#     Merge with periphdev
# 99e70b2: twdavis - 2015-09-21 16:34:31
#     added working
# $log_end_tag$
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import time

from Adafruit_MotorHAT import Adafruit_MotorHAT
from pridaperipheral import PRIDAPeripheral
from pridaperipheral import PRIDAPeripheralError


###############################################################################
## CLASSES:
###############################################################################
class Piezo(Adafruit_MotorHAT, PRIDAPeripheral):
    def __init__(self):
            PRIDAPeripheral.__init__(self)
            Adafruit_MotorHAT.__init__(self)
            self._CHANNEL = 0
            self._VOLUME = 1.0

    @property
    def channel(self):
        """
        Name:    Piezo.channel
        Feature: Returns PWM channel controlling the piezo buzzer.
        Inputs:  None
        Outputs: Integer, channel number assigned to controlling the
                 piezo buzzer (self._CHANNEL)
        """
        return self._CHANNEL

    @property
    def volume(self):
        """
        Name:    Imaging.volume
        Feature: Returns float value representing percent of maximum
                 volume
        Inputs:  None
        Outputs: Float, value representing currently assigned piezo
                 buzzer volume (self._VOLUME)
        """
        return 100 * self._VOLUME

    @channel.setter
    def channel(self, ch):
        """
        Name:    Piezo.channel
        Feature: Set the PWM channel controlling the piezo buzzer.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._CHANNEL = ch

    @volume.setter
    def volume(self, volume):
        """
        Name:    Piezo.volume
        Feature: Set piezo volume by duty cycle.
        Inputs:  int, percent of maximum volume (volume)
        Outputs: None
        """
        if type(volume) in {int, float}:
            if volume < 0:
                volume = 0
            elif volume > 100:
                volume = 100
            self._VOLUME = volume / 100.0

    def beep(self, num_beeps=3, beep_time=0.05, rest_time=0.1):
        """
        Name:    Piezo.beep
        Feature: Temporarily raise the 'piezo' channel. Default is three
                 short beeps in quick succession.
        Inputs:  - int, desired number of beeps (num_beeps)
                 - float, length of each beep in seconds (beep_time)
                 - float, length of silence between beeps (rest_time)
        Outputs: None
        """
        for i in range(num_beeps):
            # turn buzzer on
            self._pwm.setPWM(self._CHANNEL, 0, int(2048 * self._VOLUME))
            # sustain note
            time.sleep(beep_time)
            # turn buzzer off
            self._pwm.setPWM(self._CHANNEL, 0, 4096)
            # rest
            time.sleep(rest_time)

    def connect(self):
        try:
            self.beep(2)
        except:
            raise PRIDAPeripheralError('Could not connect to piezo!')

    def poweroff(self):
        self._pwm.setPWM(self._CHANNEL, 0, 0)

    def current_status(self):
        super(self.current_status())
