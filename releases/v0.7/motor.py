#!/usr/bin/python
#
# motor.py
#
# VERSION: 0.7.0-dev
#
# LAST EDIT: 2015-10-21
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 36d81c1: Nathanael Shaw - 2015-10-01 16:23:21
#     version 0.6.1-dev
# 9f50386: Nathanael Shaw - 2015-09-30 14:19:10
#     update changelogs
# 42dd0fd: Nathanael Shaw - 2015-09-28 12:18:45
#     correct typo
# d42ec29: Nathanael Shaw - 2015-09-28 12:05:04
#     remove module level vars from scripts
# 9c28bfe: Nathanael Shaw - 2015-09-28 11:57:19
#     title hotfix
# b089bd6: Nathanael Shaw - 2015-09-28 11:36:52
#     add open-source license disclaimers
# 48c37c3: Nathanael Shaw - 2015-09-24 16:37:28
#     Update changelogs for new repo
# d978041: twdavis - 2015-09-22 13:22:25
#     Addresses #3. Python 2/3 compatability.
# 44a5255: Nathanael Shaw - 2015-09-21 17:38:57
#     Merge with periphdev
# 99e70b2: twdavis - 2015-09-21 16:34:31
#     added working
# $log_end_tag$
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import time

from Adafruit_MotorHAT import Adafruit_MotorHAT
from pridaperipheral import PRIDAPeripheral
from pridaperipheral import PRIDAPeripheralError


###############################################################################
## CLASSES:
###############################################################################
class Motor(Adafruit_MotorHAT, PRIDAPeripheral):

    def __init__(self):
        """
        Initialize data memebers. Inherit attributes and
        behaviours from the Adafruit_MotorHAT object.
        Note: currently assumes that system is always using
        microstepping.
        """
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self._rpm = 2.0           # Approximate rotational velocity
        self._step_res = 200      # Number of units steps per rotation
        self._motor_port_num = 1  # Motor HAT Port Number In Use
        self._sm = self.getStepper(self.step_res, self.motor_port_num)
        #                        ^ Adafruit Stepper Motor Object
        #                       \ / Minimum Time Elapsed In a Rotation
        self._base_rot_time = 1.75 * self._sm.MICROSTEPS
        self._permit_edits = False

    @property
    def microsteps(self):
        """
        Name:    Motor.microsteps
        Feature: Returns number of microsteps per unit step
        Inputs:  None
        Outputs: Int, number of microsteps per unit step (self.microsteps)
        """
        return self._sm.MICROSTEPS

    @microsteps.setter
    def microsteps(self, value):
        """
        Name:    Motor.microsteps
        Feature: Sets the number of microsteps per unit step
        Inputs:  Int, number of microsteps per unit step (value)
        Outputs: None
        """
        if type(value) is int:
            if value in {4, 8, 12, 16}:
                self._sm.MICROSTEPS = value
                self._sm._build_microstep_curve()

    @property
    def degrees(self):
        """
        Name:    Motor.degrees
        Feature: Returns Step Resolution in terms of degrees per unit step.
        Inputs:  None
        Outputs: Float, degree representation of step resolution
                 (Motor.degrees)
        """
        return 360.0 / self.step_res

    @degrees.setter
    def degrees(self, value):
        """
        Name:    Motor.degrees
        Feature: Sets Step Resolution in terms of degrees per unit step.
        Inputs:  Float, desired degree representation of step resolution
                 (Motor.degrees)
        Outputs: None
        """
        if type(value) in {int, float}:
            if 0 < value < 360:
                if (360.0 / value) % 1 == 0:
                    self.step_res = int(360 / value)
                else:
                    raise ValueError('Degrees attribute' +
                                     ' must evenly divide 360.')

    @property
    def rpm(self):
        """
        Name:    Motor.rpm
        Feature: Returns the approximate RPM value at which the motor is
                 to spin
        Inputs:  None
        Outputs: Float, value approximating currently assigned motor
                 rotational speed (self._rpm)
        """
        return self._rpm

    @property
    def step_res(self):
        """
        Name:    Motor.step_res
        Feature: Returns the number of unit steps within a single full
                 rotation at which the motor is currently configured
        Inputs:  None
        Outputs: Integer, number of unit steps per full rotation
                 (self._step_res)
        """
        return self._step_res

    @property
    def motor_port_num(self):
        """
        Name:    Motor.motor_port_num
        Feature: Returns the port number which the hat is currently
                 controlling
        Inputs:  None
        Outputs: Integer, the number representing the in-use motorHAT
                 port (self._motor_port_num)
        """
        return self._motor_port_num

    @property
    def base_rot_time(self):
        """
        Name:    Motor.base_rot_time
        Feature: Returns the minimum amount of time at which the motor
                 can complete a full rotation with a microstepping
                 regieme
        Inputs:  None
        Outputs: Float, minimum number of seconds for a full rotation
                 (self._base_rot_time)
        """
        return self._base_rot_time

    @property
    def permit_edits(self):
        """
        Name:    Motor.permit_edits
        Feature: Returns whether or not editing critical attributes is
                 permitted.
        Inputs:  None
        Outputs: Boolean, value indicating permission to edit
                 (self._permit_edits)
        """
        return self._permit_edits

    @rpm.setter
    def rpm(self, rot_per_min):
        """
        Name:    Motor.rpm
        Feature: Set approximate rotations per minute value for the
                 motor.
        Inputs:  float, desired rpm value (rot_per_min)
        Outputs: None
        """
        if type(rot_per_min) is float or type(rot_per_min) is int:
            if rot_per_min > 0:
                self._rpm = rot_per_min
            else:
                print('RPM requires a non-negative value.')
        else:
            print('RPM must be a number.')

    @step_res.setter
    def step_res(self, sr):
        """
        Name:    Motor.step_res
        Feature: Set stepper motor step resolution. Not currently
                 supported. Data member added to reduce presence of
                 'magic numbers.'
        Inputs:  - int, desired 'step resolution,' or the number of
                   steps in a full rotation, defining the length of a
                   unit step (sr)
        Outputs: None
        """
        if self._permit_edits is True:
            if type(sr) is int:
                if sr > 0:
                    self._step_res = sr
                else:
                    print('Step resolution must be non-negative')
            else:
                print('Step resolution must be an integer')
        else:
            print('Editing step resolution is forbidden.')

    @motor_port_num.setter
    def motor_port_num(self, num):
        """
        Name:    Motor.motor_port_num
        Feature: Set stepper motor port number. Not currently supported.
                 Data member added to reduce presence of 'magic numbers.'
        Inputs:  - int, desired port number to be used on the HAT (num)
        Outputs: None
        """
        if self._permit_edits is True:
            if type(num) is int:
                if num in {1, 2}:
                    self._motor_port_num = num
                else:
                    print('Motor port number restricted to 1 or 2.')
            else:
                print('Motor port number must be an integer.')
        else:
            print('Editing the motor port number is forbbiden.')

    @base_rot_time.setter
    def base_rot_time(self, base_time):
        """
        Name:    Motor.base_rot_time
        Feature: Set stepper motor base rotation time (in seconds)
                 During full speed operation. Not currently supported.
                 Data member added to reduce presence of 'magic numbers.'
        Inputs:  - int, minmum rotation time in seconds (base_time)
        Outputs: None
        """
        if self._permit_edits is True:
            if type(base_time) is int or type(base_time) is float:
                if base_time > 0:
                    self._base_rot_time = base_time
                else:
                    print('Base rotation time must be non-negative.')
            else:
                print('Base rotation time must be a number.')
        else:
            print('Editing base rotation time is forbidden.')

    @permit_edits.setter
    def permit_edits(self, permit):
        """
        Name:    Motor.permit_edits
        Feature: Sets the permit edits attribute to a specified
                 boolean value
        Inputs:  Boolean, desired editing permissions (permit)
        Outputs: None
        """
        if type(permit) is bool:
            self._permit_edits = permit
        else:
            print('Permit edits attribute requires a boolean value.')

    def turn_off_motors(self):
        """
        Name:    Motor.turn_off_motors
        Feature: Disable all motors attached to the HAT.
        Inputs:  None
        Outputs: None
        """
        self.getMotor(1).run(self.RELEASE)
        self.getMotor(2).run(self.RELEASE)
        self.getMotor(3).run(self.RELEASE)
        self.getMotor(4).run(self.RELEASE)

    def calibrate(self):
        """
        Name:    Motor.calibrate
        Feature: Take three initial unit steps.
        Inputs:  None
        Outputs: None
        """
        for i in range(3 * self._sm.MICROSTEPS):
            self._sm.oneStep(self.FORWARD, self.MICROSTEP)
            time.sleep(.1)

    def step(self, num_photos):
        """
        Name:    Motor.step
        Feature: Take a single Inter-Photo-Angle (IPA) step.
                 Take the desired number of photos in the run and
                 calculate the IPA step size. Convert the desired RPM
                 value into the appropriate length wait time.
        Inputs:  Integer, number of photos to be taken (num_photos)
        Outputs: None
        """
        # data member error checking
        if self.rpm <= 0:
            print("RPM must be greater than zero.")
            return None
        try:
            # convert RPM to req'd value for calculating wait time
            real_rpm = (1.0 / (((60.0 / self.rpm) -
                               self.base_rot_time) / 60.0))
        except ZeroDivisionError:
            print("Set a valid RPM value.")
        else:
            # begin stepping
            for i in range((self.step_res * self._sm.MICROSTEPS) /
                           num_photos):
                self._sm.oneStep(self.FORWARD, self.MICROSTEP)
                time.sleep(60.0 / (self.step_res *
                                   self._sm.MICROSTEPS * real_rpm))

    def single_step(self):
        """
        Name:    motor.single_step
        Feature: Takes a single microstep
        Inputs:  None
        Outputs: None
        """
        # data member error checking
        if self.rpm <= 0:
            print("RPM must be greater than zero.")
            return None
        try:
            # convert RPM to req'd value for calculating wait time
            real_rpm = (1.0 / (((60.0 / self.rpm) -
                               self.base_rot_time) / 60.0))
            if real_rpm <= 0:
                raise ZeroDivisionError()
        except ZeroDivisionError:
            raise AttributeError("Set a valid RPM value" +
                                 " before attempting a step.")
        else:
            self._sm.oneStep(self.FORWARD, self.MICROSTEP)
            time.sleep(60.0 / (self.step_res * self._sm.MICROSTEPS * real_rpm))

    def connect(self):
        try:
            self.calibrate()
        except:
            raise PRIDAPeripheralError('Could not connect to motor!')

    def poweroff(self):
        self.turn_off_motors()

    def current_status(self):
        super(self.current_status())
