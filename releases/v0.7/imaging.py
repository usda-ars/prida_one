#!/usr/bin/python
#
# imaging.py
#
# VERSION: 0.7.0-dev
#
# LAST EDIT: 2015-10-21
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 36d81c1: Nathanael Shaw - 2015-10-01 16:23:21
#     version 0.6.1-dev
# 0a1d8a9: Nathanael Shaw - 2015-10-01 15:22:56
#     add date time and angle attrs to hdf image dset
# 115a934: Nathanael Shaw - 2015-10-01 09:26:54
#     abstract hard coded numbers
# caba8f8: Nathanael Shaw - 2015-09-30 17:15:28
#     fix image filenames
# 3b43982: Nathanael Shaw - 2015-09-30 16:06:00
#     num_photos default 1
# 9f50386: Nathanael Shaw - 2015-09-30 14:19:10
#     update changelogs
# e78a06b: Nathanael Shaw - 2015-09-30 14:15:57
#     Resolves #7. minor cleanup.
# 40db3ea: Nathanael Shaw - 2015-09-30 14:05:53
#     address #7. pad image filenames with leading zeros
# 74a0edd: Nathanael Shaw - 2015-09-30 13:58:01
#     address #7. replace floats with ints, capture pos
# 3d07f5e: Nathanael Shaw - 2015-09-30 13:00:02
#     address #7. include bidirectional rounding
# 8fb75c5: Nathanael Shaw - 2015-09-30 11:11:19
#     addres #7. add missing return
# 89eed13: Nathanael Shaw - 2015-09-30 11:03:46
#     address #7. fix bad rounding
# 473dcbd: Nathanael Shaw - 2015-09-30 10:52:21
#     address #7. round microsteps to next unit step
# 13944ea: Nathanael Shaw - 2015-09-30 10:37:37
#     address #7. fix list index outof range
# d31f758: Nathanael Shaw - 2015-09-30 10:17:56
#     compare apples to apples
# 461cba9: Nathanael Shaw - 2015-09-30 10:11:53
#     address #7. send filepath to hdf, update pos
# 010a27c: Nathanael Shaw - 2015-09-30 09:57:06
#     update pos by run_sequence
# 5f0aa8a: Nathanael Shaw - 2015-09-29 17:19:43
#     run_sequence return filepath only on success
# 46849bf: Nathanael Shaw - 2015-09-29 15:58:40
#     raise exceptions, consistant returns
# 19664e5: twdavis - 2015-09-29 15:18:54
#     minor updates
# 42dd0fd: Nathanael Shaw - 2015-09-28 12:18:45
#     correct typo
# d42ec29: Nathanael Shaw - 2015-09-28 12:05:04
#     remove module level vars from scripts
# 9c28bfe: Nathanael Shaw - 2015-09-28 11:57:19
#     title hotfix
# b089bd6: Nathanael Shaw - 2015-09-28 11:36:52
#     add open-source license disclaimers
# 48c37c3: Nathanael Shaw - 2015-09-24 16:37:28
#     Update changelogs for new repo
# d978041: twdavis - 2015-09-22 13:22:25
#     Addresses #3. Python 2/3 compatability.
# 44a5255: Nathanael Shaw - 2015-09-21 17:38:57
#     Merge with periphdev
# 99e70b2: twdavis - 2015-09-21 16:34:31
#     added working
# $log_end_tag$
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import time
import atexit
import traceback
import os
import datetime
try:
    from configparser import SafeConfigParser  # Python 3+
except:
    from ConfigParser import SafeConfigParser  # Python 2

from motor import Motor
from camera import Camera
from led import LED
from piezo import Piezo
from pridaperipheral import PRIDAPeripheralError


###############################################################################
## FUNCTIONS:
###############################################################################
@atexit.register
def exit_sequence():
    """
    Name:    Imaging.exit_sequence
    Feature: Disables all hardware before application close
    Inputs:  None
    Outputs: None
    """
    i = Imaging()
    i.motor.turn_off_motors()
    i.led.all_off()


###############################################################################
## CLASSES:
###############################################################################
class Imaging(object):
    """
    Aggregate imaging hardware class for PRIDA.
    """
    _motor_hat = Motor()    # Object controlling the motor hat
    _camera = Camera()      # Object controlling the camera
    _led = LED()            # Object controlling the led
    _piezo = Piezo()        # Object controlling the piezo
    _num_photos = 1         # Total to collect
    _taken = 0              # Number of current photo
    _position = 0           # Current position of the motor in degrees
    _is_running = False     # indicates if system is mid DAQ
    _is_error = False       # indicates system error
    _permit_edits = False   # indicates if datamembers are settable
    ms_count = 0
    gear_ratio = 4.9
    ms_end_pos = int(_motor_hat.microsteps * _motor_hat.step_res * gear_ratio)

    def __init__(self, path=None):
        """
        Initializes hardware states.
        """
        if path is not None:
            self._parse_config_file(path)    # pulls settings from prida.config
        else:
            self._parse_config_file()
        self._led.green_on()         # led indicates ready for DAQ
        self.motor._sm._build_microstep_curve()

    @property
    def motor(self):
        """
        Name:    Imaging.motor
        Feature: Return _motor_hat data member (getter)
        Inputs:  None
        Outputs: Motor, a Adafruit_MotorHAT abstraction (self._motor_hat)
        """
        return self._motor_hat

    @motor.setter
    def motor(self, value):
        """
        Name:    Imaging.motor
        Feature: _motor_hat attribute setter
        Inputs:  Motor, an Adafruit_MotorHAT abstraction (value)
        Outputs: None
        """
        if self._permit_edits is True:
            if isinstance(value, Motor()):
                self._motor_hat = value
            else:
                raise TypeError('_motor_hat attribute must be a Motor object.')
        else:
            raise Exception('Editing this field not permitted.')

    @property
    def camera(self):
        """
        Name:    Imaging.camera
        Feature: Return camera data member
        Inputs:  None
        Outputs: Camera, a gphoto2 camera object abstraction
                 (self._camera)
        """
        return self._camera

    @camera.setter
    def camera(self, value):
        """
        Name:    Imaging.camera
        Feature: _camera attribute setter
        Inputs:  Camera, an gphoto2 camera object abstraction (value)
        Outputs: None
        """
        if self._permit_edits is True:
            if isinstance(value, Camera()):
                self._camera = value
            else:
                raise TypeError('_camera attribute must be a Camera object.')
        else:
            raise Exception('Editing this field not permitted.')

    @property
    def led(self):
        """
        Name:    Imaging.led
        Feature: Return led data member
        Inputs:  None
        Outputs: LED, object representing a multi-colored LED that
                 inherits from the Adafruit_MotorHAT class (self._led)
        """
        return self._led

    @led.setter
    def led(self, value):
        """
        Name:    Imaging.led
        Feature: _led attribute setter
        Inputs:  LED, object representing a multi-colored LED that
                 inherits from the Adafruit_MotorHAT class (value)
        Outputs: None
        """
        if self._permit_edits is True:
            if isinstance(value, LED()):
                self._led = value
            else:
                raise TypeError('_led attribute must be a LED object.')
        else:
            raise Exception('Editing this field not permitted.')

    @property
    def piezo(self):
        """
        Name:    Imaging.piezo
        Feature: Return piezo data member
        Inputs:  None
        Outputs: Piezo, object representing a piezo-buzzer that inherits
                 from the Adafruit_MotorHAT class (self._piezo)
        """
        return self._piezo

    @piezo.setter
    def piezo(self, value):
        """
        Name:    Imaging.piezo
        Feature: _piezo attribute setter
        Inputs:  Piezo, object representing a piezo-buzzer that
                 inherits from the Adafruit_MotorHAT class (value)
        Outputs: None
        """
        if self._permit_edits is True:
            if isinstance(value, Piezo()):
                self._piezo = value
            else:
                raise TypeError('_piezo attribute must be a Piezo object.')
        else:
            raise Exception('Editing this field not permitted.')

    @property
    def is_running(self):
        """
        Name:    Imaging.is_running
        Feature: Return is_running data member
        Inputs:  None
        Outputs: Boolean, value representing system state, indicating
                 whether or not the system is currently in a imaging
                 sequence (self._is_running)
        """
        return self._is_running

    @property
    def is_error(self):
        """
        Name:    Imaging.is_error
        Feature: Return is_error data member
        Inputs:  None
        Outputs: Boolean, value representing system state, indicating
                 whether or not the hardware controls have encountered
                 an error (self._is_error)
        """
        return self._is_error

    @property
    def num_photos(self):
        """
        Name:    Imaging.num_photos
        Feature: Get the number of photos in a single expiriment.
        Inputs:  None
        Outputs: int, number of photos to be taken (self._num_photos)
        """
        return self._num_photos

    @property
    def taken(self):
        """
        Name:    Imaging.taken
        Feature: Get the number of photos in a single expiriment.
        Inputs:  None
        Outputs: int, number of photos that have been taken (self._taken)
        """
        return self._taken

    @is_running.setter
    def is_running(self, state):
        """
        Name:    Imaging.is_running
        Feature: Set is_running data member to boolean value.
        Inputs:  Boolean, desired system state (state)
        Outputs: None
        """
        if type(state) is bool and state is not self._is_running:
            self._is_running = state
        else:
            raise TypeError('_is_running must be a boolean.')

    @is_error.setter
    def is_error(self, state):
        """
        Name:    Imaging.is_error
        Feature: Set is_error data member to boolean value.
        Inputs:  Boolean, desired system state (state)
        Outputs: None
        """
        if type(state) is bool and state is not self._is_error:
            self._is_error = state
        else:
            raise TypeError('_is_error must be a boolean.')

    @num_photos.setter
    def num_photos(self, value):
        """
        Name:    Imaging.num_photos
        Feature: Set the number of photos in a single expiriment.
        Inputs:  int, number of photos to be taken (value)
        Outputs: None
        """
        if type(value) is int and value > 0:
            self._num_photos = value
        else:
            raise ValueError('Number of photos must be a positive ' +
                             'interger value.')

    @taken.setter
    def taken(self, value):
        """
        Name:    Imaging.taken
        Feature: Set the number of photos in a single expiriment.
        Inputs:  int, number of photos that have been taken (value)
        Outputs: None
        """
        if type(value) is int:
            if value >= 0:
                self._taken = value
            else:
                raise ValueError('Number of photos taken may not be negative.')
        else:
            raise TypeError('Number of photos taken must be an integer value.')

    @property
    def position(self):
        """
        Name:    Imaging.position
        Feature: Indicates the current position of the motor during the imaging
                 sequence.
        Inputs:  None
        Outputs: Float, current motor position in degrees
        """
        return self._position

    @position.setter
    def position(self, value):
        """
        Name:    Imaging.position
        Feature: Updates the current position of the motor during the imaging
                 sequence.
        Inputs:  Float, updated motor position value
        Outputs: None
        """
        if type(value) in {int, float}:
            if value >= 0:
                self._position = (value % 360)
            else:
                raise ValueError('Motor position cannot be a negative number.')
        else:
            raise TypeError('Motor position must be a number [degrees].')

    def reset_count(self):
        """
        Name:    Imaging.reset_count
        Feature: Resets the number of images taken counter
        Inputs:  None
        Outputs: None
        """
        self.taken = 0

    def reset_position(self):
        """
        Name:    Imaging.reset_position
        Feature: Resets the position indicators values
        Inputs:  None
        Outputs: None
        """
        self.position = 0
        self.ms_count = 0

    def reset(self):
        """
        Name:    Imaging.reset
        Feature: Resets the imaging hardware
        Inputs:  None
        Outputs: None
        """
        self.reset_count()
        self.reset_position()
        self.motor.turn_off_motors()
        self.check_status()

    def run_sequence(self):
        """
        Name:    Imaging.run_sequence
        Feature: Attempts to determine where in the sequence the system is,
                 and then either step the motor, take an image, or end the
                 sequence as appropriate.
        Inputs:  None
        Outputs: tuple(bool, string), is the returned filename a valid filename
                 and the filename of the image (is_filename, filename)
        """
        if self.is_error:
            try:
                self._check_error()
            except:
                traceback.print_exc()
                raise PRIDAPeripheralError(('Error not resolved! ' +
                                            'Please address and try again.'))
        if not self.is_running:
            self.motor.calibrate()
            self.is_running = True
        self.check_status()
        if ((self.taken < self.num_photos) and
            (self.ms_count < self.find_next_pos())):
            try:
                self.step_motor()
            except:
                traceback.print_exc()
                raise PRIDAPeripheralError('Motor Error!')
            else:
                return (False, '', '', '', '')
        elif ((self.taken < self.num_photos) and
              (self.ms_count >= self.find_next_pos())):
            try:
                filename = self.take_photo()
                str_args = filename.split('_')
            except:
                traceback.print_exc()
                raise PRIDAPeripheralError('Camera Error!')
            else:
                return (True, filename, str_args[0], str_args[1], str_args[2])
        else:
            if ((self.ms_count) < (self.ms_end_pos)):
                try:
                    self.step_motor()
                except:
                    traceback.print_exc()
                    raise PRIDAPeripheralError('Motor Error!')
                else:
                    return (False, '', '', '', '')
            else:
                self.end_sequence()
                return (False, '', '', '', '')

    def end_sequence(self):
        """
        Name:    Imaging.end_sequence
        Feature: Tries stopping the system.
                 If successful, turn off the motors and indicate ready
                 status. If it fails, indicate error status.
        Inputs:  None
        Outputs: None
        """
        if self.is_running:
            self.is_running = False
        try:
            self.reset()
            self.motor.turn_off_motors()
        except:
            traceback.print_exc()
            self.is_error = True
        finally:
            if not self.is_error:
                for i in range(3):
                    self.piezo.beep()
                    time.sleep(0.5)
            else:
                self.check_status()
                self.piezo.beep(3, 1, 1)
            self.check_status()

    def check_status(self):
        """
        Name:    Imaging.check_status
        Feature: Sets the LED status indicator to the appropriate color.
        Inputs:  None
        Outputs: None
        """
        if self.is_running:
            self.led.red_on()
        elif self.is_error:
            self.led.yellow_on()
        else:
            self.led.green_on()

    def _write_config(self, filepath=os.getcwd()):
        """
        Name:    Imaging._write_config
        Feature: Initializes a config file with current hardware settings.
                 PRIVATE FUNCTION DO NOT CALL
        Inputs:  String, desired absolute path to config file (filepath)
        Outputs: None
        """
        with open(os.path.join(filepath, 'prida.config'),
                  'w') as config_file:
            config = SafeConfigParser()
            attr_dict = {
                         'motor': [('rpm', str(self.motor.rpm)),
                                   ('degrees per unit step',
                                    str(self.motor.degrees)),
                                   ('microsteps per unit step',
                                    str(self.motor.microsteps)),
                                   ('port number',
                                    str(self.motor.motor_port_num))],
                         'camera': [('temporary image filepath',
                                     str(self.camera.path))],
                         'led': [('red pwm channel', str(self.led.red)),
                                 ('green pwm channel', str(self.led.green)),
                                 ('blue pwm channel', str(self.led.blue)),
                                 ('led type', str(self.led.led_type))],
                         'piezo': [('pwm channel', str(self.piezo.channel)),
                                   ('volume', str(self.piezo.volume))],
                         'imaging': []
            }
            for section in list(attr_dict.keys()):
                config.add_section(section)
                for attr, value in attr_dict[section]:
                    config.set(section, attr, value)
            config.write(config_file)

    def _parse_config_file(self, filepath=os.getcwd()):
        """
        Name:    Imaging._parse_config_file
        Feature: Sets attributes and member class attributes to values
                 as indicated by the PRIDA.CONFIG file. PRIVATE FUNCTION
                 DO NOT CALL
        Inputs:  String, absolute path to config file (filepath)
        Outputs: None
        """
        my_attrs = {'rpm': 'rpm',
                    'degrees per unit step': 'degrees',
                    'microsteps per unit step': 'microsteps',
                    'port number': 'motor_port_num',
                    'temporary image filepath': 'path',
                    'exposure time': 'exposure_time',
                    'red pwm channel': 'red',
                    'green pwm channel': 'green',
                    'blue pwm channel': 'blue',
                    'led type': 'led_type',
                    'pwm channel': 'channel',
                    'volume': 'volume',
                    }
        self.motor.permit_edits = True
        self.permit_edits = True
        config = SafeConfigParser()
        if os.path.isfile(os.path.join(filepath, 'prida.config')) is True:
            config.read(os.path.join(filepath, 'prida.config'))
            for section in config.sections():
                if section in {'motor', 'camera', 'piezo', 'led', 'imaging'}:
                    options = config.options(section)
                    for option in options:
                        if option in list(my_attrs.keys()):
                            if hasattr(self, section):
                                obj = getattr(self, section)
                                if hasattr(obj, my_attrs[option]):
                                    setattr(obj, my_attrs[option],
                                            self._parse_attr_type(
                                                config.get(section, option)))
                                else:
                                    raise AttributeError('Failed to access' +
                                                         ' "imaging.' +
                                                         section +
                                                         '.' +
                                                         my_attrs[option] +
                                                         '"')
                            else:
                                raise AttributeError('Failed to access' +
                                                     ' "imaging.' + section +
                                                     '"')
        self.motor.permit_edits = False
        self.permit_edits = False

    def _parse_attr_type(self, atr):
        """
        Name:    Imaging._parse_attr_type
        Feature: Takes a string and attempts to cast it as its intended
                 type. PRIVATE FUNCTION DO NOT CALL
        Inputs:  String, a string representing an object attribute value.
        Outputs: An object of type float, int, bool, str or None; the
                 intended attribute value. Does not handle dicts, lists,
                 longs, complex values, exceptions or other built in or
                 custom types not previously specified. (new_atr)
        """
        new_atr = None
        try:
            try:
                if '.' in atr:
                    new_atr = float(atr)
                else:
                    new_atr = int(atr)
            except ValueError:
                if atr in {'True', 'False'}:
                    new_atr = bool(atr)
                else:
                    new_atr = str(atr)
        except:
            print('Could not determine intended type!')
            new_atr = atr
        return new_atr

    def _check_error(self):
        """
        Name:    Imaging._check_error
        Feature: Checks if an error is still present in the system
        Inputs:  None
        Outputs: None
        """
        if self.is_error is True:
            try:
                self.motor.calibrate()
                self.motor.turn_off_motors()
            except:
                raise PRIDAPeripheralError('Motor problem!')
            try:
                self.camera.connect()
                self.camera.capture('test')
            except:
                raise PRIDAPeripheralError('Camera problem!')
            else:
                self.is_error = False
                self.check_status()

    def find_next_pos(self):
        """
        Name:    Imaging.find_next_pos
        Feature: Calculates the next position value at which the camera must
                 take a photo
        Inputs:  None
        Outputs: Int, position value in number of microsteps
        """
        pos_list = [i * (360.0 / self.num_photos)
                    for i in range(self.num_photos)]
        unit_step = 360.0 / self.motor.step_res
        for index in range(len(pos_list)):
            pos_b = pos_list[index]
            pos_m = (pos_b * self.gear_ratio)
            num_unit_steps = int((pos_m) / unit_step)
            min_angle = num_unit_steps * unit_step
            remaining_dist = pos_m - min_angle
            take_step = int((remaining_dist / unit_step) + 0.5)
            if take_step:
                pos_list[index] = int((num_unit_steps + 1)
                                      * self.motor.microsteps)
            else:
                pos_list[index] = int((num_unit_steps) * self.motor.microsteps)
        return pos_list[self.taken]

    def step_motor(self):
        """
        Name:    Imaging.step_motor
        Feature: Indexes the motor a single microstep,
                 updates the current position and number of microsteps.
        Inputs:  None
        Outputs: None
        """
        self.motor.single_step()
        self.position += (40.0 / 196.0) * (360.0 /
                         (self.motor.step_res * self.motor.microsteps))
        self.ms_count += 1

    def take_photo(self):
        """
        Name:    Imaging.take_photo
        Feature: Captures a photo, updates the number of photos taken and
                 returns the image filename.
        Inputs:  None
        Outputs: Str, captured image file name (filename)
        """
        name = '_'.join([datetime.datetime.now().strftime('%Y-%m-%d_%H.%M.%S'),
                         '%06.2f' % self.position])
        filename = self.camera.capture(name, verbose=False)
        self.taken += 1
        return filename