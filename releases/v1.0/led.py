#!/usr/bin/python
#
# led.py
#
# VERSION: 1.0.0
#
# LAST EDIT: 2015-10-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 6f881e8: twdavis - 2015-10-21 10:54:00
#     v0.7.0
# 4a1d374: Nathanael Shaw - 2015-10-01 16:24:45
#     update changelogs
# 36d81c1: Nathanael Shaw - 2015-10-01 16:23:21
#     version 0.6.1-dev
# 9f50386: Nathanael Shaw - 2015-09-30 14:19:10
#     update changelogs
# 42dd0fd: Nathanael Shaw - 2015-09-28 12:18:45
#     correct typo
# d42ec29: Nathanael Shaw - 2015-09-28 12:05:04
#     remove module level vars from scripts
# 9c28bfe: Nathanael Shaw - 2015-09-28 11:57:19
#     title hotfix
# b089bd6: Nathanael Shaw - 2015-09-28 11:36:52
#     add open-source license disclaimers
# 48c37c3: Nathanael Shaw - 2015-09-24 16:37:28
#     Update changelogs for new repo
# d978041: twdavis - 2015-09-22 13:22:25
#     Addresses #3. Python 2/3 compatability.
# 44a5255: Nathanael Shaw - 2015-09-21 17:38:57
#     Merge with periphdev
# 99e70b2: twdavis - 2015-09-21 16:34:31
#     added working
# $log_end_tag$
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
from time import sleep

from Adafruit_MotorHAT import Adafruit_MotorHAT
from pridaperipheral import PRIDAPeripheral
from pridaperipheral import PRIDAPeripheralError


###############################################################################
## CLASSES:
###############################################################################
class LED(Adafruit_MotorHAT, PRIDAPeripheral):
    """
    Class representing a tri-color (RGB) LED
    """

    def __init__(self):
        """
        Initialize PWM channels. Extends MotorHAT attributes and behaviours.
        """
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self._BLUE = 1         # Default channel for the blue LED
        self._GREEN = 14       # Default channel for the green LED
        self._RED = 15         # Default channel for the red LED
        self._led_type = 'CC'  # Default type Common Anode

    @property
    def blue(self):
        """
        Name:    LED.blue
        Feature: Returns the integer designation of the blue LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the blue LED (self._BLUE)
        """
        return self._BLUE

    @property
    def green(self):
        """
        Name:    LED.green
        Feature: Returns the integer designation of the green LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the green LED (self._GREEN)
        """
        return self._GREEN

    @property
    def red(self):
        """
        Name:    LED.red
        Feature: Returns the integer designation of the red LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the red LED (self._RED)
        """
        return self._RED

    @property
    def led_type(self):
        """
        Name:    LED.led_type
        Feature: Returns string representing multicolor LED
                 configuration
        Inputs:  None
        Outputs: String, two letter designation representing either
                 'Common Cathode' or 'Common Anode' (self._led_type)
        """
        return self._led_type

    @led_type.setter
    def led_type(self, led_type):
        """
        Name:    LED.led_type
        Feature: Set type to common anode or common cathode.
        Inputs:  String, two letter type designation (led_type)
        Outputs: None
        """
        if led_type == 'CA' or led_type == 'CC':
            self._led_type = led_type
        else:
            print("Bad input. Types limited to CC or CA.")

    @blue.setter
    def blue(self, ch):
        """
        Name:    LED.blue
        Feature: Set the PWM channel controlling the blue led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._BLUE = ch

    @green.setter
    def green(self, ch):
        """
        Name:    LED.green
        Feature: Set the PWM channel controlling the green led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._GREEN = ch

    @red.setter
    def red(self, ch):
        """
        Name:    LED.red
        Feature: Set the PWM channel controlling the red led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._RED = ch

    def led_off(self, channel):
        """
        Name:    LED.led_off
        Feature: Set low a specifed led channel.
        Inputs:  int, the number of the channel to lower (channel)
        Outputs: None
        """
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 4096, 0)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 0, 4096)

    def led_on(self, channel):
        """
        Name:    LED.led_on
        Feature: Set high a specified led channel.
        Inputs:  int, the number of the channel to raise (channel)
        Outputs: None
        """
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 0, 4096)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 4096, 0)

    def red_on(self):
        """
        Name:    LED.red_on
        Feature: Set high the 'RED' PWM channel, default 15.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self._RED)
        self.led_off(self._BLUE)
        self.led_off(self._GREEN)

    def blue_on(self):
        """
        Name:    LED.blue_on
        Feature: Set high the 'BLUE' PWM channel, default 1.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self._BLUE)
        self.led_off(self._RED)
        self.led_off(self._GREEN)

    def green_on(self):
        """
        Name:    LED.green_on
        Feature: Set high the 'GREEN' PWM channel, default 14.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self._GREEN)
        self.led_off(self._RED)
        self.led_off(self._BLUE)

    def yellow_on(self):
        """
        Name:    LED.yellow_on
        Feature: Set led color to yellow by combining green and red
                 light using default channels.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self._RED)
        self.led_on(self._GREEN)
        self.led_off(self._BLUE)

    def red_off(self):
        """
        Name:    LED.red_off
        Feature: Set low the 'red' PWM channel, default 15.
        Inputs:  None
        Outputs: None
        """
        self.led_off(self._RED)

    def blue_off(self):
        """
        Name:    LED.blue_off
        Feature: Set low the 'blue' PWM channel, default 1.
        Inputs:  None
        Outputs: None
        """
        self.led_off(self._BLUE)

    def green_off(self):
        """
        Name:    LED.green_off
        Feature: Set low the 'green' PWM channel, default 14.
        Inputs:  None
        Outputs: None
        """
        self.led_off(self._GREEN)

    def yellow_off(self):
        """
        Name:    LED.yellow_off
        Feature: Set low the red and green PWM channels, removing yellow
                 light.
        Inputs:  None
        Outputs: None
        """
        self.red_off()
        self.green_off()

    def all_off(self):
        """
        Name:    LED.all_off
        Feature: Set all led PWM channels to low.
        Inputs:  None
        Outputs: None
        """
        self.blue_off()
        self.green_off()
        self.red_off()

    def all_on(self):
        """
        Name:    LED.all_on
        Feature: Set all led PWM channels to high.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self._BLUE)
        self.led_on(self._GREEN)
        self.led_on(self._RED)

    def connect(self):
        try:
            self.all_on()
            sleep(1)
            self.all_off()
        except:
            raise PRIDAPeripheralError('Could not connect to LED!')

    def poweroff(self):
        self.all_off()

    def current_status(self):
        super(self.current_status())

