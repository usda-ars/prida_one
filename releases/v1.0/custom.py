#!/usr/bin/python
#
# custom.py
#
# VERSION: 1.0.0-r2
#
# LAST EDIT: 2016-03-27
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import os.path
import sys

import numpy
import scipy.misc
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPainter
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QColor
import PIL.ImageQt as ImageQt
import PIL.Image as Image


###############################################################################
# FUNCTIONS:
###############################################################################
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = sys.path[0]

    return os.path.join(base_path, relative_path)


###############################################################################
# CLASSES:
###############################################################################
class InteractiveLabel(QLabel):
    """
    Name:     InteractiveLabel
    Features: Click and drag and selection widget
    History:  Version 1.0.0
              - added image filename as default init parameter [15.10.16]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, stuff, ifile='greeter.jpg'):
        """
        Name:     InteractiveLabel.__init__
        Inputs:   - QtWidgets.QWidget, (stuff)
                  - str, greeter image file (ifile)
        Features: Initializes an InteractiveLabel.
                  * press_pos is the stored mouse press location
                  * clean_img is last image before mouse press
                  * base_img is the original image
                  * active_img is the image currently being used
                  * is_pressed indicates mouse pressed status
        """
        QLabel.__init__(self)
        self.press_pos = None
        self.greeter = resource_path(ifile)
        self.clean_img = QPixmap(self.greeter).scaledToHeight(300)
        self.base_img = QPixmap(self.greeter).scaledToHeight(300)
        self.active_img = None
        self.is_pressed = False
        self.painter = QPainter()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def mousePressEvent(self, event):
        """
        Name:     InteractiveLabel.mousePressEvent
        Input:    (event)
        Output:   None.
        Features: On left mouse click, the mouse location is saved and the
                  pressed status is set to true
        """
        if event.button() == Qt.LeftButton:
            self.press_pos = event.pos()
            self.is_pressed = True

    def mouseMoveEvent(self, event):
        """
        Name:     InteractiveLabel.mouseMoveEvent
        Input:    (event)
        Output:   None.
        Features: While mouse click is held down, reset the active image as the
                  clean image, then draw the new image and set the active image
                  to the label
        """
        if self.is_pressed:
            self.active_img = self.clean_img.copy()
            self.painter.begin(self.active_img)
            #self.painter.setPen(Qt.blue)
            self.painter.fillRect(self.press_pos.x(),
                                  self.press_pos.y(),
                                  event.x()-self.press_pos.x(),
                                  event.y()-self.press_pos.y(),
                                  QColor(128, 128, 255, 128))
            self.painter.end()
            self.setPixmap(self.active_img)

    def mouseReleaseEvent(self, event):
        """
        Name:     InteractiveLabel.mouseReleaseEvent
        Features: When left mouse click is released, set the active image as
                  the new clean image. When right mouse click is released, set
                  the shown and clean image to the base image.
        """
        if event.button() == Qt.LeftButton:
            print("Start: " + str(self.press_pos))
            print("End: " + str(event.pos()))
            if self.is_pressed:
                if (event.x() > self.pixmap().size().width() or
                        event.y() > self.pixmap().size().height() or
                        event.x() < 0 or event.y() < 0):
                    self.setPixmap(self.clean_img)
                else:
                    self.clean_img = self.active_img
                self.is_pressed = False
        else:
            self.setPixmap(self.base_img)
            self.clean_img = self.base_img


class IDA(QLabel):
    """
    Name:     IDA (Image Display Area)
    Features: The label that displays the photos in full size
    History:  Version 1.0.0-r2
              - addressed portrait images in resizeAndSet [15.10.16]
              - updated scaling method [16.03.27]
              - updated memory handling [16.03.27]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None):
        """
        Name:     IDA.__init__
        Inputs:   parent object (parent)
        Outputs:  None.
        Features: Initializes IDA
        """
        super(IDA, self).__init__(parent)

        self.base_array = None  # numpy array of original color image
        self.base_img = None    # QPixmap
        self.cur_image = None   # ImageQt

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def clearBaseImage(self, text=""):
        """
        Name:     IDA.clearBaseImage
        Inputs:   None.
        Outputs:  None.
        Features: Clears the IDA base image and all labels
        """
        self.clear()
        self.base_array = None
        self.base_img = None
        self.cur_image = None
        self.setText(text)

    def resizeAndSet(self, img_array):
        """
        Name:     IDA.resizeAndSet
        Inputs:   numpy.ndarray, array for resizing and setting (img_array)
        Outputs:  None.
        Features: Resizes and sets ImageQt from base image
        """
        img_dim = len(img_array.shape)
        if img_dim == 3:
            try:
                h, w, _ = img_array.shape
            except:
                raise
        elif img_dim == 2:
            try:
                h, w = img_array.shape
            except:
                raise
        else:
            raise ValueError("Unsupported image dimension %d" % (img_dim))

        # Calculate the required size of image:
        s_factor = numpy.array(
            [float(self.height())/h, float(self.width())/w]).min()
        nd_array = scipy.misc.imresize(img_array, s_factor, "bilinear")

        # Add binary image support:
        if nd_array.max() == 1 and len(nd_array.shape) == 2:
            nd_array[numpy.where(nd_array == 1)] *= 255

        image = Image.fromarray(nd_array)
        qt_image = ImageQt.ImageQt(image)
        self.cur_image = qt_image
        self.base_img = QPixmap.fromImage(qt_image)
        self.setPixmap(self.base_img)

    def resizeEvent(self, e):
        """
        Name:     IDA.resizeEvent
        Inputs:   event (e)
        Outputs:  None.
        Features: Upon resize events, resize and set image
        Depends:  resizeAndSet
        """
        if self.base_array is not None:
            self.resizeAndSet(self.base_array)

    def setBaseImage(self, ndarray_full):
        """
        Name:     IDA.setBaseImage
        Inputs:   numpy.ndarray, image (ndarray_full)
        Outputs:  None.
        Features: Sets and saves base image for viewing
        Depends:  resizeAndSet
        """
        self.base_array = ndarray_full
        self.resizeAndSet(self.base_array)
