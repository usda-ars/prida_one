#!/usr/bin/python
#
# Prida.py
#
# VERSION: 1.0.0-r1
#
# LAST EDIT: 2016-03-15
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import sys
import os
import glob

import numpy
import PyQt5.uic as uic
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QAbstractItemView
from PyQt5.QtWidgets import QCompleter
# from PyQt5.QtWidgets import QErrorMessage # @TODO
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QStringListModel
from PyQt5.QtCore import QThread
from PyQt5.QtCore import QSize
from PyQt5.QtCore import QDate
from PyQt5.QtCore import Qt     # used in set_input_field (as string)
import PIL.ImageQt as ImageQt
import PIL.Image as Image
import PIL.ExifTags as ExifTags

from hdf_organizer import PridaHDF

try:
    from imaging import Imaging
    hardware_mode = True
except ImportError:
    # MODE 1 INDICATES ERROR LIKELY FROM NONEXISTENCE OF HARDWARE
    print("Imaging hardware not set up")
    hardware_mode = False

###############################################################################
# GLOBAL VARIABLES:
###############################################################################
# PRIDA VERSION
PRIDA_VERSION = 'Prida 1.0.0-r1'
PRIDA_VERSION_EXPL = PRIDA_VERSION + ' - Explorer Mode'

# PAGE NUMBER ENUMS FOR mainwindow.ui
MAIN_MENU = 0
VIEWER = 1
INPUT_EXP = 2
RUN_EXP = 3
FILE_SEARCH = 4

# PAGE NUMBER ENUMS FOR SHEET/IDA
SHEET_VIEW = 0
IDA_VIEW = 1

# FILE & SESSION PATHS FOR IMAGES AND HDF FILE
filepath = ''
session_path = ''
is_path = False


###############################################################################
# FUNCTIONS:
###############################################################################
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = sys.path[0]

    return os.path.join(base_path, relative_path)


###############################################################################
# CLASSES:
###############################################################################
class HardwareThread(QThread):
    """
    Name:     HardwareThread
    Features: Thread for running hardware
    History:  Version 1.0.0-r1
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def run(self):
        """
        Name:     HardwareThread.run
        Inputs:   None.
        Outputs:  None.
        Features: Run sequence and save image to HDF file
        """
        global filepath, is_path
        if hardware_mode:
            output_args = imaging.run_sequence()
            is_path = output_args[0]
            filepath = output_args[1]
            datestamp = output_args[2]
            datestamp = os.path.basename(datestamp)
            timestamp = output_args[3]
            anglestamp = output_args[4]
            anglestamp = os.path.splitext(anglestamp)[0]
            basename = os.path.basename(filepath)
            name = os.path.splitext(basename)[0]
            a_path = os.path.join(session_path, name, basename)
            if is_path:
                hdf.save_image(session_path, filepath)
                hdf.set_attr('Date', datestamp, a_path)
                hdf.set_attr('Time', timestamp, a_path)
                hdf.set_attr('Angle', anglestamp, a_path)
                hdf.set_attr('Orientation', imaging.camera.orientation, a_path)
                hdf.set_attr('Height', imaging.camera.image_height, a_path)
                hdf.set_attr('Width', imaging.camera.image_width, a_path)
                hdf.save()


class Prida(QApplication):
    """
    Name:      Prida
    Features:  Prida main class
    History:   Version 1.0.0-r2
               -
               Version 1.0.0-r1
               - cleaned up exec commands [16.03.15]
               - cleaned up PEP8 errors [16.03.15]
               - addressed single quote meta data errors [16.03.15]
               Version 1.0.0
               - added new meta data field getters for INPUT_EXP [15.09.24]
               - set default field values in INPUT_EXP [15.09.24]
               - added QtCore.QDate for setting session date [15.09.24]
               - set session user and addr for new/open HDF5 file [15.09.24]
               - moved clear session & image sheets to back-to-menu [15.09.24]
               - fixed merge error with hardware thread spawning [15.09.29]
               - added additional fields to session sheet model [15.09.30]
               - abstracted pid_attrs and session_attrs [15.09.30]
               - updated new_session using new dictionaries [15.09.30]
               - added sheet_items variable [15.09.30]
               - updated get_data to search over known keys [15.10.01]
               - solved problem with missing meta data displaying [15.10.01]
               - created a set-defaults function [15.10.01]
               - added check for reseting IDA view for new/open file [15.10.01]
               - pushButton renamed menuButton (mainwindow.ui) [15.10.13]
               - added about ui [15.10.13]
               - created about function [15.10.14]
               - created export function [15.10.14]
               - added button disabling for hardware mode [15.10.15]
               - created FILE_SEARCH view [15.10.15]
               - created file_browse_sheet [15.10.15]
               - created file_search_sheet [15.10.15]
               - created add_search_file & rm_search_file functions [15.10.15]
               - linked FILE_SEARCH explorer button [15.10.16]
               - created import_data function [15.10.16]
               - created import_session function [15.10.16]
               - added error handling on exif tag import [15.10.19]
               - removed image meta data from session [15.10.21]
               - updated import_data exif tag handling [15.10.21]
               - addresses indexError in update session sheet model [15.10.21]
               - added editing boolean class variable [15.10.21]
               - created edit field function [15.10.21]
               - created disable pid/session attrs functions [15.10.21]
               - created cancel input function [15.10.21]
               - updated pid and session attr dicts [15.10.22]
               - created edit feature for PID & session attrs [15.10.22]
               - added QStringListModel and QCompleter modules [15.10.22]
               - created a auto-completion for PIDs [15.10.22]
               - updated Exif tag handling in import_data [15.10.28]
               - clear search bar when adding/removing search files [15.10.29]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    pid_attrs = {
        1: {'title': 'Genus Species',
            'key': 'gen_sp',
            'qt_val': 'c_gen_sp',
            'has_default': False,
            'qt_type': 'QComboBox'},
        2: {'title': 'Line',
            'key': 'line',
            'qt_val': 'c_line',
            'has_default': False,
            'qt_type': 'QLineEdit'},
        3: {'title': 'Rep Num',
            'key': 'rep_num',
            'qt_val': 'c_rep_num',
            'has_default': False,
            'qt_type': 'QSpinBox'},
        4: {'title': 'Tub ID',
            'key': 'tubid',
            'qt_val': 'c_tubid',
            'has_default': False,
            'qt_type': 'QLineEdit'},
        5: {'title': 'Germination Date',
            'key': 'germdate',
            'qt_val': 'c_germdate',
            'has_default': False,
            'qt_type': 'QDateEdit'},
        6: {'title': 'Transplant Date',
            'key': 'transdate',
            'qt_val': 'c_transdate',
            'has_default': False,
            'qt_type': 'QDateEdit'},
        7: {'title': 'Treatment',
            'key': 'treatment',
            'qt_val': 'c_treatment',
            'has_default': True,
            'def_val': 'Control',
            'qt_type': 'QLineEdit'},
        8: {'title': 'Media',
            'key': 'media',
            'qt_val': 'c_media',
            'has_default': False,
            'qt_type': 'QLineEdit'},
        9: {'title': 'Tub Size',
            'key': 'tubsize',
            'qt_val': 'c_tubsize',
            'has_default': False,
            'qt_type': 'QLineEdit'},
        10: {'title': 'Nutrient',
             'key': 'nutrient',
             'qt_val': 'c_nutrient',
             'has_default': False,
             'qt_type': 'QLineEdit'},
        11: {'title': 'Growth Temp (Day)',
             'key': 'growth_temp_day',
             'qt_val': 'c_growth_temp_day',
             'has_default': False,
             'qt_type': 'QLineEdit'},
        12: {'title': 'Growth Temp (Night)',
             'key': 'growth_temp_night',
             'qt_val': 'c_growth_temp_night',
             'has_default': False,
             'qt_type': 'QLineEdit'},
        13: {'title': 'Lighting Conditions',
             'key': 'growth_light',
             'qt_val': 'c_growth_light',
             'has_default': False,
             'qt_type': 'QLineEdit'},
        14: {'title': 'Watering schedule',
             'key': 'water_sched',
             'qt_val': 'c_water_sched',
             'has_default': False,
             'qt_type': 'QLineEdit'},
        15: {'title': 'Damage',
             'key': 'damage',
             'qt_val': 'c_damage',
             'has_default': True,
             'def_val': 'No',
             'qt_type': 'QLineEdit'},
        16: {'title': 'Plant Notes',
             'key': 'notes',
             'qt_val': 'c_plant_notes',
             'has_default': True,
             'def_val': 'N/A',
             'qt_type': 'QLineEdit'}
    }
    sheet_items = len(pid_attrs)

    session_attrs = {
        0: {'title':  'Session User',
            'key': 'user',
            'qt_val': 'c_session_user',
            'has_default': False,
            'qt_type': 'QLineEdit'},
        1: {'title': 'Session Email',
            'key': 'addr',
            'qt_val': 'c_session_addr',
            'has_default': False,
            'qt_type': 'QLineEdit'},
        2: {'title': 'Session Title',
            'key': 'number',
            'qt_val': 'c_session_number',
            'has_default': False,
            'qt_type': 'QLineEdit'},
        3: {'title': 'Session Date',
            'key': 'date',
            'qt_val': 'c_session_date',
            'has_default': True,
            'def_val': '',
            'qt_type': 'QDateEdit'},
        4: {'title': 'Rig Name',
            'key': 'rig',
            'qt_val': 'c_session_rig',
            'has_default': False,
            'qt_type': 'QLineEdit'},
        5: {'title': 'Image Count',
            'key': 'num_img',
            'qt_val': 'c_num_images',
            'has_default': False,
            'qt_type': 'QLineEdit'},
        6: {'title': 'Plant Age',
            'key': 'age_num',
            'qt_val': 'c_plant_age',
            'has_default': False,
            'qt_type': 'QLineEdit'},
        7: {'title': 'Notes',
            'key': 'notes',
            'qt_val': 'c_session_notes',
            'has_default': True,
            'def_val': 'N/A',
            'qt_type': 'QLineEdit'},
        8: {'title': 'Camera Make',
            'key': 'cam_make',
            'qt_val': 'c_cam_make',
            'has_default': True,
            'def_val': '',
            'qt_type': 'QLineEdit'},
        9: {'title': 'Camera Model',
            'key': 'cam_model',
            'qt_val': 'c_cam_model',
            'has_default': True,
            'def_val': '',
            'qt_type': 'QLineEdit'},
        10: {'title': 'Exposure Time',
             'key': 'cam_shutter',
             'qt_val': 'c_cam_shutter',
             'has_default': True,
             'def_val': '',
             'qt_type': 'QLineEdit'},
        11: {'title': 'Aperture',
             'key': 'cam_aperture',
             'qt_val': 'c_cam_aperture',
             'has_default': True,
             'def_val': '',
             'qt_type': 'QLineEdit'},
        12: {'title': 'ISO',
             'key': 'cam_exposure',
             'qt_val': 'c_cam_exposure',
             'has_default': True,
             'def_val': '',
             'qt_type': 'QLineEdit'},
    }
    session_sheet_items = len(session_attrs)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     Prida.__init__
        Inputs:   None.
        Outputs:  None.
        Depends:  - load_session_sheet_headers     - new_hdf
                  - new_session                    - open_hdf
                  - resize_sheet                   - show_ida
                  - to_sheet                       - update_progress
                  - update_session_sheet_model     - update_sheet_model
                  - set_defaults                   - about
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # Check that the necessary files exist:
        # NOTE: mainwindow.ui depends on class definitions from custom.py
        self.main_ui = resource_path('mainwindow.ui')
        self.user_ui = resource_path('input_user.ui')
        self.about_ui = resource_path('about.ui')
        self.gs_dict = resource_path('dictionary.txt')
        self.greeter = resource_path('greeter.jpg')

        if (not os.path.isfile(self.main_ui) or
                not os.path.isfile(self.user_ui) or
                not os.path.isfile(self.about_ui)):
            raise IOError("Error! Missing required ui files!")
        if not os.path.isfile(self.gs_dict):
            raise IOError("Error! Missing dictionary file!")
        if not os.path.isfile(self.greeter):
            raise IOError("Error! Missing welcome screen image!")

        # Load main window GUI to the QMainWindow:
        self.view = uic.loadUi(self.main_ui, self.base)

        # Set the welcome image:
        self.view.welcome.setPixmap(QPixmap(self.greeter).scaledToHeight(300))

        # Create the VIEWER sheet model (where PIDs and sessions are listed)
        self.sheet_model = QStandardItemModel()
        self.view.sheet.setModel(self.sheet_model)
        self.view.sheet.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # Maximize/minimize window functionality (?)
        self.view.sheet.collapsed.connect(lambda: self.resize_sheet())
        self.view.sheet.expanded.connect(lambda: self.resize_sheet())
        self.view.sheet.setAnimated(True)

        # Create the VIEWER session sheet model (for session properties)
        self.session_sheet_model = QStandardItemModel()
        self.view.session_sheet.setModel(self.session_sheet_model)
        self.view.session_sheet.setRootIsDecorated(False)
        self.load_session_sheet_headers()

        # Create FILE_SEARCH sheet model (where files + PIDs are listed)
        self.search_sheet_model = QStandardItemModel()
        self.view.file_search_sheet.setModel(self.search_sheet_model)
        self.load_file_search_sheet_headers()

        # Create the FILE_SEARCH file browse model (for displaying files)
        self.file_sheet_model = QStandardItemModel()
        self.view.file_browse_sheet.setModel(self.file_sheet_model)
        self.load_file_browse_sheet_headers()

        # Populate INPUT_EXP Genus Species dropdown menu:
        for line in open(self.gs_dict).read().splitlines():
            self.view.c_gen_sp.addItem(line)

        if hardware_mode:
            # Hardware Mode Decorator:
            self.base.setWindowTitle(PRIDA_VERSION)

            # Spawn a hardware thread if mode is enabled:
            self.hardware_thread = HardwareThread()
            self.hardware_thread.finished.connect(self.update_progress)

            # Disable "Import Data" button (only allow "Create Session")
            # and set VIEWER action for "OK" and "Create Session" buttons:
            self.view.import_data.setEnabled(False)
            self.view.c_buttons.accepted.connect(self.new_session)
            self.view.create_session.clicked.connect(
                lambda: self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
            )

            # Set default values based on imaging's camera
            self.session_attrs[8]['def_val'] = imaging.camera.make
            self.session_attrs[9]['def_val'] = imaging.camera.model
            self.session_attrs[10]['def_val'] = imaging.camera.exposure_time
            self.session_attrs[11]['def_val'] = imaging.camera.aperture
            self.session_attrs[12]['def_val'] = imaging.camera.iso_speed
        else:
            # Explorer Mode Decorator:
            self.base.setWindowTitle(PRIDA_VERSION_EXPL)

            # Disable "Create Session" button, set VIEWER action for
            # "Import Data" button & INPUT_EXP action for "OK" button:
            self.view.create_session.setEnabled(False)
            self.view.import_data.clicked.connect(self.import_data)
            self.view.c_buttons.accepted.connect(self.import_session)

        # Set MAIN_MENU actions for "New," "Open," & "Search" buttons:
        self.view.new_hdf.clicked.connect(self.new_hdf)
        self.view.open_hdf.clicked.connect(self.open_hdf)
        self.view.search_hdf.clicked.connect(
            lambda: self.view.stackedWidget.setCurrentIndex(FILE_SEARCH)
        )

        # Set VIEWER actions for "Export Data," "Main Menu," "About," "Edit,"
        # and "Back to Spreadsheet" (IDA) buttons and for edited search text;
        # also disable "Perform Analysis" button until further notice:
        self.view.export_data.clicked.connect(self.export)
        self.view.menuButton.clicked.connect(self.back_to_menu)
        self.view.aboutButton.clicked.connect(self.about)
        self.view.make_edit.clicked.connect(self.edit_field)
        self.view.to_sheet.clicked.connect(self.to_sheet)
        self.view.search.textEdited.connect(self.update_sheet_model)
        self.view.perform_analysis.setEnabled(False)

        # Set INPUT_EXP action for "Cancel" button:
        self.view.c_buttons.rejected.connect(self.cancel_input)

        # Create VIEWER image sheet model (for thumbnail previewer):
        self.img_sheet_model = QStandardItemModel()
        self.view.img_select.setModel(self.img_sheet_model)
        self.view.img_select.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.img_select.setIconSize(QSize(100, 100))

        # Create selection models for VIEWER sheet and img_sheet and for
        # FILE_SELECT file_browser_sheet:
        self.sheet_select_model = self.view.sheet.selectionModel()
        self.img_sheet_select_model = self.view.img_select.selectionModel()
        self.file_select_model = self.view.file_browse_sheet.selectionModel()

        # Set selection model actions for selection changes:
        self.img_sheet_select_model.selectionChanged.connect(self.show_ida)
        self.sheet_select_model.selectionChanged.connect(
            self.update_session_sheet_model
        )

        # Set FILE_SEARCH actions for "Main Menu," "+/-," and "Explore" buttons
        # and action for when search text is edited:
        self.view.menuButton2.clicked.connect(self.back_to_menu)
        self.view.add_file.clicked.connect(self.add_search_file)
        self.view.rm_file.clicked.connect(self.rm_search_file)
        self.view.explore_file.clicked.connect(self.explore_file)
        self.view.f_search.textEdited.connect(self.update_file_search_sheet)

        # Create a auto-completion model for PIDs:
        self.pid_completer = QCompleter()
        self.view.c_pid.setCompleter(self.pid_completer)
        self.pid_auto_model = QStringListModel()
        self.pid_completer.setModel(self.pid_auto_model)

        # Initialize the search dictionaries, list of PID metadata, list of
        # thumbnail images, session create/edit boolean, input field defaults
        # and current image:
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.img_list = []
        self.img_types = (".jpg", ".JPG", ".jpeg", ".tif", ".tiff")
        self.editing = False
        self.set_defaults()
        self.current_img = None        # not currently used

        self.base.show()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def about(self):
        """
        Name:     Prida.about
        Inputs:   None.
        Outputs:  None.
        Features: Produces the file popup dialog box
        """
        popup = uic.loadUi(self.about_ui)
        overview = hdf.get_about()

        # Set popup values to default string if get_about fails:
        popup.author_about.setText(overview.get('Author', 'Unknown'))
        popup.contact_about.setText(overview.get('Contact', 'Unknown'))
        popup.plants_about.setText(overview.get('Plants', '?'))
        popup.sessions_about.setText(overview.get('Sessions', '?'))
        popup.photos_about.setText(overview.get('Photos', '?'))
        popup.summary_about.setText(overview.get('Summary', '?'))
        if popup.exec_():
            popup.repaint()

    def add_search_file(self):
        """
        Name:     Prida.add_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Adds user-defined HDF5 file to search files dictionary and
                  saves the file's PID attributes to search data dictionary
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        path = QFileDialog.getOpenFileName(filter='*.hdf5')[0]
        if path != '':
            f_name = os.path.basename(path)
            f_name = os.path.splitext(f_name)[0]
            f_path = os.path.dirname(path)
            if f_name not in self.search_files:
                # Add file to file_sheet_model:
                self.search_files[f_name] = f_path
                f_list = [QStandardItem(f_name), QStandardItem(f_path)]
                self.file_sheet_model.appendRow(f_list)

                # Add file's data to search_sheet_model:
                if hdf.isopen:
                    hdf.close()
                hdf.open_file(path)
                for pid in hdf.list_pids():
                    my_key = "%s.%s" % (f_name, pid)
                    my_data = [f_name, pid]
                    for i in self.pid_attrs.keys():
                        my_attr = self.pid_attrs[i]['key']
                        my_attr_val = hdf.get_attr(my_attr,
                                                   os.path.join('/', str(pid)))
                        my_data.append(my_attr_val)
                    self.search_data[my_key] = my_data
                hdf.close()
            self.view.f_search.clear()
            self.update_file_search_sheet()
            self.update_file_browse_sheet()

    def back_to_menu(self):
        """
        Name:     Prida.back_to_menu
        Inputs:   None
        Outputs:  None
        Features: Returns to the menu screen, closing the HDF5 file handle and
                  clearing session and sheet data.
        Depends:  - clear_session_sheet_model
                  - update_file_browse_sheet
                  - update_file_search_sheet
                  - update_sheet_model
        """
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.editing = False
        self.update_sheet_model()
        self.update_file_browse_sheet()
        self.update_file_search_sheet()
        self.view.stackedWidget.setCurrentIndex(MAIN_MENU)
        hdf.close()

    def back_to_sheet(self):
        """
        Name:     Prida.back_to_sheet
        Inputs:   None
        Outputs:  None
        Features: Returns the current view to VIEWER cleaning up all the sheets
        Depends:  - clear_session_sheet_model
                  - update_sheet_model
        """
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.update_sheet_model()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def cancel_input(self):
        """
        Name:     Prida.cancel_input
        Inputs:   None
        Outpus:   None
        Features: Exits INPUT_EXP, resetting editing boolean, enabling all
                  QLineEdit fields, and returns to VIEWER
        Depends:  enable_all_attrs
        """
        self.editing = False
        self.enable_all_attrs()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def clear_session_sheet_model(self):
        """
        Name:     Prida.clear_session_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Clears session sheet model
        Depends:  resize_session_sheet
        """
        for i in range(self.session_sheet_items):
            self.session_sheet_model.setItem(i, 1, QStandardItem(''))
        #
        self.resize_session_sheet()

    def disable_pid_attrs(self):
        """
        Name:     Prida.disable_pid_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with PID attributes
        """
        if self.editing:
            self.view.c_pid.setEnabled(False)
            self.view.c_num_images.setEnabled(False)
            for k in self.pid_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.pid_attrs[k]['qt_val'])
                )

    def disable_session_attrs(self):
        """
        Name:     Prida.disable_session_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with session attributes
        """
        if self.editing:
            self.view.c_pid.setEnabled(False)
            for k in self.session_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.session_attrs[k]['qt_val'])
                )

    def edit_field(self):
        """
        Name:     Prida.edit_field
        Inputs:   None
        Outputs:  None
        Features: Performs INPUT_EXP field disabling based on the selection in
                  the sheet model, applies appropriate meta data to INPUT_EXP
                  fields, sets editing boolean to True and changes current
                  view to INPUT_EXP
        Depends:  - disable_pid_attrs
                  - disable_session_attrs
                  - set_defaults
                  - set_input_field
        """
        if self.sheet_select_model.hasSelection():
            self.editing = True
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session, disable PID attrs:
                self.disable_pid_attrs()

                # Get session attrs & set them to INPUT_EXP:
                pid = my_selection.parent().text()
                session = my_selection.text()
                p_path = "/%s" % (pid)
                s_path = "/%s/%s" % (pid, session)

                self.set_input_field('c_pid', 'QLineEdit', str(pid))
                for j in self.pid_attrs:
                    f_name = self.pid_attrs[j]['qt_val']
                    f_type = self.pid_attrs[j]['qt_type']
                    f_value = hdf.get_attr(self.pid_attrs[j]['key'], p_path)
                    self.set_input_field(f_name, f_type, f_value)

                for k in self.session_attrs:
                    f_name = self.session_attrs[k]['qt_val']
                    f_type = self.session_attrs[k]['qt_type']
                    f_value = hdf.get_attr(
                        self.session_attrs[k]['key'], s_path)
                    self.set_input_field(f_name, f_type, f_value)
            else:
                # Selected PID, disable session attrs:
                self.disable_session_attrs()

                # Get PID attrs & set them to INPUT_EXP:
                pid = my_selection.text()
                s_path = "/%s" % (pid)

                self.set_defaults()
                self.set_input_field('c_pid', 'QLineEdit', str(pid))
                for k in self.pid_attrs:
                    f_name = self.pid_attrs[k]['qt_val']
                    f_type = self.pid_attrs[k]['qt_type']
                    f_value = hdf.get_attr(self.pid_attrs[k]['key'], s_path)
                    self.set_input_field(f_name, f_type, f_value)
            self.view.stackedWidget.setCurrentIndex(INPUT_EXP)

    def enable_all_attrs(self):
        """
        Name:     Prida.enable_all_attrs
        Inputs:   None
        Outputs:  None
        Features: Enables all INPUT_EXP fields
        """
        self.view.c_pid.setEnabled(True)
        for k in self.pid_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.pid_attrs[k]['qt_val'])
            )

        for j in self.session_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.session_attrs[j]['qt_val'])
            )

    def explore_file(self):
        """
        Name:     Prida.explore_file
        Inputs:   None.
        Outputs:  None.
        Features: Opens the sheet VIEW with the currently selected file from
                  the search file browser
        Depends:  - get_data
                  - update_sheet_model
        """
        if self.file_select_model.hasSelection():
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0]
            )
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)
            if hdf.isopen:
                hdf.close()
            hdf.open_file(f_path)
            self.get_data()
            self.update_sheet_model()
            if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                    self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
            self.view.stackedWidget.setCurrentIndex(VIEWER)

            # Set session user and contact defaults:
            self.view.c_session_user.setText(str(hdf.get_root_user()))
            self.view.c_session_addr.setText(str(hdf.get_root_addr()))

    def export(self):
        """
        Name:     Prida.export
        Inputs:   None.
        Outputs:  None.
        Features: Exports the images from a given selection to a user-defined
                  output directory recreating the HDF5 folder structure, else
                  exports all images and meta data
        Depends:  get_sheet_model_selection
        """
        if self.sheet_select_model.hasSelection():
            s_path = self.get_sheet_model_selection()
        else:
            s_path = '/'

        path = QFileDialog.getExistingDirectory(None,
                                                'Select an output directory:',
                                                os.path.expanduser("~"),
                                                QFileDialog.ShowDirsOnly)
        if os.path.isdir(path):
            try:
                hdf.extract_attrs(path)
            except IOError as e:
                attr_error_msg = "I/O Error(%s): %s" % (e.errno, e.strerror)
                print(attr_error_msg)

            try:
                hdf.extract_datasets(s_path, path)
            except IOError as e:
                dset_error_msg = "I/O Error(%s): %s" % (e.errno, e.strerror)
                print(dset_error_msg)

    def get_data(self):
        """
        Name:     Prida.get_data
        Inputs:   None.
        Outputs:  None.
        Features: Creates a list of attribute dictionaries for each PID in
                  the HDF5 file.
        """
        self.data_list = []
        for pid in hdf.list_pids():
            d = {}
            d['pid'] = pid
            for i in self.pid_attrs.keys():
                my_attr = self.pid_attrs[i]['key']
                d[my_attr] = hdf.get_attr(my_attr, os.path.join('/', str(pid)))
            self.data_list.append(d)

    def get_input_field(self, field_name, field_type):
        """
        Name:     Prida.get_input_field
        Inputs:   - str, Qt field name (field_name)
                  - str, Qt field type (field_type)
        Outputs:  str, current input text (field_value)
        Features: Get the current text from a Qt input box
        """
        err_msg = "Could not get Qt %s, %s" % (field_type, field_name)

        if field_type == 'QComboBox':
            try:
                field_value = eval("self.view.%s.currentText()" % (field_name))
            except:
                raise NameError(err_msg)
        elif (field_type == 'QDateEdit' or
              field_type == 'QLineEdit' or
              field_type == 'QSpinBox'):
            # All three fields use text() getter
            try:
                field_value = eval("self.view.%s.text()" % (field_name))
            except:
                raise NameError(err_msg)
        else:
            raise NameError(err_msg)

        if field_value == '':
            field_value = '<Undefined>'
        return field_value

    def get_sheet_model_selection(self):
        """
        Name:     Prida.get_sheet_model_selection
        Inputs:   None
        Outputs:  str, HDF5 group path based on selection (s_path)
        Features: Returns sheet model selection path for HDF5 file
        """
        if self.sheet_select_model.hasSelection():
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session
                pid = my_selection.parent().text()
                session = my_selection.text()
                s_path = "/%s/%s" % (pid, session)
            else:
                # Selected PID
                pid = my_selection.text()
                s_path = "/%s" % (pid)

            return s_path

    def import_data(self):
        """
        Name:     Prida.import_data
        Inputs:   None.
        Outputs:  None.
        Features: Reads a user-defined directory for images, attempts to
                  extract image exif tags and defines them in INPUT_EXP view
        Depends:  set_input_field
        @TODO:    - save dataset attributes (i.e., image height, width, and
                    orientation)
                  - set view to RUN_EXP during image import
        """
        my_tags = ['Make',                 # Exif Tag (IFD0) 271
                   'Model',                # Exif Tag (IFD0) 272
                   'Orientation',          # Exif Tag (IFD0) 274
                   'DateTime',             # Exif Tag (IFD0) 306
                   'ISOSpeedRatings',      # Exif Tag (SubIFD) 34855
                   'ShutterSpeedValue',    # Exif Tag (SubIFD) 37377
                   'ApertureValue',        # Exif Tag (SubIFD) 37378
                   'NumPhotos']
        # Pseudo-intelligent search for session_attr index:
        tag_attr = {
            'Make': [k for k, v in self.session_attrs.items()
                     if v['key'] == 'cam_make'],
            'Model': [k for k, v in self.session_attrs.items()
                      if v['key'] == 'cam_model'],
            'ShutterSpeedValue': [k for k, v in self.session_attrs.items()
                                  if v['key'] == 'cam_shutter'],
            'ApertureValue': [k for k, v in self.session_attrs.items()
                              if v['key'] == 'cam_aperture'],
            'ISOSpeedRatings': [k for k, v in self.session_attrs.items()
                                if v['key'] == 'cam_exposure'],
            'DateTime': [k for k, v in self.session_attrs.items()
                         if v['key'] == 'date'],
            'NumPhotos': [k for k, v in self.session_attrs.items()
                          if v['key'] == 'num_img']
        }
        path = QFileDialog.getExistingDirectory(
            None,
            'Select directory containing images:',
            os.path.expanduser("~"),
            QFileDialog.ShowDirsOnly
        )
        if os.path.isdir(path):
            # Search through all supported image types:
            my_files = []
            for img_type in self.img_types:
                s_path = os.path.join(path, "*%s" % (img_type))
                my_files += glob.glob(s_path)
            files_found = len(my_files)
            print("Importing %d files" % (files_found))

            if files_found > 0:
                # Reset file names for each processing:
                self.search_files = {}
                for my_file in my_files:
                    self.search_files[my_file] = 0
                    img = Image.open(my_file)
                    try:
                        exif = {ExifTags.TAGS[k]: v
                                for k, v in img._getexif().items()
                                if k in ExifTags.TAGS}
                    except AttributeError:
                        exif = {}
                    for tag in my_tags:
                        my_attr = ''
                        try:
                            my_temp_attr = exif.get(tag, '')
                            if isinstance(my_temp_attr, tuple):
                                my_attr = str(my_temp_attr[0])
                            else:
                                my_attr = str(my_temp_attr)

                            if str(tag) == 'DateTime':
                                try:
                                    im_date = my_attr.split(' ')[0]
                                    my_attr = im_date.replace(':', '-')
                                except:
                                    my_attr = ''
                            elif str(tag) == 'NumPhotos':
                                my_attr = str(files_found)
                        except KeyError:
                            my_attr = ''
                        finally:
                            if tag in tag_attr:
                                j = tag_attr[tag][0]
                                f_name = self.session_attrs[j]['qt_val']
                                f_type = self.session_attrs[j]['qt_type']
                                f_val = my_attr
                                self.set_input_field(f_name, f_type, f_val)
                #
                self.view.stackedWidget.setCurrentIndex(INPUT_EXP)

    def import_session(self):
        """
        Name:     Prida.import_session
        Inputs:   None.
        Outputs:  None.
        Features: Creates a new session based on imported data
        Depends:  - get_data
                  - update_sheet_model
        """
        if self.editing:
            self.save_edits()
            self.editing = False
            self.enable_all_attrs()
            self.set_defaults()
        else:
            pid_dict = {}
            for i in self.pid_attrs.keys():
                f_key = self.pid_attrs[i]['key']
                f_name = self.pid_attrs[i]['qt_val']
                f_type = self.pid_attrs[i]['qt_type']
                f_val = self.get_input_field(f_name, f_type)
                pid_dict[f_key] = f_val

            session_dict = {}
            for j in self.session_attrs.keys():
                f_key = self.session_attrs[j]['key']
                f_name = self.session_attrs[j]['qt_val']
                f_type = self.session_attrs[j]['qt_type']
                f_val = self.get_input_field(f_name, f_type)
                session_dict[f_key] = f_val

            s_path = hdf.create_session(
                str(self.view.c_pid.text()), pid_dict, session_dict
                )

            # @TODO: progress bar?
            for filepath in sorted(list(self.search_files.keys())):
                hdf.save_image(s_path, filepath)
            hdf.save()

        # Return to sheet:
        self.get_data()
        self.back_to_sheet()

    def load_file_browse_sheet_headers(self):
        """
        Name:     Prida.load_file_browse_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the file_sheet_model
        Depends:  resize_file_browse_sheet
        """
        #
        self.file_sheet_model.setColumnCount(2)
        self.file_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File Name')
        )
        self.file_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Path')
        )
        self.resize_file_browse_sheet()

    def load_file_search_sheet_headers(self):
        """
        Name:     Prida.load_file_search_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the search_sheet_model
        Depends:  resize_search_sheet
        """
        self.search_sheet_model.setColumnCount(self.sheet_items + 1)
        self.search_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File'))
        self.search_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('PID'))
        for i in self.pid_attrs:
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.search_sheet_model.setHorizontalHeaderItem", (i + 1),
                self.pid_attrs[i]['title']
                )
            )
        self.resize_search_sheet()

    def load_session_sheet_headers(self):
        """
        Name:     Prida.load_session_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the session_sheet_model
        Depends:  resize_session_sheet
        """
        # Define session sheet column headers:
        self.session_sheet_model.setColumnCount(2)
        self.session_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('Property')
        )
        self.session_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Value')
        )
        #
        # Define session sheet rows in column 1:
        for i in range(self.session_sheet_items):
            exec('%s(%d, 0, QStandardItem("%s"))' % (
                "self.session_sheet_model.setItem", i,
                self.session_attrs[i]['title']
                )
            )
        self.resize_session_sheet()

    def load_sheet_headers(self):
        """
        Name:     Prida.load_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the sheet_model
        Depends:  resize_sheet
        """
        self.sheet_model.setColumnCount(self.sheet_items)
        self.sheet_model.setHorizontalHeaderItem(0, QStandardItem('PID'))
        for i in self.pid_attrs.keys():
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.sheet_model.setHorizontalHeaderItem", i,
                self.pid_attrs[i]['title']
                )
            )
        self.resize_sheet()

    def new_hdf(self):
        """
        Name:     Prida.new_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Gets user information, gets HDF5 file name, creates a new
                  HDF5 file, stores user information, and goes to VIEWER page
        Depends:  - get_data
                  - update_sheet_model
        """
        popup = uic.loadUi(self.user_ui)
        if popup.exec_():
            path = QFileDialog.getSaveFileName()[0]
            if path != '':
                if path[-5:] != '.hdf5':
                    path += '.hdf5'
                hdf.open_file(path)
                sess_user = str(popup.user.text())
                sess_addr = str(popup.email.text())
                sess_about = str(popup.about.toPlainText())
                hdf.set_root_user(sess_user)
                hdf.set_root_addr(sess_addr)
                hdf.set_root_about(sess_about)
                hdf.save()
                self.get_data()
                self.update_sheet_model()
                if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                    self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
                self.view.stackedWidget.setCurrentIndex(VIEWER)

                # Set session user and contact default values:
                self.view.c_session_user.setText(sess_user)
                self.view.c_session_addr.setText(sess_addr)

    def new_session(self):
        """
        Name:     Prida.new_session
        Inputs:   None.
        Outputs:  None
        Features: Creates HDF5 session, resets the progress bar, enters the
                  RUN_EXP screen, and begins a session
        """
        global session_path
        if self.editing:
            self.save_edits()
            self.editing = False
            self.enable_all_attrs()
            self.set_defaults()

            # Return to sheet:
            self.get_data()
            self.back_to_sheet()
        else:
            if hardware_mode:
                pid_dict = {}
                for i in self.pid_attrs.keys():
                    f_key = self.pid_attrs[i]['key']
                    f_name = self.pid_attrs[i]['qt_val']
                    f_type = self.pid_attrs[i]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    pid_dict[f_key] = f_val

                session_dict = {}
                for j in self.session_attrs.keys():
                    f_key = self.session_attrs[j]['key']
                    f_name = self.session_attrs[j]['qt_val']
                    f_type = self.session_attrs[j]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    session_dict[f_key] = f_val

                session_path = hdf.create_session(str(self.view.c_pid.text()),
                                                  pid_dict,
                                                  session_dict)
                imaging.num_photos = int(self.view.c_num_images.text())
                self.view.c_progress.setMinimum(0)
                self.view.c_progress.setMaximum(int(imaging.ms_end_pos))
                self.view.stackedWidget.setCurrentIndex(RUN_EXP)
                self.hardware_thread.start(QThread.TimeCriticalPriority)

    def open_hdf(self):
        """
        Name:     Prida.open_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Opens an existing HDF5 file and goes to VIEWER page
        Depends:  - get_data
                  - back_to_sheet
                  - update_pid_auto_model
        """
        path = QFileDialog.getOpenFileName(filter='*.hdf5')[0]
        if os.path.isfile(path):
            hdf.open_file(path)
            self.get_data()
            self.update_pid_auto_model()
            self.back_to_sheet()

            # Set session user and contact defaults:
            self.view.c_session_user.setText(str(hdf.get_root_user()))
            self.view.c_session_addr.setText(str(hdf.get_root_addr()))

    def resize_file_browse_sheet(self):
        """
        Name:     Prida.resize_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_browse_sheet
        """
        for col in range(2):
            self.view.file_browse_sheet.resizeColumnToContents(col)

    def resize_search_sheet(self):
        """
        Name:     Prida.resize_search_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_search_sheet
        """
        for col in range(self.sheet_items + 2):
            self.view.file_search_sheet.resizeColumnToContents(col)

    def resize_session_sheet(self):
        """
        Name:     Prida.resize_session_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the session_sheet
        """
        for col in range(2):
            self.view.session_sheet.resizeColumnToContents(col)

    def resize_sheet(self):
        """
        Name:     Prida.resize_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the sheet
        """
        for col in range(self.sheet_items + 1):
            self.view.sheet.resizeColumnToContents(col)

    def rm_search_file(self):
        """
        Name:     Prida.rm_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Removes a file from the file browse view and its
                  associated PID rows from the file search view
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        if self.file_select_model.hasSelection():
            # Get the selection and put together the file name and path:
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0]
            )
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            # Remove file from search_files:
            del self.search_files[s_name]

            # Remove file's PIDs from file_search_sheet:
            if hdf.isopen:
                hdf.close()
            hdf.open_file(f_path)
            for pid in hdf.list_pids():
                my_key = "%s.%s" % (s_name, pid)
                if my_key in self.search_data:
                    del self.search_data[my_key]
            hdf.close()
            self.view.f_search.clear()
            self.update_file_browse_sheet()
            self.update_file_search_sheet()
        else:
            print('Nothing selected!')

    def save_edits(self):
        """
        Name:     Prida.save_edits
        Inputs:   None
        Outputs:  None
        Features: Saves new attributes to HDF5 file
        Depends:  get_sheet_model_selection
        """
        if self.editing:
            if self.sheet_select_model.hasSelection():
                s_path = self.get_sheet_model_selection()

                for i in self.pid_attrs.keys():
                    f_key = self.pid_attrs[i]['key']
                    f_name = self.pid_attrs[i]['qt_val']
                    f_type = self.pid_attrs[i]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        hdf.set_attr(f_key, f_val, s_path)
                for j in self.session_attrs.keys():
                    f_key = self.session_attrs[j]['key']
                    f_name = self.session_attrs[j]['qt_val']
                    f_type = self.session_attrs[j]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        hdf.set_attr(f_key, f_val, s_path)
                hdf.save()

    def search(self, pid_meta, terms):
        """
        Name:     Prida.search
        Inputs:   - list, PID meta data values (pid_meta)
                  - list, search terms (terms)
        Outputs:  bool (found)
        Features: Searches the pid metafields and returns true if all terms are
                  found anywhere within the fields
        """
        found = True
        for term in terms:
            found = found and any(term in m_field for m_field in pid_meta)
        return found

    def set_defaults(self):
        """
        Name:     Prida.set_defaults
        Inputs:   None.
        Outputs:  None.
        Features: Sets GUI text default values
        """
        for i in self.pid_attrs.keys():
            f_name = self.pid_attrs[i]['qt_val']
            f_type = self.pid_attrs[i]['qt_type']
            if self.pid_attrs[i]['has_default']:
                f_val = self.pid_attrs[i]['def_val']
                self.set_input_field(f_name, f_type, f_val)
            else:
                self.set_input_field(f_name, f_type, '')

        for j in self.session_attrs.keys():
            f_name = self.session_attrs[j]['qt_val']
            f_type = self.session_attrs[j]['qt_type']
            if self.session_attrs[j]['has_default']:
                f_val = self.session_attrs[j]['def_val']
                try:
                    self.set_input_field(f_name, f_type, f_val)
                except:
                    self.set_input_field(f_name, f_type, '')
            else:
                self.set_input_field(f_name, f_type, '')

    def set_input_field(self, field_name, field_type, field_value):
        """
        Name:     Prida.set_input_field
        Inputs:   - str, Qt input widget name (field_name)
                  - str, Qt input widget type (field_type)
                  - str, input value (field_value)
        Outputs:  None
        Features: Sets Qt input field
        """
        err_msg = "Could not set Qt %s, %s, to value %s" % (
            field_type, field_name, field_value)

        if field_type == 'QComboBox':
            # Takes an integer, find it!
            if field_value == '':
                index = 0
            else:
                index = eval("self.view.%s.findText('%s', %s)" % (
                    field_name,
                    field_value,
                    'Qt.MatchFixedString')
                )
            try:
                exec("self.view.%s.setCurrentIndex(%d)" % (
                    field_name,
                    index)
                )
            except:
                raise ValueError(err_msg)
        elif field_type == 'QDateEdit':
            # Takes a QDate object
            if field_value == '':
                try:
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        "QDate.fromString",
                        QDate.currentDate().toString("yyyy-MM-dd"))
                    )
                except:
                    raise ValueError(err_msg)
            else:
                try:
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        'QDate.fromString',
                        field_value)
                    )
                except:
                    raise ValueError(err_msg)
        elif field_type == 'QLineEdit':
            # Takes a string
            try:
                exec('self.view.%s.setText("%s")' % (
                    field_name,
                    field_value)
                )
            except:
                raise ValueError(err_msg)
        elif field_type == 'QSpinBox':
            # Takes an integer
            if field_value == '':
                field_value = 0
            try:
                exec("self.view.%s.setValue(%d)" % (
                    field_name,
                    int(field_value))
                )
            except:
                raise ValueError(err_msg)
        else:
            raise ValueError(err_msg)

    def show_ida(self, selection):
        """
        Name:     Prida.show_ida
        Input:    obj (selection)
        Output:   None.
        Features: Switches to IDA page and sets IDA base image
        """
        print(selection.indexes())
        if selection.indexes():
            self.view.stacked_sheets.setCurrentIndex(IDA_VIEW)
            img_path = self.img_sheet_model.itemFromIndex(
                selection.indexes()[0]).data()
            ndarray_full = hdf.get_dataset(img_path)
            self.view.ida.setBaseImage(ndarray_full)

    def to_sheet(self):
        """
        Name:     Prida.to_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Switches from IDA to sheet view
        """
        self.view.img_select.clearSelection()
        self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)

    def update_file_browse_sheet(self):
        """
        Name:     Prida.update_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Reloads the rows in the file_sheet_model
        Depends:  - load_file_browse_sheet_headers
                  - resize_file_browse_sheet
        """
        self.file_sheet_model.clear()
        self.load_file_browse_sheet_headers()
        for f_name in self.search_files:
            f_path = self.search_files[f_name]
            f_list = [QStandardItem(f_name), QStandardItem(f_path)]
            self.file_sheet_model.appendRow(f_list)
        self.resize_file_browse_sheet()

    def update_file_search_sheet(self, search=''):
        """
        Name:     Prida.update_file_search_sheet
        Inputs:   [optional] str, search string (search)
        Outputs:  None.
        Features: Reloads the rows in the search_sheet_model, filtered for
                  search terms
        Depends:  - load_file_search_sheet_headers
                  - resize_search_sheet
                  - search
        """
        self.search_sheet_model.clear()
        self.load_file_search_sheet_headers()
        terms = search.lower().split()
        for i in self.search_data:
            pid_meta = []
            for j in self.search_data[i]:
                pid_meta.append(j.lower())
            if self.search(pid_meta, terms):
                d_list = []
                for k in self.search_data[i]:
                    d_list.append(QStandardItem(k))
                self.search_sheet_model.appendRow(d_list)
        self.resize_search_sheet()

    def update_pid_auto_model(self):
        """
        Name:     Prida.update_pid_auto_model
        Inputs:   None
        Outputs:  None
        Features: Updates the QStringListModel with HDF5 file's current PIDs
        """
        if hdf.isopen:
            self.pid_auto_model.setStringList(hdf.list_pids())
        else:
            self.pid_auto_model.setStringList([])

    def update_progress(self):
        """
        Name:     Prida.update_progress
        Inputs:   None.
        Outputs:  None.
        Features: Updates the progress bar, sets captured image to preview, and
                  decides the completion status of the session
        Depends:  - clear_session_sheet_model
                  - get_data
                  - update_sheet_model
        """
        self.view.c_progress.setValue(imaging.ms_count)
        if is_path:
            image = QPixmap(filepath)
            self.view.c_preview.setPixmap(image.scaledToHeight(300))
        if not imaging.is_running:
            imaging.reset_count()
            self.get_data()
            self.back_to_sheet()
        else:
            self.hardware_thread.start(QThread.TimeCriticalPriority)

    def update_session_sheet_model(self, selection):
        """
        Name:     Prida.update_session_sheet_model
        Inputs:   obj (selection)
        Outputs:  None.
        Features: Updates session sheet model with selected session's data,
                  creates the thumbnails and tags the full image paths, and
                  clears session sheet model if no sessions are selected
        Depends:  - clear_session_sheet_model
                  - resize_session_sheet
        """
        try:
            my_session = self.sheet_model.itemFromIndex(selection.indexes()[0])
        except IndexError:
            # You made a de-selection (ctrl+click):
            self.clear_session_sheet_model()
            self.img_sheet_model.clear()
            self.img_list = []
            self.resize_session_sheet()
        else:
            if (my_session.parent() is not None):
                # Session selection
                pid = my_session.parent().text()
                session = my_session.text()
                s_path = "/%s/%s" % (pid, session)
                for i in range(self.session_sheet_items):
                    exec('%s(%d, 1, QStandardItem("%s"))' % (
                        "self.session_sheet_model.setItem", i,
                        hdf.get_attr(self.session_attrs[i]['key'], s_path)
                        )
                    )
                self.img_sheet_model.clear()
                self.img_list = []
                img_counter = 0
                if hdf.isopen:
                    for obj in hdf.list_objects(s_path):
                        img_counter += 1
                        thumb_path = "%s/%s/%s.thumb" % (s_path, obj, obj)

                        try:
                            ndarray = hdf.get_dataset(thumb_path)
                            # Add binary image support:
                            if ndarray.max() == 1 and len(ndarray.shape) == 2:
                                ndarray[numpy.where(ndarray == 1)] *= 255
                        except IOError:
                            ndarray = numpy.zeros((250, 250))

                        for img_ext in self.img_types:
                            try:
                                image_path = (
                                    "%s/%s/%s%s" % (s_path, obj, obj, img_ext)
                                )
                                img = hdf.get_dataset(image_path)
                            except IOError:
                                pass
                            else:
                                image = Image.fromarray(ndarray)
                                qt_image = ImageQt.ImageQt(image)
                                self.img_list.append(qt_image)
                                pix = QPixmap.fromImage(qt_image)
                                item = QStandardItem(str(img_counter))
                                item.setIcon(QIcon(pix))
                                item.setData(image_path)
                                self.img_sheet_model.appendRow(item)
            else:
                # PID selection
                self.clear_session_sheet_model()
                self.img_sheet_model.clear()
                self.img_list = []
            self.resize_session_sheet()

    def update_sheet_model(self, search=''):
        """
        Name:     Prida.update_sheet_model
        Inputs:   [optional] str (search)
        Features: Displays all PIDs in the sheet or the PIDs that match the
                  search terms if search terms are given
        Depends:  - load_sheet_headers
                  - resize_sheet
                  - search
        """
        self.update_pid_auto_model()
        self.sheet_model.clear()
        self.load_sheet_headers()
        terms = search.lower().split()
        for data in self.data_list:
            pid_meta = []
            for key in data:
                pid_meta.append(data[key].lower())
            if self.search(pid_meta, terms):
                pid_item = QStandardItem(data['pid'])

                # Create session rows under each PID:
                for session in hdf.list_sessions(data['pid']):
                    pid_item.appendRow(QStandardItem(session))

                # Fill the PID row with its attributes:
                pid_list = [pid_item, ]
                for i in sorted(self.pid_attrs.keys()):
                    my_attr = data[self.pid_attrs[i]['key']]
                    pid_list.append(QStandardItem(my_attr))
                self.sheet_model.appendRow(pid_list)
        self.resize_sheet()

###############################################################################
# MAIN:
###############################################################################
if __name__ == '__main__':
    hdf = PridaHDF()
    if hardware_mode:
        imaging = Imaging()
    app = Prida()
    app.exec_()
    if hardware_mode:
        app.hardware_thread.wait()
    hdf.close()
    sys.exit()
