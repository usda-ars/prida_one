#!/usr/bin/python
#
# camera.py
#
# VERSION: 1.0.0
#
# LAST EDIT: 2015-10-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 6f881e8: twdavis - 2015-10-21 10:54:00
#     v0.7.0
# 9b5d69f: Nathanael Shaw - 2015-10-02 13:01:49
#     update headers
# 4a1d374: Nathanael Shaw - 2015-10-01 16:24:45
#     update changelogs
# 36d81c1: Nathanael Shaw - 2015-10-01 16:23:21
#     version 0.6.1-dev
# b08d3bd: Nathanael Shaw - 2015-10-01 14:32:35
#     return strings for eval parsing
# 9f50386: Nathanael Shaw - 2015-09-30 14:19:10
#     update changelogs
# 5f0aa8a: Nathanael Shaw - 2015-09-29 17:19:43
#     run_sequence return filepath only on success
# 19664e5: twdavis - 2015-09-29 15:18:54
#     minor updates
# 42dd0fd: Nathanael Shaw - 2015-09-28 12:18:45
#     correct typo
# d42ec29: Nathanael Shaw - 2015-09-28 12:05:04
#     remove module level vars from scripts
# 9c28bfe: Nathanael Shaw - 2015-09-28 11:57:19
#     title hotfix
# b089bd6: Nathanael Shaw - 2015-09-28 11:36:52
#     add open-source license disclaimers
# 48c37c3: Nathanael Shaw - 2015-09-24 16:37:28
#     Update changelogs for new repo
# d978041: twdavis - 2015-09-22 13:22:25
#     Addresses #3. Python 2/3 compatability.
# 44a5255: Nathanael Shaw - 2015-09-21 17:38:57
#     Merge with periphdev
# 99e70b2: twdavis - 2015-09-21 16:34:31
#     added working
# $log_end_tag$
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import os
import traceback
import gphoto2 as gp
from pridaperipheral import PRIDAPeripheral
from pridaperipheral import PRIDAPeripheralError


###############################################################################
## CLASSES:
###############################################################################
class Camera(PRIDAPeripheral):

    #camera_set = {}
    #available = {}

    def __init__(self):
        """
        Initialize data members, connect the camera.
        """
        PRIDAPeripheral.__init__(self)
        self.path = '/tmp'
        self.context = None
        self.camera = None
        self.config = None
        self._summary = ''
        self._attr_dict = {}
        self.connect()

    def connect(self):
        """
        Name:    Camera.connect
        Feature: Connects the camera to the control system.
                 Overrides PRIDAPeripheral.connect.
        Inputs:  None
        Outputs: None
        """
        try:
            if (not self.context):
                self.context = gp.gp_context_new()
                self.camera = gp.check_result(gp.gp_camera_new())
                gp.gp_camera_init(self.camera, self.context)
                self.config = gp.check_result(gp.gp_camera_get_config(
                    self.camera, self.context))
                tmp = []
                for i in range(1000):
                    try:
                        tmp.append(self.config.get_child_by_id(i).get_label())
                    except gp.GPhoto2Error:
                        break
                self._attr_dict = dict(enumerate(tmp))
        except:
            traceback.print_exc()
            raise PRIDAPeripheralError("Could not connect to camera.")

    def poweroff(self):
        """
        Name:    Camera.poweroff
        Feature: Prepares the camera for manual shutdown.
                 Overrides PRIDAPeripheral.poweroff.
        Inputs:  None
        Outputs: None
        """
        err = gp.check_result(gp.gp_camera_exit(self.camera, self.context))
        if err >= 1:
            raise PRIDAPeripheralError('Could not disconnect' +
                                       ' camera for poweroff!')
        else:
            self.context = None
            self.camera = None
            self.config = None
            self._summary = ''
            self._attr_dict = {}
            print('Camera disconnected. Safe to power down.')

    def current_status(self):
        """
        Name:    Camera.current_status
        Feature: Identifies the current status of the camera.
                 Overrides PRIDAPeripheral.check_status.
        Inputs:  None
        Outputs: None
        """
        super(self.current_status())

    def get_full_path(self):
        """
        Name:    Camera.get_full_path
        Feature: Returns a string representing the filepath to the
                 working directory
        Inputs:  None
        Outputs: String, path to working directory (self.path)
        """
        return self.path

    def set_path(self, p):
        """
        Name:    Camera.set_path
        Feature: Sets the path to the working directory
        Inputs:  String, path to save images to (p)
        Outputs: None
        """
        self.path = p

    def capture(self, name, verbose=False):
        """
        Name:    Camera.capture
        Feature: Captures an image with the connected camera
        Inputs:  - String, name of the image to be saved (name)
                 - Bool, enable verbose output (verbose)
        Outputs: String, absolute file path of captured image (filename)
        """
        file_path = gp.check_result(gp.gp_camera_capture(self.camera,
                                                         gp.GP_CAPTURE_IMAGE,
                                                         self.context))
        filename = os.path.join(self.path, str(name) + ".jpg")
        if verbose:
            print(('Copying image to: ' + filename))
        camera_file = gp.check_result(gp.gp_camera_file_get(self.camera,
                                                            file_path.folder,
                                                            file_path.name,
                                                            gp.GP_FILE_TYPE_NORMAL,
                                                            self.context))
        gp.check_result(gp.gp_file_save(camera_file, filename))
        gp.check_result(gp.gp_camera_exit(self.camera, self.context))
        return filename

    @property
    def exposure_time(self):
        """
        Name:    Camera.exposure_time
        Feature: Property describing camera exposure time or "shutter speed"
        Inputs:  None
        Outputs: Float, duration during image capture of open shutter
                 (self.exposure_time)
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return self._attr_getter(tmp['Shutter Speed']).strip('s')

    @property
    def focal_length(self):
        """
        Name:    Camera.focal_length
        Feature: Returns the focal length of the camera (in mm)
        Inputs:  None
        Outputs: Integer representing the focal length
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return self._attr_getter(tmp['Focal Length'])

    @property
    def orientation(self):
        """
        Name:    Camera.orientation
        Feature: Returns angle value corresponding to camera position (using
                 standard polar coordinates)
        Inputs:  None
        Outputs: Integer, the angular position of the camera
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return self._attr_getter(tmp['Camera Orientation'])

    @property
    def iso_speed(self):
        """
        Name:    Camera.iso_speed
        Feature: Returns integer representing camera iso speed
        Inputs:  None
        Outputs: String, the manufacturer and device model of the camera
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return self._attr_getter(tmp['ISO Speed'])

    @property
    def aperture(self):
        """
        Name:    Camera.aperture
        Feature: Returns integer representing camera aperture (if determinable)
        Inputs:  None
        Outputs: Integer, the camera aperture (if known)
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        apt_at_min = self._attr_getter(
            tmp['Maximum Aperture at Focal Length Maximum'])
        apt_at_max = self._attr_getter(
            tmp['Maximum Aperture at Focal Length Minimum'])
        if apt_at_min == apt_at_max:
            return apt_at_min
        else:
            return 'Unknown. Between %s - %s.' % (apt_at_min, apt_at_max)

    @property
    def image_size(self):
        """
        Name:    Camera.image_size
        Feature: Returns camera image resolution
        Inputs:  None
        Outputs: List (int), the pixel height and width of the images
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        img_size = self._attr_getter(tmp['Image Size'])
        img_size = img_size.split('x')
        return [img_size[0], img_size[1]]

    @property
    def image_height(self):
        """
        Name:    Camera.image_height
        Feature: Returns height dimension of the image resolution
        Inputs:  None
        Outputs: Integer, the image height in pixels
        """
        tmp = self.image_size
        if self.orientation in {90, 270}:
            tmp.sort()
            return tmp.pop()
        else:
            tmp.sort(reverse=True)
            return tmp.pop()

    @property
    def image_width(self):
        """
        Name:    Camera.image_width
        Feature: Returns width dimension of the image resolution
        Inputs:  None
        Outputs: Integer, the image width in pixels
        """
        tmp = self.image_size
        if self.orientation in {0, 180}:
            tmp.sort()
            return tmp.pop()
        else:
            tmp.sort(reverse=True)
            return tmp.pop()

    @property
    def summary(self):
        """
        Name:    Camera.summary
        Feature: Returns string containing gphoto2 camera summary data
        Inputs:  None
        Outputs: String, the manufacturer and device model of the camera
        """
        return ' '.join([self.make, self.model])

    @property
    def make(self):
        """
        Name:    Camera.make
        Feature: Returns string containing camera manufacturer
        Inputs:  None
        Outputs: String, the manufacturer of the camera
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return self._attr_getter(tmp['Camera Manufacturer'])

    @property
    def model(self):
        """
        Name:    Camera.model
        Feature: Returns string containing camera model
        Inputs:  None
        Outputs: String, the model of the camera
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return self._attr_getter(tmp['Camera Model'])

    def _attr_setter(self, index, value):
        """
        Name:    Camera._attr_setter
        Feature: Queries the camera for an atribute at an id and attempts
                 assignment to that attribute.
        Inputs:  Unspecified type, the value to attempt to assign (value)
        Outputs: None
        """
        if index in self._attr_dict:
            try:
                self.config.get_child_by_id(index).set_value(value)
                self.camera.set_config(self.config, self.context)
            except gp.GPhoto2Error:
                PRIDAPeripheralError(' "%s" is a bad parameter for "%s".'
                    % (str(value), str(self._attr_dict[index])))
        else:
            raise KeyError(' %i not a valid attribute id.' % index)

    def _attr_getter(self, index):
        """
        Name:    Camera._attr_getter
        Feature: Queries the camera for an attributes value at a specific id
        Inputs:  None
        Outputs: Unspecified type, the value of the queried attribute
        """
        if index in self._attr_dict:
            return self.config.get_child_by_id(index).get_value()
        else:
            raise KeyError(' %i not a valid attribute id.' % index)
