#!/usr/bin/python
#
# piezo.py
#
# VERSION: 0.4.2
#
# LAST EDIT: 2015-07-29
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# fe82fdc: Nathanael Shaw - 2015-07-15 19:09:26
#     Resolves #34
# 9398843: Hoi Cheng - 2015-07-15 18:19:24
#     Fixing docstrings. Added volume control to piezo.py
# 184a091: Nathanael Shaw - 2015-07-15 15:16:03
#     Initial attempt at new class organization.
# $log_end_tag$
#
from Adafruit_MotorHAT import Adafruit_MotorHAT
import time


class Piezo(Adafruit_MotorHAT):
    def __init__(self):
            Adafruit_MotorHAT.__init__(self)
            self.PIEZO = 0
            self.VOLUME = 1

    def set_piezo(self, ch):
        """
        Name:    Piezo.set_piezo
        Feature: Set the PWM channel controlling the piezo buzzer.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        self.PIEZO = ch

    def set_volume(self, volume):
        """
        Name:    Piezo.set_volume
        Feature: Set piezo volume by duty cycle.
        Inputs:  int, percent of maximum volume
        Outputs: None
        """
        if volume >= 0 and volume <= 100:
            self.VOLUME = volume/100.0

    def beep(self, num_beeps=3, beep_time=0.05, rest_time=0.1):
        """
        Name:    Piezo.beep
        Feature: Temporarily raise the 'piezo' channel. Default is three
                 short beeps in quick succession.
        Inputs:  - int, desired number of beeps (num_beeps)
                 - float, length of each beep in seconds (beep_time)
                 - float, length of silence between beeps (rest_time)
        Outputs: None
        """
        for i in xrange(num_beeps):
            self._pwm.setPWM(self.PIEZO, 0, int(2048*self.VOLUME))
            time.sleep(beep_time)
            self._pwm.setPWM(self.PIEZO, 0, 4096)
            time.sleep(rest_time)
