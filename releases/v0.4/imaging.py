#!/usr/bin/python
#
# imaging.py
#
# VERSION: 0.4.2
#
# LAST EDIT: 2015-07-29
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# 21db1f6: Hoi Cheng - 2015-07-27 12:08:10
#     refixed
# 3892066: Nathanael Shaw - 2015-07-22 13:50:29
#     Fixed imaging.py run_sequence docstring.
# fe82fdc: Nathanael Shaw - 2015-07-15 19:09:26
#     Resolves #34
# 9398843: Hoi Cheng - 2015-07-15 18:19:24
#     Fixing docstrings. Added volume control to piezo.py
# 184a091: Nathanael Shaw - 2015-07-15 15:16:03
#     Initial attempt at new class organization.
# $log_end_tag$
#
from motor import Motor
from camera import Camera
from led import LED
from piezo import Piezo
import time
import traceback


class Imaging(Motor, Camera, LED, Piezo):
    """
    Aggregate imaging hardware class for PRIDA.
    """

    def __init__(self):
        """
        Initializes attributes and hardware states.
        """
        Motor.__init__(self)        # imaging.py extends motor.py
        Camera.__init__(self)       # imaging.py extends camera.py
        LED.__init__(self)          # imaging.py extends led.py
        Piezo.__init__(self)        # imaging.py extends piezo.py
        self.is_running = False     # indicates if system is mid DAQ
        self.is_error = False       # indicates system error
        self.calibrate()            # initializes motor
        self.connect()              # connects the camera
        self.green_on()             # led indicates ready for DAQ

    def set_is_running(self, state):
        """
        Name:    Imaging.set_is_running
        Feature: Set is_running data member to boolean value.
        Inputs:  Boolean, desired system state (state)
        Outputs: None
        """
        if type(state) is bool and state is not self.is_running:
            self.is_running = state

    def set_is_error(self, state):
        """
        Name:    Imaging.set_is_error
        Feature: Set is_error data member to boolean value.
        Inputs:  Boolean, desired system state (state)
        Outputs: None
        """
        if type(state) is bool and state is not self.is_error:
            self.is_error = state

    def run_sequence(self):
        """
        Name:    Imaging.run_sequence
        Feature: Tries aquiring an image and stepping the motor an IPA
                 step. If it fails, calls end sequence and indicates a
                 system error.
        Inputs:  None
        Outputs: string, filepath to image (filepath)
        """
        if not self.is_running:
            self.set_is_running(True)
        self.check_status()
        if self.get_count() >= self.num_photos:
            self.end_sequence()
        else:
            try:
                filepath = self.capture()
                self.step()
                if self.get_count() == (self.num_photos - 1):
                    self.beep()
                elif self.get_count() == self.num_photos:
                    self.end_sequence()
                return filepath
            except:
                traceback.print_exc()
                self.set_is_error(True)
                self.check_status()
                self.end_sequence()

    def end_sequence(self):
        """
        Name:    Imaging.end_sequence
        Feature: Tries stopping the system.
                 If successful, turn off the motors and indicate ready
                 status. If it fails, indicate error status.
        Inputs:  None
        Outputs: None
        """
        if self.is_running:
            self.set_is_running(False)
        try:
            self.turn_off_motors()
            for i in xrange(3):
                self.beep()
                time.sleep(0.5)
        except:
            self.set_is_error(True)
            self.check_status()
            self.beep(3, 1, 1)
        self.check_status()

    def check_status(self):
        """
        Name:    Imaging.check_status
        Feature: Sets the LED status indicator to the appropriate color.
        Inputs:  None
        Outputs: None
        """
        if self.is_running:
            self.red_on()
        elif self.is_error:
            self.yellow_on()
        else:
            self.green_on()
