#!/usr/bin/python
#
# led.py
#
# VERSION: 0.4.2
#
# LAST EDIT: 2015-07-29
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# 7149908: Nathanael Shaw - 2015-07-30 09:53:54
#     Changed LED type default to match lab setup.
# a876b18: Hoi Cheng - 2015-07-22 11:24:43
#     Set default LED type to common cathode.
# ac05231: Nathanael Shaw - 2015-07-20 13:01:36
#     Fixed led.py to use boolean equals instead of assignment.
# b6b407d: Nathanael Shaw - 2015-07-16 17:11:38
#     Adapted led.py to accomidate common cathode LEDs
# fe82fdc: Nathanael Shaw - 2015-07-15 19:09:26
#     Resolves #34
# 9398843: Hoi Cheng - 2015-07-15 18:19:24
#     Fixing docstrings. Added volume control to piezo.py
# 184a091: Nathanael Shaw - 2015-07-15 15:16:03
#     Initial attempt at new class organization.
# $log_end_tag$
#
from Adafruit_MotorHAT import Adafruit_MotorHAT


class LED(Adafruit_MotorHAT):
    """
    Class representing a tri-color (RGB) LED
    """

    def __init__(self):
        """
        Initialize PWM channels. Extends MotorHAT attributes and behaviours.
        """
        Adafruit_MotorHAT.__init__(self)
        self.BLUE = 1        # Default channel for the blue LED
        self.GREEN = 14      # Default channel for the green LED
        self.RED = 15        # Default channel for the red LED
        self.led_type = 'CA' # Default type Common Cathode

    def set_type(self, led_type):
        """
        Name:    LED.set_type
        Feature: Set type to common anode or common cathode.
        Inputs:  String, two letter type designation (led_type)
        Outputs: None
        """
        if led_type == 'CA' or led_type == 'CC':
            self.led_type = led_type
        else:
            print("Types limited to CC or CA.")

    def set_blue(self, ch):
        """
        Name:    LED.set_blue
        Feature: Set the PWM channel controlling the blue led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        self.BLUE = ch

    def set_green(self, ch):
        """
        Name:    LED.set_green
        Feature: Set the PWM channel controlling the green led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        self.GREEN = ch

    def set_red(self, ch):
        """
        Name:    LED.set_red
        Feature: Set the PWM channel controlling the red led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        self.RED = ch

    def led_off(self, channel):
        """
        Name:    LED.led_off
        Feature: Set low a specifed led channel.
        Inputs:  int, the number of the channel to lower (channel)
        Outputs: None
        """
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 4096, 0)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 0, 4096)

    def led_on(self, channel):
        """
        Name:    LED.led_on
        Feature: Set high a specified led channel.
        Inputs:  int, the number of the channel to raise (channel)
        Outputs: None
        """
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 0, 4096)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 4096, 0)

    def red_on(self):
        """
        Name:    LED.red_on
        Feature: Set high the 'RED' PWM channel, default 15.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self.RED)
        self.led_off(self.BLUE)
        self.led_off(self.GREEN)

    def blue_on(self):
        """
        Name:    LED.blue_on
        Feature: Set high the 'BLUE' PWM channel, default 1.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self.BLUE)
        self.led_off(self.RED)
        self.led_off(self.GREEN)

    def green_on(self):
        """
        Name:    LED.green_on
        Feature: Set high the 'GREEN' PWM channel, default 14.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self.GREEN)
        self.led_off(self.RED)
        self.led_off(self.BLUE)

    def yellow_on(self):
        """
        Name:    LED.yellow_on
        Feature: Set led color to yellow by combining green and red
                 light using default channels.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self.RED)
        self.led_on(self.GREEN)
        self.led_off(self.BLUE)

    def red_off(self):
        """
        Name:    LED.red_off
        Feature: Set low the 'red' PWM channel, default 15.
        Inputs:  None
        Outputs: None
        """
        self.led_off(self.RED)

    def blue_off(self):
        """
        Name:    LED.blue_off
        Feature: Set low the 'blue' PWM channel, default 1.
        Inputs:  None
        Outputs: None
        """
        self.led_off(self.BLUE)

    def green_off(self):
        """
        Name:    LED.green_off
        Feature: Set low the 'green' PWM channel, default 14.
        Inputs:  None
        Outputs: None
        """
        self.led_off(self.GREEN)

    def yellow_off(self):
        """
        Name:    LED.yellow_off
        Feature: Set low the red and green PWM channels, removing yellow
                 light.
        Inputs:  None
        Outputs: None
        """
        self.red_off()
        self.green_off()

    def all_off(self):
        """
        Name:    LED.all_off
        Feature: Set all led PWM channels to low.
        Inputs:  None
        Outputs: None
        """
        self.blue_off()
        self.green_off()
        self.red_off()

    def all_on(self):
        """
        Name:    LED.all_on
        Feature: Set all led PWM channels to high.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self.BLUE)
        self.led_on(self.GREEN)
        self.led_on(self.RED)
