#!/usr/bin/python
#
# motor.py
#
# VERSION: 0.4.2
#
# LAST EDIT: 2015-07-29
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# fe82fdc: Nathanael Shaw - 2015-07-15 19:09:26
#     Resolves #34
# 9398843: Hoi Cheng - 2015-07-15 18:19:24
#     Fixing docstrings. Added volume control to piezo.py
# 184a091: Nathanael Shaw - 2015-07-15 15:16:03
#     Initial attempt at new class organization.
# 0ac18e2: Nathanael Shaw - 2015-02-16 20:56:28 -0500
#     Proper PEP8 docstrings. Fixed all_on function.
# 9a356b5: Nathanael Shaw - 2015-07-15 10:45:42
#     Reduced magic number usage in LED and Piezo functions.
# a098ceb: Nathanael Shaw - 2015-07-10 17:08:13
#     motor.py now PEP8
# 3a19139: Nathanael Shaw - 2015-02-16 15:20:21 -0500
#     Resolves #23
# d6cf42f: Nathanael Shaw - 2015-07-10 11:46:06
#     Added piezo and led functions to motor.py
# 2663627: Nathanael Shaw - 2015-07-01 10:37:49
#     Expirimenting with proper python-esque inheritance.
# 4bfdd02: Nathanael Shaw - 2015-06-30 16:22:26
#     Corrected motor.py to conform to PEP8
# 8a4cf0e: Nathanael Shaw - 2015-06-30 16:12:47
#     Tested and adjusted motor.py
# aebca4d: Nathanael Shaw - 2015-06-30 15:12:45
#     Converted motor.py to class (untested)
# 3642855: Nathanael Shaw - 2015-06-30 12:42:30
#     Added microstep resolution detection to motor.py
# 773aec5: Nathanael Shaw - 2015-06-29 11:24:36
#     Fixed motor.py indentations, added testMotor.py to back-up folder.
# f9c4529: Nathanael Shaw - 2015-06-24 11:57:30
#     Modifed Motor.py step function, added RPM selection support.
# 6a90843: Jay - 2015-06-18 13:58:07
#     bugfixes
# $log_end_tag$
#
from Adafruit_MotorHAT import Adafruit_MotorHAT
import time


class Motor(Adafruit_MotorHAT):

    def __init__(self):
        """
        Initialize data memebers. Inherit attributes and
        behaviours from the Adafruit_MotorHAT object.
        Note: currently assumes that system is always using
        microstepping.
        """
        Adafruit_MotorHAT.__init__(self)
        self.num_photos = 40     # Number of photos to be taken
        self.rpm = 2.0           # Approximate rotational velocity
        self.step_res = 200      # Number of units steps per rotation
        self.motor_port_num = 1  # Motor HAT Port Number In Use
        self.sm = self.getStepper(self.step_res, self.motor_port_num)
        #                        ^ Adafruit Stepper Motor Object
        #                       \ / Minimum Time Elapsed In a Rotation
        self.base_rot_time = 1.75 * self.sm.MICROSTEPS

    def set_num_photos(self, np):
        """
        Name:    Motor.set_num_photos
        Feature: Set the number of photos in a single expiriment.
        Inputs:  int, number of photos to be taken (np)
        Outputs: None
        """
        self.num_photos = np

    def set_rpm(self, rot_per_min):
        """
        Name:    Motor.set_rpm
        Feature: Set approximate rotations per minute value for the
                 motor.
        Inputs:  float, desired rpm value (rot_per_min)
        Outputs: None
        """
        self.rpm = rot_per_min

    def set_step_res(self, sr, allow=False):
        """
        Name:    Motor.set_step_res
        Feature: Set stepper motor step resolution. Not currently
                 supported. Data member added to reduce presence of
                 'magic numbers.'
        Inputs:  - int, desired 'step resolution,' or the number of
                   steps in a full rotation, defining the length of a
                   unit step (sr)
                 - boolean, allows setter for goodness knows why (allow)
        Output:
        None
        """
        if allow:
            self.step_res = sr

    def set_motor_port_num(self, num, allow=False):
        """
        Name:    Motor.set_motor_port_num
        Feature: Set stepper motor port number. Not currently supported.
                 Data member added to reduce presence of 'magic numbers.'
        Inputs:  - int, desired port number to be used on the HAT (num)
                 - bool, allows setter for goodness knows why (allow)
        Outputs: None
        """
        if allow:
            self.motor_port_num = num

    def set_base_rot_time(self, base_time, allow=False):
        """
        Name:    Motor.set_base_rot_time
        Feature: Set stepper motor base rotation time (in seconds)
                 During full speed operation. Not currently supported.
                 Data member added to reduce presence of 'magic numbers.'
        Inputs:  - int, minmum rotation time in seconds (base_time)
                 - bool, allows setter for goodness knows why (allow)
        Outputs: None
        """
        if allow:
            self.base_rot_time = base_time

    def turn_off_motors(self):
        """
        Name:    Motor.turn_off_motors
        Feature: Disable all motors attached to the HAT.
        Inputs:  None
        Outputs: None
        """
        self.getMotor(1).run(self.RELEASE)
        self.getMotor(2).run(self.RELEASE)
        self.getMotor(3).run(self.RELEASE)
        self.getMotor(4).run(self.RELEASE)

    def calibrate(self):
        """
        Name:    Motor.calibrate
        Feature: Take three initial unit steps.
        Inputs:  None
        Outputs: None
        """
        for i in xrange(3*self.sm.MICROSTEPS):
            self.sm.oneStep(self.FORWARD, self.MICROSTEP)
            time.sleep(.1)

    def step(self):
        """
        Name:    Motor.step
        Feature: Take a single Inter-Photo-Angle (IPA) step.
                 Take the desired number of photos in the run and
                 calculate the IPA step size. Convert the desired RPM
                 value into the appropriate length wait time.
        Inputs:  None
        Outputs: None
        """
        # data member error checking
        if self.rpm <= 0:
            print("RPM must be greater than zero.")
            return None
        try:
            # convert RPM to req'd value for calculating wait time
            real_rpm = (1.0 / (((60.0 / self.rpm) -
                               self.base_rot_time) / 60.0))
        except ZeroDivisionError:
            print("Set a valid RPM value.")
        else:
            # begin stepping
            for i in xrange((self.step_res*self.sm.MICROSTEPS) /
                            self.num_photos):
                self.sm.oneStep(self.FORWARD, self.MICROSTEP)
                time.sleep(60.0 / (self.step_res *
                                   self.sm.MICROSTEPS * real_rpm))
