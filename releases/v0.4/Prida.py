#!/usr/bin/python
#
# Prida.py
#
# VERSION: 0.4.2
#
# LAST EDIT: 2015-07-29
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# d9ba542: jay - 2015-07-29 05:05:40
#     done prob
# b11d1c2: Hoi Cheng - 2015-07-28 14:19:19
#     seg fault?
# 4374cfc: Hoi Cheng - 2015-07-27 12:44:25
#     fix the headers
# 21db1f6: Hoi Cheng - 2015-07-27 12:08:10
#     refixed
# 0c19b8e: jay - 2015-07-26 21:33:42
#     Master Control Update
# fbcf2a3: Hoi Cheng - 2015-07-22 14:08:33
#     no more racing
# e0dd9a7: Hoi Cheng - 2015-07-22 13:06:50
#     this is hoi. everything is better
# 83f208b: jay - 2015-07-21 23:12:18
#     v0.4.1 -- i have tested nothing may not actually run
# e2a1c02: jay - 2015-07-21 22:59:01
#     i think it works
# a5e801b: jay - 2015-07-20 02:59:56
#     control logic in progress
# 982cf7e: jay - 2015-07-20 00:41:27
#     logical
# 93ebeda: Hoi Cheng - 2015-07-16 12:53:40
#     buttons man
# 07e4aeb: jay - 2015-07-16 01:56:21
#     exciting things
# 62b0edf: jay - 2015-07-16 01:37:43
#     in progress
# 435ce9f: Hoi Cheng - 2015-07-02 12:55:48
#     bugfix
# $log_end_tag$
#
from __future__ import print_function
import sys
from PyQt5.QtWidgets import (
    QApplication, QFileDialog, QMainWindow, QTreeView)
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QPixmap, QMovie
from PyQt5.QtCore import QThread, QObject, pyqtSignal, Qt, QItemSelectionModel
from PyQt5 import uic
from hdf_organizer import *
try:
    from imaging import *
    gen_sp_dict = open('dictionary.txt').read().splitlines()
    mode = 0
except:
    print('Imaging hardware not set up')
    mode = 1

#PAGE NUMBER ENUMS FOR mainwindow.ui
MAIN_MENU = 0
VIEWER = 1
INPUT_EXP = 2
RUN_EXP = 3

class HardwareThread(QThread):

    def run(self):
        global filepath
        if mode is not 1:
            filepath = imaging.run_sequence()
            print(session_path)
            print(filepath)
            hdf.save_image(session_path, filepath)

class Prida(QApplication):

    def __init__(self):
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()
        self.view = uic.loadUi('mainwindow.ui', self.base)

        if mode is not 1:
            self.hardware_thread = HardwareThread()
            self.hardware_thread.finished.connect(self.update_progress)

        self.model = QStandardItemModel(0, 8)
        #self.load_session_headers()
        self.view.sheet.setModel(self.model)

        self.movie = QMovie('monsters.gif')
        self.view.label_2.setMovie(self.movie)
        self.movie.start()
        #self.view.page_2.setStyleSheet('background-color: blue')

        self.p_meta_model = QStandardItemModel(0,2)
        self.p_meta_model.setHeaderData(0, Qt.Horizontal, 'Property')
        self.p_meta_model.setHeaderData(1, Qt.Horizontal, 'Value')
        self.p_meta_model.setItem(0, 0, QStandardItem('PID'))
        self.p_meta_model.setItem(1, 0, QStandardItem('Genus Species'))
        self.p_meta_model.setItem(2, 0, QStandardItem('Line'))
        self.p_meta_model.setItem(3, 0, QStandardItem('Media'))
        self.p_meta_model.setItem(4, 0, QStandardItem('Germination Date'))
        self.p_meta_model.setItem(5, 0, QStandardItem('Transplant Date'))
        self.p_meta_model.setItem(6, 0, QStandardItem('Rep Num'))
        self.p_meta_model.setItem(7, 0, QStandardItem('Treatment'))
        self.p_meta_model.setItem(8, 0, QStandardItem('Tub Size'))
        self.p_meta_model.setItem(9, 0, QStandardItem('Tub ID'))
        self.p_meta_model.setItem(10, 0, QStandardItem('Nutrient'))
        self.p_meta_model.setItem(11, 0, QStandardItem('Growth Temp (Day)'))
        self.p_meta_model.setItem(12, 0, QStandardItem('Growth Temp (Night)'))
        self.p_meta_model.setItem(13, 0, QStandardItem('Lighting Conditions'))
        self.p_meta_model.setItem(14, 0, QStandardItem('Watering schedule'))
        self.p_meta_model.setItem(15, 0, QStandardItem('Plant Notes'))
        self.view.plant_meta.setModel(self.p_meta_model)

        #self.view.sheet.collapsed.connect(lambda e: self.resize())
        #self.view.sheet.expanded.connect(lambda e: self.resize())
        #self.view.sheet.setAnimated(True)
        self.resize()
        self.view.c_buttons.accepted.connect(self.new_session)
        self.view.c_buttons.rejected.connect(lambda: self.view.stackedWidget.setCurrentIndex(VIEWER))
        #self.view.next.clicked.connect(self.new_hdf)#lambda e: self.view.stackedWidget.setCurrentIndex(1))
        #gen_sp_dict = open('dictionary.txt').read().splitlines()
        if mode is not 1:
            for line in gen_sp_dict:
                self.view.c_gen_sp.addItem(line)

        self.select_model = self.view.sheet.selectionModel()
        self.select_model.selectionChanged.connect(self.v_update_pid_meta)

        self.view.new_hdf.clicked.connect(self.new_hdf)
        self.view.open_hdf.clicked.connect(self.open_hdf)
        self.view.create_session.clicked.connect(lambda e: self.view.stackedWidget.setCurrentIndex(INPUT_EXP))
        #print self.hdf.list_pids()
        if mode is not 1:
            self.base.setWindowTitle('Prida 0.4')
        else:
            self.base.setWindowTitle('Prida 0.4 - Explorer Mode')
        #self.view.sheet.selectionChanged.connect(lambda: print("G"))
        #self.base.showMaximized()
        self.base.show()

    def load_session_headers(self):
        self.model.setHeaderData(0, Qt.Horizontal, 'Name')
        self.model.setHeaderData(1, Qt.Horizontal, 'Plant Age')
        self.model.setHeaderData(2, Qt.Horizontal, 'Session Date')
        self.model.setHeaderData(3, Qt.Horizontal, 'Shutter Speed')
        self.model.setHeaderData(4, Qt.Horizontal, 'Aperture')
        self.model.setHeaderData(5, Qt.Horizontal, 'Exposure')
        self.model.setHeaderData(6, Qt.Horizontal, 'Number of Images')
        self.model.setHeaderData(7, Qt.Horizontal, 'Session Notes')

    def update_progress(self):
        self.view.c_progress.setValue(imaging.get_count())
        image = QPixmap(filepath)
        self.view.c_preview.setPixmap(image.scaledToHeight(300))
        if not imaging.is_running:
            imaging.reset_count()
            self.populate_pids()
            self.v_clear_pid_meta()
            self.view.stackedWidget.setCurrentIndex(VIEWER)
        else:
            self.hardware_thread.start(QThread.TimeCriticalPriority)

    def v_clear_pid_meta(self):
        self.p_meta_model.setItem(0, 1, QStandardItem(''))
        self.p_meta_model.setItem(1, 1, QStandardItem(''))
        self.p_meta_model.setItem(2, 1, QStandardItem(''))
        self.p_meta_model.setItem(3, 1, QStandardItem(''))
        self.p_meta_model.setItem(4, 1, QStandardItem(''))
        self.p_meta_model.setItem(5, 1, QStandardItem(''))
        self.p_meta_model.setItem(6, 1, QStandardItem(''))
        self.p_meta_model.setItem(7, 1, QStandardItem(''))
        self.p_meta_model.setItem(8, 1, QStandardItem(''))
        self.p_meta_model.setItem(9, 1, QStandardItem(''))
        self.p_meta_model.setItem(10, 1, QStandardItem(''))
        self.p_meta_model.setItem(11, 1, QStandardItem(''))
        self.p_meta_model.setItem(12, 1, QStandardItem(''))
        self.p_meta_model.setItem(13, 1, QStandardItem(''))
        self.p_meta_model.setItem(14, 1, QStandardItem(''))
        self.p_meta_model.setItem(15, 1, QStandardItem(''))

    def v_update_pid_meta(self, selection):
        if (self.model.itemFromIndex(selection.indexes()[0]).parent() == None):
            pid = self.model.itemFromIndex(selection.indexes()[0]).text()
        else:
            pid = self.model.itemFromIndex(selection.indexes()[0]).parent().text()
        data = hdf.get_pid_attrs(pid)
        item = QStandardItem()
        self.p_meta_model.setItem(0, 1, QStandardItem(pid))
        self.p_meta_model.setItem(1, 1, QStandardItem(data['gen_sp']))
        self.p_meta_model.setItem(2, 1, QStandardItem(data['line']))
        self.p_meta_model.setItem(3, 1, QStandardItem(data['media']))
        self.p_meta_model.setItem(4, 1, QStandardItem(data['germdate']))
        self.p_meta_model.setItem(5, 1, QStandardItem(data['transdate']))
        self.p_meta_model.setItem(6, 1, QStandardItem(data['rep_num']))
        self.p_meta_model.setItem(7, 1, QStandardItem(data['treatment']))
        self.p_meta_model.setItem(8, 1, QStandardItem(data['tubsize']))
        self.p_meta_model.setItem(9, 1, QStandardItem(data['tubid']))
        self.p_meta_model.setItem(10, 1, QStandardItem(data['nutrient']))
        self.p_meta_model.setItem(11, 1, QStandardItem(data['growth_temp_day']))
        self.p_meta_model.setItem(12, 1, QStandardItem(data['growth_temp_night']))
        self.p_meta_model.setItem(13, 1, QStandardItem(data['growth_light']))
        self.p_meta_model.setItem(14, 1, QStandardItem(data['water_sched']))
        self.p_meta_model.setItem(15, 1, QStandardItem(data['notes']))

    def populate_pids(self):
        self.model.clear()
        self.model.setColumnCount(8)
        self.load_session_headers()
        for pid in hdf.list_pids():
            pid_item = QStandardItem(pid)
            for session in hdf.list_sessions(pid):
                s_path = '/'+pid+'/'+session
                data = hdf.get_session_attrs(s_path)
                pid_item.appendRow([QStandardItem(session),
                                    QStandardItem(data['age_num']),
                                    QStandardItem(data['date']),
                                    QStandardItem(data['cam_shutter']),
                                    QStandardItem(data['cam_aperture']),
                                    QStandardItem(data['cam_exposure']),
                                    QStandardItem(data['img_taken']),
                                    QStandardItem(data['notes'])])
            self.model.appendRow(pid_item)
        self.resize()
        #print (self.hdf.pids)
        #print(pid_item.parent())

    def new_session(self):
        global session_path
        if mode is not 1:
            session_path = hdf.create_session(
                str(self.view.c_pid.text()),
                {'gen_sp':str(self.view.c_gen_sp.currentText()),
                'line':str(self.view.c_line.text()),
                'media':str(self.view.c_media.text()),
                'germdate':str(self.view.c_germdate.text()),
                'transdate':str(self.view.c_transdate.text()),
                'rep_num':str(self.view.c_rep_num.text()),
                'treatment':str(self.view.c_treatment.text()),
                'tubsize':str(self.view.c_tubsize.text()),
                'tubid':str(self.view.c_tubid.text()),
                'nutrient':str(self.view.c_nutrient.text()),
                'growth_temp_day':str(self.view.c_growth_temp_day.text()),
                'growth_temp_night':str(self.view.c_growth_temp_night.text()),
                'growth_light':str(self.view.c_growth_light.text()),
                'water_sched':str(self.view.c_water_sched.text()),
                'notes':str(self.view.c_plant_notes.text())},
                {'user':str(self.view.c_session_user.text()),
                'addr':str(self.view.c_session_addr.text()),
                'date':str(self.view.c_session_date.text()),
                'number':str(self.view.c_session_number.text()),
                'img_taken':str(self.view.c_num_images.text()),
                'age_num':str(self.view.c_plant_age.text()),
                'notes':str(self.view.c_session_notes.text()),
                'cam_shutter':str(self.view.c_cam_shutter.text()),
                'cam_aperture':str(self.view.c_cam_aperture.text()),
                'cam_exposure':str(self.view.c_cam_exposure.text())})
            imaging.set_num_photos(int(self.view.c_num_images.text()))
            self.view.c_progress.setMinimum(0)
            self.view.c_progress.setMaximum(int(imaging.num_photos))
            self.view.stackedWidget.setCurrentIndex(RUN_EXP)
            self.hardware_thread.start(QThread.TimeCriticalPriority)

    def resize(self):
        for col in xrange(8):
            self.view.sheet.resizeColumnToContents(col)

    def start_rep(self):
        self.started = 1
        self.motor.set_num_photos(self.hdf.get_rep_images())
        self.hardware_thread.start(QThread.TimeCriticalPriority)

    def check_continue(self):
        if self.started and self.camera.get_count() < self.hdf.get_rep_images():
            self.hardware_thread.start(QThread.TimeCriticalPriority)
        else:
            self.cancel_rep()

    def cancel_rep(self):
        self.camera.reset_count()
        self.motor.turn_off_motors()
        self.started = 0

    def new_hdf(self):
        popup = uic.loadUi('input_user.ui')
        if popup.exec_():
            path = QFileDialog.getSaveFileName()[0]
            if path != '':
                if path[-5:] != '.hdf5':
                    path += '.hdf5'
                hdf.open_file(path)
                hdf.set_root_user(str(popup.user.text()))
                hdf.set_root_addr(str(popup.email.text()))
                #self.view.stackedWidget.setCurrentIndex(1)
                self.populate_pids()
                self.view.stackedWidget.setCurrentIndex(VIEWER)

    def open_hdf(self):
        path = QFileDialog.getOpenFileName(filter='*.hdf5')[0]
        if path != '':
            hdf.open_file(path)
            self.populate_pids()
            self.view.stackedWidget.setCurrentIndex(VIEWER)

    def update_exp_details(self):
        self.view.exp_title.setText(self.hdf.get_exp_title())
        self.view.genus_species.setText(self.hdf.get_exp_genus_species())

    def clear_exp_details(self):
        self.view.exp_title.setText('')
        self.view.genus_species.setText('')

    def update_rep_details(self):
        self.view.date.setText(str(self.hdf.get_rep_date()))
        self.view.rep_number.setText(str(self.hdf.get_rep_number()))
        self.view.rep_images.setText(str(self.hdf.get_rep_images()))
        self.view.cultivar.setText(self.hdf.get_rep_cultivar())

    def clear_rep_details(self):
        self.view.date.setText('')
        self.view.rep_number.setText('')
        self.view.rep_images.setText('')
        self.view.cultivar.setText('')

    def update_user(self):
        self.view.user.setText(self.hdf.get_root_user())
        self.view.email.setText(self.hdf.get_root_addr())

    def open_exp(self, exp_index):
        self.hdf.open_exp(exp_index.data())
        self.update_rep_list()
        self.clear_rep_details()
        self.update_exp_details()

    def open_rep(self, rep_index):
        self.hdf.open_rep(rep_index.data())
        #self.update_
        self.update_rep_details()

    def new_exp(self):
        popup = uic.loadUi('input_exp.ui')
        if popup.exec_():
            self.hdf.new_exp()
            self.hdf.set_exp_title(str(popup.exp_title.text()))
            self.hdf.set_exp_genus_species(str(popup.genus_species.text()))
            self.clear_exp_details()
            self.clear_rep_details()
            self.update_exp_list()
            self.update_rep_list()

    def new_rep(self):
        popup = uic.loadUi('input_rep.ui')
        if popup.exec_():
            self.hdf.new_rep()
            self.hdf.set_rep_date(popup.date.date().toPyDate())
            self.hdf.set_rep_number(int(popup.rep_number.text()))
            self.hdf.set_rep_images(int(popup.rep_images.text()))
            self.hdf.set_rep_cultivar(str(popup.cultivar.text()))
            self.clear_rep_details()
            self.update_rep_list()

    def update_exp_list(self):
        self.exp_list.clear()
        for item in self.hdf.list_exps():
            self.exp_list.appendRow(QStandardItem(item))
        self.view.exp_list.setModel(self.exp_list)

    def update_rep_list(self):
        self.rep_list.clear()
        for item in self.hdf.list_reps():
            self.rep_list.appendRow(QStandardItem(item))
        self.view.rep_list.setModel(self.rep_list)

    def clear_rep_list(self):
        self.rep_list.clear()
        self.view.rep_list.setModel(self.rep_list)

if __name__ == '__main__':
    #sys.exit(Prida().exec_())
    hdf = PridaHDF()
    if mode is not 1:
        imaging = Imaging()
    app = Prida()
    app.exec_()
    if mode is not 1:
        app.hardware_thread.wait()
    hdf.close()
    print('properly exit')
    sys.exit()

