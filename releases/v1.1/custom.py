#!/usr/bin/python
#
# custom.py
#
# VERSION: 1.1.0
#
# LAST EDIT: 2015-11-20
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import os.path
import sys
import logging

import numpy
import scipy.misc
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPainter
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QColor
import PIL.ImageQt as ImageQt
import PIL.Image as Image


###############################################################################
# FUNCTIONS:
###############################################################################
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = sys.path[0]

    return os.path.join(base_path, relative_path)


###############################################################################
# CLASSES:
###############################################################################
class InteractiveLabel(QLabel):
    """
    Name:     InteractiveLabel
    Features: Click and drag and selection widget
    History:  Version 1.0.1-dev
              - added image filename as default init parameter [15.10.16]
              - PEP8 style fixes [15.11.18]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, stuff, ifile='greeter.jpg'):
        """
        Name:     InteractiveLabel.__init__
        Inputs:   - QtWidgets.QWidget, (stuff)
                  - str, greeter image file (ifile)
        Features: Initializes an InteractiveLabel.
                  * press_pos is the stored mouse press location
                  * clean_img is last image before mouse press
                  * base_img is the original image
                  * active_img is the image currently being used
                  * is_pressed indicates mouse pressed status
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('start.')
        QLabel.__init__(self)
        self.press_pos = None
        self.greeter = resource_path(ifile)
        self.clean_img = QPixmap(self.greeter).scaledToHeight(300)
        self.base_img = QPixmap(self.greeter).scaledToHeight(300)
        self.active_img = None
        self.is_pressed = False
        self.painter = QPainter()
        self.logger.debug('complete.')

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def mousePressEvent(self, event):
        """
        Name:     InteractiveLabel.mousePressEvent
        Input:    (event)
        Output:   None.
        Features: On left mouse click, the mouse location is saved and the
                  pressed status is set to true
        """
        self.logger.debug('start.')
        if event.button() == Qt.LeftButton:
            self.press_pos = event.pos()
            self.is_pressed = True
        self.logger.debug('complete.')

    def mouseMoveEvent(self, event):
        """
        Name:     InteractiveLabel.mouseMoveEvent
        Input:    (event)
        Output:   None.
        Features: While mouse click is held down, reset the active image as the
                  clean image, then draw the new image and set the active image
                  to the label
        """
        self.logger.debug('start.')
        if self.is_pressed:
            self.active_img = self.clean_img.copy()
            self.painter.begin(self.active_img)
            # self.painter.setPen(Qt.blue)
            self.painter.fillRect(self.press_pos.x(),
                                  self.press_pos.y(),
                                  event.x() - self.press_pos.x(),
                                  event.y() - self.press_pos.y(),
                                  QColor(128, 128, 255, 128))
            self.painter.end()
            self.setPixmap(self.active_img)
        self.logger.debug('complete.')

    def mouseReleaseEvent(self, event):
        """
        Name:     InteractiveLabel.mouseReleaseEvent
        Features: When left mouse click is released, set the active image as
                  the new clean image. When right mouse click is released, set
                  the shown and clean image to the base image.
        """
        self.logger.debug('start.')
        if event.button() == Qt.LeftButton:
            self.logger.debug(
                "Start: " + str(self.press_pos))
            self.logger.debug("End: " + str(event.pos()))
            if self.is_pressed:
                if (event.x() > self.pixmap().size().width() or
                        event.y() > self.pixmap().size().height() or
                        event.x() < 0 or event.y() < 0):
                    self.setPixmap(self.clean_img)
                else:
                    self.clean_img = self.active_img
                self.is_pressed = False
        else:
            self.setPixmap(self.base_img)
            self.clean_img = self.base_img
        self.logger.debug('complete.')


class IDA(QLabel):
    """
    Name:     IDA (Image Display Area)
    Features: The label that displays the photos in full size
    History:  Version 1.0.1-dev
              - addressed portrait images in resizeAndSet [15.10.16]
              - created rotate base image left and right functions [15.11.19]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, arg):
        """
        Name:     IDA.__init__
        Inputs:   (arg)
        Outputs:  None.
        Features: Initializes IDA
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('start.')
        QLabel.__init__(self)
        self.base_img = None
        self.cur_image = None
        self.logger.debug('complete.')

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def setBaseImage(self, ndarray_full):
        """
        Name:     IDA.setBaseImage
        Inputs:   numpy.ndarray, image (ndarray_full)
        Outputs:  None.
        Features: Sets and saves base image for viewing
        """
        self.logger.debug('setting base image')
        self.base_img = ndarray_full
        self.resizeAndSet()

    def resizeAndSet(self):
        """
        Name:     IDA.resizeAndSet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes and sets image
        """
        self.logger.debug('start.')
        # Calculate the image aspect ratio:
        h, w, _ = self.base_img.shape
        ratio = (1.0 * w) / h

        # Calculate the scaling factor for the image:
        height = float(self.width()) / ratio
        if height > self.height():
            width = float(self.height()) * ratio
            s_factor = float(width) / w
        else:
            s_factor = float(height) / h

        ndarray = scipy.misc.imresize(self.base_img, s_factor, 'bilinear')
        image = Image.fromarray(ndarray)
        qt_image = ImageQt.ImageQt(image)
        self.cur_image = qt_image
        pix = QPixmap.fromImage(self.cur_image)
        self.setPixmap(pix)
        self.logger.debug('complete.')

    def resizeEvent(self, e):
        """
        Name:     IDA.resizeEvent
        Inputs:   (e)
        Outputs:  None.
        Features: Upon resize events, resize and set image
        """
        self.logger.debug('start.')
        if self.base_img is not None:
            self.resizeAndSet()
        self.logger.debug('complete.')

    def rotateBaseImageLeft(self):
        """
        Name:     IDA.rotateBaseImageLeft
        Inputs:   None.
        Outputs:  None.
        Features: Rotates base image 90 degrees counter-clockwise
        """
        self.logger.info('Button Clicked')
        if self.base_img is not None:
            self.logger.debug('rotating')
            self.base_img = numpy.rot90(self.base_img)
            self.resizeAndSet()

    def rotateBaseImageRight(self):
        """
        Name:     IDA.rotateBaseImageRight
        Inputs:   None.
        Outputs:  None.
        Features: Rotates base image 90 degrees clockwise
        """
        self.logger.info("Button Clicked")
        if self.base_img is not None:
            self.logger.debug("rotating")
            self.base_img = numpy.rot90(self.base_img, 3)
            self.resizeAndSet()
