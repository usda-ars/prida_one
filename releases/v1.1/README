README
======

PRIDA version 1.1


LICENSE
=======

U.S. Government work not protected by U.S. copyright.


CONTENTS
========

Adafruit_I2C.py
    This is a part of the Python library for interfacing with the Adafruit Motor HAT for Raspberry Pi to control Stepper motors with single, double, interleave and microstepping.
    Based on the script written by Limor Fried for Adafruit Industries.

Adafruit_MotorHAT.py
    This is a part of the Python library for interfacing with the Adafruit Motor HAT for Raspberry Pi to control Stepper motors with single, double, interleave and microstepping.
    Based on the script written by Limor Fried for Adafruit Industries.

Adafruit_PWM_Servo_Driver.py
    This is a part of the Python library for interfacing with the Adafruit Motor HAT for Raspberry Pi to control Stepper motors with single, double, interleave and microstepping.
    Based on the script written by Limor Fried for Adafruit Industries.

Prida.py
    Python module for running the PRIDA software.
    Configures the software logging and PRIDA app.
    Includes Prida class, HardwareThread class, and StreamToLogger class definitions.

about.ui
    Qt Designer XML file for the About pop-up window.

camera.py
    Camera class definition used for communicating with and operating the digital camera via gphoto2.

custom.py
    InteractiveLabel and IDA custom widget class definitions.

dictionary.txt
    Plain text file with crop common and scientific names used in the Genus species dropdown selection field.

greeter.jpg
    Image file used for PRIDA software welcome screen.

hdf_organizer.py
    PridaHDF class definition used for handling IO operations with PRIDA HDF5 project files.

imaging.py
    Imaging class definition used for controlling the motor, camera and other hardware peripherals necessary for 2D and 3D imaging modes.

input_user.ui
    Qt Designer XML file for new/edit project file attributes pop-up window.

led.py
    LED class definition used for configuring indicator LEDs on the stepper motor HAT.

mainwindow.ui
    Qt Designer XML file for the PRIDA software GUI.

motor.py
    Motor class definition used for communicating with the Adafruit stepper motor HAT.

piezo.py
    Piezo class definition used for sounding a piezo indicator buzzer on the stepper motor HAT.

prida.config
    Configuration parameter values used by imaging.py for PRIDA hardware peripherals.

pridaperipheral.py
    PRIDAPeripheral and PRIDAPeripheralError class definitions for defining PRIDA hardware peripheral behaviors and exception handling.


CHANGELOG
=========
2015-11-20: v1.1.0
            new release snapshot
            improved logging messages

2015-11-19: v1.0.1
            Multi search file selection
            Preserve session user and email
            Preserve empty string inputs
            Auto-completion of existing PID attributes
            Stderror and stdout logging
            Debug, info, error and warning message logging
            IDA image preview rotation

2015-10-30: v1.0.0
            Edit feature
            PID auto-completer feature
            Exif tag handling

2015-10-21: v0.7.0
            Import data feature
            Export data feature
            Inter-file search feature

2015-09-30: v0.6.1
            About field
            Python 2/3 compatibility
            PRIDA repository goes public

2015-09-09: v0.5.8
            Fixed IDA image distortion for real

2015-08-29: v0.5.7
            Improved search performance
            Fixed IDA image distortion

2015-08-14: v0.5.6
            Search script implemented
            IDA implemented

2015-08-07: v0.5.5
            Thumbnail Previewer is now live
            Removed Image Display Area

2015-08-06: v0.5.4
            Image Display Area under implementation

2015-08-04: v0.5.3
            Experimental lasso tool implemented

2015-07-31: v0.5.2
            Imaging class now integrated into main program
            Fixed data display issues

2015-07-30: v0.5.1
            Swapped pid and session display scheme
            Renamed variables for readability
            All columns now automatically resize
            Gen_sp dropdown now loads regardless of mode
            Integrating new imaging class into main program (*not tested*)
            Code cleanup

2015-07-29: v0.4.2
            Bugfixes

2015-07-21: v0.4.1
            Progress bar!
            Image Preview!
            Sheet-style hdf5 browser
            Many new metadata fields added
            Imaging class (camera/motor class rework)

2015-07-02: v0.3.12
            Motor properly integrated into interface

2015-07-01: v0.3.11
            Basic GUI for HDF5 file control has been set up
            GUI appearance update

2015-06-27: v0.3.2
            Now using PyQt5 for GUI
            Program flow resolved (Model/View Signal/Slot)

2015-06-24: v0.2.5
            Structural rework: control classes being implemented

2015-06-23: v0.2.4
            PRV GUI update
            Bug fixes

2015-06-22: v0.2.1
            Major structural rework
            PRV GUI under construction

2015-06-16: v0.1.2
            Fixed some bugs
            Separated date field into month, day, and year
            Implemented date validation

2015-06-16: v0.1.0
            Camera class structural rework
            Camera capture now keeps a counter on
              number of photos taken
            Camera capture now saves each new capture to disk
            Motor step size now adjusts to the
              specified number of photos to take
            Data acquisition will now stop after the
              specified number of photos have been taken
            Added calibration function to Motor, which
              takes a small step to orientate motor
            KNOWN ISSUE: GUI freezes upon start of data acquisition

2015-06-14: v0.0.3
            Added HDF export function
            Overall appearance update

2015-06-13: v0.0.2
            Added fields for all metadata
            Added session validation
            Appearance update for main frame
            Appearance update for metadata frame

2015-06-12: v0.0.1
            Initial commit
            Camera functions: init/connect/capture
            Motor functions: step/turnoff
            Initial GUI setup: mainframe, metadata frame,
              picture preview frame, console frame
            Added support for jpeg display
