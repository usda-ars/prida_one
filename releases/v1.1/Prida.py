#!/usr/bin/python2
#
# Prida.py
#
# VERSION: 1.1.1-r1
#
# LAST EDIT: 2016-03-15
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import sys
import logging
import os
import glob

import PyQt5.uic as uic
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QAbstractItemView
from PyQt5.QtWidgets import QCompleter
# from PyQt5.QtWidgets import QErrorMessage # @TODO
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QStringListModel
from PyQt5.QtCore import QThread
from PyQt5.QtCore import QSize
from PyQt5.QtCore import QDate
from PyQt5.QtCore import Qt     # used in set_input_field (as string)
import PIL.ImageQt as ImageQt
import PIL.Image as Image
import PIL.ExifTags as ExifTags

from hdf_organizer import PridaHDF


###############################################################################
# DETECT HARDWARE MODE:
###############################################################################
try:
    from imaging import Imaging
    hardware_mode = True
except ImportError:
    # MODE 1 INDICATES ERROR LIKELY FROM NONEXISTENCE OF HARDWARE
    hardware_mode = False

###############################################################################
# GLOBAL VARIABLES:
###############################################################################
# PRIDA VERSION
PRIDA_VERSION = 'Prida 1.1.1-r1'
PRIDA_VERSION_EXPL = PRIDA_VERSION + ' - Explorer Mode'

# PAGE NUMBER ENUMS FOR mainwindow.ui
MAIN_MENU = 0
VIEWER = 1
INPUT_EXP = 2
RUN_EXP = 3
FILE_SEARCH = 4

# PAGE NUMBER ENUMS FOR SHEET/IDA
SHEET_VIEW = 0
IDA_VIEW = 1

# FILE & SESSION PATHS FOR IMAGES AND HDF FILE
filepath = ''
session_path = ''
is_path = False


###############################################################################
# FUNCTIONS:
###############################################################################
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = sys.path[0]

    return os.path.join(base_path, relative_path)


###############################################################################
# CLASSES:
###############################################################################
class HardwareThread(QThread):
    """
    Name:     HardwareThread
    Features: Thread for running hardware
    History:  Version 1.1.0
              - changed attrs_path to a_path [15.11.02]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def run(self):
        """
        Name:     HardwareThread.run
        Inputs:   None.
        Outputs:  None.
        Features: Run sequence and save image to HDF file
        """
        global filepath, is_path
        if hardware_mode:
            output_args = imaging.run_sequence()
            is_path = output_args[0]
            filepath = output_args[1]
            datestamp = output_args[2]
            datestamp = os.path.basename(datestamp)
            timestamp = output_args[3]
            anglestamp = output_args[4]
            anglestamp = os.path.splitext(anglestamp)[0]
            basename = os.path.basename(filepath)
            name = os.path.splitext(basename)[0]
            a_path = os.path.join(session_path, name, basename)
            if is_path:
                hdf.save_image(session_path, filepath)
                hdf.set_attr('Date', datestamp, a_path)
                hdf.set_attr('Time', timestamp, a_path)
                hdf.set_attr('Angle', anglestamp, a_path)
                hdf.set_attr('Orientation', imaging.camera.orientation, a_path)
                hdf.set_attr('Height', imaging.camera.image_height, a_path)
                hdf.set_attr('Width', imaging.camera.image_width, a_path)
                hdf.save()


class Prida(QApplication):
    """
    Name:      Prida
    Features:  Prida main class
    History:   Version 1.1.1
               - PEP8 style fixes [15.11.02]
               - cleared RUN_EXP image between sessions [15.11.03]
               - added HDF5 filename to Prida window title and About [15.11.18]
               - added multifile selection in file search [15.11.18]
               - added defaults to session user and email attrs [15.11.18]
               - added defaults to all pid & session attrs [15.11.18]
               - created a Prida logger [15.11.18]
               - created init functions for pid & session attrs [15.11.18]
               - started PID autofill function [15.11.18]
               - moved set defaults to open and new hdf [15.11.19]
               - connected rotate left and right IDA buttons [15.11.19]
               - created log messages [15.11.19]
                 + view changes and button clicks assigned at 'info' level
               - created read and save exif tag functions [15.11.19]
               - fixed import data function [15.12.07]
    @TODO:     * access image file creation/modification date if datetime
                 exif tag does not exist
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    pid_attrs = {}
    session_attrs = {}

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     Prida.__init__
        Inputs:   None.
        Outputs:  None.
        Depends:  - load_session_sheet_headers     - new_hdf
                  - new_session                    - open_hdf
                  - resize_sheet                   - show_ida
                  - to_sheet                       - update_progress
                  - update_session_sheet_model     - update_sheet_model
                  - set_defaults                   - about
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # Create a logger for Prida:
        self.logger = logging.getLogger(__name__)

        # Initialize PID and session attribute dictionaries:
        self.init_pid_attrs()
        self.init_session_attrs()
        self.sheet_items = len(self.pid_attrs)
        self.session_sheet_items = len(self.session_attrs)

        # Check that the necessary files exist:
        # NOTE: mainwindow.ui depends on class definitions from custom.py
        self.main_ui = resource_path('mainwindow.ui')
        self.user_ui = resource_path('input_user.ui')
        self.about_ui = resource_path('about.ui')
        self.gs_dict = resource_path('dictionary.txt')
        self.greeter = resource_path('greeter.jpg')
        # self.icon = resource_path('icon.jpg')

        if (not os.path.isfile(self.main_ui) or
                not os.path.isfile(self.user_ui) or
                not os.path.isfile(self.about_ui)):
            self.logger.error("missing required ui files!")
            raise IOError("Error! Missing required ui files!")
        if not os.path.isfile(self.gs_dict):
            self.logger.error("missing dictionary file!")
            raise IOError("Error! Missing dictionary file!")
        if not os.path.isfile(self.greeter):
            self.logger.error("missing welcome screen image!")
            raise IOError("Error! Missing welcome screen image!")
        # if not os.path.isfile(self.icon):
        #    raise IOError("Error! Missing Prida icon!")

        # Load main window GUI to the QMainWindow:
        self.view = uic.loadUi(self.main_ui, self.base)

        # Set the welcome image and icon:
        self.view.welcome.setPixmap(QPixmap(self.greeter).scaledToHeight(300))

        # Create the VIEWER sheet model (where PIDs and sessions are listed)
        self.logger.debug('creating sheet model')
        self.sheet_model = QStandardItemModel()
        self.view.sheet.setModel(self.sheet_model)
        self.view.sheet.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.sheet.collapsed.connect(lambda: self.resize_sheet())
        self.view.sheet.expanded.connect(lambda: self.resize_sheet())
        self.view.sheet.setAnimated(True)

        # Create the VIEWER session sheet model (for session properties)
        self.logger.debug('creating session sheet model')
        self.session_sheet_model = QStandardItemModel()
        self.view.session_sheet.setModel(self.session_sheet_model)
        self.view.session_sheet.setRootIsDecorated(False)
        self.load_session_sheet_headers()

        # Create FILE_SEARCH sheet model (where files + PIDs are listed)
        self.logger.debug('creating search sheet model')
        self.search_sheet_model = QStandardItemModel()
        self.view.file_search_sheet.setModel(self.search_sheet_model)
        self.load_file_search_sheet_headers()

        # Create the FILE_SEARCH file browse model (for displaying files)
        self.logger.debug('creating file sheet model')
        self.file_sheet_model = QStandardItemModel()
        self.view.file_browse_sheet.setModel(self.file_sheet_model)
        self.load_file_browse_sheet_headers()

        # Create VIEWER image sheet model (for thumbnail previewer):
        self.logger.debug('creating image sheet model')
        self.img_sheet_model = QStandardItemModel()
        self.view.img_select.setModel(self.img_sheet_model)
        self.view.img_select.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.img_select.setIconSize(QSize(100, 100))

        # Populate INPUT_EXP Genus Species dropdown menu:
        self.logger.debug('loading Genus Species dictionary')
        for line in open(self.gs_dict).read().splitlines():
            self.view.c_gen_sp.addItem(line)

        if hardware_mode:
            # Hardware Mode Decorator:
            self.prida_title = PRIDA_VERSION
            self.base.setWindowTitle(PRIDA_VERSION)
            self.logger.warning('Hardware Mode Enabled')

            # Spawn a hardware thread if mode is enabled:
            self.logger.debug('creating hardware thread')
            self.hardware_thread = HardwareThread()
            self.hardware_thread.finished.connect(self.update_progress)

            # Disable "Import Data" button (only allow "Create Session")
            # and set VIEWER action for "OK" and "Create Session" buttons:
            self.view.import_data.setEnabled(False)
            self.view.c_buttons.accepted.connect(self.new_session)
            self.view.create_session.clicked.connect(
                lambda: self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
            )

            # Set default values based on imaging's camera
            self.session_attrs[8]['def_val'] = imaging.camera.make
            self.session_attrs[9]['def_val'] = imaging.camera.model
            self.session_attrs[10]['def_val'] = imaging.camera.exposure_time
            self.session_attrs[11]['def_val'] = imaging.camera.aperture
            self.session_attrs[12]['def_val'] = imaging.camera.iso_speed
        else:
            # Explorer Mode Decorator:
            self.prida_title = PRIDA_VERSION_EXPL
            self.base.setWindowTitle(PRIDA_VERSION_EXPL)
            self.logger.warning('Preview Mode Enabled')

            # Disable "Create Session" button, set VIEWER action for
            # "Import Data" button & INPUT_EXP action for "OK" button:
            self.view.create_session.setEnabled(False)
            self.view.import_data.clicked.connect(self.import_data)
            self.view.c_buttons.accepted.connect(self.import_session)

        # Set MAIN_MENU actions for "New," "Open," & "Search" buttons:
        self.view.new_hdf.clicked.connect(self.new_hdf)
        self.view.open_hdf.clicked.connect(self.open_hdf)
        self.view.search_hdf.clicked.connect(
            lambda: self.view.stackedWidget.setCurrentIndex(FILE_SEARCH)
        )

        # Set VIEWER actions for "Export Data," "Main Menu," "About," and
        # "Edit" buttons and for edited search text;
        # also disable "Perform Analysis" button until further notice:
        self.view.export_data.clicked.connect(self.export)
        self.view.menuButton.clicked.connect(self.back_to_menu)
        self.view.aboutButton.clicked.connect(self.about)
        self.view.make_edit.clicked.connect(self.edit_field)
        self.view.search.textEdited.connect(self.update_sheet_model)
        self.view.perform_analysis.setEnabled(False)

        # Set IDA_SHEET actions for "Back to Spreadsheet," "Rotate Left," and
        # "Rotate Right" buttons:
        self.view.to_sheet.clicked.connect(self.to_sheet)
        self.view.rotateLeft.clicked.connect(self.view.ida.rotateBaseImageLeft)
        self.view.rotateRight.clicked.connect(
            self.view.ida.rotateBaseImageRight)

        # Set INPUT_EXP action for "Cancel" button and for edited PID text:
        self.view.c_buttons.rejected.connect(self.cancel_input)
        self.view.c_pid.textEdited.connect(self.autofill_pid)

        # Create selection models for VIEWER sheet and img_sheet and for
        # FILE_SELECT file_browser_sheet:
        self.logger.debug('creating selection models')
        self.sheet_select_model = self.view.sheet.selectionModel()
        self.img_sheet_select_model = self.view.img_select.selectionModel()
        self.file_select_model = self.view.file_browse_sheet.selectionModel()

        # Set selection model actions for selection changes:
        self.img_sheet_select_model.selectionChanged.connect(self.show_ida)
        self.sheet_select_model.selectionChanged.connect(
            self.update_session_sheet_model
        )

        # Set FILE_SEARCH actions for "Main Menu," "+/-," and "Explore" buttons
        # and action for when search text is edited:
        self.view.menuButton2.clicked.connect(self.back_to_menu)
        self.view.add_file.clicked.connect(self.add_search_file)
        self.view.rm_file.clicked.connect(self.rm_search_file)
        self.view.explore_file.clicked.connect(self.explore_file)
        self.view.f_search.textEdited.connect(self.update_file_search_sheet)

        # Create a auto-completion model for PIDs:
        self.logger.debug('creating auto-completion model')
        self.pid_completer = QCompleter()
        self.view.c_pid.setCompleter(self.pid_completer)
        self.pid_auto_model = QStringListModel()
        self.pid_completer.setModel(self.pid_auto_model)

        # Initialize the search dictionaries, list of PID metadata, list of
        # thumbnail images, session create/edit boolean, input field defaults
        # and current image:
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.img_list = []
        self.editing = False
        self.current_img = None        # not currently used

        self.base.show()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def about(self):
        """
        Name:     Prida.about
        Inputs:   None.
        Outputs:  None.
        Features: Produces the file's About popup dialog box
        """
        self.logger.info('Button Clicked')
        self.logger.debug('loading UI file')
        popup = uic.loadUi(self.about_ui)

        self.logger.debug('getting about info')
        overview = hdf.get_about()

        # Set popup values to default string if get_about fails:
        self.logger.debug('assigning UI fields')
        popup.author_about.setText(overview.get('Author', 'Unknown'))
        popup.contact_about.setText(overview.get('Contact', 'Unknown'))
        popup.plants_about.setText(overview.get('Plants', '?'))
        popup.sessions_about.setText(overview.get('Sessions', '?'))
        popup.photos_about.setText(overview.get('Photos', '?'))
        popup.summary_about.setText(overview.get('Summary', '?'))
        popup.setWindowTitle("About: %s" % (hdf.basename()))
        if popup.exec_():
            popup.repaint()

    def add_search_file(self):
        """
        Name:     Prida.add_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Adds user-defined HDF5 files to search files dictionary and
                  saves the files' PID attributes to search data dictionary
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.info('Button Clicked')
        self.logger.debug('requesting search files')
        items = QFileDialog.getOpenFileNames(None,
                                             "Select one or more files",
                                             os.path.expanduser("~"),
                                             "HDF5 files (*.hdf5)")
        paths = items[0]
        if paths:
            for path in paths:
                self.logger.debug('processing selection %s', path)
                f_name = os.path.basename(path)
                f_name = os.path.splitext(f_name)[0]
                f_path = os.path.dirname(path)
                if f_name not in self.search_files:
                    # Add file to file_sheet_model:
                    self.logger.debug('adding search file')
                    self.search_files[f_name] = f_path
                    f_list = [QStandardItem(f_name), QStandardItem(f_path)]
                    self.file_sheet_model.appendRow(f_list)

                    if hdf.isopen:
                        self.logger.debug('closing open HDF5 file')
                        hdf.close()

                    # Add file's data to search_sheet_model:
                    self.logger.debug('opening search file')
                    hdf.open_file(path)
                    self.logger.debug('reading PID attributes')
                    for pid in hdf.list_pids():
                        # Create a unique dictionary key for each file's PIDs:
                        my_key = "%s.%s" % (f_name, pid)
                        # Initialize the sheet's row data:
                        my_data = [f_name, pid]
                        for i in self.pid_attrs.keys():
                            my_attr = self.pid_attrs[i]['key']
                            my_attr_val = hdf.get_attr(
                                my_attr, os.path.join('/', str(pid)))
                            # Append PID attribute to row:
                            my_data.append(my_attr_val)
                        # Save search file's PID attributes:
                        self.search_data[my_key] = my_data
                    self.logger.debug('closing search file')
                    hdf.close()
            self.logger.debug('clearing FILE_SEARCH search bar')
            self.view.f_search.clear()
            self.update_file_search_sheet()
            self.update_file_browse_sheet()

    def autofill_pid(self, pid=''):
        """
        Name:     Prida.autofill_pid
        Inputs:   [optional] str, plant ID (pid)
        Outputs:  None.
        Features: Autofills the PID metadata if PID exists
        """
        if pid in hdf.list_pids():
            self.logger.debug('performing auto-completion')
            p_path = "/%s" % (pid)
            for j in self.pid_attrs:
                f_name = self.pid_attrs[j]['qt_val']
                f_type = self.pid_attrs[j]['qt_type']
                f_value = hdf.get_attr(self.pid_attrs[j]['key'], p_path)
                self.set_input_field(f_name, f_type, f_value)

    def back_to_menu(self):
        """
        Name:     Prida.back_to_menu
        Inputs:   None
        Outputs:  None
        Features: Returns to the menu screen, closing the HDF5 file handle and
                  clearing session and sheet data.
        Depends:  - clear_session_sheet_model
                  - init_pid_attrs
                  - init_session_attrs
                  - update_file_browse_sheet
                  - update_file_search_sheet
                  - update_sheet_model
        """
        self.logger.info('Button Clicked')
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.init_pid_attrs()
        self.init_session_attrs()
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.editing = False
        self.update_sheet_model()
        self.update_file_browse_sheet()
        self.update_file_search_sheet()
        self.logger.info('change view to MAIN_MENU')
        self.view.stackedWidget.setCurrentIndex(MAIN_MENU)
        self.base.setWindowTitle(self.prida_title)
        hdf.close()

    def back_to_sheet(self):
        """
        Name:     Prida.back_to_sheet
        Inputs:   None
        Outputs:  None
        Features: Returns the current view to VIEWER cleaning up all the sheets
        Depends:  - clear_session_sheet_model
                  - update_sheet_model
        """
        self.logger.debug('returning to VIEWER')
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.update_sheet_model()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.logger.info('change view to SHEET_VIEW')
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.logger.info('change view to VIEWER')
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def cancel_input(self):
        """
        Name:     Prida.cancel_input
        Inputs:   None
        Outpus:   None
        Features: Exits INPUT_EXP, resetting editing boolean, enabling all
                  QLineEdit fields, and returns to VIEWER
        Depends:  enable_all_attrs
        """
        self.logger.info('Button Clicked')
        self.editing = False
        self.enable_all_attrs()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.logger.info('change view to SHEET_VIEW')
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.logger.info('change view to VIEWER')
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def clear_session_sheet_model(self):
        """
        Name:     Prida.clear_session_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Clears session sheet model
        Depends:  resize_session_sheet
        """
        self.logger.debug('clearing session sheet')
        for i in range(self.session_sheet_items):
            self.session_sheet_model.setItem(i, 1, QStandardItem(''))
        #
        self.resize_session_sheet()

    def disable_pid_attrs(self):
        """
        Name:     Prida.disable_pid_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with PID attributes
        """
        if self.editing:
            self.logger.debug('disabling INPUT_EXP fields')
            self.view.c_pid.setEnabled(False)
            self.view.c_num_images.setEnabled(False)
            for k in self.pid_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.pid_attrs[k]['qt_val'])
                )

    def disable_session_attrs(self):
        """
        Name:     Prida.disable_session_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with session attributes
        """
        if self.editing:
            self.logger.debug('disabling INPUT_EXP fields')
            self.view.c_pid.setEnabled(False)
            for k in self.session_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.session_attrs[k]['qt_val'])
                )

    def edit_field(self):
        """
        Name:     Prida.edit_field
        Inputs:   None
        Outputs:  None
        Features: Performs INPUT_EXP field disabling based on the selection in
                  the sheet model, applies appropriate meta data to INPUT_EXP
                  fields, sets editing boolean to True and changes current
                  view to INPUT_EXP
        Depends:  - disable_pid_attrs
                  - disable_session_attrs
                  - set_defaults
                  - set_input_field
        """
        self.logger.info('Button Clicked')
        if self.sheet_select_model.hasSelection():
            self.logger.debug('set editing to True')
            self.editing = True
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session, disable PID attrs:
                self.disable_pid_attrs()

                # Get session attrs & set them to INPUT_EXP:
                pid = my_selection.parent().text()
                session = my_selection.text()
                p_path = "/%s" % (pid)
                s_path = "/%s/%s" % (pid, session)

                self.logger.debug('setting INPUT_EXP fields for PID')
                self.set_input_field('c_pid', 'QLineEdit', str(pid))
                for j in self.pid_attrs:
                    f_name = self.pid_attrs[j]['qt_val']
                    f_type = self.pid_attrs[j]['qt_type']
                    f_value = hdf.get_attr(self.pid_attrs[j]['key'], p_path)
                    self.set_input_field(f_name, f_type, f_value)

                self.logger.debug('setting INPUT_EXP fields for session')
                for k in self.session_attrs:
                    f_name = self.session_attrs[k]['qt_val']
                    f_type = self.session_attrs[k]['qt_type']
                    f_value = hdf.get_attr(
                        self.session_attrs[k]['key'], s_path)
                    self.set_input_field(f_name, f_type, f_value)
            else:
                # Selected PID, disable session attrs:
                self.disable_session_attrs()

                # Get PID attrs & set them to INPUT_EXP:
                pid = my_selection.text()
                s_path = "/%s" % (pid)

                self.set_defaults()
                self.logger.debug('setting INPUT_EXP fields for PID')
                self.set_input_field('c_pid', 'QLineEdit', str(pid))
                for k in self.pid_attrs:
                    f_name = self.pid_attrs[k]['qt_val']
                    f_type = self.pid_attrs[k]['qt_type']
                    f_value = hdf.get_attr(self.pid_attrs[k]['key'], s_path)
                    self.set_input_field(f_name, f_type, f_value)
            self.logger.info('change view to INPUT_EXP')
            self.view.stackedWidget.setCurrentIndex(INPUT_EXP)

    def enable_all_attrs(self):
        """
        Name:     Prida.enable_all_attrs
        Inputs:   None
        Outputs:  None
        Features: Enables all INPUT_EXP fields
        """
        self.logger.debug('enabling all INPUT_EXP fields')
        self.view.c_pid.setEnabled(True)
        for k in self.pid_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.pid_attrs[k]['qt_val'])
            )

        for j in self.session_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.session_attrs[j]['qt_val'])
            )

    def explore_file(self):
        """
        Name:     Prida.explore_file
        Inputs:   None.
        Outputs:  None.
        Features: Opens the sheet VIEW with the currently selected file from
                  the search file browser
        Depends:  - get_data
                  - update_sheet_model
        """
        self.logger.info('Button Clicked')
        if self.file_select_model.hasSelection():
            self.logger.debug('gathering selection details')
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0]
            )
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            if hdf.isopen:
                self.logger.debug('closing open HDF5 file')
                hdf.close()

            self.logger.debug('opening selected file')
            hdf.open_file(f_path)
            self.get_data()
            self.update_sheet_model()
            if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                self.logger.info('change view to SHEET_VIEW')
                self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
            self.logger.info('change view to VIEWER')
            self.view.stackedWidget.setCurrentIndex(VIEWER)

            # Set session user and contact defaults:
            self.logger.debug('saving session defaults')
            self.session_attrs[0]['def_val'] = str(hdf.get_root_user())
            self.session_attrs[1]['def_val'] = str(hdf.get_root_addr())
            self.logger.debug('updating window title')
            self.base.setWindowTitle(
                "%s: %s" % (self.prida_title, hdf.basename())
            )

    def export(self):
        """
        Name:     Prida.export
        Inputs:   None.
        Outputs:  None.
        Features: Exports the images from a given selection to a user-defined
                  output directory recreating the HDF5 folder structure, else
                  exports all images and meta data
        Depends:  get_sheet_model_selection
        """
        self.logger.info('Button Clicked')
        self.logger.debug('gathering user selection')
        if self.sheet_select_model.hasSelection():
            self.logger.debug('getting user selection')
            s_path = self.get_sheet_model_selection()
        else:
            self.logger.debug('no selection, exporting from root')
            s_path = '/'

        self.logger.debug('requesting output diretory from user')
        path = QFileDialog.getExistingDirectory(None,
                                                'Select an output directory:',
                                                os.path.expanduser("~"),
                                                QFileDialog.ShowDirsOnly)
        if os.path.isdir(path):
            try:
                self.logger.debug('extracting attributes')
                hdf.extract_attrs(path)
            except IOError:
                self.logger.warning('failed to export attributes to %s', path)
            except:
                self.logger.error('attributes FAILED!')

            try:
                self.logger.debug('extracting datasets')
                hdf.extract_datasets(s_path, path)
            except IOError:
                self.logger.warning('failed to export datasets to %s', path)
            except:
                self.logger.error('datasets FAILED!')

    def get_data(self):
        """
        Name:     Prida.get_data
        Inputs:   None.
        Outputs:  None.
        Features: Creates a list of attribute dictionaries for each PID in
                  the HDF5 file.
        """
        self.logger.debug('resetting data list')
        self.data_list = []

        self.logger.debug('reading PID attributes')
        for pid in hdf.list_pids():
            d = {}
            d['pid'] = pid
            for i in self.pid_attrs.keys():
                my_attr = self.pid_attrs[i]['key']
                d[my_attr] = hdf.get_attr(my_attr, os.path.join('/', str(pid)))
            self.data_list.append(d)

    def get_input_field(self, field_name, field_type):
        """
        Name:     Prida.get_input_field
        Inputs:   - str, Qt field name (field_name)
                  - str, Qt field type (field_type)
        Outputs:  str, current input text (field_value)
        Features: Get the current text from a Qt input box
        """
        err_msg = "get_input_field:could not get Qt %s, %s" % (
            field_type, field_name)

        if field_type == 'QComboBox':
            try:
                self.logger.debug('getting QComboBox current text')
                field_value = eval("self.view.%s.currentText()" % (field_name))
            except:
                self.logger.error(err_msg)
                raise NameError(err_msg)
        elif (field_type == 'QDateEdit' or
              field_type == 'QLineEdit' or
              field_type == 'QSpinBox'):
            # All three fields use text() getter
            try:
                self.logger.debug('getting field text')
                field_value = eval("self.view.%s.text()" % (field_name))
            except:
                self.logger.error(err_msg)
                raise NameError(err_msg)
        else:
            self.logger.error(err_msg)
            raise NameError(err_msg)

        return field_value

    def get_sheet_model_selection(self):
        """
        Name:     Prida.get_sheet_model_selection
        Inputs:   None
        Outputs:  str, HDF5 group path based on selection (s_path)
        Features: Returns sheet model selection path for HDF5 file
        """
        if self.sheet_select_model.hasSelection():
            self.logger.debug('gathering user selection')
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session
                self.logger.debug('returning session')
                pid = my_selection.parent().text()
                session = my_selection.text()
                s_path = "/%s/%s" % (pid, session)
            else:
                # Selected PID
                self.logger.debug('returning PID')
                pid = my_selection.text()
                s_path = "/%s" % (pid)

            return s_path

    def import_data(self):
        """
        Name:     Prida.import_data
        Inputs:   None.
        Outputs:  None.
        Features: Reads a user-defined directory for images, attempts to
                  extract image exif tags and defines them in INPUT_EXP view
        Depends:  set_input_field
        @TODO:    - save dataset attributes (i.e., image height, width, and
                    orientation)
                  - set view to RUN_EXP during image import
        """
        self.logger.info('Button Clicked')
        self.logger.debug('requesting directory from user')
        path = QFileDialog.getExistingDirectory(
            None,
            'Select directory containing images:',
            os.path.expanduser("~"),
            QFileDialog.ShowDirsOnly
        )
        if os.path.isdir(path):
            # Populate default values and overwrite where possible:
            self.set_defaults()

            # Currently filters search for only jpg and tif image files
            self.logger.debug('searching diretory for images')
            s_path = os.path.join(path, '*[(.jpg)(.tif)]')
            my_files = glob.glob(s_path)
            files_found = len(my_files)
            self.logger.info('importing %d files', files_found)

            if files_found > 0:
                # Reset file names for each processing:
                self.logger.debug('resetting search file dictionary')
                self.search_files = {}
                self.logger.debug('reading images found')
                for my_file in my_files:
                    try:
                        self.logger.debug('opening image %s', my_file)
                        img = Image.open(my_file)
                    except:
                        self.logger.error('could not open file %s', my_file)
                        raise IOError('Error! Could not open image')
                    else:
                        self.search_files[my_file] = 0
                        exif = self.read_exif_tags(img)
                #
                exif['NumPhotos'] = str(files_found)
                self.save_exif_tags(exif)
                self.logger.info('change view to INPUT_EXP')
                self.view.stackedWidget.setCurrentIndex(INPUT_EXP)

    def read_exif_tags(self, img):
        """
        Inputs:  PIL image (img)
        Outputs: dict, exif tags (my_tags)
        @TODO
        """
        # Define Exif Tags:
        my_tags = {'Make': '',                 # Exif Tag (IFD0) 271
                   'Model': '',                # Exif Tag (IFD0) 272
                   'Orientation': '',          # Exif Tag (IFD0) 274
                   'DateTime': '',             # Exif Tag (IFD0) 306
                   'ISOSpeedRatings': '',      # Exif Tag (SubIFD) 34855
                   'ShutterSpeedValue': '',    # Exif Tag (SubIFD) 37377
                   'ApertureValue': ''}        # Exif Tag (SubIFD) 37378
        try:
            self.logger.debug('gathering exif tags')
            exif = {ExifTags.TAGS[k]: v
                    for k, v in img._getexif().items()
                    if k in ExifTags.TAGS}
        except AttributeError:
            self.logger.debug('no exif tags found!')
            exif = {}

        self.logger.debug('processing exif tags')
        for tag in my_tags:
            try:
                self.logger.debug('reading tag %s', tag)
                my_attr = exif.get(tag, '')
            except KeyError:
                self.logger.debug('encountered key error')
            else:
                if isinstance(my_attr, tuple):
                    self.logger.debug('tag is a tuple')
                    self.logger.debug('reading first tuple entry')
                    my_attr = str(my_attr[0])
                    self.logger.debug('found tag %s', my_attr)
                else:
                    my_attr = str(my_attr)
                    self.logger.debug('found tag %s', my_attr)

                if str(tag) == 'DateTime':
                    try:
                        self.logger.debug('parsing datetime tag')
                        im_date = my_attr.split(' ')[0]
                        my_attr = im_date.replace(':', '-')
                        self.logger.debug('found tag %s',
                                          my_attr)
                    except:
                        self.logger.debug('datetime parsing failed')
                        my_attr = ''
                self.logger.debug('saving tag to dictionary')
                my_tags[tag] = my_attr
        return my_tags

    def save_exif_tags(self, exif):
        """
        Inputs:   dictionary, exif tages (exif)
        Features: Save exif tag values to INPUT_EXP fields
        @TODO
        """
        # Pseudo-intelligent search for session_attr index:
        tag_attr = {
            'Make': [k for k, v in self.session_attrs.items()
                     if v['key'] == 'cam_make'],
            'Model': [k for k, v in self.session_attrs.items()
                      if v['key'] == 'cam_model'],
            'ShutterSpeedValue': [k for k, v in self.session_attrs.items()
                                  if v['key'] == 'cam_shutter'],
            'ApertureValue': [k for k, v in self.session_attrs.items()
                              if v['key'] == 'cam_aperture'],
            'ISOSpeedRatings': [k for k, v in self.session_attrs.items()
                                if v['key'] == 'cam_exposure'],
            'DateTime': [k for k, v in self.session_attrs.items()
                         if v['key'] == 'date'],
            'NumPhotos': [k for k, v in self.session_attrs.items()
                          if v['key'] == 'num_img']
        }

        for tag in exif:
            if tag in tag_attr:
                self.logger.debug('setting INPUT_EXP field')
                j = tag_attr[tag][0]
                f_name = self.session_attrs[j]['qt_val']
                f_type = self.session_attrs[j]['qt_type']
                f_val = exif[tag]
                self.set_input_field(f_name, f_type, f_val)

    def import_session(self):
        """
        Name:     Prida.import_session
        Inputs:   None.
        Outputs:  None.
        Features: Creates a new session based on imported data
        Depends:  - get_data
                  - update_sheet_model
        """
        self.logger.info('Button Clicked')
        if self.editing:
            self.logger.info('Saving Edits')
            self.save_edits()
            self.logger.debug('set editing to False')
            self.editing = False
            self.enable_all_attrs()
            self.set_defaults()
        else:
            # Create PID metadata dictionary:
            self.logger.debug('building PID dictionary')
            pid_dict = {}
            for i in self.pid_attrs.keys():
                f_key = self.pid_attrs[i]['key']
                f_name = self.pid_attrs[i]['qt_val']
                f_type = self.pid_attrs[i]['qt_type']
                f_val = self.get_input_field(f_name, f_type)
                self.pid_attrs[i]['def_val'] = f_val
                pid_dict[f_key] = f_val

            # Create session metadata dictionary:
            self.logger.debug('import_session:building session dictionary')
            session_dict = {}
            for j in self.session_attrs.keys():
                f_key = self.session_attrs[j]['key']
                f_name = self.session_attrs[j]['qt_val']
                f_type = self.session_attrs[j]['qt_type']
                f_val = self.get_input_field(f_name, f_type)
                self.session_attrs[j]['def_val'] = f_val
                session_dict[f_key] = f_val

            # Create new session:
            self.logger.info('Saving New Session')
            s_path = hdf.create_session(
                str(self.view.c_pid.text()), pid_dict, session_dict
                )

            # Save images to HDF5 and flush memory to file:
            # @TODO: go to RUN_EXP and update progress bar & preview?
            for filepath in sorted(list(self.search_files.keys())):
                self.logger.debug('saving image %s', filepath)
                hdf.save_image(s_path, filepath)
            hdf.save()

        # Return to sheet:
        self.get_data()
        self.back_to_sheet()

    def init_pid_attrs(self):
        """
        Name:     Prida.init_pid_attrs
        Inputs:   None.
        Outputs:  None.
        Features: Initializes the PID attribute dictionary
        """
        self.logger.debug('initializing dictionary')
        self.pid_attrs = {
            1: {'title': 'Genus Species',
                'key': 'gen_sp',
                'qt_val': 'c_gen_sp',
                'def_val': '',
                'qt_type': 'QComboBox'},
            2: {'title': 'Line',
                'key': 'line',
                'qt_val': 'c_line',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            3: {'title': 'Rep Num',
                'key': 'rep_num',
                'qt_val': 'c_rep_num',
                'def_val': '',
                'qt_type': 'QSpinBox'},
            4: {'title': 'Tub ID',
                'key': 'tubid',
                'qt_val': 'c_tubid',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            5: {'title': 'Germination Date',
                'key': 'germdate',
                'qt_val': 'c_germdate',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            6: {'title': 'Transplant Date',
                'key': 'transdate',
                'qt_val': 'c_transdate',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            7: {'title': 'Treatment',
                'key': 'treatment',
                'qt_val': 'c_treatment',
                'def_val': 'Control',
                'qt_type': 'QLineEdit'},
            8: {'title': 'Media',
                'key': 'media',
                'qt_val': 'c_media',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            9: {'title': 'Tub Size',
                'key': 'tubsize',
                'qt_val': 'c_tubsize',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            10: {'title': 'Nutrient',
                 'key': 'nutrient',
                 'qt_val': 'c_nutrient',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            11: {'title': 'Growth Temp (Day)',
                 'key': 'growth_temp_day',
                 'qt_val': 'c_growth_temp_day',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            12: {'title': 'Growth Temp (Night)',
                 'key': 'growth_temp_night',
                 'qt_val': 'c_growth_temp_night',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            13: {'title': 'Lighting Conditions',
                 'key': 'growth_light',
                 'qt_val': 'c_growth_light',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            14: {'title': 'Watering schedule',
                 'key': 'water_sched',
                 'qt_val': 'c_water_sched',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            15: {'title': 'Damage',
                 'key': 'damage',
                 'qt_val': 'c_damage',
                 'def_val': 'No',
                 'qt_type': 'QLineEdit'},
            16: {'title': 'Plant Notes',
                 'key': 'notes',
                 'qt_val': 'c_plant_notes',
                 'def_val': 'N/A',
                 'qt_type': 'QLineEdit'}
        }

    def init_session_attrs(self):
        """
        Name:     Prida.init_session_attrs
        Inputs:   None.
        Outputs:  None.
        Features: Initializes session attribute dictionary
        """
        self.logger.debug('initializing dictionary')
        self.session_attrs = {
            0: {'title':  'Session User',
                'key': 'user',
                'qt_val': 'c_session_user',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            1: {'title': 'Session Email',
                'key': 'addr',
                'qt_val': 'c_session_addr',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            2: {'title': 'Session Title',
                'key': 'number',
                'qt_val': 'c_session_number',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            3: {'title': 'Session Date',
                'key': 'date',
                'qt_val': 'c_session_date',
                'def_val': '',
                'qt_type': 'QDateEdit'},
            4: {'title': 'Rig Name',
                'key': 'rig',
                'qt_val': 'c_session_rig',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            5: {'title': 'Image Count',
                'key': 'num_img',
                'qt_val': 'c_num_images',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            6: {'title': 'Plant Age',
                'key': 'age_num',
                'qt_val': 'c_plant_age',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            7: {'title': 'Notes',
                'key': 'notes',
                'qt_val': 'c_session_notes',
                'def_val': 'N/A',
                'qt_type': 'QLineEdit'},
            8: {'title': 'Camera Make',
                'key': 'cam_make',
                'qt_val': 'c_cam_make',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            9: {'title': 'Camera Model',
                'key': 'cam_model',
                'qt_val': 'c_cam_model',
                'def_val': '',
                'qt_type': 'QLineEdit'},
            10: {'title': 'Exposure Time',
                 'key': 'cam_shutter',
                 'qt_val': 'c_cam_shutter',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            11: {'title': 'Aperture',
                 'key': 'cam_aperture',
                 'qt_val': 'c_cam_aperture',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
            12: {'title': 'ISO',
                 'key': 'cam_exposure',
                 'qt_val': 'c_cam_exposure',
                 'def_val': '',
                 'qt_type': 'QLineEdit'},
        }

    def load_file_browse_sheet_headers(self):
        """
        Name:     Prida.load_file_browse_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the file_sheet_model
        Depends:  resize_file_browse_sheet
        """
        self.logger.debug('loading headers')
        self.file_sheet_model.setColumnCount(2)
        self.file_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File Name'))
        self.file_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Path'))
        self.resize_file_browse_sheet()

    def load_file_search_sheet_headers(self):
        """
        Name:     Prida.load_file_search_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the search_sheet_model
        Depends:  resize_search_sheet
        """
        self.logger.debug('loading headers')
        self.search_sheet_model.setColumnCount(self.sheet_items + 1)
        self.search_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File'))
        self.search_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('PID'))
        for i in self.pid_attrs:
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.search_sheet_model.setHorizontalHeaderItem", (i + 1),
                self.pid_attrs[i]['title']))
        self.resize_search_sheet()

    def load_session_sheet_headers(self):
        """
        Name:     Prida.load_session_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the session_sheet_model
        Depends:  resize_session_sheet
        """
        self.logger.debug('loading headers')
        self.session_sheet_model.setColumnCount(2)
        self.session_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('Property'))
        self.session_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Value'))
        #
        # Define session sheet rows in column 1:
        for i in range(self.session_sheet_items):
            exec('%s(%d, 0, QStandardItem("%s"))' % (
                "self.session_sheet_model.setItem", i,
                self.session_attrs[i]['title']))
        self.resize_session_sheet()

    def load_sheet_headers(self):
        """
        Name:     Prida.load_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the sheet_model
        Depends:  resize_sheet
        """
        self.logger.debug('load_sheet_headers:loading headers')
        self.sheet_model.setColumnCount(self.sheet_items)
        self.sheet_model.setHorizontalHeaderItem(0, QStandardItem('PID'))
        for i in self.pid_attrs.keys():
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.sheet_model.setHorizontalHeaderItem", i,
                self.pid_attrs[i]['title']))
        self.resize_sheet()

    def new_hdf(self):
        """
        Name:     Prida.new_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Gets user information, gets HDF5 file name, creates a new
                  HDF5 file, stores user information, and goes to VIEWER page
        Depends:  - get_data
                  - update_sheet_model
        """
        self.logger.info('Button Clicked')
        self.logger.debug('loading UI file')
        popup = uic.loadUi(self.user_ui)
        if popup.exec_():
            self.logger.debug('requesting new file name from user')
            path = QFileDialog.getSaveFileName(None,
                                               "Save File",
                                               os.path.expanduser("~"))[0]
            if path != '':
                if path[-5:] != '.hdf5':
                    path += '.hdf5'
                self.logger.debug('opening new file')
                hdf.open_file(path)
                self.logger.debug('saving about info to file')
                sess_user = str(popup.user.text())
                sess_addr = str(popup.email.text())
                sess_about = str(popup.about.toPlainText())
                hdf.set_root_user(sess_user)
                hdf.set_root_addr(sess_addr)
                hdf.set_root_about(sess_about)
                self.logger.debug('saving file')
                hdf.save()
                self.get_data()
                self.update_sheet_model()
                if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
                    self.logger.info('change view to SHEET_VIEW')
                    self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
                self.logger.info('change view eto VIEWER')
                self.view.stackedWidget.setCurrentIndex(VIEWER)

                # Set session user and contact default values:
                self.logger.debug('saving session defaults')
                self.session_attrs[0]['def_val'] = str(hdf.get_root_user())
                self.session_attrs[1]['def_val'] = str(hdf.get_root_addr())
                self.logger.debug('updating window title')
                self.base.setWindowTitle(
                    "%s: %s" % (self.prida_title, hdf.basename())
                )
                self.set_defaults()

    def new_session(self):
        """
        Name:     Prida.new_session
        Inputs:   None.
        Outputs:  None
        Features: Creates HDF5 session, resets the progress bar, enters the
                  RUN_EXP screen, and begins a session
        """
        global session_path

        self.logger.info('Button Clicked')
        if self.editing:
            self.logger.info('Saving Edits')
            self.save_edits()
            self.logger.debug('set editing to False')
            self.editing = False
            self.enable_all_attrs()
            self.set_defaults()
            self.get_data()
            self.back_to_sheet()
        else:
            if hardware_mode:
                # Create the PID metadata dictionary:
                self.logger.debug('building PID dictionary')
                pid_dict = {}
                for i in self.pid_attrs.keys():
                    f_key = self.pid_attrs[i]['key']
                    f_name = self.pid_attrs[i]['qt_val']
                    f_type = self.pid_attrs[i]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    self.pid_attrs[i]['def_val'] = f_val
                    pid_dict[f_key] = f_val

                # Create the session metadata dictionary:
                self.logger.debug('buidling session dictionary')
                session_dict = {}
                for j in self.session_attrs.keys():
                    f_key = self.session_attrs[j]['key']
                    f_name = self.session_attrs[j]['qt_val']
                    f_type = self.session_attrs[j]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    self.session_attrs[j]['def_val'] = f_val
                    session_dict[f_key] = f_val

                # Create a new session:
                self.logger.info(':Saving New Session')
                session_path = hdf.create_session(str(self.view.c_pid.text()),
                                                  pid_dict,
                                                  session_dict)

                # Update imaging class:
                self.logger.debug('updating imaging')
                imaging.cur_pid = str(self.view.c_pid.text())
                imaging.num_photos = int(self.view.c_num_images.text())

                # Prepare for RUN_EXP image capture and preview:
                self.logger.debug('preparing progress bar')
                self.view.c_preview.clear()
                self.view.c_progress.setMinimum(0)
                self.view.c_progress.setMaximum(int(imaging.find_end_pos()) +
                                                int(10 * imaging.num_photos))
                self.logger.info('change view to RUN_EXP')
                self.view.stackedWidget.setCurrentIndex(RUN_EXP)
                self.logger.debug('start hardware thread')
                self.hardware_thread.start(QThread.TimeCriticalPriority)

    def open_hdf(self):
        """
        Name:     Prida.open_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Opens an existing HDF5 file and goes to VIEWER page
        Depends:  - get_data
                  - back_to_sheet
                  - set_defaults
                  - update_pid_auto_model
        """
        self.logger.info('Button Clicked')
        self.logger.debug('request existing file from user')
        path = QFileDialog.getOpenFileName(None,
                                           "Select File",
                                           os.path.expanduser("~"),
                                           filter='*.hdf5')[0]
        if os.path.isfile(path):
            self.logger.debug('opening selected file')
            hdf.open_file(path)
            self.get_data()
            self.update_pid_auto_model()
            self.back_to_sheet()

            # Set session user and contact defaults:
            self.logger.debug('saving session defaults')
            self.session_attrs[0]['def_val'] = str(hdf.get_root_user())
            self.session_attrs[1]['def_val'] = str(hdf.get_root_addr())
            self.logger.debug('updating window title')
            self.base.setWindowTitle(
                "%s: %s" % (self.prida_title, hdf.basename())
            )
            self.set_defaults()

    def resize_file_browse_sheet(self):
        """
        Name:     Prida.resize_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_browse_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.file_browse_sheet.resizeColumnToContents(col)

    def resize_search_sheet(self):
        """
        Name:     Prida.resize_search_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_search_sheet
        """
        self.logger.debug('resizing')
        for col in range(self.sheet_items + 2):
            self.view.file_search_sheet.resizeColumnToContents(col)

    def resize_session_sheet(self):
        """
        Name:     Prida.resize_session_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the session_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.session_sheet.resizeColumnToContents(col)

    def resize_sheet(self):
        """
        Name:     Prida.resize_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the sheet
        """
        self.logger.debug('resizing')
        for col in range(self.sheet_items + 1):
            self.view.sheet.resizeColumnToContents(col)

    def rm_search_file(self):
        """
        Name:     Prida.rm_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Removes a file from the file browse view and its
                  associated PID rows from the file search view
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.info('Button Clicked')
        if self.file_select_model.hasSelection():
            # Get the selection and put together the file name and path:
            self.logger.debug('gathering user selection')
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0])
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            # Remove file from search_files:
            self.logger.debug('deleting file %s', f_name)
            del self.search_files[s_name]

            if hdf.isopen:
                self.logger.debug('closing open HDF5 file')
                hdf.close()

            # Remove file's PIDs from file_search_sheet:
            self.logger.debug('opening selected file')
            hdf.open_file(f_path)

            self.logger.debug('deleting PIDs from search data')
            for pid in hdf.list_pids():
                my_key = "%s.%s" % (s_name, pid)
                if my_key in self.search_data:
                    del self.search_data[my_key]
            self.logger.debug('closing selected file')
            hdf.close()
            self.view.f_search.clear()
            self.update_file_browse_sheet()
            self.update_file_search_sheet()
        else:
            self.logger.warning('nothing selected!')

    def save_edits(self):
        """
        Name:     Prida.save_edits
        Inputs:   None
        Outputs:  None
        Features: Saves new attributes to HDF5 file
        Depends:  get_sheet_model_selection
        """
        if self.editing:
            if self.sheet_select_model.hasSelection():
                self.logger.debug('gathering user selection')
                s_path = self.get_sheet_model_selection()

                self.logger.debug(('saving attributes from enabled ' +
                                   'INPUT_EXP fields'))
                for i in self.pid_attrs.keys():
                    f_key = self.pid_attrs[i]['key']
                    f_name = self.pid_attrs[i]['qt_val']
                    f_type = self.pid_attrs[i]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        self.pid_attrs[i]['def_val'] = f_val
                        hdf.set_attr(f_key, f_val, s_path)
                for j in self.session_attrs.keys():
                    f_key = self.session_attrs[j]['key']
                    f_name = self.session_attrs[j]['qt_val']
                    f_type = self.session_attrs[j]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        self.session_attrs[j]['def_val'] = f_val
                        hdf.set_attr(f_key, f_val, s_path)
                self.logger.debug('saving file')
                hdf.save()

    def search(self, pid_meta, terms):
        """
        Name:     Prida.search
        Inputs:   - list, PID meta data values (pid_meta)
                  - list, search terms (terms)
        Outputs:  bool (found)
        Features: Searches the pid metafields and returns true if all terms are
                  found anywhere within the fields
        """
        found = True
        for term in terms:
            found = found and any(term in m_field for m_field in pid_meta)
        return found

    def set_defaults(self):
        """
        Name:     Prida.set_defaults
        Inputs:   None.
        Outputs:  None.
        Features: Sets GUI text default values
        """
        self.logger.debug('assigning PID INPUT_EXP fields')
        for i in self.pid_attrs.keys():
            f_name = self.pid_attrs[i]['qt_val']
            f_type = self.pid_attrs[i]['qt_type']
            f_val = self.pid_attrs[i]['def_val']
            try:
                self.set_input_field(f_name, f_type, f_val)
            except:
                self.set_input_field(f_name, f_type, '')

        self.logger.debug('assigning session INPUT_EXP fields')
        for j in self.session_attrs.keys():
            f_name = self.session_attrs[j]['qt_val']
            f_type = self.session_attrs[j]['qt_type']
            f_val = self.session_attrs[j]['def_val']
            try:
                self.set_input_field(f_name, f_type, f_val)
            except:
                self.set_input_field(f_name, f_type, '')

    def set_input_field(self, field_name, field_type, field_value):
        """
        Name:     Prida.set_input_field
        Inputs:   - str, Qt input widget name (field_name)
                  - str, Qt input widget type (field_type)
                  - str, input value (field_value)
        Outputs:  None
        Features: Sets Qt input field
        """
        err_msg = "set_input_field:could not set Qt %s, %s, to value %s" % (
            field_type, field_name, field_value)

        if field_type == 'QComboBox':
            # Takes an integer, find it!
            if field_value == '':
                index = 0
            else:
                index = eval('self.view.%s.findText("%s", %s)' % (
                    field_name,
                    field_value,
                    'Qt.MatchFixedString')
                )
            try:
                self.logger.debug('setting QComboBox index to %d', index)
                exec("self.view.%s.setCurrentIndex(%d)" % (
                    field_name,
                    index)
                )
            except:
                self.logger.error(err_msg)
                raise ValueError(err_msg)
        elif field_type == 'QDateEdit':
            # Takes a QDate object
            if field_value == '':
                try:
                    self.logger.debug('setting QDateEdit to current date')
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        "QDate.fromString",
                        QDate.currentDate().toString("yyyy-MM-dd"))
                    )
                except:
                    self.logger.error(err_msg)
                    raise ValueError(err_msg)
            else:
                try:
                    self.logger.debug('setting QDate edit to %s', field_value)
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        'QDate.fromString',
                        field_value)
                    )
                except:
                    self.logger.error(err_msg)
                    raise ValueError(err_msg)
        elif field_type == 'QLineEdit':
            # Takes a string
            try:
                self.logger.debug('setting QLineEdit to %s', field_value)
                exec('self.view.%s.setText("%s")' % (
                    field_name,
                    field_value)
                )
            except:
                self.logger.error(err_msg)
                raise ValueError(err_msg)
        elif field_type == 'QSpinBox':
            # Takes an integer
            if field_value == '':
                field_value = 0
            try:
                self.logger.debug('setting QSpinBox to %d', int(field_value))
                exec("self.view.%s.setValue(%d)" % (
                    field_name,
                    int(field_value))
                )
            except:
                self.logger.error(err_msg)
                raise ValueError(err_msg)
        else:
            self.logger.error(err_msg)
            raise ValueError(err_msg)

    def show_ida(self, selection):
        """
        Name:     Prida.show_ida
        Input:    obj (selection)
        Output:   None.
        Features: Switches to IDA page and sets IDA base image
        """
        self.logger.info(selection.indexes())
        if selection.indexes():
            self.logger.info('change view to IDA_VIEW')
            self.view.stacked_sheets.setCurrentIndex(IDA_VIEW)
            self.logger.debug('gathering user selection')
            img_path = self.img_sheet_model.itemFromIndex(
                selection.indexes()[0]).data()
            self.logger.debug('get image data for selection')
            ndarray_full = hdf.get_dataset(img_path)
            self.logger.debug('set IDA base image to selection')
            self.view.ida.setBaseImage(ndarray_full)

    def to_sheet(self):
        """
        Name:     Prida.to_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Switches from IDA to sheet view
        """
        self.logger.info('Button Clicked')
        self.view.img_select.clearSelection()
        self.logger.info('change view to SHEET_VIEW')
        self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)

    def update_file_browse_sheet(self):
        """
        Name:     Prida.update_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Reloads the rows in the file_sheet_model
        Depends:  - load_file_browse_sheet_headers
                  - resize_file_browse_sheet
        """
        self.logger.debug('updating sheet')
        self.file_sheet_model.clear()
        self.load_file_browse_sheet_headers()
        for f_name in self.search_files:
            f_path = self.search_files[f_name]
            f_list = [QStandardItem(f_name), QStandardItem(f_path)]
            self.file_sheet_model.appendRow(f_list)
        self.resize_file_browse_sheet()

    def update_file_search_sheet(self, search=''):
        """
        Name:     Prida.update_file_search_sheet
        Inputs:   [optional] str, search string (search)
        Outputs:  None.
        Features: Reloads the rows in the search_sheet_model, filtered for
                  search terms
        Depends:  - load_file_search_sheet_headers
                  - resize_search_sheet
                  - search
        """
        self.logger.debug('updating sheet')
        self.search_sheet_model.clear()
        self.load_file_search_sheet_headers()
        terms = search.lower().split()
        for i in self.search_data:
            pid_meta = []
            for j in self.search_data[i]:
                pid_meta.append(j.lower())
            if self.search(pid_meta, terms):
                d_list = []
                for k in self.search_data[i]:
                    d_list.append(QStandardItem(k))
                self.search_sheet_model.appendRow(d_list)
        self.resize_search_sheet()

    def update_pid_auto_model(self):
        """
        Name:     Prida.update_pid_auto_model
        Inputs:   None
        Outputs:  None
        Features: Updates the QStringListModel with HDF5 file's current PIDs
        """
        if hdf.isopen:
            self.logger.debug('assigning PIDs to list')
            self.pid_auto_model.setStringList(hdf.list_pids())
        else:
            self.logger.debug('reset to empty list')
            self.pid_auto_model.setStringList([])

    def update_progress(self):
        """
        Name:     Prida.update_progress
        Inputs:   None.
        Outputs:  None.
        Features: Updates the progress bar, sets captured image to preview, and
                  decides the completion status of the session
        Depends:  - clear_session_sheet_model
                  - get_data
                  - update_sheet_model
        """
        self.logger.debug('update progress bar')
        self.view.c_progress.setValue(imaging.ms_count + (10 * imaging.taken))
        if is_path:
            self.logger.debug('set preview image')
            image = QPixmap(filepath)
            self.view.c_preview.setPixmap(image.scaledToHeight(300))
        if not imaging.is_running:
            self.logger.debug('imaging done')
            imaging.reset_count()
            self.get_data()
            self.back_to_sheet()
        else:
            self.logger.debug('start hardware thread')
            self.hardware_thread.start(QThread.TimeCriticalPriority)

    def update_session_sheet_model(self, selection):
        """
        Name:     Prida.update_session_sheet_model
        Inputs:   obj (selection)
        Outputs:  None.
        Features: Updates session sheet model with selected session's data,
                  creates the thumbnails and tags the full image paths, and
                  clears session sheet model if no sessions are selected
        Depends:  - clear_session_sheet_model
                  - resize_session_sheet
        """
        try:
            self.logger.debug('gathering user selection')
            my_session = self.sheet_model.itemFromIndex(selection.indexes()[0])
        except IndexError:
            # You made a de-selection (ctrl+click):
            self.logger.debug('encountered de-selection')
            self.clear_session_sheet_model()
            self.img_sheet_model.clear()
            self.img_list = []
            self.resize_session_sheet()
        else:
            if (my_session.parent() is not None):
                # Session selection
                self.logger.debug('session selected')
                pid = my_session.parent().text()
                session = my_session.text()
                s_path = "/%s/%s" % (pid, session)
                self.logger.debug('loading session fields')
                for i in range(self.session_sheet_items):
                    exec('%s(%d, 1, QStandardItem("%s"))' % (
                        "self.session_sheet_model.setItem", i,
                        hdf.get_attr(self.session_attrs[i]['key'], s_path)
                        )
                    )
                self.img_sheet_model.clear()
                self.img_list = []
                img_counter = 0
                if hdf.isopen:
                    self.logger.debug('reading session datasets')
                    for obj in hdf.list_objects(s_path):
                        img_counter += 1
                        thumb_path = "%s/%s/%s.thumb" % (s_path, obj, obj)
                        image_path = "%s/%s/%s.jpg" % (s_path, obj, obj)
                        self.logger.debug('getting data for %s', thumb_path)
                        ndarray = hdf.get_dataset(thumb_path)
                        image = Image.fromarray(ndarray)
                        qt_image = ImageQt.ImageQt(image)
                        self.img_list.append(qt_image)
                        pix = QPixmap.fromImage(qt_image)
                        self.logger.debug('saving thumb to image sheet model')
                        item = QStandardItem(str(img_counter))
                        item.setIcon(QIcon(pix))
                        item.setData(image_path)
                        self.img_sheet_model.appendRow(item)
            else:
                # PID selection
                self.logger.debug('PID selected')
                self.logger.debug(('clearing session and image sheets'))
                self.clear_session_sheet_model()
                self.img_sheet_model.clear()
                self.img_list = []
            self.resize_session_sheet()

    def update_sheet_model(self, search=''):
        """
        Name:     Prida.update_sheet_model
        Inputs:   [optional] str (search)
        Features: Displays all PIDs in the sheet or the PIDs that match the
                  search terms if search terms are given
        Depends:  - load_sheet_headers
                  - resize_sheet
                  - search
        """
        self.logger.debug('updating sheet')
        self.update_pid_auto_model()
        self.sheet_model.clear()
        self.load_sheet_headers()
        terms = search.lower().split()
        for data in self.data_list:
            pid_meta = []
            for key in data:
                pid_meta.append(data[key].lower())
            if self.search(pid_meta, terms):
                pid_item = QStandardItem(data['pid'])

                # Create session rows under each PID:
                for session in hdf.list_sessions(data['pid']):
                    pid_item.appendRow(QStandardItem(session))

                # Fill the PID row with its attributes:
                pid_list = [pid_item, ]
                for i in sorted(self.pid_attrs.keys()):
                    my_attr = data[self.pid_attrs[i]['key']]
                    pid_list.append(QStandardItem(my_attr))
                self.sheet_model.appendRow(pid_list)
        self.resize_sheet()


class StreamToLogger(object):
    """
    Name:     StreamToLogger
    Features: Logs stdout and stderr to log file
    History:  Version 1.0.1-dev
              - Based off of the Stream to Logger class written by Ferry
                Boender for logging stdout and stderr to a log file using the
                sys and logging python modules.
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        # self.line_buf = ''

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def write(self, buf):
        """
        Name:    StreamToLogger.write
        Feature: Writes the buffer to a log file. Used to make logging emulate
                 a file-like object.
        Inputs:  Str, buffer to write to the log (buf)
        Outputs: None
        """
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(self):
        """
        Name:    StreamToLogger.flush
        Feature: Dummy function to have logging emulate a file like object.
                 Actual flushing of the buffer handled within logging itself.
        Inputs:  None
        Outputs: None
        """
        pass

###############################################################################
# MAIN:
###############################################################################
if __name__ == '__main__':
    # Parse Command-Line Arguments (if any):
    if len(sys.argv) > 1 and sys.argv[1].upper() in {'--DEBUG', '--INFO',
                                                     '--WARNING', '--ERROR',
                                                     '--CRITICAL'}:
        log_level = 'logging.%s' % (sys.argv[1].upper()[2:len(sys.argv[1])])
    else:
        log_level = 'logging.INFO'

    # Configure Logging:
    root_logger = logging.getLogger()
    root_logger.setLevel(eval(log_level))

    fh = logging.FileHandler('prida.log', mode='w')
    fh.setLevel(eval(log_level))

    if 'DEBUG' in log_level:
        form = '%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s'
    else:
        form = ('%(asctime)s:%(levelname)s:' +
                '%(name)s:%(funcName)s:%(message)s')

    formatter = logging.Formatter(form, datefmt='%Y-%m-%d %H:%M:%S')

    fh.setFormatter(formatter)
    root_logger.addHandler(fh)

    stdout_logger = logging.getLogger('STDOUT')
    sl = StreamToLogger(stdout_logger, logging.INFO)
    sys.stdout = sl

    stderr_logger = logging.getLogger('STDERR')
    sl = StreamToLogger(stderr_logger, logging.ERROR)
    sys.stderr = sl

    hdf = PridaHDF()
    root_logger.info('Instantiated PridaHDF()')

    root_logger.info('Hardware Mode == %s' % (hardware_mode))
    if hardware_mode:
        imaging = Imaging()
    app = Prida()
    app.exec_()
    if hardware_mode:
        app.hardware_thread.wait()
    hdf_closed = hdf.close()
    if hdf_closed:
        root_logger.info('Closed PridaHDF()')
        sys.exit()
