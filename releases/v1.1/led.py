#!/usr/bin/python
#
# led.py
#
# VERSION: 1.1.0
#
# LAST EDIT: 2015-11-20
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 0b7102c: Nathanael Shaw - 2015-11-20 12:35:34
#     update headers, rmv func names from hdf logs
# 20eb8e2: Nathanael Shaw - 2015-11-20 10:49:42
#     remove func calls from debug msgs
# 5610e84: Nathanael Shaw - 2015-11-20 09:05:52
#     hotfix
# 0fb4b5a: Nathanael Shaw - 2015-11-19 13:42:27
#     minor logging updates
# 590b0d0: Nathanael Shaw - 2015-11-19 12:02:39
#     add logging to led
# 051e7a6: twdavis - 2015-10-30 14:13:54
#     v1.0.1
# 858afbb: Nathanael Shaw - 2015-10-30 09:42:21
#     version 1.0 snapshot
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
from time import sleep
import logging

from Adafruit_MotorHAT import Adafruit_MotorHAT
from pridaperipheral import PRIDAPeripheral
from pridaperipheral import PRIDAPeripheralError


###############################################################################
## CLASSES:
###############################################################################
class LED(Adafruit_MotorHAT, PRIDAPeripheral):
    """
    Class representing a tri-color (RGB) LED
    """

    def __init__(self):
        """
        Initialize PWM channels. Extends MotorHAT attributes and behaviours.
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('start.')
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self._BLUE = 1         # Default channel for the blue LED
        self._GREEN = 14       # Default channel for the green LED
        self._RED = 15         # Default channel for the red LED
        self._led_type = 'CC'  # Default type Common Anode
        self.logger.debug('complete.')

    @property
    def blue(self):
        """
        Name:    LED.blue
        Feature: Returns the integer designation of the blue LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the blue LED (self._BLUE)
        """
        return self._BLUE

    @property
    def green(self):
        """
        Name:    LED.green
        Feature: Returns the integer designation of the green LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the green LED (self._GREEN)
        """
        return self._GREEN

    @property
    def red(self):
        """
        Name:    LED.red
        Feature: Returns the integer designation of the red LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the red LED (self._RED)
        """
        return self._RED

    @property
    def led_type(self):
        """
        Name:    LED.led_type
        Feature: Returns string representing multicolor LED
                 configuration
        Inputs:  None
        Outputs: String, two letter designation representing either
                 'Common Cathode' or 'Common Anode' (self._led_type)
        """
        return self._led_type

    @led_type.setter
    def led_type(self, led_type):
        """
        Name:    LED.led_type
        Feature: Set type to common anode or common cathode.
        Inputs:  String, two letter type designation (led_type)
        Outputs: None
        """
        if led_type == 'CA' or led_type == 'CC':
            self._led_type = led_type
        else:
            self.logger.error('bad input. Values limited to CC or CA.')
            raise ValueError("Bad input. Values limited to CC or CA.")

    @blue.setter
    def blue(self, ch):
        """
        Name:    LED.blue
        Feature: Set the PWM channel controlling the blue led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._BLUE = ch

    @green.setter
    def green(self, ch):
        """
        Name:    LED.green
        Feature: Set the PWM channel controlling the green led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._GREEN = ch

    @red.setter
    def red(self, ch):
        """
        Name:    LED.red
        Feature: Set the PWM channel controlling the red led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._RED = ch

    def led_off(self, channel):
        """
        Name:    LED.led_off
        Feature: Set low a specifed led channel.
        Inputs:  int, the number of the channel to lower (channel)
        Outputs: None
        """
        self.logger.debug('start.')
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 4096, 0)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 0, 4096)
        self.logger.debug('complete.')

    def led_on(self, channel):
        """
        Name:    LED.led_on
        Feature: Set high a specified led channel.
        Inputs:  int, the number of the channel to raise (channel)
        Outputs: None
        """
        self.logger.debug('start.')
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 0, 4096)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 4096, 0)
        self.logger.debug('complete.')

    def red_on(self):
        """
        Name:    LED.red_on
        Feature: Set high the 'RED' PWM channel, default 15.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        self.led_on(self._RED)
        self.led_off(self._BLUE)
        self.led_off(self._GREEN)
        self.logger.debug('start.')

    def blue_on(self):
        """
        Name:    LED.blue_on
        Feature: Set high the 'BLUE' PWM channel, default 1.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        self.led_on(self._BLUE)
        self.led_off(self._RED)
        self.led_off(self._GREEN)
        self.logger.debug('complete.')

    def green_on(self):
        """
        Name:    LED.green_on
        Feature: Set high the 'GREEN' PWM channel, default 14.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        self.led_on(self._GREEN)
        self.led_off(self._RED)
        self.led_off(self._BLUE)
        self.logger.debug('complete.')

    def yellow_on(self):
        """
        Name:    LED.yellow_on
        Feature: Set led color to yellow by combining green and red
                 light using default channels.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        self.led_on(self._RED)
        self.led_on(self._GREEN)
        self.led_off(self._BLUE)
        self.logger.debug('complete.')

    def red_off(self):
        """
        Name:    LED.red_off
        Feature: Set low the 'red' PWM channel, default 15.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        self.led_off(self._RED)
        self.logger.debug('complete.')

    def blue_off(self):
        """
        Name:    LED.blue_off
        Feature: Set low the 'blue' PWM channel, default 1.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        self.led_off(self._BLUE)
        self.logger.debug('complete.')

    def green_off(self):
        """
        Name:    LED.green_off
        Feature: Set low the 'green' PWM channel, default 14.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        self.led_off(self._GREEN)
        self.logger.debug('complete.')

    def yellow_off(self):
        """
        Name:    LED.yellow_off
        Feature: Set low the red and green PWM channels, removing yellow
                 light.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        self.red_off()
        self.green_off()
        self.logger.debug('complete.')

    def all_off(self):
        """
        Name:    LED.all_off
        Feature: Set all led PWM channels to low.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        self.blue_off()
        self.green_off()
        self.red_off()
        self.logger.debug('complete.')

    def all_on(self):
        """
        Name:    LED.all_on
        Feature: Set all led PWM channels to high.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        self.led_on(self._BLUE)
        self.led_on(self._GREEN)
        self.led_on(self._RED)
        self.logger.debug('conplete.')

    def connect(self):
        self.logger.debug('start.')
        try:
            self.all_on()
            sleep(1)
            self.all_off()
        except:
            self.logger.error('could not connect to LED.')
            raise PRIDAPeripheralError('Could not connect to LED!')
        self.logger.debug('complete.')

    def poweroff(self):
        self.logger.debug('start.')
        self.all_off()
        self.logger.debug('complete.')

    def current_status(self):
        self.logger.debug('start.')
        super(self.current_status())
        self.logger.debug('complete.')
