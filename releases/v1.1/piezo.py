#!/usr/bin/python
#
# piezo.py
#
# VERSION: 1.1.0
#
# LAST EDIT: 2015-11-20
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 0b7102c: Nathanael Shaw - 2015-11-20 12:35:34
#     update headers, rmv func names from hdf logs
# 20eb8e2: Nathanael Shaw - 2015-11-20 10:49:42
#     remove func calls from debug msgs
# 8e4a390: Nathanael Shaw - 2015-11-19 12:03:00
#     add logging to piezo
# 051e7a6: twdavis - 2015-10-30 14:13:54
#     v1.0.1
# 858afbb: Nathanael Shaw - 2015-10-30 09:42:21
#     version 1.0 snapshot
# $log_end_tag$
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import time
import logging

from Adafruit_MotorHAT import Adafruit_MotorHAT
from pridaperipheral import PRIDAPeripheral
from pridaperipheral import PRIDAPeripheralError


###############################################################################
## CLASSES:
###############################################################################
class Piezo(Adafruit_MotorHAT, PRIDAPeripheral):
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.logger.debug('start.')
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self._CHANNEL = 0
        self._VOLUME = 1.0
        self.logger.debug('complete.')

    @property
    def channel(self):
        """
        Name:    Piezo.channel
        Feature: Returns PWM channel controlling the piezo buzzer.
        Inputs:  None
        Outputs: Integer, channel number assigned to controlling the
                 piezo buzzer (self._CHANNEL)
        """
        return self._CHANNEL

    @property
    def volume(self):
        """
        Name:    Imaging.volume
        Feature: Returns float value representing percent of maximum
                 volume
        Inputs:  None
        Outputs: Float, value representing currently assigned piezo
                 buzzer volume (self._VOLUME)
        """
        return 100 * self._VOLUME

    @channel.setter
    def channel(self, ch):
        """
        Name:    Piezo.channel
        Feature: Set the PWM channel controlling the piezo buzzer.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._CHANNEL = ch

    @volume.setter
    def volume(self, volume):
        """
        Name:    Piezo.volume
        Feature: Set piezo volume by duty cycle.
        Inputs:  int, percent of maximum volume (volume)
        Outputs: None
        """
        if type(volume) in {int, float}:
            if volume < 0:
                volume = 0
            elif volume > 100:
                volume = 100
            self._VOLUME = volume / 100.0

    def beep(self, num_beeps=3, beep_time=0.05, rest_time=0.1):
        """
        Name:    Piezo.beep
        Feature: Temporarily raise the 'piezo' channel. Default is three
                 short beeps in quick succession.
        Inputs:  - int, desired number of beeps (num_beeps)
                 - float, length of each beep in seconds (beep_time)
                 - float, length of silence between beeps (rest_time)
        Outputs: None
        """
        self.logger.debug('start.')
        for i in range(num_beeps):
            # turn buzzer on
            self._pwm.setPWM(self._CHANNEL, 0, int(2048 * self._VOLUME))
            # sustain note
            time.sleep(beep_time)
            # turn buzzer off
            self._pwm.setPWM(self._CHANNEL, 0, 4096)
            # rest
            time.sleep(rest_time)
        self.logger.debug('complete.')

    def connect(self):
        self.logger.debug('start.')
        try:
            self.beep(2)
        except:
            self.logger.error('could not connect to piezo.')
            raise PRIDAPeripheralError('Could not connect to piezo!')
        self.logger.debug('complete.')

    def poweroff(self):
        self.logger.debug('start.')
        self._pwm.setPWM(self._CHANNEL, 0, 0)
        self.logger.debug('complete.')

    def current_status(self):
        self.logger.debug('start.')
        super(self.current_status())
        self.logger.debug('complete.')
