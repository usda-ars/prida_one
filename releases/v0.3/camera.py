#!/usr/bin/python
#
# camera.py
#
# VERSION: 0.3.12
#
# LAST EDIT: 2015-07-02
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# dd24989: Nathanael Shaw - 2015-06-30 14:09:27
#     bugfixes
# 76030fd: jay - 2015-06-29 23:38:19
#     v0.3.9
# 6a90843: Jay - 2015-06-18 13:58:07
#     bugfixes
# $log_end_tag$
#
import os
#import subprocess
import sys

import gphoto2 as gp

class Camera():

    def __init__(self):
        self.taken = 0
        self.path = '/tmp'

    def connect(self):
        try:
            self.context = gp.gp_context_new()
            self.camera = gp.check_result(gp.gp_camera_new())
        except:
            print("Could not connect to camera")
        #gp.check_result(gp.gp_camera_init(camera, context))

    def set_path(self, p):
        self.path = p

    def reset_count(self):
        self.taken = 0

    def get_count(self):
        return self.taken

    def capture(self):
        file_path = gp.check_result(gp.gp_camera_capture(
            self.camera, gp.GP_CAPTURE_IMAGE, self.context))
        #print('Camera file path: {0}/{1}'.format(file_path.folder, file_path.name))
        filename = os.path.join(self.path, str(self.taken)+".jpg")
        print('Copying image to', filename)
        camera_file = gp.check_result(gp.gp_camera_file_get(
            self.camera, file_path.folder, file_path.name,
            gp.GP_FILE_TYPE_NORMAL, self.context))
        gp.check_result(gp.gp_file_save(camera_file, filename))
        #subprocess.call(['xdg-open', target])
        gp.check_result(gp.gp_camera_exit(self.camera, self.context))
        self.taken+=1

