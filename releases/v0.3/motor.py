#!/usr/bin/python
#
# motor.py
#
# VERSION: 0.3.12
#
# LAST EDIT: 2015-07-02
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# 2663627: Nathanael Shaw - 2015-07-01 10:37:49
#     Expirimenting with proper python-esque inheritance.
# 4bfdd02: Nathanael Shaw - 2015-06-30 16:22:26
#     Corrected motor.py to conform to PEP8
# 8a4cf0e: Nathanael Shaw - 2015-06-30 16:12:47
#     Tested and adjusted motor.py
# aebca4d: Nathanael Shaw - 2015-06-30 15:12:45
#     Converted motor.py to class (untested)
# 3642855: Nathanael Shaw - 2015-06-30 12:42:30
#     Added microstep resolution detection to motor.py
# 773aec5: Nathanael Shaw - 2015-06-29 11:24:36
#     Fixed motor.py indentations, added testMotor.py to back-up folder.
# f9c4529: Nathanael Shaw - 2015-06-24 11:57:30
#     Modifed Motor.py step function, added RPM selection support.
# 6a90843: Jay - 2015-06-18 13:58:07
#     bugfixes
# $log_end_tag$
#
from Adafruit_MotorHAT import Adafruit_MotorHAT
import time


class Motor(Adafruit_MotorHAT):

    def __init__(self):
        """
        Initializes data memebers upon class instantiation.
        Note: currently assumes that system is always using
        microstepping.
        Input:
            None
        Output:
            None
        """
        Adafruit_MotorHAT.__init__(self)
        #self.mh = Adafruit_MotorHAT() -- may be unecessary
        self.num_photos = 40
        self.rpm = 2.0
        self.step_res = 200
        self.motor_port_num = 1
        self.sm = self.getStepper(self.step_res, self.motor_port_num)
        #self.sm = self.mh.getStepper(self.step_res, self.motor_port_num)
        self.base_rot_time = 1.75 * self.sm.MICROSTEPS

    def set_num_photos(self, np):
        """
        Setter function for attribute controlling number of photos in a
        single expiriment.
        Input:
            np: int - number of photos to be taken
        Output:
            None
        """
        self.num_photos = np

    def set_rpm(self, rot_per_min):
        """
        Setter function for attribute controlling approximate rotations
        per minute value for the motor.
        Input:
            rot_per_min: float - desired rpm value
        Output:
            None
        """
        self.rpm = rot_per_min

    def set_step_res(self, sr, allow=False):
        """
        Setter function for attribute controlling stepper motor
        resolution. Not currently supported; data member added to reduce
        presence of 'magic numbers.'
        Input:
            sr: int - desired 'step resolution,' or the number of steps
            in a full rotation, defining the length of a unit step.
            allow: bool - allows setter for goodness knows why
        Output:
            None
        """
        if allow:
            self.step_res = sr

    def set_motor_port_num(self, num, allow=False):
        """
        Setter function for attribute controlling stepper motor port
        number. Not currently supported; data member added to reduce
        presence of 'magic numbers.'
        Input:
            num: int - desired port number to be used on the HAT
            allow: bool - allows setter for goodness knows why
        Output:
            None
        """
        if allow:
            self.motor_port_num = num

    def set_base_rot_time(self, base_time, allow=False):
        """
        Setter function for attribute representing stepper motor base
        rotation time (in seconds) during full speed operation. Not
        currently supported; data member added to reduce presence of
        'magic numbers.'
        Input:
            base_time: int - minmum rotation time in seconds
            allow: bool - allows setter for goodness knows why
        Output:
            None
        """
        if allow:
            self.base_rot_time = base_time

    def turn_off_motors(self):
        """
        Function to disable all motors attached to the HAT.
        Input:
            None
        Output:
            None
        """
        #self.mh.getMotor(1).run(self.mh.RELEASE)
        #self.mh.getMotor(2).run(self.mh.RELEASE)
        #self.mh.getMotor(3).run(self.mh.RELEASE)
        #self.mh.getMotor(4).run(self.mh.RELEASE)
        self.getMotor(1).run(self.RELEASE)
        self.getMotor(2).run(self.RELEASE)
        self.getMotor(3).run(self.RELEASE)
        self.getMotor(4).run(self.RELEASE)

    def calibrate(self):
        """
        Calibration function that takes three initial unit steps in
        order to better insure consistant step size between image
        captures.
        Input:
            None
        Output:
            None
        """
        for i in xrange(3*self.sm.MICROSTEPS):
            #self.sm.oneStep(self.mh.FORWARD, self.mh.MICROSTEP)
            self.sm.oneStep(self.FORWARD, self.MICROSTEP)
            time.sleep(.1)

    def step(self):
        """
        Function to perform a single Inter-Photo-Angle (IPA) step.
        Takes the desired number of photos in the run and calculates
        the IPA step size, and converts the desired RPM value into the
        appropriate length wait time.
        Inputs:
            None
        Outputs:
            None
        """
        # data member error checking
        if self.rpm <= 0:
            print("RPM must be greater than zero.")
            return None
        try:
            # convert RPM to req'd value for calculating wait time
            real_rpm = (1.0 / (((60.0 / self.rpm) -
                               self.base_rot_time) / 60.0))
        except ZeroDivisionError:
            print("Set a valid RPM value.")
        else:
            # begin stepping
            for i in xrange((self.step_res*self.sm.MICROSTEPS) /
                            self.num_photos):
                self.sm.oneStep(self.FORWARD, self.MICROSTEP)
                #self.sm.oneStep(self.mh.FORWARD, self.mh.MICROSTEP)
                time.sleep(60.0 / (self.step_res *
                                   self.sm.MICROSTEPS * real_rpm))
