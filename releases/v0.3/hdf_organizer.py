#!/usr/bin/python
#
# hdf_organizer.py
#
# VERSION: 0.3.12
#
# LAST EDIT: 2015-07-02
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# 2015-06-10 -- created
# 2015-06-30 -- last updated
#
# ------------
# description:
# ------------
# This script sets up the HDF5 (Hierarchical Data Formal) files for storing
# plant root images (a part of the PRIDA Project).
#
# The general workflow follows this pattern:
#   1. Create a class instance:
#      ``````````````````````
#      my_class = PridaHDF()
#      ,,,,,,,,,,,,,,,,,,,,,,
#
#   2. Create new / open existing an HDF5 file:
#      a. Create new file:
#         ``````````````````````````````````````````````
#         my_class.new_file(str directory, str filename)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Open existing file:
#         ````````````````````````````````
#         my_class.open_file(str filename)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   3. User's details:
#      a. Save user's details to file:
#         ````````````````````````````````````
#         my_class.set_root_user(str username)
#         my_class.set_root_addr(str useraddr)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Check details:
#         ````````````````````````
#         my_class.get_root_user()
#         my_class.get_root_addr()
#         ,,,,,,,,,,,,,,,,,,,,,,,,
#
#   4. Create a new / open an existing experiment:
#      a. Create new experiment:
#         ``````````````````
#         my_class.new_exp()
#         ,,,,,,,,,,,,,,,,,,
#
#      b. Open existing experiment:
#         1. Check for existing experiments:
#            ````````````````````
#            my_class.list_exps()
#            ,,,,,,,,,,,,,,,,,,,,
#
#         2. Select experiment to open:
#            ``````````````````````````
#            my_class.open_exp(str exp)
#            ,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   5. Current experiment's details:
#      a. Set experiment details:
#         ```````````````````````````````````
#         my_class.set_exp_title(str)
#         my_class.set_exp_genus_species(str)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Check details:
#         ````````````````````````````````
#         my_class.get_exp_title()
#         my_class.get_exp_genus_species()
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   6. Create new / open experiment rep:
#      a. Create new rep:
#         ``````````````````
#         my_class.new_rep()
#         ,,,,,,,,,,,,,,,,,,
#
#      b. Open existing rep:
#         1. List existing reps:
#            ````````````````````
#            my_class.list_reps()
#            ,,,,,,,,,,,,,,,,,,,,
#
#         2. Select rep to open:
#            ``````````````````````````
#            my_class.open_rep(str rep)
#            ,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   7. Current rep's details:
#      a. Set rep details:
#         ````````````````````````````````````
#         my_class.set_rep_date(datetime.date)
#         my_class.set_rep_number(int)
#         my_class.set_rep_images(int)
#         my_class.set_rep_cultivar(str)
#         my_class.set_rep_age_num(int)
#         my_class.set_rep_age_unit(str)
#         my_class.set_rep_media(str)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Check rep details:
#         ```````````````````````````
#         my_class.get_rep_date()
#         my_class.get_rep_number()
#         my_class.get_rep_images()
#         my_class.get_rep_cultivar()
#         my_class.get_rep_age_num()
#         my_class.get_rep_age_unit()
#         my_class.get_rep_media()
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   8. Save/extract datasets:
#      a. Save image to rep:
#         ``````````````````````````````
#         my_class.save_image(str image)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Extract image to directory:
#         ````````````````````````````````````````````
#         my_class.extract_dataset(str image, str dir)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   9. Save file:
#      ```````````````
#      my_class.save()
#      ,,,,,,,,,,,,,,,
#
#  10. Close file:
#      ```````````````
#      my_class.close()
#      ,,,,,,,,,,,,,,,
#
#
# -----
# todo:
# -----
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import cv2
import datetime
import h5py
import numpy
import os.path
import re

###############################################################################
## CLASSES
###############################################################################
class PridaHDF:
    """
    Name:     PridaHDF
    Features: This class handles the organization and storage of plant root
              images and their associated meta data into a readable/writeable
              HDF5 (Hierarchical Data Format) file
    History:  VERSION 0.2.5
              - added cv2, datetime, glob, and os.path modules [15.06.11]
              - create_experiment & create_rep class functions [15.06.12]
              - created select_hdf_file function [15.06.16]
              - moved 'cultivar' from experiment to rep meta data [15.06.16]
              - set cur_exp/cur_rep to last values in existing file [15.06.16]
              - create_hdf and open_hdf separated into own functions [15.06.18]
              - individual set/get root group meta data functions [15.06.18]
              - updated create_experiment function [15.06.18]
              - individual set/get experiment meta data functions [15.06.18]
              - rename open_file / new_file functions [15.06.19]
              - new / open experiment functions [15.06.19]
              - set / get experiment and rep meta data functions [15.06.19]
              - get_datasets function [15.06.19]
              - get_image function [15.06.19]
              - removed overwrite in open_file function [15.06.19]
              - list datasets, experiments and reps functions [15.06.19]
              - added optional exper and rep_path strings [15.06.19]
              --> allows for easy look-up without setting cur_exp or cur_rep
              - rotate_image and show_image functions [15.06.19]
              - changed experiment genus/species to a single string [15.06.23]
              - updated TIMEFORMAT to TIME_FORMAT [15.06.24]
              - get_rep_date now returns datetime.date object [15.06.24]
              - separate save and close functions [15.06.24]
              --> Note: flush request does not instantly save to disk
              - created extract_dataset function [15.06.24]
              - changed open_experiment to open_exp [15.06.26]
              - changed list_experiments to list_exps [15.06.26]
              - changed new_experiment to new_exp [15.06.26]
              - use regular expressions to name new exp and rep [15.06.26]
              - created get_attr function [15.06.26]
              --> all other get attribute functions depend on this
              - created set_attr function [15.06.26]
              --> all other set attribute functions depend on this
              - added rep_age_num, rep_age_unit and rep_media attrs [15.06.26]
              - added zero padding back to exp and rep names [15.06.30]
    Ref:      https://www.hdfgroup.org/HDF5/
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     PridaHDF.__init__
        Features: Initialize empty class.
        Inputs:   None.
        Outputs:  None.
        """
        # Define class constants:
        self.TIME_FORMAT = "%Y-%m-%d"           # strftime format
        self.isopen = False
        self.cur_exp = None  # \  set to None type to catch errors when running
        self.cur_rep = None  # /  an experiment without creating an exp or rep
    #
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def close(self):
        """
        Name:     PridaHDF.close
        Features: Saves changes to disk and closes HDF5 file handle, no further
                  reading or writing can be done.
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile.close()
            except:
                pass # already closed
            finally:
                self.isopen = False
                self.cur_exp = None
                self.cur_rep = None
    #
    def extract_dataset(self, dname, to='./'):
        """
        Name:     PridaHDF.extract_dataset
        Features: Extracts an image dataset from HDF5 file to given directory
        Inputs:   - str, dataset name (dname)
                  - [optional] str, directory to save image (to)
        Outputs:  None.
        """
        if self.isopen:
            if self.cur_rep:
                if dname in self.hdfile[self.cur_rep]:
                    oname = to + dname    # output file name
                    dset = self.hdfile[self.cur_rep][dname][:,:]
                    try:
                        cv2.imwrite(oname, dset)
                    except:
                        print "Error! Could not extract dataset to", oname
                else:
                    print "Error! Could not find image in datasets."
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not get datasets. HDF5 file not open!"
    #
    def get_attr(self, attr, path):
        """
        Name:     PridaHDF.get_attr
        Features: Returns the given attribute for the given path
        Inputs:   - str, attribute name (attr)
                  - str, group path (path)
        Outputs:  str | int | NoneType, attribute value (val)
        """
        if self.isopen:
            if path in self.hdfile:
                try:
                    val = self.hdfile[path].attrs[attr]
                except KeyError:
                    print "Error! '%s' has no attribute '%s'" % (path, attr)
                    val = None
                except:
                    print ("Error! Attribute '%s' could not be retrieved "
                           "from path '%s'!") % (attr, path)
                    val = None
            else:
                print "Error! Path not defined!"
                val = None
        else:
            print ("Error! Could not get attribute. HDF5 file not open!")
            val = None
        #
        return val
    #
    def get_dataset(self, dname):
        """
        Name:     PridaHDF.get_image
        Features: Returns a given dataset in the current rep
        Inputs:   str, dataset name (dname)
        Outputs:  numpy.ndarray
        """
        dset = numpy.array([])
        if self.isopen:
            if self.cur_rep:
                if dname in self.hdfile[self.cur_rep]:
                    dset = self.hdfile[self.cur_rep][dname][:,:]
                else:
                    print "Error! Could not find image in datasets."
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not get datasets. HDF5 file not open!"
        #
        return dset
    #
    def get_exp_genus_species(self, exper=''):
        """
        Name:     PridaHDF.get_exp_genus_species
        Features: Returns the current experiment genus and species
        Inputs:   [optional] str, experiment name (exper)
        Outputs:  str, experiment genus and species (attr)
        Depends:  get_attr
        """
        if self.isopen:
            if exper:
                attr = self.get_attr("genus_species", exper)
            elif self.cur_exp:
                attr = self.get_attr("genus_species", self.cur_exp)
            else:
                print "Error! No experiment selected!"
                attr = None
        else:
            print ("Error! Could not get experiment genus and species. "
                   "HDF5 file not open!")
            attr = None
        #
        return attr
    #
    def get_exp_title(self, exper=''):
        """
        Name:     PridaHDF.get_exp_title
        Features: Returns current experiment title
        Inputs:   [optional] str, experiment name (exper)
        Outputs:  str, experiment title (attr)
        Depends:  get_attr
        """
        if self.isopen:
            if exper:
                attr = self.get_attr("title", exper)
            elif self.cur_exp:
                attr = self.get_attr("title", self.cur_exp)
            else:
                print "Error! No experiment selected!"
                attr = None
        else:
            print "Error! Could not get experiment title. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_rep_age_num(self, rep_path=''):
        """
        Name:     PridaHDF.get_rep_age_num
        Features: Returns the current rep age number
        Inputs:   [optional] str, rep name and path (rep_path)
        Outputs:  int, age number of rep (attr)
        Depends:  get_attr
        """
        if self.isopen:
            if rep_path:
                attr = self.get_attr("age", rep_path)
            elif self.cur_rep:
                attr = self.get_attr("age", self.cur_rep)
            else:
                print "Error! No rep selected!"
                attr = None
        else:
            print "Error! Could not get rep age number. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_rep_age_unit(self, rep_path=''):
        """
        Name:     PridaHDF.get_rep_age_unit
        Features: Returns the current rep age unit
        Inputs:   [optional] str, rep name and path (rep_path)
        Outputs:  str, age unit of rep (attr)
        Depends:  get_attr
        """
        if self.isopen:
            if rep_path:
                attr = self.get_attr("unit", rep_path)
            elif self.cur_rep:
                attr = self.get_attr("unit", self.cur_rep)
            else:
                print "Error! No rep selected!"
                attr = None
        else:
            print "Error! Could not get rep age unit. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_rep_cultivar(self, rep_path=''):
        """
        Name:     PridaHDF.get_rep_cultivar
        Features: Returns the current rep cultivar
        Inputs:   [optional] str, rep name with path (rep_path)
        Outputs:  str, cultivar of current rep (attr)
        Depends:  get_attr
        """
        if self.isopen:
            if rep_path:
                attr = self.get_attr("cultivar", rep_path)
            elif self.cur_rep:
                attr = self.get_attr("cultivar", self.cur_rep)
            else:
                print "Error! No rep selected!"
                attr = None
        else:
            print "Error! Could not get rep cultivar. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_rep_date(self, rep_path=''):
        """
        Name:     PridaHDF.get_rep_date
        Features: Returns the current rep date
        Inputs:   [optional] str, rep name and path (rep_path)
        Outputs:  datetime.date, date of rep
        Depends:  get_attr
        """
        if self.isopen:
            if rep_path:
                attr = self.get_attr("date", rep_path)
            elif self.cur_rep:
                attr = self.get_attr("date", self.cur_rep)
            else:
                print "Error! No rep selected!"
                attr = '1999-12-31'
        else:
            print "Error! Could not get rep date. HDF5 file not open!"
            attr = '2000-01-01'
        #
        if attr is not None:
            try:
                attr = datetime.datetime.strptime(attr, self.TIME_FORMAT).date()
            except:
                print "Error! Datetime conversion failed!"
                attr = None
        return attr
    #
    def get_rep_images(self, rep_path=''):
        """
        Name:     PridaHDF.get_rep_images
        Features: Returns the number of images to take for current rep
        Inputs:   [optional] str, rep name and path (rep_path)
        Outputs:  int, number of rep (attr)
        Depends:  get_attr
        """
        if self.isopen:
            if rep_path:
                attr = self.get_attr("total_images", rep_path)
            elif self.cur_rep:
                attr = self.get_attr("total_images", self.cur_rep)
            else:
                print "Error! No rep selected!"
                attr = None
        else:
            print "Error! Could not get number of images. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_rep_media(self, rep_path=''):
        """
        Name:     PridaHDF.get_rep_media
        Features: Returns the media type for current rep
        Inputs:   [optional] str, rep name and path (rep_path)
        Outputs:  str, media type of rep (attr)
        Depends:  get_attr
        """
        if self.isopen:
            if rep_path:
                attr = self.get_attr("media", rep_path)
            elif self.cur_rep:
                attr = self.get_attr("media", self.cur_rep)
            else:
                print "Error! No rep selected!"
                attr = None
        else:
            print "Error! Could not get media type of rep. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_rep_number(self, rep_path=''):
        """
        Name:     PridaHDF.get_rep_num
        Features: Returns the current rep number
        Inputs:   [optional] str, rep name and path (rep_path)
        Outputs:  int, rep number (attr)
        Depends:  get_attr
        """
        if self.isopen:
            if rep_path:
                attr = self.get_attr("number", rep_path)
            elif self.cur_rep:
                attr = self.get_attr("number", self.cur_rep)
            else:
                print "Error! No rep selected!"
                attr = None
        else:
            print "Error! Could not get rep number. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_root_addr(self):
        """
        Name:     PridaHDF.get_root_addr
        Features: Returns the root group user address (e.g., NetID)
        Inputs:   None.
        Outputs:  str, root group user address (attr)
        Depends:  get_attr
        """
        if self.isopen:
            attr = self.get_attr("addr", "/")
        else:
            print "Could not get root addr. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_root_user(self):
        """
        Name:     PridaHDF.get_root_user
        Features: Returns the root group username
        Inputs:   None.
        Outputs:  str, root group username (attr)
        Depends:  get_attr
        """
        if self.isopen:
            attr = self.get_attr("user", "/")
        else:
            print "Could not get root user. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def img_to_hdf(self, img, dname):
        """
        Name:     PridaHDF.img_to_hdf
        Features: Saves image dataset to current HDF repository; currently set
                  with gzip compression.
        Inputs:   - numpy.ndarray, openCV image data array (img)
                  - str, image/dataset name (dname)
        """
        if self.cur_rep:
            ds_name = self.cur_rep + '/' + dname
            if ds_name in self.hdfile:
                print "WARNING: Overwriting dataset", self.cur_rep, dname
            #
            try:
                self.hdfile.create_dataset(name=ds_name,
                                           data=img,
                                           compression='gzip')
            except:
                print "PridaHDF.img_to_hdf ERROR: could not create dataset",
                print dname
            else:
                # Save image dimensions (i.e., pixel y, pixel x, color bands):
                # NOTE: openCV reads images in BGR not RGB
                self.hdfile[ds_name].dims[0].label = 'pixels in y'
                self.hdfile[ds_name].dims[1].label = 'pixels in x'
                self.hdfile[ds_name].dims[2].label = 'BGR color bands'
        else:
            print "Error! Could not save dataset. No rep selected!"
    #
    def list_datasets(self, rep_path=''):
        """
        Name:     PridaHDF.list_datasets
        Features: Returns a list of datasets in the current rep
        Inputs:   [optional] str, rep name and path (rep_path)
        Outputs:  list, dataset names (dsets)
        """
        dsets = []
        if self.isopen:
            if rep_path:
                if rep_path in self.hdfile:
                    for dset in self.hdfile[rep_path].keys():
                        dsets.append(dset)
                else:
                    print "Error! Could not find", rep_path
            elif self.cur_rep:
                for dset in self.hdfile[self.cur_rep].keys():
                    dsets.append(dset)
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not get datasets. HDF5 file not open!"
        #
        return dsets
    #
    def list_exps(self):
        """
        Name:     PridaHDF.list_exps
        Features: Returns a list of experiments in HDF5 file
        Inputs:   None.
        Outputs:  list, experiment names (esets)
        """
        esets = []
        if self.isopen:
            for eset in self.hdfile.keys():
                esets.append(eset)
        else:
            print "Error! Could not get experiments. HDF5 file not open!"
        #
        return esets
    #
    def list_reps(self, exper=''):
        """
        Name:     PridaHDF.list_reps
        Features: Returns a list of reps in current experiment
        Inputs:   [optional] str, experiment name
        Outputs:  list, rep names (rsets)
        """
        rsets = []
        if self.isopen:
            if exper:
                if exper in self.hdfile:
                    for rset in self.hdfile[exper].keys():
                        rsets.append(rset)
                else:
                    print "Error! Could not find experiment", exper
            elif self.cur_exp:
                for rset in self.hdfile[self.cur_exp].keys():
                    rsets.append(rset)
            else:
                print "Error! No experiment selected!"
        else:
            print "Error! Could not get experiments. HDF5 file not open!"
        #
        return rsets
    #
    def new_exp(self):
        """
        Name:     PridaHDF.new_exp
        Features: Creates a new experiment (i.e., appends new experiment to the
                  list of existing experiments)
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            # Find the number of existing experiments:
            num_exp = len(self.hdfile.keys())
            #
            if num_exp == 0:
                ename = 'e%06d' % (1)
            else:
                last_exp = self.hdfile.keys()[-1]
                try:
                    # Regular expression, greedy match trailing digits:
                    last_num = re.search('\D{1}(\d+)', last_exp).group(1)
                    last_num = int(last_num)
                    print 'last exp number:', last_num
                except AttributeError:
                    print ("PridaHDF.new_experiment error. "
                           "Failed match in regular expression")
                except ValueError:
                    print ("PridaHDF.new_experiment error. "
                           "Casting string to int.")
                except:
                    print ("PridaHDF.new_experiment error. "
                           "An unknown problem has occurred.")
                else:
                    new_num = last_num + 1
                    ename = 'e%06d' % (new_num)
            #
            try:
                self.hdfile.create_group(ename)
            except:
                print "Error! Could not create new experiment!"
            else:
                self.cur_exp = ename
                self.cur_rep = None
        else:
            print "Could not create new experiment. HDF5 file not open!"
    #
    def new_file(self, dname, fname):
        """
        Name:     PridaHDF.new_file
        Features: Creates a new HDF5 file, overwriting an existing file if it
                  exists.
        Inputs:   - str, directory path (dname)
                  - str, filename (fname)
        Outputs:  None.
        """
        # Save directory and filename as class variables:
        self.dir = dname
        self.filename = fname
        my_hdf = dname + fname
        #
        # Create empty HDF file:
        if os.path.isfile(my_hdf):
            print "Warning! That file already exits! Exiting..."
        else:
            print "Creating file", fname, "...",
            try:
                self.hdfile = h5py.File(my_hdf, 'w')
            except:
                print "failed!"
                self.isopen = False
            else:
                print "success!"
                self.isopen = True
    #
    def new_rep(self):
        """
        Name:     PridaHDF.new_rep
        Features: Creates a new repository group for storing images
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            if self.cur_exp:
                # Find the number of existing reps:
                num_rep = len(self.hdfile[self.cur_exp].keys())
                #
                if num_rep == 0:
                    rname = '%s/r%06d' % (self.cur_exp, 1)
                else:
                    last_rep = self.hdfile[self.cur_exp].keys()[-1]
                    try:
                        # Regular expression, greedy match trailing digits:
                        last_num = re.search('\D{1}(\d+)', last_rep).group(1)
                        last_num = int(last_num)
                    except AttributeError:
                        print ("PridaHDF.new_rep error. "
                            "Failed match in regular expression")
                    except ValueError:
                        print ("PridaHDF.new_rep error. "
                            "Casting string to int.")
                    except:
                        print ("PridaHDF.new_rep error. "
                            "An unknown problem has occurred.")
                    else:
                        new_num = last_num + 1
                        rname = '%s/r%06d' % (self.cur_exp, new_num)
                #
                try:
                    self.hdfile.create_group(rname)
                except:
                    print "Error! Could not create new rep!"
                else:
                    self.cur_rep = rname
            else:
                print "Error! Could not create new rep. No experiment selected!"
        else:
            print "Error! Could not create new rep. HDF5 file not open!"
    #
    def open_exp(self, ename):
        """
        Name:     PridaHDF.open_exp
        Features: Opens an existing experiment (i.e., sets the current
                  experiment flag)
        Inputs:   str, experiment group name
        Outputs:  None.
        """
        if self.isopen:
            if ename in self.hdfile:
                self.cur_exp = ename
                self.cur_rep = None
            else:
                print "Error! Could not find experiment", ename
        else:
            print "Error! Could not open experiment. HDF5 file not open!"
        #
    #
    def open_file(self, fname):
        """
        Name:     PridaHDF.open_file
        Features: Opens an existing HDF5 file or creates new if file is not
                  found.
        Inputs:   str, filename with path (fname)
        Outputs:  None.
        """
        # Open existing HDF file:
        if os.path.isfile(fname):
            print "Opening file", os.path.basename(fname), "...",
        else:
            print "Could not find file! Creating",
            print os.path.basename(fname),
            print "...",
        #
        try:
            # NOTE: 'a' flag opens existing or creates new
            self.hdfile = h5py.File(fname, 'a')
        except:
            print "failed!"
            self.isopen = False
        else:
            print "success!"
            self.isopen = True
            self.cur_exp = None  # \  Set to None type by default for
            self.cur_rep = None  # /  newly opened file.
    #
    def open_rep(self, rname):
        """
        Name:     PridaHDF.open_experiment
        Features: Opens an existing rep (i.e., sets the current rep flag) from
                  within the current experiment
        Inputs:   str, rep group name
        Outputs:  None.
        """
        if self.isopen:
            if rname in self.hdfile[self.cur_exp]:
                self.cur_rep = "%s/%s" % (self.cur_exp, rname)
            else:
                print "Error! Could not find rep", rname
        else:
            print "Error! Could not open rep. HDF5 file not open!"
        #
    #
    def save(self):
        """
        Name:     PridaHDF.save
        Features: Flushes changes to the HDF file.
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile.flush()
            except:
                print "Could not flush file!"
    #
    def save_image(self, ifile):
        """
        Name:     PridaHDF.save_image
        Features: Saves image data as a dataset in the current rep
        Input:    str, image filename with path (ifile)
        Output:   None.
        Depends:  img_to_hdf()
        """
        if self.isopen:
            # NOTE: image name (iname) includes the original file extension
   	    iname = os.path.basename(ifile)
            img = cv2.imread(ifile)
            self.img_to_hdf(img, iname)
        else:
            print "Error! Could not save image. HDF5 file not open!"
    #
    def set_attr(self, attr_name, attr_val, path):
        """
        Name:     PridaHDF.set_attr
        Features: Sets given attribute to given path
        Inputs:   - str, attribute name (attr_name)
                  - [dtype], attribute value (attr_val)
                  - str, path to group/dataset (path)
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile[path].attrs.create(name=attr_name,
                                               data=attr_val)
            except:
                print ("Error! Could not set attribute '%s' "
                       "to path '%s'") % (attr_name, path)
        else:
            print ("Error! Could not set attribute. HDF5 file not open!")
    #
    def set_exp_genus_species(self, attr, exper=''):
        """
        Name:     PridaHDF.set_exp_genus_species
        Features: Sets current experiment genus and species
        Inputs:   - str, experiment genus and species (attr)
                  - [optional] str, experiment name (exper)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if exper:
                self.set_attr("genus_species", attr, exper)
            elif self.cur_exp:
                self.set_attr("genus_species", attr, self.cur_exp)
            else:
                print "Error! No experiment selected!"
        else:
            print ("Error! Could not set experiment genus and species. "
                   "HDF5 file not open!")
    #
    def set_exp_title(self, attr, exper=''):
        """
        Name:     PridaHDF.set_exp_title
        Features: Sets current experiment title
        Inputs:   - str, experiment title (attr)
                  - [optional] str, experiment name (exper)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if exper:
                self.set_attr("title", attr, exper)
            elif self.cur_exp:
                self.set_attr("title", attr, self.cur_exp)
            else:
                print "Error! No experiment selected!"
        else:
            print "Error! Could not set experiment title. HDF5 file not open!"
    #
    def set_rep_age_num(self, attr, rep_path=''):
        """
        Name:     PridaHDF.set_rep_age_num
        Features: Sets the current rep plant age number
        Inputs:   - int, plant age number (attr)
                  - [optional] str, rep name with path (rep_path)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if rep_path:
                self.set_attr("age", attr, rep_path)
            elif self.cur_rep:
                self.set_attr("age", attr, self.cur_rep)
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not set rep age number. HDF5 file not open!"
    #
    def set_rep_age_unit(self, attr, rep_path=''):
        """
        Name:     PridaHDF.set_rep_age_unit
        Features: Sets the current rep plant age unit
        Inputs:   - str, plant age unit (attr)
                  - [optional] str, rep name with path (rep_path)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if rep_path:
                self.set_attr("unit", attr, rep_path)
            elif self.cur_rep:
                self.set_attr("unit", attr, self.cur_rep)
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not set rep age unit. HDF5 file not open!"
    #
    def set_rep_cultivar(self, attr, rep_path=''):
        """
        Name:     PridaHDF.set_rep_cultivar
        Features: Sets the current rep cultivar
        Inputs:   - str, cultivar of current rep (attr)
                  - [optional] str, rep name with path (rep_path)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if rep_path:
                self.set_attr("cultivar", attr, rep_path)
            elif self.cur_rep:
                self.set_attr("cultivar", attr, self.cur_rep)
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not set rep cultivar. HDF5 file not open!"
    #
    def set_rep_date(self, attr, rep_path=''):
        """
        Name:     PridaHDF.set_rep_date
        Features: Sets the current rep date
        Inputs:   - datetime.date, date of rep (attr)
                  - [optional] str, rep name with path (rep_path)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            # Datetime must be converted to string for HDF5 attribute
            try:
                attr_date = attr.strftime(self.TIME_FORMAT)
            except:
                print "Error! Datetime object could not be parsed!"
            else:
                if rep_path:
                    self.set_attr("date", attr_date, rep_path)
                elif self.cur_rep:
                    self.set_attr("date", attr_date, self.cur_rep)
                else:
                    print "Error! No rep selected!"
        else:
            print "Error! Could not set rep date. HDF5 file not open!"
    #
    def set_rep_images(self, attr, rep_path=''):
        """
        Name:     PridaHDF.set_rep_images
        Features: Sets the number of images to take for current rep
        Inputs:   - int, number of rep (attr)
                  - [optional] str, rep name with path (rep_path)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if rep_path:
                self.set_attr("total_images", attr, rep_path)
            elif self.cur_rep:
                self.set_attr("total_images", attr, self.cur_rep)
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not set number of images. HDF5 file not open!"
    #
    def set_rep_media(self, attr, rep_path=''):
        """
        Name:     PridaHDF.set_rep_media
        Features: Sets the media type for current rep
        Inputs:   - str, media type of rep (attr)
                  - [optional] str, rep name with path (rep_path)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if rep_path:
                self.set_attr("media", attr, rep_path)
            elif self.cur_rep:
                self.set_attr("media", attr, self.cur_rep)
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not set media type of rep. HDF5 file not open!"
    #
    def set_rep_number(self, attr, rep_path=''):
        """
        Name:     PridaHDF.set_rep_number
        Features: Sets the current rep number
        Inputs:   - int, number of rep (attr)
                  - [optional] str, rep name with path (rep_path)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if rep_path:
                self.set_attr("number", attr, rep_path)
            elif self.cur_rep:
                self.set_attr("number", attr, self.cur_rep)
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not set rep number. HDF5 file not open!"
    #
    def set_root_addr(self, attr):
        """
        Name:     PridaHDF.set_root_addr
        Features: Sets the root group user address (e.g., NetID)
        Inputs:   str, root group user address (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.set_attr("addr", attr, "/")
        else:
            print "Could not set root addr. HDF5 file not open!"
    #
    def set_root_user(self, attr):
        """
        Name:     PridaHDF.set_root_user
        Features: Sets the root group username
        Inputs:   str, root group username (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.set_attr("user", attr, "/")
        else:
            print "Could not set root user. HDF5 file not open!"
    #
    def rotate_image(self, img, angle):
        """
        Name:     PridaHDF.rotate_image
        Features: Rotates an image about center a given number of degrees
        Inputs:   - numpy.ndarray, image data (img)
                  - int, angle of rotation
        Outputs:  numpy.ndarray, rotated image
        Ref:      http://john.freml.in/opencv-rotation
        """
        w = img.shape[1]
        h = img.shape[0]
        rangle = numpy.deg2rad(angle)
        #
        # Calculate new image width and height
        nw = (abs(numpy.sin(rangle)*h) + abs(numpy.cos(rangle)*w))*1.0
        nh = (abs(numpy.cos(rangle)*h) + abs(numpy.sin(rangle)*w))*1.0
        #
        # Ask OpenCV for the rotation matrix
        rot_mat = cv2.getRotationMatrix2D((nw*0.5, nh*0.5), angle, 1.0)
        #
        # Calculate the move from the old center to the new center combined
        # with the rotation
        rot_move = numpy.dot(rot_mat, numpy.array([(nw-w)*0.5, (nh-h)*0.5,0]))
        #
        # The move only affects the translation, so update the translation
        # part of the transform
        rot_mat[0,2] += rot_move[0]
        rot_mat[1,2] += rot_move[1]
        #
        # Warp each of the three 2D color bands:
        band1, band2, band3 = cv2.split(img)
        result1 = cv2.warpAffine(band1,
                                 rot_mat,
                                 (int(numpy.ceil(nw)),
                                 int(numpy.ceil(nh))),
                                 flags=cv2.INTER_LANCZOS4)
        result2 = cv2.warpAffine(band2,
                                 rot_mat,
                                 (int(numpy.ceil(nw)),
                                 int(numpy.ceil(nh))),
                                 flags=cv2.INTER_LANCZOS4)
        result3 = cv2.warpAffine(band3,
                                 rot_mat,
                                 (int(numpy.ceil(nw)),
                                 int(numpy.ceil(nh))),
                                 flags=cv2.INTER_LANCZOS4)
        result = cv2.merge([result1,result2,result3])
        return result
    #
    def show_image(self, iname, rot=0):
        """
        Name:     PridaHDF.show_image
        Features: Displays an image using openCV
        Inputs:   - str, image name in current rep (iname)
                  - [optional] int, rotation angle (rot)
        Outputs:  None.
        Depends:  rotate_image()
        """
        if self.isopen:
            if self.cur_rep:
                if iname in self.hdfile[self.cur_rep]:
                    img_full = self.get_dataset(iname)
                    img = cv2.resize(img_full, (0,0), fx=0.25, fy=0.25)
                    cv2.namedWindow('Image', cv2.WINDOW_AUTOSIZE)
                    #
                    # Rotation:
                    if rot != 0:
                        img = self.rotate_image(img, int(rot))
                    #
                    cv2.imshow("Image", img)
                    cv2.moveWindow("Image", 0, 0)
                    cv2.waitKey(0)
                    cv2.destroyWindow("Image")
                else:
                    print "Error! Could not find image in datasets."
            else:
                print "Error! No rep selected!"
        else:
            print "Error! Could not plot image. HDF5 file not open!"
    #