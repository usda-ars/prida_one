#!/usr/bin/python
#
# Prida.py
#
# VERSION: 0.3.12
#
# LAST EDIT: 2015-07-02
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# e48d0fd: Hoi Cheng - 2015-07-02 12:57:24
#     bugfixes
# fcd5583: Hoi Cheng - 2015-07-02 12:46:08
#     v0.3.12
# 01523bb: jay - 2015-07-02 00:43:19
#     v0.3.11
# dd24989: Nathanael Shaw - 2015-06-30 14:09:27
#     bugfixes
# 76030fd: jay - 2015-06-29 23:38:19
#     v0.3.9
# d39f320: Nathanael Shaw - 2015-06-29 13:21:36
#     working on threads
# 6ce3340: jay - 2015-06-29 01:52:24
#     v0.3.7
# e6f4e6b: jay - 2015-06-28 19:42:42
#     v0.3.6
# db2eee7: jay - 2015-06-27 00:18:29
#     directory cleanup part 1
# 1e012bb: jay - 2015-06-22 01:19:16
#     v0.2.1
# 6a90843: Jay - 2015-06-18 13:58:07
#     bugfixes
# $log_end_tag$
#
import sys
from PyQt5.QtWidgets import (
    QApplication, QFileDialog, QMainWindow)
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtCore import QThread
from PyQt5 import uic
from hdf_organizer import *
from camera import *
from motor import *

class HardwareThread(QThread):

    def __init__(self, camera, motor):
        QThread.__init__(self)
        self.camera = camera
        self.motor = motor
        self.camera.connect()

    def run(self):
        self.camera.capture()
        self.motor.step()

class Prida(QApplication):

    def __init__(self):
        QApplication.__init__(self, sys.argv)
        self.camera = Camera()
        self.motor = Motor()
        self.hardware_thread = HardwareThread(self.camera, self.motor)
        self.base = QMainWindow()
        self.view = uic.loadUi('mainwindow.ui', self.base)
        self.hdf = PridaHDF()
        self.started = 0

        self.view.new_hdf.clicked.connect(self.new_hdf)
        self.view.existing_hdf.clicked.connect(self.open_hdf)
        self.view.exp_list.clicked.connect(self.open_exp)
        self.view.rep_list.clicked.connect(self.open_rep)
        self.view.new_exp.clicked.connect(self.new_exp)
        self.view.new_rep.clicked.connect(self.new_rep)
        self.view.go.clicked.connect(self.start_rep)
        self.view.cancel.clicked.connect(self.cancel_rep)
        self.view.statusBar.showMessage('sweet status bar awaiting signals')

        self.hardware_thread.finished.connect(self.check_continue)

        self.exp_list = QStandardItemModel()
        self.rep_list = QStandardItemModel()

        self.base.setWindowTitle('Prida 0.3.12')
        self.base.show()

    def start_rep(self):
        self.started = 1
        self.motor.set_num_photos(self.hdf.get_rep_images())
        self.hardware_thread.start(QThread.TimeCriticalPriority)

    def check_continue(self):
        if self.started and self.camera.get_count() < self.hdf.get_rep_images():
            self.hardware_thread.start(QThread.TimeCriticalPriority)
        else:
            self.cancel_rep()

    def cancel_rep(self):
        self.camera.reset_count()
        self.motor.turn_off_motors()
        self.started = 0

    def new_hdf(self):
        popup = uic.loadUi('input_user.ui')
        if popup.exec_():
            path = QFileDialog.getSaveFileName()[0]
            if path != '':
                if path[-5:] != '.hdf5':
                    path += '.hdf5'
                self.hdf.open_file(path)
                self.hdf.set_root_user(str(popup.user.text()))
                self.hdf.set_root_addr(str(popup.email.text()))
                self.update_user()
                self.update_exp_list()
                self.clear_rep_list()
                self.clear_exp_details()
                self.clear_rep_details()

    def open_hdf(self):
        path = QFileDialog.getOpenFileName(filter='*.hdf5')[0]
        if path != '':
            self.hdf.open_file(path)
            self.update_exp_list()
            self.clear_rep_list()
            self.update_user()
            self.clear_exp_details()
            self.clear_rep_details()

    def update_exp_details(self):
        self.view.exp_title.setText(self.hdf.get_exp_title())
        self.view.genus_species.setText(self.hdf.get_exp_genus_species())

    def clear_exp_details(self):
        self.view.exp_title.setText('')
        self.view.genus_species.setText('')

    def update_rep_details(self):
        self.view.date.setText(str(self.hdf.get_rep_date()))
        self.view.rep_number.setText(str(self.hdf.get_rep_number()))
        self.view.rep_images.setText(str(self.hdf.get_rep_images()))
        self.view.cultivar.setText(self.hdf.get_rep_cultivar())

    def clear_rep_details(self):
        self.view.date.setText('')
        self.view.rep_number.setText('')
        self.view.rep_images.setText('')
        self.view.cultivar.setText('')

    def update_user(self):
        self.view.user.setText(self.hdf.get_root_user())
        self.view.email.setText(self.hdf.get_root_addr())

    def open_exp(self, exp_index):
        self.hdf.open_exp(exp_index.data())
        self.update_rep_list()
        self.clear_rep_details()
        self.update_exp_details()

    def open_rep(self, rep_index):
        self.hdf.open_rep(rep_index.data())
        #self.update_
        self.update_rep_details()

    def new_exp(self):
        popup = uic.loadUi('input_exp.ui')
        if popup.exec_():
            self.hdf.new_exp()
            self.hdf.set_exp_title(str(popup.exp_title.text()))
            self.hdf.set_exp_genus_species(str(popup.genus_species.text()))
            self.clear_exp_details()
            self.clear_rep_details()
            self.update_exp_list()
            self.update_rep_list()

    def new_rep(self):
        popup = uic.loadUi('input_rep.ui')
        if popup.exec_():
            self.hdf.new_rep()
            self.hdf.set_rep_date(popup.date.date().toPyDate())
            self.hdf.set_rep_number(int(popup.rep_number.text()))
            self.hdf.set_rep_images(int(popup.rep_images.text()))
            self.hdf.set_rep_cultivar(str(popup.cultivar.text()))
            self.clear_rep_details()
            self.update_rep_list()

    def update_exp_list(self):
        self.exp_list.clear()
        for item in self.hdf.list_exps():
            self.exp_list.appendRow(QStandardItem(item))
        self.view.exp_list.setModel(self.exp_list)

    def update_rep_list(self):
        self.rep_list.clear()
        for item in self.hdf.list_reps():
            self.rep_list.appendRow(QStandardItem(item))
        self.view.rep_list.setModel(self.rep_list)

    def clear_rep_list(self):
        self.rep_list.clear()
        self.view.rep_list.setModel(self.rep_list)

if __name__ == '__main__':
    #sys.exit(Prida().exec_())
    app = Prida()
    app.exec_()
    sys.exit(app.hardware_thread.wait())

