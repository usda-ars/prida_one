#!/usr/bin/python
#
# motor.py
#
# VERSION: 0.5.8
#
# LAST EDIT: 2015-09-28
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# b00ff0f: Nathanael Shaw - 2015-09-21 14:49:24
#     Update changelogs
# 06ab4ef: Nathanael Shaw - 2015-09-09 14:24:45
#     Import styling modifications
# 6feaae1: Nathanael Shaw - 2015-09-08 15:40:08
#     minor changes
# d5347a2: Nathanael Shaw - 2015-09-08 12:17:09
#     Updated PEP8 styling. Attempted Python 2-3 compatability.
# 3bfa2ec: Nathanael Shaw - 2015-08-26 11:24:53
#     Added skeleton connect, poweroff and check_status behaviours to led, motor and piezo
# 105d4f8: Nathanael Shaw - 2015-08-25 15:42:02
#     Minor code cleanup, updated changelogs
# d359525: Nathanael Shaw - 2015-08-21 09:57:44
#     Fixed MRO error
# f27dd02: Nathanael Shaw - 2015-08-21 09:10:49
#     Added override functions to motor and camera for peripheral behaviours. Removed bad subprocess based properties from camera.
# a944097: Nathanael Shaw - 2015-08-19 11:19:54
#     More minor changes
# e3bd04e: Nathanael Shaw - 2015-08-19 09:08:17
#     Minor updates to docstrings
# f70ec34: Nathanael Shaw - 2015-08-18 10:25:56
#     First draft of pridaperipheral.py
# b66b4f8: Nathanael Shaw - 2015-08-14 17:46:48
#     Better changelogs better life
# 83aab76: Nathanael Shaw - 2015-08-14 16:11:35
#     Added file specific commit logs to hardware classes.
# c0053e4: Nathanael Shaw - 2015-08-13 17:04:08
#     Added additional error handling to imaging and motor for various edge cases.
# 0bb2e2d: Nathanael Shaw - 2015-08-13 15:50:41
#     Added basic position tracking in Imaging for updting PRIDA GUI statusbar.
# cba66c4: Nathanael Shaw - 2015-08-12 16:32:25
#     Fixed inefficent set construction in logical tests. Minor cleanup in imaging.py's _write_config
# fb53909: Nathanael Shaw - 2015-08-10 16:23:18
#     Implemented config file basics. Implmented auto-microstep curve selection. Small modifications to adafruit_motorhat lib.
# 326627f: Nathanael Shaw - 2015-08-07 08:52:12
#     motor/motorHAT updates
# 5cc441f: Nathanael Shaw - 2015-08-06 13:28:31
#     Added 'microsteps' property.
# 2654f0d: Nathanael Shaw - 2015-08-06 11:07:51
#     Added 'degrees' property to motor.py
# b041550: Nathanael Shaw - 2015-08-05 09:18:10
#     First draft of config parser behaviour
# 1ad7a76: Nathanael Shaw - 2015-07-30 14:48:24
#     Fully pep8 compliant hardware class flies.
# 3fd4dda: Nathanael Shaw - 2015-07-30 12:28:50
#     num_photos removed from motor.py
# 2fa39ec: Nathanael Shaw - 2015-07-30 09:51:58
#     Corrected syntax errors.
# 6c3f884: Nathanael Shaw - 2015-07-29 10:57:08
#     Converted motor.py to pythonic class with properties. Minor fixes to piezo.py.
# 5ed7639: Nathanael Shaw - 2015-07-27 13:31:20
#     Converted motor.py to new-style class.
# 79283b9: Nathanael Shaw - 2015-07-23 14:16:09
#     More graceful exit testing
# ccffdeb: Nathanael Shaw - 2015-07-23 11:07:14
#     Added getters to motor.py
# 588958f: Nathanael Shaw - 2015-07-22 15:17:55
#     Added atexit functionality to motor.py and led.py. Shifted imaging.py from inheriting motor/led/piezo/camera to having attributes of those objects.
# fe82fdc: Nathanael Shaw - 2015-07-15 19:09:26
#     Resolves #34
# 9398843: Hoi Cheng - 2015-07-15 18:19:24
#     Fixing docstrings. Added volume control to piezo.py
# 184a091: Nathanael Shaw - 2015-07-15 15:16:03
#     Initial attempt at new class organization.
# 0ac18e2: Nathanael Shaw - 2015-02-16 20:56:28 -0500
#     Proper PEP8 docstrings. Fixed all_on function.
# 9a356b5: Nathanael Shaw - 2015-07-15 10:45:42
#     Reduced magic number usage in LED and Piezo functions.
# a098ceb: Nathanael Shaw - 2015-07-10 17:08:13
#     motor.py now PEP8
# 3a19139: Nathanael Shaw - 2015-02-16 15:20:21 -0500
#     Resolves #23
# d6cf42f: Nathanael Shaw - 2015-07-10 11:46:06
#     Added piezo and led functions to motor.py
# 2663627: Nathanael Shaw - 2015-07-01 10:37:49
#     Expirimenting with proper python-esque inheritance.
# 4bfdd02: Nathanael Shaw - 2015-06-30 16:22:26
#     Corrected motor.py to conform to PEP8
# 8a4cf0e: Nathanael Shaw - 2015-06-30 16:12:47
#     Tested and adjusted motor.py
# aebca4d: Nathanael Shaw - 2015-06-30 15:12:45
#     Converted motor.py to class (untested)
# 3642855: Nathanael Shaw - 2015-06-30 12:42:30
#     Added microstep resolution detection to motor.py
# 773aec5: Nathanael Shaw - 2015-06-29 11:24:36
#     Fixed motor.py indentations, added testMotor.py to back-up folder.
# f9c4529: Nathanael Shaw - 2015-06-24 11:57:30
#     Modifed Motor.py step function, added RPM selection support.
# 6a90843: Jay - 2015-06-18 13:58:07
#     bugfixes
# 48d047e: Jay - 2015-06-16 14:10:50
#     v0.1.1
# db5bcc3: Hoi Cheng - 2015-06-16 01:46:15
#     v0.0.4
# 50cf878: Jay - 2015-06-15 13:48:22
#     v0.0.4
# ea22481: jay - 2015-06-14 02:33:49
#     v0.0.2
# 1de1b65: Jay - 2015-06-12 14:04:23
#     Merge branch 'master' of https://bitbucket.org/usda-ars/prida
# f4ae6b2: Jay - 2015-06-12 14:02:08
#     0.0.1
# 2b9a07c: Jay - 2015-06-11 15:12:30
#     Working on GUI
# 0ef64ad: Jay - 2015-06-11 11:33:26
#     fixing loops
# 629d58c: Jay - 2015-05-30 02:08:46
#     Setting up GUI
# $log_end_tag$
#
#System Modules
import time
#3rd Party Modules
from Adafruit_MotorHAT import Adafruit_MotorHAT
#Original Modules
from pridaperipheral import PRIDAPeripheral, PRIDAPeripheralError


class Motor(Adafruit_MotorHAT, PRIDAPeripheral):

    def __init__(self):
        """
        Initialize data memebers. Inherit attributes and
        behaviours from the Adafruit_MotorHAT object.
        Note: currently assumes that system is always using
        microstepping.
        """
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self._rpm = 2.0           # Approximate rotational velocity
        self._step_res = 200      # Number of units steps per rotation
        self._motor_port_num = 1  # Motor HAT Port Number In Use
        self._sm = self.getStepper(self.step_res, self.motor_port_num)
        #                        ^ Adafruit Stepper Motor Object
        #                       \ / Minimum Time Elapsed In a Rotation
        self._base_rot_time = 1.75 * self._sm.MICROSTEPS
        self._permit_edits = False

    @property
    def microsteps(self):
        """
        Name:    Motor.microsteps
        Feature: Returns number of microsteps per unit step
        Inputs:  None
        Outputs: Int, number of microsteps per unit step (self.microsteps)
        """
        return self._sm.MICROSTEPS

    @microsteps.setter
    def microsteps(self, value):
        """
        Name:    Motor.microsteps
        Feature: Sets the number of microsteps per unit step
        Inputs:  Int, number of microsteps per unit step (value)
        Outputs: None
        """
        if type(value) is int:
            if value in {4, 8, 12, 16}:
                self._sm.MICROSTEPS = value
                self._sm._build_microstep_curve()

    @property
    def degrees(self):
        """
        Name:    Motor.degrees
        Feature: Returns Step Resolution in terms of degrees per unit step.
        Inputs:  None
        Outputs: Float, degree representation of step resolution
                 (Motor.degrees)
        """
        return 360.0 / self.step_res

    @degrees.setter
    def degrees(self, value):
        """
        Name:    Motor.degrees
        Feature: Sets Step Resolution in terms of degrees per unit step.
        Inputs:  Float, desired degree representation of step resolution
                 (Motor.degrees)
        Outputs: None
        """
        if type(value) in {int, float}:
            if 0 < value < 360:
                if (360.0 / value) % 1 == 0:
                    self.step_res = int(360 / value)
                else:
                    raise ValueError('Degrees attribute' +
                                     ' must evenly divide 360.')

    @property
    def rpm(self):
        """
        Name:    Motor.rpm
        Feature: Returns the approximate RPM value at which the motor is
                 to spin
        Inputs:  None
        Outputs: Float, value approximating currently assigned motor
                 rotational speed (self._rpm)
        """
        return self._rpm

    @property
    def step_res(self):
        """
        Name:    Motor.step_res
        Feature: Returns the number of unit steps within a single full
                 rotation at which the motor is currently configured
        Inputs:  None
        Outputs: Integer, number of unit steps per full rotation
                 (self._step_res)
        """
        return self._step_res

    @property
    def motor_port_num(self):
        """
        Name:    Motor.motor_port_num
        Feature: Returns the port number which the hat is currently
                 controlling
        Inputs:  None
        Outputs: Integer, the number representing the in-use motorHAT
                 port (self._motor_port_num)
        """
        return self._motor_port_num

    @property
    def base_rot_time(self):
        """
        Name:    Motor.base_rot_time
        Feature: Returns the minimum amount of time at which the motor
                 can complete a full rotation with a microstepping
                 regieme
        Inputs:  None
        Outputs: Float, minimum number of seconds for a full rotation
                 (self._base_rot_time)
        """
        return self._base_rot_time

    @property
    def permit_edits(self):
        """
        Name:    Motor.permit_edits
        Feature: Returns whether or not editing critical attributes is
                 permitted.
        Inputs:  None
        Outputs: Boolean, value indicating permission to edit
                 (self._permit_edits)
        """
        return self._permit_edits

    @rpm.setter
    def rpm(self, rot_per_min):
        """
        Name:    Motor.rpm
        Feature: Set approximate rotations per minute value for the
                 motor.
        Inputs:  float, desired rpm value (rot_per_min)
        Outputs: None
        """
        if type(rot_per_min) is float or type(rot_per_min) is int:
            if rot_per_min > 0:
                self._rpm = rot_per_min
            else:
                print('RPM requires a non-negative value.')
        else:
            print('RPM must be a number.')

    @step_res.setter
    def step_res(self, sr):
        """
        Name:    Motor.step_res
        Feature: Set stepper motor step resolution. Not currently
                 supported. Data member added to reduce presence of
                 'magic numbers.'
        Inputs:  - int, desired 'step resolution,' or the number of
                   steps in a full rotation, defining the length of a
                   unit step (sr)
        Outputs: None
        """
        if self._permit_edits is True:
            if type(sr) is int:
                if sr > 0:
                    self._step_res = sr
                else:
                    print('Step resolution must be non-negative')
            else:
                print('Step resolution must be an integer')
        else:
            print('Editing step resolution is forbidden.')

    @motor_port_num.setter
    def motor_port_num(self, num):
        """
        Name:    Motor.motor_port_num
        Feature: Set stepper motor port number. Not currently supported.
                 Data member added to reduce presence of 'magic numbers.'
        Inputs:  - int, desired port number to be used on the HAT (num)
        Outputs: None
        """
        if self._permit_edits is True:
            if type(num) is int:
                if num in {1, 2}:
                    self._motor_port_num = num
                else:
                    print('Motor port number restricted to 1 or 2.')
            else:
                print('Motor port number must be an integer.')
        else:
            print('Editing the motor port number is forbbiden.')

    @base_rot_time.setter
    def base_rot_time(self, base_time):
        """
        Name:    Motor.base_rot_time
        Feature: Set stepper motor base rotation time (in seconds)
                 During full speed operation. Not currently supported.
                 Data member added to reduce presence of 'magic numbers.'
        Inputs:  - int, minmum rotation time in seconds (base_time)
        Outputs: None
        """
        if self._permit_edits is True:
            if type(base_time) is int or type(base_time) is float:
                if base_time > 0:
                    self._base_rot_time = base_time
                else:
                    print('Base rotation time must be non-negative.')
            else:
                print('Base rotation time must be a number.')
        else:
            print('Editing base rotation time is forbidden.')

    @permit_edits.setter
    def permit_edits(self, permit):
        """
        Name:    Motor.permit_edits
        Feature: Sets the permit edits attribute to a specified
                 boolean value
        Inputs:  Boolean, desired editing permissions (permit)
        Outputs: None
        """
        if type(permit) is bool:
            self._permit_edits = permit
        else:
            print('Permit edits attribute requires a boolean value.')

    def turn_off_motors(self):
        """
        Name:    Motor.turn_off_motors
        Feature: Disable all motors attached to the HAT.
        Inputs:  None
        Outputs: None
        """
        self.getMotor(1).run(self.RELEASE)
        self.getMotor(2).run(self.RELEASE)
        self.getMotor(3).run(self.RELEASE)
        self.getMotor(4).run(self.RELEASE)

    def calibrate(self):
        """
        Name:    Motor.calibrate
        Feature: Take three initial unit steps.
        Inputs:  None
        Outputs: None
        """
        for i in range(3 * self._sm.MICROSTEPS):
            self._sm.oneStep(self.FORWARD, self.MICROSTEP)
            time.sleep(.1)

    def step(self, num_photos):
        """
        Name:    Motor.step
        Feature: Take a single Inter-Photo-Angle (IPA) step.
                 Take the desired number of photos in the run and
                 calculate the IPA step size. Convert the desired RPM
                 value into the appropriate length wait time.
        Inputs:  Integer, number of photos to be taken (num_photos)
        Outputs: None
        """
        # data member error checking
        if self.rpm <= 0:
            print("RPM must be greater than zero.")
            return None
        try:
            # convert RPM to req'd value for calculating wait time
            real_rpm = (1.0 / (((60.0 / self.rpm) -
                               self.base_rot_time) / 60.0))
        except ZeroDivisionError:
            print("Set a valid RPM value.")
        else:
            # begin stepping
            for i in range((self.step_res * self._sm.MICROSTEPS) /
                           num_photos):
                self._sm.oneStep(self.FORWARD, self.MICROSTEP)
                time.sleep(60.0 / (self.step_res *
                                   self._sm.MICROSTEPS * real_rpm))

    def single_step(self):
        """
        Name:    motor.single_step
        Feature: Takes a single microstep
        Inputs:  None
        Outputs: None
        """
        # data member error checking
        if self.rpm <= 0:
            print("RPM must be greater than zero.")
            return None
        try:
            # convert RPM to req'd value for calculating wait time
            real_rpm = (1.0 / (((60.0 / self.rpm) -
                               self.base_rot_time) / 60.0))
            if real_rpm <= 0:
                raise ZeroDivisionError()
        except ZeroDivisionError:
            raise AttributeError("Set a valid RPM value" +
                                 " before attempting a step.")
        else:
            self._sm.oneStep(self.FORWARD, self.MICROSTEP)
            time.sleep(60.0 / (self.step_res * self._sm.MICROSTEPS * real_rpm))

    def connect(self):
        try:
            self.calibrate()
        except:
            raise PRIDAPeripheralError('Could not connect to motor!')

    def poweroff(self):
        self.turn_off_motors()

    def current_status(self):
        super(self.current_status())
