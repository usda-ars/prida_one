#!/usr/bin/python
#
# pridaperipheral.py
#
# VERSION: 0.5.8
#
# LAST EDIT: 2015-09-28
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# b00ff0f: Nathanael Shaw - 2015-09-21 14:49:24
#     Update changelogs
# 06ab4ef: Nathanael Shaw - 2015-09-09 14:24:45
#     Import styling modifications
# 105d4f8: Nathanael Shaw - 2015-08-25 15:42:02
#     Minor code cleanup, updated changelogs
# a944097: Nathanael Shaw - 2015-08-19 11:19:54
#     More minor changes
# e3bd04e: Nathanael Shaw - 2015-08-19 09:08:17
#     Minor updates to docstrings
# f70ec34: Nathanael Shaw - 2015-08-18 10:25:56
#     First draft of pridaperipheral.py
# $log_end_tag$
#


class PRIDAPeripheral(object):
    """
    Base 'Abstract Class' that all PRIDA harddware inherit from for proper
    polymorphic behaviour. Garuntee's the existance of the poweroff, connect
    and current_status behviours in addition to the state attribute as well as
    the base PRIDAPeripheralError exception.
    """

    def __init__(self):
        """
        Initialize and declare common peripheral instance variables.
        """
        self._state = None

    @property
    def state(self):
        """
        Getter for state attribute; to be overwritten by child class.
        """
        return self._state

    @state.setter
    def state(self, value):
        """
        Setter for state attribute; to be overwritten by child class.
        """
        self._state = value

    @state.deleter
    def state(self):
        """
        Deleter for state attribute; to be overwritten by child class.
        """
        del self._state

    def poweroff(self):
        """
        Name:    PRIDAPeripheral.poweroff
        Feature: Abstract behaviour. Override by child with shutdown behaviour.
        Inputs:  None
        Outputs: None
        """
        raise PRIDAPeripheralError('All peripherals require a shutdown sequence!')

    def connect(self):
        """
        Name:    PRIDAPeripheral.connect
        Feature: Abstract behaviour. Override by child with connection
                 establishing behaviour.
        Inputs:  None
        Outputs: None
        """
        raise PRIDAPeripheralError('All peripherals require a connect sequence!')

    def current_status(self):
        """
        Name:    PRIDAPeripheral.current_status
        Feature: Abstract behaviour. Override by child with behaviour for
                 attempting communication between the system and the
                 peripheral.
        Inputs:  None
        Outputs: None
        """
        raise PRIDAPeripheralError('All peripherals require knowledge about their current status!')

class PRIDAPeripheralError(Exception):
    """
    Base 'Exception' class for errors specific to PRIDA hardware.
    """
    pass
