#!/usr/bin/python
#
# camera.py
#
# VERSION: 0.5.8
#
# LAST EDIT: 2015-09-28
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# b00ff0f: Nathanael Shaw - 2015-09-21 14:49:24
#     Update changelogs
# 06ab4ef: Nathanael Shaw - 2015-09-09 14:24:45
#     Import styling modifications
# 6feaae1: Nathanael Shaw - 2015-09-08 15:40:08
#     minor changes
# d5347a2: Nathanael Shaw - 2015-09-08 12:17:09
#     Updated PEP8 styling. Attempted Python 2-3 compatability.
# 105d4f8: Nathanael Shaw - 2015-08-25 15:42:02
#     Minor code cleanup, updated changelogs
# 66ced9c: Nathanael Shaw - 2015-08-24 16:12:41
#     Tested changes, fixed minor error
# dc5e44e: Nathanael Shaw - 2015-08-24 20:10:27
#     Added make and model getters
# 86dd8d8: Nathanael Shaw - 2015-08-24 16:04:36
#     Fixed minor errors, tested getters, all is well.
# 9a9acfe: Nathanael Shaw - 2015-08-24 15:51:29
#     Drafted remaining camera getters.
# 505b4b4: Nathanael Shaw - 2015-08-24 13:15:27
#     Disabled setters. Pending decision on partial remote manipulation vs fully manual.
# dda366b: Nathanael Shaw - 2015-08-21 15:06:59
#     Added docstrings to the private functions. Minor fixes.
# a4b6267: Nathanael Shaw - 2015-08-21 14:47:18
#     weird setter errors. some of the c functions might be mutating attributes unexpecedly.
# 50e44c6: Nathanael Shaw - 2015-08-21 14:08:20
#     Adding back removed syntax now that bug has been identified
# 89aad70: Nathanael Shaw - 2015-08-21 14:06:56
#     GOSH DARN IT NATHAN
# c11a1de: Nathanael Shaw - 2015-08-21 14:00:57
#     more camera debugging
# 02b8243: Nathanael Shaw - 2015-08-21 13:54:12
#     once more with feeling
# b8589cb: Nathanael Shaw - 2015-08-21 13:52:36
#     fixed gphoto2error syntax error
# 1436b3d: Nathanael Shaw - 2015-08-21 12:35:01
#     bugfixes
# 8ef8b19: Nathanael Shaw - 2015-08-21 12:21:27
#     New attr getter and setter. Test with summary property.
# 72b5d17: Nathanael Shaw - 2015-08-21 10:29:40
#     Added 'poweroff' (safe disconnect) behaviour
# 0f3043a: Nathanael Shaw - 2015-08-21 10:22:31
#     fixed camera.summary
# e68ea5b: Nathanael Shaw - 2015-08-21 10:01:36
#     Commented out broken multiple camera connections code
# d359525: Nathanael Shaw - 2015-08-21 09:57:44
#     Fixed MRO error
# 6adf0ce: Nathanael Shaw - 2015-08-21 09:53:55
#     Camera summary getter
# 2a9a5ba: Nathanael Shaw - 2015-08-21 09:27:52
#     Testing gphoto2 python wrapper
# f27dd02: Nathanael Shaw - 2015-08-21 09:10:49
#     Added override functions to motor and camera for peripheral behaviours. Removed bad subprocess based properties from camera.
# a944097: Nathanael Shaw - 2015-08-19 11:19:54
#     More minor changes
# e3bd04e: Nathanael Shaw - 2015-08-19 09:08:17
#     Minor updates to docstrings
# f70ec34: Nathanael Shaw - 2015-08-18 10:25:56
#     First draft of pridaperipheral.py
# b66b4f8: Nathanael Shaw - 2015-08-14 17:46:48
#     Better changelogs better life
# 83aab76: Nathanael Shaw - 2015-08-14 16:11:35
#     Added file specific commit logs to hardware classes.
# be8753b: Nathanael Shaw - 2015-08-13 08:37:20
#     Noted development plans/options in camera.py; commented out unimplemented configparser code.
# 88eaae4: Nathanael Shaw - 2015-08-13 08:13:17
#     removed evil tab characters
# be7c589: Nathanael Shaw - 2015-08-13 12:05:29
#     Camera Summary property hotfix
# 7181ef6: Nathanael Shaw - 2015-08-13 08:01:12
#     Added summary camera property
# cba66c4: Nathanael Shaw - 2015-08-12 16:32:25
#     Fixed inefficent set construction in logical tests. Minor cleanup in imaging.py's _write_config
# 24d6380: Nathanael Shaw - 2015-08-12 09:04:36
#     Commented out focallength getter/setter. Property not remotely accessable. Began manualfocusdrive getter/setter code.
# 91239de: Nathanael Shaw - 2015-08-11 12:25:21
#     Fixed shutterspeed and focallength setters. Added config file support to new camera settings.
# 32fd09d: Nathanael Shaw - 2015-08-11 11:50:57
#     Added shutter speed and focal length properties. Values get/set through gphoto2 command line client.
# 5d86dd0: Nathanael Shaw - 2015-07-30 12:27:49
#     Taken property removed from camera.py
# 3d9fa89: Nathanael Shaw - 2015-07-27 13:30:32
#     Converted to new-style class.
# adea595: Nathanael Shaw - 2015-07-23 11:30:12
#     Added getters and docstrings to camera.py
# e0dd9a7: Hoi Cheng - 2015-07-22 13:06:50
#     this is hoi. everything is better
# e2a1c02: jay - 2015-07-21 22:59:01
#     i think it works
# dd24989: Nathanael Shaw - 2015-06-30 14:09:27
#     bugfixes
# 76030fd: jay - 2015-06-29 23:38:19
#     v0.3.9
# 6a90843: Jay - 2015-06-18 13:58:07
#     bugfixes
# 48d047e: Jay - 2015-06-16 14:10:50
#     v0.1.1
# db5bcc3: Hoi Cheng - 2015-06-16 01:46:15
#     v0.0.4
# 50cf878: Jay - 2015-06-15 13:48:22
#     v0.0.4
# 84cf4f8: Jay - 2015-06-12 14:18:23
#     conflict fix
# f4ae6b2: Jay - 2015-06-12 14:02:08
#     0.0.1
# 0ef64ad: Jay - 2015-06-11 11:33:26
#     fixing loops
# ad84bab: Jay - 2015-05-30 05:32:29
#     GUI setup
# 629d58c: Jay - 2015-05-30 02:08:46
#     Setting up GUI
# 7866821: Jay - 2015-06-01 17:33:08
#     setting up
# 993e880: Jay - 2015-06-01 17:28:21
#     setting up
# $log_end_tag$
#
#System Modules
import os
import traceback
#3rd Part Modules
import gphoto2 as gp
#Original Modules
from pridaperipheral import PRIDAPeripheral, PRIDAPeripheralError

__license__ = "GPL-3.0"
__maintainer__ = "Nathanael Shaw"
__email__ = "nms95@cornell.edu"
__status__ = "Development"


class Camera(PRIDAPeripheral):

    #camera_set = {}
    #available = {}

    def __init__(self):
        """
        Initialize data members, connect the camera.
        """
        PRIDAPeripheral.__init__(self)
        self.path = '/tmp'
        self.context = None
        self.camera = None
        self.config = None
        self._summary = ''
        self._attr_dict = {}
        self.connect()

    def connect(self):
        """
        Name:    Camera.connect
        Feature: Connects the camera to the control system.
                 Overrides PRIDAPeripheral.connect.
        Inputs:  None
        Outputs: None
        """
        try:
            if (not self.context):
                self.context = gp.gp_context_new()
                self.camera = gp.check_result(gp.gp_camera_new())
                gp.gp_camera_init(self.camera, self.context)
                self.config = gp.check_result(gp.gp_camera_get_config(
                    self.camera, self.context))
                tmp = []
                for i in range(1000):
                    try:
                        tmp.append(self.config.get_child_by_id(i).get_label())
                    except gp.GPhoto2Error:
                        break
                self._attr_dict = dict(enumerate(tmp))
        except:
            traceback.print_exc()
            raise PRIDAPeripheralError("Could not connect to camera.")

    def poweroff(self):
        """
        Name:    Camera.poweroff
        Feature: Prepares the camera for manual shutdown.
                 Overrides PRIDAPeripheral.poweroff.
        Inputs:  None
        Outputs: None
        """
        err = gp.check_result(gp.gp_camera_exit(self.camera, self.context))
        if err >= 1:
            raise PRIDAPeripheralError('Could not disconnect' +
                                       ' camera for poweroff!')
        else:
            self.context = None
            self.camera = None
            self.config = None
            self._summary = ''
            self._attr_dict = {}
            print('Camera disconnected. Safe to power down.')

    def current_status(self):
        """
        Name:    Camera.current_status
        Feature: Identifies the current status of the camera.
                 Overrides PRIDAPeripheral.check_status.
        Inputs:  None
        Outputs: None
        """
        super(self.current_status())

    def get_full_path(self):
        """
        Name:    Camera.get_full_path
        Feature: Returns a string representing the filepath to the
                 working directory
        Inputs:  None
        Outputs: String, path to working directory (self.path)
        """
        return self.path

    def set_path(self, p):
        """
        Name:    Camera.set_path
        Feature: Sets the path to the working directory
        Inputs:  String, path to save images to (p)
        Outputs: None
        """
        self.path = p

    def capture(self, name):
        """
        Name:    Camera.capture
        Feature: Captures an image with the connected camera
        Inputs:  String, name of the image to be saved (name)
        Outputs: String, absolute file path of captured image (filename)
        """
        file_path = gp.check_result(gp.gp_camera_capture(
            self.camera, gp.GP_CAPTURE_IMAGE, self.context))
        filename = os.path.join(self.path, str(name) + ".jpg")
        print(('Copying image to: ' + filename))
        camera_file = gp.check_result(gp.gp_camera_file_get(
            self.camera, file_path.folder, file_path.name,
            gp.GP_FILE_TYPE_NORMAL, self.context))
        gp.check_result(gp.gp_file_save(camera_file, filename))
        gp.check_result(gp.gp_camera_exit(self.camera, self.context))
        return filename

    @property
    def exposure_time(self):
        """
        Name:    Camera.exposure_time
        Feature: Property describing camera exposure time or "shutter speed"
        Inputs:  None
        Outputs: Float, duration during image capture of open shutter
                 (self.exposure_time)
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return float(self._attr_getter(tmp['Shutter Speed']).strip('s'))

    @property
    def focal_length(self):
        """
        Name:    Camera.focal_length
        Feature: Returns the focal length of the camera (in mm)
        Inputs:  None
        Outputs: Integer representing the focal length
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return int(self._attr_getter(tmp['Focal Length']))

    @property
    def orientation(self):
        """
        Name:    Camera.orientation
        Feature: Returns angle value corresponding to camera position (using
                 standard polar coordinates)
        Inputs:  None
        Outputs: Integer, the angular position of the camera
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return int(self._attr_getter(tmp['Camera Orientation']))

    @property
    def iso_speed(self):
        """
        Name:    Camera.iso_speed
        Feature: Returns integer representing camera iso speed
        Inputs:  None
        Outputs: String, the manufacturer and device model of the camera
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return int(self._attr_getter(tmp['ISO Speed']))

    @property
    def aperture(self):
        """
        Name:    Camera.aperture
        Feature: Returns integer representing camera aperture (if determinable)
        Inputs:  None
        Outputs: Integer, the camera aperture (if known)
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        apt_at_min = self._attr_getter(
            tmp['Maximum Aperture at Focal Length Maximum'])
        apt_at_max = self._attr_getter(
            tmp['Maximum Aperture at Focal Length Minimum'])
        if apt_at_min == apt_at_max:
            return int(apt_at_min)
        else:
            return 'Unknown. Between %s - %s.' % (apt_at_min, apt_at_max)

    @property
    def image_size(self):
        """
        Name:    Camera.image_size
        Feature: Returns camera image resolution
        Inputs:  None
        Outputs: List (int), the pixel height and width of the images
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        img_size = self._attr_getter(tmp['Image Size'])
        img_size = img_size.split('x')
        return [int(img_size[0]), int(img_size[1])]

    @property
    def image_height(self):
        """
        Name:    Camera.image_height
        Feature: Returns height dimension of the image resolution
        Inputs:  None
        Outputs: Integer, the image height in pixels
        """
        tmp = self.image_size
        if self.orientation in {90, 270}:
            tmp.sort()
            return tmp.pop()
        else:
            tmp.sort(reverse=True)
            return tmp.pop()

    @property
    def image_width(self):
        """
        Name:    Camera.image_width
        Feature: Returns width dimension of the image resolution
        Inputs:  None
        Outputs: Integer, the image width in pixels
        """
        tmp = self.image_size
        if self.orientation in {0, 180}:
            tmp.sort()
            return tmp.pop()
        else:
            tmp.sort(reverse=True)
            return tmp.pop()

    @property
    def summary(self):
        """
        Name:    Camera.summary
        Feature: Returns string containing gphoto2 camera summary data
        Inputs:  None
        Outputs: String, the manufacturer and device model of the camera
        """
        return ' '.join([self.make, self.model])

    @property
    def make(self):
        """
        Name:    Camera.make
        Feature: Returns string containing camera manufacturer
        Inputs:  None
        Outputs: String, the manufacturer of the camera
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return self._attr_getter(tmp['Camera Manufacturer'])

    @property
    def model(self):
        """
        Name:    Camera.model
        Feature: Returns string containing camera model
        Inputs:  None
        Outputs: String, the model of the camera
        """
        tmp = {self._attr_dict[y]: y for y in list(self._attr_dict.keys())}
        return self._attr_getter(tmp['Camera Model'])

    def _attr_setter(self, index, value):
        """
        Name:    Camera._attr_setter
        Feature: Queries the camera for an atribute at an id and attempts
                 assignment to that attribute.
        Inputs:  Unspecified type, the value to attempt to assign (value)
        Outputs: None
        """
        if index in self._attr_dict:
            try:
                self.config.get_child_by_id(index).set_value(value)
                self.camera.set_config(self.config, self.context)
            except gp.GPhoto2Error:
                PRIDAPeripheralError(' "%s" is a bad parameter for "%s".'
                    % (str(value), str(self._attr_dict[index])))
        else:
            raise KeyError(' %i not a valid attribute id.' % index)

    def _attr_getter(self, index):
        """
        Name:    Camera._attr_getter
        Feature: Queries the camera for an attributes value at a specific id
        Inputs:  None
        Outputs: Unspecified type, the value of the queried attribute
        """
        if index in self._attr_dict:
            return self.config.get_child_by_id(index).get_value()
        else:
            raise KeyError(' %i not a valid attribute id.' % index)
