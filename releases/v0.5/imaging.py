#!/usr/bin/python
#
# imaging.py
#
# VERSION: 0.5.8
#
# LAST EDIT: 2015-09-28
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# b00ff0f: Nathanael Shaw - 2015-09-21 14:49:24
#     Update changelogs
# 06ab4ef: Nathanael Shaw - 2015-09-09 14:24:45
#     Import styling modifications
# 6feaae1: Nathanael Shaw - 2015-09-08 15:40:08
#     minor changes
# d5347a2: Nathanael Shaw - 2015-09-08 12:17:09
#     Updated PEP8 styling. Attempted Python 2-3 compatability.
# 105d4f8: Nathanael Shaw - 2015-08-25 15:42:02
#     Minor code cleanup, updated changelogs
# a944097: Nathanael Shaw - 2015-08-19 11:19:54
#     More minor changes
# b66b4f8: Nathanael Shaw - 2015-08-14 17:46:48
#     Better changelogs better life
# 83aab76: Nathanael Shaw - 2015-08-14 16:11:35
#     Added file specific commit logs to hardware classes.
# eadb171: Nathanael Shaw - 2015-08-14 09:55:16
#     Fixed error where imaging was writing config files willy-nilly.
# c0053e4: Nathanael Shaw - 2015-08-13 17:04:08
#     Added additional error handling to imaging and motor for various edge cases.
# 0bb2e2d: Nathanael Shaw - 2015-08-13 15:50:41
#     Added basic position tracking in Imaging for updting PRIDA GUI statusbar.
# 345bf1c: Nathanael Shaw - 2015-08-13 12:59:05
#     Changed conf file name; added missing conf file handling.
# e07a243: Nathanael Shaw - 2015-08-13 12:39:40
#     Made new config file with new camera parameters and re-structured writer function. Removed TZ testing print statement from imaging.py
# be8753b: Nathanael Shaw - 2015-08-13 08:37:20
#     Noted development plans/options in camera.py; commented out unimplemented configparser code.
# cba66c4: Nathanael Shaw - 2015-08-12 16:32:25
#     Fixed inefficent set construction in logical tests. Minor cleanup in imaging.py's _write_config
# 6fc6f0b: Nathanael Shaw - 2015-08-12 10:08:57
#     Removed some old testing print statements.
# 91239de: Nathanael Shaw - 2015-08-11 12:25:21
#     Fixed shutterspeed and focallength setters. Added config file support to new camera settings.
# acf014d: Nathanael Shaw - 2015-08-11 09:53:29
#     Replaced _parse_config_file try-excepts with if-else-raises.
# 26035da: Nathanael Shaw - 2015-08-11 09:24:15
#     Boot sequence now checks config file led type. Fixed datetime syntax error.
# 579b1e6: Nathanael Shaw - 2015-08-11 08:47:47
#     Gave captured images timestamps for names.
# 45db0f2: Nathanael Shaw - 2015-08-10 17:27:12
#     Fixed atexit func to properly adapt to conf file
# fb53909: Nathanael Shaw - 2015-08-10 16:23:18
#     Implemented config file basics. Implmented auto-microstep curve selection. Small modifications to adafruit_motorhat lib.
# 9960369: Nathanael Shaw - 2015-08-06 13:45:01
#     Enforced PEP8 conformance.
# c824653: Nathanael Shaw - 2015-08-06 13:27:19
#     hotfix
# 12c53d3: Nathanael Shaw - 2015-08-06 11:08:35
#     Added type casting helper function for config parsing.
# 780e2fa: Nathanael Shaw - 2015-08-06 10:24:33
#     Switched to python parseconfig module
# 1fa124f: Nathanael Shaw - 2015-08-05 11:07:55
#     proper getattr/setattr usage
# ad6e646: Nathanael Shaw - 2015-08-05 10:34:27
#     buggy as all getout
# b041550: Nathanael Shaw - 2015-08-05 09:18:10
#     First draft of config parser behaviour
# 1ad7a76: Nathanael Shaw - 2015-07-30 14:48:24
#     Fully pep8 compliant hardware class flies.
# ff9592f: Nathanael Shaw - 2015-07-30 12:41:39
#     Fixed minor copy-pasta error.
# a97b834: Nathanael Shaw - 2015-07-30 12:36:25
#     Merge branch 'imgdev' of https://bitbucket.org/usda-ars/prida into imgdev
# 1bcf70f: Nathanael Shaw - 2015-07-30 12:29:11
#     taken and num_photos now attributes of imaging.py
# a0bd881: Nathanael Shaw - 2015-07-30 11:23:18
#     Further defining how to interact with an Imaging object.
# 054c903: Nathanael Shaw - 2015-07-30 09:51:40
#     Corrected syntax errors.
# 7bdc7d6: Nathanael Shaw - 2015-07-29 10:27:44
#     Addressed minor syntax errors.
# b4c5c4c: Nathanael Shaw - 2015-07-28 15:08:07
#     More property testing
# 93057d0: Nathanael Shaw - 2015-07-28 11:52:59
#     Further property updates
# 34fed9c: Nathanael Shaw - 2015-07-27 13:32:18
#     Converted imaging.py to new style class; added example property decorator getter/setter/deleter methods.
# c6fbee8: Nathanael Shaw - 2015-07-27 12:23:40
#     Merge branch 'master' into imgdev. Adressed conflict.
# 21db1f6: Hoi Cheng - 2015-07-27 12:08:10
#     refixed
# 3332d05: Nathanael Shaw - 2015-07-27 11:51:02
#     Corrected imaging.py to give error beep on hardware error detection.
# d792eba: Nathanael Shaw - 2015-07-23 14:39:38
#     Implemented (some) gracefull exiting procedures for turning off the hardware.
# 79283b9: Nathanael Shaw - 2015-07-23 14:16:09
#     More graceful exit testing
# 992c937: Nathanael Shaw - 2015-07-23 11:29:43
#     Removed redundant camera.connect() call.
# 4040732: Nathanael Shaw - 2015-07-23 11:04:52
#     Added getters to imaging.py
# 588958f: Nathanael Shaw - 2015-07-22 15:17:55
#     Added atexit functionality to motor.py and led.py. Shifted imaging.py from inheriting motor/led/piezo/camera to having attributes of those objects.
# 3892066: Nathanael Shaw - 2015-07-22 13:50:29
#     Fixed imaging.py run_sequence docstring.
# fe82fdc: Nathanael Shaw - 2015-07-15 19:09:26
#     Resolves #34
# 9398843: Hoi Cheng - 2015-07-15 18:19:24
#     Fixing docstrings. Added volume control to piezo.py
# 184a091: Nathanael Shaw - 2015-07-15 15:16:03
#     Initial attempt at new class organization.
# $log_end_tag$
#
#System Modules
import time
import atexit
import traceback
import os
import datetime
try:
    from configparser import SafeConfigParser  # Python 3+
except:
    from ConfigParser import SafeConfigParser  # Python 2
#Original Modules
from motor import Motor
from camera import Camera
from led import LED
from piezo import Piezo

__license__ = "GPL-3.0"
__maintainer__ = "Nathanael Shaw"
__email__ = "nms95@cornell.edu"
__status__ = "Development"


@atexit.register
def exit_sequence():
    """
    Name:    Imaging.exit_sequence
    Feature: Disables all hardware before application close
    Inputs:  None
    Outputs: None
    """
    i = Imaging()
    i.motor.turn_off_motors()
    i.led.all_off()


class Imaging(object):
    """
    Aggregate imaging hardware class for PRIDA.
    """
    _motor_hat = Motor()    # Object controlling the motor hat
    _camera = Camera()      # Object controlling the camera
    _led = LED()            # Object controlling the led
    _piezo = Piezo()        # Object controlling the piezo
    _num_photos = 100       # Total to collect
    _taken = 0              # Number of current photo
    _position = 0           # Current position of the motor in degrees
    _is_running = False     # indicates if system is mid DAQ
    _is_error = False       # indicates system error
    _permit_edits = False   # indicates if datamembers are settable

    def __init__(self, path=None):
        """
        Initializes hardware states.
        """
        if path is not None:
            self._parse_config_file(path)    # pulls settings from prida.config
        else:
            self._parse_config_file()
        self._led.green_on()         # led indicates ready for DAQ
        self.motor._sm._build_microstep_curve()

    @property
    def motor(self):
        """
        Name:    Imaging.motor
        Feature: Return _motor_hat data member (getter)
        Inputs:  None
        Outputs: Motor, a Adafruit_MotorHAT abstraction (self._motor_hat)
        """
        return self._motor_hat

    @motor.setter
    def motor(self, value):
        """
        Name:    Imaging.motor
        Feature: _motor_hat attribute setter
        Inputs:  Motor, an Adafruit_MotorHAT abstraction (value)
        Outputs: None
        """
        if self._permit_edits is True:
            if isinstance(value, Motor()):
                self._motor_hat = value
            else:
                print('_motor_hat attribute must be a Motor object.')
        else:
            print('Editing this field not permitted.')

    @property
    def camera(self):
        """
        Name:    Imaging.camera
        Feature: Return camera data member
        Inputs:  None
        Outputs: Camera, a gphoto2 camera object abstraction
                 (self._camera)
        """
        return self._camera

    @camera.setter
    def camera(self, value):
        """
        Name:    Imaging.camera
        Feature: _camera attribute setter
        Inputs:  Camera, an gphoto2 camera object abstraction (value)
        Outputs: None
        """
        if self._permit_edits is True:
            if isinstance(value, Camera()):
                self._camera = value
            else:
                print('_camera attribute must be a Camera object.')
        else:
            print('Editing this field not permitted.')

    @property
    def led(self):
        """
        Name:    Imaging.led
        Feature: Return led data member
        Inputs:  None
        Outputs: LED, object representing a multi-colored LED that
                 inherits from the Adafruit_MotorHAT class (self._led)
        """
        return self._led

    @led.setter
    def led(self, value):
        """
        Name:    Imaging.led
        Feature: _led attribute setter
        Inputs:  LED, object representing a multi-colored LED that
                 inherits from the Adafruit_MotorHAT class (value)
        Outputs: None
        """
        if self._permit_edits is True:
            if isinstance(value, LED()):
                self._led = value
            else:
                print('_led attribute must be a LED object.')
        else:
            print('Editing this field not permitted.')

    @property
    def piezo(self):
        """
        Name:    Imaging.piezo
        Feature: Return piezo data member
        Inputs:  None
        Outputs: Piezo, object representing a piezo-buzzer that inherits
                 from the Adafruit_MotorHAT class (self._piezo)
        """
        return self._piezo

    @piezo.setter
    def piezo(self, value):
        """
        Name:    Imaging.piezo
        Feature: _piezo attribute setter
        Inputs:  Piezo, object representing a piezo-buzzer that
                 inherits from the Adafruit_MotorHAT class (value)
        Outputs: None
        """
        if self._permit_edits is True:
            if isinstance(value, Piezo()):
                self._piezo = value
            else:
                print('_piezo attribute must be a Piezo object.')
        else:
            print('Editing this field not permitted.')

    @property
    def is_running(self):
        """
        Name:    Imaging.is_running
        Feature: Return is_running data member
        Inputs:  None
        Outputs: Boolean, value representing system state, indicating
                 whether or not the system is currently in a imaging
                 sequence (self._is_running)
        """
        return self._is_running

    @property
    def is_error(self):
        """
        Name:    Imaging.is_error
        Feature: Return is_error data member
        Inputs:  None
        Outputs: Boolean, value representing system state, indicating
                 whether or not the hardware controls have encountered
                 an error (self._is_error)
        """
        return self._is_error

    @property
    def num_photos(self):
        """
        Name:    Imaging.num_photos
        Feature: Get the number of photos in a single expiriment.
        Inputs:  None
        Outputs: int, number of photos to be taken (self._num_photos)
        """
        return self._num_photos

    @property
    def taken(self):
        """
        Name:    Imaging.taken
        Feature: Get the number of photos in a single expiriment.
        Inputs:  None
        Outputs: int, number of photos that have been taken (self._taken)
        """
        return self._taken

    @is_running.setter
    def is_running(self, state):
        """
        Name:    Imaging.is_running
        Feature: Set is_running data member to boolean value.
        Inputs:  Boolean, desired system state (state)
        Outputs: None
        """
        if type(state) is bool and state is not self._is_running:
            self._is_running = state

    @is_error.setter
    def is_error(self, state):
        """
        Name:    Imaging.is_error
        Feature: Set is_error data member to boolean value.
        Inputs:  Boolean, desired system state (state)
        Outputs: None
        """
        if type(state) is bool and state is not self._is_error:
            self._is_error = state

    @num_photos.setter
    def num_photos(self, value):
        """
        Name:    Imaging.num_photos
        Feature: Set the number of photos in a single expiriment.
        Inputs:  int, number of photos to be taken (value)
        Outputs: None
        """
        if type(value) is int:
            if value in {1, 2, 4, 5, 8, 10, 20, 25, 40, 50, 100, 200}:
                self._num_photos = value
            else:
                print('Invalid number of photos.')
        else:
            print('Invalid type for number of photos.')

    @taken.setter
    def taken(self, value):
        """
        Name:    Imaging.taken
        Feature: Set the number of photos in a single expiriment.
        Inputs:  int, number of photos that have been taken (value)
        Outputs: None
        """
        if type(value) is int:
            if value >= 0:
                self._taken = value
            else:
                print('Number of photos taken may not be negative.')
        else:
            print('Number of photos taken must be an integer value.')

    @property
    def position(self):
        """
        Name:    Imaging.position
        Feature: Indicates the current position of the motor during the imaging
                 sequence.
        Inputs:  None
        Outputs: Float, current motor position in degrees
        """
        return self._position

    @position.setter
    def position(self, value):
        """
        Name:    Imaging.position
        Feature: Updates the current position of the motor during the imaging
                 sequence.
        Inputs:  Float, updated motor position value
        Outputs: None
        """
        if type(value) in {int, float}:
            if value >= 0:
                self._position = (value % 360)
            else:
                print('Motor position cannot be a negative number.')
        else:
            print('Motor position must be a number [degrees].')

    def reset_count(self):
        """
        Name:    Imaging.reset_count
        Feature: Resets the number of images taken counter
        Inputs:  None
        Outputs: None
        """
        self.taken = 0

    def reset_position(self):
        """
        Name:    Imaging.reset_position
        Feature: Resets the position indicator value
        Inputs:  None
        Outputs: None
        """
        self.position = 0

    def reset(self):
        """
        Name:    Imaging.reset
        Feature: Resets the imaging hardware
        Inputs:  None
        Outputs: None
        """
        self.reset_count()
        self.reset_position()
        self.motor.turn_off_motors()
        self.check_status()

    def run_sequence(self):
        """
        Name:    Imaging.run_sequence
        Feature: Tries aquiring an image and stepping the motor an IPA
                 step. If it fails, calls end sequence and indicates a
                 system error.
        Inputs:  None
        Outputs: string, filepath to image (filepath)
        """
        if self.is_error:
            try:
                self._check_error()
            except:
                traceback.print_exc()
                raise Exception(('Error not resolved! ' +
                                 'Please address and try again.'))
        if not self.is_running:
            self.motor.calibrate()
            self.is_running = True
        self.check_status()
        if self.taken >= self.num_photos:
            self.end_sequence()
        else:
            try:
                filepath = self.camera.capture(
                datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
                self.taken += 1
                for i in range((self.motor.step_res * self.motor.microsteps) /
                                self.num_photos):
                    self.motor.single_step()
                    self.position += (360.0 / (self.motor.step_res *
                                               self.motor.microsteps))
                if self.taken == (self.num_photos - 1):
                    self.piezo.beep()
                return filepath
            except:
                traceback.print_exc()
                self.is_error = True
                self.check_status()
                self.end_sequence()

    def end_sequence(self):
        """
        Name:    Imaging.end_sequence
        Feature: Tries stopping the system.
                 If successful, turn off the motors and indicate ready
                 status. If it fails, indicate error status.
        Inputs:  None
        Outputs: None
        """
        if self.is_running:
            self.is_running = False
        try:
            self.reset()
        except:
            traceback.print_exc()
            self.is_error = True
        finally:
            if not self.is_error:
                for i in range(3):
                    self.piezo.beep()
                    time.sleep(0.5)
            else:
                self.check_status()
                self.piezo.beep(3, 1, 1)
            self.check_status()

    def check_status(self):
        """
        Name:    Imaging.check_status
        Feature: Sets the LED status indicator to the appropriate color.
        Inputs:  None
        Outputs: None
        """
        if self.is_running:
            self.led.red_on()
        elif self.is_error:
            self.led.yellow_on()
        else:
            self.led.green_on()

    def _write_config(self, filepath=os.getcwd()):
        """
        Name:    Imaging._write_config
        Feature: Initializes a config file with current hardware settings.
                 PRIVATE FUNCTION DO NOT CALL
        Inputs:  String, desired absolute path to config file (filepath)
        Outputs: None
        """
        with open(os.path.join(filepath, 'prida.config'),
                  'w') as config_file:
            config = SafeConfigParser()
            attr_dict = {
                         'motor': [('rpm', str(self.motor.rpm)),
                                   ('degrees per unit step',
                                    str(self.motor.degrees)),
                                   ('microsteps per unit step',
                                    str(self.motor.microsteps)),
                                   ('port number',
                                    str(self.motor.motor_port_num))],
                         'camera': [('temporary image filepath',
                                     str(self.camera.path))],
                         'led': [('red pwm channel', str(self.led.red)),
                                 ('green pwm channel', str(self.led.green)),
                                 ('blue pwm channel', str(self.led.blue)),
                                 ('led type', str(self.led.led_type))],
                         'piezo': [('pwm channel', str(self.piezo.channel)),
                                   ('volume', str(self.piezo.volume))],
                         'imaging': []
            }
            for section in list(attr_dict.keys()):
                config.add_section(section)
                for attr, value in attr_dict[section]:
                    config.set(section, attr, value)
            config.write(config_file)

    def _parse_config_file(self, filepath=os.getcwd()):
        """
        Name:    Imaging._parse_config_file
        Feature: Sets attributes and member class attributes to values
                 as indicated by the PRIDA.CONFIG file. PRIVATE FUNCTION
                 DO NOT CALL
        Inputs:  String, absolute path to config file (filepath)
        Outputs: None
        """
        my_attrs = {'rpm': 'rpm',
                    'degrees per unit step': 'degrees',
                    'microsteps per unit step': 'microsteps',
                    'port number': 'motor_port_num',
                    'temporary image filepath': 'path',
                    'exposure time': 'exposure_time',
                    # 'manual focus drive': 'focus_drive',
                    'red pwm channel': 'red',
                    'green pwm channel': 'green',
                    'blue pwm channel': 'blue',
                    'led type': 'led_type',
                    'pwm channel': 'channel',
                    'volume': 'volume',
                    }
        self.motor.permit_edits = True
        self.permit_edits = True
        config = SafeConfigParser()
        if os.path.isfile(os.path.join(filepath, 'prida.config')) is True:
            config.read(os.path.join(filepath, 'prida.config'))
            for section in config.sections():
                if section in {'motor', 'camera', 'piezo', 'led', 'imaging'}:
                    options = config.options(section)
                    for option in options:
                        if option in list(my_attrs.keys()):
                            if hasattr(self, section):
                                obj = getattr(self, section)
                                if hasattr(obj, my_attrs[option]):
                                    setattr(obj, my_attrs[option],
                                            self._parse_attr_type(
                                                config.get(section, option)))
                                else:
                                    raise AttributeError('Failed to access' +
                                                         ' "imaging.' +
                                                         section +
                                                         '.' +
                                                         my_attrs[option] +
                                                         '"')
                            else:
                                raise AttributeError('Failed to access' +
                                                     ' "imaging.' + section +
                                                     '"')
        self.motor.permit_edits = False
        self.permit_edits = False

    def _parse_attr_type(self, atr):
        """
        Name:    Imaging._parse_attr_type
        Feature: Takes a string and attempts to cast it as its intended
                 type. PRIVATE FUNCTION DO NOT CALL
        Inputs:  String, a string representing an object attribute value.
        Outputs: An object of type float, int, bool, str or None; the
                 intended attribute value. Does not handle dicts, lists,
                 longs, complex values, exceptions or other built in or
                 custom types not previously specified. (new_atr)
        """
        new_atr = None
        try:
            try:
                if '.' in atr:
                    new_atr = float(atr)
                else:
                    new_atr = int(atr)
            except ValueError:
                if atr in {'True', 'False'}:
                    new_atr = bool(atr)
                else:
                    new_atr = str(atr)
        except:
            print('Could not determine intended type!')
            new_atr = atr
        return new_atr

    def _check_error(self):
        """
        Name:    Imaging._check_error
        Feature: Checks if an error is still present in the system
        Inputs:  None
        Outputs: None
        """
        if self.is_error is True:
            try:
                self.motor.calibrate()
                self.motor.turn_off_motors()
            except:
                raise Exception('Motor problem!')
            try:
                self.camera.connect()
                self.camera.capture('test')
            except:
                raise Exception('Camera problem!')
            else:
                self.is_error = False
                self.check_status()
