#!/usr/bin/python
#
# piezo.py
#
# VERSION: 0.5.8
#
# LAST EDIT: 2015-09-28
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# a08bbda: Tyler W. Davis - 2015-09-05 01:25:40
#     Bad import in led.py. Typo in piezo.py. Out of date prida.config.
# b00ff0f: Nathanael Shaw - 2015-09-21 14:49:24
#     Update changelogs
# 06ab4ef: Nathanael Shaw - 2015-09-09 14:24:45
#     Import styling modifications
# 6feaae1: Nathanael Shaw - 2015-09-08 15:40:08
#     minor changes
# d5347a2: Nathanael Shaw - 2015-09-08 12:17:09
#     Updated PEP8 styling. Attempted Python 2-3 compatability.
# 3bfa2ec: Nathanael Shaw - 2015-08-26 11:24:53
#     Added skeleton connect, poweroff and check_status behaviours to led, motor and piezo
# 105d4f8: Nathanael Shaw - 2015-08-25 15:42:02
#     Minor code cleanup, updated changelogs
# d359525: Nathanael Shaw - 2015-08-21 09:57:44
#     Fixed MRO error
# a944097: Nathanael Shaw - 2015-08-19 11:19:54
#     More minor changes
# e3bd04e: Nathanael Shaw - 2015-08-19 09:08:17
#     Minor updates to docstrings
# f70ec34: Nathanael Shaw - 2015-08-18 10:25:56
#     First draft of pridaperipheral.py
# b66b4f8: Nathanael Shaw - 2015-08-14 17:46:48
#     Better changelogs better life
# 83aab76: Nathanael Shaw - 2015-08-14 16:11:35
#     Added file specific commit logs to hardware classes.
# cba66c4: Nathanael Shaw - 2015-08-12 16:32:25
#     Fixed inefficent set construction in logical tests. Minor cleanup in imaging.py's _write_config
# 7e8d01d: Nathanael Shaw - 2015-08-06 11:18:49
#     hotfix
# 5e22fd2: Nathanael Shaw - 2015-08-06 11:18:07
#     Removed frequence control from piezo.py
# fb8f4e0: Nathanael Shaw - 2015-08-06 10:25:03
#     Addressed set construction and other syntax errors.
# 48c736a: Nathanael Shaw - 2015-07-31 14:24:54
#     Nathanael can do hotfixes too
# 1ad7a76: Nathanael Shaw - 2015-07-30 14:48:24
#     Fully pep8 compliant hardware class flies.
# 6c3f884: Nathanael Shaw - 2015-07-29 10:57:08
#     Converted motor.py to pythonic class with properties. Minor fixes to piezo.py.
# 7bdc7d6: Nathanael Shaw - 2015-07-29 10:27:44
#     Addressed minor syntax errors.
# b4c5c4c: Nathanael Shaw - 2015-07-28 15:08:07
#     More property testing
# 9682fc0: Nathanael Shaw - 2015-07-27 13:31:48
#     Converted piezo.py to new-style class.
# 836dcef: Nathanael Shaw - 2015-07-23 11:06:08
#     Added getters to piezo.py
# fe82fdc: Nathanael Shaw - 2015-07-15 19:09:26
#     Resolves #34
# 9398843: Hoi Cheng - 2015-07-15 18:19:24
#     Fixing docstrings. Added volume control to piezo.py
# 184a091: Nathanael Shaw - 2015-07-15 15:16:03
#     Initial attempt at new class organization.
# $log_end_tag$
#
#System Modules
import time
#3rd Party Modules
from Adafruit_MotorHAT import Adafruit_MotorHAT
#Original Modules
from pridaperipheral import PRIDAPeripheral, PRIDAPeripheralError

__license__ = "GPL-3.0"
__maintainer__ = "Nathanael Shaw"
__email__ = "nms95@cornell.edu"
__status__ = "Development"


class Piezo(Adafruit_MotorHAT, PRIDAPeripheral):
    def __init__(self):
            PRIDAPeripheral.__init__(self)
            Adafruit_MotorHAT.__init__(self)
            self._CHANNEL = 0
            self._VOLUME = 1.0

    @property
    def channel(self):
        """
        Name:    Piezo.channel
        Feature: Returns PWM channel controlling the piezo buzzer.
        Inputs:  None
        Outputs: Integer, channel number assigned to controlling the
                 piezo buzzer (self._CHANNEL)
        """
        return self._CHANNEL

    @property
    def volume(self):
        """
        Name:    Imaging.volume
        Feature: Returns float value representing percent of maximum
                 volume
        Inputs:  None
        Outputs: Float, value representing currently assigned piezo
                 buzzer volume (self._VOLUME)
        """
        return 100 * self._VOLUME

    @channel.setter
    def channel(self, ch):
        """
        Name:    Piezo.channel
        Feature: Set the PWM channel controlling the piezo buzzer.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._CHANNEL = ch

    @volume.setter
    def volume(self, volume):
        """
        Name:    Piezo.volume
        Feature: Set piezo volume by duty cycle.
        Inputs:  int, percent of maximum volume (volume)
        Outputs: None
        """
        if type(volume) in {int, float}:
            if volume < 0:
                volume = 0
            elif volume > 100:
                volume = 100
            self._VOLUME = volume / 100.0

    def beep(self, num_beeps=3, beep_time=0.05, rest_time=0.1):
        """
        Name:    Piezo.beep
        Feature: Temporarily raise the 'piezo' channel. Default is three
                 short beeps in quick succession.
        Inputs:  - int, desired number of beeps (num_beeps)
                 - float, length of each beep in seconds (beep_time)
                 - float, length of silence between beeps (rest_time)
        Outputs: None
        """
        for i in range(num_beeps):
            # turn buzzer on
            self._pwm.setPWM(self._CHANNEL, 0, int(2048 * self._VOLUME))
            # sustain note
            time.sleep(beep_time)
            # turn buzzer off
            self._pwm.setPWM(self._CHANNEL, 0, 4096)
            # rest
            time.sleep(rest_time)

    def connect(self):
        try:
            self.beep(2)
        except:
            raise PRIDAPeripheralError('Could not connect to piezo!')

    def poweroff(self):
        self._pwm.setPWM(self._CHANNEL, 0, 0)

    def current_status(self):
        super(self.current_status())
