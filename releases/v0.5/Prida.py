#!/usr/bin/python
#
# Prida.py
#
# VERSION: 0.5.8
#
# LAST EDIT: 2015-09-28
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# 91906a9: Nathanael Shaw - 2015-09-21 13:19:09
#     Load datafiles from sys.path[0]
# da52374: twdavis - 2015-09-17 18:33:32
#     if hardware_mode syntax and PEP8 updates
# 058b0e9: Tyler W. Davis - 2015-09-16 16:46:17
#     Updated imports
# 166518c: Hoi Cheng - 2015-09-12 13:44:10
#     search hotfix
# e974412: Hoi Cheng - 2015-09-09 23:46:35
#     v0.5.8
# 2ea176c: twdavis - 2015-09-09 13:36:20
#     Addresses #68. Prida.py and custom.py have been doc'd
# 2e7980f: Tyler W. Davis - 2015-09-08 15:28:31 +0000
#     Addresses #61 by introducing a 1/3 cv2.resize in Prida.show_ida() before casting to ImageQt
# b970caa: twdavis - 2015-09-02 13:23:21
#     Resolves #65
# 569d622: twdavis - 2015-09-02 13:14:13
#     Resolves #62
# dffe62e: Hoi Cheng - 2015-08-29 18:05:08 +0000
#     hotfix
# 1f47ef3: Hoi Cheng - 2015-08-29 14:02:38
#     v0.5.7
# 2ee9085: Hoi Cheng - 2015-08-28 19:13:21
#     prida/custom documentation
# a8b88f2: Hoi Cheng - 2015-08-17 23:09:48
#     ida improvements in progress
# 6c79ab1: Hoi Cheng - 2015-08-14 18:10:12
#     v0.5.6 round 2
# e66a930: Hoi Cheng - 2015-08-14 01:20:47
#     v0.5.6
# f560d99: Hoi Cheng - 2015-08-12 03:09:33
#     ida & search both broken but on the way
# a3831c7: Hoi Cheng - 2015-08-07 14:12:44
#     v0.5.5
# c815241: Hoi Cheng - 2015-08-07 13:20:40
#     data file for reference
# 291a55b: Hoi Cheng - 2015-08-07 03:17:04
#     ida underway
# 721e7f0: Hoi Cheng - 2015-08-06 13:25:56
#     v0.5.4
# e47966e: Hoi Cheng - 2015-08-05 01:10:40
#     v0.5.3
# acb45de: Hoi Cheng - 2015-07-31 13:21:20
#     v0.5.2
# 75da954: jay - 2015-07-30 20:27:29
#     v0.5.1
# d9ba542: jay - 2015-07-29 05:05:40
#     done prob
# b11d1c2: Hoi Cheng - 2015-07-28 14:19:19
#     seg fault?
# 4374cfc: Hoi Cheng - 2015-07-27 12:44:25
#     fix the headers
# 21db1f6: Hoi Cheng - 2015-07-27 12:08:10
#     refixed
# 0c19b8e: jay - 2015-07-26 21:33:42
#     Master Control Update
# fbcf2a3: Hoi Cheng - 2015-07-22 14:08:33
#     no more racing
# e0dd9a7: Hoi Cheng - 2015-07-22 13:06:50
#     this is hoi. everything is better
# 83f208b: jay - 2015-07-21 23:12:18
#     v0.4.1 -- i have tested nothing may not actually run
# e2a1c02: jay - 2015-07-21 22:59:01
#     i think it works
# a5e801b: jay - 2015-07-20 02:59:56
#     control logic in progress
# 982cf7e: jay - 2015-07-20 00:41:27
#     logical
# 93ebeda: Hoi Cheng - 2015-07-16 12:53:40
#     buttons man
# 07e4aeb: jay - 2015-07-16 01:56:21
#     exciting things
# 62b0edf: jay - 2015-07-16 01:37:43
#     in progress
# 435ce9f: Hoi Cheng - 2015-07-02 12:55:48
#     bugfix
# fcd5583: Hoi Cheng - 2015-07-02 12:46:08
#     v0.3.12
# 01523bb: jay - 2015-07-02 00:43:19
#     v0.3.11
# dd24989: Nathanael Shaw - 2015-06-30 14:09:27
#     bugfixes
# 76030fd: jay - 2015-06-29 23:38:19
#     v0.3.9
# d39f320: Nathanael Shaw - 2015-06-29 13:21:36
#     working on threads
# 6ce3340: jay - 2015-06-29 01:52:24
#     v0.3.7
# e6f4e6b: jay - 2015-06-28 19:42:42
#     v0.3.6
# db2eee7: jay - 2015-06-27 00:18:29
#     directory cleanup part 1
# 1e012bb: jay - 2015-06-22 01:19:16
#     v0.2.1
# 6a90843: Jay - 2015-06-18 13:58:07
#     bugfixes
# f04bdb8: Hoi Cheng - 2015-06-16 23:26:18
#     v0.1.2
# 48d047e: Jay - 2015-06-16 14:10:50
#     v0.1.1
# cfe98d3: Hoi Cheng - 2015-06-16 01:57:14
#     working on docstrings...
# db5bcc3: Hoi Cheng - 2015-06-16 01:46:15
#     v0.0.4
# 50cf878: Jay - 2015-06-15 13:48:22
#     v0.0.4
# f5c47bc: Hoi Cheng - 2015-06-15 00:36:05
#     v0.0.3
# ea22481: jay - 2015-06-14 02:33:49
#     v0.0.2
# 53291ad: jay - 2015-06-11 23:51:59
#     PILLOW
# 2b9a07c: Jay - 2015-06-11 15:12:30
#     Working on GUI
# 0ef64ad: Jay - 2015-06-11 11:33:26
#     fixing loops
# bc9d973: Jay - 2015-06-09 13:49:19
#     converting to class based
# c7098b4: Jay - 2015-06-08 13:59:39
#     GUI
# 901ff01: Jay - 2015-05-30 10:24:35
#     Pre-app
# b19f48e: Jay - 2015-05-30 06:15:45
#     Checking GUI
# ad84bab: Jay - 2015-05-30 05:32:29
#     GUI setup
# 629d58c: Jay - 2015-05-30 02:08:46
#     Setting up GUI
# $log_end_tag$
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
from __future__ import print_function
import sys
from os.path import join
import PyQt5.uic as uic
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QAbstractItemView
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QThread
from PyQt5.QtCore import QSize
import PIL.ImageQt as ImageQt
import PIL.Image as Image

from hdf_organizer import PridaHDF

try:
    from imaging import Imaging
    hardware_mode = True
except ImportError:
    # MODE 1 INDICATES ERROR LIKELY FROM NONEXISTENCE OF HARDWARE
    print('Imaging hardware not set up')
    hardware_mode = False

###############################################################################
## GLOBAL VARIABLES:
###############################################################################
# PRIDA VERSION
PRIDA_VERSION = 'Prida 0.5.8'
PRIDA_VERSION_EXPL = PRIDA_VERSION + ' - Explorer Mode'

#PAGE NUMBER ENUMS FOR mainwindow.ui
MAIN_MENU = 0
VIEWER = 1
INPUT_EXP = 2
RUN_EXP = 3

#PAGE NUMBER ENUMS FOR SHEET/IDA
SHEET_VIEW = 0
IDA_VIEW = 1

# FILE & SESSION PATHS FOR IMAGES AND HDF FILE
filepath = ''
session_path = ''

###############################################################################
## CLASSES:
###############################################################################
class HardwareThread(QThread):
    """
    Name:     HardwareThread
    Features: Thread for running hardware
    """

    def run(self):
        """
        Name:     HardwareThread.run
        Inputs:   None.
        Outputs:  None.
        Features: Run sequence and save image to HDF file
        """
        global filepath
        if hardware_mode:
            filepath = imaging.run_sequence()
            hdf.save_image(session_path, filepath)


class Prida(QApplication):
    """
    Name:      Prida
    Features:  Prida main class
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     Prida.__init__
        Inputs:   None.
        Outputs:  None.
        Depends:  - load_session_sheet_headers     - new_hdf
                  - new_session                    - open_hdf
                  - resize_sheet                   - show_ida
                  - to_sheet                       - update_progress
                  - update_session_sheet_model     - update_sheet_model
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # NOTE: mainwindow.ui depends on InteractiveLabel and IDA classes
        #       defined in custom.py
        self.view = uic.loadUi(join(sys.path[0], 'mainwindow.ui'), self.base)

        if hardware_mode:
            self.hardware_thread = HardwareThread()
            self.hardware_thread.finished.connect(self.update_progress)

        self.view.welcome.setPixmap(QPixmap(join(sys.path[0],'berries.jpg')).scaledToHeight(300))
        self.sheet_model = QStandardItemModel()
        self.view.sheet.setModel(self.sheet_model)

        self.session_sheet_model = QStandardItemModel()
        self.view.session_sheet.setModel(self.session_sheet_model)
        self.load_session_sheet_headers()

        self.view.sheet.collapsed.connect(lambda: self.resize_sheet())
        self.view.sheet.expanded.connect(lambda: self.resize_sheet())
        self.view.sheet.setAnimated(True)
        self.view.c_buttons.accepted.connect(self.new_session)
        self.view.c_buttons.rejected.connect(
            lambda: self.view.stackedWidget.setCurrentIndex(VIEWER)
        )

        for line in open(join(sys.path[0], 'dictionary.txt')).read().splitlines():
            self.view.c_gen_sp.addItem(line)

        self.sheet_select_model = self.view.sheet.selectionModel()
        self.sheet_select_model.selectionChanged.connect(
            self.update_session_sheet_model
        )

        self.view.new_hdf.clicked.connect(self.new_hdf)
        self.view.open_hdf.clicked.connect(self.open_hdf)
        self.view.create_session.clicked.connect(
            lambda: self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
        )

        self.view.pushButton.clicked.connect(
            lambda: self.view.stackedWidget.setCurrentIndex(MAIN_MENU)
        )

        self.view.search.textEdited.connect(self.update_sheet_model)

        self.img_list = []
        self.img_sheet_model = QStandardItemModel()
        self.view.img_select.setModel(self.img_sheet_model)
        self.view.img_select.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.img_select.setIconSize(QSize(100, 100))

        self.current_img = None

        self.view.to_sheet.clicked.connect(self.to_sheet)

        #self.view.ida.setScaledContents(True)

        self.img_sheet_select_model = self.view.img_select.selectionModel()
        self.img_sheet_select_model.selectionChanged.connect(self.show_ida)

        self.view.sheet.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.session_sheet.setRootIsDecorated(False)

        self.data_list = []

        if hardware_mode:
            self.base.setWindowTitle(PRIDA_VERSION)
        else:
            self.base.setWindowTitle(PRIDA_VERSION_EXPL)
        #self.base.showMaximized()
        self.base.show()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def get_data(self):
        """
        Name:     Prida.get_data
        Inputs:   None.
        Outputs:  None.
        Features: Gets all PID metadata from HDF5 file
        """
        self.data_list = []
        for pid in hdf.list_pids():
            d = hdf.get_pid_attrs(pid)
            d['pid'] = pid
            self.data_list.append(d)

    def new_session(self):
        """
        Name:     Prida.new_session
        Inputs:   None.
        Outputs:  None
        Features: Creates HDF5 session, resets the progress bar, enters the
                  RUN_EXP screen, and begins a session
        """
        global session_path
        if hardware_mode:
            session_path = hdf.create_session(
                str(self.view.c_pid.text()),
                {'gen_sp': str(self.view.c_gen_sp.currentText()),
                 'line': str(self.view.c_line.text()),
                 'media': str(self.view.c_media.text()),
                 'germdate': str(self.view.c_germdate.text()),
                 'transdate': str(self.view.c_transdate.text()),
                 'rep_num': str(self.view.c_rep_num.text()),
                 'treatment': str(self.view.c_treatment.text()),
                 'tubsize': str(self.view.c_tubsize.text()),
                 'tubid': str(self.view.c_tubid.text()),
                 'nutrient': str(self.view.c_nutrient.text()),
                 'growth_temp_day': str(self.view.c_growth_temp_day.text()),
                 'growth_temp_night': str(self.view.c_growth_temp_night.text()),
                 'growth_light': str(self.view.c_growth_light.text()),
                 'water_sched': str(self.view.c_water_sched.text()),
                 'notes': str(self.view.c_plant_notes.text())},
                {'user': str(self.view.c_session_user.text()),
                 'addr': str(self.view.c_session_addr.text()),
                 'date': str(self.view.c_session_date.text()),
                 'number': str(self.view.c_session_number.text()),
                 'num_img': str(self.view.c_num_images.text()),
                 'age_num': str(self.view.c_plant_age.text()),
                 'notes': str(self.view.c_session_notes.text()),
                 'cam_shutter': str(self.view.c_cam_shutter.text()),
                 'cam_aperture': str(self.view.c_cam_aperture.text()),
                 'cam_exposure': str(self.view.c_cam_exposure.text())})
            imaging.num_photos = int(self.view.c_num_images.text())
            self.view.c_progress.setMinimum(0)
            self.view.c_progress.setMaximum(int(imaging.num_photos))
            self.view.stackedWidget.setCurrentIndex(RUN_EXP)
            self.hardware_thread.start(QThread.TimeCriticalPriority)

    def resize_session_sheet(self):
        """
        Name:     Prida.resize_session_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes the session sheet to fit all content
        """
        for col in xrange(2):
            self.view.session_sheet.resizeColumnToContents(col)

    def resize_sheet(self):
        """
        Name:     Prida.resize_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes the spreadsheet to fit all content
        """
        for col in xrange(16):
            self.view.sheet.resizeColumnToContents(col)

    def search(self, pid_meta, terms):
        """
        Name:     Prida.search
        Inputs:   - (pid_meta)
                  - (terms)
        Outputs:  bool (found)
        Features: Searches the pid metafields and returns true if all terms are
                  found anywhere within the fields
        """
        found = True
        for term in terms:
            found = found and any(term in meta_field for meta_field in pid_meta)
        return found

    def show_ida(self, selection):
        """
        Name:     Prida.show_ida
        Input:    obj (selection)
        Output:   None.
        Features: Switches to IDA page and sets IDA image
        """
        print(selection.indexes())
        if selection.indexes():
            self.view.stacked_sheets.setCurrentIndex(IDA_VIEW)
            img_path = self.img_sheet_model.itemFromIndex(selection.indexes()[0]).data()
            ndarray_full = hdf.get_dataset(img_path)
            self.view.ida.setBaseImage(ndarray_full)

    def to_sheet(self):
        """
        Name:     Prida.to_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Switches from IDA to sheet view
        """
        self.view.img_select.clearSelection()
        self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)

    def load_session_sheet_headers(self):
        """
        Name:     Prida.load_session_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Loads session sheet headers
        Depends:  resize_session_sheet
        """
        self.session_sheet_model.setColumnCount(2)
        self.session_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('Property')
        )
        self.session_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Value')
        )
        self.session_sheet_model.setItem(0, 0, QStandardItem('Session Name'))
        self.session_sheet_model.setItem(1, 0, QStandardItem('Plant Age'))
        self.session_sheet_model.setItem(2, 0, QStandardItem('Session Date'))
        self.session_sheet_model.setItem(3, 0, QStandardItem('Shutter Speed'))
        self.session_sheet_model.setItem(4, 0, QStandardItem('Aperture'))
        self.session_sheet_model.setItem(5, 0, QStandardItem('Exposure'))
        self.session_sheet_model.setItem(6, 0, QStandardItem('Num Images'))
        self.session_sheet_model.setItem(7, 0, QStandardItem('Session Notes'))
        self.resize_session_sheet()

    def load_sheet_headers(self):
        """
        Name:     Prida.load_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Loads sheet headers
        Depends:  resize_sheet
        """
        self.sheet_model.setColumnCount(16)
        self.sheet_model.setHorizontalHeaderItem(0, QStandardItem('PID'))
        self.sheet_model.setHorizontalHeaderItem(1, QStandardItem('Genus Species'))
        self.sheet_model.setHorizontalHeaderItem(2, QStandardItem('Line'))
        self.sheet_model.setHorizontalHeaderItem(3, QStandardItem('Media'))
        self.sheet_model.setHorizontalHeaderItem(4, QStandardItem('Germination Date'))
        self.sheet_model.setHorizontalHeaderItem(5, QStandardItem('Transplant Date'))
        self.sheet_model.setHorizontalHeaderItem(6, QStandardItem('Rep Num'))
        self.sheet_model.setHorizontalHeaderItem(7, QStandardItem('Treatment'))
        self.sheet_model.setHorizontalHeaderItem(8, QStandardItem('Tub Size'))
        self.sheet_model.setHorizontalHeaderItem(9, QStandardItem('Tub ID'))
        self.sheet_model.setHorizontalHeaderItem(10, QStandardItem('Nutrient'))
        self.sheet_model.setHorizontalHeaderItem(11, QStandardItem('Growth Temp (Day)'))
        self.sheet_model.setHorizontalHeaderItem(12, QStandardItem('Growth Temp (Night)'))
        self.sheet_model.setHorizontalHeaderItem(13, QStandardItem('Lighting Conditions'))
        self.sheet_model.setHorizontalHeaderItem(14, QStandardItem('Watering schedule'))
        self.sheet_model.setHorizontalHeaderItem(15, QStandardItem('Plant Notes'))
        self.resize_sheet()

    def update_progress(self):
        """
        Name:     Prida.update_progress
        Inputs:   None.
        Outputs:  None.
        Features: Updates the progress bar, sets captured image to preview, and
                  decides the completion status of the session
        Depends:  - clear_session_sheet_model
                  - get_data
                  - update_sheet_model
        """
        self.view.c_progress.setValue(imaging.taken)
        image = QPixmap(filepath)
        self.view.c_preview.setPixmap(image.scaledToHeight(300))
        if not imaging.is_running:
            imaging.reset_count()
            self.get_data()
            self.update_sheet_model()
            self.clear_session_sheet_model()
            self.view.stackedWidget.setCurrentIndex(VIEWER)
        else:
            self.hardware_thread.start(QThread.TimeCriticalPriority)

    def clear_session_sheet_model(self):
        """
        Name:     Prida.clear_session_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Clears session sheet model
        Depends:  resize_session_sheet
        """
        self.session_sheet_model.setItem(0, 1, QStandardItem(''))
        self.session_sheet_model.setItem(1, 1, QStandardItem(''))
        self.session_sheet_model.setItem(2, 1, QStandardItem(''))
        self.session_sheet_model.setItem(3, 1, QStandardItem(''))
        self.session_sheet_model.setItem(4, 1, QStandardItem(''))
        self.session_sheet_model.setItem(5, 1, QStandardItem(''))
        self.session_sheet_model.setItem(6, 1, QStandardItem(''))
        self.session_sheet_model.setItem(7, 1, QStandardItem(''))
        self.resize_session_sheet()

    def update_session_sheet_model(self, selection):
        """
        Name:     Prida.update_session_sheet_model
        Inputs:   obj (selection)
        Outputs:  None.
        Features: Updates session sheet model with selected session's data,
                  creates the thumbnails and tags the full image paths, and
                  clears session sheet model if no sessions are selected
        Depends:  - clear_session_sheet_model
                  - resize_session_sheet
        """
        if (self.sheet_model.itemFromIndex(selection.indexes()[0]).parent() is not None):
            pid = self.sheet_model.itemFromIndex(selection.indexes()[0]).parent().text()
            session = self.sheet_model.itemFromIndex(selection.indexes()[0]).text()
            s_path = "/%s/%s" % (pid, session)
            data = hdf.get_session_attrs(s_path)
            self.session_sheet_model.setItem(0, 1, QStandardItem(session))
            self.session_sheet_model.setItem(1, 1, QStandardItem(data['age_num']))
            self.session_sheet_model.setItem(2, 1, QStandardItem(data['date']))
            self.session_sheet_model.setItem(3, 1, QStandardItem(data['cam_shutter']))
            self.session_sheet_model.setItem(4, 1, QStandardItem(data['cam_aperture']))
            self.session_sheet_model.setItem(5, 1, QStandardItem(data['cam_exposure']))
            self.session_sheet_model.setItem(6, 1, QStandardItem(data['num_img']))
            self.session_sheet_model.setItem(7, 1, QStandardItem(data['notes']))

            self.img_sheet_model.clear()
            self.img_list = []
            for obj in hdf.list_objects(s_path):
                ndarray = hdf.get_dataset(s_path + '/' + obj + '/' + obj + '.thumb')
                image = Image.fromarray(ndarray)
                qt_image = ImageQt.ImageQt(image)
                self.img_list.append(qt_image)
                pix = QPixmap.fromImage(qt_image)
                item = QStandardItem(obj)
                item.setIcon(QIcon(pix))
                item.setData(s_path + '/' + obj + '/' + obj + '.jpg')
                self.img_sheet_model.appendRow(item)
        else:
            self.clear_session_sheet_model()
            self.img_sheet_model.clear()
            self.img_list = []
        self.resize_session_sheet()

    def update_sheet_model(self, search=''):
        """
        Name:     Prida.update_sheet_model
        Inputs:   [optional] str (search)
        Features: Displays all PIDs in the sheet or the PIDs that match the
                  search terms if search terms are given
        Depends:  - load_sheet_headers
                  - resize_sheet
                  - search
        """
        self.sheet_model.clear()
        self.load_sheet_headers()
        terms = search.lower().split()
        for data in self.data_list:
            pid_meta = []
            for key in data:
                pid_meta.append(data[key].lower())
            if self.search(pid_meta, terms):
                pid_item = QStandardItem(data['pid'])
                for session in hdf.list_sessions(data['pid']):
                    pid_item.appendRow(QStandardItem(session))
                self.sheet_model.appendRow(
                    [pid_item,
                     QStandardItem(data['gen_sp']),
                     QStandardItem(data['line']),
                     QStandardItem(data['media']),
                     QStandardItem(data['germdate']),
                     QStandardItem(data['transdate']),
                     QStandardItem(data['rep_num']),
                     QStandardItem(data['treatment']),
                     QStandardItem(data['tubsize']),
                     QStandardItem(data['tubid']),
                     QStandardItem(data['nutrient']),
                     QStandardItem(data['growth_temp_day']),
                     QStandardItem(data['growth_temp_night']),
                     QStandardItem(data['growth_light']),
                     QStandardItem(data['water_sched']),
                     QStandardItem(data['notes'])]
                )
        self.resize_sheet()

    def new_hdf(self):
        """
        Name:     Prida.new_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Gets user information, gets HDF5 file name, creates a new
                  HDF5 file, stores user information, and goes to VIEWER page
        Depends:  - get_data
                  - update_sheet_model
        """
        popup = uic.loadUi(join(sys.path[0],'input_user.ui'))
        if popup.exec_():
            path = QFileDialog.getSaveFileName()[0]
            if path != '':
                if path[-5:] != '.hdf5':
                    path += '.hdf5'
                hdf.open_file(path)
                hdf.set_root_user(str(popup.user.text()))
                hdf.set_root_addr(str(popup.email.text()))
                self.get_data()
                self.update_sheet_model()
                self.view.stackedWidget.setCurrentIndex(VIEWER)

    def open_hdf(self):
        """
        Name:     Prida.open_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Opens an existing HDF5 file and goes to VIEWER page
        Depends:  - get_data
                  - update_sheet_model
        """
        path = QFileDialog.getOpenFileName(filter='*.hdf5')[0]
        if path != '':
            hdf.open_file(path)
            self.get_data()
            self.update_sheet_model()
            self.view.stackedWidget.setCurrentIndex(VIEWER)

    def update_user(self):
        """
        Name:     Prida.update_user
        Inputs:   None.
        Outputs:  None.
        Features: Updates the GUI with user information
        NOTE:     FUNCTION NOT USED
        """
        self.view.user.setText(self.hdf.get_root_user())
        self.view.email.setText(self.hdf.get_root_addr())

###############################################################################
## MAIN:
###############################################################################
if __name__ == '__main__':
    hdf = PridaHDF()
    if hardware_mode:
        imaging = Imaging()
    app = Prida()
    app.exec_()
    if hardware_mode:
        app.hardware_thread.wait()
    hdf.close()
    sys.exit()
