#!/usr/bin/python
#
# custom.py
#
# VERSION: 0.5.8
#
# LAST EDIT: 2015-09-28
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# 91906a9: Nathanael Shaw - 2015-09-21 13:19:09
#     Load datafiles from sys.path[0]
# e974412: Hoi Cheng - 2015-09-09 23:46:35
#     v0.5.8
# 2ea176c: twdavis - 2015-09-09 13:36:20
#     Addresses #68. Prida.py and custom.py have been doc'd
# 1f47ef3: Hoi Cheng - 2015-08-29 14:02:38
#     v0.5.7
# 2ee9085: Hoi Cheng - 2015-08-28 19:13:21
#     prida/custom documentation
# a8b88f2: Hoi Cheng - 2015-08-17 23:09:48
#     ida improvements in progress
# 721e7f0: Hoi Cheng - 2015-08-06 13:25:56
#     v0.5.4
# e47966e: Hoi Cheng - 2015-08-05 01:10:40
#     v0.5.3
# $log_end_tag$
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
from os.path import join
import sys
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import (QPainter,
                         QPixmap,
                         QColor)
import cv2
from PIL import Image, ImageQt

###############################################################################
## CLASSES:
###############################################################################
class InteractiveLabel(QLabel):
    """
    Name:     InteractiveLabel
    Features: Click and drag and selection widget
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, stuff):
        """
        Name:     InteractiveLabel.__init__
        Inputs:   (stuff)
        Features: Initializes an InteractiveLabel.
                  * press_pos is the stored mouse press location
                  * clean_img is last image before mouse press
                  * base_img is the original image
                  * active_img is the image currently being used
                  * is_pressed indicates mouse pressed status
        """
        QLabel.__init__(self)
        self.press_pos = None
        self.clean_img = QPixmap(join(sys.path[0], 'berries.jpg')).scaledToHeight(300)
        self.base_img = QPixmap(join(sys.path[0], 'berries.jpg')).scaledToHeight(300)
        self.active_img = None
        self.is_pressed = False
        self.painter = QPainter()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def mousePressEvent(self, event):
        """
        Name:     InteractiveLabel.mousePressEvent
        Input:    (event)
        Output:   None.
        Features: On left mouse click, the mouse location is saved and the
                  pressed status is set to true
        """
        if event.button() == Qt.LeftButton:
            self.press_pos = event.pos()
            self.is_pressed = True

    def mouseMoveEvent(self, event):
        """
        Name:     InteractiveLabel.mouseMoveEvent
        Input:    (event)
        Output:   None.
        Features: While mouse click is held down, reset the active image as the
                  clean image, then draw the new image and set the active image
                  to the label
        """
        if self.is_pressed:
            self.active_img = self.clean_img.copy()
            self.painter.begin(self.active_img)
            #self.painter.setPen(Qt.blue)
            self.painter.fillRect(self.press_pos.x(),
                                  self.press_pos.y(),
                                  event.x()-self.press_pos.x(),
                                  event.y()-self.press_pos.y(),
                                  QColor(128, 128, 255, 128))
            self.painter.end()
            self.setPixmap(self.active_img)

    def mouseReleaseEvent(self, event):
        """
        Name:     InteractiveLabel.mouseReleaseEvent
        Features: When left mouse click is released, set the active image as
                  the new clean image. When right mouse click is released, set
                  the shown and clean image to the base image.
        """
        if event.button() == Qt.LeftButton:
            print("Start: " + str(self.press_pos))
            print("End: " + str(event.pos()))
            if self.is_pressed:
                if (event.x() > self.pixmap().size().width() or
                        event.y() > self.pixmap().size().height() or
                        event.x() < 0 or event.y() < 0):
                    self.setPixmap(self.clean_img)
                else:
                    self.clean_img = self.active_img
                self.is_pressed = False
        else:
            self.setPixmap(self.base_img)
            self.clean_img = self.base_img

class IDA(QLabel):
    """
    Name:     IDA (Image Display Area)
    Features: The label that displays the photos in full size
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, arg):
        """
        Name:     IDA.__init__
        Inputs:   (arg)
        Outputs:  None.
        Features: Initializes IDA
        """
        QLabel.__init__(self)
        self.base_img = None
        self.cur_image = None

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def setBaseImage(self, ndarray_full):
        """
        Name:     IDA.setBaseImage
        Inputs:   Image (img)
        Outputs:  None.
        Features: Sets and saves base image for viewing
        """
        self.base_img = ndarray_full
        self.resizeAndSet()

    def resizeAndSet(self):
        """
        Name:     IDA.resizeAndSet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes and sets image
        """
        height, width, _ = self.base_img.shape
        ratio = float(width) / float(height)
        width = self.width()
        height = float(width) / ratio
        if height > self.height():
            height = self.height()
            width = height * ratio
        ndarray = cv2.resize(self.base_img, (int(width), int(height)))
        image = Image.fromarray(ndarray)
        qt_image = ImageQt.ImageQt(image)
        self.cur_image = qt_image
        pix = QPixmap.fromImage(self.cur_image)
        self.setPixmap(pix)

    def resizeEvent(self, e):
        """
        Name:     IDA.resizeEvent
        Inputs:   (e)
        Outputs:  None.
        Features: Upon resize events, resize and set image
        """
        if self.base_img is not None:
            self.resizeAndSet()
