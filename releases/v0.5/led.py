#!/usr/bin/python
#
# led.py
#
# VERSION: 0.5.8
#
# LAST EDIT: 2015-09-28
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# a08bbda: Tyler W. Davis - 2015-09-05 01:25:40
#     Bad import in led.py. Typo in piezo.py. Out of date prida.config.
# b00ff0f: Nathanael Shaw - 2015-09-21 14:49:24
#     Update changelogs
# 06ab4ef: Nathanael Shaw - 2015-09-09 14:24:45
#     Import styling modifications
# 6feaae1: Nathanael Shaw - 2015-09-08 15:40:08
#     minor changes
# d5347a2: Nathanael Shaw - 2015-09-08 12:17:09
#     Updated PEP8 styling. Attempted Python 2-3 compatability.
# 3bfa2ec: Nathanael Shaw - 2015-08-26 11:24:53
#     Added skeleton connect, poweroff and check_status behaviours to led, motor and piezo
# 105d4f8: Nathanael Shaw - 2015-08-25 15:42:02
#     Minor code cleanup, updated changelogs
# d359525: Nathanael Shaw - 2015-08-21 09:57:44
#     Fixed MRO error
# a944097: Nathanael Shaw - 2015-08-19 11:19:54
#     More minor changes
# e3bd04e: Nathanael Shaw - 2015-08-19 09:08:17
#     Minor updates to docstrings
# f70ec34: Nathanael Shaw - 2015-08-18 10:25:56
#     First draft of pridaperipheral.py
# b66b4f8: Nathanael Shaw - 2015-08-14 17:46:48
#     Better changelogs better life
# 83aab76: Nathanael Shaw - 2015-08-14 16:11:35
#     Added file specific commit logs to hardware classes.
# eadb171: Nathanael Shaw - 2015-08-14 09:55:16
#     Fixed error where imaging was writing config files willy-nilly.
# cba66c4: Nathanael Shaw - 2015-08-12 16:32:25
#     Fixed inefficent set construction in logical tests. Minor cleanup in imaging.py's _write_config
# c0d749b: Nathanael Shaw - 2015-08-06 13:25:58
#     Addressed bad inline comment in init
# fb8f4e0: Nathanael Shaw - 2015-08-06 10:25:03
#     Addressed set construction and other syntax errors.
# 371ebff: Nathanael Shaw - 2015-07-30 12:28:20
#     Default LED type returned to common anode.
# 7bdc7d6: Nathanael Shaw - 2015-07-29 10:27:44
#     Addressed minor syntax errors.
# b4c5c4c: Nathanael Shaw - 2015-07-28 15:08:07
#     More property testing
# 2865ed8: Nathanael Shaw - 2015-07-27 13:30:54
#     Converted led.py to new-style class.
# 79283b9: Nathanael Shaw - 2015-07-23 14:16:09
#     More graceful exit testing
# c7cce71: Nathanael Shaw - 2015-07-23 11:05:31
#     Added getters to led.py
# 588958f: Nathanael Shaw - 2015-07-22 15:17:55
#     Added atexit functionality to motor.py and led.py. Shifted imaging.py from inheriting motor/led/piezo/camera to having attributes of those objects.
# a876b18: Hoi Cheng - 2015-07-22 11:24:43
#     Set default LED type to common cathode.
# ac05231: Nathanael Shaw - 2015-07-20 13:01:36
#     Fixed led.py to use boolean equals instead of assignment.
# b6b407d: Nathanael Shaw - 2015-07-16 17:11:38
#     Adapted led.py to accomidate common cathode LEDs
# fe82fdc: Nathanael Shaw - 2015-07-15 19:09:26
#     Resolves #34
# 9398843: Hoi Cheng - 2015-07-15 18:19:24
#     Fixing docstrings. Added volume control to piezo.py
# 184a091: Nathanael Shaw - 2015-07-15 15:16:03
#     Initial attempt at new class organization.
# $log_end_tag$
#
#System Modules
from time import sleep
#3rd Party Modules
from Adafruit_MotorHAT import Adafruit_MotorHAT
#Original Modules
from pridaperipheral import PRIDAPeripheral, PRIDAPeripheralError

__license__ = "GPL-3.0"
__maintainer__ = "Nathanael Shaw"
__email__ = "nms95@cornell.edu"
__status__ = "Development"


class LED(Adafruit_MotorHAT, PRIDAPeripheral):
    """
    Class representing a tri-color (RGB) LED
    """

    def __init__(self):
        """
        Initialize PWM channels. Extends MotorHAT attributes and behaviours.
        """
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self._BLUE = 1         # Default channel for the blue LED
        self._GREEN = 14       # Default channel for the green LED
        self._RED = 15         # Default channel for the red LED
        self._led_type = 'CC'  # Default type Common Anode

    @property
    def blue(self):
        """
        Name:    LED.blue
        Feature: Returns the integer designation of the blue LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the blue LED (self._BLUE)
        """
        return self._BLUE

    @property
    def green(self):
        """
        Name:    LED.green
        Feature: Returns the integer designation of the green LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the green LED (self._GREEN)
        """
        return self._GREEN

    @property
    def red(self):
        """
        Name:    LED.red
        Feature: Returns the integer designation of the red LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the red LED (self._RED)
        """
        return self._RED

    @property
    def led_type(self):
        """
        Name:    LED.led_type
        Feature: Returns string representing multicolor LED
                 configuration
        Inputs:  None
        Outputs: String, two letter designation representing either
                 'Common Cathode' or 'Common Anode' (self._led_type)
        """
        return self._led_type

    @led_type.setter
    def led_type(self, led_type):
        """
        Name:    LED.led_type
        Feature: Set type to common anode or common cathode.
        Inputs:  String, two letter type designation (led_type)
        Outputs: None
        """
        if led_type == 'CA' or led_type == 'CC':
            self._led_type = led_type
        else:
            print("Bad input. Types limited to CC or CA.")

    @blue.setter
    def blue(self, ch):
        """
        Name:    LED.blue
        Feature: Set the PWM channel controlling the blue led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._BLUE = ch

    @green.setter
    def green(self, ch):
        """
        Name:    LED.green
        Feature: Set the PWM channel controlling the green led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._GREEN = ch

    @red.setter
    def red(self, ch):
        """
        Name:    LED.red
        Feature: Set the PWM channel controlling the red led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if type(ch) is int:
            if ch in {0, 1, 14, 15}:
                self._RED = ch

    def led_off(self, channel):
        """
        Name:    LED.led_off
        Feature: Set low a specifed led channel.
        Inputs:  int, the number of the channel to lower (channel)
        Outputs: None
        """
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 4096, 0)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 0, 4096)

    def led_on(self, channel):
        """
        Name:    LED.led_on
        Feature: Set high a specified led channel.
        Inputs:  int, the number of the channel to raise (channel)
        Outputs: None
        """
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 0, 4096)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 4096, 0)

    def red_on(self):
        """
        Name:    LED.red_on
        Feature: Set high the 'RED' PWM channel, default 15.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self._RED)
        self.led_off(self._BLUE)
        self.led_off(self._GREEN)

    def blue_on(self):
        """
        Name:    LED.blue_on
        Feature: Set high the 'BLUE' PWM channel, default 1.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self._BLUE)
        self.led_off(self._RED)
        self.led_off(self._GREEN)

    def green_on(self):
        """
        Name:    LED.green_on
        Feature: Set high the 'GREEN' PWM channel, default 14.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self._GREEN)
        self.led_off(self._RED)
        self.led_off(self._BLUE)

    def yellow_on(self):
        """
        Name:    LED.yellow_on
        Feature: Set led color to yellow by combining green and red
                 light using default channels.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self._RED)
        self.led_on(self._GREEN)
        self.led_off(self._BLUE)

    def red_off(self):
        """
        Name:    LED.red_off
        Feature: Set low the 'red' PWM channel, default 15.
        Inputs:  None
        Outputs: None
        """
        self.led_off(self._RED)

    def blue_off(self):
        """
        Name:    LED.blue_off
        Feature: Set low the 'blue' PWM channel, default 1.
        Inputs:  None
        Outputs: None
        """
        self.led_off(self._BLUE)

    def green_off(self):
        """
        Name:    LED.green_off
        Feature: Set low the 'green' PWM channel, default 14.
        Inputs:  None
        Outputs: None
        """
        self.led_off(self._GREEN)

    def yellow_off(self):
        """
        Name:    LED.yellow_off
        Feature: Set low the red and green PWM channels, removing yellow
                 light.
        Inputs:  None
        Outputs: None
        """
        self.red_off()
        self.green_off()

    def all_off(self):
        """
        Name:    LED.all_off
        Feature: Set all led PWM channels to low.
        Inputs:  None
        Outputs: None
        """
        self.blue_off()
        self.green_off()
        self.red_off()

    def all_on(self):
        """
        Name:    LED.all_on
        Feature: Set all led PWM channels to high.
        Inputs:  None
        Outputs: None
        """
        self.led_on(self._BLUE)
        self.led_on(self._GREEN)
        self.led_on(self._RED)

    def connect(self):
        try:
            self.all_on()
            sleep(1)
            self.all_off()
        except:
            raise PRIDAPeripheralError('Could not connect to LED!')

    def poweroff(self):
        self.all_off()

    def current_status(self):
        super(self.current_status())

