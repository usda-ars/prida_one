#!/usr/bin/python
#
# hdf_organizer.py
#
# VERSION: 0.5.8
#
# LAST EDIT: 2015-09-28
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# 2015-06-10 -- created
# 2015-08-20 -- last updated
#
# ------------
# description:
# ------------
# This script sets up the HDF5 (Hierarchical Data Formal) files for storing
# plant root images and analysis (a part of the PRIDA Project).
#
# The general workflow follows this pattern:
#   1. Create a class instance:
#      ``````````````````````
#      my_class = PridaHDF()
#      ,,,,,,,,,,,,,,,,,,,,,,
#
#   2. Create new / open existing an HDF5 file:
#      a. Create new file:
#         ``````````````````````````````````````````````
#         my_class.new_file(str directory, str filename)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Open existing file:
#         ````````````````````````````````
#         my_class.open_file(str filename)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   3. List PIDs:
#      a. abstracted:
#         ````````````````````
#         my_class.list_pids()
#         ,,,,,,,,,,,,,,,,,,,,
#
#      b. explicit:
#         ``````````````````````````
#         my_class.list_objects('/')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   4. User's details:
#      a. Save HDF5 file details:
#         ````````````````````````````
#         my_class.set_root_user(str)
#         my_class.set_root_addr(str)
#         my_class.set_root_about(str)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Check details:
#         `````````````````````````
#         my_class.get_root_user()
#         my_class.get_root_addr()
#         my_class.get_root_about()
#         ,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   5. Create a new session:
#      ````````````````````````````````````````````````````````````````````
#      session_path = my_class.create_session(str pid, dict pid, dict sess)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      where the PID meta data dictionary looks like this:
#      ````````````````````````````````````````````````````````````````````
#      dict = {
#          "gen_sp"            : str, Genus species
#          "line"              : str, cultivar/line
#          "media"             : str, growing media (e.g., hydroponic, gel)
#          "germdate"          : str, germination date (YYYY-MM-DD)
#          "transdate"         : str, transplant date (YYYY-MM-DD)
#          "rep_num"           : int, replication number
#          "treatment"         : str, treatment type
#          "tubsize"           : str, size of tub used for growing plant
#          "tubid"             : str, tub indentifier
#          "nutrient"          : str, nutrient solution info
#          "growth_temp_day"   : float, daytime growing temp., deg. C
#          "growth_temp_night" : float nighttime growing temp., deg. C
#          "growth_light"      : str, lighting conditions
#          "water_sched"       : str, watering schedule
#          "notes"             : str, additional notes
#      }
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#
#      and where the session meta data dictionary looks like this:
#      ```````````````````````````````````````````````````````````````
#      dict = {
#          "user"         : str, session user's name
#          "addr"         : str, session user's email address
#          "date"         : str, session date (YYYY-MM-DD)
#          "number"       : int, session number
#          "img_taken"    : int, total number of images taken
#          "age_num"      : float, plant age
#          "age_unit"     : str, unit of plant age (e.g., days, weeks)
#          "cam_shutter"  : str, camera shutter speed
#          "cam_aperture" : str, camera aperature setting
#          "cam_exposure" : str, camera exposure time
#          "notes"        : str, additional notes
#      }
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   6. Set existing PID attributes
#      NOTE: dictionary keys that already exist will have their values
#            re-written and keys that do not exist will be added to the
#            dictionary!
#      `````````````````````````````````````
#      my_class.set_pid_attrs(str pid, dict)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   7. Set existing session attributes
#      NOTE: dictionary keys that already exist will have their values
#            re-written and keys that do not exist will be added to the
#            dictionary!
#      ``````````````````````````````````````````````````
#      my_class.set_session_attrs(str session_path, dict)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   8. Retrieve PID attrs:
#      ````````````````````````
#      my_class.get_pid_attrs()
#      ,,,,,,,,,,,,,,,,,,,,,,,,
#
#   9. List PID sessions:
#      a. sorted:
#         `````````````````````````````````
#         my_class.list_sessions(str 'PID')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. unsorted:
#         `````````````````````````````````
#         my_class.list_objects(str 'PID')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#  10. Retrieve session attrs:
#      ````````````````````````````````````````````
#      my_class.get_session_attrs(str session_path)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#  11. Save/list/extract datasets:
#      a. Save image to session:
#         ```````````````````````````````````````````
#         my_class.save_image(str session, str image)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. List images in a session:
#         `````````````````````````````````````````
#         my_class.list_objects(str '/PID/Session')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      c. Extract images to directory:
#         ````````````````````````````````````````````````````````````````
#         my_class.extract_dataset(str session, str output, str extension)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      d. Extract PID and session attributes to CSV file:
#         ````````````````````````````````````````````
#         my_class.extract_attrs(str output_location)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#  12. Save file (flush data to disk):
#      ```````````````
#      my_class.save()
#      ,,,,,,,,,,,,,,,
#
#  13. Close file (flush data to disk and close file handle):
#      ```````````````
#      my_class.close()
#      ,,,,,,,,,,,,,,,
#
#
# -----
# todo:
# -----
# 00. extract dataset level 0 should skip parent directory tree
# 01. Empty attributes should be returned as empty strings (not NoneType)
#     --> see get_attr function
# 02. Add compression, use gzip with shuffle=True and chucks=0.1*shape
#     also, try-catch image shape for monochrome images (img_to_hdf)
#
#
# $log_start_tag$
# 04e66bc: twdavis - 2015-08-20 09:02:36
#     chunked datasets
# 0b029f5: twdavis - 2015-08-14 14:22:02
#     Resolves #52
# eb4d030: twdavis - 2015-08-04 17:10:23
#     Resolves #47
# 7708f7c: twdavis - 2015-08-03 17:51:57
#     recursive method implemented; need to address recreating tree directory on image write
# d30ada0: twdavis - 2015-07-30 18:42:00
#     Addresses #47. Still working on recursive extraction
# e66ca37: twdavis - 2015-07-30 16:53:52
#     Addresses #47. Starting to mull through the requirements for recursive file extraction.
# e4b5c6f: twdavis - 2015-07-30 15:39:22
#     Resolves #46
# 64c61f3: twdavis - 2015-07-30 14:23:56
#     Resolves #45
# 1cc7f93: twdavis - 2015-07-30 14:22:16
#     Addresses #45. TODO: extract dataset
# 0059988: Hoi Cheng - 2015-07-22 13:06:12
#     This isnt Hoi, but its better now
# b233a65: twdavis - 2015-07-21 16:06:16
#     Resolves #40
# 14401a9: twdavis - 2015-07-15 17:16:24
#     addresses #19 with no positive solution; other updates included
# 186ae43: twdavis - 2015-07-15 13:04:23
#     Resolves #32
# cbb0cf4: twdavis - 2015-07-14 18:11:43
#     updates started on hdf_organizer
# a535a58: twdavis - 2015-07-06 13:28:28
#     fixed tab on L911
# 6483ae6: twdavis - 2015-07-06 12:20:30
#     Resolves #27
# 0303ec3: twdavis - 2015-07-02 16:28:06
#     getting closer to new name handling scheme
# 423dcc2: twdavis - 2015-07-01 17:32:43
#     further work
# fbd78f4: twdavis - 2015-06-30 18:02:23
#     working on exp and rep naming
# 5d218f3: twdavis - 2015-06-30 16:11:30
#     Resolves #24
# d4e1ba5: twdavis - 2015-06-26 18:13:40
#     resolved issue20
# 6bfd712: twdavis - 2015-06-26 12:40:50
#     resolved issue13
# a3f772a: twdavis - 2015-06-24 18:15:41
#     minor edit
# b48a0dd: twdavis - 2015-06-24 14:29:03
#     images can be extracted from HDF5 datasetsusing extract_dataset function
# 01b4471: twdavis - 2015-06-24 14:12:10
#     started extract_dataset function"
# f78f99e: twdavis - 2015-06-24 13:00:43
#     save and close functions implemented
# caae001: twdavis - 2015-06-24 11:58:34
#     get_rep_date now returns datetime.date object
# 50cf63f: twdavis - 2015-06-24 09:26:36
#     genus and species moved to single string
# 29deb53: twdavis - 2015-06-23 11:57:55
#     removed matplotlib dependency
# 30a2733: twdavis - 2015-06-19 17:51:27
#     Super updates
# b21830b: twdavis - 2015-06-19 13:10:28
#     hdf_organizer to class only; hdf_testing with previous functions etc.
# 51a2fa9: twdavis - 2015-06-19 13:06:15
#     moved hdf_organizer to main folder
# b7c186e: twdavis - 2015-06-19 12:54:20
#     deleted merge conflicted file
# 9a826e3: twdavis - 2015-06-19 12:52:15
#     merged?
# 6a90843: Jay - 2015-06-18 13:58:07
#     bugfixes
# ea22481: jay - 2015-06-14 02:33:49
#     v0.0.2
# $log_end_tag$
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import cv2
import errno
import h5py
import numpy
import os
import os.path
import re

###############################################################################
## CLASSES
###############################################################################
class PridaHDF:
    """
    Name:     PridaHDF
    Features: This class handles the organization and storage of plant root
              images and their associated meta data into a readable/writeable
              HDF5 (Hierarchical Data Format) file
    History:  VERSION 0.5.1-r1
              - added cv2, datetime, glob, and os.path modules [15.06.11]
              - create_experiment & create_rep class functions [15.06.12]
              - created select_hdf_file function [15.06.16]
              - moved 'cultivar' from experiment to rep meta data [15.06.16]
              - set cur_exp/cur_rep to last values in existing file [15.06.16]
              - create_hdf and open_hdf separated into own functions [15.06.18]
              - individual set/get root group meta data functions [15.06.18]
              - updated create_experiment function [15.06.18]
              - individual set/get experiment meta data functions [15.06.18]
              - rename open_file / new_file functions [15.06.19]
              - new / open experiment functions [15.06.19]
              - set / get experiment and rep meta data functions [15.06.19]
              - get_datasets function [15.06.19]
              - get_image function [15.06.19]
              - removed overwrite in open_file function [15.06.19]
              - list datasets, experiments and reps functions [15.06.19]
              - added optional exper and rep_path strings [15.06.19]
              --> allows for easy look-up without setting cur_exp or cur_rep
              - rotate_image and show_image functions [15.06.19]
              - changed experiment genus/species to a single string [15.06.23]
              - updated TIMEFORMAT to TIME_FORMAT [15.06.24]
              - get_rep_date now returns datetime.date object [15.06.24]
              - separate save and close functions [15.06.24]
              --> Note: flush request does not instantly save to disk
              - created extract_dataset function [15.06.24]
              - changed open_experiment to open_exp [15.06.26]
              - changed list_experiments to list_exps [15.06.26]
              - changed new_experiment to new_exp [15.06.26]
              - use regular expressions to name new exp and rep [15.06.26]
              - created get_attr function [15.06.26]
              --> all other get attribute functions depend on this
              - created set_attr function [15.06.26]
              --> all other set attribute functions depend on this
              - added rep_age_num, rep_age_unit and rep_media attrs [15.06.26]
              - added zero padding back to exp and rep names [15.06.30]
              - added get and set rep title functions [15.06.30]
              --> deleted rep title attribute [15.07.02]
              - created set defaults functions for exp and rep [15.06.30]
              --> default attributes defined on new function call
              - list exps and reps now return titles [15.06.30]
              - created how many exps and reps functions [15.07.01]
              - require rep number during create & use it for naming [15.07.02]
              - list_exps creates class dictionary exp_list [15.07.02]
              - created check function for exp titles [15.07.02]
              - added set/get root about attribute [15.07.14]
              - added create_session function [15.07.14]
              - added set session & pid attrs functions [15.07.14]
              - removed class variables cur_exp and cur_rep [15.07.14]
              - removed experiment and rep nomenclature [15.07.14]
              --> now focusing on PIDs and sessions
              - added get_pid_attrs function [15.07.15]
              - on open_file, pids class is populated [15.07.15]
              - list_sessions returns sorted list [15.07.15]
              - added list_pids function [15.07.15]
              - updated variables names for consistency [15.07.15]
              - added delete_object function [15.07.15]
              - added session path as a return in create_session [15.07.15]
              - removed TIME_FORMAT class variable [15.07.15]
              - abstracted refresh_pids and added to get_session_num [15.07.21]
              - added refresh_pids to delete_object [15.07.21]
              - created get_session_attrs function [15.07.21]
              - fixed save_image ds_path name [15.07.22]
              - save_image now puts image into its own sub-group [15.07.30]
              - deleted list_datasets function [15.07.30]
              - created list_objects function [15.07.30]
              --> freedom to list any parent group's member objects
              - added optional make_thumb flag in save_image [15.07.30]
              - created save_thumb function [15.07.30]
              - changed get_dataset to accept one string [15.07.30]
              - changed show_image to show_thumb [15.07.30]
              - ammended list_objects for finding datasets [15.07.30]
              - created members_are_groups checker [15.07.30]
              - new recursive method in extract_dataset [15.08.03]
              - added os and errno to imports [15.08.04]
              - added mkdir_p function [15.08.04]
              - hotfix extract_dataset function [15.08.14]
              - created extract_attrs function [15.08.14]
              - added chunks to dataset saving [15.08.20]
    Ref:      https://www.hdfgroup.org/HDF5/
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     PridaHDF.__init__
        Features: Initialize empty class.
        Inputs:   None.
        Outputs:  None.
        """
        # Define class constants:
        self.isopen = False                # HDF5 file handler
        self.pids = {}                     # PIDs are keys w/ session counter
    #
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def close(self):
        """
        Name:     PridaHDF.close
        Features: Saves changes to disk and closes HDF5 file handle, no further
                  reading or writing can be done.
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile.close()
            except:
                pass # already closed
            finally:
                self.isopen = False
                self.pids = {}
    #
    def create_session(self, pid, pid_attrs, session_attrs):
        """
        Name:     PridaHDF.create_session
        Features: Creates a new session under an existing (or newly created)
                  plant ID (PID) and sets the associated attributes to each
        Inputs:   - str, plant ID (pid)
                  - dict, PID attribute dictionary
                  - dict, session attribute dictionary
        Outputs:  str, new session path (session_path)
        Depends:
        """
        if self.isopen:
            session_num = self.get_session_number(pid)
            session_name = "Session-%d" % (session_num)
            session_path = "/%s/%s" % (pid, session_name)
            old_pid = self.pid_exists(pid)
            #
            # Create new session:
            try:
                self.hdfile.create_group(session_path)
            except:
                print "Error! Could not create session", session_path
            else:
                self.set_session_attrs(session_path, session_attrs)
                #
                # Set PID attributes and update dictionary:
                if old_pid:
                    self.pids[pid].append(session_num)
                else:
                    self.pids[pid] = [session_num,]
                    self.set_pid_attrs(pid, pid_attrs)
                return session_path
        else:
            print "Could not create new session. HDF5 file not open!"
    #
    def delete_object(self, obj_parent, obj_name):
        """
        Name:     PridaHDF.delete_object
        Features: Deletes a given object from a given HDF5 parent object's
                  member list; it does NOT reduce file size!
        Inputs:   - str, object's parent w/ path (obj_parent)
                  - str, group/dataset object name (obj_name)
        Outputs:  None.
        Depends:  refresh_pids
        """
        if self.isopen:
            if obj_parent[-1:] != '/':
                obj_parent += "/"
            obj_path = "%s%s" % (obj_parent, obj_name)
            if obj_path in self.hdfile:
                try:
                    del self.hdfile[obj_parent][obj_name]
                except:
                    print ("PridaHDF.delete_dataset error. "
                           "Could not delte dataset: %s") % (obj_path)
                else:
                    self.refresh_pids()
            else:
                print "Error! Could not find object", obj_path
        else:
            print "Error! Could not delete object. HDF5 file not open!"
    #
    def extract_attrs(self, opath):
        """
        Name:     PridaHDF.extract_attrs
        Features: Extracts PID and session attributes to two CSV files
                  a given directory
        Inputs:   str, full output path to directory where CSV files are to be
                  saved (opath)
        Outputs:  None.
        Depends:  - get_pid_attrs
                  - get_session_attrs
                  - list_objects
        """
        if self.isopen:
            # Create output files for saving attributes:
            bname = os.path.splitext(os.path.basename(self.hdfile.filename))[0]
            pids_fname = "%s_%s.%s" % (bname, "pid-attrs", "csv")
            sess_fname = "%s_%s.%s" % (bname, "sess-attrs", "csv")
            pids_file = os.path.join(opath, pids_fname)
            sess_file = os.path.join(opath, sess_fname)
            if os.path.isfile(pids_file) or os.path.isfile(sess_file):
                raise IOError("Cannot save file. File already exists!")
            else:
                try:
                    pFile = open(pids_file, 'w')
                    sFile = open(sess_file, 'w')
                except IOError:
                    print ("PridaHDF.extract_attrs error. "
                           "Could not open PID or session attribute files "
                           "for writing")
                except:
                    raise IOError("PridaHDF.extract_attrs unknown error!")
                else:
                    # Accumulate PID and session attribute keys:
                    pid_attr_keys = []
                    sess_attr_keys = []
                    for pid in self.pids:
                        pDict = self.get_pid_attrs(pid)
                        for k in pDict:
                            pid_attr_keys.append(k)
                        #
                        for session in self.list_objects(pid):
                            sPath = "/%s/%s" % (pid, session)
                            sDict = self.get_session_attrs(sPath)
                            for j in sDict:
                                sess_attr_keys.append(j)
                    pid_attr_keys = sorted(list(set(pid_attr_keys)))
                    sess_attr_keys = sorted(list(set(sess_attr_keys)))
                    #
                    # Build the headerline for PID attrs:
                    pid_headerline = 'PID,'
                    for k in pid_attr_keys:
                        pid_headerline += k
                        pid_headerline += ","
                    pid_headerline = pid_headerline.rstrip(",")
                    pid_headerline += "\n"
                    pFile.write(pid_headerline)
                    #
                    # Build the headerline for session attrs:
                    sess_headerline = 'PID,Session,'
                    for j in sess_attr_keys:
                        sess_headerline += j
                        sess_headerline += ','
                    sess_headerline = sess_headerline.rstrip(",")
                    sess_headerline += "\n"
                    sFile.write(sess_headerline)
                    #
                    # Write each PID attribute line:
                    for pid in self.pids:
                        pDict = self.get_pid_attrs(pid)
                        pid_write_line = str(pid)
                        pid_write_line += ","
                        for k in pid_attr_keys:
                            if k in pDict:
                                v = str(pDict[k])
                            else:
                                v = ''
                            v += ','
                            pid_write_line += v
                        pid_write_line = pid_write_line.rstrip(',')
                        pid_write_line += '\n'
                        pFile.write(pid_write_line)
                    #
                    # Write each session attribute line:
                    for pid in self.pids:
                        for session in self.list_objects(pid):
                            sess_write_line = str(pid)
                            sess_write_line += ","
                            sess_write_line += session
                            sess_write_line += ","
                            sPath = "/%s/%s" % (pid, session)
                            sDict = self.get_session_attrs(sPath)
                            for j in sess_attr_keys:
                                if j in sDict:
                                    u = str(sDict[j])
                                else:
                                    u = ''
                                u += ','
                                sess_write_line += u
                            sess_write_line = sess_write_line.rstrip(',')
                            sess_write_line += "\n"
                            sFile.write(sess_write_line)
                    #
                finally:
                    pFile.close()
                    sFile.close()
        #
        else:
            print "Error! Could not get attributes. HDF5 file not open!"
    #
    def extract_dataset(self, ipath, opath, img_ext='.jpg', level=0):
        """
        Name:     PridaHDF.extract_dataset
        Features: Extracts image datasets from a given HDF5 file hierarchy to
                  a given directory
        Inputs:   - str, HDF5 input object with associated path (ipath)
                  - str, absolute path to output directory (opath)
                  - [optional] str, image file extension (img_ext)
                  - [optional] int, number of levels deep (level)
        Outputs:  None.
        Note:     The extracted JPG seems to be slightly smaller than the
                  original; however, it does not appear to be an issue of lost
                  data.
        """
        if self.isopen:
            if ipath in self.hdfile:
                obj_members = self.list_objects(ipath)
                if obj_members == 0:
                    # Found dataset!
                    member_ext = os.path.splitext(ipath)[1]
                    if member_ext == img_ext:
                        out_path = os.path.join(opath, ipath[1:])
                        out_dir = os.path.dirname(out_path)
                        print ("Saving %s at level %i") % (out_path, level)
                        self.mkdir_p(out_dir)
                        dset = self.get_dataset(ipath)
                        try:
                            cv2.imwrite(out_path, dset)
                        except:
                            emess = ("Could not extract dataset "
                                     "to %s") % (out_path)
                            raise IOError(emess)
                else:
                    # Keep digging!
                    level += 1
                    for member in obj_members:
                        member_path = os.path.join(ipath, member)
                        self.extract_dataset(member_path, opath, img_ext, level)
            else:
                print ("PridaHDF.extract_datasets error! "
                       "Object '%s' does not exist.") % (ipath)
        else:
            print "Error! Could not get datasets. HDF5 file not open!"
    #
    def get_attr(self, attr, group_path):
        """
        Name:     PridaHDF.get_attr
        Features: Returns the given attribute for the given path
        Inputs:   - str, attribute name (attr)
                  - str, group path (group_path)
        Outputs:  str | int | NoneType, attribute value (val)
        """
        if self.isopen:
            if group_path in self.hdfile:
                try:
                    val = self.hdfile[group_path].attrs[attr]
                except KeyError:
                    print "Warning! '%s' has no attribute '%s'" % (group_path,
                                                                   attr)
                    val = None
                except:
                    print ("Warning! Attribute '%s' could not be retrieved "
                           "from path '%s'!") % (attr, group_path)
                    val = None
            else:
                print "Warning! Path not defined!"
                val = None
        else:
            print "Warning! Could not get attribute. HDF5 file not open!"
            val = None
        #
        return val
    #
    def get_pid_attrs(self, pid):
        """
        Name:     PridaHDF.get_pit_attrs
        Features: Returns dictionary of plant ID group attributes
        Inputs:   str, plant ID (pid)
        Outputs:  dict, PID attributes (attrs_dict)
        Depends:  set_attr
        """
        attr_dict = {}
        if self.isopen:
            if self.pid_exists(pid):
                for key,val in self.hdfile[pid].attrs.iteritems():
                    attr_dict[key] = val
            else:
                print "Error! Could not retrieve attributes from PID:", pid
        else:
            print "Error! Could not read PID attributes. HDF5 file not open!"
        #
        return attr_dict
    #
    def get_dataset(self, ds_path):
        """
        Name:     PridaHDF.get_dataset
        Features: Returns a given dataset for a given session
        Inputs:   str, dataset path (ds_path)
        Outputs:  numpy.ndarray
        """
        dset = numpy.array([])
        if self.isopen:
            if ds_path in self.hdfile:
                dset = self.hdfile[ds_path][:,:]
            else:
                print "Warning! Could not find image in dataset", ds_path
        else:
            print "Warning! Could not get datasets. HDF5 file not open!"
        #
        return dset
    #
    def get_root_about(self):
        """
        Name:     PridaHDF.get_root_about
        Features: Returns the root group about attribute
        Inputs:   None.
        Outputs:  str, root group about message (attr)
        Depends:  get_attr
        """
        if self.isopen:
            attr = self.get_attr("about", "/")
        else:
            print "Could not get root about. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_root_addr(self):
        """
        Name:     PridaHDF.get_root_addr
        Features: Returns the root group user address (e.g., NetID)
        Inputs:   None.
        Outputs:  str, root group user address (attr)
        Depends:  get_attr
        """
        if self.isopen:
            attr = self.get_attr("addr", "/")
        else:
            print "Could not get root addr. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_root_user(self):
        """
        Name:     PridaHDF.get_root_user
        Features: Returns the root group username
        Inputs:   None.
        Outputs:  str, root group username (attr)
        Depends:  get_attr
        """
        if self.isopen:
            attr = self.get_attr("user", "/")
        else:
            print "Could not get root user. HDF5 file not open!"
            attr = None
        #
        return attr
    #
    def get_session_attrs(self, session_path):
        """
        Name:     PridaHDF.get_session_attrs
        Features: Returns dictionary of plant ID session attributes
        Inputs:   str, session path (session_path)
        Outputs:  dict, session attributes (attrs_dict)
        """
        attr_dict = {}
        if self.isopen:
            if session_path in self.hdfile:
                for key,val in self.hdfile[session_path].attrs.iteritems():
                    attr_dict[key] = val
            else:
                print ("Error! Could not retrieve attributes from session: "
                       "%s") % (session_path)
        else:
            print "Error! Could not read PID attributes. HDF5 file not open!"
        #
        return attr_dict
    #
    def get_session_number(self, pid):
        """
        Name:     PridaHDF.get_session_number
        Features: Returns the value for a new session number
        Inputs:   str, plant ID (pid)
        Outputs:  None.
        Depends:  refresh_pids
        """
        self.refresh_pids()
        #
        try:
            session_max_val = max(self.pids[pid])
        except ValueError:
            session_num = 1
        except KeyError:
            session_num = 1
        except:
            print "Error! Unexpected problem with get_session_name."
            session_num = 1
        else:
            session_num = session_max_val + 1
        finally:
            return session_num
    #
    def img_to_hdf(self, img, ds_path):
        """
        Name:     PridaHDF.img_to_hdf
        Features: Saves image dataset to current HDF repository; currently set
                  without compression, but with chunks
        Inputs:   - numpy.ndarray, openCV image data array (img)
                  - str, dataset name with path (ds_path)
        """
        if ds_path in self.hdfile:
            print "WARNING: Overwriting dataset", ds_path
        if img is None:
            print "PridaHDF.img_to_hdf ERROR: no image data sent."
        else:
            try:
                x, y, z = img.shape # NOTE: issue will arise with monochrome
                if (x % 16 == 0 & y % 16 == 0):
                    my_chunks = (int(x/16), int(y/16), int(z))
                else:
                    my_chunks = (int(x/10), int(y/10), int(z))
                #
                self.hdfile.create_dataset(name=ds_path,
                                           data=img,
                                           chunks=my_chunks)
            except:
                print "PridaHDF.img_to_hdf ERROR: could not create dataset",
                print ds_path
            else:
                # Save image dimensions (i.e., pixel y, pixel x, color bands):
                # NOTE: openCV reads images in BGR not RGB
                self.hdfile[ds_path].dims[0].label = 'pixels in y'
                self.hdfile[ds_path].dims[1].label = 'pixels in x'
                self.hdfile[ds_path].dims[2].label = 'BGR color bands'
    #
    def list_objects(self, parent_path):
        """
        Name:     PridaHDF.list_objects
        Features: Returns a list of HDF5 objects under a given parent
        Inputs:   str, HDF5 path to parent object
        Outputs:  list, HDF5 objects
        """
        rlist = []
        if self.isopen:
            if parent_path in self.hdfile:
                try:
                    self.hdfile[parent_path].keys()
                except:
                    # Dataset has no members
                    rlist = 0
                else:
                    for obj in self.hdfile[parent_path].keys():
                        rlist.append(obj)
            else:
                print "Warning! Object %s does not exist!" % (parent_path)
        else:
            print "Warning! Could not get list objects. HDF5 file not open!"
        #
        return rlist
    #
    def list_pids(self):
        """
        Name:     PridaHDF.list_pids
        Features: Returns a list of current PIDs
        Inputs:   None.
        Outputs:  list, PID names
        Depends:  list_objects
        """
        pid_list = self.list_objects('/')
        return sorted(pid_list)
    #
    def list_sessions(self, pid):
        """
        Name:     PridaHDF.list_sessions
        Features: Returns a list of sorted sessions for a given PID
        Inputs:   str, plant ID (pid)
        Outputs:  list, session names
        Depends:  list_objects
        """
        rlist = []
        if self.pid_exists(pid):
            temp = {}
            for session in self.list_objects(pid):
                try:
                    temp_val = re.search('\D+(\d+)', session).group(1)
                    temp_val = int(temp_val)
                except:
                    print "There was a problem listing session", session
                else:
                    temp[temp_val] = session
            #
            for session_num in sorted(temp.keys()):
                rlist.append(temp[session_num])
        else:
            print "Warning! PID is empty."
        #
        return rlist
    #
    def mkdir_p(self, path):
        """
        Name:     PridaHDF.mkdir_p
        Features: Makes directories, including intermediate directories as
                  required (i.e., directory tree)
        Inputs:   str, directory path (path)
        Outputs:  None.
        Ref:      tzot (2009) "mkdir -p functionality in python," StackOverflow,
                  Online: http://stackoverflow.com/questions/600268/mkdir-p-
                  functionality-in-python
        """
        try:
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise
    #
    def new_file(self, dir_path, hdf_name):
        """
        Name:     PridaHDF.new_file
        Features: Creates a new HDF5 file, overwriting an existing file if it
                  exists.
        Inputs:   - str, directory path (dir_path)
                  - str, HDF5 filename (hdf_name)
        Outputs:  None.
        """
        # String handling:
        if dir_path[-1:] != "/":
            dir_path += "/"
        if hdf_name[-5:] != ".hdf5":
            hdf_name += ".hdf5"
        #
        # Save directory and filename as class variables:
        self.dir = dir_path
        self.filename = hdf_name
        my_hdf = dir_path + hdf_name
        #
        # Create empty HDF file:
        if os.path.isfile(my_hdf):
            print "Warning! That file already exits! Exiting..."
        else:
            print "Creating file", hdf_name, "...",
            try:
                self.hdfile = h5py.File(my_hdf, 'w')
            except:
                print "failed!"
                self.isopen = False
            else:
                print "success!"
                self.isopen = True
    #
    def open_file(self, hdf_path):
        """
        Name:     PridaHDF.open_file
        Features: Opens an existing HDF5 file or creates new if file is not
                  found.
        Inputs:   str, HDF5 filename with path (hdf_path)
        Outputs:  None.
        Depends:  refresh_pids
        """
        # Open existing HDF file:
        if os.path.isfile(hdf_path):
            print "Opening file", os.path.basename(hdf_path), "...",
        else:
            print "Could not find file! Creating",
            print os.path.basename(hdf_path),
            print "...",
        #
        try:
            # NOTE: 'a' flag opens existing or creates new
            self.hdfile = h5py.File(hdf_path, 'a')
        except:
            print "failed!"
            self.isopen = False
        else:
            print "success!"
            self.isopen = True
            #
            # Populate PIDS dictionary:
            # NOTE: reset pids before populating!
            self.refresh_pids()
    #
    def pid_exists(self, pid):
        """
        Name:     PridaHDF.pid_exists
        Features: Checks to see if PID already exists in HDF5 file
        Inputs:   str, plant ID (pid)
        Outputs:  bool
        """
        if pid in self.pids.keys():
            return True
        else:
            return False
    #
    def refresh_pids(self):
        """
        Name:     PridaHDF.refresh_pids
        Features: Refreshes the class dictionary with current plant IDs
        Inputs:   None.
        Outputs:  None.
        """
        self.pids = {}
        for pid in self.hdfile.keys():
            self.pids[pid] = []
            for session in self.hdfile[pid].keys():
                try:
                    # Search for digits following non-digits
                    session_num = re.search('\D+(\d+)', session).group(1)
                    session_num = int(session_num)
                except:
                    print "Error! Could not extract number from session",
                    print session
                else:
                    self.pids[pid].append(session_num)
    #
    def save(self):
        """
        Name:     PridaHDF.save
        Features: Flushes changes to the HDF file.
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile.flush()
            except:
                print "Could not flush file!"
    #
    def save_image(self, session_path, img_path, make_thumb=True):
        """
        Name:     PridaHDF.save_image
        Features: Saves image data as a dataset to a given session
        Input:    - str, session path (session_path)
                  - str, input image name with path (img_path)
                  - [optional] bool, flag for thumbnail creation (make_thumb)
        Output:   None.
        Depends:  img_to_hdf
        """
        if self.isopen:
            # NOTE: img_name includes the original file extension required for
            #       extraction (see extract_dataset)
            try:
                img_name = os.path.basename(img_path)
                img_base = os.path.splitext(img_name)[0]
            except:
                print ("PridaHDF.save_image error! "
                       "Could not process image: %s") % (img_path)
            else:
                if not os.path.isfile(img_path):
                    print ("PridaHDF.save_image error! "
                           "Image '%s' does not exist!") % (img_path)
                else:
                    if session_path[-1:] != '/':
                        session_path += "/"
                    ds_path = "%s%s/%s" % (session_path, img_base, img_name)
                    img = cv2.imread(img_path)
                    self.img_to_hdf(img, ds_path)
                    if make_thumb:
                        self.save_thumb(img, ds_path)
        else:
            print "Error! Could not save image. HDF5 file not open!"
    #
    def save_thumb(self, img, ds_path):
        """
        Name:     PridaHDF.save_thumb
        Features: Saves a thumbnail version of given image to same dataset
                  group scaled to a width of 250 pixels
        Inputs:   - numpy.ndarray, openCV image data array (img)
                  - str, dataset name with path (ds_path)
        """
        if ds_path in self.hdfile:
            if img is not None:
                try:
                    (height, width, depth) = img.shape
                except:
                    # Maybe greyscaled image?
                    print "Warning! Encountered greyscale image in save_thumb"
                    (height, width) = img.shape
                s_factor = 250.0/width
                #
                try:
                    thumb_path = "%s.thumb" % (os.path.splitext(ds_path)[0])
                    thumb = cv2.resize(img, (0,0), fx=s_factor, fy=s_factor)
                    self.hdfile.create_dataset(name=thumb_path, data=thumb)
                except:
                    print "PridaHDF.save_thumb ERROR: could not create dataset",
                    print thumb_path
    #
    def set_attr(self, attr_name, attr_val, obj_path):
        """
        Name:     PridaHDF.set_attr
        Features: Sets given attribute to given path
        Inputs:   - str, attribute name (attr_name)
                  - [dtype], attribute value (attr_val)
                  - str, path to group/dataset object (obj_path)
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile[obj_path].attrs.create(name=attr_name,
                                                   data=attr_val)
            except:
                print ("Error! Could not set attribute '%s' "
                       "to path '%s'") % (attr_name, obj_path)
        else:
            print ("Error! Could not set attribute. HDF5 file not open!")
    #
    def set_pid_attrs(self, pid, attrs_dict):
        """
        Name:     PridaHDF.set_pit_attrs
        Features: Sets plant ID group attributes
        Inputs:   - str, plant ID (pid)
                  - dict, PID attributes (attrs_dict)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if self.pid_exists(pid):
                for key,val in attrs_dict.iteritems():
                    try:
                        self.set_attr(key, val, pid)
                    except:
                        print "Error! Could not set PID attr", key
            else:
                print "Error! Could not attribute PID:", pid
        else:
            print "Error! Could not set PID attributes. HDF5 file not open!"
    #
    def set_root_about(self, attr):
        """
        Name:     PridaHDF.set_root_about
        Features: Sets the root group about attribute (e.g., short experiment
                  description)
        Inputs:   str, root group about message (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.set_attr("about", attr, "/")
        else:
            print "Could not set root about. HDF5 file not open!"
    #
    def set_root_addr(self, attr):
        """
        Name:     PridaHDF.set_root_addr
        Features: Sets the root group user address (e.g., NetID)
        Inputs:   str, root group user address (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.set_attr("addr", attr, "/")
        else:
            print "Could not set root addr. HDF5 file not open!"
    #
    def set_root_user(self, attr):
        """
        Name:     PridaHDF.set_root_user
        Features: Sets the root group username
        Inputs:   str, root group username (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.set_attr("user", attr, "/")
        else:
            print "Could not set root user. HDF5 file not open!"
    #
    def set_session_attrs(self, session_path, attrs_dict):
        """
        Name:     PridaHDF.set_pit_attrs
        Features: Sets session attributes
        Inputs:   - str, session path (session_path)
                  - dict, session attributes (attrs_dict)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if session_path in self.hdfile:
                for key,val in attrs_dict.iteritems():
                    try:
                        self.set_attr(key, val, session_path)
                    except:
                        print "Error! Could not set session attr", key
            else:
                print "Error! Could not attribute session", session_path
    #
    #    < -------- TESTING FUNCTIONS FOR DISPLAYING IMAGE DATA -------- >
    #
    def rotate_image(self, img, angle):
        """
        Name:     PridaHDF.rotate_image
        Features: Rotates an image about center a given number of degrees
        Inputs:   - numpy.ndarray, image data (img)
                  - int, angle of rotation
        Outputs:  numpy.ndarray, rotated image
        Ref:      http://john.freml.in/opencv-rotation
        """
        w = img.shape[1]
        h = img.shape[0]
        rangle = numpy.deg2rad(angle)
        #
        # Calculate new image width and height
        nw = (abs(numpy.sin(rangle)*h) + abs(numpy.cos(rangle)*w))*1.0
        nh = (abs(numpy.cos(rangle)*h) + abs(numpy.sin(rangle)*w))*1.0
        #
        # Ask OpenCV for the rotation matrix
        rot_mat = cv2.getRotationMatrix2D((nw*0.5, nh*0.5), angle, 1.0)
        #
        # Calculate the move from the old center to the new center combined
        # with the rotation
        rot_move = numpy.dot(rot_mat, numpy.array([(nw-w)*0.5, (nh-h)*0.5,0]))
        #
        # The move only affects the translation, so update the translation
        # part of the transform
        rot_mat[0,2] += rot_move[0]
        rot_mat[1,2] += rot_move[1]
        #
        # Warp each of the three 2D color bands:
        band1, band2, band3 = cv2.split(img)
        result1 = cv2.warpAffine(band1,
                                 rot_mat,
                                 (int(numpy.ceil(nw)),
                                 int(numpy.ceil(nh))),
                                 flags=cv2.INTER_LANCZOS4)
        result2 = cv2.warpAffine(band2,
                                 rot_mat,
                                 (int(numpy.ceil(nw)),
                                 int(numpy.ceil(nh))),
                                 flags=cv2.INTER_LANCZOS4)
        result3 = cv2.warpAffine(band3,
                                 rot_mat,
                                 (int(numpy.ceil(nw)),
                                 int(numpy.ceil(nh))),
                                 flags=cv2.INTER_LANCZOS4)
        result = cv2.merge([result1,result2,result3])
        return result
    #
    def show_thumb(self, session_path, base_name, rot=0):
        """
        Name:     PridaHDF.show_thumb
        Features: Displays a thumb image using openCV
        Inputs:   - str, session path (session_path)
                  - str, dataset base name (base_name)
                  - [optional] int, rotation angle (rot)
        Outputs:  None.
        Depends:  - get_dataset
                  - rotate_image
        """
        if self.isopen:
            if session_path[-1:] != '/':
                session_path += "/"
            ds_path = "%s%s/%s.thumb" % (session_path, base_name, base_name)
            if ds_path in self.hdfile:
                img = self.get_dataset(ds_path)
                cv2.namedWindow('Image', cv2.WINDOW_AUTOSIZE)
                #
                # Rotation:
                if rot != 0:
                    img = self.rotate_image(img, int(rot))
                #
                cv2.imshow("Image", img)
                cv2.moveWindow("Image", 0, 0)
                cv2.waitKey(0)
                cv2.destroyWindow("Image")
            else:
                print "Error! Could not show image:", ds_path
        else:
            print "Error! Could not plot image. HDF5 file not open!"
    #
