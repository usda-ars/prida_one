# -*- mode: python -*-

block_cipher = None

my_path = "./"
prida_path = my_path + "prida/"

a = Analysis([my_path + 'main.py'],
             pathex=['.'],
             binaries=[],
             datas=[],
             hiddenimports=['PyQt5',
                            'h5py.defs',
                            'h5py.utils',
                            'h5py.h5ac',
                            'h5py._proxy',
                            'scipy.linalg',
                            'scipy.linalg.cython_blas',
                            'scipy.linalg.cython_lapack',
                            'scipy.integrate'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=None,
             win_private_assemblies=None,
             cipher=block_cipher)
a.datas += [('greeter.jpg', prida_path + 'greeter.jpg', 'DATA'),
            ('loading.gif', prida_path + 'loading.gif', 'DATA')]
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='Prida',
          debug=False,
          strip=None,
          upx=True,
          console=True)
app = BUNDLE(exe,
             name='Prida.app',
             icon='icon.icns',         # or None if unavailable
             bundle_identifier=None,
             info_plist={
                 'CFBundleDevelopmentRegion': 'en_US',
                 'CFBundleShortVersionString': '1.4',
                 'CFBundleVersion': '1.4.0',
                 'CFBundleName': 'Prida',
                 'CFBundleDisplayName': 'Prida',
                 'CFBundleSignature': '????',
                 'NSHighResolutionCapable': 'True',
                 'NSHumanReadableCopyright': 'This software is freely available to the public for use.'
                 }
             )
