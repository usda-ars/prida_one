#!/usr/bin/python
#
# data_worker.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software is freely available to the public for use.      #
# The Department of Agriculture (USDA) and the U.S. Government have not       #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     USDA-Agricultural Research Service                                      #
#     Robert W. Holley Center for Agriculture and Health                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os

import numpy
import scipy.misc
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QMutexLocker
from PyQt5.QtCore import QObject
from PyQt5.QtGui import QPixmap

from .hdf_organizer import PridaHDF
from .utilities import get_ctime
from .utilities import init_pid_attrs
from .utilities import init_session_attrs


###############################################################################
# CLASSES:
###############################################################################
class DataWorker(QObject):
    """
    Name:     DataWorker
    Features: This class is a worker object used for threading data management.
    History:  Version 1.4.0
              - created add/rm search file signals [16.03.07]
              - created the add/rm search file functions [16.03.07]
              - created set/get input field signal and socket [16.03.07]
              - created data listed signal and get data list socket [16.03.07]
              - created file opened signal [16.03.08]
              - created new file socket [16.03.08]
              - removed abort attribute [16.03.08]
              - created pid attrs dictionary [16.03.08]
              - added session list to data list [16.03.08]
              - created get ps dict signal and socket [16.03.09]
              - updated get dataset to handle orientation [16.03.09]
              - created get/set IDA signals and sockets [16.03.09]
              - created datasets attribute [16.03.09]
              - moved autofill PID function here [16.03.09]
              - dynamically assign image number count [16.03.10]
              - created set session default socket [16.03.10]
              - created session image count checker [16.03.10]
              - edited new file function parameters [16.03.10]
              - created open search file function [16.03.15]
              - removed PS dict signal and socket [16.03.15]
              - created get analysis signal socket [16.03.22]
              - added binary image support for thumbnails [16.03.23]
              - added import done and updated finished signals [16.03.30]
              - updated dynamic image number assignment [16.04.04]
              - removed list datasets from check image count [16.04.19]
              - removed list datasets from get analysis vals [16.04.19]
              - added find datasets to get thumbs function [16.05.11]
              - updated add thumb signal [16.05.11]
              - updated search added signal with two ints [16.06.23]
              - updated search file addition function [16.06.23]
              - fixed spinner stop for no images in get thumbs [16.06.27]
              - import images socket now takes a list [16.06.30]
              - added check image shapes function [16.06.30]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    addImageThumb = pyqtSignal(int, str, numpy.ndarray)  # thumbnail creation
    addThumb = pyqtSignal(int, int, str, numpy.ndarray, int)  # for thumbnails
    checkedImages = pyqtSignal(list, bool)   # for image shape checking
    datasetsListed = pyqtSignal(list)  # list of session's datasets
    dataListed = pyqtSignal(list)  # for updating Prida's data list
    donebusy = pyqtSignal()        # for ending file IO
    fieldSet = pyqtSignal(tuple)   # for setting Qt fields
    importDone = pyqtSignal()      # to end importing
    finished = pyqtSignal()        # to end thread
    fileClosed = pyqtSignal()      # file is closed
    fileOpened = pyqtSignal(int)   # confirmation of file open
    idaSet = pyqtSignal(numpy.ndarray, int)   # oriented IDA VIEWER image
    importing = pyqtSignal(int, tuple, bool)  # import images - update GUI
    isbusy = pyqtSignal(str)      # for starting file IO
    report = pyqtSignal(str)      # for error messages to Prida app
    searchAdded = pyqtSignal(dict, int, int)  # PID attributes for search file
    searchRemoved = pyqtSignal(list)  # HDF5 file & PID key list
    sessionAttrs = pyqtSignal(dict)   # current session attributes
    setImg = pyqtSignal(numpy.ndarray, int)   # oriented image for processing
    toAnalysis = pyqtSignal(str, dict, list)  # info for analysis view

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None, mutex=None):
        """
        Name:     DataWorker.__init__
        Inputs:   [optional] Qt parent object (parent)
        Returns:  None.
        Features: Class initialization
        """
        super(DataWorker, self).__init__(parent)

        # Create a worker logger:
        self.logger = logging.getLogger(__name__)
        self.logger.debug("data worker object initialized")

        # Initialize variables
        self.mutex = mutex      # mutex lock
        self.session = None     # current session path
        self.output_dir = None  # where exports should be saved
        self.pid_attrs = init_pid_attrs()
        self.session_attrs = init_session_attrs()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def about(self):
        return self.hdf.get_about()

    @about.setter
    def about(self, value):  # lint:ok
        self.hdf.set_root_about(value)

    @property
    def basename(self):
        return self.hdf.basename()

    @property
    def pid_list(self):
        if self.hdf.isopen:
            return self.hdf.list_pids()
        else:
            return []

    @property
    def root_user(self):
        return str(self.hdf.get_root_user())

    @root_user.setter
    def root_user(self, value):  # lint:ok
        self.hdf.set_root_user(value)

    @property
    def root_addr(self):
        return str(self.hdf.get_root_addr())

    @root_addr.setter
    def root_addr(self, value):  # lint:ok
        self.hdf.set_root_addr(value)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def autofill_pid(self, pid=''):
        """
        Name:     DataWorker.autofill_pid
        Inputs:   [optional] str, plant ID (pid)
        Outputs:  None.
        Features: Activated when PID field is entered; signals data to find PID
                  attributes for autofilling
        """
        self.logger.debug("performing PID auto-completion")
        obj_path = "/%s" % (pid)
        for i in self.pid_attrs:
            f_name = self.pid_attrs[i]["qt_val"]
            f_type = self.pid_attrs[i]["qt_type"]
            attr_name = self.pid_attrs[i]["key"]
            f_value = self.get_attr(attr_name, obj_path)
            self.fieldSet.emit((f_name, f_type, f_value))

    def check_image_count(self):
        """
        Name:     DataWorker.check_image_count
        Inputs:   None.
        Outputs:  None.
        Features: Dynamically assigns session image count field
        Depends:  - list_sessions
                  - set_attr
        """
        self.logger.debug("checking image count")
        for pid in self.pid_list:
            for session in self.list_sessions(pid):
                s_path = "/%s/%s" % (pid, session)
                my_imgs = []
                for k, v in self.hdf.datasets.items():
                    if v == s_path:
                        my_imgs.append(k)
                my_imgs = sorted(my_imgs)
                num_imgs = len(my_imgs)
                attr_name = self.session_attrs[5]["key"]
                try:
                    attr_val = self.get_attr(attr_name, s_path)
                    attr_val = int(attr_val)
                except:
                    self.logger.exception("could not cast %s to int", attr_val)
                    self.logger.debug(
                        "dynamically setting %s image count to %d" % (
                            s_path, num_imgs))
                    new_val = str(num_imgs)
                    self.set_attr(attr_name, new_val, s_path)
                else:
                    if attr_val != num_imgs:
                        self.logger.debug(
                            "dynamically setting %s image count to %d" % (
                                s_path, num_imgs))
                        new_val = str(num_imgs)
                        self.set_attr(attr_name, new_val, s_path)
                    else:
                        self.logger.debug("image count is correct")

    def check_image_shapes(self, img_list):
        """
        Name:     DataWorker.check_image_shapes
        Inputs:   list, image list (img_list)
        Outputs:  bool, shape consistency check
        Features: Checks for shape consistency amongst images in a list of
                  image file paths
        """
        img_shape = None
        check = True
        for i in range(len(img_list)):
            img_path = img_list[i]
            img = scipy.misc.imread(img_path)
            if i == 0:
                img_shape = img.shape
            else:
                if img.shape != img_shape:
                    check = False
        self.logger.debug("consistency check %s", check)
        self.checkedImages.emit(img_list, check)

    def close_file(self):
        """
        Name:     DataWorker.close_file
        Inputs:   None.
        Outputs:  None.
        Features: Closes HDF file; emits data closed signal
        """
        self.logger.debug("closing HDF file")
        self.hdf.close()
        self.logger.info("signaling GUI that file is closed")
        self.fileClosed.emit()

    def close_search_file(self):
        """
        Name:     DataWorker.close_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Closes HDF file during inter-file searching
        """
        self.logger.debug("closing HDF file")
        self.hdf.close()

    def compress_file(self):
        """
        Name:     DataWorker.compress_file
        Inputs:   None.
        Outputs:  isbusy, donebusy and report signals
        Features: Creates a compressed copy of the currently opened HDF file
                  and emits the appropriate message for GUI popup
        """
        # Alert Prida that we have started compressing
        self.isbusy.emit("Compressing file...")

        err_msg = ""
        if self.hdf.isopen:
            err_code = self.hdf.compress()
            if err_code == 0:
                err_msg = "Successfully created compressed HDF file."
            elif err_code == 1:
                err_msg = ("Failed to create compressed HDF file. Check that "
                           "you have write privileges and try again.")
            elif err_code == -1:
                err_msg = ("An error was encountered during writing to the "
                           "compressed HDF file. Try re-opening this file and "
                           "running compression again.")
            elif err_code == 9999:
                err_msg = ("Failed to create compressed HDF file. The file "
                           "already exists. Please rename the existing file "
                           "and try again.")
            else:
                err_msg = ("An unknown error was encountered during "
                           "compression. Try re-opening this file and running "
                           "the compression again.")
        else:
            err_msg = ("Failed to create compressed HDF file. No open HDF "
                       "file was found. Please open an existing HDF file and "
                       "try again.")

        self.donebusy.emit()
        self.report.emit(err_msg)

    def create_hdf(self, my_parser=None, config_filepath=None):
        """
        Name:     DataWorker.create_hdf
        Inputs:   - [optional] function handle (my_parser)
                  - [optional] str, configuration file path (config_filepath)
        Outputs:  None.
        Features: Creates an PridaHDF class instance
        """
        self.logger.debug("creating HDF object")
        self.hdf = PridaHDF(my_parser, config_filepath)

    def create_session(self, pid, pid_attrs, session_attrs):
        """
        Name:     DataWorker.create_session
        Inputs:   - str, plant ID (pid)
                  - dict, plant ID metadata (pid_attrs)
                  - dict, session metadata (session_attrs)
        Outputs:  None.
        Features: Creates a new project session for a given PID, saves the
                  PID and session metadata and saves/returns the session path
        """
        self.logger.info("caught create new session signal")
        self.session = self.hdf.create_session(pid, pid_attrs, session_attrs)

    def extract_to_file(self):
        """
        Name:     DataWorker.extract_to_file
        Inputs:   None.
        Outputs:  isbusy, donebusy, and report signals
        Features: Extracts datasets and metadata at and below a given HDF level
        """
        self.isbusy.emit("Extracting to file...")

        if os.path.isdir(self.output_dir):
            # Extract attribute files:
            try:
                self.logger.debug(
                    "extracting attributes to %s", self.output_dir)
                self.hdf.extract_attrs(self.output_dir)
            except IOError:
                self.logger.warning(
                    "failed to export attributes to %s",
                    self.output_dir)
            except ValueError:
                self.logger.warning(
                    "failed to export attributes; file exists")
            except:
                self.logger.exception(
                    "caught unknown error exception")
            else:
                self.logger.debug(
                    "attributes extraction successful")

            # Extract datasets:
            try:
                self.logger.debug(
                    "extracting datasets to %s", self.output_dir)
                self.hdf.extract_datasets(self.session, self.output_dir)
            except IOError:
                self.logger.warning("failed to extract datasets")
                err_msg = ("Failed to extract datasets to %s. "
                           "Please check output directory privileges "
                           "and try again.") % (self.output_dir)
            except:
                self.logger.exception(
                    "caught unknown error exception")
                err_msg = ("An unexpected error occurred during dataset "
                           "extraction. Please check that output "
                           "directory %s exists and that you have write "
                           "privileges.") % (self.output_dir)
            else:
                self.logger.debug("datasets extraction successful")
                err_msg = ("Successfully extracted datasets "
                           "to %s") % (self.output_dir)
        else:
            err_msg = "Failed to extract, output directory does not exist!"

        self.donebusy.emit()
        self.report.emit(err_msg)

    def get_analysis_vals(self, s_path):
        """
        Name:     DataWorker.get_analysis_vals
        Inputs:   str, session path (s_path)
        Outputs:  - dict, session attributes
                  - list, session image paths
        Features: Catches for analysis signal, finds session attributes and
                  list of images; emits to analysis signal
        """
        self.logger.info("caught for analysis signal")

        # Gathering session attributes:
        for i in self.session_attrs:
            my_attr = self.get_attr(self.session_attrs[i]['key'], s_path)
            self.session_attrs[i]['cur_val'] = my_attr

        # Gathering list of session's datasets:
        my_imgs = []
        for k, v in self.hdf.datasets.items():
            if v == s_path:
                my_imgs.append(k)
        my_imgs = sorted(my_imgs)

        self.logger.info("sending to analysis signal")
        self.toAnalysis.emit(s_path, self.session_attrs, my_imgs)

    def get_attr(self, attr, obj_path):
        """
        Name:     DataWorker.get_attr
        Inputs:   - str, attribute name (attr)
                  - str, object path (obj_path)
        Outputs:  str, attribute value
        Features: Returns a requested attribute from a given object
        """
        return self.hdf.get_attr(attr, obj_path)

    def get_data_list(self):
        """
        Name:     DataWorker.get_data_list
        Inputs:   None.
        Outputs:  None.
        Features: Reads an HDF5 file's PID attributes to dictionary and emits
                  the data listed signal
        Depends:  list_sessions
        """
        self.logger.debug("caught get data list signal")
        data_list = []

        self.logger.debug("reading PID attributes")
        for pid in self.pid_list:
            p_path = "/%s" % (pid)
            d = {}
            d["pid"] = pid
            d["sessions"] = self.list_sessions(pid)
            for i in self.pid_attrs.keys():
                my_attr = self.pid_attrs[i]["key"]
                d[my_attr] = self.get_attr(my_attr, p_path)
            data_list.append(d)
        self.logger.info("sending data listed signal")
        self.dataListed.emit(data_list)

    def get_dataset(self, ds_path):
        """
        Name:     DataWorker.get_dataset
        Inputs:   str, dataset path (ds_path)
        Outputs:  ndarray, dataset
        Features: Returns a dataset (i.e., array) for a given dataset path;
                  handles orientation
        Depends:  - get_image_session
                  - get_attr
        """
        self.logger.debug("gathering dataset %s", ds_path)
        ndarray_full = self.hdf.get_dataset(ds_path)
        if ndarray_full.size != 0:
            self.logger.debug("getting image session path")
            s_path = self.get_image_session(ds_path)

            self.logger.debug("checking IDA image orientation")
            if s_path is not None:
                img_orient = self.get_attr("img_orient", s_path)
            else:
                img_orient = "Original"

            if img_orient and img_orient != "Original":
                if img_orient == "Rotated Clockwise":
                    self.logger.debug("rotating image clockwise")
                    ndarray_full = numpy.rot90(ndarray_full, 3)
                elif img_orient == "Rotated Counter-Clockwise":
                    self.logger.debug(
                        "rotating image counter-clockwise")
                    ndarray_full = numpy.rot90(ndarray_full)
                else:
                    self.logger.warning(
                        "failed to match orientation %s",
                        img_orient)
        else:
            self.logger.debug("returning empty dataset")

        return ndarray_full

    def get_ida(self, img_path, enum):
        """
        Name:     DataWorker.get_ida
        Inputs:   - str, image path (img_path)
                  - int, view enumerator (enum)
        Outputs:  None.
        Features: Catches get IDA signal, gathers dataset, and signals array
                  back to GUI
        Depends:  get_dataset
        """
        self.logger.info("caught get IDA signal")
        self.logger.debug("gathering image data for selection")
        ndarray_full = self.get_dataset(img_path)
        self.idaSet.emit(ndarray_full, enum)

    def get_image_session(self, img_path):
        """
        Name:     DataWorker.get_image_session
        Inputs:   str, image path (img_path)
        Outputs:  str, session path
        Features: Returns the session name for a given image path
        """
        return self.hdf.get_img_session(img_path)

    def get_process_img(self, img_path, img_count):
        """
        Name:     DataWorker.get_process_img
        Inputs:   - str, image path (img_path)
                  - int, image counter (img_count)
        Outputs:  None.
        Features: Catches signal, gathers dataset, and signals array
                  back for analysis
        Depends:  get_dataset
        """
        self.logger.info("caught signal for image %d", img_count)
        ndarray_full = self.get_dataset(img_path)
        self.logger.info("return signal for image %s", img_path)
        self.setImg.emit(ndarray_full, img_count)

    def get_thumbs(self, s_path, enum):
        """
        Name:     DataWorker.get_thumbs
        Inputs:   - str, session path (img_path)
                  - int, view enumerator (enum)
        Outputs:  None.
        Features: Signals arrays of session thumbs to the GUI based on the
                  enum value
        """
        self.logger.debug("searching session for images")
        self.hdf.find_datasets()
        my_imgs = []
        for k, v in self.hdf.datasets.items():
            if v == s_path:
                my_imgs.append(k)
        my_imgs = sorted(my_imgs)
        my_img_names = [
            os.path.splitext(os.path.basename(i))[0] for i in my_imgs]
        num_imgs = len(my_imgs)

        self.logger.debug("searching session for thumbnails")
        self.hdf.find_thumbnails()
        my_thumbs = []
        for k, v in self.hdf.thumbnails.items():
            if v == s_path:
                my_thumbs.append(k)
        my_thumbs = sorted(my_thumbs)
        my_thumb_names = [
            os.path.splitext(os.path.basename(i))[0] for i in my_thumbs]

        # Fix problem where spinner does not stop when no images are available:
        if num_imgs == 0:
            self.addThumb.emit(0, 0, "", numpy.array([]), enum)

        for i in range(num_imgs):
            img_name = my_img_names[i]
            img_path = my_imgs[i]
            if img_name in my_thumb_names:
                j = my_thumb_names.index(img_name)
                thumb_path = my_thumbs[j]
                thumb = self.get_dataset(thumb_path)
            else:
                ndarray_full = self.get_dataset(img_path)
                try:
                    thumb = self.hdf.make_thumb(ndarray_full)
                except IOError:
                    self.logger.exception(
                        "thumbnail scaling failed")
                    thumb = numpy.zeros((250, 250, 3))
                except:
                    self.logger.exception(
                        "encountered unknown error")
                    thumb = numpy.zeros((250, 250, 3))

            # Add binary image support:
            if thumb.max() == 1 and len(thumb.shape) == 2:
                self.logger.debug("found binary thumb image!")
                self.logger.debug("scaling binary to grayscale")
                thumb[numpy.where(thumb == 1)] *= 255
                self.logger.debug("scaling complete")

            self.logger.debug(
                "signaling image thumb %d/%d" % (i + 1, num_imgs))
            self.addThumb.emit(i + 1, num_imgs, img_path, thumb, enum)

    def get_input_field(self, obj_path, attr_name, f_name, f_type):
        """
        Name:     DataWorker.get_input_field
        Inputs:   - str, object path (obj_path)
                  - str, attribute name (attr_name)
                  - str, Qt field name (f_name)
                  - str, Qt field type (f_type)
        Outputs:  None.
        Features: Emits signal if object exists with appropriate field
                  attribute
        Depends:  get_attr
        """
        if obj_path in self.hdf.hdfile:
            self.logger.debug(
                "gathering attribute for %s", attr_name)
            f_value = self.get_attr(attr_name, obj_path)
            self.fieldSet.emit((f_name, f_type, f_value))
        else:
            self.logger.warning("%s does not exist", obj_path)

    def get_session_attrs(self, s_path):
        """
        Name:     DataWorker.get_session_attrs
        Inputs:   str, session path (s_path)
        Outputs:  None.
        Features: Receives the get session attributes signal; sets the current
                  values for all session attributes; signals the dictionary
                  back to the GUI
        """
        for i in self.session_attrs:
            my_attr = self.get_attr(self.session_attrs[i]['key'], s_path)
            self.session_attrs[i]['cur_val'] = my_attr
        self.sessionAttrs.emit(self.session_attrs)

    def get_session_datasets(self, s_path):
        """
        Name:     DataWorker.get_session_datasets
        Inputs:   str, session path (s_path)
        Outputs:  None.
        Features: Receives the signal; sends the signal
        """
        # Gathering list of session's datasets:
        self.logger.info("caught list datasets signal")
        self.hdf.datasets = {}
        self.hdf.find_datasets(s_path)
        my_imgs = sorted(list(self.hdf.datasets.keys()))
        self.logger.info("sending datasets listed signal")
        self.datasetsListed.emit(my_imgs)

    def import_images(self, img_files):
        """
        Name:     DataWorker.import_images
        Inputs:   list, image files (img_files)
        Outputs:  None.
        Features: Imports images to current session; signals importing to Prida
                  to update the GUI and import done when complete.
        """
        if len(img_files) > 0:
            for file_idx in range(len(img_files)):
                file_path = img_files[file_idx]
                img = QPixmap(file_path)
                a_date = get_ctime(file_path).strftime("%Y-%m-%d")
                a_time = get_ctime(file_path).strftime("%H.%M.%S")
                my_t = (True, file_path, a_date, a_time, str(file_idx),
                        str(img.height()), str(img.width()), "0")
                try:
                    self.logger.debug("saving image %s", file_path)
                    self.save_image(my_t)
                except:
                    self.logger.exception(
                        "failed to save dataset %s", self.session)
                else:
                    self.importing.emit(file_idx, my_t, True)
                    self.logger.debug("saved image %s", file_path)
            self.importDone.emit()

    def list_objects(self, obj_path):
        """
        Name:     DataWorker.list_objects
        Inputs:   str, object path (obj_path)
        Outputs:  list, member object names
        Features: Returns a list of member objects for a given object path
        """
        return self.hdf.list_objects(obj_path)

    def list_sessions(self, pid):
        """
        Name:     DataWorker.list_sessions
        Inputs:   str, plant ID (pid)
        Outputs:  list, session names
        Features: Returns a list of sessions for a given plant ID
        """
        return self.hdf.list_sessions(pid)

    def new_file(self, path):
        """
        Name:     DataWorker.new_file
        Inputs:   str, HDF5 file path
        Outputs:  None.
        Features: Creates a new HDF file; emits file created or failed signal
        """
        self.logger.debug("caught new file signal")
        try:
            self.hdf.open_file(path)
            self.save_file()
        except ValueError:
            self.logger.exception("failed to unpack values")
            self.fileOpened.emit(1)
        except:
            self.logger.exception("encountered unexpected error")
            self.fileOpened.emit(1)
        else:
            self.root_user = ""
            self.root_addr = ""
            self.about = ""
            self.logger.debug("sending new file opened signal")
            self.fileOpened.emit(0)

    def open_file(self, file_path):
        """
        Name:     DataWorker.open_file
        Inputs:   str, file path (file_path)
        Outputs:  None.
        Features: Opens a new HDF file; saves root user and address to
                  defaults; and emits existing file opened signal
        """
        self.logger.debug("opening HDF file %s", file_path)
        self.hdf.close()
        self.hdf.open_file(file_path)
        self.check_image_count()
        self.session_attrs[0]['def_val'] = self.root_user
        self.session_attrs[1]['def_val'] = self.root_addr
        self.fileOpened.emit(-1)

    def open_search_file(self, file_path):
        """
        Name:     DataWorker.open_search_file
        Inputs:   str, file path (file_path)
        Outputs:  None.
        Features: Opens an existing HDF file
        """
        self.logger.debug("opening HDF file %s", file_path)
        self.hdf.close()
        self.hdf.open_file(file_path)

    def quit(self):
        """
        Name:     DataWorker.quit
        Inputs:   None.
        Outputs:  None.
        Features: Returns the finished signal
        """
        self.finished.emit()

    def save_file(self):
        """
        Name:     DataWorker.save_file
        Inputs:   None.
        Outputs:  None.
        Features: Saves/flushes HDF data to file
        """
        self.logger.debug("saving HDF file")
        self.hdf.save()

    def save_image(self, t):
        """
        Name:     DataWorker.save_image
        Inputs:   - tuple (t)
        Outputs:  None.
        Features: Saves photo and attributes to HDF
        """
        self.logger.debug("gathering image metadata")
        # is_path = t[0]
        img_path = t[1]
        datestamp = t[2]
        timestamp = t[3]
        anglestamp = t[4]
        image_height = t[5]
        image_width = t[6]
        orientation = t[7]
        basename = os.path.basename(img_path)
        name = os.path.splitext(basename)[0]
        a_path = os.path.join(self.session, name, basename)
        if os.name is 'nt':
            a_path = a_path.replace(os.sep, '/')

        if self.mutex is not None:
            locker = QMutexLocker(self.mutex)

        try:
            self.logger.info(
                "saving image %s to HDF file", img_path)
            self.hdf.save_image(self.session, img_path)
        except:
            self.logger.exception(
                "failed to save image %s", img_path)
        else:
            try:
                self.logger.debug(
                    "saving session %s attributes", self.session)
                self.set_attr("Date", datestamp, a_path)
                self.set_attr("Time", timestamp, a_path)
                self.set_attr("Angle", anglestamp, a_path)
                self.set_attr("Height", image_height, a_path)
                self.set_attr("Width", image_width, a_path)
                self.set_attr("Orientation", orientation, a_path)
            except:
                self.logger.exception(
                    "failed to set session %s attributes",
                    self.session)
        finally:
            self.logger.debug("flushing to HDF file")
            self.save_file()

        if self.mutex is not None:
            locker.unlock()

    def search_file_addition(self, file_path, cur_idx, max_idx):
        """
        Name:     DataWorker.search_file_addition
        Inputs:   - str, HDF5 file path (file_path)
                  - int, current index value (cur_idx)
                  - int, maximum index value (max_idx)
        Outputs:  None.
        Features: Reads an HDF5 file's PID attributes to dictionary and emits
                  the search file signal
        """
        f_name = os.path.basename(file_path)
        f_name = os.path.splitext(f_name)[0]

        self.logger.debug("opening search file")
        self.open_search_file(file_path)

        self.logger.debug("reading search file PIDs")
        search_data = {}
        for pid in self.pid_list:
            # Create a unique dictionary key for each file's PIDs:
            my_key = "%s.%s" % (f_name, pid)

            my_data = [f_name, pid]
            my_path = os.path.join('/', str(pid))
            for i in self.pid_attrs:
                my_attr = self.pid_attrs[i]["key"]
                my_attr_val = self.get_attr(my_attr, my_path)
                self.logger.debug(
                    "adding PID attribute %s = %s" % (
                        my_attr, my_attr_val))
                my_data.append(my_attr_val)

            # Save search file's PID attributes:
            search_data[my_key] = my_data

        self.logger.debug("closing search file")
        self.hdf.close()

        self.logger.debug("emitting search file signal")
        self.searchAdded.emit(search_data, cur_idx, max_idx)

    def search_file_removal(self, file_path):
        """
        Name:     DataWorker.search_file_removal
        Inputs:   str, HDF5 file path (file_path)
        Outputs:  None.
        Features: Creates a list of HDF5 file name and PID keys and signals
                  the remove search file signal
        """
        f_name = os.path.basename(file_path)
        f_name = os.path.splitext(f_name)[0]

        self.logger.debug("opening selected file")
        self.open_search_file(file_path)

        key_list = []
        for pid in self.pid_list:
            my_key = "%s.%s" % (f_name, pid)
            key_list.append(my_key)

        self.logger.debug("closing selected file")
        self.hdf.close()
        self.searchRemoved.emit(key_list)

    def set_attr(self, attr_name, attr_val, obj_path):
        """
        Name:     DataWorker.set_attr
        Inputs:   - str, attribute name (attr_name)
                  - str, attribute value (attr_val)
                  - str, HDF object path (obj_path)
        Outputs:  None.
        Features: Saves a value to a given attribute of a given object
        """
        self.logger.debug("setting attribute %s", attr_name)
        try:
            self.hdf.set_attr(attr_name, attr_val, obj_path)
        except:
            self.logger.exception(
                "could not set attribute %s", attr_name)
        else:
            self.hdf.save()

    def set_pid_fields(self, pid_path):
        """
        Name:     DataWorker.set_pid_fields
        Inputs:   str, PID path (pid_path)
        Outputs:  None.
        Features: Signals PID attribute values back to the GUI
        """
        for i in self.pid_attrs:
            f_name = self.pid_attrs[i]["qt_val"]
            f_type = self.pid_attrs[i]["qt_type"]
            attr_name = self.pid_attrs[i]["key"]
            f_value = self.get_attr(attr_name, pid_path)
            self.fieldSet.emit((f_name, f_type, f_value))

    def set_session_default(self, f_key, f_val):
        """
        Name:     DataWorker.set_session_default
        Inputs:   - str, session dictionary field key (f_key)
                  - str, session field default value (f_val)
        Outputs:  None.
        Features: Assigns a default value to session dictionary key
        """
        self.logger.debug(
            "searching for session index for key '%s'", f_key)
        k_index = [
            k for k, d in self.session_attrs.items() if d["key"] == f_key]
        if len(k_index) == 0:
            self.logger.error("session key '%s' not found", f_key)
            raise IndexError("No entry found for session key %s", f_key)
        elif len(k_index) == 1:
            i = k_index[0]
            self.logger.debug("found session index %d", i)
            self.logger.debug("setting default value to %s", f_val)
            self.session_attrs[i]["def_val"] = f_val
        else:
            self.logger.error(
                "unexpected number of session key matches for '%s'",
                f_key)
            raise IndexError(
                "Too many matches found for session key %s", f_key)

    def set_session_fields(self, s_path):
        """
        Name:     DataWorker.set_session_fields
        Inputs:   str, session path (s_path)
        Outputs:  None.
        Features: Signals session attribute values back to the GUI
        """
        for i in self.session_attrs:
            f_name = self.session_attrs[i]["qt_val"]
            f_type = self.session_attrs[i]["qt_type"]
            attr_name = self.session_attrs[i]["key"]
            f_value = self.get_attr(attr_name, s_path)
            self.fieldSet.emit((f_name, f_type, f_value))

    def stop(self):
        """
        Features: Stops this class for shutdown, resetting everything.
        """
        self.logger.info("caught stop signal; data worker class shutting down")
        self.hdf.save()
        self.hdf.close()
        self.session = None
        self.output_dir = None
        self.pid_attrs = {}


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    # Instantiating logging handler and record format:
    root_handler = logging.FileHandler("workers.log")
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)
