#!/usr/bin/python
#
# hard_worker.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-07-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software is freely available to the public for use.      #
# The Department of Agriculture (USDA) and the U.S. Government have not       #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     USDA-Agricultural Research Service                                      #
#     Robert W. Holley Center for Agriculture and Health                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os
import sys

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QObject


###############################################################################
# CLASS:
###############################################################################
class HardWorker(QObject):
    """
    Name:     HardWorker
    Features: This class is a worker object used for threading hardware.
    History:  Version 1.4.0
              - updated imaging camera getters [15.05.04]
              - fixed exception looping in run_sequence [16.05.11]
              - added mode attribute [16.06.23]
              - added mode checker [16.07.01]
              - changed mode checker's logging statement priorities [16.07.12]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    finished = pyqtSignal()           # imaging sequence complete
    running = pyqtSignal(int, tuple)  # hardware moved - update GUI
    ready = pyqtSignal(tuple)         # photo taken event - save image
    sendAlert = pyqtSignal(str)       # send error messages to GUI

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None, mutex=None):
        """
        Name:     HardWorker.__init__
        Inputs:   [optional] Qt parent object (parent)
        Returns:  None.
        Features: Class initialization
        """
        super(HardWorker, self).__init__(parent)

        # Create a worker logger:
        self.logger = logging.getLogger(__name__)
        self.logger.debug("hardware worker object initialized")

        # Create a mutex lock
        self.mutex = mutex

        # Initialize property values:
        self.is_enabled = False

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def image_number(self):
        if self.is_enabled:
            return self.imaging.num_photos
        else:
            return None

    @image_number.setter
    def image_number(self, value):  # lint:ok
        if self.is_enabled:
            self.logger.debug(
                "setting number of images to %d", value)
            self.imaging.num_photos = value
        else:
            self.logger.warning(
                "failed to set image number; hardware not enabled")

    @property
    def image_pid(self):
        if self.is_enabled:
            return self.imaging.cur_pid
        else:
            return None

    @image_pid.setter
    def image_pid(self, value):  # lint:ok
        if self.is_enabled:
            self.logger.debug(
                "setting current image PID to %s", value)
            self.imaging.cur_pid = value
        else:
            self.logger.warning(
                "failed to set image PID; hardware not enabled")

    @property
    def camera_make(self):
        if self.is_enabled:
            return self.imaging.camera_make()
        else:
            return ""

    @property
    def camera_model(self):
        if self.is_enabled:
            return self.imaging.camera_model()
        else:
            return ""

    @property
    def camera_exposure(self):
        if self.is_enabled:
            return self.imaging.camera_exposure()
        else:
            return ""

    @property
    def camera_aperture(self):
        if self.is_enabled:
            return self.imaging.camera_aperture()
        else:
            return ""

    @property
    def camera_iso_speed(self):
        if self.is_enabled:
            return self.imaging.camera_iso_speed()
        else:
            return ""

    @property
    def mode(self):
        if self.is_enabled:
            return self.imaging.mode
        else:
            return "Explorer"

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def create_imaging(self, my_parser=None, config_filepath=None):
        """
        Name:     HardWorker.create_imaging
        Inputs:   - [optional] function handle (my_parser)
                  - [optional] str, configuration file path (config_filepath)
        Outputs:  None.
        Features: Creates an Imaging class instance
        """
        if '.hardware.imaging' not in sys.modules:
            alerted = False
            try:
                from .hardware.imaging import Imaging
                from .hardware.pridaperipheral import PRIDAPeripheralError
                self.is_enabled = True
            except ImportError:
                self.is_enabled = False
            else:
                self.logger.debug("creating imaging object")
                try:
                    self.imaging = Imaging(my_parser, config_filepath)
                except PRIDAPeripheralError as PPE:
                    self.logger.exception(
                        "encountered hardware error--starting Explorer mode")
                    self.is_enabled = False
                    if PPE.critical:
                        self.sendAlert.emit(
                            '{}. Starting in Explorer Mode.'.format(str(PPE)))
                        alerted = True
            finally:
                conf_mode = self.mode_checker(config_filepath)
                if conf_mode != 'Explorer' and not self.is_enabled:
                    if not alerted:
                        self.sendAlert.emit(
                            'Hardware mode is not supported on this system! '
                            'Starting in Explorer mode.')
        else:
            if hasattr(self, 'imaging'):
                self.logger.warning('imaging object already created')
            else:
                self.logger.warning(
                    "imaging imported but not created, disabling hardware")
                self.is_enabled = False
                self.sendAlert(
                    'Something has gone wrong!\n'
                    'Please contact your system administrator. '
                    'Starting in Explorer mode.')

    def end_sequence(self):
        """
        Name:     HardWorker.end_sequence
        Inputs:   None.
        Outputs:  None.
        Features: Stops an photo capturing sequence; the next run sequence
                  should emit the finished signal
        """
        # For 5 s, try to get the mutex lock from run_sequence:
        self.logger.debug("attempting to gather mutex lock")
        got_it = self.mutex.tryLock(timeout=5000)
        if got_it:
            self.logger.debug("lock acquired, calling end sequence")
            self.imaging.end_sequence()
        else:
            self.logger.debug("failed to acquire lock, killing")
            self.imaging.end_sequence()

    def get_maximum_value(self):
        """
        Name:     HardWorker.get_maximum_value
        Inputs:   None.
        Outputs:  None.
        Features: Returns the maximum value used for the GUI progress bar or
                  zero if hardware is not enabled
        """
        if self.is_enabled:
            max_val = int(self.imaging.find_end_pos())
            max_val += int(10 * self.imaging.num_photos)
            self.logger.debug("max value calculated as %d", max_val)
        else:
            self.logger.warning(
                "cannot calculate max value; hardware not enabled")
            max_val = 0

        return max_val

    def mode_checker(self, config_filepath):
        """
        Name:     HardWorker.mode_checker
        Inputs:   str, configuration file path (config_filepath)
        Outputs:  str, configuration mode
        Features: Returns the configuration file's mode
        """
        # Initialize variables:
        conf_mode = None

        if config_filepath is not None:
            try:
                my_user = os.path.expanduser('~')
                conf_file = os.path.join(my_user, 'Prida', 'prida.config')
                if os.path.isfile(conf_file):
                    with open(conf_file, 'r') as my_config_file:
                        for line in my_config_file:
                            if 'MODE' in line:
                                words = line.split()
                                if len(words) == 3:
                                    conf_mode = eval(words[2])
            except:
                self.logger.exception(
                    "Encountered error reading configuration file %s",
                    config_filepath)

        if conf_mode is None:
            conf_mode = "Explorer"
            self.logger.info("Configuration mode defaulted to %s", conf_mode)
        else:
            self.logger.info("Configuration mode set as %s", conf_mode)
        return str(conf_mode)

    def run_sequence(self):
        """
        Name:     HardWorker.run_sequence
        Inputs:   None.
        Outputs:  None.
        Features: Run imaging sequence loop, emitting appropriate signals to
                  indicate imaging is 1) running, 2) ready, or 3) finished
        """
        if self.is_enabled:
            keep_running = True
            locked = False
            while keep_running:
                if self.mutex is not None and not locked:
                    self.mutex.lock()
                    locked = True
                try:
                    self.logger.debug("running imaging sequence")
                    output_args = self.imaging.run_sequence()
                except PRIDAPeripheralError:  # lint:ok
                    self.logger.exception("Run sequence failed")
                    self.sendAlert.emit(("Image sequence has failed! Please "
                                         "check your log statements for "
                                         "details."))
                    if self.mutex is not None:
                        locked = False
                        self.mutex.unlock()
                    self.finished.emit()
                except:
                    self.logger.exception(
                        "Encountered unknown error in run sequence")
                    self.sendAlert.emit(("Image sequence has failed! Please "
                                         "check your log statements for "
                                         "details."))
                    if self.mutex is not None:
                        locked = False
                        self.mutex.unlock()
                    self.finished.emit()
                else:
                    self.logger.debug("image sequence taken")
                    progress_val = self.imaging.step_count
                    progress_val += 10 * self.imaging.taken

                    is_path = output_args[0]
                    keep_running = self.imaging.is_running

                    if is_path and self.imaging.is_running:
                        self.logger.debug(
                            "imaging is ready - progress at %d",
                            progress_val)
                        self.running.emit(progress_val, output_args)
                        self.ready.emit(output_args)
                        if self.mutex is not None:
                            locked = False
                            self.mutex.unlock()
                    elif not is_path and self.imaging.is_running:
                        self.logger.debug(
                            "imaging is running - progress at %d",
                            progress_val)
                        self.running.emit(progress_val, output_args)
                    elif not self.imaging.is_running:
                        self.logger.debug("imaging has completed")
                        self.finished.emit()
                        if self.mutex is not None:
                            locked = False
                            self.mutex.unlock()
                    else:
                        self.logger.warning(
                            "unknown state encountered, finishing")
                        self.finished.emit()
                        if self.mutex is not None:
                            locked = False
                            self.mutex.unlock()
        else:
            self.logger.warning(
                "failed to run sequence; hardware not enabled")


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    # Instantiating logging handler and record format:
    root_handler = logging.FileHandler("workers.log")
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)
