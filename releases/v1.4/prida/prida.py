#!/usr/bin/python
#
# Prida.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-07-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software is freely available to the public for use.      #
# The Department of Agriculture (USDA) and the U.S. Government have not       #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     USDA-Agricultural Research Service                                      #
#     Robert W. Holley Center for Agriculture and Health                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import glob
import logging
import logging.handlers
import os
import sys

import numpy
import scipy.misc
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QAbstractItemView
from PyQt5.QtWidgets import QCompleter
from PyQt5.QtWidgets import QMenu
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QMovie
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QStringListModel
from PyQt5.QtCore import QThread
from PyQt5.QtCore import QSize
from PyQt5.QtCore import QDate
from PyQt5.QtCore import QMutex
from PyQt5.QtCore import Qt
import PIL.ImageQt as ImageQt
import PIL.Image as Image

from . import __version__
from .utilities import create_config_file
from .utilities import get_ctime
from .utilities import resource_path
from .utilities import read_exif_tags
from .data_worker import DataWorker
from .hard_worker import HardWorker
from .image_worker import ImageWorker
from .mainwindow import Ui_MainWindow
from .about import Ui_About
from .input_user import Ui_Dialog


###############################################################################
# GLOBAL VARIABLES:
###############################################################################
# PAGE NUMBER ENUMS FOR mainwindow.ui
MAIN_MENU = 0
VIEWER = 1
INPUT_EXP = 2
RUN_EXP = 3
FILE_SEARCH = 4
ANALYSIS = 5

# PAGE NUMBER ENUMS FOR SHEET/IDA
SHEET_VIEW = 0
IDA_VIEW = 1


###############################################################################
# FUNCTIONS:
###############################################################################
def exit_app(my_app):
    """
    Name:     exit_app
    Inputs:   object, QApplication (my_app)
    Outputs:  None
    Features: Graceful exiting of QApplication
    """
    logging.info('exiting QApplication')
    my_app.shutdown()


###############################################################################
# CLASSES:
###############################################################################
class Prida(QApplication):
    """
    Prida, a QApplication.

    Name:      Prida
    History:   Version 1.4.0
               - renamed add/rm search file functions [16.03.07]
               - created add/rm search file signals [16.03.07]
               - created update search file additons/removals [16.03.07]
               - created set field signal [16.03.07]
               - created update input field function [16.03.07]
               - updated PID autofill function [16.03.07]
               - updated edit field function [16.03.07]
                 * note there may be a slight lag between clicking edit and
                   when all the input fields are populated
               - created list data signal [16.03.07]
               - created open and new file signals [16.03.08]
               - created hdf opened socket [16.03.08]
               - moved init pid and session attr dicts to utilities [16.03.08]
               - added session list to data list [16.03.08]
               - created get/update PS dict signal and socket [16.03.09]
               - updated analysis button clicked events [16.03.09]
               - created get/set IDA signals and sockets [16.03.09]
                 * for both VIEWER and ANALYSIS views
               - deallocated IDA on to-sheet button click [16.03.09]
               - moved autofill PID function to data [16.03.09]
               - created set session default value signal [16.03.10]
               - created set attr signal [16.03.10]
               - combined import and new session functions [16.03.10]
               - added keyboard focus in edit field [16.03.10]
               - created create session function [16.03.10]
               - moved back to sheet to update data function [16.03.10]
               - created create and update about functions [16.03.10]
               - removed old analysis button connections [16.03.14]
               - clear analysis search text on back button [16.03.14]
               - reset ps dictionary on analysis button click [16.03.21]
               - removed a_search text edit [16.03.21]
               - added update analysis combos [16.03.22]
               - created set greeter function [16.03.23]
               - clear greeter when view changes [16.03.23]
               - added thread waits in shutdown [16.03.30]
               - added new analysis QComboBox [16.04.07]
               - removed invert combo box [16.04.14]
               - added analysis thread quit to shutdown [16.04.19]
               - connected send alert to receive alert [16.04.20]
               - updated to find signal with base array [16.04.21]
               - added binary and grayscale thumbnail support [16.04.22]
               - create new save analysis signal [16.04.23]
               - added file check in save analysis [16.05.02]
               - connected hardware send alert signal [16.05.04]
               - rm update sheet model from update data [16.05.11]
               - all popups and dialogs have base as parent [16.05.11]
               - added analysis gap filling push button [16.05.18]
               - moved show base view call earlier in the init [16.06.13]
               - disabled new/open/search buttons during init [16.06.13]
               - updated about ui with Prida version [16.06.23]
               - updated add search signal with two ints [16.06.23]
               - added file search start/stop spinner enum [16.06.23]
               - updated search file addition and update for spinner [16.06.23]
               - clear user input values when back to main menu [16.06.23]
               - updated prida title bar [16.06.23]
               - addressed the open file spinner issue [16.06.24]
               - import data supports files in folder or single file [16.06.24]
               - updated get input field string handling [16.06.27]
               - enabled right-click IDA save image as feature [16.06.27]
               - hardfix single imaging during 2D mode [16.06.27]
               - created prida_dir class variable [16.06.29]
               - changed editing about from help to edit [16.06.30]
               - replace QErrorMessage with QMessageBox::warning [16.06.30]
               - created to import signal in QMessage box [16.06.30]
               - created to export signal in QMessage box [16.06.30]
               - cancel on import returns to session input screen [16.06.30]
               - to import signal now carries files found list [16.06.30]
               - created notice message popup function [16.06.30]
               - created import types string [16.06.30]
               - created version checker function [16.07.01]
               - added use defaults flag for configuring [16.07.01]
               - moved reconfigure alert to notice [16.07.01]
               - moved image save from context menu to notice [16.07.01]
               - disable image count field except in 3D mode [16.07.12]
               - Windows hotfix in import data file search [16.07.12]
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    # dictionaries
    usda_plants = {}

    # signals
    addSearch = pyqtSignal(str, int, int)
    closeFile = pyqtSignal()
    createSession = pyqtSignal(str, dict, dict)
    forAnalysis = pyqtSignal(str)
    getDataList = pyqtSignal()
    getIDA = pyqtSignal(str, int)
    getObject = pyqtSignal(int)
    getSessionAttrs = pyqtSignal(str)
    getThumbs = pyqtSignal(str, int)
    imageCheck = pyqtSignal(list)
    newFile = pyqtSignal(str)
    openFile = pyqtSignal(str)
    removeSearch = pyqtSignal(str)
    removeObject = pyqtSignal(int)
    setAttr = pyqtSignal(str, str, str)
    setField = pyqtSignal(str, str, str, str)
    setPIDFields = pyqtSignal(str)
    setSessionDefault = pyqtSignal(str, str)
    setSessionFields = pyqtSignal(str)
    stopFile = pyqtSignal()
    toCrop = pyqtSignal(int, int, int, int)
    toExport = pyqtSignal()
    toFilter = pyqtSignal(int)
    toFind = pyqtSignal(numpy.ndarray)
    toGapfill = pyqtSignal(numpy.ndarray)
    toImport = pyqtSignal(list)
    toMerge = pyqtSignal(str, int, int)
    toPlot = pyqtSignal()
    toRefresh = pyqtSignal()
    toSave = pyqtSignal(dict)
    toStretch = pyqtSignal()
    toThresh = pyqtSignal()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, dict_filepath, my_parser=None, config_filepath=None):
        """
        Prida class initialization.

        Name:     Prida.__init__
        Inputs:   None.
        Outputs:  None.
        Depends:  - load_session_sheet_headers     - new_hdf
                  - new_session                    - open_hdf
                  - resize_sheet                   - show_ida
                  - to_sheet                       - update_progress
                  - update_session_sheet_model     - update_sheet_model
                  - set_defaults                   - about
                  - version_checker                - enable_all_attrs
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # Create a logger for Prida:
        self.logger = logging.getLogger(__name__)

        # Save Prida directory path:
        if config_filepath is not None:
            self.prida_dir = os.path.dirname(config_filepath)
            self.prida_conf = os.path.basename(config_filepath)
        else:
            self.prida_dir = None
            self.prida_conf = None

        # Save dictionary file path:
        self.gs_dict = dict_filepath

        # Load main window GUI to the QMainWindow:
        self.main_ui = Ui_MainWindow()
        self.logger.debug("loading the main UI file")
        self.main_ui.setupUi(self.base)
        self.view = self.main_ui

        # Load user input pop up:
        self.user_ui = Ui_Dialog()
        self.user_dialog = QDialog(self.base)
        self.user_ui.setupUi(self.user_dialog)

        # Load about pop up
        self.about_ui = Ui_About()
        self.logger.debug("loading UI file")
        self.about_dialog = QDialog(self.base)
        self.about_ui.setupUi(self.about_dialog)

        # Connect about pop up button clicked signals:
        self.about_ui.edit.clicked.connect(self.update_about)

        # Check that the necessary image files exist:
        self.logger.debug("checking for required resources")
        if os.path.isfile(resource_path("greeter.jpg")):
            self.greeter = resource_path("greeter.jpg")
        else:
            self.greeter = None
            self.logger.warning("greeter JPG not found")
        self.set_greeter()

        if os.path.isfile(resource_path("loading.gif")):
            self.spinner = resource_path("loading.gif")
        else:
            self.logger.warning("loading GIF not found")
            self.spinner = None
        self.movie = QMovie(self.spinner)

        # Show the GUI and disable buttons
        self.base.show()
        self.view.new_hdf.setEnabled(False)
        self.view.open_hdf.setEnabled(False)
        self.view.search_hdf.setEnabled(False)
        self.view.re_configure.setEnabled(False)

        # Create the Qt mutex
        self.mutex = QMutex()

        # Create data worker:
        self.logger.debug("creating data worker")
        self.data = DataWorker(mutex=self.mutex)
        self.data.create_hdf(my_parser, config_filepath)

        self.logger.debug("creating data thread")
        self.data_thread = QThread()
        self.data.moveToThread(self.data_thread)
        self.data_thread.start()
        self.data.finished.connect(self.data_thread.quit)

        # Create hardware worker:
        self.logger.debug("creating hardware worker")
        self.hardware = HardWorker(mutex=self.mutex)
        self.hardware.sendAlert.connect(self.receive_alert)

        self.logger.debug("calculating number of PID attributes")
        self.sheet_items = len(self.data.pid_attrs)

        # Connect session default value setter signal:
        self.setSessionDefault.connect(self.data.set_session_default)

        # Create the VIEWER sheet model (where PIDs and sessions are listed)
        self.logger.debug("creating sheet model")
        self.sheet_model = QStandardItemModel()
        self.view.sheet.setModel(self.sheet_model)
        self.view.sheet.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.sheet.collapsed.connect(lambda: self.resize_sheet())
        self.view.sheet.expanded.connect(lambda: self.resize_sheet())
        self.view.sheet.setAnimated(True)

        # Create the VIEWER session sheet model (for session properties)
        self.logger.debug("creating session sheet model")
        self.session_sheet_model = QStandardItemModel()
        self.view.session_sheet.setModel(self.session_sheet_model)
        # self.view.session_sheet.setRootIsDecorated(False)
        self.load_session_sheet_headers()

        # Create FILE_SEARCH sheet model (where files + PIDs are listed)
        self.logger.debug("creating search sheet model")
        self.search_sheet_model = QStandardItemModel()
        self.view.file_search_sheet.setModel(self.search_sheet_model)
        self.load_file_search_sheet_headers()

        # Create the FILE_SEARCH file browse model (for displaying files)
        self.logger.debug("creating file sheet model")
        self.file_sheet_model = QStandardItemModel()
        self.view.file_browse_sheet.setModel(self.file_sheet_model)
        self.load_file_browse_sheet_headers()

        # Create VIEWER image sheet model (for thumbnail previewer):
        self.logger.debug("creating image sheet model")
        self.img_sheet_model = QStandardItemModel()
        self.view.img_select.setModel(self.img_sheet_model)
        self.view.img_select.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.img_select.setIconSize(QSize(100, 100))

        # @TODO - make this do something meaningful?
        self.view.abortButton.setEnabled(False)
        self.view.abortButton.clicked.connect(self.data.get_data_list)

        # Attempt to initialize imaging/hardware mode:
        self.logger.debug("creating hardware's imaging")
        self.hardware.create_imaging(my_parser, config_filepath)

        self.hardware_mode = self.hardware.is_enabled
        self.logger.info("Hardware Mode == %s", self.hardware_mode)

        self.prida_title = (
            "Prida %s - %s Mode" % (__version__, self.hardware.mode))
        self.base.setWindowTitle(self.prida_title)

        # Set HARDWARE MODE options:
        if self.hardware_mode:
            self.logger.info("Hardware Mode Enabled")

            # Spawn a hardware thread if mode is enabled:
            self.logger.debug("creating hardware thread")
            self.hardware_thread = QThread()
            self.hardware_thread.started.connect(self.hardware.run_sequence)
            self.hardware_thread.finished.connect(self.end_progress)

            self.hardware.finished.connect(self.hardware_thread.quit)
            self.hardware.running.connect(self.update_progress)
            self.hardware.ready.connect(self.data.save_image)
            self.hardware.moveToThread(self.hardware_thread)

            # Disable "Import Data" button (only allow "Create Session")
            # and set VIEWER action for "OK" and "Create Session" buttons:
            self.view.import_data.setEnabled(False)
            self.view.create_session.clicked.connect(self.create_session)

            # Set default values based on the camera settings:
            cam_make = self.hardware.camera_make
            cam_model = self.hardware.camera_model
            cam_shutter = self.hardware.camera_exposure
            cam_aperture = self.hardware.camera_aperture
            cam_exposure = self.hardware.camera_iso_speed
            self.setSessionDefault.emit("cam_make", str(cam_make))
            self.setSessionDefault.emit("cam_model", str(cam_model))
            self.setSessionDefault.emit("cam_shutter", str(cam_shutter))
            self.setSessionDefault.emit("cam_aperture", str(cam_aperture))
            self.setSessionDefault.emit("cam_exposure", str(cam_exposure))

            # Default single image for 2D mode and disable editing
            if self.hardware.mode == "2D":
                self.setSessionDefault.emit("num_img", str(1))
        else:
            self.logger.info("Preview Mode Enabled")

            # Disable "Create Session" button, set VIEWER action for
            # "Import Data" button, import data to file dialog and connections:
            self.view.create_session.setEnabled(False)
            self.view.import_data.clicked.connect(self.import_data)
            self.data.importing.connect(self.update_progress)
            self.data.importDone.connect(self.end_progress)
            self.toImport.connect(self.data.import_images)
            self.imageCheck.connect(self.data.check_image_shapes)
            self.data.checkedImages.connect(self.import_images)

        # Define analysis mode:
        self.analysis_mode = True

        # Set ANALYSIS MODE options:
        if self.analysis_mode:
            # Connect analysis button:
            self.logger.info("Analysis Mode Enabled")

            # Update ANALYSIS QComboBoxes:
            self.update_analysis_combos()

            # Create image worker:
            self.logger.debug("creating image worker and thread")
            self.image = ImageWorker()
            self.analysis_thread = QThread()
            self.image.moveToThread(self.analysis_thread)

            # Connect signals and sockets with image:
            self.view.perform_analysis.clicked.connect(self.analysis)
            self.analysis_thread.started.connect(self.image.start)
            self.analysis_thread.finished.connect(self.back_to_sheet)
            self.image.sendAlert.connect(self.receive_alert)
            self.image.isFound.connect(self.update_analysis_found)
            self.image.isMerged.connect(self.update_analysis_merge)
            self.image.isReady.connect(self.show_analysis)
            self.image.setBGColor.connect(self.update_analysis_color)
            self.image.setImgRange.connect(self.update_analysis_range)
            self.image.setImgShape.connect(self.update_analysis_shape)
            self.image.updateProc.connect(self.update_analysis_progress)
            self.getObject.connect(self.image.get_element_array)
            self.removeObject.connect(self.image.remove_element)
            self.toCrop.connect(self.image.crop)
            self.toFilter.connect(self.image.median_filter)
            self.toFind.connect(self.image.find_objects)
            self.toGapfill.connect(self.image.gap_fill)
            self.toMerge.connect(self.image.merge)
            self.toPlot.connect(self.image.plot_centers)
            self.toRefresh.connect(self.image.refresh)
            self.toSave.connect(self.image.export_data)
            self.toStretch.connect(self.image.stretch)
            self.toThresh.connect(self.image.make_binary)

            # Connect signals and sockets with data:
            self.forAnalysis.connect(self.data.get_analysis_vals)
            self.data.toAnalysis.connect(self.update_analysis_view)
            self.data.datasetsListed.connect(self.image.set_merge_files)
            self.image.listDatasets.connect(self.data.get_session_datasets)
            self.data.setImg.connect(self.image.set_image)
            self.image.getProcImg.connect(self.data.get_process_img)

            # Set ANALYSIS actions:
            self.view.a_exit.clicked.connect(self.analysis_thread.quit)
            self.view.a_save.clicked.connect(self.save_analysis)
            self.view.a_crop.clicked.connect(self.run_crop)
            self.view.a_delete.clicked.connect(self.run_delete)
            self.view.a_filter.clicked.connect(self.run_filter)
            self.view.a_find.clicked.connect(self.run_find)
            self.view.a_gapfill.clicked.connect(self.run_gapfill)
            self.view.a_merge.clicked.connect(self.run_merge)
            self.view.a_refresh.clicked.connect(self.run_refresh)
            self.view.a_show.clicked.connect(self.run_show)
            self.view.a_stretch.clicked.connect(self.run_stretch)
            self.view.a_thresh.clicked.connect(self.run_thresh)
        else:
            self.logger.warning("Analysis Mode Disabled")
            self.view.perform_analysis.setEnabled(False)

        # Set MAIN_MENU actions for "New," "Open," & "Search" buttons:
        self.view.new_hdf.clicked.connect(self.new_hdf)
        self.view.open_hdf.clicked.connect(self.open_hdf)
        self.view.search_hdf.clicked.connect(self.search_hdf)
        self.view.re_configure.clicked.connect(self.reconfigure)

        # Set file open signal and sockets:
        self.newFile.connect(self.data.new_file)
        self.openFile.connect(self.data.open_file)
        self.data.fileOpened.connect(self.hdf_opened)
        self.closeFile.connect(self.data.close_file)
        self.data.fileClosed.connect(self.back_to_main)
        self.stopFile.connect(self.data.stop)

        # Set VIEWER actions for "Main Menu," "About," and "Edit" buttons
        # and for edited search:
        self.view.aboutButton.clicked.connect(self.about)
        self.view.make_edit.clicked.connect(self.edit_field)
        self.view.menuButton.clicked.connect(self.back_to_menu)
        self.view.search.textEdited.connect(self.update_sheet_model)

        # Export data to file dialog and connections:
        self.toExport.connect(self.data.extract_to_file)
        self.view.export_data.clicked.connect(self.export)

        # Connect compression and return signal
        self.view.saveButton.clicked.connect(self.data.compress_file)
        self.data.isbusy.connect(self.start_spinner)
        self.data.donebusy.connect(self.stop_spinner)
        self.data.report.connect(self.notice)

        # Custom context menu
        self.view.page.setContextMenuPolicy(Qt.CustomContextMenu)
        self.view.page.customContextMenuRequested.connect(
            self.show_context_menu)

        # Set IDA_SHEET actions for "Back to Spreadsheet," "Rotate Left," and
        # "Rotate Right" buttons:
        self.view.to_sheet.clicked.connect(self.to_sheet)
        self.view.rotateLeft.clicked.connect(self.view.ida.rotateBaseImageLeft)
        self.view.rotateRight.clicked.connect(
            self.view.ida.rotateBaseImageRight)

        # Set IDA signals and sockets:
        self.getIDA.connect(self.data.get_ida)
        self.data.idaSet.connect(self.set_ida)

        # Set INPUT_EXP action for "OK", "Cancel" button and update the image
        # orientation and exclude combo boxes:
        self.view.c_buttons.accepted.connect(self.new_session)
        self.view.c_buttons.rejected.connect(self.cancel_input)
        self.update_orient_combo()
        self.update_exclude_combo()

        # Create selection models for VIEWER sheet and img_sheet and for
        # FILE_SELECT file_browser_sheet:
        self.logger.debug("creating selection models")
        self.sheet_select_model = self.view.sheet.selectionModel()
        self.img_sheet_select_model = self.view.img_select.selectionModel()
        self.file_select_model = self.view.file_browse_sheet.selectionModel()

        # Set selection model actions for selection changes:
        self.img_sheet_select_model.selectionChanged.connect(self.show_ida)
        self.sheet_select_model.selectionChanged.connect(
            self.update_session_sheet_model
        )

        # Set FILE_SEARCH actions for "Main Menu," "+/-," and "Explore" buttons
        # and action for when search text is edited:
        self.view.menuButton2.clicked.connect(self.back_to_menu)
        self.view.add_file.clicked.connect(self.search_file_addition)
        self.view.rm_file.clicked.connect(self.search_file_removal)
        self.view.explore_file.clicked.connect(self.explore_file)
        self.view.f_search.textEdited.connect(self.update_file_search_sheet)

        # Connect FILE_SEARCH signals and sockets:
        self.addSearch.connect(self.data.search_file_addition)
        self.removeSearch.connect(self.data.search_file_removal)
        self.data.searchAdded.connect(self.update_search_additions)
        self.data.searchRemoved.connect(self.update_search_removals)

        # Create a auto-completion model for PIDs and set action for edited
        # PID text:
        self.logger.debug("creating PID auto-completion model")
        self.pid_completer = QCompleter()
        self.view.c_pid.setCompleter(self.pid_completer)
        self.pid_auto_model = QStringListModel()
        self.pid_completer.setModel(self.pid_auto_model)
        self.pid_completer.activated.connect(self.data.autofill_pid)

        # Connect signals and sockets:
        self.setField.connect(self.data.get_input_field)
        self.data.fieldSet.connect(self.update_input_field)
        self.getDataList.connect(self.data.get_data_list)
        self.data.dataListed.connect(self.update_data)
        self.getSessionAttrs.connect(self.data.get_session_attrs)
        self.data.sessionAttrs.connect(self.set_session_sheet_model)
        self.createSession.connect(self.data.create_session)
        self.getThumbs.connect(self.data.get_thumbs)
        self.data.addThumb.connect(self.set_sheet_thumbs)
        self.setPIDFields.connect(self.data.set_pid_fields)
        self.setSessionFields.connect(self.data.set_session_fields)
        self.setAttr.connect(self.data.set_attr)

        # Create an auto-completion model for dictionary fields:
        self.logger.debug("creating dictionary auto-completion model")
        self.dict_completer = QCompleter()
        self.view.c_gen_sp.setCompleter(self.dict_completer)
        self.dict_auto_model = QStringListModel()
        self.dict_completer.setModel(self.dict_auto_model)
        self.dict_completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.dict_completer.activated.connect(self.autofill_dict)

        # Create an auto-completion model for USDA PLANTS:
        self.logger.debug("creating USDA PLANTS auto-completion model")
        self.plants_completer = QCompleter()
        self.view.c_usda_plants.setCompleter(self.plants_completer)
        self.plants_auto_model = QStringListModel()
        self.plants_completer.setModel(self.plants_auto_model)
        self.plants_completer.activated.connect(self.autofill_plants)

        # Build the USDA PLANTS database and update the auto-completion models:
        self.build_plants_db()
        self.update_dict_auto_model()

        # Initialize the search dictionaries, list of PID metadata, list of
        # thumbnail images, session create/edit boolean, input field defaults,
        # whether an image is being previewed, and the current image:
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.img_list = []
        self.img_types = (".jpg", ".JPG", ".jpeg", ".tif", ".tiff")
        self.import_types = 'Images (*.jpg *.JPG *.jpeg *.tif *.tiff)'
        self.editing = False
        self.preview = False
        self.current_img = None        # not currently used
        self.a_session = None          # analysis session path
        self.attr_dict = {}            # confiurable parameter dictionary

        # Enable the appropriate input fields:
        # NOTE: needs to be after editing flag is defined
        self.enable_all_attrs()

        # Set exit register:
        atexit.register(exit_app, self)

        # Check configuration file version against software:
        use_defaults = self.version_checker(config_filepath)

        # Parse configuration file is version is okay:
        if not use_defaults:
            if my_parser is not None and config_filepath is not None:
                my_parser(self, __name__, config_filepath)

        # Re-Enable Buttons
        self.view.new_hdf.setEnabled(True)
        self.view.open_hdf.setEnabled(True)
        self.view.search_hdf.setEnabled(True)
        self.view.re_configure.setEnabled(True)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def about(self):
        """
        Name:     Prida.about
        Inputs:   None.
        Outputs:  None.
        Features: Produces the file's About popup dialog box
        """
        self.logger.info("Button Clicked")
        self.logger.debug("getting about info")
        overview = self.data.about

        # Set popup values to default string if about fails:
        self.logger.debug("assigning about fields")
        self.about_ui.version_about.setText("%s" % (__version__))
        self.about_ui.author_about.setText(overview.get("Author", "Unknown"))
        self.about_ui.contact_about.setText(overview.get("Contact", "Unknown"))
        self.about_ui.plants_about.setText(overview.get("Plants", "?"))
        self.about_ui.sessions_about.setText(overview.get("Sessions", "?"))
        self.about_ui.photos_about.setText(overview.get("Photos", "?"))
        self.about_ui.summary_about.setText(overview.get("Summary", "?"))
        self.about_dialog.setWindowTitle("About: %s" % (self.data.basename))
        if self.about_dialog.exec_():
            self.about_dialog.update()

    def analysis(self):
        """
        Name:     Prida.analysis
        Inputs:   None.
        Outputs:  None.
        Features: Signals analysis view updates; clears ANALYSIS view
                  previews, lists and labels; changes current view to ANALYSIS,
                  and starts the analysis thread
        Depends:  - mayday
        """
        self.logger.info("Button Clicked")
        if self.sheet_select_model.hasSelection():
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0])

            if my_selection.parent() is not None:
                # Get session path:
                pid = my_selection.parent().text()
                session = my_selection.text()
                s_path = "/%s/%s" % (pid, session)
                self.a_session = s_path
                self.view.a_preview.clearBaseImage("Image Display Area")
                self.view.a_label.clear()
                self.view.stackedWidget.setCurrentIndex(ANALYSIS)
                self.forAnalysis.emit(s_path)
                self.logger.debug("starting analysis thread")
                self.analysis_thread.start()
            else:
                self.mayday("Please select a session to begin.")
                self.a_session = None
        else:
            self.mayday("Please select a session to begin.")
            self.a_session = None

    def autofill_dict(self, value=''):
        """
        Name:     Prida.autofill_dict
        Inputs:   [optional] str, Genus species (value)
        Outputs:  None.
        Features: Autofills the USDA PLANTS ID for a given Genus sp.
        Depends:  set_input_field
        """
        for k, v in self.usda_plants.items():
            if value == v:
                try:
                    self.set_input_field("c_usda_plants", "QLineEdit", str(k))
                except:
                    self.logger.exception(
                        "could not set QLineEdit c_usda_plants")

    def autofill_plants(self, key=''):
        """
        Name:     Prida.autofill_plants
        Inputs:   [optional] str, USDA PLANTS ID (key)
        Outputs:  None.
        Features: Autofills the Genus species for a given USDA PLANTS ID
        Depends:  set_input_field
        """
        if key in self.usda_plants.keys():
            try:
                self.set_input_field(
                    "c_gen_sp", "QLineEdit", str(self.usda_plants[key]))
            except:
                self.logger.exception("could not set QLineEdit c_gen_sp")

    def back_to_main(self):
        """
        Name:     Prida.back_to_main
        Inputs:   None
        Outputs:  None
        Features: Returns to the menu screen, closing the HDF5 file handle and
                  clearing session and sheet data.
        Depends:  - clear_session_sheet_model
                  - init_pid_attrs
                  - init_session_attrs
                  - update_file_browse_sheet
                  - update_file_search_sheet
                  - update_sheet_model
        """
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.editing = False
        self.image.reset()
        self.user_ui.user.setText("")
        self.user_ui.email.setText("")
        self.user_ui.about.setText("")
        self.update_sheet_model()
        self.update_file_browse_sheet()
        self.update_file_search_sheet()
        self.set_greeter()
        self.view.stackedWidget.setCurrentIndex(MAIN_MENU)
        self.logger.info("view changed to MAIN_MENU")
        self.base.setWindowTitle(self.prida_title)

    def back_to_menu(self):
        """
        Name:     Prida.back_to_menu
        Inputs:   None
        Outputs:  None
        Features: Signals data to close.
        """
        self.logger.info("Button Clicked")
        self.logger.info("signaling data to close file")
        self.closeFile.emit()

    def back_to_sheet(self):
        """
        Name:     Prida.back_to_sheet
        Inputs:   None
        Outputs:  None
        Features: Clears session and image sheets in VIEWER, clears IDAs,
                  updates the sheet model, and changes current view to VIEWER
        Depends:  - clear_session_sheet_model
                  - update_sheet_model
        """
        self.logger.debug("returning back to sheet")
        self.view.a_preview.clearBaseImage("Image Display Area")
        self.view.welcome.clearBaseImage('Welcome to Prida')
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.update_sheet_model()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.logger.info("view changed to SHEET_VIEW")
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.logger.info("view changed to VIEWER")
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def build_plants_db(self):
        """
        Name:     Prida.build_plants_db
        Inputs:   None.
        Outputs:  None.
        Features: Reads the dictionary file and parses contents into the
                  usda_plants dictionary; if no USDA PLANTS IDs found,
                  assumes generic numbering

        @TODO:    add mayday for missing or bad dictionary entries
        """
        unk_idx = 0
        try:
            with open(self.gs_dict) as f:
                my_dict = f.read().splitlines()
        except IOError:
            self.logger.warning("dictionary file not found")
        else:
            self.logger.info("read file contents to dictionary list")
            for item in my_dict:
                my_items = item.split(":")
                if len(my_items) == 2:
                    try:
                        my_keys = eval("%s" % my_items[0])
                    except NameError:
                        self.logger.exception(
                            "bad dictionary file entry '%s', check formatting",
                            item)
                    else:
                        for key in my_keys:
                            self.logger.debug("adding '%s' to database", key)
                            self.usda_plants[str(key)] = str(my_items[1])
                else:
                    self.logger.debug(
                        "USDA PLANTS ID not found, adding '%s' to database",
                        unk_idx)
                    self.usda_plants[str(unk_idx)] = item
                    unk_idx += 1

    def cancel_input(self):
        """
        Name:     Prida.cancel_input
        Inputs:   None
        Outputs:  None
        Features: Exits INPUT_EXP, resetting editing boolean, enabling all
                  QLineEdit fields, and returns to VIEWER
        Depends:  enable_all_attrs
        """
        self.logger.info("Button Clicked")
        self.editing = False
        self.enable_all_attrs()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.logger.info("view changed to SHEET_VIEW")
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.logger.info("view changed to VIEWER")
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def clear_session_sheet_model(self):
        """
        Name:     Prida.clear_session_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Clears session sheet model
        Depends:  resize_session_sheet
        """
        self.logger.debug("clearing session sheet")
        for i in sorted(list(self.data.session_attrs.keys())):
            self.session_sheet_model.setItem(i, 1, QStandardItem(''))

        self.resize_session_sheet()

    def create_session(self):
        """
        Name:     Prida.create_session
        Inputs:   None
        Outputs:  None
        Features: Changes view to INPUT_EXP with a focus on the PID field
        """
        self.logger.info("view changed to INPUT_EXP")
        self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
        self.view.c_pid.setFocus()

    def disable_pid_attrs(self):
        """
        Name:     Prida.disable_pid_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with PID attributes
        Depends:  enable_all_attrs
        """
        self.enable_all_attrs()
        if self.editing:
            self.logger.debug("disabling INPUT_EXP fields")
            self.view.c_pid.setEnabled(False)
            self.view.c_num_images.setEnabled(False)
            for k in self.data.pid_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.data.pid_attrs[k]["qt_val"]))

    def disable_session_attrs(self):
        """
        Name:     Prida.disable_session_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with session attributes
        Depends:  enable_all_attrs
        """
        self.enable_all_attrs()
        if self.editing:
            self.logger.debug("disabling INPUT_EXP fields")
            self.view.c_pid.setEnabled(False)
            for k in self.data.session_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.data.session_attrs[k]["qt_val"]))

    def edit_field(self):
        """
        Name:     Prida.edit_field
        Inputs:   None
        Outputs:  None
        Features: Performs INPUT_EXP field disabling based on the selection in
                  the sheet model, signals data to set the appropriate input
                  fields, sets editing boolean to True and changes view to
                  INPUT_EXP
        Depends:  - disable_pid_attrs
                  - disable_session_attrs
                  - set_defaults
                  - set_input_field
        """
        self.logger.info("Button Clicked")
        if self.sheet_select_model.hasSelection():
            self.logger.debug("set editing to True")
            self.editing = True
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session, disable PID attrs and set focus on user
                self.disable_pid_attrs()
                self.view.c_session_user.setFocus()

                # Get session attrs & set them to INPUT_EXP:
                pid = my_selection.parent().text()
                session = my_selection.text()
                p_path = "/%s" % (pid)
                s_path = "/%s/%s" % (pid, session)

                self.logger.debug("setting INPUT_EXP fields for PID")
                self.set_input_field("c_pid", "QLineEdit", str(pid))
                self.setPIDFields.emit(p_path)

                self.logger.debug("setting INPUT_EXP fields for session")
                self.setSessionFields.emit(s_path)
            else:
                # Selected PID, disable session attrs and set focus to ID
                self.disable_session_attrs()
                self.view.c_usda_plants.setFocus()

                # Get PID attrs & set them to INPUT_EXP:
                pid = my_selection.text()
                p_path = "/%s" % (pid)

                self.set_defaults()
                self.logger.debug("setting INPUT_EXP fields for PID")
                self.set_input_field("c_pid", "QLineEdit", str(pid))
                self.setPIDFields.emit(p_path)
            self.logger.info("view changed to INPUT_EXP")
            self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
        else:
            err_msg = "Nothing selected!"
            self.mayday(err_msg)

    def enable_all_attrs(self):
        """
        Name:     Prida.enable_all_attrs
        Inputs:   None
        Outputs:  None
        Features: Enables all INPUT_EXP fields, except image count unless in
                  3D mode
        """
        self.logger.debug("enabling all INPUT_EXP fields")
        self.view.c_pid.setEnabled(True)
        for k in self.data.pid_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.data.pid_attrs[k]["qt_val"]))

        for j in self.data.session_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.data.session_attrs[j]['qt_val']))

        # NOTE: disable user editing of image count field
        #       unless capturing photos in 3D mode
        self.view.c_num_images.setEnabled(False)
        if self.hardware.mode == '3D' and not self.editing:
            self.logger.debug("Imaging in 3D mode; allow image count edits")
            self.view.c_num_images.setEnabled(True)

    def end_progress(self):
        """
        Name:     Prida.end_progress
        Inputs:   None.
        Outputs:  None.
        Features: Clears preview image, resets progress bar, gathers data, and
                  returns to sheet view.
        """
        self.logger.debug("imaging done")
        self.view.c_preview.clear()
        self.view.c_progress.reset()
        self.preview = False
        self.logger.info("sending get data list signal")
        self.getDataList.emit()
        self.base.statusBar().showMessage("Imaging complete!", 2000)

    def explore_file(self):
        """
        Name:     Prida.explore_file
        Inputs:   None.
        Outputs:  None.
        Features: Checks for user selection and signals to data for opening
        Depends:  - start_spinner
                  - mayday
        """
        self.logger.info("Button Clicked")
        if self.file_select_model.hasSelection():
            self.logger.debug("gathering selection details")
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0]
            )
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            self.logger.debug("opening file %s", s_name)
            self.start_spinner("Opening file ...")
            self.openFile.emit(f_path)
        else:
            err_msg = "Nothing selected!"
            self.mayday(err_msg)

    def export(self):
        """
        Name:     Prida.export
        Inputs:   None.
        Outputs:  None.
        Features: Exports the images from a given selection to a user-defined
                  output directory recreating the HDF5 folder structure, else
                  exports all images and meta data
        Depends:  get_sheet_model_selection
        """
        self.logger.info("Button Clicked")
        self.logger.debug("gathering user selection")
        if self.sheet_select_model.hasSelection():
            self.logger.debug("getting user selection")
            s_path = self.get_sheet_model_selection()
        else:
            self.logger.debug("no selection, exporting from root")
            s_path = "/"

        self.logger.debug("requesting output directory from user")
        path = QFileDialog.getExistingDirectory(self.base,
                                                "Select an output directory:",
                                                os.path.expanduser("~"),
                                                QFileDialog.ShowDirsOnly)
        self.data.session = s_path
        self.data.output_dir = path
        self.toExport.emit()

    def get_input_field(self, field_name, field_type):
        """
        Name:     Prida.get_input_field
        Inputs:   - str, Qt field name (field_name)
                  - str, Qt field type (field_type)
        Outputs:  str, current input text (field_value)
        Features: Get the current text from a Qt input box
        Raises:   NameError
        """
        err_msg = "could not get %s %s value" % (field_type, field_name)

        if field_type == "QComboBox":
            try:
                field_value = eval("self.view.%s.currentText()" % (field_name))
            except:
                self.logger.exception(err_msg)
                raise NameError(err_msg)
            else:
                self.logger.debug("read %s from %s %s" % (
                    field_value, field_type, field_name))
        elif (field_type == "QDateEdit" or
              field_type == "QLineEdit" or
              field_type == "QSpinBox"):
            # All three fields use text() getter
            try:
                field_value = eval("self.view.%s.text()" % (field_name))
            except:
                self.logger.exception(err_msg)
                raise NameError(err_msg)
            else:
                self.logger.info("read %s from %s %s" % (
                    field_value, field_type, field_name))
        else:
            self.logger.error(err_msg)
            raise NameError(err_msg)

        # Handle some forbidden characters:
        field_value = field_value.replace("\\", "\\\\")
        field_value = field_value.replace("\"", "'")
        return str(field_value)

    def get_sheet_model_selection(self):
        """
        Name:     Prida.get_sheet_model_selection
        Inputs:   None
        Outputs:  str, HDF5 group path based on selection (s_path)
        Features: Returns sheet model selection path for HDF5 file
        """
        if self.sheet_select_model.hasSelection():
            self.logger.debug("gathering user selection")
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session
                pid = my_selection.parent().text()
                session = my_selection.text()
                s_path = "/%s/%s" % (pid, session)
                self.logger.debug("returning session %s", s_path)
            else:
                # Selected PID
                pid = my_selection.text()
                s_path = "/%s" % (pid)
                self.logger.debug("returning PID %s", s_path)

            return s_path

    def hdf_opened(self, err_val):
        """
        Name:     Prida.hdf_opened
        Inputs:   int, error value (err_val)
                  * 0: new file
                  * 1: error
                  * -1: existing file
        Outputs:  None.
        Features: Catches signal from data after an HDF5 file is opened; reads
                  data, updates PID autofill model, sets window title, and
                  returns to VIEW
        Depends:  - back_to_menu
                  - get_data
                  - set_about
                  - set_defaults
                  - mayday
        """
        self.logger.info("caught file opened signal with code %d", err_val)

        if err_val == 0:
            self.logger.debug("HDF5 file created")
            self.set_about()

            self.logger.info("sending get data list signal")
            self.getDataList.emit()

            self.logger.debug('updating window title')
            self.base.setWindowTitle(
                "%s: %s" % (self.prida_title, self.data.basename)
            )
            self.set_defaults()
            self.base.statusBar().showMessage("New file created!", 2000)
        elif err_val == -1:
            self.logger.debug("HDF5 file opened")
            self.logger.info("sending get data list signal")
            self.getDataList.emit()

            self.logger.debug('updating window title to %s', self.prida_title)
            self.base.setWindowTitle(
                "%s: %s" % (self.prida_title, self.data.basename))
            self.set_defaults()
            self.stop_spinner()
            self.base.statusBar().showMessage("File opened!", 2000)
        elif err_val == 1:
            self.logger.warning("open file failed")
            self.logger.debug("returning back to menu")
            self.back_to_menu()
            self.mayday("Could not open the selected file.")
        else:
            self.logger.warning("unknown error encountered")
            self.logger.debug("returning back to menu")
            self.back_to_menu()
            self.mayday("An unknown error was encountered during file open!")

    def import_data(self):
        """
        Name:     Prida.import_data
        Inputs:   None.
        Outputs:  None.
        Features: Reads a user-defined directory for images, saves images to
                  search_files dictionary, attempts to extract image exif tags
                  and defines them in INPUT_EXP view; for multi-file import,
                  emits signal for data consistency check
        Depends:  - get_ctime
                  - read_exif_tags
                  - set_defaults
                  - set_input_field
        """
        self.logger.info("Button Clicked")
        self.logger.debug("requesting directory from user")

        # Ask user if files are in a directory?
        reply = QMessageBox.question(
            self.base,
            "Image Import",
            "Are your images stored in a directory?")
        if reply == QMessageBox.No:
            self.logger.debug("QMessageBox: user selected 'No'")
            my_file = QFileDialog.getOpenFileName(
                self.base,
                "Select File",
                os.path.expanduser("~"),
                (self.import_types))[0]
            self.logger.debug("user selected file %s", my_file)
            if os.path.isfile(my_file):
                self.logger.debug("importing 1 file")
                self.logger.debug("resetting search file dictionary")
                self.search_files = {}
                try:
                    self.logger.debug("opening image %s", my_file)
                    img = Image.open(my_file)
                except:
                    self.logger.exception("failed to open %s", my_file)
                    raise IOError("Error! Could not open image")
                else:
                    self.search_files[my_file] = 0
                    exif = read_exif_tags(img)

                    # If DateTime is empty, try last modified time:
                    if exif["DateTime"] == "":
                        try:
                            self.logger.debug("reading file modification date")
                            my_dt = get_ctime(my_file).strftime("%Y-%m-%d")
                        except:
                            self.logger.debug("no datetime exif tag")
                        else:
                            exif["DateTime"] = my_dt

                    exif["NumPhotos"] = "1"

                    # Save the exif tag values to INPUT_EXP fields, set
                    # keyboard focus to PID and change view to INPUT_EXP
                    self.save_exif_tags(exif)
                    self.view.c_pid.setFocus()
                    self.logger.info("view changed to INPUT_EXP")
                    self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
            else:
                self.mayday("Please select a file for importing.")
                self.logger.warning("could not find file %s", my_file)
        elif reply == QMessageBox.Yes:
            self.logger.debug("QMessageBox: user selected 'Yes'")
            path = QFileDialog.getExistingDirectory(
                self.base,
                "Select directory containing images:",
                os.path.expanduser("~"),
                QFileDialog.ShowDirsOnly
            )
            if os.path.isdir(path):
                # Populate default values and overwrite where possible:
                self.set_defaults()

                # Currently filters search for only jpg and tif image files
                self.logger.debug("searching directory for images")
                my_files = []
                for img_type in self.img_types:
                    s_path = os.path.join(path, "*%s" % (img_type))
                    my_files += glob.glob(s_path)
                my_files = sorted(list(set(my_files)))
                files_found = len(my_files)
                self.logger.info("importing %d files", files_found)

                if files_found > 0:
                    # Check for consistent image shapes:
                    self.start_spinner("Validating import images ...")
                    self.imageCheck.emit(my_files)
                else:
                    self.mayday("No images found in directory.")
                    self.logger.warning("no images found in %s", path)

    def import_images(self, my_files, check_passed):
        """
        Name:     Prida.import_images
        Inputs:   - list, image file list (my_files)
                  - bool, consistency check (check_passed)
        Outputs:  None.
        Features: Catches consistency check signal from data and continues the
                  image import, gathering exif tags where and when possible
        Depends:  - read_exif_tags
                  - stop_spinner
                  - get_ctime
        """
        self.stop_spinner()
        answ = QMessageBox.No
        if not check_passed:
            q = ("One or more images from the directory you "
                 "selected are oriented or sized differently. "
                 "Do you want to continue?")
            answ = QMessageBox.question(self.base, "Consistency check!", q)
        if check_passed or answ == QMessageBox.Yes:
            # Reset file names for each processing:
            self.logger.debug("resetting search file dictionary")
            self.search_files = {}

            self.logger.debug("reading images found")
            for my_file in my_files:
                try:
                    self.logger.debug("opening image %s", my_file)
                    img = Image.open(my_file)
                except:
                    self.logger.exception("failed to open %s", my_file)
                    raise IOError("Error! Could not open image")
                else:
                    self.search_files[my_file] = 0
                    exif = read_exif_tags(img)

                    # If DateTime is empty, try last modified time:
                    if exif["DateTime"] == "":
                        try:
                            self.logger.debug("reading file modification date")
                            my_dt = get_ctime(my_file).strftime("%Y-%m-%d")
                        except:
                            self.logger.debug("no datetime exif tag")
                        else:
                            exif["DateTime"] = my_dt

            exif["NumPhotos"] = str(len(my_files))

            # Save the exif tag values to INPUT_EXP fields, set
            # keyboard focus to PID and change view to INPUT_EXP
            self.save_exif_tags(exif)
            self.view.c_pid.setFocus()
            self.logger.info("view changed to INPUT_EXP")
            self.view.stackedWidget.setCurrentIndex(INPUT_EXP)

    def load_file_browse_sheet_headers(self):
        """
        Name:     Prida.load_file_browse_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the file_sheet_model
        Depends:  resize_file_browse_sheet
        """
        self.logger.debug('loading headers')
        self.file_sheet_model.setColumnCount(2)
        self.file_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File Name'))
        self.file_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Path'))
        self.resize_file_browse_sheet()

    def load_file_search_sheet_headers(self):
        """
        Name:     Prida.load_file_search_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the search_sheet_model
        Depends:  resize_search_sheet
        """
        self.logger.debug('loading headers')
        self.search_sheet_model.setColumnCount(self.sheet_items + 1)
        self.search_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File'))
        self.search_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('PID'))
        for i in self.data.pid_attrs:
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.search_sheet_model.setHorizontalHeaderItem", (i + 1),
                self.data.pid_attrs[i]['title']))
        self.resize_search_sheet()

    def load_session_sheet_headers(self):
        """
        Name:     Prida.load_session_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the session_sheet_model
        Depends:  resize_session_sheet
        """
        self.logger.debug('loading headers')
        self.session_sheet_model.setColumnCount(2)
        self.session_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('Property'))
        self.session_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Value'))

        # Define session sheet rows in column 1:
        for i in self.data.session_attrs:
            exec("%s(%d, 0, QStandardItem('%s'))" % (
                "self.session_sheet_model.setItem", i,
                self.data.session_attrs[i]['title']))
        self.resize_session_sheet()

    def load_sheet_headers(self):
        """
        Name:     Prida.load_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the sheet_model
        Depends:  resize_sheet
        """
        self.logger.debug('loading %d headers', self.sheet_items)
        self.sheet_model.setColumnCount(self.sheet_items)
        self.sheet_model.setHorizontalHeaderItem(0, QStandardItem('PID'))
        for i in self.data.pid_attrs:
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.sheet_model.setHorizontalHeaderItem", i,
                self.data.pid_attrs[i]['title']))
        self.resize_sheet()

    def mayday(self, msg):
        """
        Name:     Prida.mayday
        Inputs:   str, warning message (msg)
        Outputs:  None.
        Features: Opens a popup window displaying the warning text
        """
        QMessageBox.warning(self.base, "Warning", msg)

    def new_hdf(self):
        """
        Name:     Prida.new_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Prompts for user information, prompts for new HDF5 file name,
                  and signals to data for opening
        """
        self.logger.info('Button Clicked')
        self.logger.debug('requesting new file name from user')
        path = QFileDialog.getSaveFileName(self.base,
                                           "Save File",
                                           os.path.expanduser("~"))[0]
        if path != '':
            if os.path.splitext(path)[1] == '':
                path += '.hdf5'
                if os.path.isfile(path):
                    self.mayday('File already exists.'
                                ' Please select a different name.')
                else:
                    self.logger.debug("sending new file signal")
                    self.newFile.emit(path)
                    self.base.statusBar().showMessage("Opening file ...", 2000)
            elif os.path.splitext(path)[1] != '.hdf5':
                self.mayday('Only hdf5 files are permitted.')
            else:
                if os.path.isfile(path):
                    self.mayday('File already exists.'
                                ' Please select a different name.')
                else:
                    self.logger.debug("sending new file signal")
                    self.newFile.emit(path)
                    self.base.statusBar().showMessage("Opening file ...", 2000)

    def new_session(self):
        """
        Name:     Prida.new_session
        Inputs:   None.
        Outputs:  None
        Features: Saves attribute edits or creates a new session in hardware
                  mode, resets the progress bar, enters the RUN_EXP screen, and
                  begins the hardware thread
        Depends:  Editing*                  Hardware Mode*
                  - save_edits              - get_input_field
                  - enable_all_attrs        Preview Mode*
                  - set_defaults            - get_input_field
        """
        self.logger.info('Button Clicked')
        if self.editing:
            self.logger.info('Saving Edits')
            self.save_edits()
            self.logger.debug('set editing to False')
            self.editing = False
            self.enable_all_attrs()
            self.set_defaults()
            self.logger.info("sending get data list signal")
            self.getDataList.emit()
        else:
            # Get user defined PID:
            pid = str(self.view.c_pid.text())

            # Create the PID metadata dictionary:
            self.logger.debug('building PID dictionary')
            pid_dict = {}
            for i in self.data.pid_attrs:
                f_key = self.data.pid_attrs[i]['key']
                f_name = self.data.pid_attrs[i]['qt_val']
                f_type = self.data.pid_attrs[i]['qt_type']
                f_val = self.get_input_field(f_name, f_type)
                pid_dict[f_key] = f_val

            # Create the session metadata dictionary:
            self.logger.debug('building session dictionary')
            session_dict = {}
            for j in self.data.session_attrs:
                f_key = self.data.session_attrs[j]['key']
                f_name = self.data.session_attrs[j]['qt_val']
                f_type = self.data.session_attrs[j]['qt_type']
                f_val = self.get_input_field(f_name, f_type)
                session_dict[f_key] = f_val

            if self.hardware_mode:
                # Imaging ...
                self.logger.info("view changed to RUN_EXP")
                self.view.stackedWidget.setCurrentIndex(RUN_EXP)
                self.view.stackedWidget.show()

                self.logger.info("signaling for new session")
                self.createSession.emit(pid, pid_dict, session_dict)

                self.logger.debug("updating hardware")
                self.hardware.image_number = int(self.view.c_num_images.text())
                self.hardware.image_pid = str(self.view.c_pid.text())

                self.logger.debug("preparing progress bar")
                progress_max = self.hardware.get_maximum_value()
                self.view.c_preview.clear()
                self.view.c_progress.setMinimum(0)
                self.view.c_progress.setMaximum(progress_max)

                self.logger.info("starting hardware thread")
                self.hardware_thread.start()
                self.base.statusBar().showMessage(
                    "Capturing images ...", 2000)
            else:
                # Importing ...
                self.logger.debug("calculating files for import")
                files_found = sorted(list(self.search_files.keys()))
                num_files = len(files_found)
                if num_files > 0:
                    self.logger.info("signaling for new session")
                    self.createSession.emit(pid, pid_dict, session_dict)

                    self.logger.debug("preparing progress bar")
                    self.view.c_progress.setMinimum(0)
                    self.view.c_progress.setMaximum(num_files - 1)

                    # Set appropriate data variables for processing:
                    msg = "Click OK to begin importing %d images." % (
                        num_files)
                    resp = QMessageBox.information(
                        self.base,
                        "Import Images",
                        msg,
                        buttons=QMessageBox.Ok | QMessageBox.Cancel,
                        defaultButton=QMessageBox.Ok)
                    if resp == QMessageBox.Ok:
                        self.logger.info("view changed to RUN_EXP")
                        self.view.stackedWidget.setCurrentIndex(RUN_EXP)
                        self.view.stackedWidget.show()
                        self.logger.debug("User selected OK")
                        self.toImport.emit(files_found)
                    else:
                        self.logger.debug("User selected cancel")
                else:
                    self.mayday("No images found for importing.")
                    self.logger.info("sending get data list signal")
                    self.getDataList.emit()

    def notice(self, msg):
        """
        Name:     Prida.notice
        Inputs:   str, notification message (msg)
        Outputs:  None.
        Features: Opens a popup window displaying the notification text
        """
        QMessageBox.information(self.base, "Notice", msg)

    def open_hdf(self):
        """
        Name:     Prida.open_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Prompts user for an existing HDF5 file and signals to data
                  for opening
        Depends:  start_spinner
        """
        self.logger.info("Button Clicked")
        self.logger.debug("prompting user for existing file")
        path = QFileDialog.getOpenFileName(self.base,
                                           "Select File",
                                           os.path.expanduser("~"),
                                           filter='*.hdf5')[0]
        self.logger.debug("user selected file %s", path)
        if path != '':
            if os.path.isfile(path):
                self.logger.debug("opening file %s", path)
                self.start_spinner("Opening file ...")
                self.openFile.emit(path)
                self.base.statusBar().showMessage("Opening file ...", 2000)
            else:
                self.mayday("Please select an existing file for opening.")

    def receive_alert(self, msg):
        """
        Name:     Prida.receive_alert
        Inputs:   str, alert message (msg)
        Outputs:  None.
        Features: Stops analysis spinner and displays alert message
        Depends:  - mayday
                  - stop_spinner
        """
        self.stop_spinner(ANALYSIS)
        self.mayday(msg)

    def reconfigure(self):
        """
        Name:     Prida.reconfigure
        Inputs:   None.
        Outputs:  None.
        Features: Replaces the configuration file in the Prida directory
        Depends:  - create_config_file
                  - notice
        """
        self.logger.info("Button Clicked")
        if self.prida_dir is not None:
            try:
                create_config_file(self.prida_dir, self.prida_conf)
            except IOError:
                self.logger.exception("Failed to create new config file")
            except:
                self.logger.exception("Encountered unexpected error")
            else:
                self.notice(
                    ("A new configuration file was created in %s. "
                     "Please exit this application and relaunch to read "
                     "changes in the configure file.") % (
                        os.path.join(self.prida_dir, self.prida_conf)))

    def resize_file_browse_sheet(self):
        """
        Name:     Prida.resize_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_browse_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.file_browse_sheet.resizeColumnToContents(col)

    def resize_search_sheet(self):
        """
        Name:     Prida.resize_search_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_search_sheet
        """
        self.logger.debug('resizing')
        for col in range(self.sheet_items + 2):
            self.view.file_search_sheet.resizeColumnToContents(col)

    def resize_session_sheet(self):
        """
        Name:     Prida.resize_session_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the session_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.session_sheet.resizeColumnToContents(col)

    def resize_sheet(self):
        """
        Name:     Prida.resize_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the sheet
        """
        self.logger.debug("resizing")
        for col in range(self.sheet_items + 1):
            self.view.sheet.resizeColumnToContents(col)

    def run_crop(self):
        """
        Name:     Prida.run_crop
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to crop the process image
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Cropping ...", ANALYSIS)
        a_top = self.view.a_crop_top.value()
        a_bot = self.view.a_crop_bottom.value()
        a_left = self.view.a_crop_left.value()
        a_right = self.view.a_crop_right.value()
        self.toCrop.emit(a_top, a_bot, a_left, a_right)

    def run_delete(self):
        """
        Name:     Prida.run_delete
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to delete the selected element
        """
        self.logger.info("Button Clicked")

        try:
            obj_data = self.view.a_element_combo.currentData()
            self.removeObject.emit(obj_data)
        except:
            self.logger.exception("Failed to delete object")
            self.mayday("Are your UI files up-to-date?")
        else:
            if obj_data >= 0:
                self.start_spinner("Deleting 1 object ...", ANALYSIS)
            else:
                self.start_spinner("Deleting all objects ...", ANALYSIS)

    def run_filter(self):
        """
        Name:     Prida.run_filter
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to filter the process image
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Filtering ...", ANALYSIS)
        a_kern = self.view.a_kernel_spin.value()
        self.toFilter.emit(a_kern)

    def run_find(self):
        """
        Name:     Prida.run_find
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to find calibration objects in the preview
                  image
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Searching ...", ANALYSIS)
        self.toFind.emit(self.view.a_preview.base_array[:, :, 0])

    def run_gapfill(self):
        """
        Name:     Prida.run_gapfill
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to gapfill the preview image
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Gapfilling ...", ANALYSIS)
        self.toGapfill.emit(self.view.a_preview.base_array[:, :, 0])

    def run_merge(self):
        """
        Name:     Prida.run_merge
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to begin merge for analysis session
        """
        self.logger.info("Button Clicked")
        self.view.a_preview.clearBaseImage("Image Display Area")

        self.start_spinner("Processing ...", ANALYSIS)
        self.logger.debug("gathering process values")
        a_bg = self.view.a_bgcolor_combo.currentData()
        a_sf = self.view.a_scale_combo.currentData()
        self.toMerge.emit(self.a_session, a_bg, a_sf)

    def run_refresh(self):
        """
        Name:     Prida.run_refresh
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to refresh merge image
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Refreshing ...", ANALYSIS)
        self.toRefresh.emit()

    def run_show(self):
        """
        Name:     Prida.run_show
        Input:    None.
        Output:   None.
        Features: Signals to image to show selected object
        """
        self.logger.info("Button Clicked")
        try:
            obj_data = self.view.a_element_combo.currentData()
            self.getObject.emit(obj_data)
        except:
            self.logger.exception("Failed to show object")
            self.mayday("Are your UI files up-to-date?")
        else:
            self.start_spinner("Preparing object preview ...", ANALYSIS)

    def run_stretch(self):
        """
        Name:     Prida.run_stretch
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to stretch the grayscale luminosity of the
                  process image
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Stretching ...", ANALYSIS)
        self.toStretch.emit()

    def run_thresh(self):
        """
        Name:     Prida.run_thresh
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to threshold the process image
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Thresholding ...", ANALYSIS)
        self.toThresh.emit()

    def save_analysis(self):
        """
        Name:     Prida.save_analysis
        Inputs:   None.
        Outputs:  None.
        Features: Prompts user for output directory, saves current preview
                  image as TIFF and signals for image to save process/element
                  data
        """
        self.logger.info('Button Clicked')
        self.logger.debug('requesting new file name from user')
        path = QFileDialog.getExistingDirectory(self.base,
                                                "Select an output directory:",
                                                os.path.expanduser("~"),
                                                QFileDialog.ShowDirsOnly)
        if path != '':
            bname = os.path.splitext(self.data.basename)[0]
            fname = "%s_proc.tiff" % (bname)
            file_path = os.path.join(path, fname)
            is_ready = False
            f_idx = 1
            while not is_ready:
                if os.path.isfile(file_path):
                    fname = "%s_proc-%d.tiff" % (bname, f_idx)
                    file_path = os.path.join(path, fname)
                    is_ready = False
                    f_idx += 1
                    self.logger.debug("updated output filename to %s",
                                      file_path)
                else:
                    is_ready = True

            try:
                self.logger.debug('saving image %s', file_path)
                self.view.a_preview.export_to_file(file_path)
            except:
                self.logger.exception("save failed!")
                self.mayday("Failed to save image.")
            else:
                if os.path.isfile(file_path):
                    self.notice("Image save was successful.")
                else:
                    self.mayday("Failed to save image.")

            self.logger.debug("preparing to export processing parameters")
            export_file = "prida-calib-export_%s.txt" % (__version__)
            export_file_path = os.path.join(path, export_file)
            session_date = self.view.a_sess_date.text()
            my_dict = {'path': export_file_path,
                       'name': self.data.basename,
                       'date': session_date}
            self.logger.debug("sending save signal")
            self.toSave.emit(my_dict)
        else:
            self.mayday("Image save canceled.")
            self.toPlot.emit()

    def save_edits(self):
        """
        Name:     Prida.save_edits
        Inputs:   None
        Outputs:  None
        Features: Signals data to save attributes for all enabled input fields
        Depends:  get_sheet_model_selection
        """
        if self.editing:
            if self.sheet_select_model.hasSelection():
                self.logger.debug("gathering user selection")
                s_path = self.get_sheet_model_selection()

                self.logger.debug(
                    "saving attributes from enabled INPUT_EXP fields")
                for i in self.data.pid_attrs:
                    f_key = self.data.pid_attrs[i]["key"]
                    f_name = self.data.pid_attrs[i]["qt_val"]
                    f_type = self.data.pid_attrs[i]["qt_type"]
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        self.logger.info("signaling data to save %s", f_key)
                        self.setAttr.emit(f_key, f_val, s_path)

                for j in self.data.session_attrs:
                    f_key = self.data.session_attrs[j]["key"]
                    f_name = self.data.session_attrs[j]["qt_val"]
                    f_type = self.data.session_attrs[j]["qt_type"]
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        self.logger.info("signaling data to save %s", f_key)
                        self.setAttr.emit(f_key, f_val, s_path)

    def save_exif_tags(self, exif):
        """
        Name:     Prida.save_exif_tags
        Inputs:   dictionary, exif tags (exif)
        Outputs:  None.
        Features: Save exif tag values to INPUT_EXP fields
        Depends:  set_input_field
        """
        # Pseudo-intelligent search for session_attr index:
        tag_attr = {
            "Make": [k for k, v in self.data.session_attrs.items()
                     if v["key"] == "cam_make"],
            "Model": [k for k, v in self.data.session_attrs.items()
                      if v["key"] == "cam_model"],
            "ShutterSpeedValue": [k for k, v in self.data.session_attrs.items()
                                  if v["key"] == "cam_shutter"],
            "ApertureValue": [k for k, v in self.data.session_attrs.items()
                              if v["key"] == "cam_aperture"],
            "ISOSpeedRatings": [k for k, v in self.data.session_attrs.items()
                                if v["key"] == "cam_exposure"],
            "DateTime": [k for k, v in self.data.session_attrs.items()
                         if v["key"] == "date"],
            "NumPhotos": [k for k, v in self.data.session_attrs.items()
                          if v["key"] == "num_img"]
        }

        for tag in exif:
            if tag in tag_attr:
                j = tag_attr[tag][0]
                f_name = self.data.session_attrs[j]["qt_val"]
                f_type = self.data.session_attrs[j]["qt_type"]
                f_val = exif[tag]
                self.logger.debug("setting %s %s to %s" % (
                    f_type, f_name, f_val))
                self.set_input_field(f_name, f_type, f_val)

    def search(self, pid_meta, terms):
        """
        Name:     Prida.search
        Inputs:   - list, PID meta data values (pid_meta)
                  - list, search terms (terms)
        Outputs:  bool (found)
        Features: Searches the pid metafields and returns true if all terms are
                  found anywhere within the fields
        """
        found = True
        for term in terms:
            found = found and any(term in m_field for m_field in pid_meta)
        return found

    def search_file_addition(self):
        """
        Name:     Prida.search_file_addition
        Inputs:   None.
        Outputs:  None.
        Features: Adds user-defined HDF5 files to search files dictionary and
                  signals data to read PID and attributes from HDF5 file
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.info("Button Clicked")
        self.logger.debug("requesting search files")
        items = QFileDialog.getOpenFileNames(self.base,
                                             "Select one or more files",
                                             os.path.expanduser("~"),
                                             "HDF5 files (*.hdf5)")
        paths = items[0]
        num_paths = len(paths)
        if num_paths > 0:
            # Start spinner:
            self.start_spinner("Adding selection to list ...", FILE_SEARCH)
            path_idx = 0
            for path in paths:
                path_idx += 1
                self.logger.debug("processing selection %s", path)
                f_name = os.path.basename(path)
                f_name = os.path.splitext(f_name)[0]
                f_path = os.path.dirname(path)
                if f_name not in self.search_files:
                    # Add file to file_sheet_model:
                    self.logger.debug("adding search file %s to list", f_name)
                    self.search_files[f_name] = f_path
                    f_list = [QStandardItem(f_name), QStandardItem(f_path)]
                    self.file_sheet_model.appendRow(f_list)

                    self.logger.info("signaling data to add search file")
                    self.addSearch.emit(path, path_idx, num_paths)

    def search_file_removal(self):
        """
        Name:     Prida.search_file_removal
        Inputs:   None.
        Outputs:  None.
        Features: Removes a file from the file browse view and its
                  associated PID rows from the file search view
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.info("Button Clicked")
        if self.file_select_model.hasSelection():
            # Get the selection and put together the file name and path:
            self.logger.debug("gathering user selection")
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0])
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            # Remove file from search_files:
            self.logger.debug("deleting file %s", f_name)
            del self.search_files[s_name]

            # Remove file's PIDs from file_search_sheet:
            self.removeSearch.emit(f_path)
        else:
            self.logger.warning("nothing selected!")
            err_msg = "No selection made. Please select a file to remove."
            self.mayday(err_msg)

    def search_hdf(self):
        """
        Name:     Prida.search_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Changed view to FILE SEARCH and clears welcome greeter
        """
        self.logger.info("Button Clicked!")
        self.logger.info("View changed to FILE SEARCH")
        self.view.stackedWidget.setCurrentIndex(FILE_SEARCH)
        self.view.welcome.clearBaseImage('Welcome to Prida')

    def set_about(self):
        """
        Name:     Prida.set_about
        Inputs:   None.
        Outputs:  None.
        Features: Opens the user input popup and signals root attributes and
                  session defaults to data
        """
        self.logger.debug('loading input user popup')
        self.user_ui.user.setFocus()
        if self.user_dialog.exec_():
            r_user = str(self.user_ui.user.text())
            r_addr = str(self.user_ui.email.text())
            r_about = str(self.user_ui.about.toPlainText())

            self.logger.debug("signaling root attributes")
            self.setAttr.emit("user", r_user, "/")
            self.setAttr.emit("addr", r_addr, "/")
            self.setAttr.emit("about", r_about, "/")

            self.logger.debug("setting session defaults")
            self.setSessionDefault.emit("user", r_user)
            self.setSessionDefault.emit("addr", r_addr)

            self.logger.debug("updating about popup fields")
            self.about_ui.author_about.setText(r_user)
            self.about_ui.contact_about.setText(r_addr)
            self.about_ui.summary_about.setText(r_about)

    def set_defaults(self):
        """
        Name:     Prida.set_defaults
        Inputs:   None.
        Outputs:  None.
        Features: Sets GUI text default values
        """
        self.logger.debug("resetting INPUT_EXP PID default fields")
        self.set_input_field("c_pid", "QLineEdit", "")
        for i in self.data.pid_attrs:
            f_name = self.data.pid_attrs[i]["qt_val"]
            f_type = self.data.pid_attrs[i]["qt_type"]
            f_val = self.data.pid_attrs[i]["def_val"]
            try:
                self.set_input_field(f_name, f_type, f_val)
            except:
                self.set_input_field(f_name, f_type, '')

        self.logger.debug("resetting INPUT_EXP session default fields")
        for j in self.data.session_attrs:
            f_name = self.data.session_attrs[j]["qt_val"]
            f_type = self.data.session_attrs[j]["qt_type"]
            f_val = self.data.session_attrs[j]["def_val"]
            try:
                self.set_input_field(f_name, f_type, f_val)
            except:
                self.set_input_field(f_name, f_type, '')

    def set_greeter(self):
        """
        Name:     Prida.set_greeter
        Inputs:   None.
        Outputs:  None.
        Features: Sets greeter image
        """
        if self.greeter is not None:
            self.logger.debug("setting the greeter image")
            self.view.welcome.setBaseImage(scipy.misc.imread(self.greeter))

    def set_input_field(self, field_name, field_type, field_value):
        """
        Name:     Prida.set_input_field
        Inputs:   - str, Qt input widget name (field_name)
                  - str, Qt input widget type (field_type)
                  - str, input value (field_value)
        Outputs:  None
        Features: Sets Qt input field

        @TODO: change empty string to None type
        """
        err_msg = "set_input_field:could not set Qt %s, %s, to value %s" % (
            field_type, field_name, field_value)

        if field_type == "QComboBox":
            # Takes an integer, find it!
            if field_value == '':
                index = 0
            else:
                index = eval('self.view.%s.findText("%s", %s)' % (
                    field_name,
                    field_value,
                    "Qt.MatchFixedString")
                )
            try:
                self.logger.debug("setting QComboBox index to %d", index)
                exec("self.view.%s.setCurrentIndex(%d)" % (
                    field_name,
                    index)
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        elif field_type == "QDateEdit":
            # Takes a QDate object
            if field_value == '':
                try:
                    self.logger.debug("setting QDateEdit to current date")
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        "QDate.fromString",
                        QDate.currentDate().toString("yyyy-MM-dd"))
                    )
                except:
                    self.logger.exception(err_msg)
                    raise ValueError(err_msg)
            else:
                try:
                    self.logger.debug("setting QDate edit to %s", field_value)
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        "QDate.fromString",
                        field_value)
                    )
                except:
                    self.logger.exception(err_msg)
                    raise ValueError(err_msg)
        elif field_type == "QLineEdit":
            # Takes a string
            try:
                self.logger.debug("setting QLineEdit to %s", field_value)
                exec('self.view.%s.setText("%s")' % (
                    field_name,
                    field_value)
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        elif field_type == "QSpinBox":
            # Takes an integer
            if field_value == '':
                field_value = 0
            try:
                self.logger.debug("setting QSpinBox to %d", int(field_value))
                exec("self.view.%s.setValue(%d)" % (
                    field_name,
                    int(field_value))
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        else:
            self.logger.error(err_msg)
            raise ValueError(err_msg)

    def set_ida(self, my_array, enum):
        """
        Name:     Prida.set_ida
        Inputs:   - ndarray, image dataset array (my_array)
                  - int, view enumerator (enum)
        Output:   None.
        Features: Catches signal from data; sets IDA base image
        """
        if enum == VIEWER:
            self.stop_spinner()
            self.logger.debug("setting IDA base image to selection")
            self.view.ida.setBaseImage(my_array)
        elif enum == ANALYSIS:
            self.logger.debug("clearing IDA base image")
            self.movie.stop()
            self.view.a_spinner.clear()
            self.logger.debug("setting IDA base image to selection")
            self.view.a_preview.setBaseImage(my_array)

    def set_session_sheet_model(self, my_dict):
        """
        Name:     Prida.set_session_sheet_model
        Inputs:   dict, session attributes (my_dict)
        Outputs:  None.
        Features: Receives the session attribute signal from data; sets
                  attribute values to session sheet
        """
        self.logger.debug("loading session fields")
        for i in my_dict:
            exec('%s(%d, 1, QStandardItem("%s"))' % (
                "self.session_sheet_model.setItem", i, my_dict[i]['cur_val'])
            )

    def set_sheet_thumbs(self, img_count, img_tot, img_path, img_array, enum):
        """
        Name:     Prida.set_sheet_thumbs
        Inputs:   - int, image counter index (img_count)
                  - int, total number of images (img_tot)
                  - str, image path (img_path)
                  - ndarray, image dataset array (my_array)
                  - int, view enumerator (enum)
        Outputs:  None.
        Features: Catches signal from data; adds thumbnails to the image sheet
                  model
        Depends:  - mayday
                  - stop_spinner
        """
        self.logger.info("caught signal for thumbnail %d", img_count)
        if img_array.size != 0:
            img_dim = len(img_array.shape)

            # Binary image support:
            if img_array.max() == 1 and img_dim == 2:
                self.logger.debug("found binary image!")
                self.logger.debug("scaling binary pixels for grayscale")
                img_array[numpy.where(img_array == 1)] *= 255
                self.logger.debug("pixel scaling complete")

            # Grayscale image support:
            if img_dim == 2:
                try:
                    self.logger.debug("checking dimensions on grayscale image")
                    temp_array = numpy.zeros(
                        (img_array.shape[0], img_array.shape[1], 3))
                    temp_array[:, :, 0] = numpy.copy(img_array)
                    temp_array[:, :, 1] = numpy.copy(img_array)
                    temp_array[:, :, 2] = numpy.copy(img_array)
                except:
                    self.logger.exception("something is awry")
                    raise
                else:
                    img_array = numpy.copy(temp_array.astype('uint8'))

            try:
                self.logger.debug("creating thumbnail %d", img_count)
                image = Image.fromarray(img_array)
                qt_image = ImageQt.ImageQt(image)
                pix = QPixmap.fromImage(qt_image)
            except:
                self.logger.exception("Failed to set thumbnail")
            else:
                self.img_list.append(qt_image)
                item = QStandardItem(str(img_count))
                item.setIcon(QIcon(pix))
                item.setData(img_path)
                if enum == VIEWER:
                    self.logger.debug("adding thumbnail %d", img_count)
                    self.img_sheet_model.appendRow(item)
                elif enum == ANALYSIS:
                    self.logger.warning("received deprecated signal!")
                else:
                    self.logger.warning(
                        "received unknown enum %d for updating thumbnails",
                        enum)
                    self.mayday(
                        "received unknown enum for creating thumbnails")

        # Stop spinner when last thumb arrives:
        if img_count == img_tot:
            self.stop_spinner()

    def show_analysis(self, img_array):
        """
        Name:     Prida.show_analysis
        Input:    numpy.ndarray, image array (img_array)
        Output:   None.
        Features: Sets preview image to analysis IDA
        """
        self.stop_spinner(ANALYSIS)
        self.logger.debug("caught image with size %d", img_array.size)
        if img_array.size > 1:
            self.logger.debug("setting IDA base image to selection")
            self.view.a_preview.setBaseImage(img_array)

    def show_context_menu(self, pos=None):
        """
        Name:     Prida.show_context_menu
        Inputs:   [optional] QPoint, where the click occurred (pos)
        Outputs:  None.
        Features: Opens context menu for right clicking on IDA (in view's page)
                  to request the save image as feature
        Depends:  - mayday
                  - notice
        Ref:      http://www.setnode.com/blog/right-click-context-menus-with-qt
        """
        gpos = self.view.page.mapToGlobal(pos)
        my_menu = QMenu()
        my_menu.addAction("Save Image As ...")
        selected_item = my_menu.exec_(gpos)
        if selected_item:
            path = QFileDialog.getSaveFileName(
                self.base, "Save Image As ...", os.path.expanduser("~"))[0]
            if path != '':
                # Check to see if user selected a supported image format:
                p_ext = os.path.splitext(path)[1]
                if p_ext not in self.img_types:
                    err_msg = (
                        "Please select one of the following image formats: ")
                    err_msg += ", ".join(self.img_types)
                    self.mayday(err_msg)
                else:
                    self.logger.debug("Saving image to %s", path)
                    try:
                        self.view.ida.export_to_file(path, False)
                    except:
                        self.logger.exception("save failed!")
                        self.mayday(
                            "Failed to save image. "
                            "Please check your file name and try again.")
                    else:
                        if os.path.isfile(path):
                            self.notice("Image save was successful.")
                        else:
                            self.mayday("Failed to save image.")
        else:
            self.logger.debug("User did not make selection")

    def show_ida(self, selection):
        """
        Name:     Prida.show_ida
        Input:    obj (selection)
        Output:   None.
        Features: Switches to IDA page in VIEWER view and sets IDA base image;
                  handles image orientation
        """
        if selection.indexes():
            self.logger.debug("selection made")
            self.logger.info("view changed to IDA_VIEW")
            self.view.stacked_sheets.setCurrentIndex(IDA_VIEW)

            self.logger.debug("gathering user selection")
            img_path = self.img_sheet_model.itemFromIndex(
                selection.indexes()[0]).data()

            # Signal for IDA image
            self.getIDA.emit(img_path, VIEWER)
            self.start_spinner("opening preview")

    def shutdown(self):
        """
        Name:     Prida.shutdown
        Inputs:   None.
        Outputs:  None.
        Features: Shutsdown all open processes and threads
        """
        try:
            self.stopFile.emit()
            self.data.quit()
            self.data_thread.wait()
            if self.hardware_mode:
                self.hardware_thread.quit()
                self.hardware_thread.wait()
            if self.analysis_mode:
                self.analysis_thread.quit()
                self.analysis_thread.wait()
        except:
            self.logger.warning("failed to shutdown")
        else:
            self.logger.info("app is shutting down")
        finally:
            self.quit()

    def start_spinner(self, msg, enum=VIEWER):
        """
        Name:     Prida.start_spinner
        Inputs:   str, spinner's message text (msg)
        Outputs:  None.
        Features: Starts the spinner GIF and with associated text
        """
        if enum == VIEWER:
            self.logger.debug("starting spinner in VIEWER")
            self.view.spinner_txt.setText(msg)
            self.view.spinner_gif.setMovie(self.movie)
            self.view.spinner_gif.show()
            self.movie.start()
            self.logger.debug("spinner started in VIEWER")
        elif enum == ANALYSIS:
            self.logger.debug("starting spinner in ANALYSIS")
            self.view.a_label.setText(msg)
            self.view.a_spinner.setMovie(self.movie)
            self.view.a_spinner.show()
            self.movie.start()
            self.logger.debug("spinner started in ANALYSIS")
        elif enum == FILE_SEARCH:
            self.logger.debug("starting spinner in FILE_SEARCH")
            self.view.fs_label.setText(msg)
            self.view.fs_spinner.setMovie(self.movie)
            self.view.fs_spinner.show()
            self.movie.start()
            self.logger.debug("spinner started in FILE_SEARCH")
        else:
            self.logger.warning("could not process spinner enum %d", enum)

    def stop_spinner(self, enum=VIEWER):
        """
        Name:     Prida.stop_spinner
        Inputs:   None.
        Outputs:  None.
        Features: Stops the spinner GIF and clears the associated text
        """
        self.logger.debug("stopping spinner")
        self.movie.stop()
        if enum == VIEWER:
            self.view.spinner_txt.setText("")
            self.view.spinner_gif.clear()
        elif enum == ANALYSIS:
            self.view.a_spinner.clear()
        elif enum == FILE_SEARCH:
            self.view.fs_label.setText("")
            self.view.fs_spinner.clear()

    def to_sheet(self):
        """
        Name:     Prida.to_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Switches from IDA to sheet view; releasing resources
        """
        self.logger.info("Button Clicked")
        self.logger.debug("clearing IDA base image")
        self.view.ida.clearBaseImage("Image Display Area")
        self.logger.debug("clearing img_select")
        self.view.img_select.clearSelection()
        self.logger.info("view changed to SHEET_VIEW")
        self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)

    def tool_not_enabled(self):
        """
        THIS IS A PLACEHOLDER FUNCTION
        """
        self.logger.warning("tool not enabled!")
        err_msg = "This tool has not be enabled yet."
        self.mayday(err_msg)

    def update_about(self):
        """
        Name:     Prida.update_about
        Inputs:   None.
        Outputs:  None.
        Features: Sets user input fields and opens a new user input popup
        Depends:  set_about
        """
        self.logger.debug("setting user input values")
        overview = self.data.about
        self.user_ui.user.setText(overview.get("Author", ""))
        self.user_ui.email.setText(overview.get("Contact", ""))
        self.user_ui.about.setText(overview.get("Summary", ""))

        self.logger.debug("requesting new input values")
        self.set_about()

    def update_analysis_color(self, bg_color):
        """
        Name:     Prida.update_analysis_color
        Inputs:   str, background color (bg_color)
        Outputs:  None.
        Features: Updates the QLabel with current background color
        """
        try:
            self.view.a_background.clear()
            self.view.a_background.setText(bg_color)
        except:
            self.mayday("Have you updated your UI files?")

    def update_analysis_combos(self):
        """
        Name:     Prida.update_analysis_combos
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QComboBox with exclude options
        """
        self.logger.debug("setting the analysis view QComboBox values")
        try:
            self.view.a_bgcolor_combo.clear()
            self.view.a_bgcolor_combo.insertItem(1, "Black", 0)
            self.view.a_bgcolor_combo.insertItem(2, "White", 1)
            self.view.a_bgcolor_combo.setCurrentIndex(0)

            self.view.a_scale_combo.clear()
            for i in range(1, 5):
                s_factor = int(100 - 25*(i-1))
                s_label = "%d%%" % (s_factor)
                self.view.a_scale_combo.insertItem(i, s_label, s_factor)
            self.view.a_scale_combo.setCurrentIndex(0)
        except AttributeError:
            self.logger.exception("Could not update QComboBoxes")
            self.mayday(("An error has been encountered. Please make certain"
                         " your UI files are up-to-date."))
        except:
            self.logger.exception("Encountered unexpected error.")
            raise

    def update_analysis_found(self, num_found):
        """
        Name:     Prida.update_analysis_found
        Inputs:   int, number of objects found (num_found)
        Outputs:  None.
        Features: Updates the QCombo with current number of objects found
        """
        try:
            self.view.a_element_combo.clear()
            if num_found > 0:
                for i in range(num_found):
                    j = i + 1
                    self.view.a_element_combo.insertItem(j, str(j), i)
                self.view.a_element_combo.insertItem(0, "All", -1)
                self.view.a_element_combo.setCurrentIndex(0)
        except:
            self.logger.exception("Could not update element combo box")
            self.mayday("Are your UI files up-to-date?")

    def update_analysis_progress(self, msg):
        """
        Name:     Prida.update_analysis_progress
        Inputs:   str, progress message (msg)
        Outputs:  None.
        Features: Updates the QLabel with current progress
        """
        self.view.a_label.clear()
        self.view.a_label.setText(msg)

    def update_analysis_merge(self, h, w):
        """
        Name:     Prida.update_analysis_merge
        Inputs:   - int, image pixel height (h)
                  - int, image pixel width (w)
        Outputs:  None.
        Features: Updates the QSpinBox with current image size
        """
        self.view.a_crop_left.clear()
        self.view.a_crop_left.setRange(0, w)
        self.view.a_crop_left.setValue(0)
        self.view.a_crop_left.setSingleStep(10)

        self.view.a_crop_right.clear()
        self.view.a_crop_right.setRange(0, w)
        self.view.a_crop_right.setValue(w)
        self.view.a_crop_right.setSingleStep(10)

        self.view.a_crop_top.clear()
        self.view.a_crop_top.setRange(0, h)
        self.view.a_crop_top.setValue(0)
        self.view.a_crop_top.setSingleStep(10)

        self.view.a_crop_bottom.clear()
        self.view.a_crop_bottom.setRange(0, h)
        self.view.a_crop_bottom.setValue(h)
        self.view.a_crop_bottom.setSingleStep(10)

    def update_analysis_range(self, img_range):
        """
        Name:     Prida.update_analysis_shape
        Inputs:   str, image range (img_range)
        Outputs:  None.
        Features: Updates the QLabel with current image range
        """
        try:
            self.view.a_img_range.clear()
            self.view.a_img_range.setText(img_range)
        except:
            self.mayday("Have you updated your UI files lately?")

    def update_analysis_shape(self, img_shape):
        """
        Name:     Prida.update_analysis_shape
        Inputs:   str, image shape (img_shape)
        Outputs:  None.
        Features: Updates the QLabel with current image shape
        """
        self.view.a_img_shape.clear()
        self.view.a_img_shape.setText(img_shape)

    def update_analysis_view(self, s_path, s_dict, img_list):
        """
        Name:     Prida.update_analysis_view
        Inputs:   - str, session path (s_path)
                  - dict, session attributes (s_dict)
                  - list, session image list (img_list)
        Outputs:  None.
        Features: Updates analysis view
        """
        # Set session attributes to GUI labels:
        session_title = s_dict[2]['cur_val']
        self.view.a_sess_title.setText(session_title)
        session_date = s_dict[3]['cur_val']
        self.view.a_sess_date.setText(session_date)
        num_img = len(img_list)
        self.view.a_sess_count.setText(str(num_img))

    def update_data(self, my_list):
        """
        Name:     Prida.update_data
        Inputs:   list, data list (my_list)
        Outputs:  None.
        Features: Catches signal from data, updates data list with current
                  PIDs and their associated attributes
        Depends:  back_to_sheet
        """
        self.logger.info("caught data listed signal")
        self.data_list = []
        self.data_list = my_list
        self.logger.debug("updating sheet model")
        self.back_to_sheet()

    def update_dict_auto_model(self):
        """
        Name:     Prida.update_dict_auto_model
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QStringListModel for the dictionary file's
                  contents
        """
        self.plants_auto_model.setStringList(list(self.usda_plants.keys()))
        self.dict_auto_model.setStringList(
            list(set(self.usda_plants.values())))

    def update_exclude_combo(self):
        """
        Name:     Prida.update_exclude_combo
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QComboBox with exclude options
        """
        self.logger.debug("setting the QComboBox values for c_exclude")
        my_options = ["False",
                      "True"]
        self.view.c_exclude.insertItems(0, my_options)

    def update_file_browse_sheet(self):
        """
        Name:     Prida.update_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Reloads the rows in the file_sheet_model
        Depends:  - load_file_browse_sheet_headers
                  - resize_file_browse_sheet
        """
        self.logger.debug("updating sheet")
        self.file_sheet_model.clear()
        self.load_file_browse_sheet_headers()
        for f_name in self.search_files:
            f_path = self.search_files[f_name]
            f_list = [QStandardItem(f_name), QStandardItem(f_path)]
            self.file_sheet_model.appendRow(f_list)
        self.resize_file_browse_sheet()

    def update_file_search_sheet(self, search=''):
        """
        Name:     Prida.update_file_search_sheet
        Inputs:   [optional] str, search string (search)
        Outputs:  None.
        Features: Reloads the rows in the search_sheet_model, filtered for
                  search terms
        Depends:  - search_data (attr)
                  - load_file_search_sheet_headers (func)
                  - resize_search_sheet (func)
                  - search (func)
        """
        self.logger.debug("updating sheet")
        self.search_sheet_model.clear()
        self.load_file_search_sheet_headers()
        terms = search.lower().split()
        for i in self.search_data:
            pid_meta = []
            for j in self.search_data[i]:
                pid_meta.append(j.lower())
            if self.search(pid_meta, terms):
                d_list = []
                for k in self.search_data[i]:
                    d_list.append(QStandardItem(k))
                self.search_sheet_model.appendRow(d_list)
        self.resize_search_sheet()

    def update_input_field(self, my_vals):
        """
        Name:     Prida.update_input_field
        Inputs:   tuple, Qt field name, type, and value (my_vals)
        Outputs:  None
        Features: Catches data's set field signal and sets the input field with
                  the appropriate info
        Depends:  set_input_field
        """
        (f_name, f_type, f_value) = my_vals
        self.set_input_field(f_name, f_type, f_value)

    def update_orient_combo(self):
        """
        Name:     Prida.update_orient_combo
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QComboBox with orientation options
        """
        self.logger.debug("setting the QComboBox values for c_img_orient")
        my_options = ["Original",
                      "Rotated Counter-Clockwise",
                      "Rotated Clockwise"]
        self.view.c_img_orient.insertItems(0, my_options)

    def update_pid_auto_model(self):
        """
        Name:     Prida.update_pid_auto_model
        Inputs:   None
        Outputs:  None
        Features: Updates the QStringListModel with HDF5 file's current PIDs
        """
        self.logger.debug("assigning %d PIDs to list", len(self.data.pid_list))
        self.pid_auto_model.setStringList(self.data.pid_list)

    def update_progress(self, prog_val, t, show_all=False):
        """
        Name:     Prida.update_progress
        Inputs:   - int, progress value (prog_val)
                  - tuple, imaging output arguments (t)
                  - bool, show all preview images (show_all)
        Outputs:  None.
        Features: Threading slot.
                  Updates the progress bar during imaging, creates a preview
                  of the first image taken, and saves the image along with
                  associated metadata to HDF5 file
        """
        self.logger.debug("updating progress bar")
        self.view.c_progress.setValue(prog_val)

        self.logger.debug("gathering image metadata")
        is_path = t[0]
        file_path = t[1]

        if show_all:
            self.preview = False

        self.logger.debug("setting image preview")
        if is_path and not self.preview:
            try:
                image = QPixmap(file_path)
            except:
                self.logger.exception("QPixmap failed for file %s", file_path)
                self.preview = False
            else:
                self.logger.debug("image preview set")
                self.view.c_preview.setPixmap(image.scaledToHeight(300))
                self.preview = True
        else:
            self.logger.debug("skipping image, preview already set")

    def update_search_additions(self, search_dict, cur_idx, max_idx):
        """
        Name:     Prida.update_search_additions
        Inputs:   - dict, search file dictionary (search_dict)
                  - int, current index value (cur_idx)
                  - int, maximum index value (max_idx)
        Outputs:  None.
        Features: Catches signal from data and updates file search and file
                  browse sheets
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.debug("updating search data")
        for key in search_dict.keys():
            value = search_dict.get(key, "N/A")
            self.search_data[key] = value

        self.logger.debug("clearing FILE_SEARCH search bar")
        self.view.f_search.clear()

        self.logger.debug("updating file search and file browse sheets")
        self.update_file_search_sheet()
        self.update_file_browse_sheet()
        if cur_idx == max_idx:
            self.stop_spinner(FILE_SEARCH)

    def update_search_removals(self, key_list):
        """
        Name:     Prida.update_search_removals
        Inputs:   list, search file PID keys (key_list)
        Outputs:  None.
        Features: Catches signal from data and updates file search and file
                  browse sheets
        Depends:  - search_data (attr)
                  - update_file_browse_sheet (func)
                  - update_file_search_sheet (func)
        """
        self.logger.debug("removing items from file search sheet")
        for key in key_list:
            if key in self.search_data:
                del self.search_data[key]

        self.view.f_search.clear()
        self.update_file_browse_sheet()
        self.update_file_search_sheet()

    def update_session_sheet_model(self, selection):
        """
        Name:     Prida.update_session_sheet_model
        Inputs:   obj (selection)
        Outputs:  None.
        Features: Parses user's sheet selection; clears session and image
                  sheets if PID is selected; signals for session attributes and
                  thumbnails if session is selected
        Depends:  - clear_session_sheet_model
                  - resize_session_sheet
                  - start_spinner
        """
        if selection.indexes():
            self.logger.debug("gathering user selection")
            my_session = self.sheet_model.itemFromIndex(selection.indexes()[0])

            if (my_session.parent() is not None):
                # Session selection
                self.logger.debug("session selected")
                pid = my_session.parent().text()
                session = my_session.text()
                s_path = "/%s/%s" % (pid, session)

                # Signal data to set session attributes:
                self.logger.info("signaling data for session attributes")
                self.getSessionAttrs.emit(s_path)

                self.logger.info("signaling data for thumbnails")
                self.img_sheet_model.clear()
                self.img_list = []
                self.start_spinner("")
                self.getThumbs.emit(s_path, VIEWER)

            else:
                # PID selection
                self.logger.debug("PID selected")
                self.logger.debug("clearing session and image sheets")
                self.clear_session_sheet_model()
                self.img_sheet_model.clear()
                self.img_list = []
        else:
            # You made a de-selection (ctrl+click):
            self.logger.debug("encountered de-selection")
            self.logger.debug("clearing session and image sheets")
            self.clear_session_sheet_model()
            self.img_sheet_model.clear()
            self.img_list = []
        self.logger.debug("resizing session sheet")
        self.resize_session_sheet()

    def update_sheet_model(self, search=''):
        """
        Name:     Prida.update_sheet_model
        Inputs:   [optional] str (search)
        Features: Displays all PIDs in the sheet or the PIDs that match the
                  search terms if search terms are given
        Depends:  - load_sheet_headers
                  - resize_sheet
                  - search
                  - update_pid_auto_model
        """
        self.logger.debug("updating the sheet model")
        self.update_pid_auto_model()
        self.sheet_model.clear()
        self.load_sheet_headers()
        self.logger.debug("parsing search terms")
        terms = search.lower().split()
        for d in self.data_list:
            pid_meta = []
            for key in d:
                if key != "sessions":
                    pid_meta.append(d[key].lower())
            if self.search(pid_meta, terms):
                pid_item = QStandardItem(d["pid"])

                # Create session rows under each PID:
                for session in d["sessions"]:
                    pid_item.appendRow(QStandardItem(session))

                # Fill the PID row with its ordered attributes:
                pid_list = [pid_item, ]
                for i in sorted(list(self.data.pid_attrs.keys())):
                    f_key = self.data.pid_attrs[i]["key"]
                    my_attr = d[f_key]
                    pid_list.append(QStandardItem(my_attr))
                self.sheet_model.appendRow(pid_list)
        self.resize_sheet()

    def version_checker(self, config_filepath):
        """
        Name:     Prida.version_checker
        Inputs:   str, configuration file path (config_filepath)
        Outputs:  bool, use defaults flag
        Features: Checks the configuration file version against current
                  software version; if matched, return False, else return True
        """
        # Initialize error string and configuration file version:
        use_defaults = False
        err_str = None
        conf_ver = None

        if config_filepath is not None:
            try:
                my_user = os.path.expanduser('~')
                conf_file = os.path.join(my_user, 'Prida', 'prida.config')
                if os.path.isfile(conf_file):
                    with open(conf_file, 'r') as my_config_file:
                        for line in my_config_file:
                            if 'PRIDA_VERSION' in line:
                                words = line.split()
                                if len(words) == 3:
                                    conf_ver = eval(words[2])
                                else:
                                    err_str = (
                                        'Configuration formatting error!\n'
                                        'Please check your configuration file '
                                        'and restart the program. '
                                        'Assuming program defaults.')
                            else:
                                err_str = (
                                    'Configuration version missing!\n'
                                    'Please check your configuration file and '
                                    'restart the program. '
                                    'Assuming program defaults.')
                else:
                    err_str = (
                        'Configuration file missing!\n'
                        'Please make sure your configuration file is located '
                        'in your local Prida directory. '
                        'Assuming program defaults.')
            except:
                err_str = (
                    'Configuration file parsing error!\n'
                    'Please check your configuration file is in the proper '
                    'format and is located in your local Prida directory. '
                    'Assuming program defaults.')
                self.logger.exception(err_str)
            if conf_ver is not None:
                if conf_ver != __version__:
                    err_str = (
                        'Configuration file version mis-match!\n'
                        'Please update your configuration file version  (%s) '
                        'to match your software version (%s). '
                        'Assuming program defaults.' % (conf_ver, __version__))
                else:
                    err_str = None
        else:
            err_str = (
                'Configuration file parsing error!\n'
                'Please check that your configuration file is in the proper '
                'format and is located in your local Prida directory. '
                'Assuming program defaults.')

        if err_str:
            use_defaults = True
            self.mayday(err_str)

        return use_defaults
