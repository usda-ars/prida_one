#!/usr/bin/python
#
# utilities.py
#
# VERSION 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import datetime
import logging
import os.path
import sys
import time

import numpy
import PIL.ExifTags as ExifTags

from . import __version__


###############################################################################
# CLASSES:
###############################################################################
class element():
    """
    Name:     element
    Features: Class representing a discrete, continuous body of black pixels,
              called an element.
    History:  Version 1.4.0
              - created center property [16.04.26]
              - added coi property [16.04.27]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, data, shape):
        """
        Name:     element.__init__
        Inputs:   - set(tuple(int)), set of coordinate points (data)
                  - tuple(int, int), image array shape (shape)
        Outputs:  None.
        Features: Class initialization
        """
        self.shape = shape      # original image array shape
        self._center = None     # pixel center coordinates of element
        self._coi = None        # center of image
        self._orig = data       # coordinate data points
        self._size = None       # total number of pixels in element
        self._width = None      # pixel width of element
        self._height = None     # pixel height of element
        self._img = None        # pixel representation of data points

    def __str__(self):
        """
        Name:     element.__str__
        Inputs:   None.
        Outputs:  str
        Features: Class string property, accessed via the print function
        """
        pstr = '{} - \n'.format(self.__class__)
        pstr += ('container shape: '
                 '({0[0]}[px], {0[1]}[px])\n').format(self.shape)
        pstr += 'element size: {:d}[px]\n'.format(self.size)
        pstr += 'element width: {:d}[px]\n'.format(self.width)
        pstr += 'element height: {:d}[px]\n'.format(self.height)
        return pstr

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def center(self):
        """Pixel center of element"""
        if self._center is not None:
            return self._center
        else:
            # Extract the sorted x and y coordinates:
            yvect = numpy.array([coord[0] for coord in self._orig])
            xvect = numpy.array([coord[1] for coord in self._orig])

            # Round to the center-most pixel:
            mean_y = 0.5*(yvect.max() + yvect.min())
            mean_x = 0.5*(xvect.max() + xvect.min())
            mid_y = int(mean_y + 0.5)
            mid_x = int(mean_x + 0.5)

            self._center = (mid_y, mid_x)
            return self._center

    @center.setter
    def center(self, val):
        raise AttributeError("manual setting of center is forbidden")

    @property
    def coi(self):
        """Pixel center of image coordinates"""
        if self._coi is not None:
            return self._coi
        else:
            h, w = self.shape
            mid_h = int(0.5*h + 0.5)
            mid_w = int(0.5*w + 0.5)
            self._coi = (mid_h, mid_w)
            return self._coi

    @coi.setter
    def coi(self, val):
        raise AttributeError("manual setting of coi is forbidden")

    @property
    def height(self):
        """Pixel height of element"""
        if self._height is not None:
            return self._height
        else:
            yvect = numpy.array([coord[0] for coord in self._orig])
            self._height = int(yvect.max() - yvect.min()) + 1
            return self._height

    @height.setter
    def height(self, val):
        raise AttributeError('manual setting of height is forbidden')

    @property
    def img(self):
        """Pixel representation of coordinates"""
        if self._img is not None:
            return self._img
        else:
            new_array = numpy.ones(self.shape).astype("uint8")
            new_array *= 255
            for item in self._orig:
                new_array[item] = 0
            self._img = new_array
            return new_array

    @img.setter
    def img(self, val):
        raise AttributeError("manual setting of img is forbidden")

    @property
    def size(self):
        """Number of pixels in element"""
        if self._size is not None:
            return self._size
        else:
            self._size = len(self._orig)
            return self._size

    @size.setter
    def size(self, val):
        raise AttributeError('manual setting of size is forbidden')

    @property
    def width(self):
        """Pixel width of element"""
        if self._width is not None:
            return self._width
        else:
            xvect = numpy.array([coord[1] for coord in self._orig])
            self._width = int(xvect.max() - xvect.min()) + 1
            return self._width

    @width.setter
    def width(self, val):
        raise AttributeError('manual setting of width is forbidden')


###############################################################################
# FUNCTIONS:
###############################################################################
def create_config_file(prida_dir, file_name):
    """
    Name:     create_config_file
    Inputs:   str, config file name with path (file_path)
    Outputs:  None.
    Features: Create a default configuration file at given location.
    """
    config_txt = (
        "PRIDA.HARDWARE.PIEZO        PWM_CHANNEL           0\n"
        "PRIDA.HARDWARE.PIEZO        VOLUME                100.0\n"
        "PRIDA.HARDWARE.CAMERA       IMAGE_DIR             '%s'\n"
        "PRIDA.HARDWARE.LED          RED_LED_CHANNEL       15\n"
        "PRIDA.HARDWARE.LED          GREEN_LED_CHANNEL     14\n"
        "PRIDA.HARDWARE.LED          BLUE_LED_CHANNEL      1\n"
        "PRIDA.HARDWARE.LED          LED_TYPE              'CC'\n"
        "PRIDA.HARDWARE.IMAGING      GEAR_RATIO            1.0\n"
        "PRIDA.HARDWARE.IMAGING      DRIVER                ''\n"
        "PRIDA.HARDWARE.IMAGING      HAS_LED               False\n"
        "PRIDA.HARDWARE.IMAGING      HAS_PIEZO             False\n"
        "PRIDA.HARDWARE.IMAGING      MODE                  'Explorer'\n"
        "PRIDA.HARDWARE.IMAGING      SETTLING_TIME         2.0\n"
        "PRIDA.HARDWARE.CONTROLLER   SPEED                 4.0\n"
        "PRIDA.HARDWARE.CONTROLLER   DEGREES_PER_STEP      1.8\n"
        "PRIDA.HARDWARE.CONTROLLER   MICROSTEPS_PER_STEP   4\n"
        "PRIDA.HARDWARE.CONTROLLER   PORT_NUMBER           1\n"
        "PRIDA.GLOBAL_CONF           PRIDA_VERSION         '%s'\n"
        "PRIDA.GLOBAL_CONF           LOG_DIR               '%s'\n"
        "PRIDA.GLOBAL_CONF           CREDENTIAL_DIR        '%s'\n"
        "PRIDA.GLOBAL_CONF           DICTIONARY_DIR        '%s'\n"
        "PRIDA.GLOBAL_CONF           LOG_FILENAME          'prida.log'\n"
        "PRIDA.GLOBAL_CONF           CREDENTIAL_FILENAME   'credentials'\n"
        "PRIDA.GLOBAL_CONF           DICTIONARY_FILENAME   'dictionary.txt'\n"
        "PRIDA.GLOBAL_CONF           LOG_LEVEL             'info'\n"
        "PRIDA.HDF_ORGANIZER         COMPRESS_LV           6\n"
        % (os.path.join(prida_dir, 'photos'),
           __version__,
           os.path.join(prida_dir, "logs"),
           prida_dir,
           prida_dir)
    )
    file_path = os.path.join(prida_dir, file_name)
    try:
        with open(file_path, 'w') as my_file:
            my_file.write(config_txt)
    except:
        raise IOError("Can not write to config file.")


def func_line(x, a, b):
    """
    Name:     func_line
    Inputs:   - numpy.ndarray, x values (x)
              - float, slope (a)
              - float, intercept (b)
    Returns   numpy.ndarray, y values
    Features: Returns y = ax + b
    """
    return float(a)*x + float(b)


def fitEllipse(x, y):
    """
    Ref: http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html
    """
    x = x[:, numpy.newaxis]
    y = y[:, numpy.newaxis]
    D = numpy.hstack((x*x, x*y, y*y, x, y, numpy.ones_like(x)))
    S = numpy.dot(D.T, D)
    C = numpy.zeros([6, 6])
    C[0, 2] = C[2, 0] = 2
    C[1, 1] = -1
    E, V = numpy.linalg.eig(numpy.dot(numpy.linalg.inv(S), C))
    n = numpy.argmax(numpy.abs(E))
    a = V[:, n]
    return a


def ellipse_center(a):
    """
    Ref: http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html
    """
    b = a[1]/2
    c = a[2]
    d = a[3]/2
    f = a[4]/2
    h = a[0]
    num = b*b - h*c
    x0 = (c*d - b*f)/num
    y0 = (h*f - b*d)/num
    return numpy.array([x0, y0])


def ellipse_angle_of_rotation(a):
    """
    Ref: http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html
    """
    b = a[1]/2
    c = a[2]
    h = a[0]
    if b == 0:
        if h > c:
            return 0
        else:
            return numpy.pi/2
    else:
        if h > c:
            return numpy.arctan(2*b/(h-c))/2
        else:
            return numpy.pi/2 + numpy.arctan(2*b/(h-c))/2


def ellipse_axis_length(a):
    """
    Ref: http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html
    """
    b = a[1]/2
    c = a[2]
    d = a[3]/2
    f = a[4]/2
    g = a[5]
    h = a[0]
    up = 2*(h*f*f + c*d*d + g*b*b - 2*b*d*f - h*c*g)
    down1 = (b*b - h*c)*((c - h)*numpy.sqrt(1 + 4*b*b/((h-c)*(h-c))) - (c+h))
    down2 = (b*b - h*c)*((h-c)*numpy.sqrt(1 + 4*b*b/((h-c)*(h-c))) - (c+h))
    res1 = numpy.sqrt(up/down1)
    res2 = numpy.sqrt(up/down2)
    return numpy.array([res1, res2])


def func_poly(x, a, b, c):
    """
    Name:     func_poly
    Inputs:   - numpy.ndarray, x values (x)
              - float, coeff (a)
              - float, coeff (b)
              - float, const (c)
    Outputs:  numpy.ndarray, y values
    Features: Returns y = ax^2 + bx + c
    """
    return float(a)*numpy.power(x, 2.) + float(b)*x + float(c)


def get_ctime(file_path):
    """
    Name:     get_ctime
    Inputs:   str, file path (file_path)
    Outputs:  datetime.datetime object (my_date)
    Features: Returns datetime object of file's *creation* time
    Note:     Creation time is not actual file creation time on *nix systems,
              rather, it refers to the last time the inode data changed
    """
    try:
        my_date = datetime.datetime.strptime(
            time.ctime(os.path.getctime(file_path)),
            "%a %b %d %H:%M:%S %Y")
    except OSError:
        my_date = ""
    finally:
        return my_date


def resource_path(relative_path):
    """
    Name:     resource_path
    Inputs:   str, resource file with path (relative_path)
    Outputs:  str, resource file with a potentially modified path
    Features: Returns absolute path for a given resource, works for dev and
              for PyInstaller.
    """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except AttributeError:
        base_path = os.path.join(sys.path[0], 'prida')
    return os.path.join(base_path, relative_path)


def read_exif_tags(img):
    """
    Name:     read_exif_tags
    Inputs:   PIL.Image (img)
    Outputs:  dict, exif tag values (my_tags)
    Features: Returns dictionary of image exif tags
    """
    # Define Exif Tags:
    my_tags = {"Make": "",                 # Exif Tag (IFD0) 271
               "Model": "",                # Exif Tag (IFD0) 272
               "Orientation": "",          # Exif Tag (IFD0) 274
               "DateTime": "",             # Exif Tag (IFD0) 306
               "ISOSpeedRatings": "",      # Exif Tag (SubIFD) 34855
               "ShutterSpeedValue": "",    # Exif Tag (SubIFD) 37377
               "ApertureValue": ""}        # Exif Tag (SubIFD) 37378

    # TIFF images do not support Exif Tags
    logging.debug("received image of format %s", img.format)
    try:
        logging.debug("reading image tags")
        exif = {ExifTags.TAGS[k]: v for k, v in img._getexif().items()
                if k in ExifTags.TAGS}
    except:
        # Unable to gather exif tags from image
        logging.exception("failed to find exif tags")
        exif = {}
    else:
        for tag in my_tags:
            try:
                my_attr = exif.get(tag, "")
            except:
                my_attr = ""
            else:
                # Handle tuple values:
                if isinstance(my_attr, tuple):
                    # Try reading first entry:
                    try:
                        my_attr = str(my_attr[0])
                    except:
                        my_attr = ""
                else:
                    try:
                        my_attr = str(my_attr)
                    except:
                        my_attr = ""

                if tag == "DateTime":
                    try:
                        # Parse date from datetime:
                        im_date = my_attr.split(' ')[0]
                        my_attr = im_date.replace(":", "-")
                    except:
                        my_attr = ""
                my_tags[tag] = my_attr
    return my_tags


def conf_parser(obj_inst, obj_name, config_file):
    """
    Name:    conf_parser
    Feature: Parses config file for relevant entries and assigns object
             attributes the user defined values found in the file
    Inputs:  - obj, instance of object to configure (obj_inst)
             - str, name of script calling the parser (obj_name)
             - str, absolute path to config file (config_file)
    Outputs: None
    """
    # Track whether we found a matching entry in the config file:
    found_match = False
    try:
        with open(config_file, 'r') as my_conf:
            for line in my_conf.readlines():
                words = line.split()

                # Check config file line formating
                if not len(words) == 3:
                    obj_inst.logger.error(
                        'config file poorly conditioned, check formatting.')
                    raise IOError(
                        'config file poorly conditioned, check formatting.')

                # Only access valid lines that match script/module name:
                if obj_name.upper() == words[0]:
                    found_match = True

                    # Check class attribute dictionary for valid entry:
                    if words[1] in obj_inst.attr_dict.keys():

                        # Double check that class has the attribute variable:
                        if hasattr(obj_inst, obj_inst.attr_dict[words[1]]):
                            try:

                                # Check if OS is windows and
                                # the attribute is a directory:
                                if os.name is 'nt' and '_DIR' in words[1]:

                                    # Replace escape characters:
                                    words[2] = words[2].replace(os.sep, '/')

                                # Set attribute value:
                                exec("obj_inst.%s = %s" % (
                                    obj_inst.attr_dict[words[1]], words[2]))
                                obj_inst.logger.debug("%s.%s = %s" % (
                                    obj_name,
                                    obj_inst.attr_dict[words[1]],
                                    words[2]))
                            except (ValueError, TypeError, AttributeError):
                                obj_inst.logger.exception(
                                    "failed to set %s.%s to %s" % (
                                        obj_name,
                                        obj_inst.attr_dict[words[1]],
                                        words[2]))
                            else:
                                obj_inst.logger.debug("%s.%s = %s" % (
                                    obj_name,
                                    obj_inst.attr_dict[words[1]],
                                    words[2]))
                        else:
                            obj_inst.logger.warning(
                                "%s does not have attribute %s" % (
                                    obj_name, obj_inst.attr_dict[words[1]]))
                    else:
                        obj_inst.logger.warning(
                            "%s is not an configurable attribute of %s" % (
                                words[1], obj_name))
                else:
                    obj_inst.logger.debug("ignoring %s", line.rstrip())
    except IOError:
        obj_inst.logger.exception(
            "Could not find config file at '%s'! Using defaults.", config_file)
    else:
        if not found_match:
            obj_inst.logger.warning("%s not found in config file!", obj_name)


def init_pid_attrs():
    """
    Name:     init_pid_attrs
    Inputs:   None.
    Outputs:  None.
    Features: Returns the PID attribute dictionary
    """
    pid_attrs = {
        1: {'title': 'USDA PLANTS',
            'key': 'usda_plants',
            'qt_val': 'c_usda_plants',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        2: {'title': 'Genus Species',
            'key': 'gen_sp',
            'qt_val': 'c_gen_sp',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        3: {'title': 'Line',
            'key': 'line',
            'qt_val': 'c_line',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        4: {'title': 'Rep',
            'key': 'rep_num',
            'qt_val': 'c_rep_num',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        5: {'title': 'Tub ID',
            'key': 'tubid',
            'qt_val': 'c_tubid',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        6: {'title': 'Germination Date',
            'key': 'germdate',
            'qt_val': 'c_germdate',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        7: {'title': 'Transplant Date',
            'key': 'transdate',
            'qt_val': 'c_transdate',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        8: {'title': 'Treatment',
            'key': 'treatment',
            'qt_val': 'c_treatment',
            'def_val': 'Control',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        9: {'title': 'Media',
            'key': 'media',
            'qt_val': 'c_media',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        10: {'title': 'Tub Size',
             'key': 'tubsize',
             'qt_val': 'c_tubsize',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        11: {'title': 'Nutrient',
             'key': 'nutrient',
             'qt_val': 'c_nutrient',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        12: {'title': 'Growth Temp (Day)',
             'key': 'growth_temp_day',
             'qt_val': 'c_growth_temp_day',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        13: {'title': 'Growth Temp (Night)',
             'key': 'growth_temp_night',
             'qt_val': 'c_growth_temp_night',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        14: {'title': 'Lighting Conditions',
             'key': 'growth_light',
             'qt_val': 'c_growth_light',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        15: {'title': 'Watering schedule',
             'key': 'water_sched',
             'qt_val': 'c_water_sched',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        16: {'title': 'Damage',
             'key': 'damage',
             'qt_val': 'c_damage',
             'def_val': 'No',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        17: {'title': 'Plant Notes',
             'key': 'notes',
             'qt_val': 'c_plant_notes',
             'def_val': 'N/A',
             'cur_val': '',
             'qt_type': 'QLineEdit'}
    }
    return pid_attrs


def init_session_attrs():
    """
    Name:     init_session_attrs
    Inputs:   None.
    Outputs:  None.
    Features: Returns the session attribute dictionary
    """
    session_attrs = {
        0: {'title':  'Session User',
            'key': 'user',
            'qt_val': 'c_session_user',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        1: {'title': 'Session Email',
            'key': 'addr',
            'qt_val': 'c_session_addr',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        2: {'title': 'Session Title',
            'key': 'number',
            'qt_val': 'c_session_number',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        3: {'title': 'Session Date',
            'key': 'date',
            'qt_val': 'c_session_date',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QDateEdit'},
        4: {'title': 'Rig Name',
            'key': 'rig',
            'qt_val': 'c_session_rig',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        5: {'title': 'Image Count',
            'key': 'num_img',
            'qt_val': 'c_num_images',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        6: {'title': 'Plant Age',
            'key': 'age_num',
            'qt_val': 'c_plant_age',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        7: {'title': 'Camera Make',
            'key': 'cam_make',
            'qt_val': 'c_cam_make',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        8: {'title': 'Camera Model',
            'key': 'cam_model',
            'qt_val': 'c_cam_model',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        9: {'title': 'Exposure Time',
            'key': 'cam_shutter',
            'qt_val': 'c_cam_shutter',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        10: {'title': 'Aperture',
             'key': 'cam_aperture',
             'qt_val': 'c_cam_aperture',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        11: {'title': 'ISO',
             'key': 'cam_exposure',
             'qt_val': 'c_cam_exposure',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        12: {'title': 'Tank Distance',
             'key': 'cam_dist',
             'qt_val': 'c_cam_dist',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        13: {'title': 'Orientation',
             'key': 'img_orient',
             'qt_val': 'c_img_orient',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QComboBox'},
        14: {'title': 'Exclude',
             'key': 'exclude',
             'qt_val': 'c_exclude',
             'def_val': 'False',
             'cur_val': '',
             'qt_type': 'QComboBox'},
        15: {'title': 'Notes',
             'key': 'notes',
             'qt_val': 'c_session_notes',
             'def_val': 'N/A',
             'cur_val': '',
             'qt_type': 'QLineEdit'}
    }
    return session_attrs
