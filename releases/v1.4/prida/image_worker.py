#!/usr/bin/python
#
# image_worker.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software is freely available to the public for use.      #
# The Department of Agriculture (USDA) and the U.S. Government have not       #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     USDA-Agricultural Research Service                                      #
#     Robert W. Holley Center for Agriculture and Health                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os

import numpy
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QObject
import scipy.misc
import scipy.optimize

from .utilities import element
from .utilities import ellipse_center
from .utilities import ellipse_angle_of_rotation
from .utilities import ellipse_axis_length
from .utilities import fitEllipse
from .utilities import func_line
from .utilities import func_poly


###############################################################################
# CLASS:
###############################################################################
class ImageWorker(QObject):
    """
    Name:     ImageWorker
    Features: This class is a worker object used for image processing.
    History:  Version 1.4.0
              - created [16.03.22]
              - created reset function [16.04.04]
              - added Otsu threshold to display message [16.04.04]
              - created check proc vals functin [16.04.07]
              - moved invert image to combo box selection [16.04.07]
              - updated Otsu to correct foreground always to black [16.04.14]
              - removed invert option [16.04.14]
              - created set image shape signal [16.04.14]
              - complete rework of class [16.04.19]
              - added reset call to start [16.04.20]
              - only process to binary if not already [16.04.20]
              - find object now takes an array [16.04.21]
              - added second try to scale image [16.04.22]
              - added datasets class variable [16.04.22]
              - changed the signal/socket for getting merge images [16.04.22]
              - changed hyphen to en dash in progress bar [16.04.22]
              - created export data function [16.04.26]
              - created calc roll function [16.04.27]
              - updated variable tau to dt (translation) [16.05.02]
              - finished calc hf prime function [16.05.02]
              - added filename check in export [16.05.02]
              - adjusted error prop calcs [16.05.02]
              - added crop ROI and kernel width as class variables [16.05.02]
              - updated/fixed roll equations [16.05.17]
              - fixed merge counter [16.05.18]
              - added gap fill function [16.05.18]
              - addressed division by zero error in set images [16.05.24]

    @TODO:    * finish export data function
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    finished = pyqtSignal()
    getProcImg = pyqtSignal(str, int)
    isFound = pyqtSignal(int)
    isMerged = pyqtSignal(int, int)
    isReady = pyqtSignal(numpy.ndarray)
    listDatasets = pyqtSignal(str)
    setBGColor = pyqtSignal(str)
    setImgRange = pyqtSignal(str)
    setImgShape = pyqtSignal(str)
    updateProc = pyqtSignal(str)
    sendAlert = pyqtSignal(str)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None, mutex=None):
        """
        Name:     ImageWorker.__init__
        Inputs:   - [optional] Qt parent object (parent)
                  - [optional] Qt mutex lock (mutex)
        Returns:  None.
        Features: Class initialization
        """
        super(ImageWorker, self).__init__(parent)

        # Create a worker logger:
        self.logger = logging.getLogger(__name__)
        self.logger.debug("image worker object initialized")

        # Initialize variables
        self.mutex = mutex         # Qt mutex lock
        self.datasets = []         # list of HDF5 dataset paths
        self.elements = []         # list of element objects
        self.proc_img = None       # processed image array
        self.merge_img = None      # merged image array

        self.is_cropped = False    # boolean for cropped images
        self.is_binary = False     # boolean for binary images
        self.is_merged = False     # boolean for merged images
        self.is_stretched = False  # boolean for stretched images

        self.bin_thresh = 0        # Otsu's threshold value
        self.bg_color = 0          # background color for thresholding
        self.crop_left = 0         # image crop column left
        self.crop_right = 0        # image crop column right
        self.crop_top = 0          # image crop row top
        self.crop_bottom = 0       # image crop tow bottom
        self.kernel = 0            # kernel width
        self.merge_count = 0       # image counter for merging
        self.path = None           # HDF5 path (session or image)
        self.sfactor = 100         # scaling factor

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def calc_hf_prime(self):
        """
        Name:     ImageWorker.calc_hf_prime
        Inputs:   None.
        Outputs:  tuple, HF' and its associated std error
        Features: Returns estimate of HF', the pixel coordinate of the observed
                  center of focus above/below the center of image
        """
        num_e = len(self.elements)
        if num_e > 0:
            self.logger.debug("extracting element data")
            center_points = []
            element_heights = []
            for i in range(num_e):
                obj = self.elements[i]
                center_points.append(obj.center)
                element_heights.append(obj.height)
            self.logger.debug("creating x-y arrays")
            x_array = numpy.array([point[0] for point in center_points])
            y_array = numpy.array(element_heights)
            min_y = y_array.min()  # seed value for 'c' parameter in poly

            try:
                self.logger.debug("performing curve fit")
                p_opt, p_cov = scipy.optimize.curve_fit(
                    func_poly, x_array, y_array, (1., 0., min_y)
                )
            except:
                self.logger.exception("curve fit failed")
                hf_prime = None
                hf_prime_err = None
            else:
                a, b, c = p_opt
                self.logger.info("poly a = %f", a)
                self.logger.info("poly b = %f", b)
                self.logger.info("poly c = %f", c)

                # Get coordinates for center of image:
                h_coi, w_coi = self.elements[0].coi

                try:
                    self.logger.debug(
                        "calculating estimate standard errors")
                    std_err = numpy.sqrt(numpy.diag(p_cov))
                except:
                    self.logger.debug(
                        "failed to calculate standard error")
                    a_err = 0.
                    b_err = 0.
                    c_err = 0.
                else:
                    a_err, b_err, c_err = std_err
                    self.logger.info("a_err = %f", a_err)
                    self.logger.info("b_err = %f", b_err)
                    self.logger.info("c_err = %f", c_err)

                # Setting derivative of parabola equation equal to zero and
                # solving for the pixel coordinate of the root (minimization)
                # NOTE: HF' is positive above the COI
                try:
                    self.logger.debug("calculating hf prime")
                    hf_prime = h_coi
                    hf_prime -= (-1.0 * b) / (2.0 * a)
                except:
                    self.logger.exception("HF' calculation failed")
                    hf_prime = None
                    hf_prime_err = None
                else:
                    self.logger.info("hf_prime = %f", hf_prime)

                    try:
                        self.logger.debug("calculating std error")
                        dhpda = b / (2.0 * a * a)
                        dhpdb = -1.0 / (2.0 * a)
                        aerr2 = (dhpda * a_err) * (dhpda * a_err)
                        berr2 = (dhpdb * b_err) * (dhpdb * b_err)
                        hf_prime_err = numpy.sqrt(aerr2 + berr2)
                    except:
                        self.logger.debug(
                            "HF' std error calculation failed")
                        hf_prime_err = 0.
                    else:
                        self.logger.info(
                            "hf_prime_err = %f", hf_prime_err)
        else:
            self.logger.warning("no elements for calculation")
            hf_prime = None
            hf_prime_err = None

        return (hf_prime, hf_prime_err)

    def calc_roll_translation(self):
        """
        Name:     ImageWorker.calc_roll_translation
        Inputs:   None.
        Outputs:  tuple, roll and its standard error, degrees, and Dt and its
                  standard error, pixels (theta, theta_err, dt, dt_err)
        Features: Calculates the roll, theta, and translation, Dt, of the
                  calibration elements and their associated standard errors
        """
        num_e = len(self.elements)
        if num_e > 0:
            self.logger.debug(
                "extracting center points of %d elements", num_e)
            center_points = []
            for i in range(num_e):
                obj = self.elements[i]
                center_points.append(obj.center)
            self.logger.debug("calculating center max and min")
            h_points = numpy.array([point[0] for point in center_points])
            h_max = h_points.max()
            y_array = numpy.array([h_max - h for h in h_points])

            w_points = numpy.array([point[1] for point in center_points])
            w_min = w_points.min()
            x_array = numpy.array([w - w_min for w in w_points])

            try:
                self.logger.debug("performing curve fitting")
                p_opt, p_cov = scipy.optimize.curve_fit(
                    func_line, x_array, y_array, (1., 0.))
            except:
                self.logger.exception("curve fit failed")
                theta = None
                theta_err = None
                dt = None
                dt_err = None
            else:
                m, b = p_opt
                self.logger.info("slope = %f", m)
                self.logger.info("intercept = %f", b)
                try:
                    self.logger.debug(
                        "calculating estimate standard errors")
                    std_err = numpy.sqrt(numpy.diag(p_cov))
                except:
                    self.logger.exception(
                        "failed to calculate standard error")
                    m_err = 0.
                    b_err = 0.
                else:
                    m_err, b_err = std_err
                    self.logger.info("slope error = %f", m_err)
                    self.logger.info("intercept error = %f", b_err)

                # Calculate roll:
                try:
                    self.logger.debug("calculating roll")
                    pir = 180.0 / numpy.pi
                    theta = pir * numpy.sign(m)
                    theta *= numpy.pi / 2. - numpy.abs(numpy.arctan(m))
                    theta_err = -1.0 * pir * numpy.sign(m) * m_err
                    theta_err /= 1. + numpy.power(m, 2.)
                except:
                    self.logger.exception(
                        "roll calculation failed")
                    theta = None
                    theta_err = None
                else:
                    self.logger.info(
                        "roll = %f degrees", theta)
                    self.logger.info(
                        "roll error = %f degrees", theta_err)

                # Calculate translation:
                self.logger.debug("calculating translation")
                h_coi, w_coi = self.elements[0].coi
                self.logger.info(
                    "center of image = (%d, %d)" % (
                        h_coi, w_coi))

                # Convert COI to our pseudo x-y coordinates
                y_coi = h_max - h_coi
                x_coi = w_coi - w_min

                # Calculate x based on y_coi:
                x_hat = (y_coi - float(b)) / m

                # Translation:
                dt = x_coi - x_hat
                self.logger.info("Dt = %f", dt)

                # Error propagation:
                try:
                    dtdb = 1. / m
                    berr2 = (dtdb * b_err) * (dtdb * b_err)
                    dtdm = -1. * b / (m * m)
                    merr2 = (dtdm * m_err) * (dtdm * m_err)
                    dt_err = numpy.sqrt(berr2 + merr2)
                except:
                    self.logger.exception(
                        "error propagation failed")
                    dt_err = 0.
                else:
                    self.logger.info("Dt error = %f", dt_err)
            finally:
                return(theta, theta_err, dt, dt_err)
        else:
            self.logger.warning("no elements for calculation")
            return(None, None, None, None)

    def crop(self, tidx, bidx, lidx, ridx):
        """
        Name:     ImageWorker.crop
        Inputs:   - int, top row index (tidx)
                  - int, bottom row index (bidx)
                  - int, left column index (lidx)
                  - int, right column index (ridx)
        Outputs:  None.
        Features: Crops merge image
        Depends:  update_image_range
        """
        if self.merge_img is not None:
            self.crop_left = lidx
            self.crop_right = ridx
            self.crop_top = tidx
            self.crop_bottom = bidx

            h, w = self.merge_img.shape
            if tidx < 0 or tidx >= bidx or bidx > h:
                self.sendAlert.emit(
                    "Please select a top index less than the bottom index.")
                self.updateProc.emit("Image Processing Ready")
            elif lidx < 0 or lidx >= ridx or ridx > w:
                self.sendAlert.emit(
                    "Please select left index less than right index.")
                self.updateProc.emit("Image Processing Ready")
            else:
                self.proc_img = numpy.copy(self.merge_img)
                self.proc_img = self.proc_img[tidx:bidx, lidx:ridx]
                self.is_cropped = True
                self.updateProc.emit("Cropped merge image")
                self.update_image_range()
                self.isReady.emit(self.proc_img)
        else:
            self.sendAlert.emit("Please first load merge image.")
            self.updateProc.emit("Image Processing Ready")
            self.crop_left = 0
            self.crop_right = 0
            self.crop_top = 0
            self.crop_bottom = 0

    def export_data(self, out_dict):
        """
        Name:     ImageWorker.export_data
        Inputs:   dict, output values (out_dict)
        Outputs:  None.
        Features: Saves output values to file
        Depends:  - calc_hf_prime
                  - calc_roll_translation

        @TODO
        """
        self.logger.debug("preparing export data")
        file_path = out_dict.get('path', '')
        bname = out_dict.get('name', '')
        sdate = out_dict.get('date', '')
        if self.bg_color == 0:
            background = "Black"
        elif self.bg_color == 1:
            background = "White"
        else:
            background = "N/A"

        # Header column names:
        col_names = ['Name', 'Date', 'ImgCount', 'ImgHeight', 'ImgWidth',
                     'Scale', 'Background', 'ROI_top', 'ROI_bottom',
                     'ROI_left', 'ROI_right', 'Otsu', 'KernelWidth',
                     'ElementCount', 'Transl_px', 'TranslErr_px', 'Roll_deg',
                     'RollErr_deg', 'HFprime_px', 'HFprimeErr_px']

        # Column data (NOTE: everything must be a string in order to use join):
        export_dict = {
            'Name': str(bname),
            'Date': str(sdate),
            'ImgCount': str(self.merge_count),
            'ImgHeight': str(self.merge_img.shape[0]),
            'ImgWidth': str(self.merge_img.shape[1]),
            'Scale': str(self.sfactor),
            'Background': background,
            'ROI_top': str(self.crop_top),
            'ROI_bottom': str(self.crop_bottom),
            'ROI_left': str(self.crop_left),
            'ROI_right': str(self.crop_right),
            'Otsu': str(self.bin_thresh),
            'KernelWidth': str(self.kernel),
            'ElementCount': str(len(self.elements)),
        }
        header_line = ';'.join(col_names)
        output_vals = [export_dict.get(i, '') for i in col_names]

        self.logger.debug("preparing to write to %s", file_path)

        if len(self.elements) == 0:
            self.logger.debug("no elements found, exporting ...")
            output_string = ";".join(output_vals)
        else:
            self.logger.debug("found %d elements, exporting ...",
                              len(self.elements))

            # Perform calibration:
            self.logger.debug("calculating roll and translation")
            roll, roll_err, dt, dt_err = self.calc_roll_translation()
            if roll is not None and dt is not None:
                export_dict["Roll_deg"] = str(roll)
                export_dict["RollErr_deg"] = str(roll_err)
                export_dict["Transl_px"] = str(dt)
                export_dict["TranslErr_px"] = str(dt_err)
                output_vals = [export_dict.get(i, '') for i in col_names]

            self.logger.debug("calculating HFprime")
            hf_prime, hf_prime_err = self.calc_hf_prime()
            if hf_prime is not None:
                export_dict["HFprime_px"] = str(hf_prime)
                export_dict["HFprimeErr_px"] = str(hf_prime_err)
                output_vals = [export_dict.get(i, '') for i in col_names]

            # Update headerline and output values for each element
            element_header = ""
            for i in range(len(self.elements)):
                idx = str(i + 1)
                element_header += "".join([";Element-", idx, "_size"])
                element_header += "".join([";Element-", idx, "_height"])
                element_header += "".join([";Element-", idx, "_width"])

                obj = self.elements[i]
                try:
                    output_vals += [str(obj.size),
                                    str(obj.height),
                                    str(obj.width)]
                except:
                    self.logger.exception("###ELEMENT FAILED###")
            header_line += element_header

            # Create output string:
            try:
                self.logger.debug("creating output string")
                output_string = ";".join(output_vals)
            except:
                self.logger.exception("###FAILED###")
                output_string = ""

            # NOTE: temporary testing function:
            self.plot_centers()

        # Prepare the output file name (update is already exists)
        if os.path.isfile(file_path):
            self.logger.debug("appending to existing file")
            to_append = True
        else:
            self.logger.debug("creating export file")
            to_append = False

        try:
            self.logger.debug("writing to file")
            my_file = open(file_path, 'a')
            if not to_append:
                my_file.write(header_line)
                my_file.write("\n")
            my_file.write(output_string)
            my_file.write("\n")
        except:
            self.logger.exception(
                "failed to write to file %s", file_path)
        else:
            self.logger.debug("successfully exported data")
        finally:
            my_file.close()

    def find_nearest(self, my_array, coord, visited):
        """
        Name:     ImageWorker.find_nearest
        Inputs:   - numpy.ndarray, a binary image array (my_array)
                  - tuple, starting location coordinates (coord)
                  - set, coordinates already visited (visited)
        Outputs:  set, unvisted neighboring black pixel coords (neighbors)
        Features: Returns neighboring black pixel coordinates that have yet to
                  be visited
        """
        y, x = coord
        h, w = my_array.shape
        neighbors = []

        # determine cardinal neighbor coordinates
        cards = [(y, x - 1), (y - 1, x), (y + 1, x), (y, x + 1)]

        # for each location, check if it's in the image, black and unvisted
        for loc in cards:
            if (loc[0] >= 0 and loc[0] < h):
                if (loc[1] >= 0 and loc[1] < w):
                    if my_array[loc] == 0:
                        if loc not in visited:
                            neighbors.append(loc)
        return set(neighbors)

    def find_neighbors(self, my_array, coord):
        """
        Name:     ImageWorker.find_neighbors
        Inputs:   - numpy.ndarray, binary image array (my_array)
                  - tuple(int, int), starting location coordinates (coord)
        Outputs:  set, coordinates of connected pixels with given location
        Features: Finds all black pixels neighboring the given pixel
        Depends:  find_nearest
        """
        self.logger.debug("finding neighbors of %s", str(coord))
        visited = set()
        to_visit = set()
        visited.add(coord)
        to_visit.update(self.find_nearest(my_array, coord, visited))

        # loop untill all neighbors to visit have been visited
        while len(to_visit.difference(visited)) != 0:
            # accumulate new neighbors
            new_neighbors = set()
            for loc in to_visit:
                # update visited list and add its new neighbors
                visited.add(loc)
                new_neighbors.update(self.find_nearest(my_array, loc, visited))
            # remove visited pixels from new to visit list
            to_visit = new_neighbors
        return visited

    def find_objects(self, my_array):
        """
        Name:     ImageWorker.find_objects
        Inputs:   numpy.ndarray, image array (my_array)
        Outputs:  None.
        Features: Searches the binary process image for calibration objects
        Depends:  - find_neighbors
                  - investigate
        """
        self.logger.debug("finding objects ...")
        self.investigate(my_array)
        if self.is_binary:
            my_img = numpy.ones(my_array.shape)
            my_img *= 255
            self.isReady.emit(my_img)

            # determine the central width pixel:
            h, w = my_img.shape
            m = int(0.5 * w)

            # initialize variables:
            self.elements = []
            visited = set()
            num_found = 0
            for p in range(h):
                loc = (p, m)
                if my_array[loc] == 0 and loc not in visited:
                    # New black pixel found! perform next sweep
                    num_found += 1
                    self.updateProc.emit(
                        "Searching ... found %d objects" % (num_found))
                    self.logger.debug(
                        "found new object starting at %s",
                        str(loc))
                    new_e = element(
                        data=self.find_neighbors(my_array, loc),
                        shape=(h, w))
                    self.elements.append(new_e)
                    visited.update(new_e._orig)
                    # Update element image:
                    my_img = numpy.minimum(my_img, new_e.img)
            self.updateProc.emit("Found %d objects!" % (num_found))
            self.isReady.emit(my_img)
            self.isFound.emit(num_found)
        else:
            self.logger.warning(
                "objects can only be found in binary images")
            self.sendAlert.emit("Please first threshold the image.")
            self.updateProc.emit("Image Processing Ready")

    def gap_fill(self, my_array):
        """
        Name:     ImageWorker.gap_fill
        Inputs:   numpy.ndarray, binary image (my_array)
        Outputs:  None.
        Features: Gap fills binary image; for each background pixel (white), if
                  there exists a foreground pixel (black) both to the right and
                  to the left on the same side of the median line within the
                  same row, then convert the background pixel to foreground;
                  signals the gapfilled image back to GUI
        Depends:  investigate
        """
        self.logger.debug("caught toGapfill signal")
        self.investigate(my_array)
        if self.is_binary:
            # Create a copy of image for working on:
            my_img = numpy.copy(my_array)

            # Extract shape info:
            h, w = my_array.shape

            # Set background pixel to white:
            fg = my_array.min()  # aka 0

            # Find row range for foreground:
            fg_idx = numpy.where(my_array == fg)
            row_min = fg_idx[0].min()
            row_max = fg_idx[0].max()
            row_range = row_max - row_min + 1

            # Find the indexes for foreground rows:
            row_count = 0
            fg_rows = []
            for row in range(row_min, row_max + 1):
                if fg in my_array[row, :]:
                    row_count += 1
                    fg_rows.append(row)

            # Calculate the allowance; this is 25% of the estimated white
            # space between elements (the tallest stack is 15 rings)
            allowance = int((row_range - row_count) / (4. * (15 - 1)) + 0.5)

            # Estimate the number of elements:
            ring_starts = [fg_rows[0], ]
            ring_ends = []
            for i in range(row_count - 1):
                search_failed = False
                for j in range(allowance):
                    if fg_rows[i] + j == fg_rows[i + 1]:
                        search_failed = True
                if not search_failed:
                    ring_starts.append(fg_rows[i + 1])
                    ring_ends.append(fg_rows[i])
            ring_ends.append(fg_rows[-1])
            num_rings = len(ring_starts)
            self.logger.debug(
                "counted %d element regions", num_rings)

            old_z = -1
            for j in range(num_rings):
                # Update progress bar:
                z = 100.0 * (j - 1.0) / (num_rings - 1)
                if int(z) % 5 == 0 and int(z) != old_z:
                    msg1 = "Gapfilling ... "
                    msg2 = "[{}{}] {}%".format(
                        '#' * int(z / 5), '\u2013' * (20 - int(z / 5)), int(z))
                    msg = msg1 + msg2
                    self.logger.debug(msg)
                    self.updateProc.emit(msg)
                    old_z = int(z)

                # Pull starting and ending row numbers from above:
                start_row = ring_starts[j]
                end_row = ring_ends[j]

                # Find foreground pixels for these rows to get start/end cols:
                ring_idx = numpy.where(
                    my_array[start_row:end_row + 1, :] == fg)
                start_col = ring_idx[1].min()
                end_col = ring_idx[1].max()

                # Calculate ring height and width & respective centers:
                # ring_width = end_col - start_col + 1
                # ring_height = end_row - start_row + 1
                center_row = int((start_row + end_row) / 2 + 0.5)
                center_col = int((start_col + end_col) / 2 + 0.5)

                # Find foreground pixels within this bounding box:
                ring_points = numpy.where(
                    my_array[start_row:end_row + 1,
                             start_col:end_col + 1] == fg)

                # Change coordinate system centered at ellipse:
                y_points = ring_points[0] + start_row - center_row
                x_points = ring_points[1] + start_col - center_col

                # Try ellipse fitting based on Fischer et al. (1996).
                # Direct least squares fitting of ellipses, in Proc. 13th
                # Intl. Pattern Recognition, 253--257, Vienna.
                # @TODO: check these methods:
                arr = fitEllipse(x_points, y_points)
                center = ellipse_center(arr)
                phi = ellipse_angle_of_rotation(arr)
                axes = ellipse_axis_length(arr)
                #

                #
                # @TODO what to do from here on?
                # Convert x-y points back to image rows and cols:
                # for i in range(len(x_vals)):
                #    x_val = int(x_vals[i])
                #    y_val = func_ellipse(x_val, ell_a, ell_b)
                #    y_val_pos = int(y_val + 0.5)
                #    y_val_neg = int(-1.0*y_val - 0.5)
                #    ring_col = x_val + center_col
                #    ring_row_pos = y_val_pos + center_row
                #    ring_row_neg = y_val_neg + center_row
                #    my_img[ring_row_neg, ring_col] = fg
                #    my_img[ring_row_pos, ring_col] = fg

        self.updateProc.emit("Image gapfilled")
        self.isReady.emit(my_img)

    def get_element_array(self, idx):
        """
        Name:     ImageWorker.get_element_array
        Inputs:   int, element list index (idx)
        Outputs:  None.
        Features: Catches Prida's getObject signal to display a given element
                  and signals the array back to the GUI for preview
        """
        self.logger.debug("caught signal %d", idx)
        try:
            if idx >= 0:
                self.updateProc.emit("Showing object %d" % (idx + 1))
                obj = self.elements[idx]
                self.isReady.emit(obj.img)
            else:
                obj = self.elements[0]
                my_img = numpy.copy(obj.img)
                for obj in self.elements:
                    my_img = numpy.minimum(my_img, obj.img)
                self.updateProc.emit(
                    "Showing %d objects" % (len(self.elements)))
                self.isReady.emit(my_img)
        except IndexError:
            self.logger.exception("Failed to get element array")
            self.sendAlert.emit("Please first find objects.")
        except:
            self.logger.exception("Encountered unknown error")
            raise

    def gray_luminance(self, nd_array):
        """
        Name:     ImageWorker.gray_luminance
        Inputs:   numpy.ndarray, RGB color image array (nd_array)
        Outputs:  numpy.ndarray, 2D grayscale image array (my_gray)
        Features: Converts color image to grayscale based on a weighted sum
                  of the color bands as a calculation of luminance
        """
        if len(nd_array.shape) == 3:
            try:
                R = nd_array[:, :, 0]
                G = nd_array[:, :, 1]
                B = nd_array[:, :, 2]
            except:
                self.logger.exception(
                    "failed to unpack color bands")
                Y = nd_array
            else:
                self.logger.info(
                    "converting color to luminance grayscale")
                Y = (0.299 * R) + (0.587 * G) + (0.114 * B)
            finally:
                return Y
        else:
            self.logger.info("skipping grayscale image")
            return nd_array

    def invert_image(self, nd_array):
        """
        Name:     ImageWorker.invert_image
        Inputs:   numpy.ndarray
        Outputs:  numpy.ndarray
        Features: Inverts grayscale or binary image colors
        """
        if len(nd_array.shape) == 2:
            self.logger.info("inverting image")
            if nd_array.max() == 1:
                temp_array = nd_array.copy()
                temp_array = 1 - temp_array
            else:
                temp_array = nd_array.copy()
                temp_array = 255 - temp_array
            return temp_array

    def investigate(self, my_array):
        """
        Name:     ImageWorker.investigate
        Inputs:   numpy.ndarray, image array (my_array)
        Outputs:  None.
        Features: Checks an image array for grayscale and binary
        """
        if isinstance(my_array, numpy.ndarray):
            self.logger.debug("checking if image is grayscale")
            if len(my_array.shape) == 2:
                self.logger.debug("... yes")

                my_hist = numpy.histogram(my_array)[0]
                num_pos_bins = len(numpy.where(my_hist != 0)[0])
                self.logger.debug("checking if image is binary")
                if num_pos_bins == 1 or num_pos_bins == 2:
                    self.logger.debug("... yes")
                    self.is_binary = True
                else:
                    self.logger.debug("... no")
                    self.is_binary = False
            else:
                self.logger.debug("... no")
                self.is_binary = False
        else:
            raise TypeError("Function parameter must be an array.")

    def make_binary(self):
        """
        Name:     ImageWorker.make_binary
        Inputs:   None.
        Outputs:  None.
        Features: Converts process image to binary image
        Depends:  - gray_luminance
                  - otsu
        """
        self.investigate(self.proc_img)
        if not self.is_binary:
            img_dim = len(self.proc_img.shape)
            self.logger.debug("image has dimension %d", img_dim)
            if img_dim == 2:
                # Check grayscale values:
                my_array = numpy.clip(self.proc_img.astype("uint8"), 0, 255)
            elif img_dim == 3:
                self.logger.debug(
                    "converting color image to grayscale")
                my_array = self.gray_luminance(self.proc_img)
                my_array = numpy.clip(my_array.astype("uint8"), 0, 255)
            else:
                raise ValueError("Unhandled array dimension encountered")

            try:
                self.proc_img = self.otsu(my_array)
            except:
                self.logger.exception("convert to binary failed")
                self.isReady.emit(numpy.ndarray([]))
            else:
                self.updateProc.emit("Binary merged image (Otsu %d)" % (
                    self.bin_thresh))
                self.isReady.emit(self.proc_img)
        else:
            self.logger.debug(
                "skipping binary process; already binary")

    def median_filter(self, kwidth):
        """
        Name:     ImageWorker.median_filter
        Input:    int, kernel width (kwidth)
        Output:   None.
        Features: Performs median filter
        """
        self.logger.info("filtering with kernel size = %d", kwidth)
        if kwidth < 3:
            self.logger.info("skipping median filter, kernel = %d", kwidth)
            self.sendAlert.emit("Please select an odd kernel size.")
            self.updateProc.emit("Image Processing Ready")
            self.kernel = 0
        elif kwidth % 2 == 0:
            self.logger.error("kernel width (%d) must be odd number!", kwidth)
            self.sendAlert.emit("Please select an odd number for kernel size.")
            self.updateProc.emit("Image Processing Ready")
            self.kernel = 0
        elif self.proc_img is not None:
            self.kernel = kwidth
            hkernel = int(0.5 * (kwidth - 1))
            self.logger.debug("half-kernel size = %d", hkernel)

            img_dim = len(self.proc_img.shape)
            self.logger.debug("image dimension = %d", img_dim)

            temp_array = numpy.copy(self.proc_img)
            if img_dim == 2:
                h, w = self.proc_img.shape
                old_z = -1
                for y in range(h):
                    # Each column
                    z = 100.0 * (y - 1.0) / (h - 1.0)
                    if int(z) % 5 == 0 and int(z) != old_z:
                        msg1 = "Filtering ... "
                        msg2 = "[{}{}] {}%".format(
                            '#' * int(z / 5),
                            '\u2013' * (20 - int(z / 5)),
                            int(z))
                        msg = msg1 + msg2
                        self.logger.debug(msg)
                        self.updateProc.emit(msg)
                        old_z = int(z)
                    prev_row = numpy.zeros((w,))
                    cur_row = numpy.copy(self.proc_img[y, :])
                    while not numpy.array_equal(prev_row, cur_row):
                        prev_row = numpy.copy(temp_array[y, :])
                        for x in range(hkernel, w - hkernel, 1):
                            k_grid = [x + i - hkernel for i in range(kwidth)]
                            k_grid = numpy.array(k_grid)
                            k_vals = prev_row[k_grid]
                            k_median = numpy.median(k_vals)
                            cur_row[x] = k_median
                        temp_array[y, :] = cur_row
                self.updateProc.emit(
                    "Filtered merged image (kernel size = %d)" % (kwidth))
                self.isReady.emit(temp_array)
            else:
                self.logger.warning("encountered color image!")
                self.sendAlert.emit("Please try loading merge image again.")
                self.updateProc.emit("Image Processing Ready")
        else:
            self.logger.warning("process image is not set!")
            self.sendAlert.emit("Please first load merged image")
            self.updateProc.emit("Image Processing Ready")

    def merge(self, s_path, bg_color, sfactor):
        """
        Name:     ImageWorker.merge
        Inputs:   - str, session path (s_path)
                  - int, background color (bg_color)
                  - int, scale factor (sfactor)
        Outputs:  None.
        Features: Signals list datasets for merging images
        """
        self.logger.info("processing merged image")
        self.reset()

        self.path = s_path
        self.logger.info("setting path to %s", s_path)
        self.bg_color = bg_color
        self.logger.info("setting background to %d", bg_color)
        self.sfactor = int(sfactor)
        self.logger.info("setting scale factor to %d", sfactor)

        self.updateProc.emit("Processing ... listing datasets")
        self.listDatasets.emit(self.path)

    def otsu(self, nd_array):
        """
        Name:     ImageWorker.otsu
        Inputs:   numpy.ndarray, 2D integer array (nd_array)
        Outputs:  numpy.ndarray, two-dimensional integer array (my_binary)
        Features: Returns a binary image based on the Otsu (1979) method
        Ref:      Otsu, N. (1979) A threshold selection method from gray-level
                  histograms, IEEE Transactions on Systems, Man, and
                  Cybernetics, 9(1), 62--66.
        """
        # Otsu's max and min level values:
        if self.bg_color == 0:
            # Black background, make foreground objects black
            self.logger.info("Otsu black background")
            l_max = 0
            l_min = 255
        elif self.bg_color == 1:
            # White background, make forground objects black
            self.logger.info("Otsu white background")
            l_max = 255
            l_min = 0

        # Otsu's number of pixels at each level, n, total number of pixels, N,
        # and normalized probability distribution, p
        n = numpy.bincount(nd_array.astype("uint8").ravel(), minlength=256)
        N = n.sum()
        p = n.astype("f4") / N
        self.logger.debug("total number of pixels: %d", N)

        # Otsu's mean pixel value at each level, mu:
        mu = [i * p[i] for i in range(len(p))]
        mu = numpy.array(mu)

        # Otsu's probabilities of class occurrence, wk [Eq. 6], class mean
        # values, uk [Eq. 7], and the total mean level of the original
        # image, ut [Eq. 8]:
        wk = p.cumsum()
        uk = mu.cumsum()
        ut = mu.sum()

        # Calculate the between-class variances, vb [Eq. 18]:
        vb = ut * wk[:-1]
        vb -= uk[:-1]
        vb = numpy.power(vb, 2.0)
        denom = 1. - wk[:-1]
        denom *= wk[:-1]
        if 0 in denom:
            self.logger.warning(
                "Otsu found zeros in denominator, adjusting")
            denom[numpy.where(denom == 0)] += 1.e-3
        try:
            vb /= denom
        except:
            self.logger.exception("encountered division error")
            raise
        else:
            # Search for maximum between-class variance, i.e., threshold value:
            k = numpy.where(vb == vb.max())[0]
            if len(k) == 0:
                self.logger.error("failed to find maximum value")
                raise ValueError("No maximum value found in Otsu method")
            elif len(k) > 1:
                self.logger.warning(
                    "found %d maximum values", len(k))
                self.logger.warning("using first occurrence")
            thresh = k[0]
            self.bin_thresh = thresh
            self.logger.info(
                "Otsu threshold set at %d", int(thresh))

            # Find the foreground and background pixel values:
            my_binary = numpy.copy(nd_array).astype("f4")
            fore_idx = numpy.where(my_binary > thresh)
            back_idx = numpy.where(my_binary <= thresh)

            # Set foreground and background:
            my_binary[fore_idx] *= 0
            my_binary[fore_idx] += 1
            my_binary[fore_idx] *= l_max

            my_binary[back_idx] *= 0
            my_binary[back_idx] += 1
            my_binary[back_idx] *= l_min

            return my_binary.astype("uint8")

    def plot_centers(self):
        """
        TEMPORARY FUNCTION
        Plots elements with red dots at their centers and a blue dot at the COI
        """
        if len(self.elements) > 0:
            hf_prime, hf_prime_err = self.calc_hf_prime()

            temp_array = numpy.ones(
                (self.elements[0].shape[0],
                 self.elements[0].shape[1], 3)).astype('uint8')
            temp_array *= 255

            # Create merged object image:
            for obj in self.elements:
                min_array = numpy.minimum(temp_array[:, :, 0], obj.img)
                temp_array[:, :, 0] = numpy.copy(min_array)
                temp_array[:, :, 1] = numpy.copy(min_array)
                temp_array[:, :, 2] = numpy.copy(min_array)

            # Color centers red:
            for k in range(len(self.elements)):
                obj = self.elements[k]
                center_y, center_x = obj.center
                self.logger.debug(
                    "Element %d: center: (%d, %d)" % (k, center_y, center_x))
                for i in numpy.arange(-4, 5, 1):
                    for j in numpy.arange(-4, 5, 1):
                        temp_array[center_y + i, center_x + j, 0] = 255
                        temp_array[center_y + i, center_x + j, 1] = 0
                        temp_array[center_y + i, center_x + j, 2] = 0

            # Color COI blue:
            coi_y, coi_x = self.elements[0].coi
            for i in numpy.arange(-4, 5, 1):
                for j in numpy.arange(-4, 5, 1):
                    temp_array[coi_y + i, coi_x + j, 0] = 0
                    temp_array[coi_y + i, coi_x + j, 1] = 0
                    temp_array[coi_y + i, coi_x + j, 2] = 255

            # Color HFprime green:
            if hf_prime is not None:
                hf_star = int(coi_y - hf_prime + 0.5)
                for i in numpy.arange(-4, 5, 1):
                    for j in numpy.arange(-4, 5, 1):
                        temp_array[hf_star + i, coi_x + j, 0] = 0
                        temp_array[hf_star + i, coi_x + j, 1] = 255
                        temp_array[hf_star + i, coi_x + j, 2] = 0

            self.updateProc.emit(
                "Element centers in red; COI in blue; HF' in green")
            self.isReady.emit(temp_array)

    def refresh(self):
        """
        Name:     ImageWorker.refresh
        Inputs:   None
        Returns:  None
        Features: Resets view to original merged image
        Depends:  update_image_range

        @TODO: reset the elements array
        """
        if self.merge_img is not None:
            self.proc_img = numpy.copy(self.merge_img)
            self.is_cropped = False
            self.kernel = 0
            self.crop_left = 0
            self.crop_right = self.merge_img.shape[1]
            self.crop_top = 0
            self.crop_bottom = self.merge_img.shape[0]
            self.isFound.emit(0)
            self.isReady.emit(self.proc_img)
            self.update_image_range()
            self.updateProc.emit("Merged grayscale image")
        else:
            self.sendAlert.emit("Please first load merge image.")
            self.updateProc.emit("Image Processing Ready")

    def remove_element(self, idx):
        """
        Name:     ImageWorker.remove_element
        Inputs:   int, element list index (idx)
        Outputs:  None.
        Features: Catches Prida's removeObject signal to delete a given element
                  and drops the element from the current list
        Depends:  get_element_array
        """
        self.logger.debug("caught signal %d", idx)
        if idx >= 0:
            try:
                self.elements.pop(idx)
            except:
                self.logger.exception("Failed to remove element")
            else:
                num_found = len(self.elements)
                self.isFound.emit(num_found)
                self.get_element_array(-1)
        else:
            self.elements = []
            self.isFound.emit(0)

    def reset(self):
        """
        Name:     ImageWorker.reset
        Inputs:   None
        Returns:  None
        Features: Reset all variables
        """
        self.logger.info("resetting process values")
        self.datasets = []
        self.elements = []
        self.crop_left = 0
        self.crop_right = 0
        self.crop_top = 0
        self.crop_bottom = 0
        self.kernel = 0
        self.proc_img = None
        self.merge_img = None
        self.merge_count = 0
        self.is_binary = False
        self.is_cropped = False
        self.is_merged = False
        self.isFound.emit(0)
        self.setBGColor.emit("")
        self.setImgRange.emit("")
        self.setImgShape.emit("")

    def scale_image(self, nd_array, sfactor):
        """
        Name:     ImageWorker.scale_image
        Inputs:   - numpy.ndarray, unscaled image array (nd_array)
                  - int, scaling factor (sfactor)
        Returns:  numpy.ndarray, scaled image array
        Features: Returns a scaled numpy array
        """
        if sfactor != 100:
            try:
                my_array = scipy.misc.imresize(nd_array, sfactor, 'bilinear')
            except:
                self.logger.warning(
                    "first scaling attempt failed, trying again")
                try:
                    sfactor = float(sfactor) / 100.0
                    my_array = scipy.misc.imresize(
                        nd_array, sfactor, 'bilinear')
                except:
                    self.logger.exception(
                        "failed to scale image; returning original")
                    return nd_array
                else:
                    self.logger.info(
                        "scaled image %d%%", 100 * sfactor)
                    return my_array
            else:
                self.logger.info("scaled image %d%%", sfactor)
                return my_array
        else:
            self.logger.info("skipping scaling")
            return nd_array

    def set_merge_files(self, ds_list):
        """
        Name:     ImageWorker.set_merge_files
        Inputs:   list, list of session dataset paths (ds_list)
        Outputs:  None.
        Features: Receives datasets listed signal and signals for first process
                  image.
        """
        self.logger.info("caught datasets listed signal")
        self.datasets = ds_list
        self.merge_count = len(ds_list)
        self.logger.debug("merge count is %d", self.merge_count)

        self.logger.debug("starting to gather images from data")
        dset = ds_list[0]
        self.logger.info("signaling for process image 0")
        self.getProcImg.emit(dset, 0)

    def set_image(self, my_array, img_count):
        """
        Name:     ImageWorker.set_image
        Inputs:   - numpy.ndarray, image array (my_array)
                  - int, image number (img_count)
        Returns:  None.
        Features: Socket for procImg; performs array scaling, grayscaling and
                  image merging
        Depends:  - gray_luminance
                  - investigate
                  - scale_image
                  - update_image_range
        """
        self.logger.debug("caught signal for image %d", img_count)

        # Progress bar:
        if self.merge_count != 0:
            bval = self.merge_count / 20.0
            if int(img_count / bval) % 1 == 0:
                msg1 = "Merging ... "
                msg2 = "[{}{}] {}%".format(
                    '#' * int(img_count / bval),
                    '\u2013' * (20 - int(img_count / bval)),
                    5 * int(img_count / bval))
                msg = msg1 + msg2
                self.updateProc.emit(msg)

        if self.sfactor != 100:
            self.logger.debug("scaling image")
            my_array = self.scale_image(my_array, self.sfactor)

        self.logger.debug("converting image to grayscale")
        gray_img = self.gray_luminance(my_array)

        self.logger.debug("merging image")
        if img_count == 0 and img_count < self.merge_count - 1:
            self.logger.info("setting first merge image")
            self.merge_img = gray_img
            self.is_merged = False
            img_count += 1
            dset = self.datasets[img_count]
            self.logger.info("signaling for image %d", img_count)
            self.getProcImg.emit(dset, img_count)
        elif img_count == 0 and img_count == self.merge_count - 1:
            # There was only one image in the session set
            self.logger.info("setting image")
            self.merge_img = gray_img
            self.is_merged = True
            self.proc_img = numpy.copy(self.merge_img)
            self.setImgShape.emit(str(gray_img.shape))
            self.update_image_range()
            self.updateProc.emit("Grayscale image")
            h, w = self.proc_img.shape
            self.crop_left = 0
            self.crop_right = w
            self.crop_top = 0
            self.crop_bottom = h
            self.isMerged.emit(h, w)
            self.isReady.emit(self.proc_img)
        elif img_count == self.merge_count - 1:
            if self.bg_color == 0:
                # Black background, merge foreground (white)
                self.logger.info(
                    "merging last image with black background")
                self.merge_img = numpy.maximum(self.merge_img, gray_img)
                self.setBGColor.emit("Black")
            elif self.bg_color == 1:
                self.logger.info(
                    "merging last image with white background")
                self.merge_img = numpy.minimum(self.merge_img, gray_img)
                self.setBGColor.emit("White")
            self.is_merged = True
            self.proc_img = numpy.copy(self.merge_img)
            self.setImgShape.emit(str(gray_img.shape))
            self.update_image_range()
            self.updateProc.emit("Merged grayscale image")
            h, w = self.proc_img.shape
            self.crop_left = 0
            self.crop_right = w
            self.crop_top = 0
            self.crop_bottom = h
            self.isMerged.emit(h, w)
            self.isReady.emit(self.proc_img)
        elif img_count < self.merge_count - 1:
            if self.bg_color == 0:
                # Black background, merge foreground (white)
                self.logger.info(
                    "merging image %d with black background",
                    img_count)
                self.merge_img = numpy.maximum(self.merge_img, gray_img)
            elif self.bg_color == 1:
                self.logger.info(
                    "merging image %d with white background",
                    img_count)
                self.merge_img = numpy.minimum(self.merge_img, gray_img)
            self.is_merged = False
            img_count += 1
            dset = self.datasets[img_count]
            self.logger.info("signaling for image %d", img_count)
            self.getProcImg.emit(dset, img_count)

    def start(self):
        """
        Name:     ImageWorker.start
        Inputs:   None.
        Returns:  None.
        Features: Analysis thread run function; emits set image shape and
                  update process signals to GUI
        """
        self.logger.info("start")
        self.reset()
        self.updateProc.emit("Image Processing Ready")

    def stretch(self):
        """
        Name:     ImageWorker.stretch
        Inputs:   None
        Returns:  None
        Features: Stretches the grayscale luminosity of the process image
        Depends:  update_image_range
        """
        if self.proc_img is not None:
            img_dim = len(self.proc_img.shape)
            if img_dim == 2:
                h, w = self.proc_img.shape
                max_val = self.proc_img.max()
                min_val = self.proc_img.min()
                if max_val == 255 and min_val == 0:
                    self.logger.debug("image already stretched")
                else:
                    old_z = -1
                    for i in range(h):
                        z = 100.0 * (i - 1.0) / (h - 1.0)
                        if int(z) % 5 == 0 and int(z) != old_z:
                            msg1 = "Stretching ... "
                            msg2 = "[{}{}] {}%".format(
                                '#' * int(z / 5),
                                '\u2013' * (20 - int(z / 5)),
                                int(z))
                            msg = msg1 + msg2
                            self.logger.debug(msg)
                            self.updateProc.emit(msg)
                            old_z = int(z)
                        for j in range(w):
                            my_val = self.proc_img[i, j]
                            s_val = 255. * float(my_val - min_val)
                            s_val /= float(max_val - min_val)
                            s_val = int(s_val)
                            self.proc_img[i, j] = s_val
                self.updateProc.emit("Stretched merged image.")
                self.isReady.emit(self.proc_img)
                self.update_image_range()
            else:
                self.sendAlert.emit("Processed image is not grayscale!")
                self.updateProc.emit("Image Processing Ready")
        else:
            self.sendAlert.emit("Please load merged image first.")
            self.updateProc.emit("Image Processing Ready")

    def update_image_range(self):
        """
        Name:     ImageWorker.update_image_range
        Inputs:   None.
        Returns:  None.
        Features: Signals to GUI the process image's luminosity range.
        """
        if self.proc_img is not None:
            max_val = self.proc_img.max()
            min_val = self.proc_img.min()
            my_range = "(%d, %d)" % (min_val, max_val)
            self.setImgRange.emit(my_range)
        else:
            self.setImgRange.emit("")


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    # Instantiating logging handler and record format:
    root_handler = logging.FileHandler("workers.log")
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)
