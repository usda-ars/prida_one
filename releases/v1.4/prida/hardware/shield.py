#!/usr/bin/python
#
# Shield.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 6fc69ed: Nathanael Shaw - 2016-05-12 17:49:43
#     major hardware update
# c36d5b8: twdavis - 2016-05-04 15:09:36
#     typo fix and removal of unneccessary imports
# ea65dc8: Nathanael Shaw - 2016-04-19 13:05:09
#     reorganize order of ops in connect func
# e68489c: Nathanael Shaw - 2016-04-19 11:58:25
#     resolve #75. resolve #86. address #87.
# 67506da: Nathanael Shaw - 2016-04-18 11:06:23
#     change name and code cleanup
# f5ea81a: Nathanael Shaw - 2016-04-14 16:46:39
#     update docstrings, changelogs and conf defaults
# b22fb9e: Nathanael Shaw - 2016-04-14 12:34:40
#     address #75. address #86. configurable periphs
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import time
import logging

import serial
import serial.tools.list_ports

from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError
from .basecontroller import BaseController


###############################################################################
# CLASSES:
###############################################################################
class Shield(PRIDAPeripheral, BaseController):

    def __init__(self, my_parser=None, config_filepath=None):
        """
        Initialize data memebers. Inherit attributes and
        behaviours from the BaseController and PridaPeripheral absctract
        classes.
        Note: currently assumes that system is always using
        microstepping.
        """
        logging.debug('start initializing shield.')
        super(Shield, self).__init__()
        my_name = __name__[:-6] + 'controller'
        self.logger = logging.getLogger(my_name)
        # binary motor serial comuunication values
        self.CAL = bin(127)       # 01111111 <-- calibrate command
        self.BEACON = bin(63)     # 00111111 <-- reset shield
        self.ECHO = bin(32)       # 00100000 <-- echo command
        self.RELEASE = bin(0)     # 00000000 <-- release command

        self.STEP = bin(64)       # 01000000 <-- step flag
        self.FORWARD = bin(16)    # 00010000 <-- step - direction
        self.BACKWARD = bin(0)    # 00000000 <-- step - direction
        self.SINGLE = bin(1)      # 00000001 <-- step - regieme
        self.DOUBLE = bin(2)      # 00000010 <-- step - regieme
        self.INTER = bin(4)       # 00000100 <-- step - regieme
        self.MICRO = bin(8)       # 00001000 <-- step - regieme

        # private attributes for property definitions
        self._speed = 2.0         # Approximate rotational velocity
        self._sleep_val = 0.01    # Time to sleep between steps
        self._step_res = 200      # Number of units steps per rotation
        self.__step_types = {self.SINGLE: 'single',
                             self.DOUBLE: 'double',
                             self.INTER: 'inter',
                             self.MICRO: 'micro',
                             }
        self._step_type = self.__step_types[self.MICRO]  # Stepping type
        self._microsteps = 8      # Number of microsteps per unit step
        self._motor_port_num = 1  # Motor Shield Port Number In Use
        self._serial_port = None
        self._baud_rate = 9600
        self._my_serial = serial.Serial()

        # configure shield from config file
        self.attr_dict = {'SPEED': 'speed',
                          'DEGREES_PER_STEP': 'degrees',
                          'PORT_NUMBER': 'motor_port_num',
                          'STEP_TYPE': 'step_type'
                          }
        if my_parser is not None and config_filepath is not None:
            my_parser(self, my_name, config_filepath)
        self._stepstr = self.binadd(self.STEP,
                                    self.FORWARD,
                                    eval('self.{}'.format(
                                        self._step_type.upper())))
        if self.step_type is 'micro':
            substeps_per_step = self.microsteps
        elif self.step_type is 'inter':
            substeps_per_step = 2
        else:
            substeps_per_step = 1
        self._base_rot_time = 0.2 * substeps_per_step
        self.logger.debug('complete.')
        # set property values
        self.timeout = 5
        self.connect()

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def base_rot_time(self):
        """
        Name:    Shield.base_rot_time
        Feature: Returns the minimum amount of time at which the motor
                 can complete a full rotation with a microstepping
                 regieme
        Inputs:  None
        Outputs: Float, minimum number of seconds for a full rotation
                 (self._base_rot_time)
        """
        return self._base_rot_time

    @base_rot_time.setter
    def base_rot_time(self, val):  # lint:ok
        """
        Name:    Shield.base_rot_time
        Feature: Set stepper motor base rotation time (in seconds)
                 During full speed operation.
        Inputs:  - int || float, minmum rotation time in seconds (val)
        Outputs: None
        """
        if isinstance(val, int) or isinstance(val, float):
            if val > 0:
                self._base_rot_time = val
            else:
                self.logger.error('base rotation time' +
                                  ' must be non-negative.')
                raise ValueError('Base rotation time must be non-negative.')
        else:
            self.logger.error('base rotation time must be a number.')
            raise TypeError('Base rotation time must be a number.')

    @property
    def baud_rate(self):
        """
        Name:    Shield.baud_rate
        Feature: baud rate property getter
        Inputs:  None
        Outputs: int, baud rate (self._baud_rate)
        """
        return self._baud_rate

    @baud_rate.setter
    def baud_rate(self, val):  # lint:ok
        """
        Name:    Shield.baud_rate
        Feature: Baud rate property setter
        Inputs:  int, baud rate (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val > 0:
                self._baud_rate = val
            else:
                self.logger.error('baud rate must be a positive value')
                raise ValueError('baud rate must be a positive value')
        else:
            self.logger.error('baud rate must be an integer')
            raise TypeError('baud rate must be an integer')

    @property
    def degrees(self):
        """
        Name:    Shield.degrees
        Feature: Returns Step Resolution in terms of degrees per unit step.
        Inputs:  None
        Outputs: Float, degree representation of step resolution
                 (Shield.degrees)
        """
        return 360.0 / self.step_res

    @degrees.setter
    def degrees(self, value):  # lint:ok
        """
        Name:    Shield.degrees
        Feature: Sets Step Resolution in terms of degrees per unit step.
        Inputs:  Float, desired degree representation of step resolution
                 (Shield.degrees)
        Outputs: None
        """
        if isinstance(value, int) or isinstance(value, float):
            if 0 < value < 360:
                if (360.0 / value) % 1 == 0:
                    self.step_res = int(360 / value)
                else:
                    self.logger.error('degrees attribute' +
                                      ' must evenly divide 360.')
                    raise ValueError('Degrees attribute' +
                                     ' must evenly divide 360.')
            else:
                self.logger.error('degrees must be a value between 0 and 360.')
                raise ValueError('degrees must be a value between 0 and 360.')
        else:
            self.logger.error('degrees must be a integer or float.')
            raise TypeError('degrees must be a integer or float.')

    @property
    def microsteps(self):
        """
        Name:    Shield.microsteps
        Feature: Returns number of microsteps per unit step
        Inputs:  None
        Outputs: Int, number of microsteps per unit step (self.microsteps)
        """
        return self._microsteps

    @microsteps.setter
    def microsteps(self, value):  # lint:ok
        """
        Name:    Shield.microsteps
        Feature: Sets the number of microsteps per unit step
                 NOTE: currently only 8 us/step is supported by shield
        Inputs:  Int, number of microsteps per unit step (value)
        Outputs: None
        """
        if isinstance(value, int):
            if value in {8}:
                self._microsteps = value
            else:
                self.logger.error(
                    'number of microsteps per step must be a multiple of four.'
                    )
                raise ValueError(
                    'number of microsteps per step must be a multiple of four.'
                    )
        else:
            self.logger.error(
                'number of microsteps per step must be an integer.')
            raise TypeError(
                'number of microsteps per step must be an integer.')

    @property
    def motor_port_num(self):
        """
        Name:    Shield.motor_port_num
        Feature: Returns the port number which the shield is currently
                 controlling
        Inputs:  None
        Outputs: Integer, the number representing the in-use motor shield
                 port (self._motor_port_num)
        """
        return self._motor_port_num

    @motor_port_num.setter
    def motor_port_num(self, num):  # lint:ok
        """
        Name:    Shield.motor_port_num
        Feature: Set stepper motor port number.
        Inputs:  int, desired port number to be used on the shield (num)
        Outputs: None
        """
        if isinstance(num, int):
            if num in {1, 2}:
                self._motor_port_num = num
            else:
                self.logger.error('motor port number restricted to 1 or 2.')
                raise ValueError('motor port number restricted to 1 or 2.')
        else:
            self.logger.error('motor port number must be an integer.')
            raise TypeError('motor port number must be an integer.')

    @property
    def my_serial(self):
        """
        Name:    Shield.my_serial
        Feature: serial property getter
        Inputs:  None
        Outputs: Serial, a python serial object (self._my_serial)
        """
        return self._my_serial

    @my_serial.setter
    def my_serial(self, val):  # lint:ok
        """
        Name:    Shield.my_serial
        Feature: serial property setter
        Inputs:  Serial, a python serial object (val)
        Outputs: None
        """
        raise AttributeError('setting of the serial device atr is forbidden')

    @property
    def serial_port(self):
        """
        Name:    Shield.serial_port
        Feature: serial port property getter
        Inputs:  None
        Outputs: str, computer serial port location (self._serial_port)
        """
        return self._serial_port

    @serial_port.setter
    def serial_port(self, val):  # lint:ok
        """
        Name:    Shield.serial_port
        Feature: serial port property setter
        Inputs:  str, computer serial port location (val)
        Outputs: None
        """
        if isinstance(val, str):
            ports = serial.tools.list_ports.comports()
            p_list = [com.device for com in ports]
            if val in p_list:
                self._serial_port = val
            else:
                self.logger.error('%s is not a valid '
                                  'path to a serial device' % (val))
                raise ValueError('%s is not a valid '
                                 'path to a serial device' % (val))
        else:
            self.logger.error('serial port attribute must be a string')
            raise TypeError('serial port attribute must be a string')

    @property
    def sleep_val(self):
        """
        Name:    Shield.sleep_val
        Feature: Property getter for sleep time between steps/substeps
        Inputs:  None
        Outputs: float, time to sleep in seconds (self._sleep_val)
        """
        return self._sleep_val

    @sleep_val.setter
    def sleep_val(self, val):  # lint:ok
        """
        Name:    Shield.sleep_val
        Feature: Property setter for sleep time between steps/substeps
        Inputs:  float, time to sleep in seconds (val)
        Outputs: None
        """
        if isinstance(val, (int, float)):
            if val >= 0.01:
                self._sleep_val = val
            else:
                self.logger.error('value below allowable minimum')
                raise ValueError('value below allowable minimum')
        else:
            self.logger.error('sleep value must be a float or integer')
            raise TypeError('sleep value must be a float or integer')

    @property
    def speed(self):
        """
        Name:    Shield.speed
        Feature: Returns the approximate speed value at which the motor is
                 to spin
        Inputs:  None
        Outputs: Float, value approximating currently assigned motor
                 rotational speed (self._speed)
        """
        return self._speed

    @speed.setter
    def speed(self, val):  # lint:ok
        """
        Name:    Shield.speed
        Feature: Set approximate rotations per minute value for the
                 motor.
        Inputs:  float, desired speed value (val)
        Outputs: None
        """
        if isinstance(val, (float, int)):
            if val > 0:
                self._speed = val
                try:
                    # check step type for calculating sleep time
                    if self.step_type is self.__step_types[self.MICRO]:
                        substeps_per_rot = self.step_res * self.microsteps
                        substeps_per_min = (substeps_per_rot * self.speed)
                    elif self.step_type is self.__step_types[self.INTER]:
                        substeps_per_rot = self.step_res * 2
                        substeps_per_min = (substeps_per_rot * self.speed)
                    else:
                        substeps_per_min = (self.step_res * self.speed)
                    self.sleep_val = 60.0 / substeps_per_min
                    self.logger.debug('sleep val:{}'.format(self.sleep_val))
                except:
                    self.logger.warning('speed attr results in invalid sleep'
                                        ' time! setting to minimum.')
                    self.sleep_val = 0.01
            else:
                self.logger.error('speed requires a non-negative value.')
                raise ValueError('speed requires a non-negative value.')
        else:
            self.logger.error('speed must be a number.')
            raise TypeError('speed must be a number.')

    @property
    def step_res(self):
        """
        Name:    Shield.step_res
        Feature: Returns the number of unit steps within a single revolution
                 at which the motor is currently configured
        Inputs:  None
        Outputs: Integer, number of unit steps per full rotation
                 (self._step_res)
        """
        return self._step_res

    @step_res.setter
    def step_res(self, sr):  # lint:ok
        """
        Name:    Shield.step_res
        Feature: Set stepper motor step resolution.
        Inputs:  int, desired 'step resolution,' or the number of
                 steps in a full rotation, defining the length of a
                 unit step (sr)
        Outputs: None
        """
        if isinstance(sr, int):
            if sr > 0:
                self._step_res = sr
            else:
                self.logger.error('step resolution must be non-negative.')
                raise ValueError('Step resolution must be non-negative')
        else:
            self.logger.error('step resolution must be an integer.')
            raise TypeError('Step resolution must be an integer')

    @property
    def step_type(self):
        """
        Name:    Shield.step_type
        Feature: step type property getter
        Inputs:  None
        Outputs: str, step type (self._step_type)
        """
        return self._step_type

    @step_type.setter
    def step_type(self, val):  # lint:ok
        """
        Name:    Shield.step_type
        Feature: step type property setter
        Inputs:  str, desired step type (val)
        Outputs: None
        """
        if isinstance(val, str):
            if val in self.__step_types.values():
                self._step_type = val
            else:
                try:
                    decval = int(val, 2)
                except ValueError:
                    self.logger.error(
                        "'{}' not a valid stepping regieme".format(val))
                    raise ValueError(
                        "'{}' not a valid stepping regieme".format(val))
                else:
                    if bin(decval) in self.__step_types.keys():
                        self._step_type = self.__step_types[bin(decval)]
                    else:
                        self.logger.error(
                            "binary string '{}' does not ".format(val) +
                            "correspond to a stepping regieme")
                        raise ValueError(
                            "binary string '{}' does not ".format(val) +
                            "correspond to a stepping regieme")
        try:
            # re-check step type for calculating sleep time
            if self.step_type is self.__step_types[self.MICRO]:
                substeps_per_rot = self.step_res * self.microsteps
                substeps_per_min = (substeps_per_rot * self.speed)
            elif self.step_type is self.__step_types[self.INTER]:
                substeps_per_rot = self.step_res * 2
                substeps_per_min = (substeps_per_rot * self.speed)
            else:
                substeps_per_min = (self.step_res * self.speed)
            self.sleep_val = 60.0 / substeps_per_min
            self.logger.debug('sleep val:{}'.format(self.sleep_val))
        except:
            self.logger.warning('step type attr results in invalid sleep'
                                ' time! setting to minimum.')
            self.sleep_val = 0.01

    @property
    def stepstr(self):
        """
        Name:    Shield.stepstr
        Feature: default step command property getter
        Inputs:  None
        Outputs: str, string rep of binary step command (self._stepstr)
        """
        return self._stepstr

    @stepstr.setter
    def stepstr(self, val):  # lint:ok
        """
        Name:    Shield.stepstr
        Feature: default step command property setter
        Inputs:  str, string representation of binary step command (val)
        Outputs: None
        """
        if isinstance(val, str):
            try:
                int(val, 2)
            except:
                self.logger.exception('stepstr value must be a binary string')
                raise
            else:
                self._stepstr = val

    @property
    def timeout(self):
        """
        Name:    Shield.timeout
        Feature: serial communication timeout property getter
        Inputs:  None
        Outputs: float, seconds to timeout (self.my_serial.timeout)
        """
        return self.my_serial.timeout

    @timeout.setter
    def timeout(self, val):  # lint:ok
        """
        Name:    Shield.timeout
        Feature: serial communication timeout property setter
        Inputs:  float, seconds to timeout (val)
        Outputs: None
        """
        if isinstance(val, int) or isinstance(val, float):
            if val >= 0:
                self.my_serial.timeout = val
            else:
                self.logger.debug('timeout atr must be a non-negative value')
                raise ValueError('timeout atr must be a non-negative value')
        else:
            self.logger.debug('timeout atr must be a number of type int'
                              ' or float')
            raise TypeError('timeout atr must be a number of type int'
                            ' or float')

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def bin_to_bytes(self, binstr):
        """
        Name:    Shield.bin_to_bytes
        Feature: converts a binary string to the equivilent ASCII byte string
        Inputs:  str, binary string to be converted
        Outputs: str, byte string representation (bytestr)
        """
        bytestr = b''
        try:
            bytestr = chr(int(binstr, 2)).encode('utf-8')
        except:
            self.logger.exception('failed binary to bytestring conversion')
        finally:
            return bytestr

    def binadd(self, *args, **kwargs):
        """
        Name:    Shield.binadd
        Feature: adds together an arbitary number of python binary strings
        Inputs:  - any number of binary string arguments (*args)
                 - any number of additional keyword arguments (**kwargs)
        Outputs: str, resultant binary string sum (binsum)
        """
        binsum = ''
        try:
            binsum = bin(sum([int(arg, 2) for arg in args]))
        except:
            self.logger.exception('failed binary addition')
        finally:
            return binsum

    def bytes_to_bin(self, bytestr):
        """
        Name:    Shield.bytes_to_bin
        Feature: converts a byte string ASCII character to its eqivalent
                 binary string value
        Inputs:  str, byte string to convert (bytestr)
        Outputs: str, resulting binary string (binstr)
        """
        binstr = ''
        try:
            binstr = bin(ord(bytestr.decode('utf-8')))
        except:
            self.logger.exception('failed bytestring to binary conversion')
        finally:
            return binstr

    def calibrate(self):
        """
        Name:    Shield.calibrate
        Feature: Take three initial unit steps to engage coils.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('calibrating motor...')
        if self.my_serial.isOpen():
            self.wakeup()
            self.my_serial.write(self.bin_to_bytes(self.CAL))
            time.sleep(0.05)
            tmp = self.timeout
            self.logger.debug('current timeout:{}'.format(tmp))
            self.my_serial.timeout = None
            mybyte = self.my_serial.read(size=1)
            self.logger.debug('assigning timeout:{}'.format(tmp))
            self.timeout = tmp
            self.logger.debug('new timeout:{}'.format(self.timeout))
            if mybyte != self.bin_to_bytes(self.CAL):
                self.logger.warning(
                    "recieved unexpected byte '{}'".format(mybyte))

    def connect(self):
        """
        Name:    Shield.connect
        Feature: attempt to connect the computer to the shield
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('establishing connection...')
        status = 0
        if not self.my_serial.isOpen():
            for port in serial.tools.list_ports.comports():
                self.my_serial.baudrate = self.baud_rate
                self.my_serial.port = port.device
                self.my_serial.open()
                status = self.listen()
                if status == 1:
                    self.serial_port = port.device
                    break
                self.my_serial.close()
        else:
            self.logger.warning('serial port open, may already be connected')
            status = self.current_status()
        if status == 1:
            self.logger.debug(
                'connection established at port {}'.format(self.serial_port))
        else:
            self.logger.error('could not establish connection')
            raise PRIDAPeripheralError(msg='could not establish connection')

    def current_status(self):
        """
        Name:    Shield.current_status
        Feature: Attempt a simple handshake with the shield
        Inputs:  None
        Outputs: int, status indicator (status)
        """
        status = 0
        if self.my_serial.isOpen():
            # flush serial buffers
            self.logger.debug('flushing buffers...')
            self.my_serial.reset_input_buffer()
            self.my_serial.reset_output_buffer()
            # write echo command ascii character to serial port
            self.logger.debug('attempting handshake...')
            self.my_serial.write(self.bin_to_bytes(self.ECHO))
            time.sleep(0.05)
            my_bytes = self.my_serial.read(size=1)
            if my_bytes == b'':
                err_str = 'could not read value from motor! timed out.'
                self.logger.warning(err_str)
            elif self.bytes_to_bin(my_bytes) == self.ECHO:
                status = 1
            else:
                err_str = ('bad return on echo call,'
                           ' received %s' % (str(my_bytes)))
                self.logger.warning(err_str)
        return status

    def listen(self, force=True):
        """
        Name:    shield.listen
        Feature: connect subfunc for listening for arduino on the current port
        Inputs:  None
        Outputs: None
        """
        status = 0
        if self.my_serial.isOpen():
            try:
                mybyte = self.my_serial.read(size=1)
                if mybyte == b'':
                    raise PRIDAPeripheralError(msg='could not read value from'
                                               ' motor! timed out.')
            except PRIDAPeripheralError as PPE:
                self.logger.warning(PPE.args[0])
            except:
                self.logger.exception('exception caught during serial read')
            else:
                if mybyte == self.bin_to_bytes(self.RELEASE):
                    self.logger.debug('found arduino, sending signal')
                    self.my_serial.write(self.bin_to_bytes(self.ECHO))
                    time.sleep(0.05)
                    self.logger.debug('checking status')
                    status = self.current_status()
                    if status == 1:
                        self.logger.debug('connection established.')
                else:
                    raise PRIDAPeripheralError(msg='unexpected character'
                                               ' recieved! please check'
                                               ' your shield.')
        return status

    def poweroff(self):
        """
        Name:    Shield.poweroff
        Feature: sends the serial command to power down the coils
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('powering down the controller...')
        if self.my_serial.isOpen():
            self.wakeup()
            self.my_serial.write(self.bin_to_bytes(self.RELEASE))
            time.sleep(0.05)
            self.my_serial.reset_input_buffer()
            self.my_serial.reset_output_buffer()
            self.my_serial.write(self.bin_to_bytes(self.BEACON))
            time.sleep(0.05)
            self.my_serial.close()

    def single_step(self):
        """
        Name:    motor.single_step
        Feature: Takes a single microstep
        Inputs:  None
        Outputs: None
        """
        if self.my_serial.isOpen():
            # write step command to arduino
            self.my_serial.write(self.bin_to_bytes(self.stepstr))
            time.sleep(self.sleep_val)

            # check arduino echo
            mybyte = self.my_serial.read(size=1)
            if mybyte != self.bin_to_bytes(self.stepstr):
                self.logger.warning(
                    "recieved unexpected byte '{}'".format(mybyte))

    def turn_off_motors(self):
        """
        Name:    Shield.turn_off_motors
        Feature: Disable all motors attached to the serial port.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('powering down the motor...')
        if self.my_serial.isOpen():
            self.wakeup()
            self.my_serial.write(self.bin_to_bytes(self.RELEASE))
            time.sleep(0.05)
            mybyte = self.my_serial.read(size=1)
            if mybyte != self.bin_to_bytes(self.RELEASE):
                self.logger.warning(
                    "recieved unexpected byte '{}'".format(mybyte))

    def wakeup(self):
        """
        Name:    Shield.wakeup
        Feature: write/reads an echo command to the shield to insure a return
                 from idle-mode.
        Inputs:  None
        Outputs: None
        """
        if self.my_serial.isOpen():
            self.my_serial.reset_input_buffer()
            self.my_serial.reset_input_buffer()
            self.my_serial.write(self.bin_to_bytes(self.ECHO))
            time.sleep(0.05)
            mybyte = self.my_serial.read(size=1)
            if mybyte != self.bin_to_bytes(self.ECHO):
                self.logger.warning(
                    "recieved unexpected byte '{}'".format(mybyte))
