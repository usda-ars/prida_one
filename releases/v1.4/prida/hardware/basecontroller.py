#!/usr/bin/python
#
# basecontroller.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 6fc69ed: Nathanael Shaw - 2016-05-12 17:49:43
#     major hardware update
# 2d36763: Nathanael Shaw - 2016-04-12 17:15:21
#     mod controller class for serial com. with arduino
# d3acf8d: Nathanael - 2016-04-07 10:54:18
#     add controller and basecontroller, update imaging
# $log_end_tag$
#
###############################################################################


###############################################################################
# CLASSES:
###############################################################################
class BaseController():

    def __init__(self):
        """
        Initialize data memebers.
        Note: currently assumes that system is always using
        microstepping.
        """
        # All attributes require re-implementing as public properties
        self._speed = None
        self._step_res = None
        self._step_type = None
        self._microsteps = None

    def turn_off_motors(self):
        """
        Name:    BaseController.turn_off_motors
        Feature: Disable all motors attached to the controller.
        Inputs:  None
        Outputs: None
        """
        raise NotImplementedError('turn off motors not implemented')

    def calibrate(self):
        """
        Name:    BaseController.calibrate
        Feature: Take three initial unit steps.
        Inputs:  None
        Outputs: None
        """
        raise NotImplementedError('calibrate not implemented')

    def single_step(self):
        """
        Name:    BaseController.single_step
        Feature: Takes a single microstep
        Inputs:  None
        Outputs: None
        """
        raise NotImplementedError('single step not implemented')
