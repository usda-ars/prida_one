#!/usr/bin/python
#
# r325pe_serial.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
#
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import time

import serial
import serial.tools.list_ports

from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError
from .basecontroller import BaseController


###############################################################################
# CLASS DEFINITION:
###############################################################################
class R325PE(BaseController, PRIDAPeripheral):
    """
    Name:    R325PE
    Feature: Class representing the and controlling the serial communication to
             the R325PE stepper motor driver
    """

    def __init__(self, gr=1):
        """
        Name:    r325pe_serial.__init__
        Feature: initializes an instance of the R325PE class
        Inputs:  float, gear ratio (gr)
        Outputs: None
        """
        super(R325PE, self).__init__()

        # Setup logging
        my_name = __name__[:-13] + 'controller'
        self.logger = logging.getLogger(my_name)

        # Define commands
        self.CAL = None
        self.ECHO = None
        self.LOAD = None
        self.RELEASE = None
        self.STEP = None
        self.MICRO = None
        self.RES = None

        # Initialize private variables
        self.__addrs = [chr(i) for i in range(65, 91)]

        # Initialize property values
        self._my_addr = None
        self._my_serial = serial.Serial()
        self._baud_rate = 9600
        self._serial_port = None  # '/dev/ttyUSB0'
        self._rpm = 2.0
        self._step_res = None
        self._microsteps = None

        # Configure device
        self.timeout = 5
        self.gear_ratio = gr
        self.connect()

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def baud_rate(self):
        """
        Name:    r325pe_serial.baud_rate
        Feature: serial object baud rate getter
        Inputs:  None
        Outputs: int, baud rate (self.my_serial.baudrate)
        """
        if self.my_serial.is_open:
            return self.my_serial.baudrate
        else:
            self.logger.error('serial port is not opened, please connect')
            raise PRIDAPeripheralError('serial port is not opened,'
                                       ' please connect')

    @baud_rate.setter
    def baud_rate(self, val):
        """
        Name:    r325pe_serial.baud_rate
        Feature: serial object baud rate setter
        Inputs:  int, baud rate (val)
        Outputs: None
        """
        if self.my_serial.is_open:
            if isinstance(val, int):
                if val in [50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800,
                           2400, 4800, 9600, 19200, 38400, 57600, 115200]:
                    self.my_serial.baud = val
                else:
                    self.logger.error(
                        "'{}' not a valid baud rate value".format(val))
                    raise ValueError(
                        "'{}' not a valid baud rate value".format(val))
            else:
                self.logger.error('baud rate attribute must be an integer')
                raise TypeError('baud rate attribute must be an integer')
        else:
            self.logger.error('serial port is not opened, please connect')
            raise PRIDAPeripheralError('serial port is not opened,'
                                       ' please connect')

    @property
    def microsteps(self):
        """
        Name:    r325pe_serial.microsteps
        Feature: microstep resolution getter
        Inputs:  None
        Outputs: int, driver microstep resolution (self._microsteps)
        """
        if self.my_serial.is_open and self.my_addr is not None:
            if self._microsteps is None:
                self.my_serial.write(self.to_command(self.MICRO))
                time.sleep(0.2)
                self._microsteps = self.my_serial.read()
            return self._microsteps

    @microsteps.setter
    def microsteps(self, val):
        """
        Name:    r325pe_serial.microsteps
        Feature: microstep resolution setter
        Inputs:  int, driver microstep resolution (val)
        Outputs: None
        """
        if self.my_serial.is_open and self.my_addr is not None:
            if isinstance(val, int) or isinstance(val, float):
                if val in [2**n for n in range(1, 9)]:
                    self._microsteps = val
                    self.my_serial.write(self.to_command(self.MICRO, val))
                    time.sleep(0.2)
                else:
                    self.logger.error('microsteps must be a power of two '
                                      'between 2 and 256')
                    raise ValueError('microsteps must be a power of two '
                                     'between 2 and 256')
            else:
                self.logger.error('microsteps must be a number')
                raise TypeError('microsteps must be a number')
        else:
            self.logger.error('serial port is not opened, please connect')
            raise PRIDAPeripheralError('serial port is not opened,'
                                       ' please connect')

    @property
    def my_addr(self):
        """
        Name:    r325pe_serial.my_addr
        Feature: ASCII driver address getter
        Inputs:  None
        Outputs: str, driver address (self._my_addr)
        """
        if self._my_addr is None:
            for addr in self.__addrs:
                self._my_addr = addr
                self.load_commands()
                self.my_serial.write(self.to_command(self.ECHO))
                time.sleep(1)
                resp = self.my_serial.read()
                if resp is '*{0}MA{0}'.format(self._my_addr):
                    break
        if self._my_addr is not None:
            return self._my_addr
        else:
            self.logger.error('could not find a valid device address')
            raise AttributeError('could not find a valid device address')

    @my_addr.setter
    def my_addr(self, val):
        """
        Name:    r325pe_serial.my_addr
        Feature: ASCII driver address setter
        Inputs:  str, driver address (val)
        Outputs: None
        """
        if isinstance(str, val):
            if val in self.__addrs:
                self._my_addr = val
            else:
                self.logger.error('{} is not a valid address'.format(val))
                raise ValueError('{} is not a valid address'.format(val))
        else:
            self.logger.error('address attribute must be a string')
            raise TypeError('address attribute must be a string')

    @property
    def my_serial(self):
        """
        Name:    r325pe_serial.my_serial
        Feature: pyserial Serial object getter
        Inputs:  None
        Outputs: serial, pyserial Serial object (self._my_serial)
        """
        return self._my_serial

    @my_serial.setter
    def my_serial(self, val):
        """
        Name:    r325pe_serial.my_serial
        Feature: pyserial Serial object setter
        Inputs:  serial, pyserial Serial object (val)
        Outputs: None
        """
        if isinstance(val, serial.Serial):
            self._my_serial = val
        else:
            self.logger.error('serial attribute must be a Serial object')
            raise TypeError('serial attribute must be a Serial object')

    @property
    def rpm(self):
        """
        Name:    r325pe_serial.rpm
        Feature: driver rpm setting getter
        Inputs:  None
        Outputs: float, driver rpm setting (self._rpm)
        """
        return self._rpm

    @rpm.setter
    def rpm(self, val):
        """
        Name:    r325pe_serial.rpm
        Feature: driver rpm setting setter
        Inputs:  float, driver rpm setting (val)
        Outputs: None
        """
        if isinstance(val, float) or isinstance(val, int):
            if val > 0:
                self._rpm = val
            else:
                self.logger.error('rpm attribute must be a positive value')
                raise ValueError('rpm attribute must be a positive value')
        else:
            self.logger.error('rpm attribute must be a integer or float')
            raise TypeError('rpm attribute must be a integer or float')

    @property
    def serial_port(self):
        """
        Name:    r325pe_serial.serial_port
        Feature: driver serial communication port getter
        Inputs:  None
        Outputs: str, serial port (self._serial_port)
        """
        return self._serial_port

    @serial_port.setter
    def serial_port(self, val):
        """
        Name:    r325pe_serial.serial_port
        Feature: driver serial communication port setter
        Inputs:  str, serial port (val)
        Outputs: None
        """
        if isinstance(val, str):
            ports = serial.tools.list_ports.comports()
            p_list = [com.device for com in ports]
            if val in p_list:
                self._serial_port = val
            else:
                self.logger.error('%s is not a valid '
                                  'path to a serial device' % (val))
                raise ValueError('%s is not a valid '
                                 'path to a serial device' % (val))
        else:
            self.logger.error('serial port attribute must be a string')
            raise TypeError('serial port attribute must be a string')

    @property
    def step_res(self):
        """
        Name:    r325pe_serial.step_res
        Feature: driver step resolution getter
        Inputs:  None
        Outputs: int, driver step resolution (self._step_res)
        """
        return self._step_res

    @step_res.setter
    def step_res(self, val):
        """
        Name:    r325pe_serial.step_res
        Feature: driver step resolution setter
        Inputs:  int, driver step resolution (val)
        Outputs: None
        """
        self._step_res = val
        self.my_serial.write(self.to_command(self.RES, val))
        time.sleep(0.2)

    @property
    def timeout(self):
        """
        Name:    r325pe_serial.timeout
        Feature: serial object timeout attribute getter
        Inputs:  None
        Outputs: float, timeout value (self.my_serial.timeout)
        """
        return self.my_serial.timeout

    @timeout.setter
    def timeout(self, val):
        """
        Name:    r325pe_serial.timeout
        Feature: serial object timeout attribute setter
        Inputs:  float, timeout value (val)
        Outputs: None
        """
        self.my_serial.timeout = val

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def calibrate(self):
        """
        Name:    r325pe_serial.calibrate
        Feature: Writes a command to the driver to take a few initial steps
                 with the motor to engage the coils
        Inputs:  None
        Outputs: None
        """
        self.my_serial.write(self.to_command(self.CAL))

    def connect(self):
        """
        Name:    r325pe_serial.connect
        Feature: initialize serial communications with the driver
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('establishing connection...')
        status = 0
        if not self.my_serial.isOpen:
            for port in serial.tools.list_ports.comports():
                self.my_serial.baudrate = self.baud_rate
                self.my_serial.port = self.serial_port = port.device
                self.my_serial.open()
                status = self.current_status()
                if status == 1:
                    break
                self.my_serial.close()
        else:
            self.logger.warning('serial port open, may already be connected')
            status = self.current_status()
        if status == 1:
            self.logger.debug(
                'connection established at port {}'.format(self.serial_port))
            self.my_serial.write(self.to_command(self.LOAD))
            time.sleep(1)
        else:
            self.logger.error('could not establish connection')
            raise PRIDAPeripheralError('could not establish connection')

    def current_status(self):
        """
        Name:    r325pe_serial.current_status
        Feature: attempt communications with the driver
        Inputs:  None
        Outputs: None
        """
        status = 0
        if self.my_serial.isOpen:
            if self.my_addr is None:
                for addr in self.__addrs:
                    self.my_addr = addr
                    self.load_commands()
                    self.my_serial.write(self.to_command(self.ECHO))
                    time.sleep(1)
                    resp = self.my_serial.read()
                    if resp is '*{0}MA{0}'.format(self.my_addr):
                        break
            if self.my_addr is not None:
                self.my_serial.write(self.to_command(self.ECHO))
                time.sleep(1)
                resp = self.my_serial.read()
                if resp is '*{0}MA{0}'.format(self.my_addr):
                    status = 1
        return status

    def poweroff(self):
        """
        Name:    r325pe_serial.poweroff
        Feature: set the motor hold current to zero and disable serial
                 communications with the motor
        Inputs:  None
        Outputs: None
        """
        if self.my_serial.isOpen:
            self.turn_off_motors()
            self.my_addr = None
            self.serial_port = None
            self.CAL = None
            self.ECHO = None
            self.LOAD = None
            self.RELEASE = None
            self.STEP = None
            self.RES = None
            self.my_serial.close()

    def load_commands(self):
        """
        Name:    r325pe_serial.load_commands
        Feature: initialize the serial commands with the device address
        Inputs:  None
        Outputs: None
        """
        if self.my_addr is not None and self.my_serial.isOpen:
            self.CAL = '#{}PM3'.format(self.my_addr)
            self.ECHO = '#{}MA'.format(self.my_addr)
            self.LOAD = '#{}LD'.format(self.my_addr)
            self.RELEASE = '#{}HI0'.format(self.my_addr)
            self.STEP = '#{}SF'.format(self.my_addr)
            self.RES = '#{}SR'.format(self.my_addr)

    def single_step(self):
        """
        Name:    r325pe_serial.single_step
        Feature: write a forward step command to the device driver
        Inputs:  None
        Outputs: None
        """
        if self.my_serial.isOpen:
            self.my_serial.write(self.to_command(self.STEP))
            time.sleep(60 / (self.step_res * self.rpm * self.gear_ratio))

    def to_command(self, bytestr, value=''):
        """
        Name:    r325pe_serial.to_command
        Feature: formats a command string to the apropriate bytestring
        Inputs:  - str, command to be issued (bytestr)
                 - str, value to be assigned to the command (value)
        Outputs: str, the formatted command bytestring (cmdstr)
        """
        cmdstr = '{}{}{}{}'.format(str(bytestr), str(value), chr(13), chr(10))
        cmdstr = cmdstr.encode('utf-8')
        return cmdstr

    def turn_off_motors(self):
        """
        Name:    r325pe_serial.turn_off_motors
        Feature: set the motor hold current to zero
        Inputs:  None
        Outputs: None
        """
        if self.my_serial.isOpen:
            self.my_serial.write(self.to_command(self.RELEASE))
            time.sleep(1)
