#!/usr/bin/python
#
# imaging.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 6fc69ed: Nathanael Shaw - 2016-05-12 17:49:43
#     major hardware update
# bbf63cd: Nathanael Shaw - 2016-05-05 10:01:26
#     add piezo & led checks in prop. getrs and setrs
# c4b32a3: twdavis - 2016-05-04 15:35:02
#     added returns in exception blocks
# e27ddf0: twdavis - 2016-05-04 14:10:35
#     explorer mode option enabled
# 9ee7af4: twdavis - 2016-05-04 13:53:45
#     hotfix imaging camera getters
# 094aabd: Nathanael Shaw - 2016-04-19 12:42:55
#     add pause for motor calibrate
# e68489c: Nathanael Shaw - 2016-04-19 11:58:25
#     resolve #75. resolve #86. address #87.
# 2769e57: Nathanael Shaw - 2016-04-18 17:08:02
#     add mode checking and camera wrapper funcs
# 52fdcde: Nathanael Shaw - 2016-04-18 11:06:59
#     add mode attr and code cleanup
# f5ea81a: Nathanael Shaw - 2016-04-14 16:46:39
#     update docstrings, changelogs and conf defaults
# 71e71de: Nathanael Shaw - 2016-04-14 16:05:50
#     Merge branch 'master' into iss75
# 6b241e0: Nathanael Shaw - 2016-04-14 15:23:39
#     add one sec of sleep to image capture
# b22fb9e: Nathanael Shaw - 2016-04-14 12:34:40
#     address #75. address #86. configurable periphs
# a757b19: Nathanael Shaw - 2016-04-13 13:03:19
#     fix typos, rmv more rpi dependacies
# 1fba20f: Nathanael Shaw - 2016-04-13 11:30:48
#     update controller docstr, rmv img. rpi dep.
# 2d36763: Nathanael Shaw - 2016-04-12 17:15:21
#     mod controller class for serial com. with arduino
# d3acf8d: Nathanael - 2016-04-07 10:54:18
#     add controller and basecontroller, update imaging
# 4808738: Nathanael Shaw - 2016-03-10 13:17:51
#     revert controller to hat
# 1935148: Nathanael Shaw - 2016-03-09 09:34:48
#     update changelogs
# cee72e8: Nathanael Shaw - 2016-02-25 09:51:35
#     resolve #72
# 69344e0: Nathanael Shaw - 2016-02-18 16:26:50
#     hotfix basestring
# 7107cd8: Nathanael Shaw - 2016-02-03 12:31:27
#     imaging beep hotfix
# 980ca12: Nathanael Shaw - 2016-02-03 09:59:01
#     hotfix
# 0a00abf: Nathanael Shaw - 2016-02-02 12:07:53
#     module hierarchy draft
# 49461a4: Nathanael Shaw - 2016-01-29 15:21:33
#     hidden custom import stymied module creation
# 2d665f8: Nathanael Shaw - 2016-01-29 10:52:31
#     setup module hierarchy
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import datetime
import logging
import os
import sys
import time

from .camera import Camera
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# FUNCTIONS:
###############################################################################
def exit_sequence(my_imaging):
    """
    Name:    Imaging.exit_sequence
    Feature: Disables all hardware before application close
    Inputs:  Imaging, the instance of Imaging to cleanup (my_imaging)
    Outputs: None
    """
    logging.info('start.')
    if my_imaging.driver in my_imaging.supported_drivers:
        my_imaging.turn_off_controller()
    if my_imaging.has_led:
        my_imaging.led_off()
    logging.info('complete.')


###############################################################################
# CLASSES:
###############################################################################
class Imaging(object):
    """
    Aggregate imaging hardware class for PRIDA.
    """
    _controller = None  # Attribute name for object controlling the controller
    _camera = None      # Attribute name for object controlling the camera
    _led = None         # Attribute name for object controlling the led
    _piezo = None       # Attribute name for object controlling the piezo

    def __init__(self, my_parser=None, config_filepath=None):
        """
        Initializes hardware states.
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('Instantiating imaging.')
        self._num_photos = 1         # total to collect
        self._taken = 0              # number of current photo
        self._position = 0           # current position of the motor in degrees
        self._is_running = False     # indicates if system is mid DAQ
        self._is_error = False       # indicates system error
        self._step_count = 0         # current microstep position
        self._gear_ratio = 1         # 4.9 for use with RM101 setup
        self._cur_pid = None         # current PID being imaged
        self._end_pos = 0            # end positiom of the motor in usteps
        self._driver = ''            # motor driver being implemented
        self._settling_time = 2.     # settling time in seconds
        self._supported_drivers = {'HAT', 'Shield', 'Controller'}
        self._has_led = False        # led peripheral attached
        self._has_piezo = False      # piezo peripheral attached
        self._mode = 'Explorer'
        self.attr_dict = {'GEAR_RATIO': 'gear_ratio',
                          'DRIVER': 'driver',
                          'HAS_LED': 'has_led',
                          'HAS_PIEZO': 'has_piezo',
                          'MODE': 'mode',
                          'SETTLING_TIME': 'settling_time',
                          }
        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        self.load_peripherals(my_parser, config_filepath)
        self.logger.debug('Imaging instantiated.')
        atexit.register(exit_sequence, self)

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def camera(self):
        """
        Name:    Imaging.camera
        Feature: Return camera data member
        Inputs:  None
        Outputs: Camera, a gphoto2 camera object abstraction
                 (self._camera)
        """
        return self._camera

    @camera.setter
    def camera(self, value):  # lint:ok
        """
        Name:    Imaging.camera
        Feature: _camera attribute setter
        Inputs:  Camera, an gphoto2 camera object abstraction (value)
        Outputs: None
        """
        if self.mode is '2D' or self.mode is '3D':
            if isinstance(value, Camera()):
                self._camera = value
            else:
                self.logger.error('_camera attribute must be a Camera' +
                                  ' object.')
                raise TypeError('_camera attribute must be a Camera object.')
        else:
            self.logger.warning('not in 2D or 3D modes! '
                                'Cannot set camera attribute.')
            self._camera = None

    @property
    def controller(self):
        """
        Name:    Imaging.controller
        Feature: Return _controller data member (getter)
        Inputs:  None
        Outputs: controller, an Adafruit_MotorHAT abstraction (i.e.,
                 self._controller)
        """
        return self._controller

    @controller.setter
    def controller(self, value):  # lint:ok
        """
        Name:    Imaging.controller
        Feature: _controller attribute setter
        Inputs:  cotroller, an Adafruit_MotorHAT abstraction (value)
        Outputs: None
        """
        if self.mode is '3D':
            if isinstance(value, eval('{}()'.format(self.driver))):
                self._controller = value
            else:
                self.logger.error(
                    '_controller attribute must be a '
                    '{} object.'.format(self.driver))
                raise TypeError(
                    '_controller attribute must be a '
                    '{} object.'.format(self.driver))
        else:
            self.logger.warning('Not in 3d mode! '
                                'Cannot set controller attribute.')

    @property
    def cur_pid(self):
        """
        Name:    Imaging.cur_pid
        Feature: Returns the current pid
        Inputs:  None
        Outputs: Str, the current PID (self._cur_pid)
        """
        return self._cur_pid

    @cur_pid.setter
    def cur_pid(self, value):  # lint:ok
        """
        Name:    Imaging.cur_pid
        Feature: Sets the current PID
        Inputs:  Str, the new PID (value)
        Outputs: None
        """
        if isinstance(value, str):
            self._cur_pid = value
        else:
            self.logger.error('current pid must be a string.')
            raise TypeError('current pid must be a string.')

    @property
    def driver(self):
        """
        Name:    Imaging.driver
        Feature: Motor driver API name property getter
        Inputs:  None
        Outputs: str, motor driver API name (self._driver)
        """
        return self._driver

    @driver.setter
    def driver(self, val):  # lint:ok
        """
        Name:    Imaging.driver
        Feature: Motor driver API name property setter
        Inputs:  str, motor driver API name (val)
        Outputs: None
        """
        if isinstance(val, str):
            if val in self.supported_drivers:
                self._driver = val
            elif val is '':
                self.logger.debug('No driver set, disabling controller.')
                self._driver = val
            else:
                self.logger.error(
                    "driver '{}' not supported by prida.".format(val))
                raise ValueError(
                    "driver '{}' not supported by prida.".format(val))
        else:
            self.logger.error('driver attribute must be of type string')
            raise TypeError('driver attribute must be of type string')

    @property
    def end_pos(self):
        """
        Name:    Imaging.end_pos
        Feature: Returns the end position of the motor for the current sequence
                 in number of steps
        Inputs:  None
        Outputs: Int, end position in steps (self._end_pos)
        """
        return self._end_pos

    @end_pos.setter
    def end_pos(self, value):  # lint:ok
        """
        Name:    Imaging.end_pos
        Feature: Forbids setting the end position of the motor for the current
                 sequence in number of steps
        Inputs:  dtype, user value specified for assignment (value)
        Outputs: None
        """
        self.logger.error(
            'setting the step end position manualy is forbidden.')
        raise AttributeError(
            'setting the step end position manualy is forbidden.')

    @property
    def gear_ratio(self):
        """
        Name:    Imaging.gear_ratio
        Feature: Returns the setup's gear ratio
        Inputs:  None
        Outputs: Float, the gear ratio (self._gear_ratio)
        """
        return self._gear_ratio

    @gear_ratio.setter
    def gear_ratio(self, value):  # lint:ok
        """
        Name:    Imaging.gear_ratio
        Feature: Sets the system's gear ratio
        Inputs:  Float, the new gear ratio (value)
        Outputs: None
        """
        if isinstance(value, float) or isinstance(value, int):
            if value >= 0:
                self._gear_ratio = value
            else:
                self.logger.error('gear ratio must be a positive value.')
                raise ValueError('gear ratio must be a positive value.')
        else:
            self.logger.error('gear ratio must be a numeric value.')
            raise TypeError('gear ratio must be a numeric value.')

    @property
    def has_led(self):
        """
        Name:    Imaging.has_led
        Feature: has_led boolean property getter
        Inputs:  None
        Outputs: bool, has_led attribute (self._has_led)
        """
        return self._has_led

    @has_led.setter
    def has_led(self, val):  # lint:ok
        """
        Name:    Imaging.has_led
        Feature: has_led boolean property setter
        Inputs:  bool, has_led attribute (val)
        Outputs: None
        """
        if isinstance(val, bool):
            self._has_led = val
        else:
            self.logger.error("'has led' attribute must be of type bool")
            raise TypeError("'has led' attribute must be of type bool")

    @property
    def has_piezo(self):
        """
        Name:    Imaging.has_piezo
        Feature: has_piezo boolean property getter
        Inputs:  None
        Outputs: bool, has_piezo attribute (self._has_piezo)
        """
        return self._has_piezo

    @has_piezo.setter
    def has_piezo(self, val):  # lint:ok
        """
        Name:    Imaging.has_piezo
        Feature: has_piezo boolean property setter
        Inputs:  bool, has_piezo attribute (val)
        Outputs: None
        """
        if isinstance(val, bool):
            self._has_piezo = val
        else:
            self.logger.error("'has piezo' attribute must be of type bool")
            raise TypeError("'has piezo' attribute must be of type bool")

    @property
    def is_error(self):
        """
        Name:    Imaging.is_error
        Feature: Return is_error data member
        Inputs:  None
        Outputs: Boolean, value representing system state, indicating
                 whether or not the hardware controls have encountered
                 an error (self._is_error)
        """
        return self._is_error

    @is_error.setter
    def is_error(self, state):  # lint:ok
        """
        Name:    Imaging.is_error
        Feature: Set is_error data member to boolean value.
        Inputs:  Boolean, desired system state (state)
        Outputs: None
        """
        if isinstance(state, bool):
            self._is_error = state
        else:
            self.logger.error('_is_error must be a boolean.')
            raise TypeError('_is_error must be a boolean.')

    @property
    def is_running(self):
        """
        Name:    Imaging.is_running
        Feature: Return is_running data member
        Inputs:  None
        Outputs: Boolean, value representing system state, indicating
                 whether or not the system is currently in a imaging
                 sequence (self._is_running)
        """
        return self._is_running

    @is_running.setter
    def is_running(self, state):  # lint:ok
        """
        Name:    Imaging.is_running
        Feature: Set is_running data member to boolean value.
        Inputs:  Boolean, desired system state (state)
        Outputs: None
        """
        if isinstance(state, bool):
            self._is_running = state
        else:
            self.logger.error('_is_running must be a boolean.')
            raise TypeError('_is_running must be a boolean.')

    @property
    def led(self):
        """
        Name:    Imaging.led
        Feature: Return led data member
        Inputs:  None
        Outputs: LED, object representing a multi-colored LED that
                 inherits from the Adafruit_MotorHAT class (self._led)
        """
        if self.has_led:
            return self._led
        else:
            raise AttributeError('LED peripheral not configured.')

    @led.setter
    def led(self, value):  # lint:ok
        """
        Name:    Imaging.led
        Feature: _led attribute setter
        Inputs:  LED, object representing a multi-colored LED that
                 inherits from the Adafruit_MotorHAT class (value)
        Outputs: None
        """
        if self.has_led:
            if isinstance(value, LED()):  # lint:ok
                self._led = value
            else:
                self.logger.error('_led attribute must be a LED object.')
                raise TypeError('_led attribute must be a LED object.')
        else:
            raise AttributeError("LED peripheral not configured.")

    @property
    def mode(self):
        """
        Name:    imaging.mode
        Feature: mode attribute getter
        Inputs:  None
        Outputs: str, current imaging mode (self._mode)
        """
        return self._mode

    @mode.setter
    def mode(self, val):  # lint:ok
        """
        Name:    imaging.mode
        Feature: mode attribute setter
        Inputs:  str, desired imaging mode (val)
        Outputs: None
        """
        if isinstance(val, str):
            if val is '2D':
                self._mode = val
            elif val is '3D' and self.driver in self.supported_drivers:
                self._mode = val
            elif val is 'Explorer':
                self._mode = val
            else:
                if val is '3D':
                    self.logger.error("driver missing or unsupported,"
                                      " cannot boot 3D mode")
                    raise PRIDAPeripheralError("driver missing or unsupported,"
                                               " cannot boot 3D mode")
                self.logger.error("'{}' is not a valid prida mode".format(val))
                raise ValueError("'{}' is not a valid prida mode".format(val))
        else:
            self.logger.error('imaging mode must be of type string')
            raise TypeError('imaging mode must be of type string')

    @property
    def num_photos(self):
        """
        Name:    Imaging.num_photos
        Feature: Get the number of photos in a single expiriment.
        Inputs:  None
        Outputs: int, number of photos to be taken (self._num_photos)
        """
        return self._num_photos

    @num_photos.setter
    def num_photos(self, value):  # lint:ok
        """
        Name:    Imaging.num_photos
        Feature: Set the number of photos in a single expiriment.
        Inputs:  int, number of photos to be taken (value)
        Outputs: None
        """
        if isinstance(value, int):
            if value > 0:
                self._num_photos = value
            else:
                self.logger.error('Number of photos must be a positive value.')
                raise ValueError('Number of photos must be a positive value.')
        else:
            self.logger.error('Number of photos must be a integer.')
            raise TypeError('Number of photos must be a integer.')

    @property
    def piezo(self):
        """
        Name:    Imaging.piezo
        Feature: Return piezo data member
        Inputs:  None
        Outputs: Piezo, object representing a piezo-buzzer that inherits
                 from the Adafruit_MotorHAT class (self._piezo)
        """
        if self.has_piezo:
            return self._piezo
        else:
            raise AttributeError('Piezo peripheral not configured.')

    @piezo.setter
    def piezo(self, value):  # lint:ok
        """
        Name:    Imaging.piezo
        Feature: _piezo attribute setter
        Inputs:  Piezo, object representing a piezo-buzzer that
                 inherits from the Adafruit_MotorHAT class (value)
        Outputs: None
        """
        if self.has_piezo:
            if isinstance(value, Piezo()):  # lint:ok
                self._piezo = value
            else:
                self.logger.error('_piezo attribute must be a Piezo object.')
                raise TypeError('_piezo attribute must be a Piezo object.')
        else:
            raise AttributeError('Piezo peripheral not configured.')

    @property
    def position(self):
        """
        Name:    Imaging.position
        Feature: Indicates the current position of the bearing during the
                 imaging sequence.
        Inputs:  None
        Outputs: Float, current motor position in degrees
        """
        return self._position

    @position.setter
    def position(self, value):  # lint:ok
        """
        Name:    Imaging.position
        Feature: Updates the current position of the bearing during the imaging
                 sequence.
        Inputs:  Float, updated motor position value
        Outputs: None
        """
        if isinstance(value, int) or isinstance(value, float):
            if value >= 0:
                self._position = (value % 360)
            else:
                self.logger.error(
                    'Motor position cannot be a negative number.')
                raise ValueError('Motor position cannot be a negative number.')
        else:
            self.logger.error('Motor position must be a number [degrees].')
            raise TypeError('Motor position must be a number [degrees].')

    @property
    def settling_time(self):
        """The wait period between motor stopping and camera capture"""
        return self._settling_time

    @settling_time.setter
    def settling_time(self, val):  # lint:ok
        if isinstance(val, (int, float)):
            if val > 0.:
                self._settling_time = val
            else:
                self.logger.error('settling time must be a positive value.')
                raise ValueError('settling time must be a positive value')
        else:
            self.logger.error('settling time must be of type int or float')
            raise TypeError('settling time must be of type int or float')

    @property
    def step_count(self):
        """
        Name:    Imaging.step_count
        Feature: Returns the current microstep position
        Inputs:  None
        Outputs: Int, current motor position in microsteps (self._step_count)
        """
        return self._step_count

    @step_count.setter
    def step_count(self, value):  # lint:ok
        """
        Name:    Imaging.step_count
        Feature: sets the current microstep position
        Inputs:  Int, the new position in microsteps (value)
        Outputs: None
        """
        if isinstance(value, int):
            if value >= 0:
                self._step_count = value
            else:
                self.logger.error(
                    'microstep count must be a positive interger value.'
                    )
                raise ValueError(
                    'microstep count must be a positive interger value.'
                    )
        else:
            self.logger.error('microstep count must be an integer.')
            raise TypeError('microstep count must be an integer.')

    @property
    def supported_drivers(self):
        """
        Name:    Imaging.supported_drivers
        Feature: supported drivers set property getter
        Inputs:  None
        Outputs: set<str>, set of supported motor drivers
                 (self._supported_drivers)
        """
        return self._supported_drivers

    @supported_drivers.setter
    def supported_drivers(self, val):  # lint:ok
        """
        Name:    Imaging.supported_drivers
        Feature: supported drivers set property setter
        Inputs:  set<str>, set of supported motor drivers (val)
        """
        raise AttributeError('setting of supported drivers is forbidden')

    @property
    def taken(self):
        """
        Name:    Imaging.taken
        Feature: Get the number of photos in a single expiriment.
        Inputs:  None
        Outputs: int, number of photos that have been taken (self._taken)
        """
        return self._taken

    @taken.setter
    def taken(self, value):  # lint:ok
        """
        Name:    Imaging.taken
        Feature: Set the number of photos in a single expiriment.
        Inputs:  int, number of photos that have been taken (value)
        Outputs: None
        """
        if isinstance(value, int):
            if value >= 0:
                self._taken = value
            else:
                self.logger.error(
                    'Number of photos taken may not be negative.')
                raise ValueError('Number of photos taken may not be negative.')
        else:
            self.logger.error(
                'Number of photos taken must be an integer value.')
            raise TypeError('Number of photos taken must be an integer value.')

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def calibrate_motor(self):
        """
        Name:    Imaging.calibrate_motor
        Feature: Wrapper for controller.calibrate
        Inputs:  None
        Outputs: None
        """
        if self.mode is '3D':
            self.controller.calibrate()
            time.sleep(5)

    def camera_aperture(self):
        """
        Name:    imaging.camera_aperture
        Feature: wrapper function for camera's aperature property getter
        Inputs:  None
        Outputs: int, the camera's aperature (self.camera.aperture)
        """
        if self.mode is '2D' or self.mode is '3D':
            try:
                return self.camera.aperture
            except Exception as exp:
                self.logger.error(exp.args[0])
                return ''
        else:
            self.logger.warning('no camera connected. Ignoring request.')
            return ''

    def camera_exposure(self):
        """
        Name:    imaging.camera_exposure
        Feature: wrapper function for camera's exposure time property getter
        Inputs:  None
        Outputs: float, camera exposure time (self.camera.exposure_time)
        """
        if self.mode is '2D' or self.mode is '3D':
            try:
                return self.camera.exposure_time
            except Exception as exp:
                self.logger.error(exp.args[0])
                return ''
        else:
            self.logger.warning('no camera connected. Ignoring request.')
            return ''

    def camera_iso_speed(self):
        """
        Name:    imaging.camera_iso_speed
        Feature: wrapper function for camera's iso speed property getter
        Inputs:  None
        Outputs: str, camera iso speed (self.camera.iso_speed)
        """
        if self.mode is '2D' or self.mode is '3D':
            try:
                return self.camera.iso_speed
            except Exception as exp:
                self.logger.error(exp.args[0])
                return ''
        else:
            self.logger.warning('no camera connected. Ignoring request.')
            return ''

    def camera_make(self):
        """
        Name:    imaging.camera_make
        Feature: wrapper function for camera's exposure time property getter
        Inputs:  None
        Outputs: str, camera make (self.camera.make)
        """
        if self.mode is '2D' or self.mode is '3D':
            try:
                return self.camera.make
            except Exception as exp:
                self.logger.error(exp.args[0])
                return ''
        else:
            self.logger.warning('no camera connected. Ignoring request.')
            return ''

    def camera_model(self):
        """
        Name:    imaging.camera_model
        Feature: wrapper function for camera's model property getter
        Inputs:  None
        Outputs: str, camera model (self.camera.model)
        """
        if self.mode is '2D' or self.mode is '3D':
            try:
                return self.camera.model
            except Exception as exp:
                self.logger.error(exp.args[0])
                return ''
        else:
            self.logger.warning('no camera connected. Ignoring request.')
            return ''

    def check_error(self):
        """
        Name:    Imaging.check_error
        Feature: Checks if an error is still present in the system
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start.')
        if self.is_error is True:
            self.logger.info('attempting to determine origin...')
            try:
                self.calibrate_motor()
                self.turn_off_motors()
            except:
                tb = sys.exc_info()[2]
                self.logger.error('cannot communicate with motor.')
                raise PRIDAPeripheralError('Motor problem!').with_traceback(tb)
            try:
                self.camera.connect()
                self.camera.capture('test')
            except:
                tb = sys.exc_info()[2]
                self.logger.error('cannot communicate with camera.')
                raise PRIDAPeripheralError(
                    'Camera problem!').with_traceback(tb)
            else:
                self.logger.info('no hardware errors detected.')
                self.is_error = False
                self.check_status()
            self.logger.debug('complete.')

    def check_status(self):
        """
        Name:    Imaging.check_status
        Feature: Sets the LED status indicator to the appropriate color.
        Inputs:  None
        Outputs: None
        """
        if self.has_led:
            self.logger.debug('detecting system status')
            if self.is_error:
                self.led.yellow_on()
            elif self.is_running:
                self.led.red_on()
            else:
                self.led.green_on()
            self.logger.debug('adjusted led')

    def end_sequence(self):
        """
        Name:    Imaging.end_sequence
        Feature: Tries stopping the system.
                 If successful, turn off the motors and indicate ready
                 status. If it fails, indicate error status.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('start cleanup')
        if self.is_running:
            self.is_running = False
        try:
            self.reset()
            self.turn_off_motors()
        except:
            self.logger.exception('unexpected hardware error!')
            self.is_error = True
        finally:
            if not self.is_error:
                self.finished_alert()
            else:
                self.check_status()
                self.warning_alert()
            self.check_status()
            self.logger.info('imaging sequence complete.')

    def find_end_pos(self):
        """
        Name:    Imaging.find_end_pos
        Feature: Calculates the end position value at which the camera must
                 take a photo
        Inputs:  None
        Outputs: Int, position value in number of microsteps
        """
        if self.mode is '3D':
            if self.controller.step_type is 'micro':
                substeps_per_step = self.controller.microsteps
            elif self.controller.step_type is 'inter':
                substeps_per_step = 2
            else:
                substeps_per_step = 1
            pos_b = 360.0 * (1.0 - (1.0 / self.num_photos))
            unit_step = 360.0 / self.controller.step_res
            pos_m = (pos_b * self.gear_ratio)
            num_unit_steps = int((pos_m) / unit_step)
            min_angle = num_unit_steps * unit_step
            remaining_dist = pos_m - min_angle
            take_step = int((remaining_dist / unit_step) + 0.5)
            if take_step:
                tmp = int((num_unit_steps + 1) * substeps_per_step)
            else:
                tmp = int((num_unit_steps) * substeps_per_step)
        else:
            tmp = 0
        return tmp

    def find_next_pos(self):
        """
        Name:    Imaging.find_next_pos
        Feature: Calculates the next position value at which the camera must
                 take a photo
        Inputs:  None
        Outputs: Int, position value in number of microsteps
        """
        if self.mode is '3D':
            if self.controller.step_type is 'micro':
                substeps_per_step = self.controller.microsteps
            elif self.controller.step_type is 'inter':
                substeps_per_step = 2
            else:
                substeps_per_step = 1
            pos_list = [i * (360.0 / self.num_photos)
                        for i in range(self.num_photos)]
            unit_step = 360.0 / self.controller.step_res
            for index in range(len(pos_list)):
                pos_b = pos_list[index]
                pos_m = (pos_b * self.gear_ratio)
                num_unit_steps = int((pos_m) / unit_step)
                min_angle = num_unit_steps * unit_step
                remaining_dist = pos_m - min_angle
                take_step = int((remaining_dist / unit_step) + 0.5)
                if take_step:
                    pos_list[index] = int((num_unit_steps + 1)
                                          * substeps_per_step)
                else:
                    pos_list[index] = int(
                        (num_unit_steps) * substeps_per_step)
            res = pos_list[self.taken]
        else:
            res = 0
        return res

    def finished_alert(self):
        """
        Name:    Imaging.finished_alert
        Feature: Wrapper for piezo.beep for sequence completion
        Inputs:  None
        Outputs: None
        """
        if self.has_piezo:
            for i in range(3):
                self.piezo.beep()
                time.sleep(0.5)

    def get_settling_time_ms(self):
        """Deprecated"""
        return self.settling_time * 1000

    def led_off(self):
        """
        Name:    imaging.led_off
        Feature: wrapper function for led's all off behaviour
        Inputs:  None
        Outputs: None
        """
        if self.has_led:
            self.led.all_off()

    def load_peripherals(self, my_parser, config_filepath):
        """
        Name:    Imaging.load_peripherals
        Feature: Attempt to assign hardware peripheral attributes as designated
                 by the user in the prida config file
        Inputs:  - func, configuration file parsing function handle (my_parser)
                 - str, path to configuration file (config_filepath)
        Outputs: None
        """
        if self.mode is 'Explorer':
            self.logger.warning("Explorer mode is enabled---canceling imaging")
            raise PRIDAPeripheralError(
                crit=False,
                msg="Explorer Mode is set in config file")
        if self.mode is '2D' or self.mode is '3D':
            if Imaging.__dict__['_camera'] is None:
                try:
                    setattr(Imaging, '_camera',
                            Camera(my_parser, config_filepath))
                except PRIDAPeripheralError:
                    self.mode = 'Explorer'
                    raise
                except:
                    tb = sys.exc_info()[2]
                    self.mode = 'Explorer'
                    self.logger.exception('unexpected exception caught!')
                    raise PRIDAPeripheralError(
                        msg='could not instantiate camera, please check your '
                        'connection or select a different mode.'
                        ).with_traceback(tb)
        if self.mode is '3D':
            try:
                exec('from .{} import {}'.format(self.driver.lower(),
                                                 self.driver))
            except ImportError:
                tb = sys.exc_info()[2]
                self.logger.warning('failed to import driver, please check'
                                    ' your hardware is properly configured.')
                self.mode = 'Explorer'
                raise PRIDAPeripheralError(
                    msg='failed to import driver, please check your hardware'
                    ' is properly configured.').with_traceback(tb)
            if Imaging.__dict__['_controller'] is None:
                try:
                    exec("setattr(Imaging, '_controller',"
                         " {}(my_parser, config_filepath))".format(
                             self.driver))
                    self._end_pos = int(self.controller.microsteps *
                                        self.controller.step_res *
                                        self.gear_ratio)
                    bearing_rpm = self.controller.speed
                    motor_rpm = bearing_rpm * self.gear_ratio
                    self.controller.speed = motor_rpm
                except PRIDAPeripheralError:
                    self.mode = 'Explorer'
                    raise
                except:
                    tb = sys.exc_info()[2]
                    self.mode = 'Explorer'
                    self.logger.exception('unexpected exception caught!')
                    raise PRIDAPeripheralError(
                        msg='could not instantiate controller, please '
                        'check your connection or select a different '
                        'mode.').with_traceback(tb)
        if self.has_led:
            from .led import LED
            if Imaging.__dict__['_led'] is None:
                try:
                    setattr(Imaging, '_led', LED(my_parser, config_filepath))
                    self.led.green_on()
                except PRIDAPeripheralError:
                    self.has_led = False
                    raise
                except:
                    tb = sys.exc_info()[2]
                    self.has_led = False
                    self.logger.exception('unexpected exception caught!')
                    raise PRIDAPeripheralError(
                        msg='could not instantiate led, please check'
                        ' your connection or correct your config'
                        ' file.').with_traceback(tb)
        if self.has_piezo:
            from .piezo import Piezo
            if Imaging.__dict__['_piezo'] is None:
                try:
                    setattr(Imaging, '_piezo',
                            Piezo(my_parser, config_filepath))
                except PRIDAPeripheralError:
                    self.has_piezo = False
                    raise
                except:
                    tb = sys.exc_info()[2]
                    self.has_piezo = False
                    self.logger.exception('unexpected exception caught!')
                    raise PRIDAPeripheralError(
                        msg='could not instantiate piezo, please check your '
                        'connection or correct your config '
                        'file.').with_traceback(tb)

    def reset(self):
        """
        Name:    Imaging.reset
        Feature: Resets the imaging hardware
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('Resetting all counters...')
        self.reset_count()
        self.reset_position()
        self.logger.debug('Turning off the motor(s)...')
        self.turn_off_motors()
        self.check_status()
        self.logger.debug('complete.')

    def reset_count(self):
        """
        Name:    Imaging.reset_count
        Feature: Resets the number of images taken counter
        Inputs:  None
        Outputs: None
        """
        self.taken = 0

    def reset_position(self):
        """
        Name:    Imaging.reset_position
        Feature: Resets the position indicators values
        Inputs:  None
        Outputs: None
        """
        self.position = 0
        self.step_count = 0

    def run_sequence(self):
        """
        Name:    Imaging.run_sequence
        Feature: Attempts to determine where in the sequence the system is,
                 and then either step the motor, take an image, or end the
                 sequence as appropriate.
        Inputs:  None
        Outputs: tuple(bool, string, string, string, string), is the returned
                 filename a valid filename and the filename of the image
                 (is_filename, filename, date, time, angle)
        """
        if self.is_error and self.is_running:
            self.logger.exception('Error detected! ' +
                                  'Please address and try again.')
            self.end_sequence()
            raise PRIDAPeripheralError(msg='Error detected! ' +
                                       'Please address and try again.')
        elif self.is_error:
            try:
                self.check_error()
            except:
                self.logger.exception('Error not resolved! ' +
                                      'Please address and try again.')
                self.end_sequence()
                raise PRIDAPeripheralError(msg='Error not resolved! ' +
                                           'Please address and try again.')
            else:
                self.is_error = False
                self.logger.info('Error resolved, beginning sequence.')
        if not self.is_running:
            self.calibrate_motor()
            self.is_running = True
            self.check_status()
        if (self.taken < self.num_photos and
                self.step_count < self.find_next_pos()):
            try:
                self.step_motor()
            except:
                self.is_error = True
                self.check_status()
                self.logger.exception('Motor Error!')
                return (False, '', '', '', '', '', '', '')
            else:
                return (False, '', '', '', '', '', '', '')
        elif ((self.taken < self.num_photos) and
              (self.step_count >= self.find_next_pos())):
            try:
                filename = self.take_photo()
                str_args = filename.split('_')
                str_args[3] = os.path.splitext(str_args[3])[0]
                height = self.camera.image_height
                width = self.camera.image_width
                orientation = self.camera.orientation
            except:
                self.is_error = True
                self.check_status()
                self.logger.exception('Camera Error!')
                return (False, '', '', '', '', '', '', '')
            else:
                return (True,
                        filename,
                        str_args[1],
                        str_args[2],
                        str_args[3],
                        height,
                        width,
                        orientation)
        elif (self.taken < self.num_photos and
              self.step_count >= self.end_pos):
            self.logger.warning('something has gone wrong, motor is finished'
                                ' but camera is not. Are you in 2D mode?')
            self.end_sequence()
            return (False, '', '', '', '', '', '', '')
        else:
            self.end_sequence()
            return (False, '', '', '', '', '', '', '')

    def step_motor(self):
        """
        Name:    Imaging.step_motor
        Feature: Indexes the motor a single microstep,
                 updates the current position and number of microsteps.
        Inputs:  None
        Outputs: None
        """
        if self.mode is '3D':
            if self.controller.step_type is 'micro':
                substeps_per_step = self.controller.microsteps
            elif self.controller.step_type is 'inter':
                substeps_per_step = 2
            else:
                substeps_per_step = 1
            self.controller.single_step()
            self.position += ((1.0 / self.gear_ratio) *
                              (360.0 / (self.controller.step_res *
                                        substeps_per_step)))
            self.step_count += 1

    def take_photo(self):
        """
        Name:    Imaging.take_photo
        Feature: Captures a photo, updates the number of photos taken and
                 returns the image filename.
        Inputs:  None
        Outputs: Str, captured image file name (filename)
        """
        if self.mode is '2D' or self.mode is '3D':
            self.logger.debug('start.')
            name = '_'.join([self.cur_pid,
                             datetime.datetime.now().strftime(
                                 '%Y-%m-%d_%H.%M.%S'),
                             '%06.2f' % self.position])
            self.logger.info('photo name - %s' % (name))
            time.sleep(self.settling_time)
            filename = self.camera.capture(name, verbose=False)
            self.taken += 1
            self.logger.debug('complete.')
            return filename

    def turn_off_controller(self):
        """
        Name:    Imaging.turn_off_controller
        Feature: Wrapper for controller.poweroff
        Inputs:  None
        Outputs: None
        """
        if self.mode is '3D':
            self.controller.poweroff()

    def turn_off_motors(self):
        """
        Name:    Imaging.turn_off_motors
        Feature: Wrapper for controller.turn_off_motors
        Inputs:  None
        Outputs: None
        """
        if self.mode is '3D':
            self.controller.turn_off_motors()

    def warning_alert(self):
        """
        Name:    Imaging.warning_alert
        Feature: Wrapper for piezo.beep for error alert
        Inputs:  None
        Outputs: None
        """
        if self.has_piezo:
            self.piezo.beep(3, 1, 1)
