#!/usr/bin/python
#
# led.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 69344e0: Nathanael Shaw - 2016-02-18 16:26:50
#     hotfix basestring
# 0a00abf: Nathanael Shaw - 2016-02-02 12:07:53
#     module hierarchy draft
# 49461a4: Nathanael Shaw - 2016-01-29 15:21:33
#     hidden custom import stymied module creation
# 2d665f8: Nathanael Shaw - 2016-01-29 10:52:31
#     setup module hierarchy
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
from time import sleep
import logging

from .Adafruit_MotorHAT import Adafruit_MotorHAT
from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class LED(Adafruit_MotorHAT, PRIDAPeripheral):
    """
    Class representing a tri-color (RGB) LED
    """

    def __init__(self, my_parser=None, config_filepath=None):
        """
        Initialize PWM channels. Extends MotorHAT attributes and behaviours.
        """
        logging.debug('start initializing led.')
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self.logger = logging.getLogger(__name__)
        self._BLUE = 1         # Default channel for the blue LED
        self._GREEN = 14       # Default channel for the green LED
        self._RED = 15         # Default channel for the red LED
        self._led_type = 'CC'  # Default type Common Anode
        self.attr_dict = {'RED_LED_CHANNEL': 'red',
                          'BLUE_LED_CHANNEL': 'blue',
                          'GREEN_LED_CHANNEL': 'green',
                          'LED_TYPE': 'led_type',
                          }
        self._CURRENT_COLOR = ''
        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('complete.')

    @property
    def blue(self):
        """
        Name:    LED.blue
        Feature: Returns the integer designation of the blue LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the blue LED (self._BLUE)
        """
        return self._BLUE

    @property
    def green(self):
        """
        Name:    LED.green
        Feature: Returns the integer designation of the green LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the green LED (self._GREEN)
        """
        return self._GREEN

    @property
    def red(self):
        """
        Name:    LED.red
        Feature: Returns the integer designation of the red LED
        Inputs:  None
        Outputs: Integer, channel on the Adafruit_MotorHAT connected to
        the red LED (self._RED)
        """
        return self._RED

    @property
    def led_type(self):
        """
        Name:    LED.led_type
        Feature: Returns string representing multicolor LED
                 configuration
        Inputs:  None
        Outputs: String, two letter designation representing either
                 'Common Cathode' or 'Common Anode' (self._led_type)
        """
        return self._led_type

    @property
    def current_color(self):
        """
        Name:    LED.current_color
        Feature: Returns string representing current LED state
        Inputs:  None
        Outputs: Str, the current color of the led (self._CURRENT_COLOR)
        """
        return self._CURRENT_COLOR

    @led_type.setter
    def led_type(self, led_type):
        """
        Name:    LED.led_type
        Feature: Set type to common anode or common cathode.
        Inputs:  String, two letter type designation (led_type)
        Outputs: None
        """
        if isinstance(led_type, str):
            if led_type == 'CA' or led_type == 'CC':
                self._led_type = led_type
            else:
                self.logger.error('bad input. Values limited to CC or CA.')
                raise ValueError("Bad input. Values limited to CC or CA.")
        else:
            self.logger.error('led type attribute must be a string.')
            raise TypeError('led type attribute must be a string.')

    @blue.setter
    def blue(self, ch):
        """
        Name:    LED.blue
        Feature: Set the PWM channel controlling the blue led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if isinstance(ch, int):
            if ch in {0, 1, 14, 15}:
                self._BLUE = ch
            else:
                self.logger.error('channels limited to 0, 1, 14 or 15.')
                raise ValueError('channels limited to 0, 1, 14 or 15.')
        else:
            self.logger.error('blue channel number must be an integer.')
            raise TypeError('blue channel number must be an integer.')

    @green.setter
    def green(self, ch):
        """
        Name:    LED.green
        Feature: Set the PWM channel controlling the green led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if isinstance(ch, int):
            if ch in {0, 1, 14, 15}:
                self._GREEN = ch
            else:
                self.logger.error('channels limited to 0, 1, 14 or 15.')
                raise ValueError('channels limited to 0, 1, 14 or 15.')
        else:
            self.logger.error('green channel number must be an integer.')
            raise TypeError('green channel number must be an integer.')

    @red.setter
    def red(self, ch):
        """
        Name:    LED.red
        Feature: Set the PWM channel controlling the red led.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if isinstance(ch, int):
            if ch in {0, 1, 14, 15}:
                self._RED = ch
            else:
                self.logger.error('channels limited to 0, 1, 14 or 15.')
                raise ValueError('channels limited to 0, 1, 14 or 15.')
        else:
            self.logger.error('red channel number must be an integer.')
            raise TypeError('red channel number must be an integer.')

    @current_color.setter
    def current_color(self, color_str):
        """
        Name:    LED.current_color
        Feature: Returns string representing current LED state
        Inputs:  Str, a string representing the new led state (color_str)
        Outputs: None
        """
        if isinstance(color_str, str):
            if color_str in {'red', 'green', 'blue', 'yellow', 'white', ''}:
                self._CURRENT_COLOR = color_str
            else:
                self.logger.error('current color cannot'
                                  ' be set to %s' % (color_str))
                raise ValueError('current color cannot'
                                 ' be set to %s' % (color_str))
        else:
            self.logger.error('current color attribute must be a string')
            raise TypeError('current color attribute must be a string')

    def led_off(self, channel):
        """
        Name:    LED.led_off
        Feature: Set low a specifed led channel.
        Inputs:  int, the number of the channel to lower (channel)
        Outputs: None
        """
        self.logger.debug('turning off led channel %i', channel)
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 4096, 0)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 0, 4096)

    def led_on(self, channel):
        """
        Name:    LED.led_on
        Feature: Set high a specified led channel.
        Inputs:  int, the number of the channel to raise (channel)
        Outputs: None
        """
        self.logger.debug('turning on led channel %i', channel)
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 0, 4096)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 4096, 0)

    def red_on(self):
        """
        Name:    LED.red_on
        Feature: Set high the 'RED' PWM channel, default 15.
        Inputs:  None
        Outputs: None
        """
        if self.current_color != 'red':
            self.logger.debug('start.')
            self.led_on(self._RED)
            self.led_off(self._BLUE)
            self.led_off(self._GREEN)
            self.current_color = 'red'

    def blue_on(self):
        """
        Name:    LED.blue_on
        Feature: Set high the 'BLUE' PWM channel, default 1.
        Inputs:  None
        Outputs: None
        """
        if self.current_color != 'blue':
            self.logger.debug('start.')
            self.led_on(self._BLUE)
            self.led_off(self._RED)
            self.led_off(self._GREEN)
            self.current_color = 'blue'

    def green_on(self):
        """
        Name:    LED.green_on
        Feature: Set high the 'GREEN' PWM channel, default 14.
        Inputs:  None
        Outputs: None
        """
        if self.current_color != 'green':
            self.logger.debug('start.')
            self.led_on(self._GREEN)
            self.led_off(self._RED)
            self.led_off(self._BLUE)
            self.current_color = 'green'

    def yellow_on(self):
        """
        Name:    LED.yellow_on
        Feature: Set led color to yellow by combining green and red
                 light using default channels.
        Inputs:  None
        Outputs: None
        """
        if self.current_color != 'yellow':
            self.logger.debug('start.')
            self.led_on(self._RED)
            self.led_on(self._GREEN)
            self.led_off(self._BLUE)
            self.current_color = 'yellow'

    def red_off(self):
        """
        Name:    LED.red_off
        Feature: Set low the 'red' PWM channel, default 15.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('called')
        self.led_off(self._RED)
        if self.current_color == 'red':
            self.current_color = ''

    def blue_off(self):
        """
        Name:    LED.blue_off
        Feature: Set low the 'blue' PWM channel, default 1.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('called')
        self.led_off(self._BLUE)
        if self.current_color == 'blue':
            self.current_color = ''

    def green_off(self):
        """
        Name:    LED.green_off
        Feature: Set low the 'green' PWM channel, default 14.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('called')
        self.led_off(self._GREEN)
        if self.current_color == 'green':
            self.current_color = ''

    def yellow_off(self):
        """
        Name:    LED.yellow_off
        Feature: Set low the red and green PWM channels, removing yellow
                 light.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('called')
        self.red_off()
        self.green_off()
        if self.current_color == 'yellow':
            self.current_color = ''

    def all_off(self):
        """
        Name:    LED.all_off
        Feature: Set all led PWM channels to low.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('called')
        self.blue_off()
        self.green_off()
        self.red_off()
        self.current_color = ''

    def all_on(self):
        """
        Name:    LED.all_on
        Feature: Set all led PWM channels to high.
        Inputs:  None
        Outputs: None
        """
        if self.current_color != 'white':
            self.logger.debug('start.')
            self.led_on(self._BLUE)
            self.led_on(self._GREEN)
            self.led_on(self._RED)
            self.current_color = 'white'

    def connect(self):
        self.logger.debug('called')
        try:
            self.all_on()
            sleep(1)
            self.all_off()
        except:
            self.logger.error('could not connect to LED.')
            raise PRIDAPeripheralError('Could not connect to LED!')

    def poweroff(self):
        self.logger.debug('called')
        self.all_off()

    def current_status(self):
        self.logger.debug('called')
        super(self.current_status())
