#!/usr/bin/python
#
# piezo.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 0a00abf: Nathanael Shaw - 2016-02-02 12:07:53
#     module hierarchy draft
# 49461a4: Nathanael Shaw - 2016-01-29 15:21:33
#     hidden custom import stymied module creation
# 2d665f8: Nathanael Shaw - 2016-01-29 10:52:31
#     setup module hierarchy
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import time
import logging

from .Adafruit_MotorHAT import Adafruit_MotorHAT
from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class Piezo(Adafruit_MotorHAT, PRIDAPeripheral):

    def __init__(self, my_parser=None, config_filepath=None):
        logging.debug('start initializing piezo.')
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self.logger = logging.getLogger(__name__)
        self._CHANNEL = 0
        self._VOLUME = 1.0
        self.attr_dict = {'PWM_CHANNEL': 'channel',
                          'VOLUME': 'volume',
                          }
        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('complete.')

    @property
    def channel(self):
        """
        Name:    Piezo.channel
        Feature: Returns PWM channel controlling the piezo buzzer.
        Inputs:  None
        Outputs: Integer, channel number assigned to controlling the
                 piezo buzzer (self._CHANNEL)
        """
        return self._CHANNEL

    @property
    def volume(self):
        """
        Name:    Imaging.volume
        Feature: Returns float value representing percent of maximum
                 volume
        Inputs:  None
        Outputs: Float, value representing currently assigned piezo
                 buzzer volume (self._VOLUME)
        """
        return 100 * self._VOLUME

    @channel.setter
    def channel(self, ch):
        """
        Name:    Piezo.channel
        Feature: Set the PWM channel controlling the piezo buzzer.
        Inputs:  int, desired channel value (ch)
        Outputs: None
        """
        if isinstance(ch, int):
            if ch in {0, 1, 14, 15}:
                self._CHANNEL = ch

    @volume.setter
    def volume(self, volume):
        """
        Name:    Piezo.volume
        Feature: Set piezo volume by duty cycle.
        Inputs:  int, percent of maximum volume (volume)
        Outputs: None
        """
        if isinstance(volume, int) or isinstance(volume, float):
            if volume < 0:
                volume = 0
            elif volume > 100:
                volume = 100
            self._VOLUME = volume / 100.0

    def beep(self, num_beeps=3, beep_time=0.05, rest_time=0.1):
        """
        Name:    Piezo.beep
        Feature: Temporarily raise the 'piezo' channel. Default is three
                 short beeps in quick succession.
        Inputs:  - int, desired number of beeps (num_beeps)
                 - float, length of each beep in seconds (beep_time)
                 - float, length of silence between beeps (rest_time)
        Outputs: None
        """
        self.logger.debug('called')
        for i in range(num_beeps):
            # turn buzzer on
            self._pwm.setPWM(self._CHANNEL, 0, int(2048 * self._VOLUME))
            # sustain note
            time.sleep(beep_time)
            # turn buzzer off
            self._pwm.setPWM(self._CHANNEL, 0, 4096)
            # rest
            time.sleep(rest_time)

    def connect(self):
        self.logger.debug('start.')
        try:
            self.beep(2)
        except:
            self.logger.error('could not connect to piezo.')
            raise PRIDAPeripheralError('Could not connect to piezo!')
        self.logger.debug('complete.')

    def poweroff(self):
        self.logger.debug('called')
        self._pwm.setPWM(self._CHANNEL, 0, 0)

    def current_status(self):
        self.logger.debug('called')
        super(self.current_status())
