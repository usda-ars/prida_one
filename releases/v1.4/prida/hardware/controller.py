#!/usr/bin/python
#
# controller.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# b22fb9e: Nathanael Shaw - 2016-04-14 12:34:40
#     address #75. address #86. configurable periphs
# a757b19: Nathanael Shaw - 2016-04-13 13:03:19
#     fix typos, rmv more rpi dependacies
# 1fba20f: Nathanael Shaw - 2016-04-13 11:30:48
#     update controller docstr, rmv img. rpi dep.
# 65a13b4: Nathanael Shaw - 2016-04-13 10:41:52
#     add bin support, code cleanup
# 2d36763: Nathanael Shaw - 2016-04-12 17:15:21
#     mod controller class for serial com. with arduino
# d3acf8d: Nathanael - 2016-04-07 10:54:18
#     add controller and basecontroller, update imaging
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import time
import logging
import os
import glob

import serial

from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError
from .basecontroller import BaseController


###############################################################################
# CLASSES:
###############################################################################
class Controller(PRIDAPeripheral, BaseController):

    def __init__(self, my_parser=None, config_filepath=None):
        """
        Initialize data memebers. Inherit attributes and
        behaviours from the BaseController and PridaPeripheral absctract
        classes.
        Note: currently assumes that system is always using
        microstepping.
        """
        logging.debug('start initializing controller.')
        super(Controller, self).__init__()
        self.logger = logging.getLogger(__name__)
        # binary motor serial comuunication values
        self.CAL = bin(127)       # 01111111 <-- calibrate command
        self.ECHO = bin(32)       # 00100000 <-- echo command
        self.RELEASE = bin(0)     # 00000000 <-- release command

        self.STEP = bin(64)       # 01000000 <-- step flag
        self.FORWARD = bin(16)    # 00010000 <-- step - direction
        self.BACKWARD = bin(0)    # 00000000 <-- step - direction
        self.SINGLE = bin(1)      # 00000001 <-- step - regieme
        self.DOUBLE = bin(2)      # 00000010 <-- step - regieme
        self.INTER = bin(4)       # 00000100 <-- step - regieme
        self.MICRO = bin(8)       # 00001000 <-- step - regieme
        # command = flag + dir + reg
        # private attributes for property definitions
        self._rpm = 2.0           # Approximate rotational velocity
        self._step_res = 200      # Number of units steps per rotation
        self._microsteps = 8      # Number of microsteps per unit step
        self._motor_port_num = 1  # Motor Controller Port Number In Use
        self._base_rot_time = 1.75 * self._microsteps
        self._serial_port = '/dev/ttyACM0'
        self._baud_rate = 9600
        self._my_serial = None
        self._stepstr = self.binadd(self.STEP, self.FORWARD, self.MICRO)
        # configure controller from config file
        self.attr_dict = {'RPM': 'rpm',
                          'DEGREES_PER_STEP': 'degrees',
                          'PORT_NUMBER': 'motor_port_num',
                          }
        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('complete.')
        # set property values
        self.timeout = 5

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def base_rot_time(self):
        """
        Name:    Controller.base_rot_time
        Feature: Returns the minimum amount of time at which the motor
                 can complete a full rotation with a microstepping
                 regieme
        Inputs:  None
        Outputs: Float, minimum number of seconds for a full rotation
                 (self._base_rot_time)
        """
        return self._base_rot_time

    @base_rot_time.setter
    def base_rot_time(self, val):
        """
        Name:    Controller.base_rot_time
        Feature: Set stepper motor base rotation time (in seconds)
                 During full speed operation.
        Inputs:  - int, minmum rotation time in seconds (val)
        Outputs: None
        """
        if isinstance(val, int) or isinstance(val, float):
            if val > 0:
                self._base_rot_time = val
            else:
                self.logger.error('base rotation time' +
                                  ' must be non-negative.')
                raise ValueError('Base rotation time must be non-negative.')
        else:
            self.logger.error('base rotation time must be a number.')
            raise TypeError('Base rotation time must be a number.')

    @property
    def baud_rate(self):
        """
        Name:    Controller.baud_rate
        Feature: baud rate property getter
        Inputs:  None
        Outputs: int, baud rate (self._baud_rate)
        """
        return self._baud_rate

    @baud_rate.setter
    def baud_rate(self, val):
        """
        Name:    Controller.baud_rate
        Feature: Baud rate property setter
        Inputs:  int, baud rate (val)
        Outputs: None
        """
        if isinstance(val, int):
            if val > 0:
                self._baud_rate = val
            else:
                self.logger.error('baud rate must be a positive value')
                raise ValueError('baud rate must be a positive value')
        else:
            self.logger.error('baud rate must be an integer')
            raise TypeError('baud rate must be an integer')

    @property
    def degrees(self):
        """
        Name:    Controller.degrees
        Feature: Returns Step Resolution in terms of degrees per unit step.
        Inputs:  None
        Outputs: Float, degree representation of step resolution
                 (Controller.degrees)
        """
        return 360.0 / self.step_res

    @degrees.setter
    def degrees(self, value):
        """
        Name:    Controller.degrees
        Feature: Sets Step Resolution in terms of degrees per unit step.
        Inputs:  Float, desired degree representation of step resolution
                 (Controller.degrees)
        Outputs: None
        """
        if isinstance(value, int) or isinstance(value, float):
            if 0 < value < 360:
                if (360.0 / value) % 1 == 0:
                    self.step_res = int(360 / value)
                else:
                    self.logger.error('degrees attribute' +
                                      ' must evenly divide 360.')
                    raise ValueError('Degrees attribute' +
                                     ' must evenly divide 360.')
            else:
                self.logger.error('degrees must be a value between 0 and 360.')
                raise ValueError('degrees must be a value between 0 and 360.')
        else:
            self.logger.error('degrees must be a integer or float.')
            raise TypeError('degrees must be a integer or float.')

    @property
    def microsteps(self):
        """
        Name:    Controller.microsteps
        Feature: Returns number of microsteps per unit step
        Inputs:  None
        Outputs: Int, number of microsteps per unit step (self.microsteps)
        """
        return self._microsteps

    @microsteps.setter
    def microsteps(self, value):
        """
        Name:    Controller.microsteps
        Feature: Sets the number of microsteps per unit step
                 NOTE: currently only 8 us/step is supported by controller
        Inputs:  Int, number of microsteps per unit step (value)
        Outputs: None
        """
        if isinstance(value, int):
            if value in {8}:
                self._microsteps = value
            else:
                self.logger.error(
                    'number of microsteps per step must be a multiple of four.'
                    )
                raise ValueError(
                    'number of microsteps per step must be a multiple of four.'
                    )
        else:
            self.logger.error(
                'number of microsteps per step must be an integer.')
            raise TypeError(
                'number of microsteps per step must be an integer.')

    @property
    def motor_port_num(self):
        """
        Name:    Controller.motor_port_num
        Feature: Returns the port number which the controller is currently
                 controlling
        Inputs:  None
        Outputs: Integer, the number representing the in-use motor controller
                 port (self._motor_port_num)
        """
        return self._motor_port_num

    @motor_port_num.setter
    def motor_port_num(self, num):
        """
        Name:    Controller.motor_port_num
        Feature: Set stepper motor port number.
        Inputs:  int, desired port number to be used on the controller (num)
        Outputs: None
        """
        if isinstance(num, int):
            if num in {1, 2}:
                self._motor_port_num = num
            else:
                self.logger.error('motor port number restricted to 1 or 2.')
                raise ValueError('motor port number restricted to 1 or 2.')
        else:
            self.logger.error('motor port number must be an integer.')
            raise TypeError('motor port number must be an integer.')

    @property
    def my_serial(self):
        """
        Name:    Controller.my_serial
        Feature: serial property getter
        Inputs:  None
        Outputs: Serial, a python serial object (self._my_serial)
        """
        if self._my_serial is None:
            self._my_serial = serial.Serial(self.serial_port, self.baud_rate)
            self._my_serial.timeout = self.timeout
        return self._my_serial

    @my_serial.setter
    def my_serial(self, val):
        """
        Name:    Controller.my_serial
        Feature: serial property setter
        Inputs:  Serial, a python serial object (val)
        Outputs: None
        """
        raise AttributeError('setting of the serial device atr is forbidden')

    @property
    def rpm(self):
        """
        Name:    Controller.rpm
        Feature: Returns the approximate RPM value at which the motor is
                 to spin
        Inputs:  None
        Outputs: Float, value approximating currently assigned motor
                 rotational speed (self._rpm)
        """
        return self._rpm

    @rpm.setter
    def rpm(self, rot_per_min):
        """
        Name:    Controller.rpm
        Feature: Set approximate rotations per minute value for the
                 motor.
        Inputs:  float, desired rpm value (rot_per_min)
        Outputs: None
        """
        if isinstance(rot_per_min, float) or isinstance(rot_per_min, int):
            if rot_per_min > 0:
                self._rpm = rot_per_min
            else:
                self.logger.error('RPM requires a non-negative value.')
                raise ValueError('RPM requires a non-negative value.')
        else:
            self.logger.error('RPM must be a number.')
            raise TypeError('RPM must be a number.')

    @property
    def serial_port(self):
        """
        Name:    Controller.serial_port
        Feature: serial port property getter
        Inputs:  None
        Outputs: str, computer serial port location (self._serial_port)
        """
        return self._serial_port

    @serial_port.setter
    def serial_port(self, val):
        """
        Name:    Controller.serial_port
        Feature: serial port property setter
        Inputs:  str, computer serial port location (val)
        Outputs: None
        """
        if os.name is 'posix':
            if isinstance(val, str):
                if val in glob.glob('/dev/ttyACM*'):
                    self._serial_port = val
                else:
                    self.logger.error('%s is not a valid '
                                      'path to a serial device' % (val))
                    raise ValueError('%s is not a valid '
                                     'path to a serial device' % (val))
            else:
                self.logger.error('serial port attribute must be a string')
                raise TypeError('serial port attribute must be a string')
        else:
            self.logger.error('non-posix serial communication not currently'
                              ' supported')
            raise OSError('non-posix serial communication not currently'
                          ' supported')

    @property
    def stepstr(self):
        """
        Name:    Controller.stepstr
        Feature: default step command property getter
        Inputs:  None
        Outputs: str, string rep of binary step command (self._stepstr)
        """
        return self._stepstr

    @stepstr.setter
    def stepstr(self, val):
        """
        Name:    Controller.stepstr
        Feature: default step command property setter
        Inputs:  str, string representation of binary step command (val)
        Outputs: None
        """
        if isinstance(val, str):
            try:
                int(val, 2)
            except:
                self.logger.exception('stepstr value must be a binary string')
                raise
            else:
                self._stepstr = val

    @property
    def step_res(self):
        """
        Name:    Controller.step_res
        Feature: Returns the number of unit steps within a single revolution
                 at which the motor is currently configured
        Inputs:  None
        Outputs: Integer, number of unit steps per full rotation
                 (self._step_res)
        """
        return self._step_res

    @step_res.setter
    def step_res(self, sr):
        """
        Name:    Controller.step_res
        Feature: Set stepper motor step resolution.
        Inputs:  int, desired 'step resolution,' or the number of
                 steps in a full rotation, defining the length of a
                 unit step (sr)
        Outputs: None
        """
        if isinstance(sr, int):
            if sr > 0:
                self._step_res = sr
            else:
                self.logger.error('step resolution must be non-negative.')
                raise ValueError('Step resolution must be non-negative')
        else:
            self.logger.error('step resolution must be an integer.')
            raise TypeError('Step resolution must be an integer')

    @property
    def timeout(self):
        """
        Name:    Controller.timeout
        Feature: serial communication timeout property getter
        Inputs:  None
        Outputs: float, seconds to timeout (self.my_serial.timeout)
        """
        return self.my_serial.timeout

    @timeout.setter
    def timeout(self, val):
        """
        Name:    Controller.timeout
        Feature: serial communication timeout property setter
        Inputs:  float, seconds to timeout (val)
        Outputs: None
        """
        if isinstance(val, int) or isinstance(val, float):
            if val >= 0:
                self.my_serial.timeout = val
            else:
                self.logger.debug('timeout atr must be a non-negative value')
                raise ValueError('timeout atr must be a non-negative value')
        else:
            self.logger.debug('timeout atr must be a number of type int'
                              ' or float')
            raise TypeError('timeout atr must be a number of type int'
                            ' or float')

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def bin_to_bytes(self, binstr):
        """
        Name:    Controller.bin_to_bytes
        Feature: converts a binary string to the equivilent ASCII byte string
        Inputs:  str, binary string to be converted
        Outputs: str, byte string representation (bystr)
        """
        decnum = int(binstr, 2)
        ustr = chr(decnum)
        bystr = ustr.encode('utf-8')
        return bystr

    def binadd(self, *args, **kwargs):
        """
        Name:    Controller.binadd
        Feature: adds together an arbitary number of python binary strings
        Inputs:  - any number of binary string arguments (*args)
                 - any number of additional keyword arguments (**kwargs)
        Outputs: str, resultant binary string sum (binsum)
        """
        binsum = 0
        for arg in args:
            binsum += int(arg, 2)
        return bin(binsum)

    def bytes_to_bin(self, bystr):
        """
        Name:    Controller.bytes_to_bin
        Feature: converts a byte string ASCII character to its eqivalent
                 binary string value
        Inputs:  str, byte string to convert (bystr)
        Outputs: str, resulting binary string (binstr)
        """
        ustr = bystr.decode('utf-8')
        decnum = ord(ustr)
        binstr = bin(decnum)
        return binstr

    def calibrate(self):
        """
        Name:    Controller.calibrate
        Feature: Take three initial unit steps to engage coils.
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('calibrating motor...')
        self.my_serial.write(self.bin_to_bytes(self.CAL))

    def connect(self):
        """
        Name:    Controller.connect
        Feature: attempt to connect the computer to the controller
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('establishing connection...')
        status = None
        try:
            mybyte = self.my_serial.read(size=1)
            if mybyte == b'':
                raise PRIDAPeripheralError('could not read value from motor!'
                                           ' timed out.')
        except:
            self.logger.exception('exception caught during serial read')
            try:
                self.logger.warning('controller may already be connected,'
                                    ' checking status...')
                status = self.current_status()
            except:
                raise
        else:
            if mybyte == self.bin_to_bytes(self.RELEASE):
                self.my_serial.reset_input_buffer()
                self.my_serial.reset_output_buffer()
                status = self.current_status()
                if status == 1:
                    self.logger.debug('connection established.')
            else:
                self.logger.error('unexpected character recieved!'
                                  ' please check your controller.')
                raise PRIDAPeripheralError('unexpected character recieved!'
                                           ' please check your controller.')
        return status

    def current_status(self):
        """
        Name:    Controller.current_status
        Feature: Attempt a simple handshake with the controller
        Inputs:  None
        Outputs: int, status indicator (status)
        """
        status = 0
        self.logger.debug('attempting handshake...')
        # write echo command ascii character to serial port
        self.my_serial.write(self.bin_to_bytes(self.ECHO))
        my_bytes = self.my_serial.read(size=1)
        if my_bytes == b'':
            raise PRIDAPeripheralError('could not read value from motor!'
                                       ' timed out.')
        elif self.bytes_to_bin(my_bytes) == self.ECHO:
            status = 1
            return status
        else:
            raise PRIDAPeripheralError('bad return on echo call,'
                                       ' recieived %s' % (str(my_bytes)))

    def poweroff(self):
        """
        Name:    Controller.poweroff
        Feature: sends the serial command to power down the coils
        Inputs:  None
        Outputs: None
        """
        self.logger.debug('powering down the motor...')
        self.my_serial.write(self.bin_to_bytes(self.RELEASE))

    def single_step(self):
        """
        Name:    motor.single_step
        Feature: Takes a single microstep
        Inputs:  None
        Outputs: None
        """
        # data member error checking
        if self.rpm <= 0:
            self.logger.error('RPM value must be greater than zero.')
            raise ValueError("RPM must be greater than zero.")
        try:
            # convert RPM to req'd value for calculating wait time
            real_rpm = (1.0 / (((60.0 / self.rpm) -
                               self.base_rot_time) / 60.0))
            if real_rpm <= 0:
                raise ZeroDivisionError()
        except ZeroDivisionError:
            self.logger.exception('set a valid RPM value before' +
                                  ' attempting a step.')
            raise AttributeError("Set a valid RPM value" +
                                 " before attempting a step.")
        else:
            self.my_serial.write(self.bin_to_bytes(self.stepstr))
            time.sleep(60.0 / (self.step_res * self.microsteps * real_rpm))

    def turn_off_motors(self):
        """
        Name:    Controller.turn_off_motors
        Feature: Disable all motors attached to the serial port.
        Inputs:  None
        Outputs: None
        """
        self.poweroff()
