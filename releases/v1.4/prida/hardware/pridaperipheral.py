#!/usr/bin/python
#
# pridaperipheral.py
#
# VERSION: 1.4.0
#
# LAST EDIT: 2016-06-30
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# 0a00abf: Nathanael Shaw - 2016-02-02 12:07:53
#     module hierarchy draft
# 49461a4: Nathanael Shaw - 2016-01-29 15:21:33
#     hidden custom import stymied module creation
# 2d665f8: Nathanael Shaw - 2016-01-29 10:52:31
#     setup module hierarchy
# $log_end_tag$
#


###############################################################################
# CLASSES:
###############################################################################
class PRIDAPeripheral(object):
    """
    Base 'Abstract Class' that all PRIDA harddware inherit from for proper
    polymorphic behaviour. Garuntee's the existance of the poweroff, connect
    and current_status behviours in addition to the state attribute as well as
    the base PRIDAPeripheralError exception.
    """

    def __init__(self):
        """
        Initialize and declare common peripheral instance variables.
        """
        self._state = None

    @property
    def state(self):
        """
        Getter for state attribute; to be overwritten by child class.
        """
        return self._state

    @state.setter
    def state(self, value):  # lint:ok
        """
        Setter for state attribute; to be overwritten by child class.
        """
        self._state = value

    @state.deleter
    def state(self):  # lint:ok
        """
        Deleter for state attribute; to be overwritten by child class.
        """
        del self._state

    def poweroff(self):
        """
        Name:    PRIDAPeripheral.poweroff
        Feature: Abstract behaviour. Override by child with shutdown behaviour.
        Inputs:  None
        Outputs: None
        """
        raise PRIDAPeripheralError('All peripherals require ' +
                                   'a shutdown sequence!')

    def connect(self):
        """
        Name:    PRIDAPeripheral.connect
        Feature: Abstract behaviour. Override by child with connection
                 establishing behaviour.
        Inputs:  None
        Outputs: None
        """
        raise PRIDAPeripheralError('All peripherals require ' +
                                   'a connect sequence!')

    def current_status(self):
        """
        Name:    PRIDAPeripheral.current_status
        Feature: Abstract behaviour. Override by child with behaviour for
                 attempting communication between the system and the
                 peripheral.
        Inputs:  None
        Outputs: None
        """
        raise PRIDAPeripheralError('All peripherals require ' +
                                   'knowledge about their current status!')


class PRIDAPeripheralError(Exception):
    """
    Base 'Exception' class for errors specific to PRIDA hardware.
    """
    def __init__(self, *args, crit=True, msg='', **kwargs):  # lint:ok
        """Class initialization"""
        Exception.__init__(self, args, kwargs)
        self._critical = crit
        if msg != '':
            self._message = msg
        elif len(args) > 0:
            self._message = str(args[0])
        else:
            self._message = None

    def __str__(self):
        """Return string for print calls"""
        return self.message

    @property
    def critical(self):
        """Critical error flag"""
        return self._critical

    @critical.setter
    def critical(self, val):  # lint:ok
        if isinstance(val, bool):
            self._critical = val
        else:
            raise TypeError('critical attribute must be of type bool')

    @property
    def message(self):
        """Error message"""
        return self._message

    @message.setter
    def message(self, val):  # lint:ok
        if isinstance(val, str):
            self._message = val
        else:
            raise TypeError('message attribute must be of type string')
