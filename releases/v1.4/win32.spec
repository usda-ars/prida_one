# -*- mode: python -*-

block_cipher = None


my_path = '.\\'
prida_path = my_path + 'prida\\'

a = Analysis([my_path + 'main.py'],
             pathex=['.'],
             binaries=None,
             datas=None,
             hiddenimports=['h5py.defs',
                            'h5py.utils',
                            'h5py.h5ac',
                            'h5py._proxy',
                            'scipy.linalg',
                            'scipy.linalg.cython_blas',
                            'scipy.linalg.cython_lapack',
                            'scipy.integrate'],
             hookspath=None,
             runtime_hooks=None,
             excludes=None,
             win_no_prefer_redirects=None,
             win_private_assemblies=None,
             cipher=block_cipher)

pyz = PYZ(a.pure,
          a.zipped_data,
          cipher=block_cipher)

a.datas += [('greeter.jpg', prida_path + 'greeter.jpg', 'DATA'),
            ('loading.gif', prida_path + 'loading.gif', 'DATA')]

# To get rid of the popup warning message in Windows:
a.binaries -= [('.\pywintypes34.dll', None, None)]

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='Prida.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False,
          icon='icon.ico')
