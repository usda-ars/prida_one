#!/usr/bin/python
#
# motor.py
#
# VERSION: 0.1.3
#
# LAST EDIT: 2015-06-25
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# d5735cb: twdavis - 2015-06-25 14:43:06
#     working copy of PRIDA v0.1
# f9c4529: Nathanael Shaw - 2015-06-24 11:57:30
#     Modifed Motor.py step function, added RPM selection support.
# 6a90843: Jay - 2015-06-18 13:58:07
#     bugfixes
# 48d047e: Jay - 2015-06-16 14:10:50
#     v0.1.1
# db5bcc3: Hoi Cheng - 2015-06-16 01:46:15
#     v0.0.4
# 50cf878: Jay - 2015-06-15 13:48:22
#     v0.0.4
# ea22481: jay - 2015-06-14 02:33:49
#     v0.0.2
# 1de1b65: Jay - 2015-06-12 14:04:23
#     Merge branch 'master' of https://bitbucket.org/usda-ars/prida
# f4ae6b2: Jay - 2015-06-12 14:02:08
#     0.0.1
# 2b9a07c: Jay - 2015-06-11 15:12:30
#     Working on GUI
# 0ef64ad: Jay - 2015-06-11 11:33:26
#     fixing loops
# 629d58c: Jay - 2015-05-30 02:08:46
#     Setting up GUI
# $log_end_tag$
#
from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor, Adafruit_StepperMotor
import time

# create a default object, no changes to I2C address or frequency
mh = Adafruit_MotorHAT()

# recommended for auto-disabling motors on shutdown!
def turnOffMotors():
    mh.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
    mh.getMotor(2).run(Adafruit_MotorHAT.RELEASE)
    mh.getMotor(3).run(Adafruit_MotorHAT.RELEASE)
    mh.getMotor(4).run(Adafruit_MotorHAT.RELEASE)

def calibrate():
    myStepper = mh.getStepper(200, 1)   # Steps/rev (unused), motor port #
    myStepper.setSpeed(1)               # RPM
    for i in xrange(24):
        myStepper.oneStep(Adafruit_MotorHAT.FORWARD, Adafruit_MotorHAT.MICROSTEP)
        time.sleep(.1)

def step(np):
    myStepper = mh.getStepper(200, 1)   # Steps/rev (unused), motor port #
    myStepper.setSpeed(5)               # RPM (orig set at 1)
    for i in xrange(1600/np):
        myStepper.oneStep(Adafruit_MotorHAT.FORWARD, Adafruit_MotorHAT.MICROSTEP)
        time.sleep(.02)
