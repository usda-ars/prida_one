#!/usr/bin/python
#
# PRIDA.py
#
# VERSION: 0.1.3
#
# LAST EDIT: 2015-06-25
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# d5735cb: twdavis - 2015-06-25 14:43:06
#     working copy of PRIDA v0.1
# 1e012bb: jay - 2015-06-22 01:19:16
#     v0.2.1
# $log_end_tag$
#
# ------------
# description:
# ------------
# User interface for PRIDA.

from Tkinter import *
from tkFileDialog import *
#from tkMessageBox import *
import camera
import motor as m
import sys
#from functools import partial
import os
import time
import atexit
from PIL import Image, ImageTk
import hdf_organizer
import datetime

class App(Tk):

    def __init__(self):
        Tk.__init__(self)
        self.c = camera.Camera()
        #self.attributes('-fullscreen', True)
        main_frame = Frame(background="blue",
            height = self.winfo_screenheight()-40,
            width = self.winfo_screenwidth()-40)
        #main_frame.grid_propagate(False)
        main_frame.grid(padx=20, pady=20)
        main_frame.update()

        self.mf = MetaF(main_frame)
        self.mf.grid(ipadx=10, ipady=10, row=0, column=0)
        self.mf.update()

        remaining_width = main_frame.winfo_width() - self.mf.winfo_width()
        remaining_height = main_frame.winfo_height() - self.mf.winfo_height()

        self.cf = ConsoleF(main_frame, self.mf.winfo_width(), remaining_height)
        self.cf.grid(row=1, column=0)
        self.cf.update()

        self.pf = PicF(main_frame, remaining_width,
            main_frame.winfo_height())
        self.pf.grid_propagate(False)
        self.pf.grid(row=0, column=1, rowspan=2)
        #self.columnconfigure(1, minsize=remaining_width)
        self.pf.update()

        #self.pf.update_preview()
        self.src = None
        #self.validate()
        #main_frame.columnconfigure(1, weight=1)
        Button(main_frame, text="Go!", command=self.validate).grid(row=1, column=0)
        Button(main_frame, text="Save as HDF5 to Workspace", command=self.toHDF).grid(row=0, column=1)
        #print (self.winfo_screenheight())
        #print (self.winfo_screenwidth())
        #main_frame.lower()

  #  def animation(self):
  #      self.main_frame.update()
  #      self.main_frame.config(width=self.main_frame.winfo_width()-50)
  #      #mf.width = mf.width-1
  #      self.main_frame.update()
  #      self.after(100, self.animation)


    def validate(self):
        print("validating")
        if (os.path.isdir(self.mf.workspace.get())):
            self.src = self.mf.workspace.get() + "/" + self.mf.session.get() + "/"
            self.filename = self.mf.workspace.get() + "/" + self.mf.session.get() + ".hdf"
            try:
                if os.path.isdir(self.src) or os.path.isfile(self.filename):
                    print("Invalid session name or session already exists")
                    return
                else:
                    print(self.src)
                    os.makedirs(self.src)
                    self.c.setPath(self.src)
                    self.c.connect()
            except:
                print("Could not create session")
            m.calibrate()
            self.go()
        else:
            print("Workspace does not exist")

    def go(self):
        print("here i am")
        #self.c.connect()
        print("there we are")
        #print(self.c.capture())
        print(self.mf.num_photos.get())
        if (self.c.capture() > int(self.mf.num_photos.get())):
            self.c.resetCount()
            return
        print("where am i")
        m.step(int(self.mf.num_photos.get()))
        print("yay")
        self.after(00000, self.go)

    def toHDF(self):
        #self.validate()
        file_name = "1.hdf5"
        hdf = hdf_organizer.PRIDA_HDF(self.mf.workspace.get() + "/", file_name)

        hdf.set_root_metadata(self.mf.researcher.get(),
                              self.mf.email.get())

        hdf.create_experiment(self.mf.expr_title.get(),
                              self.mf.genus.get(),
                              self.mf.species.get(),
                              self.mf.cultivar.get())

        hdf.create_rep(self.mf.get_date(),
                       self.mf.rep_number.get(),
                       int(self.mf.num_photos.get()),
                       self.mf.notes.get())

        hdf.save_images(self.src)

        hdf.save()

class ConsoleF(Frame):

    def __init__(self, parent, w, h):
        Frame.__init__(self, parent, background="green",
            width=w, height=h)

class PicF(Frame):

    def __init__(self, parent, w, h):
        Frame.__init__(self, parent, background="pink",
            width=w, height=h)

    def update_preview(self):
        img = Image.open("DSC_0001.JPG")
        img.thumbnail((150,100), Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(img)
        label = Label(self, image=photo)
        label.image = photo
        label.place(relx=0.5, rely=0.5, anchor=CENTER)
        #label.grid(sticky=NSEW)

class MetaF(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent, borderwidth=1, relief=RAISED)
            #height = root.winfo_screenheight(), width = root.winfo_screenwidth())

        self.columnconfigure(0, minsize=20)
        self.columnconfigure(2, pad=20)
        self.rowconfigure(0, minsize=20)
        for i in xrange(1, 13):
            self.rowconfigure(i, uniform="r")

        Label(self, text="Workspace:").grid(row=1, column=1, sticky=W)
        self.workspace = Entry(self, width=30)
        self.workspace.grid(row=1, column=2)
        Button(self, text="Browse...", command=self.set_workspace).grid(row=1, column=3)

        Label(self, text="Session Name:").grid(row=2, column=1, sticky=W)
        self.session = Entry(self, width=30)
        self.session.grid(row=2, column=2)

        Label(self, text="Researcher:").grid(row=3, column=1, sticky=W)
        self.researcher = Entry(self, width=30)
        self.researcher.grid(row=3, column=2)

        Label(self, text="Email:").grid(row=4, column=1, sticky=W)
        self.email = Entry(self, width=30)
        self.email.grid(row=4, column=2)

        Label(self, text="Experiment Title:").grid(row=5, column=1, sticky=W)
        self.expr_title = Entry(self, width=30)
        self.expr_title.grid(row=5, column=2)

        Label(self, text="Genus:").grid(row=6, column=1, sticky=W)
        self.genus = Entry(self, width=30)
        self.genus.grid(row=6, column=2)

        Label(self, text="Species:").grid(row=7, column=1, sticky=W)
        self.species = Entry(self, width=30)
        self.species.grid(row=7, column=2)

        Label(self, text="Cultivar:").grid(row=8, column=1, sticky=W)
        self.cultivar = Entry(self, width=30)
        self.cultivar.grid(row=8, column=2)

        Label(self, text="Month:").grid(row=9, column=1, sticky=W)
        self.month = Entry(self, width=30)
        self.month.grid(row=9, column=2)

        Label(self, text="Day:").grid(row=10, column=1, sticky=W)
        self.day = Entry(self, width=30)
        self.day.grid(row=10, column=2)

        Label(self, text="Year:").grid(row=11, column=1, sticky=W)
        self.year = Entry(self, width=30)
        self.year.grid(row=11, column=2)

        Label(self, text="Rep Number:").grid(row=12, column=1, sticky=W)
        self.rep_number = Entry(self, width=30)
        self.rep_number.grid(row=12, column=2)

        Label(self, text="Number of Images:").grid(row=13, column=1, sticky=W)
        self.num_photos = Entry(self, width=30)
        self.num_photos.grid(row=13, column=2)

        Label(self, text="Notes:").grid(row=14, column=1, sticky=W)
        self.notes = Entry(self, width=30)
        self.notes.grid(row=14, column=2)

        #self.get_date()

        #self.lower()

    def set_workspace(self):
        self.workspace.delete(0, END)
        self.workspace.insert(0, askdirectory())

    def get_date(self):
        return datetime.date(int(self.year.get()), int(self.month.get()), int(self.day.get()))
        #datetime.datetime

if __name__ == "__main__":
    atexit.register(m.turnOffMotors)
    App().mainloop()

