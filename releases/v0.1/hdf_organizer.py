#!/usr/bin/python
#
# hdf_organzier.py
#
# VERSION: 0.1.3
#
# LAST EDIT: 2017-06-19
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# ------------
# description:
# ------------
# This script sets up the HDF5 (Hierarchical Data Formal) files for storing
# plant root images (a part of the PRIDA Project).
#
# ----------
# changelog:
# ----------
# 00. created [15.06.10]
# 01. added cv2, datetime, glob, and os.path modules [15.06.11]
# 02. created PRIDA_HDF class [15.06.11]
# 03. create_experiment & create_rep class functions [15.06.12]
# 04. removed matplotlib dependency [17.06.19]
#
# -----
# todo:
# -----
#
###############################################################################
## REQUIRED MODULES:
###############################################################################
import cv2
import datetime
import glob
import h5py
import numpy
import os.path

###############################################################################
## CLASSES
###############################################################################
class PRIDA_HDF:
    """
    Name:     PRIDA_HDF
    Features: This class handles the organization and storage of plant root
              images and their associated meta data into a readable/writeable
              HDF5 (Hierarchical Data Format) file
    Ref:      https://www.hdfgroup.org/HDF5/
    TODO:     [1] add "Select Existing Experiment" function
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, dirname, filename):
        """
        Name:     PRIDA_HDF.__init__
        Features: Creates an empty HDF5 file.
        Inputs:   - str, directory name (dirname)
                  - str, HDF5 file name (filename)
        Outputs:  None.
        """
        # Define class constants:
        self.TIMEFORMAT = "%Y-%m-%d"           # strftime format
        self.IMG_TYPES = ('.jpg', '.JPG')      # supported image formats
        #
        # Save directory and filename as class variables:
        self.dir = dirname
        self.filename = filename
        my_hdf = dirname + filename
        #
        # Create empty HDF file:
        if os.path.isfile(my_hdf):
            print "Warning! Overwriting file", filename
        #
        try:
            self.hdfile = h5py.File(my_hdf, 'w')
        except:
            print "Could not create file:", my_hdf
            self.isopen = False
        else:
            self.isopen = True
        finally:
            self.cur_exp = ""  # \  set by default so processing can begin w/o
            self.cur_rep = ""  # /  explicitly creating an exp or rep
    #
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def create_experiment(self, etitle, egens, espec, ecult):
        """
        Name:     PRIDA_HDF.create_experiment
        Features: Creates a new experiment and associated meta data
        Inputs:   - str, experiment title (etitle)
                  - str, experiment genus name (egens)
                  - str, experiment species name (espec)
                  - str, experiment cultivar (ecult)
        Outputs:  None.
        """
        # Find the number of existing experiments:
        num_exp = len(self.hdfile.keys())
        #
        # Create the next experiment group by incrementing by one:
        ename = '/exp%02d' % (num_exp + 1)
        self.hdfile.create_group(ename)
        self.cur_exp = ename
        #
        # Set experiment meta data:
        if self.isopen:
            self.hdfile[ename].attrs.create(name="experiment", data=etitle)
            self.hdfile[ename].attrs.create(name="genus", data=egens)
            self.hdfile[ename].attrs.create(name="species", data=espec)
            self.hdfile[ename].attrs.create(name="cultivar", data=ecult)
    #
    def create_rep(self, rday, rnum, rtot, rnote=''):
        """
        Name:     PRIDA_HDF.create_rep
        Features: Creates a new repository group for storing images
        Inputs:   - datetime.date, day of experiment rep (rday)
                  - int, number for repository (rnum)
                  - int, total number of images for rep (rtot)
                  - str, [optional] special note regarding rep (rnote)
        """
        # Find the number of existing reps:
        num_rep = len(self.hdfile[self.cur_exp].keys())
        #
        # Create the next rep group by incrementing by one:
        rname = '%s/rep%02d' % (self.cur_exp, num_rep + 1)
        self.hdfile.create_group(rname)
        self.cur_rep = rname
        #
        # Set experiment's rep meta data:
        if self.isopen:
            self.hdfile[rname].attrs.create(
                name="date",
                data=rday.strftime(self.TIMEFORMAT)
            )
            self.hdfile[rname].attrs.create(name="number", data=rnum)
            self.hdfile[rname].attrs.create(name="total_images", data=rtot)
            self.hdfile[rname].attrs.create(name="notes", data=rnote)
    #
    def img_to_hdf(self, img, dname):
        """
        Name:     PRIDA_HDF.img_to_hdf
        Features: Saves image dataset to current HDF repository; currently set
                  with gzip compression.
        Inputs:   - numpy.ndarray, openCV image data array (img)
                  - str, image/dataset name (dname)
        """
        ds_name = self.cur_rep + '/' + dname
        if (ds_name) in self.hdfile:
            print "WARNING: Overwriting dataset", self.cur_rep, dname
        #
        self.hdfile.create_dataset(name=ds_name,
                                   data=img,
                                   compression='gzip')
        #
        # Save image dimensions (currently pixel y, pixel x, color bands):
        # NOTE: openCV reads images in BGR not RGB
        self.hdfile[ds_name].dims[0].label = 'pixels in y'
        self.hdfile[ds_name].dims[1].label = 'pixels in x'
        self.hdfile[ds_name].dims[2].label = 'BGR color bands'
    #
    def save(self):
        """
        Name:     PRIDA_HDF.save
        Features: Closes the HDF file (saves to disk)
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.hdfile.close()
            except:
                pass # already closed
            finally:
                self.isopen = False
    #
    def save_images(self, idir):
        """
        Name:     PRIDA_HDF.save_images
        Features: Save images found in the given directory to HDF file
        Input:    str, path to image directory (idir)
        Output:   None.
        Depends:  - img_to_hdf()
                  - glob
                  - numpy
        """
        # Read supported image files from directory:
        my_files = []
        for files in self.IMG_TYPES:
            my_files.extend(glob.glob(idir + "*" + files))
        #
        if my_files:
            num_files = len(my_files)
            if num_files != self.hdfile[self.cur_rep].attrs["total_images"]:
                print num_files
                print self.hdfile[self.cur_rep].attrs["total_images"]
                print "ERROR! Unexpected number of images found in", idir
            else:
                my_files = numpy.sort(my_files)
                for my_file in my_files:
                    # NOTE: image name has the original file extension
   	            iname = os.path.basename(my_file)
                    my_img = cv2.imread(my_file)
                    self.img_to_hdf(my_img, iname)
    #
    def set_root_metadata(self, uname, uaddr):
        """
        Name:     PRIDA_HDF.set_root_metadata
        Features: Sets the HDF root meta data
        Inputs:   - str, user name (uname)
                  - str, user address, e.g. NetID (uaddr)
        Outputs:  None.
        """
        if self.isopen:
            self.hdfile.attrs.create(name="user", data=uname)
            self.hdfile.attrs.create(name="mail", data=uaddr)
    #

# ###############################################################################
# ## MAIN:
# ###############################################################################
# # Configure directory, filename and meta data:
# my_dir = "/Users/twdavis/Desktop/temp/"
# my_file = "exp06.hdf"
# my_root_meta = {
    # "User" : 'Tyler W. Davis',
    # "Mail" : 'twd34'
# }
# my_exp_meta = {
    # "Experiment" : 'My Test Experiment 6',
    # "Genus" : 'Carcharodon',
    # "Species" : 'carcharias',
    # "Cultivar" : 'Taxus baccata variegata',
# }
# my_rep_meta = {
    # "Date" : datetime.date.today(),
    # "Number" : 1,
    # "Images" : 40,
    # "Note" : "Low pH"
# }
# img_dir = ('/Users/twdavis/Projects/rsa/root_reader/sequences/'
           # '150324_day9_Azu1/orig_jpg/')

# # PROCESS EXAMPLE:
# ## 1. Initialize empty class
# my_class = PRIDA_HDF(my_dir, my_file)

# ## 2. Set the root group meta data:
# my_class.set_root_metadata(my_root_meta['User'],
                           # my_root_meta['Mail'])

# ## 3. Create an experiment (subgroup of root):
# my_class.create_experiment(my_exp_meta['Experiment'],
                           # my_exp_meta['Genus'],
                           # my_exp_meta['Species'],
                           # my_exp_meta['Cultivar'])

# ## 4. Create a rep for your experiment (sub-subgroup for storing images):
# my_class.create_rep(my_rep_meta['Date'],
                    # my_rep_meta['Number'],
                    # my_rep_meta['Images'],
                    # my_rep_meta['Note'])

# ## 5. Save images to current repository:
# my_class.save_images(img_dir)

# ## 6. Save HDF file (flushes data from buffer to disk):
# my_class.save()



# # ~~~~~~~~~~~~~~~~~ TESTING GROUNDS ~~~~~~~~~~~~~~~~~~~ #
# my_dir = "/Users/twdavis/Desktop/temp/"
# my_hdf = my_dir + "example.hdf5"
# my_tif = my_dir + "root_gel_ex02.tif"
# my_jpg = my_dir + "root_mesh_ex01.jpg"


# # Open images w/ OpenCV as unsigned 8-bit arrays
# img_tif = cv2.imread(my_tif)
# img_jpg = cv2.imread(my_jpg)


# # Creates a blank HDF5 file:
# f = h5py.File(my_hdf, 'w')
# f.name     # '/' is the "root group" (unicode string)
# f.filename # name of the file on disk (unicode string)
# f.mode     # 'r+' read/write (NOTE: 'w' becomes 'r+')


# # Set root group attributes:
# f.attrs.create(name="Name", data="Tyler W. Davis")
# f.attrs.create(name="NetID", data="twd34")


# # Create some new groups:
# grp = f.create_group('subgroup')
# grp.name   # '/subgroup' (unicode string)
# grp.file   # the file instance in which the group resides
# grp.parent # group instance containing this group (e.g., "/" root)
# f.keys()   # list of subgroups/ datasets
# #            NOTE: Python 3 returns objects instead of lists


# # Print the names of all subgroups (i.e., everything under "root")
# for name in f:
    # print name


# # Create a dataset under subgroup:
# dset = grp.create_dataset("mydataset", (100,), dtype='f')
# dset.name   # mydataset
# dset.shape  # (100,)
# dset.dtype  # float32
# dset.size   # number of elements (i.e., 100)
# dset.file   # the file instance in which the data resides
# dset.parent # group instance containing this data
# "mydataset" in grp            # True
# grp.__contains__("mydataset") # True
# "subgroup/mydataset" in f     # True


# # Assign attributes
# # NOTE: these can be assigned to either groups or datasets
# #       * they can be created from any scalar or numpy array
# #       * each should be small (i.e., < 64k)
# #       * there is no slicing (entire attribute must be read)
# dset.attrs['temperature'] = 23.5
# dset.attrs.create(name="elevation", data=101.0, shape=(), dtype='f')
# dset.attrs.__contains__("temperature") # True
# dset.attrs.keys()
# dset.attrs.values()


# # Modify an attribute value (preserves shape and type)
# dset.attrs.modify("elevation", 200.25)


# # Initialize dataset to an existing numpy array:
# # NOTE: in "create_dataset," you can set compression flag, i.e.
# #       f.create_dataset('name', data=my_data, compression='gzip'),
# #       which results in compressed data when written to disk that is
# #       decompressed when read from disk
# my_array = numpy.array([i for i in xrange(16)])
# dset2 = f.create_dataset("myarray", data=my_array)
# dset2.resize((4,4)) # typical numpy resize behaviour
# dset2.name
# dset2.shape
# dset2.dtype
# dset2[:,0]          # numpy.ndarray of all rows, first column
# dset2[my_array > 5] # apply numpy boolean 'mask' arrays
# dset2.len()         # typical length behaviour


# # Set data dimensions
# f['myarray'].dims[0].label = 'y'
# f['myarray'].dims[1].label = 'x'


# # Define region reference (e.g., image ROI) & save as data attribute:
# regref = dset2.regionref[1:3, 1:3]
# dset2.attrs['ROI'] = regref
# dset2[regref]                # returns the region you defined above
# dset2[dset2.attrs['ROI']]    # alt. method


# # Flush data buffers to disk:
# f.flush()
