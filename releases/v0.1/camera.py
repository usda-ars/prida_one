#!/usr/bin/python
#
# camera.py
#
# VERSION: 0.1.3
#
# LAST EDIT: 2015-06-25
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# $log_start_tag$
# d5735cb: twdavis - 2015-06-25 14:43:06
#     working copy of PRIDA v0.1
# 6a90843: Jay - 2015-06-18 13:58:07
#     bugfixes
# 48d047e: Jay - 2015-06-16 14:10:50
#     v0.1.1
# db5bcc3: Hoi Cheng - 2015-06-16 01:46:15
#     v0.0.4
# 50cf878: Jay - 2015-06-15 13:48:22
#     v0.0.4
# 84cf4f8: Jay - 2015-06-12 14:18:23
#     conflict fix
# f4ae6b2: Jay - 2015-06-12 14:02:08
#     0.0.1
# 0ef64ad: Jay - 2015-06-11 11:33:26
#     fixing loops
# ad84bab: Jay - 2015-05-30 05:32:29
#     GUI setup
# 629d58c: Jay - 2015-05-30 02:08:46
#     Setting up GUI
# 7866821: Jay - 2015-06-01 17:33:08
#     setting up
# 993e880: Jay - 2015-06-01 17:28:21
#     setting up
# $log_end_tag$
#
#import logging
import os
#import subprocess
import sys

import gphoto2 as gp

class Camera():

    def __init__(self):
        self.photo_num = 1
        self.path = '/tmp'

    def connect(self):
        try:
            self.context = gp.gp_context_new()
            self.camera = gp.check_result(gp.gp_camera_new())
        except:
            print("Could not connect to camera")
        #gp.check_result(gp.gp_camera_init(camera, context))

    def setPath(self, p):
        self.path = p

    def resetCount(self):
        self.photo_num = 1

    def capture(self):
        file_path = gp.check_result(gp.gp_camera_capture(
            self.camera, gp.GP_CAPTURE_IMAGE, self.context))
        #print('Camera file path: {0}/{1}'.format(file_path.folder, file_path.name))
        filename = os.path.join(self.path, str(self.photo_num)+".jpg")
        print('Copying image to', filename)
        camera_file = gp.check_result(gp.gp_camera_file_get(
            self.camera, file_path.folder, file_path.name,
            gp.GP_FILE_TYPE_NORMAL, self.context))
        gp.check_result(gp.gp_file_save(camera_file, filename))
        #subprocess.call(['xdg-open', target])
        gp.check_result(gp.gp_camera_exit(self.camera, self.context))
        self.photo_num+=1
        return self.photo_num

