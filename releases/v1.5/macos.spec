# -*- mode: python -*-

block_cipher = None

my_path = "./"
img_path = my_path + "prida/images/"
a = Analysis([my_path + 'main.py'],
             pathex=['.'],
             binaries=[],
             datas=[],
             hiddenimports=['PyQt5',
                            'h5py.defs',
                            'h5py.utils',
                            'h5py.h5ac',
                            'h5py._proxy',
                            'scipy.linalg',
                            'scipy.linalg.cython_blas',
                            'scipy.linalg.cython_lapack',
                            'scipy.integrate'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=None,
             win_private_assemblies=None,
             cipher=block_cipher)
a.datas += [('images/greeter.jpg', img_path + 'greeter.jpg', 'DATA'),
            ('images/loading.gif', img_path + 'loading.gif', 'DATA'),
            ('images/icon.png', img_path + 'icon.png', 'DATA'),
            ('images/calib_dist_center.jpg', img_path + 'calib_dist_center.jpg', 'DATA'),
            ('images/calib_dist_wall.jpg', img_path + 'calib_dist_wall.jpg', 'DATA'),
            ('images/calib_tank_depth.jpg', img_path + 'calib_tank_depth.jpg', 'DATA'),
            ('images/calib_tank_wall.jpg', img_path + 'calib_tank_wall.jpg', 'DATA')]
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='Prida',
          debug=False,
          strip=None,
          upx=True,
          console=True)
app = BUNDLE(exe,
             name='Prida.app',
             icon='icon.icns',         # or None if unavailable
             bundle_identifier=None,
             info_plist={
                 'CFBundleDevelopmentRegion': 'en_US',
                 'CFBundleShortVersionString': '1.5',
                 'CFBundleVersion': '1.5.0',
                 'CFBundleName': 'Prida',
                 'CFBundleDisplayName': 'Prida',
                 'CFBundleSignature': '????',
                 'NSHighResolutionCapable': 'True',
                 'NSHumanReadableCopyright': 'This software is freely available to the public for use.'
                 }
             )
