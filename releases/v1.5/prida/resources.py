#!/usr/bin/python
#
# resources.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################

###############################################################################
# REQUIRED MODULES:
###############################################################################
import os
import sys

from . import __version__


###############################################################################
# FUNCTIONS:
###############################################################################
def create_config_file(prida_dir, file_name):
    """
    Name:     create_config_file
    Inputs:   str, config file name with path (file_path)
    Outputs:  None.
    Features: Create a default configuration file at given location.
    """
    config_txt = (
        "PRIDA.GLOBAL_CONF           PRIDA_VERSION         '%s'\n"
        "PRIDA.GLOBAL_CONF           LOG_DIR               '%s'\n"
        "PRIDA.GLOBAL_CONF           LOG_FILENAME          'prida.log'\n"
        "PRIDA.GLOBAL_CONF           DICTIONARY_DIR        '%s'\n"
        "PRIDA.GLOBAL_CONF           DICTIONARY_FILENAME   'dictionary.txt'\n"
        "PRIDA.GLOBAL_CONF           LOG_LEVEL             'info'\n"
        "PRIDA.HARDWARE.CAMERA       IMAGE_DIR             '%s'\n"
        "PRIDA.HARDWARE.IMAGING      MODE                  'Explorer'\n"
        "PRIDA.HARDWARE.IMAGING      DRIVER                ''\n"
        "PRIDA.HARDWARE.IMAGING      HAS_LED               False\n"
        "PRIDA.HARDWARE.IMAGING      HAS_PIEZO             False\n"
        "PRIDA.HARDWARE.IMAGING      GEAR_RATIO            10.0\n"
        "PRIDA.HARDWARE.IMAGING      SETTLING_TIME         2.0\n"
        "PRIDA.HARDWARE.CONTROLLER   SPEED                 12.0\n"
        "PRIDA.HARDWARE.CONTROLLER   DEGREES_PER_STEP      1.8\n"
        "PRIDA.HARDWARE.CONTROLLER   PORT_NUMBER           1\n"
        "PRIDA.HARDWARE.CONTROLLER   STEP_TYPE             'double'\n"
        "PRIDA.HDF_ORGANIZER         COMPRESS_LV           6\n"
        % (__version__,
           os.path.join(prida_dir, "logs"),
           prida_dir,
           os.path.join(prida_dir, 'photos'))
    )
    file_path = os.path.join(prida_dir, file_name)
    try:
        with open(file_path, 'w') as my_file:
            my_file.write(config_txt)
    except:
        raise IOError("Can not write to config file.")


def create_dictionary(prida_dir, file_name):
    """
    Name:     create_dictionary
    Inputs:   - str, prida directory (prida_dir)
              - str, path to and name of output text file (file_path)
    Outputs:  None.
    Features: Creates a default dictionary file for the genus species
              suggestion text box based on the USDA PLANTS database
    Depends:  write_default_dictionary
    Ref:      USDA, NRCS. 2015. The PLANTS Database (http://plants.usda.gov).
              National Plant Data Team, Greensboro, NC 27401-4901 USA
    """
    # Default dictionary file:
    diction_txt = (
        "['BRNA',]:Canola (Brassica napus)\n"
        "['CUSA4',]:Cucumber (Cucumis sativus)\n"
        "['GLMA4',]:Soybean (Glycine max)\n"
        "['ORSA',]:Rice (Oryza sativa)\n"
        "['SOLY2',]:Tomato (Solanum lycopersicum)\n"
        "['SOBI2',]:Sorghum (Sorghum bicolor)\n"
        "['ZEMA',]:Corn (Zea mays)"
    )
    file_path = os.path.join(prida_dir, file_name)
    try:
        with open(file_path, 'w') as my_file:
            my_file.write(diction_txt)
    except:
        raise IOError("Can not write to dictionary file.")


def create_local_prida(prida_dir,
                       conf_file="prida.config",
                       dict_file="dictionary.txt"):
    """
    Name:     create_local_prida
    Inputs:   - str, prida directory (prida_dir)
              - [optional] str, configuration file name (conf_file)
              - [optional] str, dictionary file name (dict_file)
    Outputs:  int, return code
              > 1 ...... create directory failed
              > -1 ..... create configuration file/dictionary failed
              > 9999 ... directory already exists
    Features: Checks for prexistance of local Prida directory, creates one if
              one does not already exists, and returns a value representing
              the result.
    """
    if not os.path.isdir(prida_dir):
        try:
            os.mkdir(prida_dir)
            os.mkdir(os.path.join(prida_dir, "logs"))
            os.mkdir(os.path.join(prida_dir, "photos"))
        except:
            # Failed to create Prida directory
            return 1
        else:
            try:
                create_config_file(prida_dir, conf_file)
                create_dictionary(prida_dir, dict_file)
            except:
                # Failed to create configuration/dictionary files
                return -1
            else:
                return 0
    else:
        # Directory already exists!
        return 9999


def resource_path(relative_path):
    """
    Name:     resource_path
    Inputs:   str, resource file with path (relative_path)
    Outputs:  str, resource file with a potentially modified path
    Features: Returns absolute path for a given resource, works for dev and
              for PyInstaller.
    """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except AttributeError:
        base_path = os.path.join(sys.path[0], 'prida')
    return os.path.join(base_path, relative_path)
