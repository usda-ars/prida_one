#!/usr/bin/python
#
# stream_to_logger.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging


###############################################################################
# CLASS DEFINITION:
###############################################################################
class StreamToLogger(object):
    """
    Name:     StreamToLogger
    Features: Logs stdout and stderr to log file
    Ref:      Based off of the Stream to Logger class written by Ferry
              Boender for logging stdout and stderr to a log file using the
              sys and logging python modules.
    History:  Version 1.5.0
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        # self.line_buf = ''

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def write(self, buf):
        """
        Name:    StreamToLogger.write
        Feature: Writes the buffer to a log file. Used to make logging emulate
                 a file-like object.
        Inputs:  Str, buffer to write to the log (buf)
        Outputs: None
        """
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(self):
        """
        Name:    StreamToLogger.flush
        Feature: Dummy function to have logging emulate a file like object.
                 Actual flushing of the buffer handled within logging itself.
        Inputs:  None
        Outputs: None
        """
        pass
