#!/usr/bin/python
#
# hdf_organizer.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software is freely available to the public for use.      #
# The Department of Agriculture (USDA) and the U.S. Government have not       #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     USDA-Agricultural Research Service                                      #
#     Robert W. Holley Center for Agriculture and Health                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# ------------
# description:
# ------------
# This script sets up the HDF5 (Hierarchical Data Formal) files for storing
# plant root images and analysis (a part of the PRIDA Project).
#
# The general workflow follows this pattern:
#   1. Create a class instance:
#      ``````````````````````
#      my_class = PridaHDF()
#      ,,,,,,,,,,,,,,,,,,,,,,
#
#   2. Create new / open existing an HDF5 file:
#      a. Create new file:
#         ``````````````````````````````````````````````
#         my_class.new_file(str directory, str filename)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Open existing file:
#         ````````````````````````````````
#         my_class.open_file(str filename)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   3. List PIDs:
#      a. abstracted:
#         ````````````````````
#         my_class.list_pids()
#         ,,,,,,,,,,,,,,,,,,,,
#
#      b. explicit:
#         ``````````````````````````
#         my_class.list_objects('/')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   4. User's details:
#      a. Save HDF5 file details:
#         ````````````````````````````
#         my_class.set_root_user(str)
#         my_class.set_root_addr(str)
#         my_class.set_root_about(str)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. Check details:
#         `````````````````````````
#         my_class.get_root_user()
#         my_class.get_root_addr()
#         my_class.get_root_about()
#         ,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   5. Create a new session:
#      ````````````````````````````````````````````````````````````````````
#      session_path = my_class.create_session(str pid, dict pid, dict sess)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      where the PID meta data dictionary looks like this:
#      ````````````````````````````````````````````````````````````````````
#      dict = {
#          "gen_sp"            : str, Genus species
#          "line"              : str, cultivar/line
#          "media"             : str, growing media (e.g., hydroponic, gel)
#          "germdate"          : str, germination date (YYYY-MM-DD)
#          "transdate"         : str, transplant date (YYYY-MM-DD)
#          "rep_num"           : int, replication number
#          "treatment"         : str, treatment type
#          "tubsize"           : str, size of tub used for growing plant
#          "tubid"             : str, tub indentifier
#          "nutrient"          : str, nutrient solution info
#          "growth_temp_day"   : float, daytime growing temp., deg. C
#          "growth_temp_night" : float nighttime growing temp., deg. C
#          "growth_light"      : str, lighting conditions
#          "water_sched"       : str, watering schedule
#          "notes"             : str, additional notes
#      }
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#
#      and where the session meta data dictionary looks like this:
#      ```````````````````````````````````````````````````````````````
#      dict = {
#          "user"         : str, session user's name
#          "addr"         : str, session user's email address
#          "date"         : str, session date (YYYY-MM-DD)
#          "number"       : int, session number
#          "img_taken"    : int, total number of images taken
#          "age_num"      : float, plant age
#          "age_unit"     : str, unit of plant age (e.g., days, weeks)
#          "cam_shutter"  : str, camera shutter speed
#          "cam_aperture" : str, camera aperature setting
#          "cam_exposure" : str, camera exposure time
#          "notes"        : str, additional notes
#      }
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   6. Set existing PID attributes
#      NOTE: dictionary keys that already exist will have their values
#            re-written and keys that do not exist will be added to the
#            dictionary!
#      `````````````````````````````````````
#      my_class.set_pid_attrs(str pid, dict)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   7. Set existing session attributes
#      NOTE: dictionary keys that already exist will have their values
#            re-written and keys that do not exist will be added to the
#            dictionary!
#      ``````````````````````````````````````````````````
#      my_class.set_session_attrs(str session_path, dict)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#   8. Retrieve PID attrs:
#      ````````````````````````
#      my_class.get_pid_attrs()
#      ,,,,,,,,,,,,,,,,,,,,,,,,
#
#   9. List PID sessions:
#      a. sorted:
#         `````````````````````````````````
#         my_class.list_sessions(str 'PID')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. unsorted:
#         `````````````````````````````````
#         my_class.list_objects(str 'PID')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#  10. Retrieve session attrs:
#      ````````````````````````````````````````````
#      my_class.get_object_attrs(str session_path)
#      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#  11. Save/list/extract datasets:
#      a. Save image to session:
#         ```````````````````````````````````````````
#         my_class.save_image(str session, str image)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      b. List images in a session:
#         `````````````````````````````````````````
#         my_class.list_objects(str '/PID/Session')
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      c. Extract images to directory:
#         ````````````````````````````````````````````````````````````````
#         my_class.extract_datasets(str session, str output, str extension)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#      d. Extract PID and session attributes to CSV file:
#         ````````````````````````````````````````````
#         my_class.extract_attrs(str output_location)
#         ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
#
#  12. Save file (flush data to disk):
#      ```````````````
#      my_class.save()
#      ,,,,,,,,,,,,,,,
#
#  13. Close file (flush data to disk and close file handle):
#      ````````````````
#      my_class.close()
#      ,,,,,,,,,,,,,,,,
#
#
# -----
# todo:
# -----
# 00. extract dataset level 0 should skip parent directory tree
#
# To display:
#   import PIL
#   my_array = my_class.get_dataset(ds_path)
#   my_img = PIL.Image.fromarray(my_array)
#   my_scaled_img = my_img.resize(int(my_img.size[0]/4),
#                                 int(my_img.size[1]/4))
#   my_scaled_img.show()
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import errno
import glob
import logging
import logging.handlers
import os
import re
import time

import h5py
import numpy

from .imutil import imopen
from .imutil import is_binary
from .imutil import is_grayscale
from .imutil import is_rgb
from .imutil import rel_crop
from .imutil import rgb_to_gray
from .imutil import scale_image
from .ui.pilutil import imsave
from .utilities import img_types
from .utilities import read_exif_tags


###############################################################################
# FUNCTIONS
###############################################################################
def exit_organizer(my_organizer):
    """
    Name:    exit_organizer
    Feature: Graceful exiting of PridaHDF
    Inputs:  object, PridaHDF (my_organizer)
    Outputs: None
    """
    logging.info('exiting PridaHDF')
    my_organizer.close()


###############################################################################
# CLASSES
###############################################################################
class PridaHDF(object):
    """
    Name:     PridaHDF
    Features: This class handles the organization and storage of plant root
              images and their associated meta data into a readable/writeable
              HDF5 (Hierarchical Data Format) file
    History:  VERSION 1.5.0
              - removed scipy.misc dependency [16.07.25]
              - added file path as compress function argument [16.08.03]
              - added rawpy package for reading NEF images [16.10.26]
              - created imread convenience function [16.10.26]
              - added value errors to compress level setter [16.11.30]
              - saved directory name during file open [17.01.05]
              - added options to extract datasets [17.01.05]
              - created write to file function [17.01.05]
              - NoneType handling in set attr function [17.01.10]
              - added image group attributes to save image [17.01.10]
              - updated save image function [17.01.11]
                * photo tags saved to group
                * photo attributes saved to dataset
              - created output file naming functions [17.02.24]
              - added orientation correction to dataset extraction [17.02.24]
              - fixed colon in dataset time [17.02.24]
              - new imutil import [17.03.13]
              - reduced color write to file options [17.03.13]
              - updated default about return [17.03.16]
              - created image array mode and shape convenience funcs [17.03.17]
              - new file version property [17.03.17]
              - add file version to about dictionary [17.03.21]
              - moved imread to imutil as imopen [17.03.29]
              - added relative cropping to export [17.03.30]
              - get object attrs now decodes bytes [17.04.04]
              - created extract dataset (ds) function [17.04.05]
              - created get dataset list function [17.04.05]
              - updated export file naming (esp. for prida) [17.06.02]
    Refs:     https://www.hdfgroup.org/HDF5/
              http://docs.h5py.org/en/latest/index.html
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    UNDEFINED = ""

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parser=None, config_filepath=None):
        """
        Name:     PridaHDF.__init__
        Features: Initialize empty class.
        Inputs:   - [optional] configuration function handle (parser)
                  - [optional] str, configuration file (config_filepath)
        Outputs:  None.
        """
        # Define class constants:
        self.isopen = False             # HDF5 file handler
        self.pids = {}                  # PIDs are keys w/ session counter
        self.datasets = {}              # dict of all datasets (i.e., photos)
        self.thumbnails = {}            # dict of all thumbnails
        self.dir = ''                   # HDF file directory
        self.filename = ''              # HDF file name

        # Create a logger for PridaHDF:
        self.logger = logging.getLogger(__name__)

        # Define configurable attribute dictionary and default values:
        self.attr_dict = {'COMPRESS_LV': 'c_level'}
        self._clevel = 6

        # Parse configuration file:
        if parser is not None and config_filepath is not None:
            parser(self, __name__, config_filepath)
        self.logger.debug("_clevel = %d", self._clevel)
        self.logger.debug("c_level = %d", self.c_level)

        # Define exit register:
        atexit.register(exit_organizer, self)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Attribute Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def c_level(self):
        """
        The current configured compression level (1-9)
        """
        return self._clevel

    @c_level.setter
    def c_level(self, value):
        try:
            my_int = int(value)
        except:
            self.logger.error("compression level %s not an integer", value)
            raise ValueError("Failed to set HDF compression level.")
        else:
            if my_int > 9 or my_int < 1:
                self.logger.error("configuration level %d undefined", my_int)
                self.logger.error("using compression level %d", self._clevel)
                raise ValueError("Compression level is out of range.")
            else:
                self.logger.debug("level %d is between 1 and 9", my_int)
                self._clevel = my_int

    @property
    def version(self):
        """
        The HDF5 file version
        """
        return self.get_attr("version", "/")

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def basename(self):
        """
        Name:     PridaHDF.basename
        Inputs:   None.
        Outputs:  str, basename
        Features: Returns the HDF5 file's basename
        Note:     This seems redundant with the filename property
        """
        if self.isopen:
            self.logger.debug('file is open, returning basename')
            return os.path.basename(self.hdfile.filename)
        else:
            self.logger.warning('file is not open, returning empty')
            return ""

    def close(self):
        """
        Name:     PridaHDF.close
        Features: Saves changes to disk and closes HDF5 file handle, no further
                  reading or writing can be done.
        Inputs:   None.
        Outputs:  bool, return success
        """
        if self.isopen:
            try:
                self.logger.debug('closing HDF5 file')
                self.hdfile.close()
            except:
                self.logger.debug('HDF5 file already closed')
                pass  # already closed
            finally:
                self.logger.debug('resetting class variables')
                self.isopen = False
                self.pids = {}
                self.datasets = {}
                return True

    def compress(self, cfile):
        """
        Name:     PridaHDF.compress
        Inputs:   str, compressed file path (cfile)
        Outputs:  int, error code (see below)
                    * 0: success
                    * 1: failed to create file
                    * -1: failed during compression
                    * 9999: file already exists
        Features: Creates a new HDF file (chdfile) and re-creates the group
                  and data structure from hdfile using compression
        Depends:  run_compression
        """
        if self.isopen:
            if os.path.isfile(cfile):
                self.logger.error("compressed file already exists. Stopping.")
                return 9999
            else:
                try:
                    self.chdfile = h5py.File(cfile, 'w')
                except:
                    self.logger.error("failed to create compressed HDF5 file")
                    r_val = 1
                else:
                    self.logger.info("running compression")
                    try:
                        self.run_compression()
                    except:
                        self.logger.exception(
                            "encountered error during compression")
                        r_val = -1
                    else:
                        self.logger.info("compression finished")
                        r_val = 0
                    finally:
                        try:
                            self.logger.info("closing compressed HDF file")
                            self.chdfile.close()
                        except:
                            self.logger.info("HDF file already closed")
                        else:
                            self.logger.info("HDF file closed")
                        return r_val
        else:
            self.logger.warning("cannot compress; HDF file not open")
            return 1

    def run_compression(self, ipath='/'):
        """
        Name:     PridaHDF.run_compression
        Inputs:   [optional] str, object path (ipath)
        Outputs:  None.
        Features: Loops through hdfile and re-creates the group and dataset
                  structure in chdfile along with the appropriate metadata;
                  excludes any group/dataset with a True exclude attribute
        Depends:  - compress_dataset
                  - compress_group
        """
        if self.isopen:
            if ipath in self.hdfile:
                obj_members = self.list_objects(ipath)
                if obj_members == 0:
                    # Found dataset!
                    self.compress_dataset(ipath)
                else:
                    # Found group! Check for exclude attribute:
                    excl_attr = self.get_attr('exclude', ipath)
                    if excl_attr == "True":
                        self.logger.info("excluding %s", ipath)
                    else:
                        # Create group (if not done already) & apply metadata:
                        self.compress_group(ipath)

                        # Keep digging!
                        for member in obj_members:
                            if ipath[-1] != '/':
                                ipath += '/'
                            member_path = "%s%s" % (ipath, member)
                            self.run_compression(member_path)
        else:
            self.logger.warning("cannot run compression; HDF file not open")

    def compress_dataset(self, ds_path, to_chunk=True):
        """
        Name:     PridaHDF.compress_dataset
        Inputs:   - str, dataset path (ds_path)
                  - [optional] bool, chunking option (to_chunk)
        Outputs:  None.
        Features: Creates dataset in compressed HDF file with the appropriate
                  metadata
        Depends:  get_object_attrs
        """
        if self.isopen:
            if ds_path in self.hdfile:
                if ds_path not in self.chdfile:
                    try:
                        self.logger.debug("creating %s", ds_path)
                        img = self.get_dataset(ds_path)
                        ds_shape = img.shape
                        if to_chunk:
                            if len(ds_shape) == 3:
                                chunk_val = (120, 120, ds_shape[2])
                            elif len(ds_shape) == 2:
                                chunk_val = (120, 120)
                            else:
                                chunk_val = True

                        self.logger.debug("c_level = %d", self.c_level)
                        self.chdfile.create_dataset(
                            name=ds_path,
                            data=img,
                            compression="gzip",
                            compression_opts=self.c_level,
                            chunks=chunk_val)
                    except:
                        self.logger.error(
                            "failed to create dataset %s", ds_path)
                    else:
                        self.logger.debug("finished %s", ds_path)
                        self.logger.debug("retrieving dataset attributes")
                        ds_attrs = self.get_object_attrs(ds_path)
                        for key in ds_attrs.keys():
                            val = ds_attrs[key]
                            try:
                                if isinstance(val, str):
                                    val = val.encode("UTF-8")

                                self.chdfile[ds_path].attrs.create(name=key,
                                                                   data=val)
                            except:
                                self.logger.error(
                                    "failed to set attribute %s", key)
                            else:
                                self.logger.debug(
                                    "set attribute %s to value %s" % (key, val)
                                )

                        # Set dataset attribute 'compressed' to True
                        try:
                            self.chdfile[ds_path].attrs["compressed"] = "True"
                        except KeyError:
                            # Try to create new compressed attribute
                            try:
                                self.chdfile[ds_path].attrs.create(
                                    name="compressed", data="True")
                            except:
                                self.logger.error(
                                    "failed to set attribute compressed")
                            else:
                                self.logger.debug(
                                    "set attribute compressed to value True")
                        except:
                            self.logger.error(
                                "failed to set attribute compressed")
                        else:
                            self.logger.debug(
                                "set attribute compressed to value True")
                else:
                    self.logger.warning("dataset %s already created", ds_path)
            else:
                self.logger.warning(
                    "cannot create dataset %s; it does not exist", ds_path)
        else:
            self.logger.warning(
                "cannot create dataset %s; HDF file not open", ds_path)

    def compress_group(self, g_path):
        """
        Name:     PridaHDF.compress_group
        Inputs:   str, group path (g_path)
        Outputs:  None.
        Features: Creates group in compressed HDF file with the appropriate
                  metadata
        Depends:  get_object_attrs
        """
        if self.isopen:
            if g_path in self.hdfile:
                if g_path not in self.chdfile:
                    try:
                        self.logger.debug("creating %s", g_path)
                        self.chdfile.create_group(g_path)
                    except:
                        self.logger.error("failed to create group %s", g_path)
                    else:
                        self.logger.debug("finished %s", g_path)
                        self.logger.debug("retrieving group attributes")
                        g_attrs = self.get_object_attrs(g_path)
                        for key in g_attrs.keys():
                            val = g_attrs[key]
                            try:
                                if isinstance(val, str):
                                    val = val.encode("UTF-8")

                                self.chdfile[g_path].attrs.create(name=key,
                                                                  data=val)
                            except:
                                self.logger.error(
                                    "failed to set attribute %s", key)
                            else:
                                self.logger.debug(
                                    "set attribute %s to value %s" % (key, val)
                                )
                elif g_path == "/":
                    self.logger.debug("retrieving group attributes")
                    g_attrs = self.get_object_attrs(g_path)
                    for key in g_attrs.keys():
                        val = g_attrs[key]
                        try:
                            if isinstance(val, str):
                                val = val.encode("UTF-8")

                            self.chdfile[g_path].attrs.create(name=key,
                                                              data=val)
                        except:
                            self.logger.error(
                                "failed to set attribute %s", key)
                        else:
                            self.logger.debug(
                                "set attribute %s to value %s" % (key, val)
                            )
                else:
                    self.logger.warning("group %s already created", g_path)
            else:
                self.logger.warning(
                    "cannot create group %s; it does not exist", g_path)
        else:
            self.logger.warning(
                "cannot create group %s; HDF file not open", g_path)

    def create_session(self, pid, pid_attrs, session_attrs):
        """
        Name:     PridaHDF.create_session
        Features: Creates a new session under an existing (or newly created)
                  plant ID (PID) and sets the associated attributes to each
        Inputs:   - str, plant ID (pid)
                  - dict, PID attribute dictionary
                  - dict, session attribute dictionary
        Outputs:  str, new session path (session_path)
        Depends:  - get_session_number
                  - set_pid_attrs
                  - set_session_attrs
        """
        if self.isopen:
            self.logger.debug('getting session number')
            session_num = self.get_session_number(pid)
            session_name = "Session-%d" % (session_num)
            session_path = "/%s/%s" % (pid, session_name)
            self.logger.debug('created session %s', session_path)

            self.logger.debug('checking if PID exists...')
            old_pid = self.pid_exists(pid)

            # Create new session:
            try:
                self.logger.debug('creating new group')
                self.hdfile.create_group(session_path)
            except:
                self.logger.error('failed to create new group!')
                err_msg = "Error! Could not create session %s" % (session_path)
                raise IOError(err_msg)
            else:
                self.logger.debug('new group created')
                self.logger.debug('setting group attributes')
                self.set_session_attrs(session_path, session_attrs)
                #
                # Set PID attributes and update dictionary:
                if old_pid:
                    self.logger.debug('appending new group')
                    self.pids[pid].append(session_num)
                else:
                    self.logger.debug('appending new group and attributes')
                    self.pids[pid] = [session_num, ]
                    self.set_pid_attrs(pid, pid_attrs)
                return session_path
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError("Could not create new session. HDF5 file not open!")

    def delete_object(self, obj_parent, obj_name):
        """
        Name:     PridaHDF.delete_object
        Features: Deletes a given object from a given HDF5 parent object's
                  member list; it does NOT reduce file size!
        Inputs:   - str, object's parent w/ path (obj_parent)
                  - str, group/dataset object name (obj_name)
        Outputs:  None.
        Depends:  refresh_pids
        """
        if self.isopen:
            self.logger.debug('building path')
            if obj_parent[-1:] != '/':
                obj_parent += "/"
            obj_path = "%s%s" % (obj_parent, obj_name)

            if obj_path in self.hdfile:
                try:
                    self.logger.debug('deleting object')
                    del self.hdfile[obj_parent][obj_name]
                except:
                    self.logger.error('failed to delete object')
                    err_msg = ("PridaHDF.delete_dataset error. "
                               "Could not delte dataset: %s") % (obj_path)
                    raise IOError(err_msg)
                else:
                    self.logger.debug('refreshing PIDs')
                    self.refresh_pids()
            else:
                self.logger.warning('object does not exist')
                err_msg = "Error! Could not find object %s" % (obj_path)
                raise IOError(err_msg)
        else:
            self.logger.error('HDF5 file not open')
            raise IOError(
                "Error! Could not delete object. HDF5 file not open!")

    def ds_is_binary(self, ds_path):
        """
        Name:     PridaHDF.ds_is_binary
        Inputs:   str, HDF5 dataset path (ds_path)
        Outputs:  bool
        Features: Returns whether the dataset array is a binary image
        Depends:  - get_dataset
                  - is_binary
        """
        self.logger.debug("Returning dataset binary boolean")
        ds_array = self.get_dataset(ds_path)
        return is_binary(ds_array)

    def ds_is_grayscale(self, ds_path):
        """
        Name:     PridaHDF.ds_is_grayscale
        Inputs:   str, HDF5 dataset path (ds_path)
        Outputs:  bool
        Features: Returns whether the dataset array is a grayscale image
        Depends:  - get_dataset
                  - is_grayscale
        """
        self.logger.debug("Returning dataset grayscale boolean")
        ds_array = self.get_dataset(ds_path)
        return is_grayscale(ds_array)

    def ds_is_rgb(self, ds_path):
        """
        Name:     PridaHDF.ds_is_rgb
        Inputs:   str, HDF5 dataset path (ds_path)
        Outputs:  bool
        Features: Returns whether the dataset array is a grayscale image
        Depends:  - get_dataset
                  - is_rgb
        """
        self.logger.debug("Returning dataset RGB boolean")
        ds_array = self.get_dataset(ds_path)
        return is_rgb(ds_array)

    def ds_shape(self, ds_path):
        """
        Name:     PridaHDF.ds_shape
        Inputs:   str, HDF5 dataset path (ds_path)
        Outputs:  tuple
        Features: Returns the tuple with the dataset array shape
        Depends:  get_dataset
        """
        self.logger.debug("Returning dataset shape")
        ds_array = self.get_dataset(ds_path)
        return ds_array.shape

    def extract_attrs(self, opath):
        """
        Name:     PridaHDF.extract_attrs
        Features: Extracts PID and session attributes to two CSV files
                  a given directory
        Inputs:   str, full output path to directory where CSV files are to be
                  saved (opath)
        Outputs:  None.
        Depends:  - get_pid_attrs
                  - get_object_attrs
                  - list_objects
        """
        if self.isopen:
            # Create output files for saving attributes:
            self.logger.debug('naming output files')
            bname = os.path.splitext(os.path.basename(self.hdfile.filename))[0]
            pids_fname = "%s_%s.%s" % (bname, "pid-attrs", "csv")
            sess_fname = "%s_%s.%s" % (bname, "sess-attrs", "csv")
            pids_file = os.path.join(opath, pids_fname)
            sess_file = os.path.join(opath, sess_fname)

            if os.path.isfile(pids_file) or os.path.isfile(sess_file):
                self.logger.warning('output file exists')
                raise ValueError("Cannot save file. File already exists!")
            else:
                try:
                    self.logger.debug('opening output files')
                    pFile = open(pids_file, 'w')
                    sFile = open(sess_file, 'w')
                except IOError:
                    self.logger.error('could not write to file')
                    err_msg = ("PridaHDF.extract_attrs error. "
                               "Could not open PID or session attribute files "
                               "for writing")
                    raise IOError(err_msg)
                except:
                    self.logger.error('could not write to files')
                    raise IOError("PridaHDF.extract_attrs unknown error!")
                else:
                    # Accumulate PID and session attribute keys:
                    pid_attr_keys = []
                    sess_attr_keys = []
                    self.logger.debug('getting attributes')
                    for pid in self.pids:
                        pDict = self.get_pid_attrs(pid)
                        for k in pDict:
                            pid_attr_keys.append(k)
                        #
                        for session in self.list_objects(pid):
                            sPath = "/%s/%s" % (pid, session)
                            sDict = self.get_object_attrs(sPath)
                            for j in sDict:
                                sess_attr_keys.append(j)

                    self.logger.debug('sorting attribute keys')
                    pid_attr_keys = sorted(list(set(pid_attr_keys)))
                    sess_attr_keys = sorted(list(set(sess_attr_keys)))

                    # Build the headerline for PID attrs:
                    pid_headerline = 'PID,'
                    for k in pid_attr_keys:
                        pid_headerline += k
                        pid_headerline += ","
                    pid_headerline = pid_headerline.rstrip(",")
                    pid_headerline += "\n"
                    self.logger.debug('writing PID headers')
                    pFile.write(pid_headerline)

                    # Build the headerline for session attrs:
                    sess_headerline = 'PID,Session,'
                    for j in sess_attr_keys:
                        sess_headerline += j
                        sess_headerline += ','
                    sess_headerline = sess_headerline.rstrip(",")
                    sess_headerline += "\n"
                    self.logger.debug('writing session headers')
                    sFile.write(sess_headerline)

                    # Write each PID attribute line:
                    self.logger.debug('writing PIDs to file')
                    for pid in self.pids:
                        pDict = self.get_pid_attrs(pid)
                        pid_write_line = str(pid)
                        pid_write_line += ","
                        for k in pid_attr_keys:
                            if k in pDict:
                                v = str(pDict[k])
                            else:
                                v = ''
                            v += ','
                            pid_write_line += v
                        pid_write_line = pid_write_line.rstrip(',')
                        pid_write_line += '\n'
                        pFile.write(pid_write_line)

                    # Write each session attribute line:
                    self.logger.debug('writing sessions to file')
                    for pid in self.pids:
                        for session in self.list_objects(pid):
                            sess_write_line = str(pid)
                            sess_write_line += ","
                            sess_write_line += session
                            sess_write_line += ","
                            sPath = "/%s/%s" % (pid, session)
                            sDict = self.get_object_attrs(sPath)
                            for j in sess_attr_keys:
                                if j in sDict:
                                    u = str(sDict[j])
                                else:
                                    u = ''
                                u += ','
                                sess_write_line += u
                            sess_write_line = sess_write_line.rstrip(',')
                            sess_write_line += "\n"
                            sFile.write(sess_write_line)
                finally:
                    self.logger.debug('closing file handles')
                    pFile.close()
                    sFile.close()
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError(
                "Error! Could not get attributes. HDF5 file not open!")

    def extract_datasets(self, ipath, opath, opts=None, img_ext=img_types,
                         level=0):
        """
        Name:     PridaHDF.extract_datasets
        Inputs:   - str, HDF5 input object with associated path (ipath)
                  - str, absolute path to output directory (opath)
                  - [optional] dict, extraction options (opts)
                  - [optional] tuple, image file extensions (img_ext)
                  - [optional] int, number of levels deep (level)
        Outputs:  None.
        Features: Extracts image datasets from a given HDF5 file hierarchy to
                  a given directory.
        Depends:  - extract_datasets        - list_objects
                  - get_dataset             - write_to_file
                  - get_output_name
        """
        if self.isopen:
            if ipath in self.hdfile:
                self.logger.debug("gathering members for %s", ipath)
                obj_members = self.list_objects(ipath)
                if obj_members == 0:
                    # Found dataset!
                    member_ext = os.path.splitext(ipath)[1]
                    if member_ext in img_ext:
                        out_name = self.get_output_name(ipath, opts)
                        out_dir = os.path.dirname(ipath).lstrip("/")
                        out_path = os.path.join(opath, out_dir, out_name)
                        self.logger.debug(
                            "Saving %s at level %i" % (out_path, level))

                        # Set option for correcting image orientation:
                        ds_orient = self.get_ds_orient(ipath)
                        if isinstance(opts, dict):
                            opts['orient'] = ds_orient
                        else:
                            opts = {'orient': ds_orient}

                        dset = self.get_dataset(ipath)
                        try:
                            self.write_to_file(out_path, dset, opts)
                        except:
                            self.logger.error('save failed!')
                            err_msg = ("Could not extract dataset "
                                       "to %s") % (out_path)
                            raise IOError(err_msg)
                else:
                    # Keep digging!
                    level += 1
                    for member in obj_members:
                        if ipath[-1] != '/':
                            ipath += '/'
                        member_path = "%s%s" % (ipath, member)
                        self.extract_datasets(
                            member_path, opath, opts, img_ext, level)
            else:
                self.logger.error('%s does not exist', ipath)
                err_msg = ("PridaHDF.extract_datasets error! "
                           "Object '%s' does not exist.") % (ipath)
                raise IOError(err_msg)
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError("Error! Could not get datasets. HDF5 file not open!")

    def extract_ds(self, ds_path, opath, opts=None):
        """
        Name:     PridaHDF.extract_ds
        Inputs:   - str, HDF5 dataset path (ds_path)
                  - str, absolute path to output directory (opath)
                  - [optional] dict, extraction options (opts)
        Outputs:  None.
        Features: Extracts image datasets from a given HDF5 file hierarchy to
                  a given directory.
        Depends:  - get_dataset
                  - get_ds_orient
                  - get_output_name
                  - write_to_file
        """
        if self.isopen:
            if ds_path in self.hdfile:
                out_name = self.get_output_name(ds_path, opts)
                out_dir = os.path.dirname(ds_path).lstrip("/")
                out_path = os.path.join(opath, out_dir, out_name)

                # Set option for correcting image orientation:
                ds_orient = self.get_ds_orient(ds_path)
                if isinstance(opts, dict):
                    opts['orient'] = ds_orient
                else:
                    opts = {'orient': ds_orient}

                dset = self.get_dataset(ds_path)
                try:
                    self.write_to_file(out_path, dset, opts)
                except:
                    self.logger.error('save failed!')
                    err_msg = ("Could not extract dataset "
                               "to %s") % (out_path)
                    raise IOError(err_msg)
            else:
                self.logger.error('%s does not exist', ds_path)
                raise IOError("Dataset '%s' does not exist." % ds_path)
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError("Error! Could not get datasets. HDF5 file not open!")

    def find_datasets(self, ipath='/', img_ext=img_types):
        """
        Name:     PridaHDF.find_datasets
        Inputs:   - str, current group/dataset path (ipath)
                  - str, search dataset extension (img_ext)
        Outputs:  None.
        Features: Adds all data sets to global datasets list
        Depends:  - find_datasets (recursive)
                  - get_ds_session
        """
        if self.isopen:
            self.logger.debug("checking %s", ipath)
            if ipath in self.hdfile:
                obj_members = self.list_objects(ipath)
                if obj_members == 0:
                    # Found dataset!
                    member_ext = os.path.splitext(ipath)[1]
                    if member_ext in img_ext:
                        if ipath not in self.datasets:
                            self.logger.debug('found %s', ipath)
                            self.datasets[ipath] = self.get_ds_session(ipath)
                    else:
                        self.logger.debug("skipping %s", ipath)
                else:
                    # Keep digging!
                    for member in obj_members:
                        if ipath[-1] != '/':
                            ipath += '/'
                        member_path = "%s%s" % (ipath, member)
                        self.find_datasets(member_path, img_ext)
            else:
                self.logger.error('%s does not exist', ipath)
                err_msg = "Dataset '%s' does not exist." % (ipath)
                raise IOError(err_msg)
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError(
                "Error! Could not find datasets. HDF5 file not open!")

    def find_thumbnails(self, ipath='/'):
        """
        Name:     PridaHDF.find_thumbnails
        Inputs:   str, current group/dataset path (ipath)
        Outputs:  None.
        Features: Adds all data sets to global datasets list
        Depends:  - find_thumbnails (recursive)
                  - get_ds_session
        """
        if self.isopen:
            self.logger.debug("checking %s", ipath)
            if ipath in self.hdfile:
                obj_members = self.list_objects(ipath)
                if obj_members == 0:
                    # Found dataset!
                    member_ext = os.path.splitext(ipath)[1]
                    if member_ext == ".thumb":
                        if ipath not in self.thumbnails:
                            self.logger.debug('found %s', ipath)
                            self.thumbnails[ipath] = self.get_ds_session(
                                ipath)
                    else:
                        self.logger.debug("skipping %s", ipath)
                else:
                    # Keep digging!
                    for member in obj_members:
                        if ipath[-1] != '/':
                            ipath += '/'
                        member_path = "%s%s" % (ipath, member)
                        self.find_thumbnails(member_path)
            else:
                self.logger.error('%s does not exist', ipath)
                err_msg = (
                    "PridaHDF.find_thumbnails error! '%s' does not exist."
                    ) % (ipath)
                raise IOError(err_msg)
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError(
                "Error! Could not find datasets. HDF5 file not open!")

    def get_about(self):
        """
        Name:     PridaHDF.get_about
        Features: Returns dictionary of about strings
        Inputs:   None.
        Outputs:  dict, about strings (my_overview)
        Depends:  find_datasets
        """
        if self.isopen:
            self.logger.debug("refreshing datasets")
            self.datasets = {}
            self.find_datasets()

            self.logger.debug("acquiring about info")
            my_author = "%s" % (self.get_root_user())
            my_contact = "%s" % (self.get_root_addr())
            my_summary = "%s" % (self.get_root_about())
            my_plants = "%d plants" % (len(list(self.pids.keys())))
            my_sessions = "%d sessions" % (numpy.array(
                [len(self.pids[i]) for i in self.pids]).sum())
            my_photos = "%d images" % (len(list(self.datasets.keys())))
            my_version = self.version
            if my_version == self.UNDEFINED:
                my_version = "None"

            self.logger.debug("creating about dictionary")
            my_overview = {"Author": my_author,
                           "Contact": my_contact,
                           "Summary": my_summary,
                           "Plants": my_plants,
                           "Sessions": my_sessions,
                           "Photos": my_photos,
                           "Version": my_version}
        else:
            self.logger.warning("HDF5 file not open!")
            self.logger.warning("returning undefined values")
            my_overview = {"Author": "None",
                           "Contact": "None",
                           "Summary": "No HDF5 project file opened.",
                           "Plants": "None",
                           "Sessions": self.UNDEFINED,
                           "Photos": self.UNDEFINED,
                           "Version": "None"}
        return my_overview

    def get_attr(self, attr, group_path):
        """
        Name:     PridaHDF.get_attr
        Features: Returns the given attribute for the given path
        Inputs:   - str, attribute name (attr)
                  - str, group path (group_path)
        Outputs:  str, attribute value (val)
        """
        if self.isopen:
            if group_path in self.hdfile:
                if attr in self.hdfile[group_path].attrs.keys():
                    try:
                        self.logger.debug('retrieving %s', attr)
                        tmp = self.hdfile[group_path].attrs[attr]

                        if isinstance(tmp, str):
                            val = tmp
                        elif isinstance(tmp, numpy.ndarray):
                            val = tmp
                        else:
                            try:
                                self.logger.debug('decoding string %s', tmp)
                                val = tmp.decode('UTF-8')
                            except:
                                self.logger.debug('decode failed')
                                val = tmp
                    except KeyError:
                        self.logger.warning(
                            "'%s' has no attribute '%s'" % (group_path, attr))
                        val = self.UNDEFINED
                    except:
                        self.logger.warning(
                            ("attribute '%s' could not be retrieved "
                             "from path '%s'!") % (attr, group_path))
                        val = self.UNDEFINED
                else:
                    # NOTE: this litters the log file with the "exclude" attr
                    #       during compression
                    self.logger.warning(
                        "'%s' has no attribute '%s'" % (group_path, attr))
                    val = self.UNDEFINED
            else:
                self.logger.warning('path not defined!')
                val = self.UNDEFINED
        else:
            self.logger.warning('HDF5 file not open!')
            self.logger.warning('returning undefined value')
            val = self.UNDEFINED

        return val

    def get_dataset_list(self, ipath):
        """
        Name:     PridaHDF.get_dataset_list
        Inputs:   str, HDF5 input path (ipath)
        Outputs:  list, dataset paths
        Features: Returns a list of datasets at a given HDF5 path
        Depends:  find_datasets
        """
        ds_list = []
        if ipath in self.hdfile:
            self.datasets = {}
            self.find_datasets(ipath)
            for ds in sorted(list(self.datasets.keys())):
                ds_list.append(ds)
            self.find_datasets()
        return ds_list

    def get_ds_date_time(self, ds_path):
        """
        Name:     PridaHDF.get_ds_date_time
        Inputs:   str, dataset path in HDF5 file (ds_path)
        Outputs:  tuple, date (str) and time (str)
        Features: Returns the date and time for a given dataset
        Depends:  - get_object_attrs
                  - get_ds_session
        NOTE:     EXIF tags are saved to each dataset's parent group;
                  you could also check these tags for datetime attr
        """
        # Check dataset attributes for datetime:
        ds_attrs = self.get_object_attrs(ds_path)
        ds_datetime = ds_attrs.get("Date", None)
        if ds_datetime is None:
            # Check the session date attribute (this is a legacy attr)
            s_path = self.get_ds_session(ds_path)
            sess_attrs = self.get_object_attrs(s_path)
            ds_date = sess_attrs.get("date", None)
            ds_time = None
        else:
            # Check for those few that didn't get timestamped:
            ds_date_parts = ds_datetime.split(" ")
            if len(ds_date_parts) == 2:
                ds_date, ds_time = ds_date_parts
            elif len(ds_date_parts) == 1:
                ds_date = ds_date_parts[0]
                # Check for separate time attribute (pre v.1.5)
                ds_time = ds_attrs.get("Time", None)

        if ds_time is not None:
            # Fix issue with colons in file names:
            ds_time = ds_time.replace(":", ".")

        return (ds_date, ds_time)

    def get_ds_index(self, ds_path):
        """
        Name:     PridaHDF.get_ds_index
        Inputs:   str, dataset path (ds_path)
        Outputs:  int, index value
        Features: Returns a dataset's index (i.e., sequential image number)
        Depends:  - get_object_attrs
                  - get_ds_session
                  - list_objects
        """
        # Check for dataset index attribute:
        ds_attrs = self.get_object_attrs(ds_path)
        ds_index = ds_attrs.get("Index", None)
        if ds_index is None:
            # Find the index in the session's object list:
            # NOTE: index is associated with number of images taken (1-indexed)
            s_path = self.get_ds_session(ds_path)
            ds_name = os.path.splitext(os.path.basename(ds_path))[0]
            ds_list = self.list_objects(s_path)
            ds_index = ds_list.index(ds_name)
            ds_index += 1
        else:
            try:
                ds_index = int(ds_index)
            except:
                # Once again, get index from the session list:
                s_path = self.get_ds_session(ds_path)
                ds_name = os.path.splitext(os.path.basename(ds_path))[0]
                ds_list = self.list_objects(s_path)
                ds_index = ds_list.index(ds_name)
                ds_index += 1

        return ds_index

    def get_ds_orient(self, ds_path):
        """
        Name:     PridaHDF.get_ds_orient
        Inputs:   str, dataset path (ds_path)
        Outputs:  str, orientation preference
        Features: Returns a dataset's preferred orientation based on its
                  session attribute
        Depends:  - get_ds_session
                  - get_object_attrs
        """
        # Check dataset's session attributes for image orientation:
        s_path = self.get_ds_session(ds_path)
        sess_attrs = self.get_object_attrs(s_path)
        ds_orient = sess_attrs.get("img_orient", "Original")
        return ds_orient

    def get_ds_pid(self, ds_path):
        """
        Name:     PridaHDF.get_ds_pid
        Inputs:   str, dataset path (ds_path)
        Outputs:  str, sessin path (s_path)
        Features: Returns the PID path associated with a given dataset
        """
        if self.isopen:
            if ds_path in self.hdfile and self.list_objects(ds_path) == 0:
                s_path = self.hdfile[ds_path].parent.parent.parent.name
            else:
                self.logger.warning("dataset %s does not exist", ds_path)
                s_path = None
        else:
            self.logger.warning("HDF5 file not open")
            s_path = None

        if s_path in self.hdfile or s_path is None:
            if s_path:
                self.logger.debug("returning session path %s", s_path)
            return s_path
        else:
            self.logger.error(
                "Unexpected error finding the session for image %s", ds_path)

    def get_ds_session(self, ds_path):
        """
        Name:     PridaHDF.get_ds_session
        Inputs:   str, dataset path (ds_path)
        Outputs:  str, session path (s_path)
        Features: Returns the session path associated with a given dataset
        """
        if self.isopen:
            if ds_path in self.hdfile and self.list_objects(ds_path) == 0:
                s_path = self.hdfile[ds_path].parent.parent.name
            else:
                self.logger.warning("dataset %s does not exist", ds_path)
                s_path = None
        else:
            self.logger.warning("HDF5 file not open")
            s_path = None

        if s_path in self.hdfile or s_path is None:
            if s_path:
                self.logger.debug("returning session path %s", s_path)
            return s_path
        else:
            self.logger.error(
                "Unexpected error finding the session for image %s", ds_path)

    def get_output_name(self, ds_path, opts):
        """
        Name:     PridaHDF.get_output_name
        Inputs:   - str, dataset path (ds_path)
                  - dict, preference options (opts)
        Outputs:  str, preferred output file name
        Features: Creates the preferred output file name for exporting
                  * currently supports:
                    'prida': <pid>_<date>_<time>_<angle><ext>
                    'rr3d': <pid>_<date>_<session>_<index><ext>
        """
        # Check naming preference:
        pname = 'prida'
        pformat = None
        if opts is not None and isinstance(opts, dict):
            pname = opts.get('name', 'prida')
            pformat = opts.get('format', None)

        # Get dataset attributes:
        s_path = self.get_ds_session(ds_path)
        ds_sess = os.path.basename(s_path)
        ds_pid = self.get_ds_pid(ds_path).lstrip("/")
        self.logger.debug("PID: %s", ds_pid)

        ds_date, ds_time = self.get_ds_date_time(ds_path)
        self.logger.debug("Date: %s", ds_date)
        self.logger.debug("Time: %s", ds_time)
        ds_index = self.get_ds_index(ds_path)
        self.logger.debug("Index: %s", ds_index)
        ds_attrs = self.get_object_attrs(ds_path)
        if "Angle" in ds_attrs:
            ds_position = ds_attrs.get("Angle")
        else:
            # Default to index if position is unavailable
            ds_position = "%03d" % ds_index
        self.logger.debug("Pos: %s", ds_position)

        # Compile output file name:
        if pformat is not None and pformat in img_types:
            ds_ext = pformat
        else:
            ds_ext = os.path.splitext(ds_path)[1]

        if pname == 'prida':
            if ds_time is None:
                out_name = "%s_%s_%s%s" % (
                    ds_pid, ds_date, ds_position, ds_ext)
            else:
                out_name = "%s_%s_%s_%s%s" % (
                    ds_pid, ds_date, ds_time, ds_position, ds_ext)
        elif pname == 'rr3d':
            out_name = "%s_%s_%s_%03d%s" % (
                ds_pid, ds_date, ds_sess, ds_index, ds_ext)
        else:
            raise ValueError("Preferred name is unknown!")

        self.logger.debug("Output name: %s", out_name)
        return out_name

    def get_pid_attrs(self, pid):
        """
        Name:     PridaHDF.get_pit_attrs
        Features: Returns dictionary of plant ID group attributes
        Inputs:   str, plant ID (pid)
        Outputs:  dict, PID attributes (attrs_dict)
        Depends:  pid_exists
        """
        attr_dict = {}
        if self.isopen:
            if self.pid_exists(pid):
                self.logger.debug('retrieving PID attributes')
                for key in self.hdfile[pid].attrs.keys():
                    val = self.hdfile[pid].attrs[key]
                    if isinstance(val, str):
                        attr_dict[key] = val
                    else:
                        self.logger.debug('decoding string %s', str(val))
                        attr_dict[key] = val.decode('UTF-8')
            else:
                err_msg = ("could not retrieve attributes "
                           "from PID: %s") % (pid)
                self.logger.error(err_msg)
                raise IOError(err_msg)
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError(
                "Error! Could not read PID attributes. HDF5 file not open!")

        return attr_dict

    def get_dataset(self, ds_path):
        """
        Name:     PridaHDF.get_dataset
        Features: Returns a given dataset for a given session
        Inputs:   str, dataset path (ds_path)
        Outputs:  numpy.ndarray
        """
        dset = numpy.array([])
        if self.isopen:
            if ds_path in self.hdfile:
                self.logger.debug("retrieving dataset")
                dset = self.hdfile[ds_path][:, :]
            else:
                self.logger.warning(
                    "failed to get dataset %s; it does not exist", ds_path)
                self.logger.warning("returning empty array")
        else:
            self.logger.warning(
                "failed to get dataset %s; HDF file not open", ds_path)
            self.logger.warning("returning empty array")

        return dset

    def get_object_attrs(self, obj_path):
        """
        Name:     PridaHDF.get_object_attrs
        Features: Returns dictionary of group/dataset attributes
        Inputs:   str, object path (obj_path)
        Outputs:  dict, session attributes (attrs_dict)
        """
        attr_dict = {}
        if self.isopen:
            if obj_path is not None and obj_path in self.hdfile:
                self.logger.debug('building dictionary')
                for key in self.hdfile[obj_path].attrs.keys():
                    val = self.hdfile[obj_path].attrs[key]
                    if isinstance(val, bytes):
                        attr_dict[key] = val.decode('UTF-8')
                    else:
                        attr_dict[key] = val
            else:
                self.logger.warning(
                    "could not get attributes for %s; object does not exist!",
                    obj_path)
        else:
            self.logger.warning(
                "could not get attributes for %s; HDF5 file not open!",
                obj_path)

        return attr_dict

    def get_root_about(self):
        """
        Name:     PridaHDF.get_root_about
        Features: Returns the root group about attribute
        Inputs:   None.
        Outputs:  str, root group about message (attr)
        Depends:  get_attr
        """
        if self.isopen:
            self.logger.debug('getting attribute')
            attr = self.get_attr("about", "/")
        else:
            self.logger.warning('HDF5 file not open!')
            self.logger.warning('returning undefined value')
            attr = self.UNDEFINED
        #
        return attr

    def get_root_addr(self):
        """
        Name:     PridaHDF.get_root_addr
        Features: Returns the root group user address (e.g., NetID)
        Inputs:   None.
        Outputs:  str, root group user address (attr)
        Depends:  get_attr
        """
        if self.isopen:
            self.logger.debug('getting attribute')
            attr = self.get_attr("addr", "/")
        else:
            self.logger.warning('HDF5 file not open!')
            self.logger.warning('returning undefined value')
            attr = self.UNDEFINED
        #
        return attr

    def get_root_user(self):
        """
        Name:     PridaHDF.get_root_user
        Features: Returns the root group username
        Inputs:   None.
        Outputs:  str, root group username (attr)
        Depends:  get_attr
        """
        if self.isopen:
            self.logger.debug('getting attribute')
            attr = self.get_attr("user", "/")
        else:
            self.logger.warning('HDF5 file not open!')
            self.logger.warning('returning undefined value')
            attr = self.UNDEFINED
        #
        return attr

    def get_session_number(self, pid):
        """
        Name:     PridaHDF.get_session_number
        Features: Returns the value for a new session number
        Inputs:   str, plant ID (pid)
        Outputs:  None.
        Depends:  refresh_pids
        """
        self.logger.debug('refreshing PIDs')
        self.refresh_pids()
        #
        try:
            self.logger.debug('finding session value')
            session_max_val = max(self.pids[pid])
        except ValueError:
            self.logger.warning('encountered value error')
            self.logger.warning('session number set to 1')
            session_num = 1
        except KeyError:
            self.logger.warning('encountered key error')
            self.logger.warning('session number set to 1')
            session_num = 1
        except:
            self.logger.warning('encountered an error')
            self.logger.warning('session number set to 1')
            session_num = 1
        else:
            self.logger.debug('incrementing session number')
            session_num = session_max_val + 1
        finally:
            return session_num

    def img_to_hdf(self, img, ds_path,
                   to_zip=False, opts=4, to_shuffle=False, chunk_val=True):
        """
        Name:     PridaHDF.img_to_hdf
        Features: Saves image dataset to current HDF repository; with or
                  without compression
        Inputs:   - numpy.ndarray, openCV image data array (img)
                  - str, dataset name with path (ds_path)
                  - [optional] bool, whether to do compression (to_zip)
                  - [optional] int, gzip compression level [0-9] (opts)
                  - [optional] bool, whether to shuffle (to_shuffle)
                  - [optional] bool | tuple, chunk value (chunk_val)
        Outputs:  None.
        """
        if ds_path in self.hdfile:
            wmgs = "overwriting dataset %s" % (ds_path)
            self.logger.warning(wmgs)
        if img is None:
            err_msg = "did not receive image data"
            self.logger.error(err_msg)
            raise IOError(err_msg)
        else:
            try:
                self.logger.info('creating dataset of type %s', img.dtype)
                if to_zip:
                    self.hdfile.create_dataset(name=ds_path,
                                               data=img,
                                               shuffle=to_shuffle,
                                               chunks=chunk_val,
                                               compression="gzip",
                                               compression_opts=opts)
                else:
                    self.hdfile.create_dataset(name=ds_path,
                                               data=img)
            except:
                err_msg = ("create dataset '%s' failed") % (ds_path)
                self.logger.error(err_msg)
                raise IOError(err_msg)

    def list_objects(self, parent_path):
        """
        Name:     PridaHDF.list_objects
        Features: Returns a sorted list of HDF5 objects under a given parent
        Inputs:   str, HDF5 path to parent object
        Outputs:  list, HDF5 objects
        """
        rlist = []
        if self.isopen:
            if parent_path in self.hdfile:
                try:
                    self.logger.debug('listing members of %s', parent_path)
                    self.hdfile[parent_path].keys()
                except:
                    # Dataset has no members
                    self.logger.debug('no members found; returning 0')
                    rlist = 0
                else:
                    for obj in sorted(list(self.hdfile[parent_path].keys())):
                        rlist.append(obj)
            else:
                wmgs = "'%s' does not exist!" % (parent_path)
                self.logger.warning(wmgs)
                self.logger.warning('returning empty list')
        else:
            self.logger.warning('HDF5 file not open')
            self.logger.warning('returning empty list')
        #
        return rlist

    def list_pids(self):
        """
        Name:     PridaHDF.list_pids
        Features: Returns a list of current PIDs
        Inputs:   None.
        Outputs:  list, PID names
        Depends:  list_objects
        """
        self.logger.debug('listing root member objects')
        pid_list = self.list_objects('/')
        return sorted(pid_list)

    def list_sessions(self, pid):
        """
        Name:     PridaHDF.list_sessions
        Features: Returns a list of sorted sessions for a given PID
        Inputs:   str, plant ID (pid)
        Outputs:  list, session names
        Depends:  list_objects
        """
        rlist = []
        if self.pid_exists(pid):
            temp = {}
            self.logger.debug('searching for sessions')
            for session in self.list_objects(pid):
                try:
                    temp_val = re.search('\D+(\d+)', session).group(1)
                    temp_val = int(temp_val)
                except:
                    wmgs = ("could not get session value from %s") % (session)
                    self.logger.warning(wmgs)
                else:
                    temp[temp_val] = session
            #
            self.logger.debug('sorting session numbers')
            for session_num in sorted(temp.keys()):
                rlist.append(temp[session_num])
        else:
            self.logger.warning('PID has no sessions!')
        #
        return rlist

    def mkdir_p(self, path):
        """
        Name:     PridaHDF.mkdir_p
        Features: Makes directories, including intermediate directories as
                  required (i.e., directory tree)
        Inputs:   str, directory path (path)
        Outputs:  None.
        Ref:      tzot (2009) "mkdir -p functionality in python,"
                  StackOverflow, Online:
                  http://stackoverflow.com/questions/600268/
                    mkdir-p-functionality-in-python
        """
        try:
            self.logger.debug('building directory tree')
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                self.logger.debug('directory exists')
                pass
            else:
                self.logger.error('failed to create directory!')
                raise

    def new_file(self, hdf_path):
        """
        Name:     PridaHDF.new_file
        Features: Creates a new HDF5 file, overwriting an existing file if it
                  exists.
        Inputs:   str, HDF5 file path (hdf_path)
        Outputs:  None.
        """
        # Save directory and filename as class variables:
        self.filename = os.path.basename(hdf_path)
        self.dir = os.path.dirname(hdf_path)

        # Create empty HDF file:
        if os.path.isfile(hdf_path):
            self.logger.warning('overwriting existing file!')

        try:
            self.logger.info('creating file %s', hdf_path)
            self.hdfile = h5py.File(hdf_path, 'w')
        except:
            self.logger.error('FAILED!')
            self.isopen = False
        else:
            self.logger.info('SUCCESS!')
            self.isopen = True

    def open_file(self, hdf_path):
        """
        Name:     PridaHDF.open_file
        Features: Opens an existing HDF5 file or creates new if file is not
                  found.
        Inputs:   str, HDF5 filename with path (hdf_path)
        Outputs:  None.
        Depends:  - refresh_pids
                  - find_datasets
        """
        hdf_name = os.path.basename(hdf_path)

        try:
            if os.path.isfile(hdf_path):
                self.logger.info("opening file %s", hdf_name)
                self.hdfile = h5py.File(hdf_path, 'a')
            else:
                self.logger.info("creating file %s", hdf_name)
                self.hdfile = h5py.File(hdf_path, 'w')
        except:
            self.logger.exception('FAILED!')
            self.isopen = False
        else:
            self.logger.info('SUCCESS!')
            self.isopen = True
            self.filename = hdf_name
            self.dir = os.path.dirname(hdf_path)

            # Populate PID & dataset dictionaries:
            self.logger.debug('refreshing PIDs')
            self.refresh_pids()
            self.logger.debug('finding datasets')
            self.find_datasets()

    def pid_exists(self, pid):
        """
        Name:     PridaHDF.pid_exists
        Features: Checks to see if PID already exists in HDF5 file
        Inputs:   str, plant ID (pid)
        Outputs:  bool
        """
        if pid in self.pids.keys():
            self.logger.debug('True')
            return True
        else:
            self.logger.debug('False')
            return False

    def refresh_pids(self):
        """
        Name:     PridaHDF.refresh_pids
        Features: Refreshes the class dictionary with current plant IDs
        Inputs:   None.
        Outputs:  None.
        """
        self.pids = {}
        for pid in self.hdfile.keys():
            self.logger.debug("searching PID '%s'", pid)
            self.pids[pid] = []
            for session in self.hdfile[pid].keys():
                try:
                    # Search for digits following non-digits
                    self.logger.debug("appending session '%s'", session)
                    session_num = re.search('\D+(\d+)', session).group(1)
                    session_num = int(session_num)
                except:
                    err_msg = ("could not get session value from "
                               "%s") % (session)
                    self.logger.error(err_msg)
                    raise IOError(err_msg)
                else:
                    self.pids[pid].append(session_num)

    def save(self):
        """
        Name:     PridaHDF.save
        Features: Flushes changes to the HDF file.
        Inputs:   None.
        Outputs:  None.
        """
        if self.isopen:
            try:
                self.logger.debug('flushing file')
                self.hdfile.flush()
            except:
                self.logger.error('failed to flush!')
                raise IOError("Could not flush HDF5 file!")

    def save_image(self, session_path, img_path, make_thumb=True):
        """
        Name:     PridaHDF.save_image
        Input:    - str, session path (session_path)
                  - str, input image name with path (img_path)
                  - [optional] bool, flag for thumbnail creation (make_thumb)
        Output:   None.
        Features: Saves image data as a dataset to a given session;
                  saves image height, width and color to the dataset;
                  saves image tags to the image group;
        Depends:  - img_to_hdf
                  - imopen
                  - set_attr
        """
        if self.isopen:
            self.logger.debug('start.')
            try:
                self.logger.debug('gathering image name and path')
                img_name = os.path.basename(img_path)
                img_base, img_ext = os.path.splitext(img_name)
            except:
                err_msg = "could not process image: %s" % (img_path)
                self.logger.error(err_msg)
                raise IOError(err_msg)
            else:
                self.logger.debug('building dataset path')
                if session_path[-1:] != '/':
                    session_path += "/"
                im_path = "%s%s" % (session_path, img_base)
                ds_path = "%s/%s" % (im_path, img_name)

                self.logger.debug('reading dataset from file')
                img = imopen(img_path)
                nfields = len(img.shape)
                if nfields == 3:
                    height, width, _ = img.shape
                    img_color = "RGB Color"
                elif nfields == 2:
                    height, width = img.shape
                    img_color = "Grayscale"
                else:
                    height = None
                    width = None
                    img_color = "Undefined"

                self.logger.debug('saving image')
                try:
                    self.img_to_hdf(img, ds_path)
                except:
                    self.logger.error("Could not save image")
                    raise
                else:
                    if make_thumb:
                        self.logger.debug('saving thumb')
                        self.save_thumb(img, ds_path)

                    self.logger.debug("saving image group tags")
                    tags = read_exif_tags(img_path)
                    for tag in tags:
                        val = tags[tag]
                        self.set_attr(tag, val, im_path)

                    self.logger.debug("saving image dataset attributes")
                    self.set_attr("Color", img_color, ds_path)
                    self.set_attr("Height", str(height), ds_path)
                    self.set_attr("Width", str(width), ds_path)

                    self.logger.debug("complete.")
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError("Error! Could not save image. HDF5 file not open!")

    def make_thumb(self, img):
        """
        Name:     PridaHDF.make_thumb
        Inputs:   numpy.ndarray, image data array (img)
        Outputs:  numpy.ndarray, scaled data array (thumb)
        Features: Scales an image to a thumbnail with a width of 250 pixels
        """
        if isinstance(img, numpy.ndarray):
            if img.size != 0:
                try:
                    self.logger.debug('getting array shape')
                    (height, width, depth) = img.shape
                except:
                    # Maybe greyscaled image?
                    self.logger.warning("encountered possible greyscale image")
                    (height, width) = img.shape

                self.logger.debug('calculating scaling factor')
                s_factor = 250.0/width

                try:
                    self.logger.debug('resizing image to thumb')
                    thumb = scale_image(img, s_factor)
                except:
                    self.logger.exception("failed to scale image to thumb")
                    raise IOError("Failed to scale image to thumb")
                else:
                    return thumb
            else:
                self.logger.error("cannot resize image with size zero")
                raise IOError("Failed to scale image, array size is zero")
        else:
            self.logger.error("did not receive an array")
            raise TypeError("function takes an array, not type %s", type(img))

    def save_thumb(self, img, ds_path):
        """
        Name:     PridaHDF.save_thumb
        Inputs:   - numpy.ndarray, openCV image data array (img)
                  - str, dataset name with path (ds_path)
        Outputs:  None.
        Features: Saves a thumbnail version of given image to the same dataset
                  group
        Depends:  make_thumb
        """
        if ds_path in self.hdfile:
            if img is not None:
                try:
                    thumb_path = "%s.thumb" % (os.path.splitext(ds_path)[0])
                    self.logger.debug("resizing image to thumb")
                    thumb = self.make_thumb(img)

                    self.logger.debug("saving thumb to HDF5 file")
                    self.hdfile.create_dataset(name=thumb_path, data=thumb)
                except:
                    err_msg = "could not create dataset %s" % (thumb_path)
                    self.logger.exception(err_msg)
                    raise IOError(err_msg)

    def set_attr(self, attr_name, attr_val, obj_path):
        """
        Name:     PridaHDF.set_attr
        Inputs:   - str, attribute name (attr_name)
                  - [dtype], attribute value (attr_val)
                  - str, path to group/dataset object (obj_path)
        Outputs:  None.
        Features: Sets given attribute to given path;
                  * raises exception upon failure
        """
        if self.isopen:
            try:
                self.logger.debug('creating attribute "%s"', attr_name)

                # Typecast strings to data:
                if isinstance(attr_val, str):
                    self.logger.debug('encoding "%s"', attr_val)
                    attr_val = attr_val.encode('utf-8')
                elif attr_val is None:
                    self.logger.debug("NoneType attribute value")
                    attr_val = "".encode('utf-8')

                self.hdfile[obj_path].attrs.create(name=attr_name,
                                                   data=attr_val)
            except:
                err_msg = "could not set '%s' to '%s' at '%s'" % (
                    attr_val, attr_name, obj_path)
                self.logger.exception(err_msg)
                raise
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError(
                "Error! Could not set attribute. HDF5 file not open!")

    def set_pid_attrs(self, pid, attrs_dict):
        """
        Name:     PridaHDF.set_pit_attrs
        Features: Sets plant ID group attributes
        Inputs:   - str, plant ID (pid)
                  - dict, PID attributes (attrs_dict)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if self.pid_exists(pid):
                self.logger.debug('searching PID attributes')
                for key in attrs_dict.keys():
                    val = attrs_dict[key]
                    try:
                        self.logger.debug("setting attribute %s", key)
                        self.set_attr(key, val, pid)
                    except:
                        err_msg = "could not set attr %s" % (key)
                        self.logger.error(err_msg)
                        raise IOError(err_msg)
            else:
                err_msg = "could not attribute PID: %s" % (pid)
                self.logger.error(err_msg)
                raise IOError(err_msg)
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError(
                "Error! Could not set PID attributes. HDF5 file not open!")

    def set_root_about(self, attr):
        """
        Name:     PridaHDF.set_root_about
        Features: Sets the root group about attribute (e.g., short experiment
                  description)
        Inputs:   str, root group about message (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.logger.debug('setting attribute')
            self.set_attr("about", attr, "/")
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError(
                "Error! Could not set root about. HDF5 file not open!")

    def set_root_addr(self, attr):
        """
        Name:     PridaHDF.set_root_addr
        Features: Sets the root group user address (e.g., NetID)
        Inputs:   str, root group user address (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.logger.debug('setting attribute')
            self.set_attr("addr", attr, "/")
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError(
                "Error! Could not set root addr. HDF5 file not open!")

    def set_root_user(self, attr):
        """
        Name:     PridaHDF.set_root_user
        Features: Sets the root group username
        Inputs:   str, root group username (attr)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            self.logger.debug('setting attribute')
            self.set_attr("user", attr, "/")
        else:
            self.logger.error('HDF5 file not open!')
            raise IOError(
                "Error! Could not set root user. HDF5 file not open!")

    def set_session_attrs(self, session_path, attrs_dict):
        """
        Name:     PridaHDF.set_pit_attrs
        Features: Sets session attributes
        Inputs:   - str, session path (session_path)
                  - dict, session attributes (attrs_dict)
        Outputs:  None.
        Depends:  set_attr
        """
        if self.isopen:
            if session_path in self.hdfile:
                self.logger.debug('searching attributes')
                for key in attrs_dict.keys():
                    val = attrs_dict[key]
                    try:
                        self.logger.debug('setting attribute %s', key)
                        self.set_attr(key, val, session_path)
                    except:
                        err_msg = "could not set attr %s" % (
                            key)
                        self.logger.error(err_msg)
                        raise IOError(err_msg)
            else:
                err_msg = "could not attribute %s" % (
                    session_path)
                self.logger.error(err_msg)
                raise IOError(err_msg)

    def write_to_file(self, out_path, img_array, opts=None):
        """
        Name:     PridaHDF.write_to_file
        Inputs:   - str, output file path (out_path)
                  - numpy.ndarray, image array (img_array)
                  - [optional] dict, extraction options (opts)
        Outputs:  None.
        Features: Writes an image dataset to file after assigning extraction
                  options
        Depends:  - imsave
                  - is_rgb
                  - mkdir_p
                  - rel_crop
                  - rgb_to_gray
                  - scale_image
        """
        # Get extraction options:
        pcolor = None
        porient = "Original"
        pscale = 1.0
        tokeep = False
        tocrop = False
        tval = 0
        bval = 100
        lval = 0
        rval = 100
        if opts is not None and isinstance(opts, dict):
            pcolor = opts.get('color', None)
            porient = opts.get('orient', "Original")
            pscale = opts.get('scale', 1.0)
            tokeep = opts.get('keep', False)
            tocrop = opts.get('crop', False)
            tval = opts.get('crop_top', 0)
            bval = opts.get('crop_bottom', 100)
            lval = opts.get('crop_left', 0)
            rval = opts.get('crop_right', 100)

        # Move images up one directory to keep them together:
        if tokeep:
            out_dir = os.path.dirname(out_path)
            out_file = os.path.basename(out_path)
            out_path = os.path.join(os.path.split(out_dir)[0], out_file)

        # Crop image (if necessary)
        if tocrop:
            img_array = rel_crop(img_array, tval, rval, bval, lval)

        # Scale image (if necessary):
        if pscale < 100:
            img_array = scale_image(img_array, pscale)

        # Correct image orientation:
        if porient != "Original":
            if porient == "Rotated Clockwise":
                self.logger.debug("rotating image clockwise")
                img_array = numpy.rot90(img_array, 3)
            elif porient == "Rotated Counter-Clockwise":
                self.logger.debug("rotating image counter-clockwise")
                img_array = numpy.rot90(img_array)
            else:
                self.logger.warning("failed to match orientation %s", porient)

        # Convert RGB to grayscale based on preferred color (if necessary):
        if pcolor is not None and is_rgb(img_array):
            if pcolor == 'gray':
                img_array = rgb_to_gray(img_array)

        # Create output directory tree if it does not exist:
        out_dir = os.path.dirname(out_path)
        if not os.path.isdir(out_dir):
            self.mkdir_p(out_dir)

        self.logger.debug('saving image %s', out_path)
        imsave(out_path, img_array)


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    # Setup log file:
    root_logger = logging.getLogger()
    root_logger.setLevel("INFO")

    fh = logging.handlers.RotatingFileHandler("prida_hdf.log", backupCount=9)
    rec_format = '%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s'
    formatter = logging.Formatter(rec_format, datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)

    root_logger.addHandler(fh)

    # Test compression:
    my_file = "/Users/twdavis/Desktop/temp/test.hdf5"
    my_class = PridaHDF()
    my_class.open_file(my_file)
    my_class.compress()
    my_class.close()

    # Test compression methods, compression levels, and chunking:
    if False:
        # Find images to process:
        img_dir = ("/Users/twdavis/Dropbox/work/usda/projects/rootimaging/"
                   "prida/Images/")
        img_files = glob.glob(img_dir + "*.jpg")

        # Create an HDF5 file:
        hdf_dir = '/Users/twdavis/Desktop/temp/test_hdf/'
        my_class = PridaHDF()
        log_file = os.path.join(hdf_dir, 'comp_test.txt')
        my_file = open(log_file, 'w')
        my_file.write("Img:Zip_option:Shuffle:Chunk:Time\n")
        my_file.close()

        # Save imgs to HDF:
        my_shuffles = [True, False]
        my_chunks = [True, (400, 600, 3), (400, 600, 1)]
        my_opts = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        for i in range(len(my_shuffles)):
            for j in range(len(my_chunks)):
                for k in range(len(my_opts)):
                    shuffle_val = my_shuffles[i]
                    chunk_val = my_chunks[j]
                    opt_val = my_opts[k]
                    hdf_name = "test_%d-%d-%d.hdf5" % (i, j, k)
                    hdf_file = os.path.join(hdf_dir, hdf_name)
                    my_class.open_file(hdf_file)
                    #
                    for img_path in img_files:
                        img_basename = os.path.splitext(
                            os.path.basename(img_path))[0]
                        ds_path = "/%s" % (img_basename)
                        img = imopen(img_path)
                        t_start = time.time()
                        my_class.img_to_hdf(img,
                                            ds_path,
                                            to_zip=True,
                                            opts=opt_val,
                                            to_shuffle=shuffle_val,
                                            chunk_val=chunk_val)
                        t_end = time.time()
                        my_time = t_end - t_start
                        my_line = "%s:%d:%s:%s:%d\n" % (img_basename,
                                                        opt_val,
                                                        shuffle_val,
                                                        chunk_val,
                                                        my_time)
                        my_file = open(log_file, 'a')
                        my_file.write(my_line)
                        my_file.close()
                    while not my_class.close():
                        pass
