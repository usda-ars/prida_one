#!/usr/bin/python
#
# calibutil.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################

###############################################################################
# REQUIRED MODULES:
###############################################################################
import numpy


###############################################################################
# CONSTANTS:
###############################################################################
# Mean indexes of refraction for visible light
eta_a = 1.000269     # air at standard room conditions (20 degC, 53% RH,
eta_a_err = 1.9e-6   # 400ppm [CO2]) (Ciddor, 1996)

eta_g = 1.496        # acrylic (PMMA) at 20 degC and 53% RH
eta_g_err = 5.2e-3   # (Beadie et al., 2005)

eta_w = 1.336        # distilled water at 20 degC; Cauchy formula
eta_w_err = 4.8e-3   # (Kedenburg et al., 2012)

# Uncertainty in measured distances:
z_err = 0.005        # meters, (i.e., 0.5 cm)
zt_err = z_err*numpy.sqrt(3.0)


###############################################################################
# CLASSES:
###############################################################################
class element():
    """
    Name:     element
    Features: Class representing a discrete, continuous body of black pixels,
              called an element.
    History:  Version 1.5.0
              - added offset class variable [16.09.09]
              - corrected pixel centers to pixel coordinate system [16.09.09]
              - removed coi attribute [16.09.09]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, data, shape, offset):
        """
        Name:     element.__init__
        Inputs:   - set(tuple(int)), set of coordinate points (data)
                  - tuple(int, int), image array shape (shape)
                  - tuple(int, int), p1 and p2 cropping offsets, px (offset)
        Outputs:  None.
        Features: Class initialization
        """
        self.shape = shape      # cropped image array shape
        self.offset = offset    # offset of cropped image from original
        self._center = None     # pixel center coordinates of element
        self._orig = data       # coordinate data points
        self._size = None       # total number of pixels in element
        self._width = None      # pixel width of element
        self._height = None     # pixel height of element
        self._img = None        # pixel representation of data points

    def __str__(self):
        """
        Name:     element.__str__
        Inputs:   None.
        Outputs:  str
        Features: Class string property, accessed via the print function
        """
        pstr = '{} - \n'.format(self.__class__)
        pstr += ('container shape: '
                 '({0[0]}[px], {0[1]}[px])\n').format(self.shape)
        pstr += 'element size: {:d}[px]\n'.format(self.size)
        pstr += 'element width: {:d}[px]\n'.format(self.width)
        pstr += 'element height: {:d}[px]\n'.format(self.height)
        return pstr

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def center(self):
        """Pixel center of element"""
        if self._center is not None:
            return self._center
        else:
            # Extract the sorted x and y coordinates:
            yvect = numpy.array(
                [coord[0] + self.offset[0] for coord in self._orig])
            xvect = numpy.array(
                [coord[1] + self.offset[1] for coord in self._orig])

            # Round to the center-most pixel:
            mean_y = 0.5*(yvect.max() + yvect.min())
            mean_x = 0.5*(xvect.max() + xvect.min())
            mid_y = int(mean_y + 0.5)
            mid_x = int(mean_x + 0.5)

            self._center = (mid_y, mid_x)
            return self._center

    @center.setter
    def center(self, val):
        raise AttributeError("manual setting of center is forbidden")

    @property
    def height(self):
        """Pixel height of element"""
        if self._height is not None:
            return self._height
        else:
            yvect = numpy.array([coord[0] for coord in self._orig])
            self._height = int(yvect.max() - yvect.min()) + 1
            return self._height

    @height.setter
    def height(self, val):
        raise AttributeError('manual setting of height is forbidden')

    @property
    def img(self):
        """Pixel representation of coordinates"""
        if self._img is not None:
            return self._img
        else:
            new_array = numpy.ones(self.shape).astype("uint8")
            new_array *= 255
            for item in self._orig:
                new_array[item] = 0
            self._img = new_array
            return new_array

    @img.setter
    def img(self, val):
        raise AttributeError("manual setting of img is forbidden")

    @property
    def size(self):
        """Number of pixels in element"""
        if self._size is not None:
            return self._size
        else:
            self._size = len(self._orig)
            return self._size

    @size.setter
    def size(self, val):
        raise AttributeError('manual setting of size is forbidden')

    @property
    def width(self):
        """Pixel width of element"""
        if self._width is not None:
            return self._width
        else:
            xvect = numpy.array([coord[1] for coord in self._orig])
            self._width = int(xvect.max() - xvect.min()) + 1
            return self._width

    @width.setter
    def width(self, val):
        raise AttributeError('manual setting of width is forbidden')


class PARTICLE():
    """
    Name:     PARTICLE
    Features: Class representing a discrete, continuous body of foreground
              pixels; simplified element class for dust removal.
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, data):
        """
        Name:     PARTICLE.__init__
        Inputs:   set(tuple(int)), set of coordinate points (data)
        Outputs:  None.
        Features: Class initialization
        """
        self._orig = data       # coordinate data points
        self._size = None       # total number of pixels in element

    def __str__(self):
        """
        Name:     PARTICLE.__str__
        Inputs:   None.
        Outputs:  str
        Features: Class string property, accessed via the print function
        """
        pstr = '{} - \n'.format(self.__class__)
        pstr += 'particle size: {:d}[px]\n'.format(self.size)
        return pstr

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def points(self):
        """
        The list of original coordinate points
        """
        return list(self._orig)

    @points.setter
    def points(self, val):
        raise AttributeError('manual setting of size is forbidden')

    @property
    def size(self):
        """
        The total number of pixels in the particle cluster
        """
        if self._size is not None:
            return self._size
        else:
            self._size = len(self._orig)
            return self._size

    @size.setter
    def size(self, val):
        raise AttributeError('manual setting of size is forbidden')


###############################################################################
# FUNCTIONS:
###############################################################################
def calc_ha(za, zt, hfp, hfp_err):
    """
    Name:     calc_ha
    Inputs:   - float, za distance, m (za)
              - float, zt distance, i.e., zt=za+zg+zw, m (zt)
              - float, uncorrected hf, m (hfp)
              - float, uncorrected hf uncertainty, m (hfp_err)
    Outputs:  tuple, ha distance and associated uncertainty, m
    Features: Calculates the ha distance and its error
    """
    ha = za*hfp
    ha /= zt

    ztx = numpy.power(zt, 2.0)
    ddzax = numpy.power(hfp*z_err/zt, 2.0)
    ddztx = numpy.power(-za*hfp*zt_err/ztx, 2.0)
    ddhfx = numpy.power(za*hfp_err/zt, 2.0)
    ha_err = numpy.sqrt(ddzax + ddztx + ddhfx)

    return (ha, ha_err)


def calc_hg(zg, zt, hfp, hfp_err):
    """
    Name:     calc_hg
    Inputs:   - float, zg distance, m (zg)
              - float, zt distance, i.e., zt=za+zg+zw, m (zt)
              - float, uncorrected hf, m (hfp)
              - float, uncorrected hf uncertainty, m (hfp_err)
    Outputs:  tuple, hg distance and associated uncertainty, m
    Features: Calculates the hg distance and its error
    """
    nax = numpy.power(eta_a, 2.0)
    ngx = numpy.power(eta_g, 2.0)
    ztx = numpy.power(zt, 2.0)
    hfx = numpy.power(hfp, 2.0)
    nzhx = numpy.sqrt(ngx*ztx + hfx*(ngx - nax))
    nzhxx = numpy.power(nzhx, 3.0)

    hg = zg*eta_a*hfp
    hg /= nzhx

    ddzg = eta_a*hfp
    ddzg /= nzhx
    ddhf = eta_a*zg*ngx*ztx
    ddhf /= nzhxx
    ddng = -eta_a*zg*eta_g*hfp*(ztx + hfx)
    ddng /= nzhxx
    ddzt = -eta_a*zg*ngx*zt*hfp
    ddzt /= nzhxx

    ddzgx = numpy.power(ddzg*z_err, 2.0)
    ddztx = numpy.power(ddzt*zt_err, 2.0)
    ddngx = numpy.power(ddng*eta_g_err, 2.0)
    ddhfx = numpy.power(ddhf*hfp_err, 2.0)
    hg_err = numpy.sqrt(ddzgx + ddztx + ddngx + ddhfx)

    return (hg, hg_err)


def calc_hw(zw, zt, hfp, hfp_err):
    """
    Name:     calc_hw
    Inputs:   - float, zw distance, m (zw)
              - float, zt distance, i.e., zt=za+zg+zw, m (zt)
              - float, uncorrected hf, m (hfp)
              - float, uncorrected hf uncertainty, m (hfp_err)
    Outputs:  tuple, hw distance and associated uncertainty, m
    Features: Calculates the hw distance and its error
    """
    nax = numpy.power(eta_a, 2.0)
    nwx = numpy.power(eta_w, 2.0)
    ztx = numpy.power(zt, 2.0)
    hfx = numpy.power(hfp, 2.0)
    nzhx = numpy.sqrt(nwx*ztx + hfx*(nwx - nax))
    nzhxx = numpy.power(nzhx, 3.0)

    hw = zw*eta_a*hfp
    hw /= nzhx

    ddzw = eta_a*hfp
    ddzw /= nzhx
    ddhf = eta_a*zw*nwx*ztx
    ddhf /= nzhxx
    ddnw = -eta_a*zw*eta_w*hfp*(ztx + hfx)
    ddnw /= nzhxx
    ddzt = -eta_a*zw*nwx*zt*hfp
    ddzt /= nzhxx

    ddzwx = numpy.power(ddzw*z_err, 2.0)
    ddztx = numpy.power(ddzt*zt_err, 2.0)
    ddnwx = numpy.power(ddnw*eta_w_err, 2.0)
    ddhfx = numpy.power(ddhf*hfp_err, 2.0)
    hw_err = numpy.sqrt(ddzwx + ddztx + ddnwx + ddhfx)

    return (hw, hw_err)


def calc_ir(xo, xo_err, eXo):
    """
    Name:     calc_ir
    Inputs:   - float, xo distance, m (xo)
              - float, xo distance uncertainty, m (xo_err)
              - int, pixel height, px (eXo)
    Outputs:  tuple, image resolution factor and associated uncertainty, m/px
    Features: Calculates the image resolution conversion factor and error
    """
    ir = xo/eXo
    ir_err = xo_err/eXo

    return (ir, ir_err)


def calc_phi(hf, hf_err, zt):
    """
    Name:     calc_phi
    Inputs:   - float, hf distance, m (hf)
              - float, hf distance uncertainty, m (hf_err)
              - float, zt distance, i.e., zt=za+zg+zw, m (zt)
    Outputs:  tuple, pitch and associated uncertainty, deg
    Features: Calculates the pitch and its error
    """
    phi = numpy.arctan(hf/zt)
    phi = numpy.degrees(phi)

    # Compute variable substitutes:
    hfx = numpy.power(hf, 2.0)
    ztx = numpy.power(zt, 2.0)

    # Compute the partial derivatives:
    ddhf = zt
    ddhf /= ztx + hfx
    ddzt = -hf
    ddzt /= ztx + hfx

    ddhfx = numpy.power(ddhf*hf_err, 2.0)
    ddztx = numpy.power(ddzt*zt_err, 2.0)

    phi_err = numpy.sqrt(ddhfx + ddztx)
    phi_err = numpy.degrees(phi_err)

    return (phi, phi_err)


def calc_theta(c1, c1_err):
    """
    Name:     calc_theta
    Inputs:   - float, slope of regression (c1)
              - float, slope of regression uncertainty (c1_err)
    Outputs:  tuple, roll and associated uncertainty, degrees
    Features: Calculates roll and its error
    """
    theta = numpy.sign(c1)
    theta *= numpy.pi / 2.0 - numpy.abs(numpy.arctan(c1))
    theta = numpy.degrees(theta)

    theta_err = -1.0 * c1_err
    theta_err /= 1.0 + numpy.power(c1, 2.0)
    theta_err = numpy.degrees(numpy.abs(theta_err))

    return (theta, theta_err)


def calc_xa(za, alpha):
    """
    Name:     calc_xa
    Inputs:   - float, za distance, m (za)
              - float, camera field of view, degrees (alpha)
    Outputs:  tuple, xa distance, m, and associated uncertainty, m
    Features: Calculates the xa distance and its error
    """
    talph = numpy.tan(0.5*numpy.radians(alpha))
    xa = za*talph
    xa_err = z_err*talph

    return (xa, xa_err)


def calc_xg(zg, xi):
    """
    Name:     calc_xg
    Inputs:   - float, zg distance, m (zg)
              - float, variable substitute (xi)
    Outputs:  tuple, xg distance, m, and associated uncertainty, m
    Features: Calculates the xg distance and its error
    """
    # Compute variable substitutes:
    ngx = numpy.power(eta_g, 2.0)
    nax = numpy.power(eta_a, 2.0)
    xix = numpy.power(xi, 2.0)
    nggax = numpy.sqrt(2.0*ngx - nax*xix)
    nggaxx = numpy.power(nggax, 3.0)
    sxgn = -2.0*zg*eta_g*eta_a*xi

    xg = zg*eta_a*xi
    xg /= nggax

    xg_err = numpy.power(eta_a*xi*z_err/nggax, 2.0)
    xg_err += numpy.power(sxgn*eta_g_err/nggaxx, 2.0)
    xg_err = numpy.sqrt(xg_err)

    return (xg, xg_err)


def calc_xo(xa, xa_err, xg, xg_err, xw, xw_err):
    """
    Name:     calc_xo
    Inputs:   - float, the xa distance, m (xa)
              - float, the xa distance uncertainty, m (xa_err)
              - float, the xg distance, m (xg)
              - float, the xg distance uncertainty, m (xg_err)
              - float, the xw distance, m (xw)
              - float, the xw distance uncertainty, m (xw_err)
    Outputs:  tuple, the xo distance and associated uncertainty, m
    Features: Calculates the xo distance and its error
    """
    # Compute variable substitutes:
    xax = numpy.power(xa_err, 2.0)
    xgx = numpy.power(xg_err, 2.0)
    xwx = numpy.power(xw_err, 2.0)

    xo = 2.0*(xa + xg + xw)
    xo_err = 2.0*numpy.sqrt(xax + xgx + xwx)

    return (xo, xo_err)


def calc_xw(zw, xi):
    """
    Name:     calc_xw
    Inputs:   - float, zw distance, m (zw)
              - float, variable substitute (xi)
    Outputs:  tuple, xw distance, m, and associated uncertainty, m
    Features: Calculates the xw distance and its error
    """
    # Compute variable substitutes:
    nwx = numpy.power(eta_w, 2.0)
    nax = numpy.power(eta_a, 2.0)
    xix = numpy.power(xi, 2.0)
    nwwax = numpy.sqrt(2.0*nwx - nax*xix)
    nwwaxx = numpy.power(nwwax, 3.0)
    sxwn = -2.0*zw*eta_w*eta_a*xi

    xw = zw*eta_a*xi
    xw /= nwwax

    xw_err = numpy.power(eta_a*xi*z_err/nwwax, 2.0)
    xw_err += numpy.power(sxwn*eta_w_err/nwwaxx, 2.0)
    xw_err = numpy.sqrt(xw_err)

    return (xw, xw_err)


def calc_ya(za, zt, ytp, ytp_err):
    """
    Name:     calc_ya
    Inputs:   - float, za distance, m (za)
              - float, zt distance, i.e., zt=za+zg+zw, m (zt)
              - float, uncorrected translation, m (ytp)
              - float, uncorrected translation uncertainty, m (ytp_err)
    Outputs:  tuple, ya distance and associated uncertainty, m
    Features: Calculates the ya distance and its error
    """
    ya = za*ytp
    ya /= zt

    ztx = numpy.power(zt, 2.0)
    ddzax = numpy.power(ytp*z_err/zt, 2.0)
    ddztx = numpy.power(-za*ytp*zt_err/ztx, 2.0)
    ddytx = numpy.power(za*ytp_err/zt, 2.0)
    ya_err = numpy.sqrt(ddzax + ddztx + ddytx)

    return (ya, ya_err)


def calc_yg(zg, zt, ytp, ytp_err):
    """
    Name:     calc_yg
    Inputs:   - float, zg distance, m (zg)
              - float, zt distance, i.e., zt=za+zg+zw, m (zt)
              - float, uncorrected translation, m (ytp)
              - float, uncorrected translation uncertainty, m (ytp_err)
    Outputs:  tuple, yg distance and associated uncertainty, m
    Features: Calculates the yg distance and its error
    """
    nax = numpy.power(eta_a, 2.0)
    ngx = numpy.power(eta_g, 2.0)
    ztx = numpy.power(zt, 2.0)
    ytx = numpy.power(ytp, 2.0)
    nzyx = numpy.sqrt(ngx*ztx + ytx*(ngx - nax))
    nzyxx = numpy.power(nzyx, 3.0)

    yg = zg*eta_a*ytp
    yg /= nzyx

    ddzg = eta_a*ytp
    ddzg /= nzyx
    ddyt = eta_a*zg*ngx*ztx
    ddyt /= nzyxx
    ddzt = -eta_a*zg*ngx*zt*ytp
    ddzt /= nzyxx
    ddng = -eta_a*zg*eta_g*ytp*(ztx + ytx)
    ddng /= nzyxx

    ddzgx = numpy.power(ddzg*z_err, 2.0)
    ddztx = numpy.power(ddzt*zt_err, 2.0)
    ddngx = numpy.power(ddng*eta_g_err, 2.0)
    ddytx = numpy.power(ddyt*ytp_err, 2.0)
    yg_err = numpy.sqrt(ddzgx + ddztx + ddngx + ddytx)

    return (yg, yg_err)


def calc_yw(zw, zt, ytp, ytp_err):
    """
    Name:     calc_yw
    Inputs:   - float, zw distance, m (zw)
              - float, zt distance, i.e., zt=za+zg+zw, m (zt)
              - float, uncorrected translation, m (ytp)
              - float, uncorrected translation uncertainty, m (ytp_err)
    Outputs:  tuple, yw distance and associated uncertainty, m
    Features: Calculates the yw distance and its error
    """
    nax = numpy.power(eta_a, 2.0)
    nwx = numpy.power(eta_w, 2.0)
    ztx = numpy.power(zt, 2.0)
    ytx = numpy.power(ytp, 2.0)
    nzyx = numpy.sqrt(nwx*ztx + ytx*(nwx - nax))
    nzyxx = numpy.power(nzyx, 3.0)

    yw = zw*eta_a*ytp
    yw /= nzyx

    ddzw = eta_a*ytp
    ddzw /= nzyx
    ddyt = eta_a*zw*nwx*ztx
    ddyt /= nzyxx
    ddzt = -eta_a*zw*nwx*zt*ytp
    ddzt /= nzyxx
    ddnw = -eta_a*zw*eta_w*ytp*(ztx + ytx)
    ddnw /= nzyxx

    ddzwx = numpy.power(ddzw*z_err, 2.0)
    ddztx = numpy.power(ddzt*zt_err, 2.0)
    ddnwx = numpy.power(ddnw*eta_w_err, 2.0)
    ddytx = numpy.power(ddyt*ytp_err, 2.0)
    yw_err = numpy.sqrt(ddzwx + ddztx + ddnwx + ddytx)

    return (yw, yw_err)


def find_nearest(my_array, coord, visited):
    """
    Name:     find_nearest
    Inputs:   - numpy.ndarray, a binary image array (my_array)
              - tuple, starting location coordinates (coord)
              - set, coordinates already visited (visited)
    Outputs:  set, unvisted neighboring foreground pixel coords (neighbors)
    Features: Returns neighboring foreground (i.e., black) pixel coordinates
              that have yet to be visited
    """
    y, x = coord
    h, w = my_array.shape
    neighbors = []

    # determine cardinal neighbor coordinates
    cards = [(y, x-1), (y-1, x), (y+1, x), (y, x+1),
             (y-1, x-1), (y-1, x+1), (y+1, x-1), (y+1, x+1)]

    # for each location, check if it's in the image, black and unvisted
    for loc in cards:
        if (loc[0] >= 0 and loc[0] < h):
            if (loc[1] >= 0 and loc[1] < w):
                if my_array[loc] == 0:
                    if loc not in visited:
                        neighbors.append(loc)
    return set(neighbors)


def find_neighbors(my_array, coord):
    """
    Name:     find_neighbors
    Inputs:   - numpy.ndarray, binary image array (my_array)
              - tuple(int, int), starting location coordinates (coord)
    Outputs:  set, coordinates of connected pixels with given location
    Features: Finds all black pixels neighboring the given pixel
    Depends:  find_nearest
    """
    visited = set()
    to_visit = set()
    visited.add(coord)
    to_visit.update(find_nearest(my_array, coord, visited))

    # loop until all neighbors to visit have been visited
    while len(to_visit.difference(visited)) != 0:
        # accumulate new neighbors
        new_neighbors = set()
        for loc in to_visit:
            # update visited list and add its new neighbors
            visited.add(loc)
            new_neighbors.update(find_nearest(my_array, loc, visited))
        # remove visited pixels from new to visit list
        to_visit = new_neighbors
    return visited


def find_particles(bin_array):
    """
    Name:     find_particles
    Inputs:   numpy.ndarray, a binary image array (bin_array)
    Outputs:  list, PARTICLE list
    Features: finds all continuous groups of black pixels that cross the
              horizontal center of the image and displays them as a new image
    Depends:  find_neighbors
    """
    # NOTE: method assumes image array is binary
    bin_array = bin_array.astype("bool")

    # Instantiate sets:
    # NOTE: visited is the set of all coords whose neighbors have been found
    parts = []
    visited = set()
    fg_pixels = numpy.where(~bin_array)
    yids, xids = fg_pixels
    for i in range(len(xids)):
        n = xids[i]
        m = yids[i]
        if (m, n) not in visited:
            # New pixel found! Perform next search:
            elem = PARTICLE(data=find_neighbors(bin_array, (m, n)))
            parts.append(elem)
            visited.update(elem._orig)
    return parts


def find_feature_extents(my_array):
    """
    Name:     find_feature_extents
    Inputs:   numpy.ndarray, binary image array (my_array)
    Outputs:  list, starting and ending row indexes
    Features: Returns a list of tuples of staring and ending rows for
              foreground features
    Depends:  pop_single_rows
    """
    # Set foreground pixel to black, aka 0, (or min pixel value):
    fg = my_array.min()

    # Find row range for foreground:
    fg_idx = numpy.where(my_array == fg)
    row_min = fg_idx[0].min()
    row_max = fg_idx[0].max()
    row_range = row_max - row_min + 1

    # Find the indexes for foreground rows:
    fg_rows = []
    for row in range(row_min, row_max + 1):
        if fg in my_array[row, :]:
            fg_rows.append(row)
    row_count = len(fg_rows)

    # Calculate the allowance; this is 25% of the estimated white
    # space between elements (the tallest stack is 15 rings)
    max_rings = 15
    allowance = int((row_range - row_count) / (4.0 * (max_rings - 1)) + 0.5)

    # Estimate the number of elements:
    ring_exts = []
    r_start = fg_rows[0]
    r_end = None
    for i in range(row_count - 1):
        search_failed = False
        for j in range(allowance):
            if fg_rows[i] + j == fg_rows[i + 1]:
                search_failed = True
        if not search_failed:
            r_end = fg_rows[i]
            ring_exts.append(tuple([r_start, r_end]))
            r_start = fg_rows[i + 1]
    r_end = fg_rows[-1]
    ring_exts.append(tuple([r_start, r_end]))

    # Clean up any single row features:
    corr_exts = []
    for k in range(len(ring_exts)):
        s_row, e_row = ring_exts[k]
        if s_row != e_row:
            corr_exts.append(ring_exts[k])

    return corr_exts


def find_foreground(my_array, row_exts):
    """
    Name:     find_foreground
    Inputs:   - numpy.ndarray, binary image array (my_array)
              - tuple, starting and ending row indexes (row_exts)
    Outputs:  set, coordinates of foreground pixels
    Features: Returns the set of foreground (i.e., black) pixels in an image
              between the given starting and ending rows

    """
    my_pixels = []
    h, w = my_array.shape
    start_row, end_row = row_exts
    if start_row >= 0 and start_row < end_row and end_row <= h:
        for m in range(start_row, end_row + 1):
            for n in range(w):
                if my_array[m, n] == 0:
                    my_pixels.append(tuple([m, n]))

    return set(my_pixels)


def func_line(x, a, b):
    """
    Name:     func_line
    Inputs:   - numpy.ndarray, x values (x)
              - float, slope (a)
              - float, intercept (b)
    Returns   numpy.ndarray, y values
    Features: Returns y = ax + b
    """
    return float(a)*x + float(b)


def func_poly(x, a, b, c):
    """
    Name:     func_poly
    Inputs:   - numpy.ndarray, x values (x)
              - float, coeff (a)
              - float, coeff (b)
              - float, const (c)
    Outputs:  numpy.ndarray, y values
    Features: Returns y = ax^2 + bx + c
    """
    return float(a)*numpy.power(x, 2.) + float(b)*x + float(c)
