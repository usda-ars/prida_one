#!/usr/bin/python
#
# data_worker.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software is freely available to the public for use.      #
# The Department of Agriculture (USDA) and the U.S. Government have not       #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     USDA-Agricultural Research Service                                      #
#     Robert W. Holley Center for Agriculture and Health                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os

import numpy
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QMutexLocker
from PyQt5.QtCore import QObject

from . import __version__
from .hdf_organizer import PridaHDF
from .imutil import imopen
from .utilities import get_ctime
from .utilities import init_photo_attrs
from .utilities import init_pid_attrs
from .utilities import init_session_attrs
from .utilities import read_exif_tags


###############################################################################
# CLASSES:
###############################################################################
class DataWorker(QObject):
    """
    Name:     DataWorker
    Features: This class is a worker object used for threading data management.
    History:  Version 1.5.0
              - removed scipy.misc dependency [16.07.25]
              - added file path as compress file function argument [16.08.03]
              - upgraded logging level for changed attributes [16.09.15]
              - reimplement PridaHDF's imread convenience function [16.10.26]
              - added compression level property [16.11.30]
              - added try catches to setter functions [16.11.30]
              - created socket for configuring compression level [17.01.03]
              - created HDF5 file directory property [17.01.05]
              - added options to extract to file function [17.01.05]
              - created photo attrs dictionary [17.01.10]
              - updated import function for ctime [17.01.10]
              - created photo attrs signal / socket [17.01.11]
              - updated import images function [17.01.11]
                * failed to read image height/width correctly for .NEF's
                * new get ctime function returns string
              - updated save image function [17.01.11]
                * saves attributes to photo dataset
              - added index to imaging tuple and dataset attr [17.01.12]
              - created get tags signal / socket [17.01.12]
              - updated datasets listed and to analysis signals [17.01.18]
              - updated get analysis vals function [17.01.18]
              - updated get dataset session function name [17.02.24]
              - added get image orient in get dataset functin [17.02.24]
              - add software version to new HDF5 files [17.02.24]
              - created set and check file version functions [17.02.24]
              - updated default about return [17.03.16]
                * NOTE: to all 'if self.hdf is not None' checks, consider
                  adding: 'and self.hdf.isopen'
              - created is open property [17.03.16]
              - updated get photo attrs function [17.03.17]
              - updated check file version function [17.03.17]
              - added check for Nonetype qt vals in session attrs [17.03.21]
              - updated to imopen [17.03.29]
              - created signal/socket for session image crop extents [17.03.29]
              - update current session path with new user selection [17.03.29]
              - removed signal for crop extents---not needed [17.03.30]
              - save image now takes a dictionary of image metadata [17.04.04]
              - import image now uses image metadata dictionary [17.04.04]
              - left QMutexLocker in save image (is it needed?) [17.04.04]
              - created extract datasets function w/ update msgs [17.04.05]
              - check image shapes now includes progress message [17.04.07]
              - removed check image count from open file [17.06.02]
              - created check file function and signal [17.06.02]
              - created sockets for preview attrs, tags and thumbs [17.06.08]
    TODO:     _ add a image attribute checker to fix bad attrs
                (similar to the number of images checker)
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    addImageThumb = pyqtSignal(int, str, numpy.ndarray)  # thumbnail creation
    addThumb = pyqtSignal(int, int, str, numpy.ndarray, int)  # for thumbnails
    checkedImages = pyqtSignal(list, bool)   # for image shape checking
    datasetsListed = pyqtSignal(str, list)   # list of session's datasets
    dataListed = pyqtSignal(list)  # for updating Prida's data list
    donebusy = pyqtSignal()        # for ending file IO
    fieldSet = pyqtSignal(tuple)   # for setting Qt fields
    importDone = pyqtSignal()      # to end importing
    finished = pyqtSignal()        # to end thread
    fileChecked = pyqtSignal()     # completed file checks
    fileClosed = pyqtSignal()      # file is closed
    fileOpened = pyqtSignal(int)   # confirmation of file open
    idaSet = pyqtSignal(numpy.ndarray, int)   # oriented IDA VIEWER image
    importing = pyqtSignal(int, dict, bool)  # import images - update GUI
    isbusy = pyqtSignal(str)      # for starting file IO
    photoAttrs = pyqtSignal(dict)  # standard photo attributes
    report = pyqtSignal(str)      # for error messages to Prida app
    searchAdded = pyqtSignal(dict, int, int)  # PID attributes for search file
    searchRemoved = pyqtSignal(list)  # HDF5 file & PID key list
    sessionAttrs = pyqtSignal(dict)   # current session attributes
    setImg = pyqtSignal(numpy.ndarray, int)   # oriented image for processing
    tagsGotten = pyqtSignal(dict)  # EXIF tags for image group
    toAnalysis = pyqtSignal(dict, int)  # info for analysis view

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None, mutex=None):
        """
        Name:     DataWorker.__init__
        Inputs:   [optional] Qt parent object (parent)
        Returns:  None.
        Features: Class initialization
        """
        super(DataWorker, self).__init__(parent)

        # Create a worker logger:
        self.logger = logging.getLogger(__name__)
        self.logger.debug("data worker object initialized")

        # Initialize variables
        self.hdf = None         # PridaHDF object
        self.mutex = mutex      # mutex lock
        self.session = None     # current session path
        self.output_dir = None  # where exports should be saved
        self.pid_attrs = init_pid_attrs()
        self.session_attrs = init_session_attrs()
        self.photo_attrs = init_photo_attrs()  # do I need this?

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def about(self):
        if self.hdf is not None:
            return self.hdf.get_about()
        else:
            return dict()

    @about.setter
    def about(self, value):  # lint:ok
        if self.hdf is not None:
            self.hdf.set_root_about(value)
        else:
            self.report.emit("No HDF object created!")
            raise IOError("No HDF object created!")

    @property
    def basename(self):
        if self.hdf is not None:
            return self.hdf.basename()
        else:
            return ""

    @property
    def compression_lv(self):
        """HDF5 file compression level"""
        if self.hdf is not None:
            return self.hdf.c_level
        else:
            return None

    @compression_lv.setter
    def compression_lv(self, val):
        try:
            self.hdf.c_level = val
        except ValueError as err:
            self.report.emit(
                "Error! Failed to set compression level. {0}".format(err))
        except:
            self.report.emit("Error! Failed to set compression level.")
        else:
            self.logger.info("HDF compression level set to %d", val)
            self.report.emit("Compression level set to %s." % (val))

    @property
    def directory(self):
        """HDF5 file directory"""
        if self.hdf is not None:
            return self.hdf.dir
        else:
            return ''

    @directory.setter
    def directory(self, val):
        raise NotImplementedError("You cannot set this value in this way.")

    @property
    def is_open(self):
        """Boolean for HDF5 file status"""
        if self.hdf is not None:
            return self.hdf.isopen
        else:
            return False

    @is_open.setter
    def is_open(self, val):
        raise NotImplementedError("You cannot set this value in this way.")

    @property
    def pid_list(self):
        if self.hdf is not None and self.hdf.isopen:
            return self.hdf.list_pids()
        else:
            return []

    @property
    def root_user(self):
        if self.hdf is not None:
            return str(self.hdf.get_root_user())
        else:
            return ""

    @root_user.setter
    def root_user(self, value):  # lint:ok
        try:
            self.hdf.set_root_user(value)
        except IOError as err:
            self.report.emit("Error! {0}".format(err))
        except:
            self.report.emit("Error setting root user!")
        else:
            self.logger.info("Root user set to %s", value)

    @property
    def root_addr(self):
        if self.hdf is not None:
            return str(self.hdf.get_root_addr())
        else:
            return ""

    @root_addr.setter
    def root_addr(self, value):  # lint:ok
        try:
            self.hdf.set_root_addr(value)
        except IOError as err:
            self.report.emit("Error! {0}".format(err))
        except:
            self.report.emit("Error setting root address!")
        else:
            self.logger.info("Root address set to %s", value)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def autofill_pid(self, pid=''):
        """
        Name:     DataWorker.autofill_pid
        Inputs:   [optional] str, plant ID (pid)
        Outputs:  None.
        Features: Activated when PID field is entered; signals data to find PID
                  attributes for autofilling
        """
        self.logger.debug("performing PID auto-completion")
        obj_path = "/%s" % (pid)
        for i in self.pid_attrs:
            f_name = self.pid_attrs[i]["qt_val"]
            f_type = self.pid_attrs[i]["qt_type"]
            attr_name = self.pid_attrs[i]["key"]
            f_value = self.get_attr(attr_name, obj_path)
            self.fieldSet.emit((f_name, f_type, f_value))

    def check_file(self):
        """
        Name:     DataWorker.check_file
        Inputs:   None.
        Outputs:  None.
        Features: Perform file checks
        Depends:  check_image_count
        @TODO:    _ add image attribute checker (e.g., bad/missing time stamps
                    or shapes)
        """
        self.check_image_count()
        self.fileChecked.emit()

    def check_file_version(self):
        """
        Name:     DataWorker.check_file_version
        Inputs:   None.
        Outputs:  None.
        Features: Checks file version against software version for
                  compatability
        """
        if self.hdf is not None:
            fversion = self.hdf.version
            if fversion != self.hdf.UNDEFINED:
                fvp = [int(x.rstrip("-dev")) for x in fversion.split(".")]
                pvp = [int(x.rstrip("-dev")) for x in __version__.split(".")]
                if len(fvp) == len(pvp) and len(fvp) == 3:
                    if fvp[0] != pvp[0]:
                        self.report.emit(
                            ("Caution. "
                             "Your file was created with a different version "
                             "(%s) "
                             "of this software.") % (fversion))
                    elif fvp[1] != pvp[1]:
                        self.report.emit(
                            ("Caution. "
                             "Your file was created with a different revision "
                             "(%s) "
                             "of this software.") % (fversion))
                else:
                    self.logger.warning(
                        "File and/or software version parsing error.")
            else:
                self.report.emit(
                    "Caution. "
                    "Your file is not associated with a version "
                    "of this software.")
        else:
            self.logger.warning("HDF5 file is not open.")

    def check_image_count(self):
        """
        Name:     DataWorker.check_image_count
        Inputs:   None.
        Outputs:  None.
        Features: Dynamically assigns session image count field
        Depends:  - list_sessions
                  - set_attr
        """
        self.logger.info("checking image count")
        for pid in self.pid_list:
            for session in self.list_sessions(pid):
                s_path = "/%s/%s" % (pid, session)
                my_imgs = []
                for k, v in self.hdf.datasets.items():
                    if v == s_path:
                        my_imgs.append(k)
                my_imgs = sorted(my_imgs)
                num_imgs = len(my_imgs)
                # "Image Count" attribute by index
                # NOTE: this could cause trouble if session attrs ever change
                attr_name = self.session_attrs[5]["key"]
                try:
                    attr_val = self.get_attr(attr_name, s_path)
                    attr_val = int(attr_val)
                except:
                    self.logger.exception("could not cast %s to int", attr_val)
                    self.logger.critical(
                        "dynamically setting %s image count to %d" % (
                            s_path, num_imgs))
                    new_val = str(num_imgs)
                    self.set_attr(attr_name, new_val, s_path)
                else:
                    if attr_val != num_imgs:
                        self.logger.critical(
                            "dynamically setting %s image count to %d" % (
                                s_path, num_imgs))
                        new_val = str(num_imgs)
                        self.set_attr(attr_name, new_val, s_path)
                    else:
                        self.logger.debug("image count is correct")
        self.logger.info("image count check complete")

    def check_image_shapes(self, img_list):
        """
        Name:     DataWorker.check_image_shapes
        Inputs:   list, image list (img_list)
        Outputs:  bool, shape consistency check
        Features: Checks for shape consistency amongst images in a list of
                  image file paths
        Depends:  imopen
        """
        img_shape = None
        img_num = len(img_list)
        check = True
        for i in range(img_num):
            img_path = img_list[i]
            img = imopen(img_path)
            if i == 0:
                img_shape = img.shape
            else:
                if img.shape != img_shape:
                    check = False
            s_str = "Validating import images (%d/%d) ..." % (i+1, img_num)
            self.isbusy.emit(s_str)
        self.logger.debug("consistency check %s", check)
        self.checkedImages.emit(img_list, check)

    def close_file(self):
        """
        Name:     DataWorker.close_file
        Inputs:   None.
        Outputs:  None.
        Features: Closes HDF file; emits data closed signal
        """
        self.logger.debug("closing HDF file")
        self.hdf.close()
        self.logger.info("signaling GUI that file is closed")
        self.fileClosed.emit()

    def close_search_file(self):
        """
        Name:     DataWorker.close_search_file
        Inputs:   None.
        Outputs:  None.
        Features: Closes HDF file during inter-file searching
        """
        self.logger.debug("closing HDF file")
        self.hdf.close()

    def compress_file(self, cfile):
        """
        Name:     DataWorker.compress_file
        Inputs:   str, compressed file path (cfile)
        Outputs:  isbusy, donebusy and report signals
        Features: Creates a compressed copy of the currently opened HDF file
                  and emits the appropriate message for GUI popup
        NOTE:     compression does not update the new compressed file's root
                  version attribute
        """
        # Alert Prida that we have started compressing
        self.isbusy.emit("Compressing file...")

        err_msg = ""
        if self.hdf.isopen:
            err_code = self.hdf.compress(cfile)
            if err_code == 0:
                err_msg = "Successfully created compressed HDF file."
            elif err_code == 1:
                err_msg = ("Failed to create compressed HDF file. Check that "
                           "you have write privileges and try again.")
            elif err_code == -1:
                err_msg = ("An error was encountered during writing to the "
                           "compressed HDF file. Try re-opening this file and "
                           "running compression again.")
            elif err_code == 9999:
                err_msg = ("Failed to create compressed HDF file. The file "
                           "already exists. Please rename the existing file "
                           "and try again.")
            else:
                err_msg = ("An unknown error was encountered during "
                           "compression. Try re-opening this file and running "
                           "the compression again.")
        else:
            err_msg = ("Failed to create compressed HDF file. No open HDF "
                       "file was found. Please open an existing HDF file and "
                       "try again.")

        self.donebusy.emit()
        self.report.emit(err_msg)

    def create_hdf(self, my_parser=None, config_filepath=None):
        """
        Name:     DataWorker.create_hdf
        Inputs:   - [optional] function handle (my_parser)
                  - [optional] str, configuration file path (config_filepath)
        Outputs:  None.
        Features: Creates an PridaHDF class instance
        """
        self.logger.debug("creating HDF object")
        self.hdf = PridaHDF(my_parser, config_filepath)

    def create_session(self, pid, pid_attrs, session_attrs):
        """
        Name:     DataWorker.create_session
        Inputs:   - str, plant ID (pid)
                  - dict, plant ID metadata (pid_attrs)
                  - dict, session metadata (session_attrs)
        Outputs:  None.
        Features: Creates a new project session for a given PID, saves the
                  PID and session metadata and saves/returns the session path
        """
        self.logger.info("caught create new session signal")
        self.session = self.hdf.create_session(pid, pid_attrs, session_attrs)

    def extract_datasets(self, group_path, out_dir, opt_dict=None):
        """
        Name:     DataWorker.extract_datasets
        Inputs:   - str, HDF5 file group path (group_path)
                  - str, output directory (out_dir)
                  - [optional] dict, exporting options (opt_dict)
        Outputs:  str, error message
        Features: Extracts datasets below a given HDF level
        """
        try:
            ds_list = self.hdf.get_dataset_list(group_path)
            ds_num = len(ds_list)
            for i in range(ds_num):
                ds_path = ds_list[i]
                try:
                    self.hdf.extract_ds(ds_path, out_dir, opt_dict)
                except:
                    self.logger.error(
                        "failed to extract %s to %s" % (ds_path, out_dir))
                else:
                    s_str = "Extracting to file (%d/%d)..." % (i+1, ds_num)
                    self.isbusy.emit(s_str)
        except IOError:
            self.logger.warning("failed to extract datasets")
            err_msg = ("Failed to extract datasets to %s. "
                       "Please check output directory privileges "
                       "and try again.") % (out_dir)
        except:
            self.logger.exception("caught unknown error exception")
            err_msg = ("An unexpected error occurred during dataset "
                       "extraction. Please check that output "
                       "directory %s exists and that you have write "
                       "privileges.") % (out_dir)
        else:
            self.logger.debug("datasets extraction successful")
            err_msg = ("Successfully extracted datasets to %s") % (out_dir)

        return err_msg

    def extract_to_file(self, group_path, out_dir, opt_dict=None):
        """
        Name:     DataWorker.extract_to_file
        Inputs:   - str, HDF5 file group path (group_path)
                  - str, output directory (out_dir)
                  - [optional] dict, exporting options (opt_dict)
        Outputs:  isbusy, donebusy, and report signals
        Features: Extracts datasets and metadata at and below a given HDF level
        Depends:  extract_datasets
        """
        self.isbusy.emit("Extracting to file...")
        err_msg = ''

        if os.path.isdir(out_dir):
            self.output_dir = out_dir

            # Extract attribute files:
            try:
                self.logger.info("extracting attributes to %s", out_dir)
                self.hdf.extract_attrs(out_dir)
            except IOError:
                self.logger.warning(
                    "failed to export attributes to %s", out_dir)
            except ValueError:
                self.logger.warning("failed to export attributes; file exists")
            except:
                self.logger.exception("caught unknown error exception")
            else:
                self.logger.info("attributes extraction successful")

            # Extract datasets:
            err_msg = self.extract_datasets(group_path, out_dir, opt_dict)
        else:
            err_msg = "Failed to extract, output directory does not exist!"

        self.donebusy.emit()
        self.report.emit(err_msg)

    def get_analysis_vals(self, s_path):
        """
        Name:     DataWorker.get_analysis_vals
        Inputs:   str, session path (s_path)
        Outputs:  - dict, session attributes
                  - list, session image paths
        Signals   - toAnalysis (to Prida)
                  - datasetsListed (to ImageWorker)
        Features: Catches for analysis signal, finds session attributes and
                  list of images; emits to analysis signal
        """
        self.logger.info("caught for analysis signal")

        # Gathering session attributes:
        for i in self.session_attrs:
            my_attr = self.get_attr(self.session_attrs[i]['key'], s_path)
            self.session_attrs[i]['cur_val'] = my_attr

        # Gathering list of session's datasets:
        my_imgs = self.hdf.get_dataset_list(s_path)

        self.logger.info("sending to analysis signal")
        self.toAnalysis.emit(self.session_attrs, len(my_imgs))
        self.datasetsListed.emit(s_path, my_imgs)

    def get_attr(self, attr, obj_path):
        """
        Name:     DataWorker.get_attr
        Inputs:   - str, attribute name (attr)
                  - str, object path (obj_path)
        Outputs:  str, attribute value
        Features: Returns a requested attribute from a given object
        """
        if self.hdf is not None:
            return self.hdf.get_attr(attr, obj_path)

    def get_data_list(self):
        """
        Name:     DataWorker.get_data_list
        Inputs:   None.
        Outputs:  None.
        Features: Reads an HDF5 file's PID attributes to dictionary and emits
                  the data listed signal
        Depends:  list_sessions
        """
        self.logger.debug("caught get data list signal")
        data_list = []

        self.logger.debug("reading PID attributes")
        for pid in self.pid_list:
            p_path = "/%s" % (pid)
            d = {}
            d["pid"] = pid
            d["sessions"] = self.list_sessions(pid)
            for i in self.pid_attrs.keys():
                my_attr = self.pid_attrs[i]["key"]
                d[my_attr] = self.get_attr(my_attr, p_path)
            data_list.append(d)
        self.logger.info("sending data listed signal")
        self.dataListed.emit(data_list)

    def get_dataset(self, ds_path):
        """
        Name:     DataWorker.get_dataset
        Inputs:   str, dataset path (ds_path)
        Outputs:  ndarray, dataset
        Features: Returns a dataset (i.e., array) for a given dataset path;
                  handles orientation
        Depends:  - get_image_session
                  - get_attr
        """
        self.logger.debug("gathering dataset %s", ds_path)
        ndarray_full = self.hdf.get_dataset(ds_path)
        if ndarray_full.size != 0:
            img_orient = self.hdf.get_ds_orient(ds_path)

            if img_orient and img_orient != "Original":
                if img_orient == "Rotated Clockwise":
                    self.logger.debug("rotating image clockwise")
                    ndarray_full = numpy.rot90(ndarray_full, 3)
                elif img_orient == "Rotated Counter-Clockwise":
                    self.logger.debug("rotating image counter-clockwise")
                    ndarray_full = numpy.rot90(ndarray_full)
                else:
                    self.logger.warning(
                        "failed to match orientation %s", img_orient)
        else:
            self.logger.debug("returning empty dataset")

        return ndarray_full

    def get_ida(self, img_path, enum):
        """
        Name:     DataWorker.get_ida
        Inputs:   - str, image path (img_path)
                  - int, view enumerator (enum)
        Outputs:  None.
        Features: Catches get IDA signal, gathers dataset, and signals array
                  back to GUI
        Depends:  get_dataset
        """
        self.logger.debug("caught get IDA signal")
        self.logger.debug("gathering image data for selection")
        ndarray_full = self.get_dataset(img_path)
        self.idaSet.emit(ndarray_full, enum)

    def get_image_session(self, img_path):
        """
        Name:     DataWorker.get_image_session
        Inputs:   str, image path (img_path)
        Outputs:  str, session path
        Features: Returns the session name for a given image path
        """
        return self.hdf.get_ds_session(img_path)

    def get_preview_attrs(self, photo_path, enum):
        """
        Name:     DataWorker.get_preview_attrs
        Inputs:   - str, directory path to photo (photo_path)
                  - int, view enumerator (enum)
        Outputs:  None.
        Features: Reads a preview image from file and signals image attributes
                  back to the GUI
        Depends:  - imopen
                  - init_photo_attrs
                  - get_ctime
        """
        self.logger.debug("Retrieving photo attributes")
        preview_attrs = init_photo_attrs()

        try:
            pim = imopen(photo_path)
        except:
            self.logger.exception("Failed to open preview image")
            self.report.emit("Failed to open preview image!")
        else:
            ds_name, ds_format = os.path.splitext(os.path.basename(photo_path))
            ds_date = get_ctime(photo_path)
            ds_orient = "Original"

            pim_dim = pim.shape
            num_dim = len(pim_dim)
            if num_dim == 3:
                ds_color = "RGB"
                height, width, _ = pim_dim
            elif num_dim == 2:
                ds_color = "Grayscale"
                height, width = pim_dim
                # NOTE: no support for binary preview images!
            else:
                ds_color = "Undefined"
                height = "Undefined"
                width = "Undefined"

            for i in preview_attrs:
                attr_key = self.photo_attrs[i]['key']
                if attr_key == "Height":
                    my_attr = height
                elif attr_key == "Width":
                    my_attr = width
                elif attr_key == "Color":
                    my_attr = ds_color
                elif attr_key == "Format":
                    my_attr = ds_format
                elif attr_key == "File":
                    my_attr = ds_name
                elif attr_key == "Orientation":
                    my_attr = ds_orient
                elif attr_key == "Date":
                    my_attr = ds_date
                else:
                    my_attr = "Undefined"
                preview_attrs[i]['val'] = my_attr

            self.photoAttrs.emit(preview_attrs)
            self.idaSet.emit(pim, enum)

    def get_preview_tags(self, photo_path):
        """
        Name:     DataWorker.get_preview_tags
        Inputs:   str, photo path (photo_path)
        Outputs:  None.
        Features: Signals EXIF tags for a given photo back to GUI
        Depends:  read_exif_tags
        """
        img_tags = read_exif_tags(photo_path)
        self.tagsGotten.emit(img_tags)

    def get_preview_thumb(self, photo_path, enum):
        """
        Name:     DataWorker.get_preview_thumb
        Inputs:   - str, photo path (photo_path)
                  - int, view enumerator (enum)
        Outputs:  None.
        Features: Signals preview photo thumbnail back to GUI
        Depends:  imopen
        """
        try:
            pim = imopen(photo_path)
            thumb = self.hdf.make_thumb(pim)
        except:
            self.logger.exception("Failed to get preview thumbnail")
            self.report.emit("Failed to get preview thumbnail!")
        else:
            self.addThumb.emit(9999, 9999, photo_path, thumb, enum)

    def get_process_img(self, img_path, img_count):
        """
        Name:     DataWorker.get_process_img
        Inputs:   - str, image path (img_path)
                  - int, image counter (img_count)
        Outputs:  None.
        Features: Catches signal, gathers dataset, and signals array
                  back for analysis
        Depends:  get_dataset
        """
        self.logger.info("caught signal for image %d", img_count)
        ndarray_full = self.get_dataset(img_path)
        self.logger.info("return signal for image %s", img_path)
        self.setImg.emit(ndarray_full, img_count)

    def get_session_crop_extents(self):
        """
        ~~~ UNUSED ~~~
        Name:     DataWorker.get_session_crop_extents
        Inputs:   None.
        Outputs:  None.
        Features: Returns a signal with a dictionary of image height and width,
                  corrected for user orientation preference
        """
        if self.session is not None:
            ds_list = self.hdf.get_dataset_list(self.session)
            ds_num = len(ds_list)
            self.logger.debug("Found %d datasets", ds_num)
            crop_extents = {'height': 0, 'width': 0}
            if ds_num > 0:
                ds_path = ds_list[0]
                ds_orient = self.hdf.get_ds_orient(ds_path)
                ds_shape = self.hdf.ds_shape(ds_path)
                ds_dim = len(ds_shape)
                if ds_dim == 2:
                    if ds_orient == "Original":
                        height, width = ds_shape
                    else:
                        width, height = ds_shape
                    crop_extents['height'] = height
                    crop_extents['width'] = width
                elif ds_dim == 3:
                    if ds_orient == "Original":
                        height, width, _ = ds_shape
                    else:
                        width, height, _ = ds_shape
                    crop_extents['height'] = height
                    crop_extents['width'] = width

    def get_tags(self, img_path):
        """
        Name:     DataWorker.get_tags
        Inputs:   str, image path (img_path)
        Outputs:  signal, tagsGotten
        Features: Socket for Prida's getTags signal; reads attributes from the
                  image path's parent group and signals the dictionary back to
                  the GUI
        """
        img_dir = os.path.dirname(img_path)
        img_tags = self.hdf.get_object_attrs(img_dir)
        self.tagsGotten.emit(img_tags)

    def get_thumbs(self, s_path, enum):
        """
        Name:     DataWorker.get_thumbs
        Inputs:   - str, session path (img_path)
                  - int, view enumerator (enum)
        Outputs:  None.
        Features: Signals arrays of session thumbs to the GUI based on the
                  enum value
        """
        self.logger.debug("searching session for images")
        self.hdf.find_datasets()
        my_imgs = []
        for k, v in self.hdf.datasets.items():
            if v == s_path:
                my_imgs.append(k)
        my_imgs = sorted(my_imgs)
        my_img_names = [
            os.path.splitext(os.path.basename(i))[0] for i in my_imgs]
        num_imgs = len(my_imgs)

        self.logger.debug("searching session for thumbnails")
        self.hdf.find_thumbnails()
        my_thumbs = []
        for k, v in self.hdf.thumbnails.items():
            if v == s_path:
                my_thumbs.append(k)
        my_thumbs = sorted(my_thumbs)
        my_thumb_names = [
            os.path.splitext(os.path.basename(i))[0] for i in my_thumbs]

        # Fix problem where spinner does not stop when no images are available:
        if num_imgs == 0:
            self.addThumb.emit(0, 0, "", numpy.array([]), enum)

        for i in range(num_imgs):
            img_name = my_img_names[i]
            img_path = my_imgs[i]
            if img_name in my_thumb_names:
                j = my_thumb_names.index(img_name)
                thumb_path = my_thumbs[j]
                thumb = self.get_dataset(thumb_path)
            else:
                ndarray_full = self.get_dataset(img_path)
                try:
                    thumb = self.hdf.make_thumb(ndarray_full)
                except IOError:
                    self.logger.exception("thumbnail scaling failed")
                    thumb = numpy.zeros((250, 250, 3))
                except:
                    self.logger.exception("encountered unknown error")
                    thumb = numpy.zeros((250, 250, 3))

            # Add binary image support:
            if thumb.max() == 1 and len(thumb.shape) == 2:
                self.logger.debug("found binary thumb image!")
                self.logger.debug("scaling binary to grayscale")
                thumb[numpy.where(thumb == 1)] *= 255
                self.logger.debug("scaling complete")

            self.logger.debug(
                "signaling image thumb %d/%d" % (i + 1, num_imgs))
            self.addThumb.emit(i + 1, num_imgs, img_path, thumb, enum)

    def get_input_field(self, obj_path, attr_name, f_name, f_type):
        """
        Name:     DataWorker.get_input_field
        Inputs:   - str, object path (obj_path)
                  - str, attribute name (attr_name)
                  - str, Qt field name (f_name)
                  - str, Qt field type (f_type)
        Outputs:  None.
        Features: Emits signal if object exists with appropriate field
                  attribute
        Depends:  get_attr
        """
        if obj_path in self.hdf.hdfile:
            self.logger.debug(
                "gathering attribute for %s", attr_name)
            f_value = self.get_attr(attr_name, obj_path)
            self.fieldSet.emit((f_name, f_type, f_value))
        else:
            self.logger.warning("%s does not exist", obj_path)

    def get_photo_attrs(self, ds_path):
        """
        Name:     DataWorker.get_photo_attrs
        Inputs:   str, image group path (group_path)
        Outputs:  None.
        Features: Receives the get photo attributes signal; sets the current
                  values for photo dataset attributes; signals the dictionary
                  back to the GUI
        """
        self.logger.debug("Retrieving photo attributes")
        ds_name, ds_format = os.path.splitext(os.path.basename(ds_path))
        if self.hdf.ds_is_rgb(ds_path):
            ds_color = "RGB"
            height, width, _ = self.hdf.ds_shape(ds_path)
        elif self.hdf.ds_is_grayscale(ds_path):
            ds_color = "Grayscale"
            height, width = self.hdf.ds_shape(ds_path)
            if self.hdf.ds_is_binary(ds_path):
                ds_color = "Binary"
        else:
            height = "Undefined"
            width = "Undefined"
            ds_color = "Undefined"

        for i in self.photo_attrs:
            attr_key = self.photo_attrs[i]['key']
            my_attr = self.get_attr(attr_key, ds_path)
            if my_attr == self.hdf.UNDEFINED:
                if attr_key == "Height":
                    my_attr = height
                elif attr_key == "Width":
                    my_attr = width
                elif attr_key == "Color":
                    my_attr = ds_color
                elif attr_key == "Format":
                    my_attr = ds_format
                elif attr_key == "File":
                    my_attr = ds_name
                elif attr_key == "Orientation":
                    my_attr = self.hdf.get_ds_orient(ds_path)
                else:
                    my_attr = "Undefined"
            self.photo_attrs[i]['val'] = my_attr
        self.photoAttrs.emit(self.photo_attrs)

    def get_session_attrs(self, s_path):
        """
        Name:     DataWorker.get_session_attrs
        Inputs:   str, session path (s_path)
        Outputs:  None.
        Features: Receives the get session attributes signal based on new user
                  selection; sets the current session path and values for all
                  session attributes; signals the dictionary back to the GUI
        """
        # Update the current session based on the user's selection:
        self.session = s_path

        # Update current session attributes and signal back the dictionary:
        for i in self.session_attrs:
            my_attr = self.get_attr(self.session_attrs[i]['key'], s_path)
            self.session_attrs[i]['cur_val'] = my_attr
        self.sessionAttrs.emit(self.session_attrs)

    def import_images(self, img_files):
        """
        Name:     DataWorker.import_images
        Inputs:   list, image files (img_files)
        Outputs:  None.
        Features: Imports images to current session; signals importing to Prida
                  to update the GUI and import done when complete.
        Depends:  - get_ctime
                  - save_image
        """
        if len(img_files) > 0:
            for file_idx in range(len(img_files)):
                file_path = img_files[file_idx]
                file_date = get_ctime(file_path)  # YYYY-MM-DD HH:MM:SS

                img_meta = {}
                img_meta['ready'] = True
                img_meta['path'] = file_path
                img_meta['datetime'] = file_date
                img_meta['angle'] = None
                img_meta['index'] = file_idx

                try:
                    self.logger.debug("saving image %s", file_path)
                    self.save_image(img_meta)
                except:
                    self.logger.exception(
                        "failed to save dataset %s", self.session)
                else:
                    self.importing.emit(file_idx, img_meta, True)
                    self.logger.debug("saved image %s", file_path)
            self.importDone.emit()

    def list_objects(self, obj_path):
        """
        Name:     DataWorker.list_objects
        Inputs:   str, object path (obj_path)
        Outputs:  list, member object names
        Features: Returns a list of member objects for a given object path
        """
        return self.hdf.list_objects(obj_path)

    def list_sessions(self, pid):
        """
        Name:     DataWorker.list_sessions
        Inputs:   str, plant ID (pid)
        Outputs:  list, session names
        Features: Returns a list of sessions for a given plant ID
        """
        return self.hdf.list_sessions(pid)

    def new_file(self, path):
        """
        Name:     DataWorker.new_file
        Inputs:   str, HDF5 file path
        Outputs:  None.
        Features: Creates a new HDF file; emits file created or failed signal
        Depends:  - save_file
                  - set_file_version
        """
        self.logger.debug("caught new file signal")
        try:
            self.hdf.open_file(path)
            self.save_file()
        except ValueError:
            self.logger.exception("failed to unpack values")
            self.fileOpened.emit(1)
        except:
            self.logger.exception("encountered unexpected error")
            self.fileOpened.emit(1)
        else:
            # Initialize root attributes:
            self.root_user = ""
            self.root_addr = ""
            self.about = ""
            self.set_file_version()
            self.logger.debug("sending new file opened signal")
            self.fileOpened.emit(0)

    def open_file(self, file_path):
        """
        Name:     DataWorker.open_file
        Inputs:   str, file path (file_path)
        Outputs:  None.
        Features: Opens a new HDF file; saves root user and address to
                  defaults; and emits existing file opened signal
        Depends:  check_file_version
        """
        self.logger.debug("opening HDF file %s", file_path)
        self.hdf.close()
        self.hdf.open_file(file_path)
        self.check_file_version()
        self.session_attrs[0]['def_val'] = self.root_user
        self.session_attrs[1]['def_val'] = self.root_addr
        self.fileOpened.emit(-1)

    def open_search_file(self, file_path):
        """
        Name:     DataWorker.open_search_file
        Inputs:   str, file path (file_path)
        Outputs:  None.
        Features: Opens an existing HDF file
        """
        self.logger.debug("opening HDF file %s", file_path)
        self.hdf.close()
        self.hdf.open_file(file_path)

    def quit(self):
        """
        Name:     DataWorker.quit
        Inputs:   None.
        Outputs:  None.
        Features: Returns the finished signal
        """
        self.finished.emit()

    def save_file(self):
        """
        Name:     DataWorker.save_file
        Inputs:   None.
        Outputs:  None.
        Features: Saves/flushes HDF data to file
        """
        self.logger.debug("saving HDF file")
        self.hdf.save()

    def save_image(self, t):
        """
        Name:     DataWorker.save_image
        Inputs:   dict, image metadata (t)
        Outputs:  None.
        Features: Saves photo and dataset attributes to HDF
        """
        self.logger.debug("gathering image metadata")
        # NOTE: these are defined in Imaging.image function
        img_path = t['path']
        anglestamp = t.get('angle', None)
        img_idx = t.get('index', None)
        dtstamp = t.get('datetime', None)
        dtstamp = dtstamp.replace("_", " ").replace(".", ":")

        basename = os.path.basename(img_path)
        name, fext = os.path.splitext(basename)
        a_path = os.path.join(self.session, name, basename)
        if os.name is 'nt':
            a_path = a_path.replace(os.sep, '/')

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~
        #      MUTEX LOCK
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~
        if self.mutex is not None:
            # QMutexLocker releases the lock when exiting function scope
            self.logger.debug("creating a mutex locker")
            locker = QMutexLocker(self.mutex)

        try:
            self.logger.info("saving image %s to HDF file", img_path)
            self.hdf.save_image(self.session, img_path)
        except:
            self.logger.exception("failed to save image %s", img_path)
        else:
            try:
                self.logger.debug("saving %s attributes", self.session)
                self.set_attr("File", basename, a_path)
                self.set_attr("Format", fext, a_path)
                self.set_attr("Date", dtstamp, a_path)
                self.set_attr("Angle", str(anglestamp), a_path)
                self.set_attr("Index", str(img_idx), a_path)
            except:
                self.logger.exception(
                    "failed to set %s attributes", self.session)
        finally:
            self.logger.debug("flushing to HDF file")
            self.save_file()

    def search_file_addition(self, file_path, cur_idx, max_idx):
        """
        Name:     DataWorker.search_file_addition
        Inputs:   - str, HDF5 file path (file_path)
                  - int, current index value (cur_idx)
                  - int, maximum index value (max_idx)
        Outputs:  None.
        Features: Reads an HDF5 file's PID attributes to dictionary and emits
                  the search file signal
        """
        f_name = os.path.basename(file_path)
        f_name = os.path.splitext(f_name)[0]

        self.logger.debug("opening search file")
        self.open_search_file(file_path)

        self.logger.debug("reading search file PIDs")
        search_data = {}
        for pid in self.pid_list:
            # Create a unique dictionary key for each file's PIDs:
            my_key = "%s.%s" % (f_name, pid)

            my_data = [f_name, pid]
            my_path = os.path.join('/', str(pid))
            for i in self.pid_attrs:
                my_attr = self.pid_attrs[i]["key"]
                my_attr_val = self.get_attr(my_attr, my_path)
                self.logger.debug(
                    "adding PID attribute %s = %s" % (
                        my_attr, my_attr_val))
                my_data.append(my_attr_val)

            # Save search file's PID attributes:
            search_data[my_key] = my_data

        self.logger.debug("closing search file")
        self.hdf.close()

        self.logger.debug("emitting search file signal")
        self.searchAdded.emit(search_data, cur_idx, max_idx)

    def search_file_removal(self, file_path):
        """
        Name:     DataWorker.search_file_removal
        Inputs:   str, HDF5 file path (file_path)
        Outputs:  None.
        Features: Creates a list of HDF5 file name and PID keys and signals
                  the remove search file signal
        """
        f_name = os.path.basename(file_path)
        f_name = os.path.splitext(f_name)[0]

        self.logger.debug("opening selected file")
        self.open_search_file(file_path)

        key_list = []
        for pid in self.pid_list:
            my_key = "%s.%s" % (f_name, pid)
            key_list.append(my_key)

        self.logger.debug("closing selected file")
        self.hdf.close()
        self.searchRemoved.emit(key_list)

    def set_attr(self, attr_name, attr_val, obj_path):
        """
        Name:     DataWorker.set_attr
        Inputs:   - str, attribute name (attr_name)
                  - str, attribute value (attr_val)
                  - str, HDF object path (obj_path)
        Outputs:  None.
        Features: Saves a value to a given attribute of a given object
        """
        self.logger.debug("setting attribute %s", attr_name)
        try:
            self.hdf.set_attr(attr_name, attr_val, obj_path)
        except:
            self.logger.exception(
                "could not set attribute %s", attr_name)
        else:
            self.hdf.save()

    def set_clevel(self, clevel):
        """
        Name:     DataWorker.set_clevel
        Inputs:   int, compression level (clevel)
        Outputs:  None.
        Features: Socket for configuring the compression level
        """
        self.logger.debug("Caught setDataLV signal")
        self.compression_lv = clevel

    def set_file_version(self):
        """
        Name:     DataWorker.set_file_version
        Inputs:   None.
        Outputs:  None.
        Features: Sets the software version as a root attribute
        Depends:  set_attr
        """
        if self.hdf is not None and self.hdf.isopen:
            self.set_attr("version", __version__, "/")

    def set_pid_fields(self, pid_path):
        """
        Name:     DataWorker.set_pid_fields
        Inputs:   str, PID path (pid_path)
        Outputs:  None.
        Features: Signals PID attribute values back to the GUI
        """
        for i in self.pid_attrs:
            f_name = self.pid_attrs[i]["qt_val"]
            f_type = self.pid_attrs[i]["qt_type"]
            attr_name = self.pid_attrs[i]["key"]
            f_value = self.get_attr(attr_name, pid_path)
            self.fieldSet.emit((f_name, f_type, f_value))

    def set_session_default(self, f_key, f_val):
        """
        Name:     DataWorker.set_session_default
        Inputs:   - str, session dictionary field key (f_key)
                  - str, session field default value (f_val)
        Outputs:  None.
        Features: Assigns a default value to session dictionary key
        """
        self.logger.debug(
            "searching for session index for key '%s'", f_key)
        k_index = [
            k for k, d in self.session_attrs.items() if d["key"] == f_key]
        if len(k_index) == 0:
            self.logger.error("session key '%s' not found", f_key)
            raise IndexError("No entry found for session key %s", f_key)
        elif len(k_index) == 1:
            i = k_index[0]
            self.logger.debug("found session index %d", i)
            self.logger.debug("setting default value to %s", f_val)
            self.session_attrs[i]["def_val"] = f_val
        else:
            self.logger.error(
                "unexpected number of session key matches for '%s'",
                f_key)
            raise IndexError(
                "Too many matches found for session key %s", f_key)

    def set_session_fields(self, s_path):
        """
        Name:     DataWorker.set_session_fields
        Inputs:   str, session path (s_path)
        Outputs:  None.
        Features: Signals session attribute values back to the GUI
        """
        for i in self.session_attrs:
            if self.session_attrs[i]['qt_val'] is not None:
                f_name = self.session_attrs[i]["qt_val"]
                f_type = self.session_attrs[i]["qt_type"]
                attr_name = self.session_attrs[i]["key"]
                f_value = self.get_attr(attr_name, s_path)
                self.fieldSet.emit((f_name, f_type, f_value))

    def stop(self):
        """
        Features: Stops this class for shutdown, resetting everything.
        """
        self.logger.info("caught stop signal; data worker class shutting down")
        self.hdf.save()
        self.hdf.close()
        self.session = None
        self.output_dir = None
        self.pid_attrs = {}


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    # Instantiating logging handler and record format:
    root_handler = logging.FileHandler("workers.log")
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)
