#!/usr/bin/python
#
# imaging.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import datetime
import logging
import os
import sys
import time

from .camera import Camera
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# FUNCTIONS:
###############################################################################
def exit_sequence(my_imaging):
    """
    Name:    Imaging.exit_sequence
    Feature: Disables all hardware before application close
    Inputs:  Imaging, the instance of Imaging to cleanup (my_imaging)
    Outputs: None
    """
    logging.info('start.')
    if my_imaging.driver in my_imaging.supported_drivers:
        my_imaging.turn_off_controller()
    if my_imaging.has_led:
        my_imaging.led_off()
    logging.info('complete.')


###############################################################################
# CLASSES:
###############################################################################
class Imaging(object):
    """
    Imaging, an aggregate imaging hardware class for PRIDA.

    Name:       Imaging
    History:    Version 1.5.0
                - added camera connect in load peripherals [16.11.02]
                - removed get settling time ms [16.11.02]
                - removed load peripherals from init [16.11.03]
                - separated peripheral loading functions [16.11.03]
                - removed step count [16.11.10]
                - created single step motor function [16.11.10]
                - fixed position calculation for step types [16.11.10]
                - added step motor for unit stepping [16.11.10]
                - added step to next for inter-photo stepping [16.11.10]
                - removed find end/next position functions [16.11.10]
                - irregular photo spacings now throw error [16.11.10]
                - created image function [16.11.10]
                - updated total rotation calculation in num photos [16.11.30]
                - added property for camera's path [16.12.01]
                - updated exception handling in load peripherals [16.12.02]
                - lowered error message level for Explorer mode [16.12.19]
                - fixed ordering problem with mode and driver config [16.12.19]
                - check camera connection before other peripherals [16.12.19]
                - mode case insensitive [16.12.19]
                - fixed load controller function for 2D mode [17.01.03]
                - created test photo function [17.01.03]
                - added motor speed property [17.01.04]
                - added motor step type property [17.01.04]
                - updated minimum settling time [17.01.04]
                - created controller deleter and unset function [17.01.04]
                - updated motor step degree setter [17.01.04]
                - added speed ramping option to step motor function [17.01.06]
                - added taken index to imaging tuple [17.01.12]
                - fixed if-statement for total steps [17.02.10]
                  * the single-image check is performed in steps per photo
                - fixed underscore issue in image name parsing [17.02.17]
                - updated motor calibration [17.03.27]
                - take photo returns filename and timestamp [17.04.04]
                - new image attribute dictionary [17.04.04]
                - fixed error w/ turn off motor and controller [17.04.05]
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    _controller = None  # Attribute name for object controlling the controller
    _camera = None      # Attribute name for object controlling the camera
    _led = None         # Attribute name for object controlling the led
    _piezo = None       # Attribute name for object controlling the piezo

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, my_parser=None, config_filepath=None):
        """
        Name:     Imaging.__init__
        Inputs:   - [optional] object, parser function (my_parser)
                  - [optional] str, configuration file path (config_filepath)
        Features: Initializes hardware states.
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('Instantiating imaging.')
        self.parser = my_parser
        self.config_file = config_filepath
        self._num_photos = 1         # total to collect
        self._taken = 0              # number of current photo
        self._position = 0           # current position of the motor in degrees
        self._is_running = False     # indicates if system is mid DAQ
        self._is_error = False       # indicates system error
        self._gear_ratio = 1         # 4.9 for use with RM101 setup
        self._cur_pid = None         # current PID being imaged
        self._end_pos = 0            # end positiom of the motor in usteps
        self._driver = None          # motor driver being implemented
        self._settling_time = 2.0    # settling time in seconds
        self._supported_drivers = {'HAT', 'Shield', 'Controller'}
        self._has_led = False        # led peripheral attached
        self._has_piezo = False      # piezo peripheral attached
        self._mode = 'Explorer'      # set default mode to Explorer

        # List attributes that can be set by the config file:
        self.attr_dict = {'GEAR_RATIO': 'gear_ratio',
                          'DRIVER': 'driver',
                          'HAS_LED': 'has_led',
                          'HAS_PIEZO': 'has_piezo',
                          'MODE': 'mode',
                          'SETTLING_TIME': 'settling_time',
                          }
        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('Imaging instantiated.')
        atexit.register(exit_sequence, self)

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def camera(self):
        """
        Camera class, a gphoto2 camera object abstraction (2D and 3D modes)
        """
        return self._camera

    @camera.setter
    def camera(self, value):  # lint:ok
        if self.mode in ('2D', '3D'):
            if isinstance(value, Camera()):
                self._camera = value
            else:
                self.logger.error('camera attribute must be a Camera object')
                raise TypeError('Camera attribute must be a Camera object.')
        else:
            self.logger.warning(
                'not in 2D or 3D mode! Cannot set camera attribute')
            self._camera = None

    @property
    def controller(self):
        """
        Controller, a stepper motor driver (3D mode)
        """
        return self._controller

    @controller.setter
    def controller(self, value):  # lint:ok
        if self.mode == '3D':
            if isinstance(value, eval('{}()'.format(self.driver))):
                self._controller = value
            else:
                self.logger.error(
                    '_controller attribute must be a '
                    '{} object'.format(self.driver))
                raise TypeError(
                    '_controller attribute must be a '
                    '{} object.'.format(self.driver))
        else:
            self.logger.warning(
                'cannot set controller attribute; not in 3D mode!')

    @controller.deleter
    def controller(self):
        if self._controller is not None:
            self.logger.info("unsetting current controller")
            self._controller.poweroff()
            setattr(Imaging, '_controller', None)

    @property
    def cur_pid(self):
        """
        The current plant identifier
        """
        return self._cur_pid

    @cur_pid.setter
    def cur_pid(self, value):  # lint:ok
        if isinstance(value, str):
            self._cur_pid = value
        else:
            self.logger.error('current pid must be a string')
            raise TypeError('Current pid must be a string.')

    @property
    def driver(self):
        """
        The driver name for the Controller
        """
        return self._driver

    @driver.setter
    def driver(self, val):  # lint:ok
        if isinstance(val, str):
            if val in self.supported_drivers:
                self.logger.info("driver set to %s", val)
                self._driver = val
            elif val is '':
                self.logger.warning('no driver set, disabling controller')
                self._driver = val
            else:
                self.logger.error("driver '{}' not supported".format(val))
                raise ValueError("Driver '{}' not supported.".format(val))
        else:
            self.logger.error('driver attribute must be of type string')
            raise TypeError('Driver attribute must be of type string.')

    @property
    def end_pos(self):
        """
        The end position (i.e., number of steps) for the motor in the current
        sequence
        """
        return self._end_pos

    @end_pos.setter
    def end_pos(self, value):  # lint:ok
        self.logger.error('setting the step end position is forbidden.')
        raise AttributeError('Setting the step end position is forbidden.')

    @property
    def gear_ratio(self):
        """
        The ratio of the bearing (or turn table) to the motor wheel diameters
        """
        return self._gear_ratio

    @gear_ratio.setter
    def gear_ratio(self, value):  # lint:ok
        if isinstance(value, (int, float)):
            if value >= 0:
                self.logger.info("gear ratio set to %s", value)
                self._gear_ratio = value
            else:
                self.logger.error('gear ratio must be a positive value')
                raise ValueError('Gear ratio must be a positive value.')
        else:
            self.logger.error('gear ratio must be a numeric value')
            raise TypeError('Gear ratio must be a numeric value.')

    @property
    def has_led(self):
        """
        Boolean property for LED attribute existence
        """
        return self._has_led

    @has_led.setter
    def has_led(self, val):  # lint:ok
        if isinstance(val, bool):
            self._has_led = val
        else:
            self.logger.error("'has led' attribute must be of type bool")
            raise TypeError("The 'has led' attribute must be of type bool.")

    @property
    def has_piezo(self):
        """
        Boolean property for Piezo attribute existence
        """
        return self._has_piezo

    @has_piezo.setter
    def has_piezo(self, val):  # lint:ok
        if isinstance(val, bool):
            self._has_piezo = val
        else:
            self.logger.error("'has piezo' attribute must be of type bool")
            raise TypeError("The 'has piezo' attribute must be of type bool.")

    @property
    def is_error(self):
        """
        Boolean representing system state, indicating whether or not the
        hardware controls have encountered an error
        """
        return self._is_error

    @is_error.setter
    def is_error(self, state):  # lint:ok
        if isinstance(state, bool):
            self._is_error = state
        else:
            self.logger.error('_is_error must be a boolean')
            raise TypeError('The _is_error property must be a boolean.')

    @property
    def is_running(self):
        """
        Boolean representing system state, indicating whether or not the
        system is currently in a imaging sequence
        """
        return self._is_running

    @is_running.setter
    def is_running(self, state):  # lint:ok
        if isinstance(state, bool):
            self._is_running = state
        else:
            self.logger.error('_is_running must be a boolean')
            raise TypeError('The _is_running property must be a boolean.')

    @property
    def led(self):
        """
        LED object representing a multi-colored LED that inherits from the
        Adafruit_MotorHAT class
        """
        if self.has_led:
            return self._led
        else:
            raise AttributeError('LED peripheral not configured.')

    @led.setter
    def led(self, value):  # lint:ok
        if self.has_led:
            if isinstance(value, LED()):  # lint:ok
                self._led = value
            else:
                self.logger.error('_led attribute must be a LED object')
                raise TypeError('The _led attribute must be a LED object.')
        else:
            raise AttributeError("LED peripheral not configured.")

    @property
    def mode(self):
        """
        Current imaging mode (Explorer, 2D or 3D)
        """
        return self._mode

    @mode.setter
    def mode(self, val):  # lint:ok
        if isinstance(val, str):
            if val.lower() == 'explorer':
                self._mode = 'Explorer'
            elif val.lower() == '2d':
                self._mode = '2D'
            elif val.lower() == '3d' and (
                    self.driver in self.supported_drivers
                    or self.driver is None):
                self._mode = '3D'
            elif val.lower() == '3d' and self.driver is not None:
                self.logger.error(
                    "driver missing or unsupported, cannot boot 3D mode")
                raise PRIDAPeripheralError(
                    msg=("Driver missing or unsupported; "
                         "please check your configuration."))
            else:
                self.logger.error(
                    "'{}' is not a valid imaging mode".format(val))
                raise PRIDAPeripheralError(
                    msg=("Imaging mode missing or unsupported; "
                         "please check your configuration."))
        else:
            self.logger.error('imaging mode must be of type string')
            raise PRIDAPeripheralError(
                msg=("Imaging mode not unsupported; "
                     "please check your configuration."))

    @property
    def num_photos(self):
        """
        Number of photos to be taken.
        """
        return self._num_photos

    @num_photos.setter
    def num_photos(self, value):  # lint:ok
        if isinstance(value, int):
            if value > 0:
                self._num_photos = value
            else:
                self.logger.error('number of photos must be a positive value')
                raise ValueError('Number of photos must be a positive value.')

            if self._num_photos > 1 and self.step_deg is not None:
                tot_rot = (self.steps_per_photo * self._num_photos *
                           self.step_deg / self.gear_ratio)
                if int(tot_rot + 0.5) < 360:
                    self.logger.warning(
                        ("current number of photos results in under-rotating "
                         "the bearing by %0.2f degrees") % (360 - tot_rot))
                    raise PRIDAPeripheralError(
                        msg=("Current number of photos results in "
                             "under-rotating the bearing by "
                             "%0.2f degrees.") % (360 - tot_rot))
                elif int(tot_rot) > 360 + self.step_deg:
                    self.logger.warning(
                        ("current number of photos results in over-rotating "
                         "the bearing by %0.2f degrees") % (tot_rot - 360))
                    raise PRIDAPeripheralError(
                        msg=("Current number of photos results in "
                             "over-rotating the bearing by "
                             "%0.2f degrees.") % (tot_rot - 360))
        else:
            self.logger.error('number of photos must be a integer')
            raise TypeError('Number of photos must be a integer.')

    @property
    def path(self):
        """Directory for saving camera images"""
        if self.camera is not None:
            return self.camera.path
        else:
            return ""

    @path.setter
    def path(self, val):
        if self.camera is not None:
            self.camera.path = val
        else:
            raise PRIDAPeripheralError(msg="No camera set!")

    @property
    def piezo(self):
        """
        Piezo object representing a piezo-buzzer that inherits from the
        Adafruit_MotorHAT class
        """
        if self.has_piezo:
            return self._piezo
        else:
            raise AttributeError('Piezo peripheral not configured.')

    @piezo.setter
    def piezo(self, value):  # lint:ok
        if self.has_piezo:
            if isinstance(value, Piezo()):  # lint:ok
                self._piezo = value
            else:
                self.logger.error('_piezo attribute must be a Piezo object')
                raise TypeError('The _piezo attribute must be a Piezo object.')
        else:
            raise AttributeError('Piezo peripheral not configured.')

    @property
    def position(self):
        """
        The current position of the full bearing rotation in degrees
        """
        if int(self._position) == 360:
            return 360
        else:
            return self._position % 360

    @position.setter
    def position(self, value):  # lint:ok
        if isinstance(value, (int, float)):
            # Allow for one over step:
            if value >= 0:
                self._position = value
            else:
                self.logger.error('motor position must be positive')
                raise ValueError('Motor position must be positive.')
        else:
            self.logger.error('motor position must be a number [degrees]')
            raise TypeError('Motor position must be a number [degrees].')

    @property
    def settling_time(self):
        """
        Time between end of motor movement and camera capture (seconds)
        """
        return self._settling_time

    @settling_time.setter
    def settling_time(self, val):  # lint:ok
        if isinstance(val, (int, float)):
            if val >= 0.0:
                self._settling_time = val
            else:
                self.logger.error('settling time must be a positive value')
                raise ValueError('Settling time must be a positive value.')
        else:
            self.logger.error('settling time must be of type int or float')
            raise TypeError('Settling time must be of type int or float.')

    @property
    def speed(self):
        """
        The speed of the motor, loosely based on rotations per minute.
        """
        if self.controller is not None:
            return self._controller.speed
        else:
            return None

    @speed.setter
    def speed(self, val):
        try:
            self.controller.speed = val
        except (ValueError, TypeError) as err:
            self.logger.error("failed to set motor speed")
            raise PRIDAPeripheralError(
                msg="Failed to set motor speed! {}".format(str(err)))
        except:
            self.logger.exception("unexpected error when setting motor speed")
            raise PRIDAPeripheralError(
                msg="Motor speed could not be assigned.")

    @property
    def step_deg(self):
        """
        The step resolution of the motor in terms of degrees per unit step.
        """
        if self.controller is not None:
            return self._controller.degrees
        else:
            return None

    @step_deg.setter
    def step_deg(self, value):
        if isinstance(value, (int, float)):
            if 0 < value:
                self.controller.degrees = value
            else:
                self.logger.error('degrees must be a value greater than 0')
                raise ValueError('Degrees must be a value greater than 0.')
        else:
            self.logger.error('degrees must be a integer or float')
            raise TypeError('Degrees must be a integer or float.')

    @property
    def steps_per_photo(self):
        """
        Number of unit steps per photo
        """
        if self.num_photos > 1 and self.step_deg is not None:
            return int(0.5 + self.total_steps / self.num_photos)
        else:
            return 0

    @steps_per_photo.setter
    def steps_per_photo(self, value):
        self.logger.error('setting the steps per photo manualy is forbidden')
        raise AttributeError('Setting steps per photo is forbidden.')

    @property
    def step_type(self):
        """
        The motor step type (i.e., micro, single, double, inter)
        """
        if self.controller is not None:
            return self._controller.step_type
        else:
            return None

    @step_type.setter
    def step_type(self, val):
        try:
            self.controller.step_type = val
        except ValueError as err:
            self.logger.error("failed to set motor step type")
            raise PRIDAPeripheralError(
                msg="Failed to set motor step type! {}".format(str(err)))
        except:
            self.logger.exception(
                "unexpected error during step type assignment")
            raise PRIDAPeripheralError(
                msg="Motor step type could not be assigned.")

    @property
    def supported_drivers(self):
        """
        Supported motor drivers
        """
        return self._supported_drivers

    @supported_drivers.setter
    def supported_drivers(self, val):  # lint:ok
        self.logger.error(
            'setting the supported drivers list manually is forbidden')
        raise AttributeError('Setting of supported drivers is forbidden.')

    @property
    def taken(self):
        """
        The number of photos that have already been taken
        """
        return self._taken

    @taken.setter
    def taken(self, value):  # lint:ok
        if isinstance(value, int):
            if value >= 0:
                self._taken = value
            else:
                self.logger.error('number of photos taken may not be negative')
                raise ValueError('Number of photos taken may not be negative.')
        else:
            self.logger.error('number of photos taken must be an integer')
            raise TypeError('Number of photos taken must be an integer value.')

    @property
    def total_steps(self):
        """
        Number of unit steps to circumnavigate the bearing
        """
        if self.step_deg is not None:
            return int(360.0*self.gear_ratio/(self.step_deg) + 0.5)
        else:
            return 0

    @total_steps.setter
    def total_steps(self, value):
        self.logger.error('setting the total steps manually is forbidden')
        raise AttributeError('Setting total steps is forbidden.')

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def calibrate_motor(self):
        """
        Name:     Imaging.calibrate_motor
        Inputs:   None
        Outputs:  None
        Features: Checks and waits for communication with motor controller.
        Note:     New speed ramping does not require the coil warm up as was
                  previously done to avoid jerking starts
        """
        if self.mode == '3D':
            num_tries = 0
            while self.controller.current_status() != 0:
                num_tries += 1
                if num_tries == 4:
                    raise PRIDAPeripheralError("Lost connection with Motor!")
                # self.controller.calibrate()
                time.sleep(1)

    def camera_aperture(self):
        """
        Name:     Imaging.camera_aperture
        Inputs:   None
        Outputs:  int, camera aperture setting
        Features: Returns the camera aperture setting
        """
        if self.mode in ('2D', '3D'):
            try:
                return self.camera.aperture
            except Exception as exp:
                self.logger.error(exp.args[0])
                return ''
        else:
            self.logger.warning('no camera in Explorer mode')
            return ''

    def camera_exposure(self):
        """
        Name:     Imaging.camera_exposure
        Inputs:   None
        Outputs:  float, camera exposure time
        Features: Returns the camera exposure time in seconds
        """
        if self.mode in ('2D', '3D'):
            try:
                return self.camera.exposure_time
            except Exception as exp:
                self.logger.error(exp.args[0])
                return ''
        else:
            self.logger.warning('no camera in Explorer mode')
            return ''

    def camera_iso_speed(self):
        """
        Name:     Imaging.camera_iso_speed
        Inputs:   None
        Outputs:  str, camera iso speed
        Features: Returns the camera iso speed
        """
        if self.mode in ('2D', '3D'):
            try:
                return self.camera.iso_speed
            except Exception as exp:
                self.logger.error(exp.args[0])
                return ''
        else:
            self.logger.warning('no camera in Explorer mode')
            return ''

    def camera_make(self):
        """
        Name:     Imaging.camera_make
        Inputs:   None
        Outputs:  str, camera make
        Features: Returns the camera make
        """
        if self.mode in ('2D', '3D'):
            try:
                return self.camera.make
            except Exception as exp:
                self.logger.error(exp.args[0])
                return ''
        else:
            self.logger.warning('no camera in Explorer mode')
            return ''

    def camera_model(self):
        """
        Name:     Imaging.camera_model
        Inputs:   None
        Outputs:  str, camera model
        Features: Returns the camera model
        """
        if self.mode in ('2D', '3D'):
            try:
                return self.camera.model
            except Exception as exp:
                self.logger.error(exp.args[0])
                return ''
        else:
            self.logger.warning('no camera in Explorer mode')
            return ''

    def check_error(self):
        """
        Name:     Imaging.check_error
        Inputs:   None
        Outputs:  None
        Features: Checks if an error is still present in the system
        Depends:  - calibrate_motor
                  - check_status
                  - turn_off_motors
        """
        self.logger.debug('start')
        if self.is_error is True:
            self.logger.info('attempting to determine origin...')
            try:
                self.calibrate_motor()
                self.turn_off_motors()
            except:
                tb = sys.exc_info()[2]
                raise PRIDAPeripheralError(
                    'Encountered motor problem!').with_traceback(tb)

            try:
                self.camera.connect()
                self.camera.capture('test')
            except:
                tb = sys.exc_info()[2]
                raise PRIDAPeripheralError(
                    'Encountered camera problem!').with_traceback(tb)
            else:
                self.logger.info('no hardware errors detected')
                self.is_error = False
                self.check_status()
            self.logger.debug('complete')

    def check_status(self):
        """
        Name:     Imaging.check_status
        Inputs:   None
        Outputs:  None
        Features: Sets the LED status indicator to the appropriate color.
        """
        if self.has_led:
            self.logger.debug('detecting system status')
            if self.is_error:
                self.led.yellow_on()
            elif self.is_running:
                self.led.red_on()
            else:
                self.led.green_on()
            self.logger.debug('adjusted led')

    def end_sequence(self):
        """
        Name:     Imaging.end_sequence
        Inputs:   None
        Outputs:  None
        Features: Stops imaging, turns off the motors and indicate ready
                  status, else indicates the error status.
        Depends:  - check_status
                  - finished_alert
                  - reset
                  - turn_off_motors
                  - warning_alert
        """
        self.logger.debug('start cleanup')
        if self.is_running:
            self.is_running = False

        try:
            self.reset()
            self.turn_off_motors()
        except:
            self.logger.exception('unexpected hardware error!')
            self.is_error = True
        finally:
            if not self.is_error:
                self.finished_alert()
            else:
                self.check_status()
                self.warning_alert()
            self.check_status()
            self.logger.info('imaging sequence complete')

    def finished_alert(self):
        """
        Name:     Imaging.finished_alert
        Inputs:   None
        Outputs:  None
        Features: Sounds the piezo buzzer for sequence completion
        """
        if self.has_piezo:
            for i in range(3):
                self.piezo.beep()
                time.sleep(0.5)

    def image(self):
        """
        Name:     Imaging.image
        Inputs:   None.
        Outputs:  dictionary of image meta data
                  > 'ready': bool, if an image is ready to save
                  > 'path': str, filepath to image
                  > 'datetime': str, timestamp of image capture (cpu clock)
                  > 'angle': float, rotation angle in 3D
                  > 'index': int, image sequence index
        Features: Takes a photo and returns image metadata
        Depends:  - check_status
                  - take_photo
        """
        img_meta = {'ready': False, 'path': None, 'datetime': None,
                    'angle': None, 'index': None}
        try:
            filename, dtime = self.take_photo()
        except:
            self.is_error = True
            self.check_status()
            self.logger.exception("Camera Error!")
            return img_meta
        else:
            self.is_error = False
            img_meta['ready'] = True
            img_meta['path'] = filename
            img_meta['datetime'] = dtime
            img_meta['angle'] = self.position
            img_meta['index'] = self.taken
            return img_meta

    def led_off(self):
        """
        Name:     Imaging.led_off
        Inputs:   None
        Outputs:  None
        Features: Turns off the LED
        """
        if self.has_led:
            self.led.all_off()

    def load_camera(self):
        """
        Name:     Imaging.load_camera
        Inputs:   None.
        Outputs:  None.
        Features: Assigns a camera object to Imaging
        """
        if self.mode in ('2D', '3D'):
            if Imaging.__dict__['_camera'] is None:
                try:
                    setattr(Imaging, '_camera',
                            Camera(self.parser, self.config_file))
                except PRIDAPeripheralError:
                    self.mode = 'Explorer'
                    raise
                except:
                    self.mode = 'Explorer'
                    self.logger.exception('unexpected exception caught!')
                    tb = sys.exc_info()[2]
                    raise PRIDAPeripheralError(
                        msg=('Could not instantiate camera. '
                             'Please check your connection or select '
                             'a different mode.')).with_traceback(tb)

    def load_controller(self):
        """
        Name:     Imaging.load_controller
        Inputs:   None.
        Outputs:  None.
        Features: Imports controller driver and assigns a controller object
                  to Imaging
        """
        if self.mode == '3D':
            if self.driver in self.supported_drivers:
                self.logger.debug("importing necessary driver")
                try:
                    exec('from .{} import {}'.format(
                        self.driver.lower(), self.driver))
                except ImportError:
                    tb = sys.exc_info()[2]
                    self.mode = 'Explorer'
                    self.logger.warning(
                        'failed to import driver, please check that your '
                        'hardware is properly configured')
                    raise PRIDAPeripheralError(
                        msg=('Failed to import driver, '
                             'please check your hardware '
                             'is properly configured.')).with_traceback(tb)

                if Imaging.__dict__['_controller'] is None:
                    try:
                        exec("setattr(Imaging, '_controller', "
                             "{}(self.parser, self.config_file))".format(
                                 self.driver))
                    except PRIDAPeripheralError:
                        self.mode = 'Explorer'
                        raise
                    except:
                        tb = sys.exc_info()[2]
                        self.mode = 'Explorer'
                        self.logger.exception('unexpected exception caught!')
                        raise PRIDAPeripheralError(
                            msg=('Could not instantiate controller. Please '
                                 'check your connection or select a different '
                                 'mode.')).with_traceback(tb)
                else:
                    self.logger.warning("controller is already loaded")
            else:
                self.logger.error("driver, %s, is not supported", self.driver)
                raise PRIDAPeripheralError(
                    msg=('Driver is not properly assigned. '
                         'Please check your configuration.'))
        else:
            self.logger.warning("cannot load controller; not in 3D mode")

    def load_led(self):
        """
        Name:     Imaging.load_led
        Inputs:   None.
        Outputs:  None.
        Features: Imports LED and assigns an LED object to Imaging
        """
        if self.has_led:
            # Import LED object
            from .led import LED

            if Imaging.__dict__['_led'] is None:
                try:
                    setattr(
                        Imaging, '_led', LED(self.parser, self.config_file))
                    self.led.green_on()
                except PRIDAPeripheralError:
                    self.has_led = False
                    raise
                except:
                    tb = sys.exc_info()[2]
                    self.has_led = False
                    self.logger.exception('unexpected exception caught!')
                    raise PRIDAPeripheralError(
                        msg=('Could not instantiate LED. '
                             'Please check your connection or disable LED in '
                             'your config file.')).with_traceback(tb)

    def load_piezo(self):
        """
        Name:     Imaging.load_piezo
        Inputs:   None.
        Outputs:  None.
        Features: Imports Piezo and assigns a Piezo object to Imaging
        """
        if self.has_piezo:
            # Import Piezo object
            from .piezo import Piezo

            if Imaging.__dict__['_piezo'] is None:
                try:
                    setattr(Imaging, '_piezo',
                            Piezo(self.parser, self.config_file))
                except PRIDAPeripheralError:
                    self.has_piezo = False
                    raise
                except:
                    tb = sys.exc_info()[2]
                    self.has_piezo = False
                    self.logger.exception('unexpected exception caught!')
                    raise PRIDAPeripheralError(
                        msg=('Could not instantiate Piezo. '
                             'Please check your connection or disable Piezo '
                             'in your config file.')).with_traceback(tb)

    def load_peripherals(self):
        """
        Name:     Imaging.load_peripherals
        Inputs:   None.
        Outputs:  None.
        Features: Assigns hardware peripheral attributes as designated by the
                  user in the configuration file
        Depends:  - load_camera
                  - load_controller
                  - load_led
                  - load_piezo
        """
        if self.mode == 'Explorer':
            self.logger.info("Explorer mode is enabled; halting imaging setup")
            raise PRIDAPeripheralError(
                crit=False,
                msg="Explorer Mode is set in config file.")
        else:
            # Try camera first, it's a quicker telltale of hardware problems
            try:
                self.load_camera()
            except PRIDAPeripheralError as ppe:
                self.logger.error(
                    "failed to load peripherals! {0}".format(str(ppe)))
                raise
            except:
                self.logger.exception("error loading peripherals!")
                raise PRIDAPeripheralError(
                    msg=('Unexpected hardware error encountered. '
                         'Please send log file to your system administrator.'))
            else:
                try:
                    self.camera.connect()
                    if self.controller is not None:
                        if self.controller.current_status() != 0:
                            self.logger.info("connecting to controller")
                            self.controller.connect()
                except PRIDAPeripheralError as ppe:
                    self.logger.error(
                        "failed to connect peripherals! {0}".format(str(ppe)))
                    raise
                except:
                    self.logger.exception("error connecting peripherals!")
                    raise PRIDAPeripheralError(
                        msg=('Unexpected hardware error encountered. Please '
                             'send log file to your system administrator.'))
                else:
                    try:
                        self.load_led()
                        self.load_piezo()
                        self.load_controller()
                    except PRIDAPeripheralError as ppe:
                        self.logger.error(
                            "failed to load peripherals! {0}".format(str(ppe)))
                        raise
                    except:
                        self.logger.exception("error loading peripherals!")
                        raise PRIDAPeripheralError(
                            msg=('Unexpected hardware error encountered. '
                                 'Please send log file to your system '
                                 'administrator.'))
                    else:
                        self.logger.info("connected to peripherals")

    def reset(self):
        """
        Name:     Imaging.reset
        Inputs:   None
        Outputs:  None
        Features: Resets the imaging hardware
        Depends:  - check_status
                  - reset_count
                  - reset_position
                  - turn_off_motors
        """
        self.logger.debug('resetting all counters...')
        self.reset_count()
        self.reset_position()

        self.logger.debug('turning off the motor(s)...')
        self.turn_off_motors()
        self.check_status()
        self.logger.debug('complete')

    def reset_count(self):
        """
        Name:     Imaging.reset_count
        Inputs:   None
        Outputs:  None
        Features: Resets the number of images taken counter
        """
        self.taken = 0

    def reset_position(self):
        """
        Name:     Imaging.reset_position
        Inputs:   None
        Outputs:  None
        Features: Resets the position indicators values
        """
        self.position = 0

    def run_sequence(self):
        """
        Name:     Imaging.run_sequence
        Inputs:   None
        Outputs:  dict, image meta data
        Features: Steps the motor, takes an image, or ends the
                  sequence as appropriate.
        Depends:  - calibrate_motor
                  - check_error
                  - check_status
                  - end_sequence
        """
        # Error handling with motor:
        if self.is_error and self.is_running:
            self.logger.exception('error during run sequence!')
            self.end_sequence()
            raise PRIDAPeripheralError(
                msg='Error detected! Please address and try again.')
        elif self.is_error:
            try:
                self.check_error()
            except:
                self.logger.exception('run sequence error not resolved!')
                self.end_sequence()
                raise PRIDAPeripheralError(
                    msg='Error not resolved! Please address and try again.')
            else:
                self.is_error = False
                self.logger.info('error resolved, beginning sequence')

        # Warm up coils and set running flag to True
        if not self.is_running:
            self.calibrate_motor()
            self.is_running = True
            self.check_status()

        img_meta = {'ready': False, 'path': None, 'datetime': None,
                    'angle': None, 'index': None}
        if self.taken < self.num_photos:
            try:
                self.step_to_next()
            except:
                self.is_error = True
                self.check_status()
                self.logger.exception('Motor Error!')
                return img_meta
            else:
                return self.image()
        else:
            self.end_sequence()
            return img_meta

    def single_step_motor(self):
        """
        Name:     Imaging.single_step_motor
        Inputs:   None
        Outputs:  None
        Features: Indexes the motor to the next unit step.
        """
        if self.mode == '3D':
            self.controller.single_step()
            if self.controller.step_type == "micro":
                self.position += self.step_deg / (
                    self.gear_ratio * self.controller.microsteps)
            elif self.controller.step_type == 'inter':
                self.position += self.step_deg / (self.gear_ratio * 2)
            else:
                self.position += self.step_deg / self.gear_ratio

    def step_motor(self, steps=1):
        """
        Name:     Imaging.step_motor
        Inputs:   [optional] int, number of unit steps (steps)
        Outputs:  None
        Features: Moves the motor one (or more) unit steps with ramped speed
        """
        if self.mode == '3D':
            if steps > 0:
                self.controller.step(steps, True)
                self.position += self.step_deg * steps / self.gear_ratio

    def step_to_next(self):
        """
        Name:     Imaging.step_to_next
        Inputs:   None
        Outputs:  None
        Features: Indexes the motor to the next image with ramped speed.
        """
        if self.mode == '3D':
            self.controller.step(self.steps_per_photo, True)
            self.position += 360 / self.num_photos

    def take_photo(self):
        """
        Name:     Imaging.take_photo
        Inputs:   None
        Outputs:  tuple, captured image file name and datetime
        Features: Captures a photo, updates the number of photos taken and
                  returns the image filename.
        """
        if self.mode in ('2D', '3D'):
            self.logger.debug('start.')
            my_time = datetime.datetime.now().strftime('%Y-%m-%d_%H.%M.%S')
            my_pos = '%06.2f' % self.position
            name = '_'.join([self.cur_pid, my_time, my_pos])
            self.logger.info('photo name - %s' % (name))
            time.sleep(self.settling_time)
            filename = self.camera.capture(name, verbose=False)
            self.taken += 1
            self.logger.debug('complete.')
            return (filename, my_time)

    def test_photo(self):
        """
        Name:     Imaging.test_photo
        Inputs:   None
        Outputs:  str, captured image file name (filename)
        Features: Captures a test photo and returns the image filename.
        """
        if self.mode in ('2D', '3D'):
            self.logger.debug('start')
            my_time = datetime.datetime.now().strftime('%Y-%m-%d_%H.%M.%S')
            name = '_'.join(["test", my_time])
            self.logger.info('photo name - %s' % (name))
            time.sleep(self.settling_time)

            try:
                filename = self.camera.capture(name, verbose=False)
            except:
                raise PRIDAPeripheralError(msg='Camera capture failed!')
            else:
                self.logger.debug('complete')
                return filename

    def turn_off_controller(self):
        """
        Name:     Imaging.turn_off_controller
        Inputs:   None
        Outputs:  None
        Features: Turns off motor controller
        """
        if self.mode == '3D' and self.controller is not None:
            self.controller.poweroff()

    def turn_off_motors(self):
        """
        Name:     Imaging.turn_off_motors
        Inputs:   None
        Outputs:  None
        Features: Turns off controller motor
        """
        if self.mode == '3D' and self.controller is not None:
            self.controller.turn_off_motors()

    def unset_controller(self):
        """
        Name:     Imaging.unset_controller
        Inputs:   None.
        Outputs:  None.
        Features: Deletes the current controller
        """
        self.logger.debug("deleting controller")
        del self.controller

    def warning_alert(self):
        """
        Name:     Imaging.warning_alert
        Inputs:   None
        Outputs:  None
        Features: Sounds piezo buzzer warning
        """
        if self.has_piezo:
            self.piezo.beep(3, 1, 1)


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    # To suppress linter warnings:
    from .led import LED
    from .piezo import Piezo

    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)

    # Instantiating logging handler and record format:
    root_handler = logging.StreamHandler()
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)

    img = Imaging()
    img.cur_pid = "001"
    img.driver = "Shield"
    img.mode = "3D"

    img.load_camera()
    img.camera.path = os.path.join(os.path.expanduser("~"), "Desktop")
    img.camera.connect()
    img.num_photos = 13

    img.load_controller()
    img.step_type = "double"
    img.speed = 20
    img.gear_ratio = 1.0
    img.step_motor(200)
