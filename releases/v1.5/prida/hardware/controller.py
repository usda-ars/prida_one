#!/usr/bin/python
#
# controller.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging

from .basecontroller import BaseController


###############################################################################
# CLASSES:
###############################################################################
class Controller(BaseController):
    """
    A generic guide to creating a motor controller class.

    Name:    Controller
    History: Version 1.5.0
             - trimmed to show generic controller outline [17.01.04]
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, my_parser=None, config_filepath=None):
        """
        Initialize data memebers. Inherit attributes and
        behaviours from the BaseController and PridaPeripheral absctract
        classes.
        Note: currently assumes that system is always using
        microstepping.
        """
        logging.debug('start initializing controller.')
        super(Controller, self).__init__()
        self.logger = logging.getLogger(__name__)

        # private attributes for property definitions
        self._speed = 2.0         # approximate rotational velocity
        self._step_res = 200      # number of units steps per rotation
        self._microsteps = 8      # number of microsteps per unit step
        self._motor_port_num = 1  # motor controller port number in use

        self.__step_types = {1: 'single', 2: 'double', 3: 'inter', 4: 'micro'}
        self._step_type = 'single'

        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('complete.')

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def degrees(self):
        """
        The step resolution in terms of degrees per unit step.
        """
        return 360.0 / self.step_res

    @degrees.setter
    def degrees(self, value):
        if isinstance(value, (int, float)):
            if 0 < value < 360:
                if (360.0 / value) % 1 == 0:
                    self.step_res = int(360 / value)
                else:
                    self.logger.error(
                        'degrees attribute must evenly divide 360.')
                    raise ValueError(
                        'Degrees attribute must evenly divide 360.')
            else:
                self.logger.error('degrees must be a value between 0 and 360.')
                raise ValueError('Degrees must be a value between 0 and 360.')
        else:
            self.logger.error('degrees must be a integer or float.')
            raise TypeError('Degrees must be a integer or float.')

    @property
    def microsteps(self):
        """
        The number of microsteps per unit step
        """
        return self._microsteps

    @microsteps.setter
    def microsteps(self, value):
        raise AttributeError('Setting of the microsteps is forbidden.')

    @property
    def motor_port_num(self):
        """
        The motor port number that the shield is currently controlling
        """
        return self._motor_port_num

    @motor_port_num.setter
    def motor_port_num(self, num):
        if isinstance(num, int):
            if num in (1, 2):
                self._motor_port_num = num
            else:
                self.logger.error('motor port number restricted to 1 or 2')
                raise ValueError('Motor port number restricted to 1 or 2.')
        else:
            self.logger.error('motor port number must be an integer')
            raise TypeError('Motor port number must be an integer.')

    @property
    def sleep_val(self):
        """
        The shortest delay in seconds between consecutive single steps.
        """
        # check step type for calculating sleep time
        if self.step_type == 'micro':
            substeps_per_rot = self.step_res * self.microsteps
        elif self.step_type == 'inter':
            substeps_per_rot = self.step_res * 2
        else:
            substeps_per_rot = self.step_res
        substeps_per_min = substeps_per_rot * self.speed
        self._sleep_val = 60.0 / substeps_per_min
        self.logger.debug('sleep val:{}'.format(self._sleep_val))

        return self._sleep_val

    @sleep_val.setter
    def sleep_val(self, val):
        raise AttributeError('Setting of the sleep value is forbidden.')

    @property
    def speed(self):
        """
        The approximate speed value at which the motor spins.
        """
        return self._speed

    @speed.setter
    def speed(self, val):
        """
        Updates the motor speed by affecting the sleep value between
        consecutive single steps loosely based on rotations per minute.
        """
        if isinstance(val, (float, int)):
            if val > 0:
                self._speed = val
            else:
                self.logger.error('speed requires a non-negative value.')
                raise ValueError('Motor speed requires a non-negative value.')
        else:
            self.logger.error('speed must be a number.')
            raise TypeError('Motor speed must be a number.')

    @property
    def step_res(self):
        """
        The number of unit steps to complete a full revolution of the motor
        shaft. A 1.8-deg motor has a step resolution of 200.
        """
        return self._step_res

    @step_res.setter
    def step_res(self, sr):
        if isinstance(sr, int):
            if sr > 0:
                self._step_res = sr
            else:
                self.logger.error('step resolution must be non-negative.')
                raise ValueError('Step resolution must be non-negative.')
        else:
            self.logger.error('step resolution must be an integer.')
            raise TypeError('Step resolution must be an integer.')

    @property
    def step_type(self):
        """
        The stepper motor's step type (micro, single, double, inter).
        """
        return self._step_type

    @step_type.setter
    def step_type(self, val):
        if isinstance(val, str):
            if val in self.__step_types.values():
                self._step_type = val
            else:
                self.logger.error(
                    "'{}' is not a valid stepping regime".format(val))
                raise ValueError(
                    "'{}' is not a valid stepping regime.".format(val))
        elif isinstance(val, int):
            if val in self.__step_types:
                self._step_type = self.__step_types[val]
            else:
                self.logger.error(
                    "'{}' is not a valid stepping regime".format(val))
                raise ValueError(
                    "'{}' is not a valid stepping regime.".format(val))
        else:
            self.logger.error(
                "'{}' is not a valid stepping regime".format(val))
            raise ValueError(
                "'{}' is not a valid stepping regime.".format(val))

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def calibrate(self):
        """
        Name:     Controller.calibrate
        Inputs:   None
        Outputs:  None
        Features: Perform a motor calibration to ensure operation.
                  * Required for BaseController
        """
        self.logger.debug('calibrating motor...')

    def connect(self):
        """
        Name:     Controller.connect
        Inputs:   None
        Outputs:  None
        Features: Attempt to connect to the controller
                  * Required for PRIDAPeripheral
        """
        self.logger.debug('establishing motor connection...')

    def current_status(self):
        """
        Name:     Controller.current_status
        Inputs:   None
        Outputs:  int, status indicator (status)
        Features: Attempt a simple handshake with the controller
                  * Required for PRIDAPeripheral
        """
        self.logger.debug('attempting handshake...')
        status = 0
        return status

    def poweroff(self):
        """
        Name:     Controller.poweroff
        Inputs:   None
        Outputs:  None
        Features: Send the command to power down the motor coils
                  * Required for PRIDAPeripheral
        """
        self.logger.debug('powering down the motor...')

    def single_step(self):
        """
        Name:     Controller.single_step
        Inputs:   None
        Outputs:  None
        Features: Take a unit step
                  * Required for BaseController
        """
        self.logger.debug("signalling motor for single step...")

    def turn_off_motors(self):
        """
        Name:     Controller.turn_off_motors
        Inputs:   None
        Outputs:  None
        Features: Disable all motors attached to the serial port.
                  * Required for BaseController
        Depends:  poweroff
        """
        self.logger.debug("disabling motor...")
        self.poweroff()
