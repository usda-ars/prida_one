#!/usr/bin/python
#
# pridaperipheral.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#


###############################################################################
# CLASSES:
###############################################################################
class PRIDAPeripheral(object):
    """
    Base 'Abstract Class' from which all PRIDA hardware inherit for proper
    polymorphic behaviour that guarantees the existence of the poweroff,
    connect and current_status behviours in addition to the state attribute
    as well as the base PRIDAPeripheralError exception.

    Name:     PRIDAPeripheral
    History:  Version 1.5.0
              - housekeeping [17.01.04]
              - changed state property to status [17.01.04]
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Initialize and declare common peripheral instance variables.
        """
        self._state = None

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def status(self):
        """
        Getter for status attribute; to be overwritten by child class.
        """
        return self._state

    @status.setter
    def status(self, value):  # lint:ok
        """
        Setter for status attribute; to be overwritten by child class.
        """
        self._state = value

    @status.deleter
    def status(self):  # lint:ok
        """
        Deleter for status attribute; to be overwritten by child class.
        """
        del self._state

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def connect(self):
        """
        Name:     PRIDAPeripheral.connect
        Inputs:   None
        Outputs:  None
        Features: Abstract behaviour. Override by child with connection
                  establishing behaviour.
        """
        raise PRIDAPeripheralError(
            msg='All peripherals require a connect sequence!')

    def current_status(self):
        """
        Name:     PRIDAPeripheral.current_status
        Inputs:   None
        Outputs:  None
        Features: Abstract behaviour. Override by child with behaviour for
                  attempting communication between the system and the
                  peripheral.
        """
        raise PRIDAPeripheralError(
            msg='All peripherals require knowledge of their current status!')

    def poweroff(self):
        """
        Name:     PRIDAPeripheral.poweroff
        Inputs:   None
        Outputs:  None
        Features: Abstract behaviour. Override by child with shutdown
                  behaviour.
        """
        raise PRIDAPeripheralError(
            msg='All peripherals require a shutdown sequence!')


class PRIDAPeripheralError(Exception):
    """
    Base 'Exception' class for errors specific to PRIDA hardware.

    Name:     PRIDAPeripheralError
    History:  Version 1.5.0-dev
              - housekeeping [17.01.04]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, *args, crit=True, msg='', **kwargs):  # lint:ok
        """
        Name:     PRIDAPeripheralError.__init__
        Inputs:   - args
                  - [optional] bool, critical message flag (crit)
                  - [optional] str, exception message string (msg)
                  - kwargs
        Features: Assigns exception critical level and error message
        Note: Linter does not like the ordering of these function parameters
        """
        Exception.__init__(self, args, kwargs)
        self._critical = crit
        if msg != '':
            self._message = msg
        elif len(args) > 0:
            self._message = str(args[0])
        else:
            self._message = None

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def critical(self):
        """
        Boolean flag for critical messages
        """
        return self._critical

    @critical.setter
    def critical(self, val):  # lint:ok
        if isinstance(val, bool):
            self._critical = val
        else:
            raise TypeError('critical attribute must be of type bool')

    @property
    def message(self):
        """
        Exception message string.
        """
        return self._message

    @message.setter
    def message(self, val):  # lint:ok
        if isinstance(val, str):
            self._message = val
        else:
            raise TypeError('message attribute must be of type string')

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __str__(self):
        """
        Returns the exception message upon use of the str() function.
        """
        return self.message
