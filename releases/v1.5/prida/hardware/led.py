#!/usr/bin/python
#
# led.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
from time import sleep
import logging

from .Adafruit_MotorHAT import Adafruit_MotorHAT
from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class LED(Adafruit_MotorHAT, PRIDAPeripheral):
    """
    LED, a class representing a tri-color (RGB) LED

    Name:       LED
    History:    Version 1.5.0
                - created status attribute [16.11.10]
                - fixed current status function [16.11.10]
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, my_parser=None, config_filepath=None):
        """
        Name:     LED.__init__
        Inputs:   - [optional] object, parser function (my_parser)
                  - [optional] str, configuration file path (config_filepath)
        Features: Initializes PWM channels; extends MotorHAT attributes and
                  behaviours.
        """
        logging.debug('start initializing led.')
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)
        self.logger = logging.getLogger(__name__)
        self._BLUE = 1         # Default channel for the blue LED
        self._GREEN = 14       # Default channel for the green LED
        self._RED = 15         # Default channel for the red LED
        self._led_type = 'CC'  # Default type Common Anode
        self.attr_dict = {'RED_LED_CHANNEL': 'red',
                          'BLUE_LED_CHANNEL': 'blue',
                          'GREEN_LED_CHANNEL': 'green',
                          'LED_TYPE': 'led_type',
                          }
        self._CURRENT_COLOR = ''
        self.status = 1
        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('complete.')

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def blue(self):
        """
        Channel on the Adafruit_MotorHAT connected to the blue LED
        """
        return self._BLUE

    @blue.setter
    def blue(self, ch):
        """
        Set the PWM channel controlling the blue led.
        """
        if isinstance(ch, int):
            if ch in {0, 1, 14, 15}:
                self._BLUE = ch
            else:
                self.logger.error('channels limited to 0, 1, 14 or 15.')
                raise ValueError('channels limited to 0, 1, 14 or 15.')
        else:
            self.logger.error('blue channel number must be an integer.')
            raise TypeError('blue channel number must be an integer.')

    @property
    def current_color(self):
        """
        The current color of the led
        """
        return self._CURRENT_COLOR

    @current_color.setter
    def current_color(self, color_str):
        if isinstance(color_str, str):
            if color_str in {'red', 'green', 'blue', 'yellow', 'white', ''}:
                self._CURRENT_COLOR = color_str
            else:
                self.logger.error('current color cannot'
                                  ' be set to %s' % (color_str))
                raise ValueError('current color cannot'
                                 ' be set to %s' % (color_str))
        else:
            self.logger.error('current color attribute must be a string')
            raise TypeError('current color attribute must be a string')

    @property
    def green(self):
        """
        Channel on the Adafruit_MotorHAT connected to the green LED
        """
        return self._GREEN

    @green.setter
    def green(self, ch):
        """
        Set the PWM channel controlling the green led.
        """
        if isinstance(ch, int):
            if ch in {0, 1, 14, 15}:
                self._GREEN = ch
            else:
                self.logger.error('channels limited to 0, 1, 14 or 15.')
                raise ValueError('channels limited to 0, 1, 14 or 15.')
        else:
            self.logger.error('green channel number must be an integer.')
            raise TypeError('green channel number must be an integer.')

    @property
    def led_type(self):
        """
        Two letter designation representing either 'Common Cathode' or
        'Common Anode' LED style
        """
        return self._led_type

    @led_type.setter
    def led_type(self, led_type):
        if isinstance(led_type, str):
            if led_type == 'CA' or led_type == 'CC':
                self._led_type = led_type
            else:
                self.logger.error('bad input. Values limited to CC or CA.')
                raise ValueError("Bad input. Values limited to CC or CA.")
        else:
            self.logger.error('led type attribute must be a string.')
            raise TypeError('led type attribute must be a string.')

    @property
    def red(self):
        """
        Channel on the Adafruit_MotorHAT connected to the red LED
        """
        return self._RED

    @red.setter
    def red(self, ch):
        """
        Set the PWM channel controlling the red led.
        """
        if isinstance(ch, int):
            if ch in {0, 1, 14, 15}:
                self._RED = ch
            else:
                self.logger.error('channels limited to 0, 1, 14 or 15.')
                raise ValueError('channels limited to 0, 1, 14 or 15.')
        else:
            self.logger.error('red channel number must be an integer.')
            raise TypeError('red channel number must be an integer.')

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def all_off(self):
        """
        Name:     LED.all_off
        Inputs:   None
        Outputs:  None
        Features: Sets all led PWM channels to low.
        """
        self.logger.debug('called')
        self.blue_off()
        self.green_off()
        self.red_off()
        self.current_color = ''

    def all_on(self):
        """
        Name:     LED.all_on
        Inputs:   None
        Outputs:  None
        Features: Sets all led PWM channels to high.
        """
        if self.current_color != 'white':
            self.logger.debug('start.')
            self.led_on(self._BLUE)
            self.led_on(self._GREEN)
            self.led_on(self._RED)
            self.current_color = 'white'

    def blue_off(self):
        """
        Name:     LED.blue_off
        Inputs:   None
        Outputs:  None
        Features: Sets low the 'blue' PWM channel, default 1.
        """
        self.logger.debug('called')
        self.led_off(self._BLUE)
        if self.current_color == 'blue':
            self.current_color = ''

    def blue_on(self):
        """
        Name:     LED.blue_on
        Inputs:   None
        Outputs:  None
        Features: Sets high the 'BLUE' PWM channel, default 1.
        """
        if self.current_color != 'blue':
            self.logger.debug('start.')
            self.led_on(self._BLUE)
            self.led_off(self._RED)
            self.led_off(self._GREEN)
            self.current_color = 'blue'

    def connect(self):
        """
        Name:     LED.connect
        Inputs:   None.
        Outputs:  None.
        Features: Blink LEDs
        Depends:  - all_off
                  - all_on
        """
        self.logger.debug('called')
        self.status = 1
        try:
            self.all_on()
            sleep(1)
            self.all_off()
        except:
            self.logger.error('could not connect to LED.')
            raise PRIDAPeripheralError(msg='Could not connect to LED!')
        else:
            self.status = 0

    def current_status(self):
        """
        Name:     LED.current_status
        Inputs:   None
        Outputs:  int, status indicator (status)
        Features: Identifies the current status of the LED.
                  Required for PRIDAPeripheral
        """
        return self.status

    def green_off(self):
        """
        Name:     LED.green_off
        Inputs:   None
        Outputs:  None
        Features: Sets low the 'green' PWM channel, default 14.
        """
        self.logger.debug('called')
        self.led_off(self._GREEN)
        if self.current_color == 'green':
            self.current_color = ''

    def green_on(self):
        """
        Name:     LED.green_on
        Inputs:   None
        Outputs:  None
        Features: Sets high the 'GREEN' PWM channel, default 14.
        """
        if self.current_color != 'green':
            self.logger.debug('start.')
            self.led_on(self._GREEN)
            self.led_off(self._RED)
            self.led_off(self._BLUE)
            self.current_color = 'green'

    def led_off(self, channel):
        """
        Name:     LED.led_off
        Inputs:   int, the number of the channel to lower (channel)
        Outputs:  None
        Features: Sets low a specifed led channel.
        """
        self.logger.debug('turning off led channel %i', channel)
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 4096, 0)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 0, 4096)

    def led_on(self, channel):
        """
        Name:     LED.led_on
        Inputs:   int, the number of the channel to raise (channel)
        Outputs:  None
        Features: Sets high a specified led channel.
        """
        self.logger.debug('turning on led channel %i', channel)
        if self.led_type == 'CA':
            self._pwm.setPWM(channel, 0, 4096)
        elif self.led_type == 'CC':
            self._pwm.setPWM(channel, 4096, 0)

    def poweroff(self):
        """
        Name:     LED.poweroff
        Inputs:   None
        Outputs:  None
        Features: Sends the command to power down the LEDs
        Depends:  all_off
        """
        self.logger.debug('called')
        self.all_off()
        self.status = 1

    def red_off(self):
        """
        Name:     LED.red_off
        Inputs:   None
        Outputs:  None
        Features: Sets low the 'red' PWM channel, default 15.
        """
        self.logger.debug('called')
        self.led_off(self._RED)
        if self.current_color == 'red':
            self.current_color = ''

    def red_on(self):
        """
        Name:     LED.red_on
        Inputs:   None
        Outputs:  None
        Features: Sets high the 'RED' PWM channel, default 15.
        """
        if self.current_color != 'red':
            self.logger.debug('start.')
            self.led_on(self._RED)
            self.led_off(self._BLUE)
            self.led_off(self._GREEN)
            self.current_color = 'red'

    def yellow_off(self):
        """
        Name:     LED.yellow_off
        Inputs:   None
        Outputs:  None
        Features: Sets low the red and green PWM channels.
        """
        self.logger.debug('called')
        self.red_off()
        self.green_off()
        if self.current_color == 'yellow':
            self.current_color = ''

    def yellow_on(self):
        """
        Name:     LED.yellow_on
        Inputs:   None
        Outputs:  None
        Features: Sets led color to yellow by combining green and red
        """
        if self.current_color != 'yellow':
            self.logger.debug('start.')
            self.led_on(self._RED)
            self.led_on(self._GREEN)
            self.led_off(self._BLUE)
            self.current_color = 'yellow'
