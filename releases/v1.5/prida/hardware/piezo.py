#!/usr/bin/python
#
# piezo.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import time
import logging

from .Adafruit_MotorHAT import Adafruit_MotorHAT
from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class Piezo(Adafruit_MotorHAT, PRIDAPeripheral):
    """
    Name:       Piezo
    History:    Version 1.5.0
                - created status attribute [16.11.10]
                - fixed current status function [16.11.10]
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, my_parser=None, config_filepath=None):
        logging.debug('start initializing piezo.')
        PRIDAPeripheral.__init__(self)
        Adafruit_MotorHAT.__init__(self)

        self.logger = logging.getLogger(__name__)

        self._CHANNEL = 0
        self._VOLUME = 1.0
        self.attr_dict = {'PWM_CHANNEL': 'channel',
                          'VOLUME': 'volume',
                          }
        self.status = 1

        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('complete.')

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def channel(self):
        """
        Channel number assigned to controlling the piezo buzzer
        """
        return self._CHANNEL

    @channel.setter
    def channel(self, ch):
        if isinstance(ch, int):
            if ch in {0, 1, 14, 15}:
                self._CHANNEL = ch

    @property
    def volume(self):
        """
        Value representing currently assigned piezo buzzer volume
        """
        return 100 * self._VOLUME

    @volume.setter
    def volume(self, volume):
        if isinstance(volume, int) or isinstance(volume, float):
            if volume < 0:
                volume = 0
            elif volume > 100:
                volume = 100
            self._VOLUME = volume / 100.0

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def beep(self, num_beeps=3, beep_time=0.05, rest_time=0.1):
        """
        Name:     Piezo.beep
        Inputs:   - [optional] int, number of beeps (num_beeps)
                  - [optional] float, length of each beep, seconds (beep_time)
                  - [optional] float, inter-beep silence, seconds (rest_time)
        Outputs:  None
        Features: Temporarily raise the 'piezo' channel. Default is three
                  short beeps in quick succession.
        """
        self.logger.debug('called')
        for i in range(num_beeps):
            # turn buzzer on
            self._pwm.setPWM(self._CHANNEL, 0, int(2048 * self._VOLUME))
            # sustain note
            time.sleep(beep_time)
            # turn buzzer off
            self._pwm.setPWM(self._CHANNEL, 0, 4096)
            # rest
            time.sleep(rest_time)

    def connect(self):
        """
        Name:     Piezo.connect
        Inputs:   None.
        Outputs:  None.
        Features: Beep twice
        Depends:  beep
        """
        self.status = 1
        try:
            self.beep(2)
        except:
            self.logger.error('could not connect to piezo.')
            raise PRIDAPeripheralError('Could not connect to piezo!')
        else:
            self.logger.info("Piezo connected!")
            self.status = 0

    def current_status(self):
        """
        Name:     Piezo.current_status
        Inputs:   None
        Outputs:  int, status indicator (status)
        Features: Identifies the current status of the Piezo.
                  Required for PRIDAPeripheral
        """
        return self.status

    def poweroff(self):
        """
        Name:     Piezo.poweroff
        Inputs:   None
        Outputs:  None
        Features: Sends the command to power down the Piezo
        """
        self.logger.debug('called')
        self._pwm.setPWM(self._CHANNEL, 0, 0)
