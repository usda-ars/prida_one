#!/usr/bin/python
#
# hat.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import time
import logging

from .Adafruit_MotorHAT import Adafruit_MotorHAT
from .basecontroller import BaseController
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class HAT(Adafruit_MotorHAT, BaseController):
    """
    Name:    HAT
    History: Version 1.5.0
             - changed rpm to speed [16.11.10]
             - updated attr dict [16.11.10]
             - removed base rot time [16.11.10]
             - created sleep value parameter [16.11.10]
             - created status parameter [16.11.10]
             - updated single step and step functions [16.11.10]
             - added support for single, double and interleave [16.11.10]
             - motor port num redefines stepper motor object [16.11.10]
             - added PRIDAPeripheralError messages to step [16.11.23]
             - updated speed and step type setter error messages [17.01.04]
             - added connect call to class init [17.01.04]
             - inherits BaseController instead of PRIDAPeripheral [17.01.04]
             - disabled degrees setter [17.01.04]
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, my_parser=None, config_filepath=None):
        """
        Name:     HAT.__init__
        Inputs:   - [optional] object, parser function (my_parser)
                  - [optional] str, configuration file path (config_filepath)
        Features: Initialize data memebers, inherit attributes and behaviours
                  from the Adafruit_MotorHAT object.
        Depends:  getStepper
        Note:     Currently assumes that system is always using microstepping.
        """
        logging.debug('start initializing hat.')
        BaseController.__init__(self)
        Adafruit_MotorHAT.__init__(self)

        my_name = __name__[:-3] + 'controller'
        self.logger = logging.getLogger(my_name)

        self.status = 1           # 0 for ready; 1 for not-ready
        self._speed = 2.0         # Approximate rotational velocity
        self._sleep_val = 0.01    # Time to sleep between steps
        self._step_res = 200      # Number of units steps per rotation
        self.__step_types = {self.SINGLE: 'single',     # SINGLE, DOUBLE,
                             self.DOUBLE: 'double',     # INTERLEAVE and
                             self.INTERLEAVE: 'inter',  # MICROSTEP are defined
                             self.MICROSTEP: 'micro',   # in Adafruit_MotorHAT
                             }
        self._step_type = 'micro'

        # Create Adafruit_StepperMotor object:
        self._motor_port_num = 1
        self._sm = self.getStepper(self.step_res, self.motor_port_num)

        if my_parser is not None and config_filepath is not None:
            my_parser(self, my_name, config_filepath)

        self.connect()
        self.logger.debug('complete.')

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def degrees(self):
        """
        The step resolution in terms of degrees per unit step.
        """
        return 360.0 / self.step_res

    @degrees.setter
    def degrees(self, value):
        self.logger.error('setting the step degree manualy is forbidden')
        raise AttributeError('Setting step degree is forbidden.')

    @property
    def microsteps(self):
        """
        The number of microsteps per unit step
        """
        return self._sm.MICROSTEPS

    @microsteps.setter
    def microsteps(self, value):
        if isinstance(value, int):
            if value in {4, 8, 12, 16}:
                self._sm.MICROSTEPS = value
                self._sm._build_microstep_curve()
            else:
                self.logger.error(
                    'microsteps per step must be a multiple of four.')
                raise ValueError(
                    'microsteps per step must be a multiple of four.')
        else:
            self.logger.error(
                'number of microsteps per step must be an integer.')
            raise TypeError(
                'number of microsteps per step must be an integer.')

    @property
    def motor_port_num(self):
        """
        The motor port number that the HAT is currently controlling
        """
        return self._motor_port_num

    @motor_port_num.setter
    def motor_port_num(self, num):
        """
        Sets stepper motor port number and re-establishes stepper motor object
        """
        if isinstance(num, int):
            if num in (1, 2):
                self._motor_port_num = num
                self._sm = self.getStepper(self.step_res, num)
            else:
                self.logger.error('motor port number restricted to 1 or 2.')
                raise ValueError('motor port number restricted to 1 or 2.')
        else:
            self.logger.error('motor port number must be an integer.')
            raise TypeError('motor port number must be an integer.')

    @property
    def sleep_val(self):
        """
        The shortest delay in seconds between consecutive single steps.
        """
        # check step type for calculating sleep time
        if self.step_type is self.__step_types[self.MICROSTEP]:
            substeps_per_rot = self.step_res * self.microsteps
        elif self.step_type is self.__step_types[self.INTERLEAVE]:
            substeps_per_rot = self.step_res * 2
        else:
            substeps_per_rot = self.step_res
        substeps_per_min = substeps_per_rot * self.speed
        self._sleep_val = 60.0 / substeps_per_min
        self.logger.debug('sleep val:{}'.format(self._sleep_val))

        return self._sleep_val

    @sleep_val.setter
    def sleep_val(self, val):
        raise AttributeError('setting of the sleep value is forbidden')

    @property
    def speed(self):
        """
        The approximate speed value at which the motor spins.
        """
        return self._speed

    @speed.setter
    def speed(self, val):
        """
        Updates the motor speed by affecting the sleep value between
        consecutive single steps loosely based on rotations per minute.
        """
        if isinstance(val, (float, int)):
            if val > 0:
                self._speed = val
            else:
                self.logger.error('speed requires a non-negative value.')
                raise ValueError('Motor speed requires a non-negative value.')
        else:
            self.logger.error('speed must be a number.')
            raise TypeError('Motor speed must be a number.')

    @property
    def step_res(self):
        """
        The number of unit steps to complete a full revolution of the motor
        shaft.
        """
        return self._step_res

    @step_res.setter
    def step_res(self, sr):
        """
        Sets stepper motor step resolution. Not currently supported.
        This property was added to reduce presence of 'magic numbers.'
        """
        if isinstance(sr, int):
            if sr > 0:
                self._step_res = sr
            else:
                self.logger.error('step resolution must be non-negative.')
                raise ValueError('Step resolution must be non-negative')
        else:
            self.logger.error('step resolution must be an integer.')
            raise TypeError('Step resolution must be an integer')

    @property
    def step_type(self):
        """
        The stepper motor's step type (micro, single, double, interleave).
        """
        return self._step_type

    @step_type.setter
    def step_type(self, val):
        if isinstance(val, str):
            if val in self.__step_types.values():
                self._step_type = val
            else:
                try:
                    decval = int(val, 2)
                except ValueError:
                    self.logger.error(
                        "'{}' is not a valid stepping regime".format(val))
                    raise ValueError(
                        "'{}' is not a valid stepping regime.".format(val))
                else:
                    if decval in self.__step_types.keys():
                        self._step_type = self.__step_types[decval]
                    else:
                        self.logger.error(
                            "'{}' does not ".format(val) +
                            "correspond to a stepping regime")
                        raise ValueError(
                            "'{}' does not ".format(val) +
                            "correspond to a stepping regime.")

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def calibrate(self):
        """
        Name:     HAT.calibrate
        Inputs:   None
        Outputs:  None
        Features: Takes three unit steps to engage coils.
                  * Required for BaseController
        """
        self.logger.debug('called')
        for i in range(3 * self._sm.MICROSTEPS):
            self._sm.oneStep(self.FORWARD, self.MICROSTEP)
            time.sleep(.1)

    def connect(self):
        """
        Name:     HAT.connect
        Inputs:   None.
        Outputs:  None.
        Features: Gather stepper from port number and calibrate
                  * Required for PRIDAPeripheral
        Depends:  - calibrate
                  - getStepper (defined in Adafruit_MotorHAT)
        """
        self.logger.debug('called')
        self.status = 1
        try:
            self.calibrate()
        except:
            raise PRIDAPeripheralError(msg='Could not connect to motor!')
        else:
            self.status = 0
            self.logger.info("Motor connected!")

    def current_status(self):
        """
        Name:     HAT.current_status
        Inputs:   None
        Outputs:  int, status indicator (status)
        Features: Identifies the current status of the HAT.
                  * Required for PRIDAPeripheral
        """
        self.logger.debug('called')
        return self.status

    def poweroff(self):
        """
        Name:     HAT.poweroff
        Inputs:   None
        Outputs:  None
        Features: Sends the command to power down the coils and sets the
                  status to 'not ready'
                  * Required for PRIDAPeripheral
        Depends:  turn_off_motors
        """
        self.logger.debug('called')
        self.turn_off_motors()
        self.status = 1

    def single_step(self):
        """
        Name:     HAT.single_step
        Inputs:   None
        Outputs:  None
        Features: Takes a single step
                  * Required for BaseController
        """
        if self.status == 0:
            time.sleep(self.sleep_val)
            if self.step_type == 'micro':
                my_step = self.MICROSTEP
            elif self.step_type == 'inter':
                my_step = self.INTERLEAVE
            elif self.step_type == 'single':
                my_step = self.SINGLE
            elif self.step_type == 'double':
                my_step = self.DOUBLE
            else:
                raise PRIDAPeripheralError(
                    msg="Cannot step with type %s" % (self.step_type))
            self._sm.oneStep(self.FORWARD, my_step)
        else:
            raise PRIDAPeripheralError(msg="Motor status is not ready!")

    def turn_off_motors(self):
        """
        Name:     HAT.turn_off_motors
        Inputs:   None
        Outputs:  None
        Features: Disable all motors attached to the HAT.
                  * Required for BaseController
        """
        self.logger.debug('called')
        self.getMotor(1).run(self.RELEASE)
        self.getMotor(2).run(self.RELEASE)
        self.getMotor(3).run(self.RELEASE)
        self.getMotor(4).run(self.RELEASE)
