#!/usr/bin/python
#
# hard_worker.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software is freely available to the public for use.      #
# The Department of Agriculture (USDA) and the U.S. Government have not       #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     USDA-Agricultural Research Service                                      #
#     Robert W. Holley Center for Agriculture and Health                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os
import sys

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QMutexLocker
from PyQt5.QtCore import QObject

from .hardware.pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASS:
###############################################################################
class HardWorker(QObject):
    """
    Name:     HardWorker
    Features: This class is a worker object used for threading hardware.
    History:  Version 1.5.0
              - changed logging location in create imaging [16.07.19]
              - moved Peripheral Error to the preamble [16.09.30]
              - separate call to imaging load peripherals [16.11.03]
              - update max value based on total unit steps [16.11.10]
              - started updating run sequence (needs work) [16.11.10]
              - new version of run sequence (needs testing) [16.11.23]
              - updated get maximum value to 360 degrees [16.11.30]
              - added property for imaging path [16.12.01]
              - updated logging statements [16.12.19]
              - created report signal [17.01.03]
              - created take photo function for image previewing [17.01.03]
              - created test motor function [17.01.03]
              - added motor speed and step type properties [17.01.04]
              - added a reset during test motor [17.01.04]
              - added image settling time property [17.01.04]
              - added gear ratio property [17.01.04]
              - added motor control driver property [17.01.04]
              - added motor degree resolution property [17.01.04]
              - error handling during motor calibration [17.01.18]
              - added total steps property [17.02.10]
              - created test gear ratio function [17.02.10]
              - changed running/ready signal vals to dict [17.04.04]
              - all new run sequence w/ 2D workflow [17.04.04]
              - added QMutexLocker [17.04.04]
              - removed unnecessary end sequence function [17.04.04]
              - created cancel imaging function [17.04.04]
              - added abort flag to run sequence [17.04.04]
              - UNDO abort [17.04.04]
              - updated take preview photo function [17.06.08]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    finished = pyqtSignal()           # imaging sequence complete
    running = pyqtSignal(int, dict)   # hardware moved - update GUI
    ready = pyqtSignal(dict)          # photo taken event - save image
    sendAlert = pyqtSignal(str)       # send error messages to GUI
    report = pyqtSignal(str)          # for reporting messages to Prida app

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None, mutex=None):
        """
        Name:     HardWorker.__init__
        Inputs:   [optional] Qt parent object (parent)
        Returns:  None.
        Features: Class initialization
        """
        super(HardWorker, self).__init__(parent)

        # Create a worker logger:
        self.logger = logging.getLogger(__name__)
        self.logger.debug("hardware worker object initialized")

        # Create a mutex lock
        self.mutex = mutex

        # Initialize property values:
        self.is_enabled = False

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def image_number(self):
        """
        Total number of images to be taken.
        """
        if self.is_enabled:
            return self.imaging.num_photos
        else:
            return None

    @image_number.setter
    def image_number(self, value):  # lint:ok
        if self.is_enabled:
            self.logger.debug("setting number of images to %d", value)
            try:
                self.imaging.num_photos = value
            except PRIDAPeripheralError as PPE:
                self.sendAlert.emit(str(PPE))
        else:
            self.logger.warning("image number not set; hardware not enabled")

    @property
    def image_pid(self):
        """
        Current plant identifier used for naming photos
        """
        if self.is_enabled:
            return self.imaging.cur_pid
        else:
            return None

    @image_pid.setter
    def image_pid(self, value):  # lint:ok
        if self.is_enabled:
            self.logger.debug("setting current image PID to %s", value)
            self.imaging.cur_pid = value
        else:
            self.logger.warning("image PID not set; hardware not enabled")

    @property
    def image_motor_driver(self):
        """
        The driver name used for the motor controller
        """
        if self.is_enabled:
            return self.imaging.driver
        else:
            return None

    @image_motor_driver.setter
    def image_motor_driver(self, val):
        try:
            self.imaging.driver = val
        except:
            raise PRIDAPeripheralError(msg="Failed to update driver.")
        else:
            if self.is_enabled:
                self.imaging.unset_controller()
                self.imaging.load_controller()
            else:
                self.imaging.load_peripherals()

    @property
    def image_settling_time(self):
        """
        Time between end of motor movement and camera capture (seconds)
        """
        if self.is_enabled:
            return self.imaging.settling_time
        else:
            return None

    @image_settling_time.setter
    def image_settling_time(self, val):
        if self.is_enabled:
            self.imaging.settling_time = val
        else:
            raise PRIDAPeripheralError(msg="Hardware is not currently enabled")

    @property
    def camera_make(self):
        """
        The camera make.
        """
        if self.is_enabled:
            return self.imaging.camera_make()
        else:
            return ""

    @property
    def camera_model(self):
        """
        The camera model.
        """
        if self.is_enabled:
            return self.imaging.camera_model()
        else:
            return ""

    @property
    def camera_exposure(self):
        """
        The camera exposure value.
        """
        if self.is_enabled:
            return self.imaging.camera_exposure()
        else:
            return ""

    @property
    def camera_aperture(self):
        """
        The camera aperture value.
        """
        if self.is_enabled:
            return self.imaging.camera_aperture()
        else:
            return ""

    @property
    def camera_iso_speed(self):
        """
        The camera ISO value.
        """
        if self.is_enabled:
            return self.imaging.camera_iso_speed()
        else:
            return ""

    @property
    def mode(self):
        """
        The current imaging mode (Explorer, 2D, or 3D).
        """
        if self.is_enabled:
            return self.imaging.mode
        else:
            return "Explorer"

    @property
    def motor_degrees(self):
        """
        The step resolution of the motor in terms of degrees per unit step.
        """
        if self.is_enabled:
            return self.imaging.step_deg
        else:
            return None

    @motor_degrees.setter
    def motor_degrees(self, val):
        if self.is_enabled:
            self.imaging.step_deg = val
        else:
            raise PRIDAPeripheralError(msg='Hardware is not currently enabled')

    @property
    def motor_gear_ratio(self):
        """
        The ratio of the bearing to motor wheel diameters
        """
        if self.is_enabled:
            return self.imaging.gear_ratio
        else:
            return None

    @motor_gear_ratio.setter
    def motor_gear_ratio(self, val):
        if self.is_enabled:
            self.imaging.gear_ratio = val
        else:
            raise PRIDAPeripheralError(msg='Hardware is not currently enabled')

    @property
    def motor_speed(self):
        """
        The current motor speed.
        """
        if self.is_enabled:
            return self.imaging.speed
        else:
            return None

    @motor_speed.setter
    def motor_speed(self, val):
        if self.is_enabled:
            self.imaging.speed = val
        else:
            raise PRIDAPeripheralError(msg='Hardware is not currently enabled')

    @property
    def motor_step_type(self):
        """
        The current motor step type (e.g., micro, single, double, inter)
        """
        if self.is_enabled:
            return self.imaging.step_type
        else:
            return None

    @motor_step_type.setter
    def motor_step_type(self, val):
        if self.is_enabled:
            self.imaging.step_type = val
        else:
            raise PRIDAPeripheralError(msg='Hardware is not currently enabled')

    @property
    def path(self):
        """Imaging path"""
        if self.is_enabled:
            return self.imaging.path
        else:
            return ""

    @path.setter
    def path(self, val):
        if self.is_enabled:
            try:
                self.imaging.path = val
            except (PRIDAPeripheralError, TypeError, ValueError) as ppe:
                self.sendAlert.emit("Error! {0}".format(ppe))
            except:
                self.sendAlert.emit("Error! Could not assign camera path.")
            else:
                self.logger.info("Imaging path set to %s", val)
                self.report.emit("Camera path set to %s" % (val))

    @property
    def total_steps(self):
        """
        Number of unit steps to circumnavigate the bearing
        """
        if self.is_enabled:
            return self.imaging.total_steps
        else:
            return 0

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def create_imaging(self, my_parser=None, config_filepath=None):
        """
        Name:     HardWorker.create_imaging
        Inputs:   - [optional] function handle (my_parser)
                  - [optional] str, configuration file path (config_filepath)
        Outputs:  None.
        Features: Creates an Imaging class instance
        """
        if '.hardware.imaging' not in sys.modules:
            alerted = False
            try:
                from .hardware.imaging import Imaging
                self.is_enabled = True
            except ImportError:
                self.logger.warning("Imaging import failed")
                self.is_enabled = False
            except:
                self.logger.exception("Unexpected error during import.")
                self.is_enabled = False
            else:
                self.logger.info("Imaging successfully imported")
                try:
                    self.imaging = Imaging(my_parser, config_filepath)
                except PRIDAPeripheralError as PPE:
                    self.logger.warning("Imaging instantiation failed")
                    self.is_enabled = False
                    if PPE.critical:
                        self.logger.error(
                            "Critical Error! Starting Explorer mode")
                        self.sendAlert.emit(
                            '{}. Starting in Explorer Mode.'.format(str(PPE)))
                        alerted = True
                else:
                    try:
                        self.imaging.load_peripherals()
                    except PRIDAPeripheralError as PPE:
                        self.logger.warning("Imaging failed to load")
                        self.is_enabled = False
                        if PPE.critical:
                            self.logger.error(
                                "Critical Error! Starting Explorer mode")
                            self.sendAlert.emit(
                                '{}. Starting in Explorer Mode.'.format(
                                    str(PPE)))
                            alerted = True
            finally:
                conf_mode = self.mode_checker(config_filepath)
                if conf_mode.lower() != 'explorer' and not self.is_enabled:
                    if not alerted:
                        self.sendAlert.emit(
                            'Hardware mode is not supported on this system! '
                            'Starting in Explorer mode.')
                else:
                    self.logger.info(
                        "Explorer mode is enabled; imaging setup halted")
        else:
            if hasattr(self, 'imaging'):
                self.logger.warning('imaging object already created')
            else:
                self.logger.warning(
                    "imaging imported but not created, disabling hardware")
                self.is_enabled = False
                self.sendAlert.emit(
                    'Something has gone wrong!\n'
                    'Please contact your system administrator. '
                    'Starting in Explorer mode.')

    def get_maximum_value(self):
        """
        Name:     HardWorker.get_maximum_value
        Inputs:   None.
        Outputs:  None.
        Features: Returns the maximum value used for the GUI progress bar or
                  zero if hardware is not enabled; currently set as rotation
                  angle of plant
        """
        if self.is_enabled:
            max_val = 360
            self.logger.debug("max value calculated as %d", max_val)
        else:
            self.logger.warning(
                "cannot calculate max value; hardware not enabled")
            max_val = 0

        return max_val

    def mode_checker(self, config_filepath):
        """
        Name:     HardWorker.mode_checker
        Inputs:   str, configuration file path (config_filepath)
        Outputs:  str, configuration mode
        Features: Returns the configuration file's mode
        """
        # Initialize variables:
        conf_mode = None

        if config_filepath is not None:
            try:
                my_user = os.path.expanduser('~')
                conf_file = os.path.join(my_user, 'Prida', 'prida.config')
                if os.path.isfile(conf_file):
                    with open(conf_file, 'r') as my_config_file:
                        for line in my_config_file:
                            if 'MODE' in line:
                                words = line.split()
                                if len(words) == 3:
                                    conf_mode = eval(words[2])
            except:
                self.logger.exception(
                    "Encountered error reading configuration file %s",
                    config_filepath)

        if conf_mode is None:
            conf_mode = "Explorer"

        self.logger.debug("Configuration mode is %s", conf_mode)
        return str(conf_mode)

    def run_sequence(self):
        """
        Name:     HardWorker.run_sequence
        Inputs:   None.
        Outputs:  None.
        Features: Run imaging sequence loop, emitting appropriate signals to
                  indicate imaging is 1) running, 2) ready, or 3) finished
        Depends:  end_sequence
        """
        if self.is_enabled:
            # Take single picture
            if self.mode == '2D':
                # Reset imaging properties and take photo:
                self.imaging.reset()
                img_vals = self.imaging.image()

                progress_val = self.imaging.position
                self.running.emit(progress_val, img_vals)
                self.ready.emit(img_vals)
                self.finished.emit()
            else:
                # Take image sequence:
                while (self.imaging.taken < self.imaging.num_photos):
                    # Make certain the motor is working and ready:
                    if not self.imaging.is_running:
                        try:
                            self.imaging.calibrate_motor()
                        except PRIDAPeripheralError as PPE:
                            self.logger.error("Calibration failed")
                            self.sendAlert.emit(str(PPE))
                            self.imaging.end_sequence()
                            self.finished.emit()
                            break
                        else:
                            self.imaging.reset_position()
                            self.imaging.is_running = True

                    # ~~~~~~~~~~~~~~~~~~~~~~~~~~
                    #      MUTEX LOCK
                    # ~~~~~~~~~~~~~~~~~~~~~~~~~~
                    if self.mutex is not None:
                        # Get mutex lock for uninterrupted motor movement:
                        self.logger.debug("creating a mutex locker")
                        locker = QMutexLocker(self.mutex)

                    # Move motor to next position and take a photo:
                    try:
                        self.imaging.step_to_next()
                    except PRIDAPeripheralError as PPE:
                        self.logger.error("Motor stepping failed")
                        self.sendAlert.emit(str(PPE))
                        self.imaging.end_sequence()
                        self.finished.emit()
                        break
                    except:
                        self.logger.exception(
                            "Encountered unknown error during motor stepping.")
                        self.sendAlert.emit(
                            ("Motor stepping has failed! Please check "
                             "your log statements for details."))
                        self.imaging.end_sequence()
                        self.finished.emit()
                        break
                    else:
                        img_vals = self.imaging.image()
                        if not self.imaging.is_error:
                            # ~~~~~~~~~~~~~~~~~~~~~~~~~~
                            #      MUTEX UNLOCK
                            # ~~~~~~~~~~~~~~~~~~~~~~~~~~
                            if self.mutex is not None:
                                self.logger.debug("releasing mutex locker")
                                locker.unlock()

                            # Progress value is between 0 and 360 degrees:
                            progress_val = self.imaging.position
                            self.running.emit(progress_val, img_vals)
                            self.ready.emit(img_vals)
                        else:
                            self.sendAlert.emit(
                                ("An error occurred during imaging! "
                                 "Please check log statements for details"))
                            self.imaging.end_sequence()
                            self.finished.emit()
                            break

                # All images taken; release lock and go to end sequence:
                if self.imaging.is_running:
                    self.imaging.end_sequence()
                    self.finished.emit()
        else:
            self.logger.warning("failed to run sequence; hardware not enabled")
            self.sendAlert.emit("Hardware Mode is unavailable!")

    def take_preview_photo(self):
        """
        Name:     HardWorker.take_preview_photo
        Inputs:   None.
        Outputs:  str, photo path
        Features: Returns the image path for a preview photo captured by
                  imaging's camera
        """
        if self.is_enabled:
            self.logger.debug("taking test photo")
            try:
                img_path = self.imaging.test_photo()
            except PRIDAPeripheralError as PPE:
                self.logger.error("Preview photo failed")
                self.sendAlert.emit(str(PPE))
            except:
                self.logger.exception("Preview photo failed!")
                self.sendAlert.emit(
                    "Failed to take preview photo. Please check camera.")
            else:
                return img_path

    def test_gear_ratio(self):
        """
        Name:     HardWorker.test_gear_ratio
        Inputs:   None.
        Outputs:  None.
        Features: Runs the motor for the total number of steps for full
                  rotation
        """
        if self.is_enabled:
            # Engaging step motor in this way circumvents the ramping function:
            for i in range(self.total_steps):
                self.imaging.step_motor()
            self.imaging.reset()

    def test_motor(self, num_steps):
        """
        Name:     HardWorker.test_motor
        Inputs:   int, number of steps (num_steps)
        Outputs:  None.
        Features: Runs a test on the motor for a given number of steps

        NOTE: Does this need a call to turn_off_motors? YES!
        """
        if self.is_enabled:
            self.imaging.step_motor(num_steps)
            self.imaging.reset()


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    # Instantiating logging handler and record format:
    root_handler = logging.FileHandler("workers.log")
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)
