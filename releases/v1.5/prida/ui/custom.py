#!/usr/bin/python
#
# custom.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software is freely available to the public for use.      #
# The Department of Agriculture (USDA) and the U.S. Government have not       #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     USDA-Agricultural Research Service                                      #
#     Robert W. Holley Center for Agriculture and Health                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging

import numpy
from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPixmap
import PIL.ImageQt as ImageQt
import PIL.Image as Image

from .pilutil import imresize


###############################################################################
# CLASSES:
###############################################################################
class IDA(QLabel):
    """
    Name:     IDA (Image Display Area)
    Features: The label that displays images in rescalable size
    History:  Version 1.5.0
              - removed scipy.misc dependency [16.07.25]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None):
        """
        Name:     IDA.__init__
        Inputs:   parent object (parent)
        Outputs:  None.
        Features: Initializes IDA
        """
        super(IDA, self).__init__(parent)

        self.logger = logging.getLogger(__name__)
        self.logger.debug('initializing IDA class')

        self.base_array = None  # numpy array of original color image
        self.cur_pixmap = None  # QPixmap
        self.cur_qimage = None  # ImageQt

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def isGrayscale(self):
        """Boolean for grayscale preview images"""
        if self.cur_qimage is not None:
            return self.cur_qimage.isGrayscale()
        else:
            return False

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def clearBaseImage(self, text=""):
        """
        Name:     IDA.clearBaseImage
        Inputs:   None.
        Outputs:  None.
        Features: Clears the IDA base image and all labels
        """
        self.logger.debug("clearing base image")
        self.clear()
        self.base_array = None
        self.cur_pixmap = None
        self.cur_qimage = None
        self.setText(text)

    def export_to_file(self, file_path, as_gray=True):
        """
        Name:     IDA.export_to_file
        Inputs:   - str, image file name with path (file_path)
                  - bool, whether to save as grayscale, i.e., mode=L (as_gray)
        Outputs:  None.
        Features: Tries to save the current base array to file
        """
        try:
            if as_gray:
                image = Image.fromarray(self.base_array[:, :, 0])
            else:
                image = Image.fromarray(self.base_array)
        except:
            self.logger.exception("Failed to create PIL image")
        else:
            self.logger.debug(
                "created PIL image from array using mode %s", image.mode)
            try:
                image.save(file_path)
            except:
                self.logger.exception("Failed to save image")
            else:
                self.logger.debug("Image saved successfully to %s", file_path)

    def resizeAndSet(self, img_array):
        """
        Name:     IDA.resizeAndSet
        Inputs:   numpy.ndarray, array for resizing and setting (img_array)
        Outputs:  None.
        Features: Resizes and sets ImageQt from base image
        """
        img_dim = len(img_array.shape)
        if img_dim == 3:
            try:
                self.logger.debug("checking dimensions of RGB image")
                h, w, _ = img_array.shape
            except:
                self.logger.exception("something is awry")
                raise
        elif img_dim == 2:
            try:
                self.logger.debug("checking dimensions on grayscale image")
                h, w = img_array.shape
            except:
                self.logger.exception("something is awry")
                raise
        else:
            self.logger.warning(
                "Can not handle image with dimension %d", img_dim)
            raise ValueError("Unsupported image dimension %d", img_dim)

        # Calculate the required size of image:
        self.logger.debug("resizing image")
        s_factor = numpy.array(
            [float(self.height())/h, float(self.width())/w]).min()
        self.logger.debug("IDA resize factor set to %f", s_factor)

        self.logger.debug("resizing image...")
        nd_array = imresize(img_array, s_factor)
        self.logger.debug("...resize complete")

        image = Image.fromarray(nd_array)
        self.logger.debug(
            "created PIL image from array using mode %s", image.mode)

        self.logger.debug("setting ImageQt...")
        qt_image = ImageQt.ImageQt(image)
        self.cur_qimage = qt_image
        self.logger.debug(
            "ImageQt is grayscale %s", qt_image.isGrayscale())

        self.logger.debug("setting QPixmap...")
        self.cur_pixmap = QPixmap.fromImage(self.cur_qimage)
        self.logger.debug("...setting Pixmap image")
        self.setPixmap(self.cur_pixmap)
        self.logger.debug("...pixmap set is complete")

    def resizeEvent(self, e):
        """
        Name:     IDA.resizeEvent
        Inputs:   event (e)
        Outputs:  None.
        Features: Upon resize events, resize and set image
        Depends:  resizeAndSet
        """
        self.logger.debug('start.')
        if self.base_array is not None:
            self.resizeAndSet(self.base_array)
        self.logger.debug('complete.')

    def rotateBaseImageLeft(self):
        """
        Name:     IDA.rotateBaseImageLeft
        Inputs:   None.
        Outputs:  None.
        Features: Rotates base image 90 degrees counter-clockwise
        Depends:  resizeAndSet
        """
        self.logger.info('Button Clicked')
        if self.base_array is not None:
            self.logger.debug('rotating')
            self.base_array = numpy.rot90(self.base_array)
            self.resizeAndSet(self.base_array)

    def rotateBaseImageRight(self):
        """
        Name:     IDA.rotateBaseImageRight
        Inputs:   None.
        Outputs:  None.
        Features: Rotates base image 90 degrees clockwise
        Depends:  resizeAndSet
        """
        self.logger.info("Button Clicked")
        if self.base_array is not None:
            self.logger.debug("rotating")
            self.base_array = numpy.rot90(self.base_array, 3)
            self.resizeAndSet(self.base_array)

    def setBaseImage(self, ndarray_full):
        """
        Name:     IDA.setBaseImage
        Inputs:   numpy.ndarray, image (ndarray_full)
        Outputs:  None.
        Features: Sets and saves base image for viewing
        Depends:  resizeAndSet
        """
        self.logger.debug("setting base array")
        img_dim = len(ndarray_full.shape)

        # Add binary image support:
        if ndarray_full.max() == 1 and img_dim == 2:
            self.logger.debug("found binary image!")
            self.logger.debug("scaling binary pixels for grayscale")
            ndarray_full[numpy.where(ndarray_full == 1)] *= 255
            self.logger.debug("pixel scaling complete")

        if img_dim == 2:
            try:
                self.logger.debug("checking dimensions on grayscale image")
                temp_array = numpy.zeros(
                    (ndarray_full.shape[0], ndarray_full.shape[1], 3))
                temp_array[:, :, 0] = numpy.copy(ndarray_full)
                temp_array[:, :, 1] = numpy.copy(ndarray_full)
                temp_array[:, :, 2] = numpy.copy(ndarray_full)
            except:
                self.logger.exception("something is awry")
                raise
            else:
                self.base_array = temp_array.astype('uint8')
                self.resizeAndSet(self.base_array)
        elif img_dim == 3:
            self.base_array = ndarray_full.astype('uint8')
            self.resizeAndSet(self.base_array)
        else:
            self.logger.warning(
                "Can not handle image with dimension %d", img_dim)
            raise ValueError("Unsupported image dimension %d", img_dim)
