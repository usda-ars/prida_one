#!/usr/bin/python
#
# imutil.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################

###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os

import numpy
import PIL.Image as Image
import rawpy

from .ui.pilutil import imread
from .ui.pilutil import imresize


###############################################################################
# FUNCTIONS:
###############################################################################
def imopen(img_path):
    """
    Name:     imopen
    Inputs:   str, path to image file (img_path)
    Outputs:  numpy.ndarray, image array
    Features: Extends the PIL util imread for raw image types
    Depends:  imread
    """
    if not os.path.isfile(img_path):
        err_msg = "'%s' does not exist!" % (img_path)
        raise IOError(err_msg)

    img_name = os.path.basename(img_path)
    img_base, img_ext = os.path.splitext(img_name)

    if img_ext in ['.NEF', '.nef']:
        # Use libraw image reader:
        raw = rawpy.imread(img_path)
        img = raw.postprocess()
    else:
        # Use scipy PIL image reader:
        img = imread(img_path)

    return img


def is_binary(nd_array):
    """
    Name:     is_binary
    Inputs:   numpy.ndarray, image array (nd_array)
    Outputs:  bool
    Features: Returns whether an image array is binary
    """
    # Check #1: is array:
    if not isinstance(nd_array, numpy.ndarray):
        return False

    # Check #2: is 2D array:
    img_dim = len(nd_array.shape)
    if img_dim != 2:
        return False

    try:
        im = Image.fromarray(nd_array)
    except:
        return False
    else:
        if im.mode != 'L':
            return False
        elif len(im.getcolors()) != 2:
            return False
        else:
            # binary image only has two colors:
            return True


def is_grayscale(nd_array):
    """
    Name:     is_grayscale
    Inputs:   numpy.ndarray, image array (nd_array)
    Outputs:  bool
    Features: Returns whether an image is grayscale
    """
    if not isinstance(nd_array, numpy.ndarray):
        return False

    img_dim = len(nd_array.shape)
    if img_dim != 2:
        return False

    try:
        im = Image.fromarray(nd_array)
    except:
        return False
    else:
        if im.mode == 'L':
            return True
        else:
            return False


def is_rgb(nd_array):
    """
    Name:     is_rgb
    Inputs:   numpy.ndarray, image array (nd_array)
    Outputs:  bool
    Features: Returns whether an image array is RGB
    NOTE 1:   Some color TIFF image files have four color bands where color
              band 4 (alpha) is all 255's!
    NOTE 2:   Use im = Image.fromarray(nd_array) and check im.mode against
              'RGB' or 'RGBA'
    """
    # Check 1: is array:
    if not isinstance(nd_array, numpy.ndarray):
        return False

    try:
        im = Image.fromarray(nd_array)
    except:
        return False
    else:
        if im.mode in ("RGB", "RGBA"):
            return True
        else:
            return False


def rel_crop(nd_array, tpct, rpct, bpct, lpct):
    """
    Name:     rel_crop
    Inputs:   - numpy.ndarray, original image array (nd_array)
              - int, percentage to crop from top, 0--50 (tpct)
              - int, percentage to crop from right, 50--100 (rpct)
              - int, percentage to crop from bottom, 50--100 (bpct)
              - int, percentage to crop from left, 0--50 (rpct)
    Outputs:  numpy.ndarray, cropped image array
    Features: Returns a cropped image array
    """
    im_shape = nd_array.shape
    shape_len = len(im_shape)
    if shape_len == 3:
        h, w, d = im_shape
    elif shape_len == 2:
        h, w, = im_shape
        d = None

    tidx = int(h*(tpct/100.0))
    lidx = int(w*(lpct/100.0))
    bidx = int(h*(bpct/100.0) - 1)
    ridx = int(w*(rpct/100.0) - 1)
    logging.debug("crop extents: %d, %d, %d, %d" % (tidx, ridx, bidx, lidx))

    if tidx >= 0 and tidx < bidx and bidx < h:
        if lidx >= 0 and lidx < ridx and ridx < w:
            if d is not None:
                tmp_array = nd_array[tidx:bidx, lidx:ridx, :]
            else:
                tmp_array = nd_array[tidx:bidx, lidx:ridx]
            return tmp_array
        else:
            raise IndexError("Left/right index out of range!")
    else:
        raise IndexError("Top/bottom index out of range!")


def rgb_to_gray(nd_array):
    """
    Name:     rgb_to_gray
    Inputs:   numpy.ndarray, RGB color image array (nd_array)
    Outputs:  numpy.ndarray, 2D grayscale image array
    Features: Converts color image to grayscale based on a weighted sum
              of the color bands as a calculation of luminance
    Depends:  is_rgb
    """
    if is_rgb(nd_array):
        R = nd_array[:, :, 0]
        G = nd_array[:, :, 1]
        B = nd_array[:, :, 2]
        Y = (0.299 * R) + (0.587 * G) + (0.114 * B)
        return Y
    else:
        img_dim = len(nd_array.shape)
        if img_dim == 2:
            print("Already grayscale.")
            return nd_array
        else:
            raise TypeError("Image array is not RGB!")


def scale_image(nd_array, sfactor):
    """
    Name:     scale_image
    Inputs:   - numpy.ndarray, unscaled image array (nd_array)
              - int, scaling factor (sfactor)
    Returns:  numpy.ndarray, scaled image array
    Features: Returns a scaled numpy array
    Depends:  imresize
    """
    if sfactor != 100:
        try:
            # Try scaling factor as int:
            my_array = imresize(nd_array, sfactor)
        except:
            try:
                # Try scaling factor as float:
                sfactor = float(sfactor) / 100.0
                my_array = imresize(nd_array, sfactor)
            except:
                # Scaling failed, return original array:
                logging.warning("Scaling failed; returning original array")
                return nd_array
            else:
                return my_array
        else:
            return my_array
    else:
        # Skip scaling (not necessary)
        return nd_array
