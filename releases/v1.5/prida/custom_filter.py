#!/usr/bin/python
#
# custom_filter.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging


###############################################################################
# CLASS DEFINITION:
###############################################################################
class CustomFilter(logging.Filter):

    def __init__(self, myLevel=logging.DEBUG):
        # Create custom filter instance
        super(CustomFilter, self).__init__()
        self.myLevel = myLevel

    def filter(self, record):
        """
        @OVERRIDES logging.Filter.filter
        Name:    CustomFilter.filter
        Feature: selects logs for emitting based on their log level.
        Inputs:  LogRecord, an instance of the LogRecord class (record)
        Outputs: bool, boolean indicating to handler whether or not to emit
                 record
        """
        if record.levelno >= self.myLevel:
            return True
        else:
            return False
