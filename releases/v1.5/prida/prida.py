#!/usr/bin/env python3
#
# prida.py
#
# VERSION 1.5.0
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software is freely available to the public for use.      #
# The Department of Agriculture (USDA) and the U.S. Government have not       #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     USDA-Agricultural Research Service                                      #
#     Robert W. Holley Center for Agriculture and Health                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import logging
import logging.handlers
import os
import sys

import numpy
from PyQt5.QtWidgets import QAction
from PyQt5.QtWidgets import QAbstractItemView
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QCompleter
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QInputDialog
from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QMenu
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QKeySequence
from PyQt5.QtGui import QMovie
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QDate
from PyQt5.QtCore import QMutex
from PyQt5.QtCore import QRegExp
from PyQt5.QtCore import QSize
from PyQt5.QtCore import QStringListModel
from PyQt5.QtCore import QThread
from PyQt5.QtCore import Qt
import PIL.ImageQt as ImageQt
import PIL.Image as Image

from . import __version__
from .calibutil import calc_ir
from .calibutil import calc_xa
from .calibutil import calc_xg
from .calibutil import calc_xo
from .calibutil import calc_xw
from .calibutil import zt_err
from .imutil import imopen
from .imutil import is_binary
from .imutil import is_grayscale
from .imutil import rel_crop
from .resources import create_config_file
from .resources import resource_path
from .utilities import find_image_files
from .utilities import img_types
from .utilities import img_import_str
from .utilities import read_exif_tags
from .utilities import get_ctime
from .data_worker import DataWorker
from .hard_worker import HardWorker
from .image_worker import ImageWorker
from .ui.mainwindow import Ui_MainWindow
from .ui.about import Ui_About
from .ui.export_as import Ui_ExportAs
from .ui.input_calib import Ui_CalibInput
from .ui.input_user import Ui_Dialog
from .ui.tags import Ui_TagInfo


###############################################################################
# GLOBAL VARIABLES:
###############################################################################
# PAGE NUMBER ENUMS FOR mainwindow.ui
MAIN_MENU = 0
VIEWER = 1
INPUT_EXP = 2
RUN_EXP = 3
FILE_SEARCH = 4
ANALYSIS = 5

# PAGE NUMBER ENUMS FOR SHEET/IDA
SHEET_VIEW = 0
IDA_VIEW = 1

# PAGE NUMBER ENUMS FOR ATTR SHEETS
SHEET_ATTR = 0
PHOTO_ATTR = 1


###############################################################################
# FUNCTIONS:
###############################################################################
def exit_app(my_app):
    """
    Name:     exit_app
    Inputs:   object, QApplication (my_app)
    Outputs:  None
    Features: Graceful exiting of QApplication
    """
    logging.info('exiting QApplication')
    my_app.shutdown()


###############################################################################
# CLASSES:
###############################################################################
class Prida(QApplication):
    """
    Prida, a QApplication.

    Name:      Prida
    History:   Version 1.5.0
               - removed scipy.misc dependency [16.07.25]
               - created compress file signal [16.08.03]
               - created create compressed copy function [16.08.03]
               - changed perform_analysis to calibrate [16.08.15]
               - created calib functions for handling popup [16.08.16]
               - created distance input validator [16.08.16]
               - fixed unhandled analysis button exceptions [16.08.30]
               - created camera and calibration class vars [16.08.30]
               - updated calibration combo inputs [16.08.30]
               - started calibration parameter handling [16.08.30]
               - added calibration parameter, Xo [16.08.31]
               - created convenience function to set qlabel text [16.08.31]
               - created to calibrate signal [16.08.31]
               - created load images function [16.09.07]
               - created configure analysis function [16.09.08]
               - added D200 and D600 to camera list [16.09.09]
               - change to VIEWER during file open [16.09.15]
               - img types and img import str now from utilities [16.09.15]
               - changed pixmap read method in update progress [16.10.26]
               - added rotate image buttons to analysis [16.10.26]
               - moved load images to end of init w/ status update [16.11.30]
               - created configuration menu (needs updated) [16.11.30]
               - created integer validator for number of images [16.12.01]
               - check for empty PID and image number before imaging [16.12.01]
               - created additional config menu items [16.12.01]
               - disabled config menu items based on hardware mode [16.12.01]
               - lowered a logging level to debug [16.12.19]
               - started take image preview menu action [17.01.03]
               - added config data compression level action [17.01.03]
               - added config camera directory action [17.01.03]
               - added take preview image action [17.01.03]
               - added test motor action [17.01.03]
               - added config motor speed and step type actions [17.01.04]
               - added config image settling time action [17.01.04]
               - added config gear ratio action [17.01.04]
               - created load ui function [17.01.05]
               - new QDialog for exporting options [17.01.05]
               - created get export directory function [17.01.05]
               - added variables to the toExport signal [17.01.05]
               - updated export function for exporting options [17.01.05]
               - added try catch around analysis preview image [17.01.05]
               - added photo sheet model for photo attributes [17.01.10]
               - created load photo sheet headers function [17.01.10]
               - removed exif tag reading [17.01.10]
               - created standard photo attributes dictionary [17.01.10]
               - created get photo attrs signal [17.01.11]
               - added clear photo sheet when exiting IDA_VIEW [17.01.11]
               - created set session fields for labels during import [17.01.11]
               - added new tag info UI [17.01.12]
               - added get / show image tags functions [17.01.12]
               - created show first signal for analysis [17.01.18]
               - added run dsone function [17.01.18]
               - updated analysis signals [17.01.18]
               - added method 2 for analysis elements [17.01.18]
               - open ANALYSIS view to tab index 0 [17.01.18]
               - removed gapfill signals / sockets [17.01.18]
               - created undo filter button / signal [17.01.19]
               - created gear ratio test menu item [17.02.10]
               - increased gear ratio resolution to four decimals [17.02.10]
               - created last directory class variable [17.02.14]
               - removed error msg from image dir config [17.02.17]
               - added naming preference to export options [17.02.24]
               - created find image files utility function [17.02.27]
               - removed glob package import [17.02.27]
               - loading session text [17.02.28]
               - reduced color export options [17.03.13]
               - added new file and help menus [17.03.16]
               - created update menus function [17.03.16]
               - removed old buttons that are now menu items [17.03.16]
               - added mainwindow icon [17.03.16]
               - created menu action shortcuts [17.03.21]
               - added file version to about popup [17.03.21]
               - use rowCount in clear session sheet model [17.03.21]
               - added check for Nonetype qt vals in session attrs [17.03.21]
               - added EXIF tag search [17.03.28]
               - EXIF tag search focus and clear [17.03.28]
               - updated to imopen [17.03.29]
               - new signals for session cropping spin boxes [17.03.29]
               - new crop on and crop extents properties [17.03.29]
               - started new update crop functions [17.03.29]
               - changed crop options to relative percents [17.03.30]
               - assign crop options to thumbnails [17.03.30]
               - assign crop options to VIEWER's IDA [17.03.30]
               - assign crop options to export [17.03.30]
               - update progress now takes a metadata dictionary [17.04.04]
               - created new session property 'mode' [17.04.04]
               - moved preview base image clearing to back to sheet [17.04.04]
               - connected abort button to hardware thread [17.04.04]
               - UNDO abort [17.04.04]
               - update compression level config menu [17.04.05]
               - updated set greeter [17.04.05]
               - moved load images above helper classes [17.04.05]
                 * fixed errors thrown w/ receive alert function
               - updated start/stop spinner functions [17.04.05]
               - updated calibration parameter labels [17.04.13]
               - connected extract naming option signal [17.06.02]
               - created check file signal and socket [17.06.02]
               - changed name of calib filter to smoother [17.06.05]
               - added dust filter [17.06.05]
               - moved calib thread events to avoid async merging [17.06.05]
               - created get preview signals [17.06.08]
               - created open preview image menu action [17.06.08]
               - image tags now handle preview images [17.06.08]
               - show IDA now handles preview images [17.06.08]
               - thumbnail image count 9999 adds label 'Preview' [17.06.08]
               - updated take preview photo [17.06.08]
               - created open preview photo function [17.06.08]
               - created to preview function [17.06.08]
               - allow open preview photo in explorer mode [17.06.09]
               - added spinner to open preview photo [17.06.09]
    Notes:     * Clicking a command link button (e.g. Edit) while in VIEWER's
                 IDA (previewing a thumbnail) does not clear the base image.
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    # dictionaries
    usda_plants = {}

    # signals
    addSearch = pyqtSignal(str, int, int)
    checkFile = pyqtSignal()
    closeFile = pyqtSignal()
    compressFile = pyqtSignal(str)
    createSession = pyqtSignal(str, dict, dict)
    forAnalysis = pyqtSignal(str)
    getDataList = pyqtSignal()
    getIDA = pyqtSignal(str, int)
    getObject = pyqtSignal(int)
    getPhotoAttrs = pyqtSignal(str)
    getPreviewPhotoAttrs = pyqtSignal(str, int)
    getPreviewTags = pyqtSignal(str)
    getPreviewThumb = pyqtSignal(str, int)
    getSessionAttrs = pyqtSignal(str)
    getTags = pyqtSignal(str)
    getThumbs = pyqtSignal(str, int)
    imageCheck = pyqtSignal(list)
    newFile = pyqtSignal(str)
    openFile = pyqtSignal(str)
    removeSearch = pyqtSignal(str)
    removeObject = pyqtSignal(int)
    setAttr = pyqtSignal(str, str, str)
    setDataLV = pyqtSignal(int)
    setField = pyqtSignal(str, str, str, str)
    setHWDir = pyqtSignal(str)
    setPIDFields = pyqtSignal(str)
    setSessionDefault = pyqtSignal(str, str)
    setSessionFields = pyqtSignal(str)
    showFirst = pyqtSignal(dict)
    stopFile = pyqtSignal()
    toCalibrate = pyqtSignal(tuple)
    toCrop = pyqtSignal(int, int, int, int)
    toExport = pyqtSignal(str, str, dict)
    toFilter = pyqtSignal(int)
    toSmooth = pyqtSignal(int)
    toFind = pyqtSignal(numpy.ndarray)
    toFind2 = pyqtSignal(numpy.ndarray)
    toImport = pyqtSignal(list)
    toMerge = pyqtSignal(dict)
    toPlot = pyqtSignal()
    toRefresh = pyqtSignal()
    toSave = pyqtSignal(dict)
    toStretch = pyqtSignal()
    toThresh = pyqtSignal()
    toUndo = pyqtSignal()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, dict_filepath, my_parser=None, config_filepath=None):
        """
        Prida class initialization.

        Name:     Prida.__init__
        Inputs:   None.
        Outputs:  None.
        Depends:  - load_session_sheet_headers     - new_hdf
                  - new_session                    - open_hdf
                  - resize_sheet                   - show_ida
                  - to_sheet                       - update_progress
                  - update_session_sheet_model     - update_sheet_model
                  - set_defaults                   - about
                  - version_checker                - enable_all_attrs
                  - configure_analysis             - load_ui
        """
        QApplication.__init__(self, sys.argv)
        self.base = QMainWindow()

        # Create a logger for Prida:
        self.logger = logging.getLogger(__name__)

        # Save Prida directory path:
        if config_filepath is not None:
            self.prida_dir = os.path.dirname(config_filepath)
            self.prida_conf = os.path.basename(config_filepath)
        else:
            self.prida_dir = None
            self.prida_conf = None

        # Save dictionary file path:
        self.gs_dict = dict_filepath

        self.logger.debug("loading UI files")
        self.load_ui()

        # Define the last directory the user opened:
        self.last_dir = os.path.expanduser("~")

        # Display loading splash screen:
        self.view.welcome.setText(
            '<html><head/><body><p align="center">'
            '<span style="font-size:24pt; font-weight:600; color:#ffffff;">'
            'Loading Prida ... Please Wait'
            '</span></p></body></html>')

        # Show the GUI and disable buttons
        self.base.show()
        self.view.new_hdf.setEnabled(False)
        self.view.open_hdf.setEnabled(False)
        self.view.search_hdf.setEnabled(False)
        self.view.re_configure.setEnabled(False)

        # Create a non-recursive Qt mutex
        self.mutex = QMutex()

        # Check that the necessary image files exist:
        self.load_images()

        # Create data worker:
        self.logger.debug("creating data worker")
        self.data = DataWorker(mutex=self.mutex)
        self.data.create_hdf(my_parser, config_filepath)

        self.logger.debug("creating data thread")
        self.data_thread = QThread()
        self.data.moveToThread(self.data_thread)
        self.data_thread.start()
        self.data.finished.connect(self.data_thread.quit)

        # Create hardware worker:
        self.logger.debug("creating hardware worker")
        self.hardware = HardWorker(mutex=self.mutex)
        self.hardware.sendAlert.connect(self.receive_alert)
        self.hardware.report.connect(self.notice)

        self.logger.debug("calculating number of PID attributes")
        self.sheet_items = len(self.data.pid_attrs)

        # Connect session default value setter signal:
        self.setSessionDefault.connect(self.data.set_session_default)

        # Create the VIEWER sheet model (where PIDs and sessions are listed)
        self.logger.debug("creating sheet model")
        self.sheet_model = QStandardItemModel()
        self.view.sheet.setModel(self.sheet_model)
        self.view.sheet.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.sheet.collapsed.connect(lambda: self.resize_sheet())
        self.view.sheet.expanded.connect(lambda: self.resize_sheet())
        self.view.sheet.setAnimated(True)

        # Create the VIEWER session sheet model (for session properties)
        self.logger.debug("creating session sheet model")
        self.session_sheet_model = QStandardItemModel()
        self.view.session_sheet.setModel(self.session_sheet_model)
        # self.view.session_sheet.setRootIsDecorated(False)
        self.load_session_sheet_headers()

        # Create the VIEWER photo sheet model (for photo properties)
        self.logger.debug("creating photo sheet model")
        self.photo_sheet_model = QStandardItemModel()
        self.view.photo_sheet.setModel(self.photo_sheet_model)
        self.load_photo_sheet_headers()

        # Create FILE_SEARCH sheet model (where files + PIDs are listed)
        self.logger.debug("creating search sheet model")
        self.search_sheet_model = QStandardItemModel()
        self.view.file_search_sheet.setModel(self.search_sheet_model)
        self.load_file_search_sheet_headers()

        # Create the FILE_SEARCH file browse model (for displaying files)
        self.logger.debug("creating file sheet model")
        self.file_sheet_model = QStandardItemModel()
        self.view.file_browse_sheet.setModel(self.file_sheet_model)
        self.load_file_browse_sheet_headers()

        # Create VIEWER image sheet model (for thumbnail previewer):
        self.logger.debug("creating image sheet model")
        self.img_sheet_model = QStandardItemModel()
        self.view.img_select.setModel(self.img_sheet_model)
        self.view.img_select.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.view.img_select.setIconSize(QSize(100, 100))

        # @TODO - make this do something meaningful?
        self.view.abortButton.setEnabled(False)
        # self.view.abortButton.clicked.connect(self.hardware.cancel_imaging)

        # Attempt to initialize imaging/hardware mode:
        self.logger.debug("creating hardware's imaging")
        self.hardware.create_imaging(my_parser, config_filepath)

        self.hardware_mode = self.hardware.is_enabled
        self.logger.debug("Hardware Mode set to %s", self.hardware_mode)

        self.prida_title = (
            "Prida %s - %s Mode" % (__version__, self.hardware.mode))
        self.base.setWindowTitle(self.prida_title)

        # Set HARDWARE MODE options:
        if self.hardware_mode:
            self.logger.info("Hardware Mode Enabled")

            # Spawn a hardware thread if mode is enabled:
            self.logger.debug("creating hardware thread")
            self.hardware_thread = QThread()
            self.hardware_thread.started.connect(self.hardware.run_sequence)
            self.hardware_thread.finished.connect(self.end_progress)

            self.hardware.finished.connect(self.hardware_thread.quit)
            self.hardware.running.connect(self.update_progress)
            self.hardware.ready.connect(self.data.save_image)
            self.hardware.moveToThread(self.hardware_thread)

            # Disable "Import Data" button (only allow "Create Session")
            # and set VIEWER action for "OK" and "Create Session" buttons:
            self.view.import_data.setEnabled(False)
            self.view.create_session.clicked.connect(self.create_session)

            # Default single image for 2D mode and disable editing
            if self.hardware.mode == "2D":
                self.setSessionDefault.emit("num_img", str(1))
        else:
            self.logger.info("Preview Mode Enabled")

            # Disable "Create Session" button, set VIEWER action for
            # "Import Data" button, import data to file dialog and connections:
            self.view.create_session.setEnabled(False)
            self.view.import_data.clicked.connect(self.import_data)
            self.data.importing.connect(self.update_progress)
            self.data.importDone.connect(self.end_progress)
            self.toImport.connect(self.data.import_images)
            self.imageCheck.connect(self.data.check_image_shapes)
            self.data.checkedImages.connect(self.import_images)

        # Create image worker:
        self.logger.debug("creating image worker")
        self.image = ImageWorker()
        self.analysis_mode = self.image.is_enabled

        # Set ANALYSIS MODE options:
        if self.analysis_mode:
            self.logger.info("Analysis Mode Enabled")
            self.configure_analysis()
        else:
            self.logger.warning("Analysis Mode Disabled")
            self.view.calibrate.setEnabled(False)

        # Set MAIN_MENU actions for "New," "Open," & "Search" buttons:
        self.view.new_hdf.clicked.connect(self.new_hdf)
        self.view.open_hdf.clicked.connect(self.open_hdf)
        self.view.search_hdf.clicked.connect(self.search_hdf)
        self.view.re_configure.clicked.connect(self.reconfigure)

        # Set file signal and sockets:
        self.newFile.connect(self.data.new_file)
        self.openFile.connect(self.data.open_file)
        self.data.fileOpened.connect(self.hdf_opened)
        self.closeFile.connect(self.data.close_file)
        self.data.fileClosed.connect(self.back_to_main)
        self.stopFile.connect(self.data.stop)
        self.checkFile.connect(self.data.check_file)
        self.data.fileChecked.connect(self.check_complete)

        # Set VIEWER actions for "Main Menu," "About," and "Edit" buttons
        # and for edited search:
        # self.view.aboutButton.clicked.connect(self.about)
        # self.view.menuButton.clicked.connect(self.back_to_menu)
        self.view.make_edit.clicked.connect(self.edit_field)
        self.view.search.textEdited.connect(self.update_sheet_model)
        self.view.tagButton.clicked.connect(self.get_tags)

        self.getTags.connect(self.data.get_tags)
        self.data.tagsGotten.connect(self.show_tags)

        # Export data to file dialog and connections:
        self.toExport.connect(self.data.extract_to_file)
        self.view.export_data.clicked.connect(self.export)

        # Connect compression and return signal
        # self.view.saveButton.clicked.connect(self.data.compress_file)
        # self.view.saveButton.clicked.connect(self.save_compressed_copy)
        self.compressFile.connect(self.data.compress_file)
        self.data.isbusy.connect(self.start_spinner)
        self.data.donebusy.connect(self.stop_spinner)
        self.data.report.connect(self.notice)

        # Custom context menu for preview image "Save As"
        self.view.page.setContextMenuPolicy(Qt.CustomContextMenu)
        self.view.page.customContextMenuRequested.connect(
            self.show_context_menu)

        # Set IDA_SHEET actions for "Back to Spreadsheet," "Rotate Left," and
        # "Rotate Right" buttons:
        self.view.to_sheet.clicked.connect(self.to_sheet)
        self.view.rotateLeft.clicked.connect(self.view.ida.rotateBaseImageLeft)
        self.view.rotateRight.clicked.connect(
            self.view.ida.rotateBaseImageRight)

        # Set IDA signals and sockets:
        self.getIDA.connect(self.data.get_ida)
        self.data.idaSet.connect(self.set_ida)

        # Set INPUT_EXP action for "OK", "Cancel" button; connect the image
        # cropping checkbox for enable/disable crop widget; and update the
        # image orientation and exclude combo boxes:
        self.view.c_buttons.accepted.connect(self.new_session)
        self.view.c_buttons.rejected.connect(self.cancel_input)
        self.view.cropBox.clicked.connect(self.update_cropbox)
        self.view.c_crop_top.valueChanged.connect(self.update_crop_spinbox)
        self.view.c_crop_bottom.valueChanged.connect(self.update_crop_spinbox)
        self.view.c_crop_left.valueChanged.connect(self.update_crop_spinbox)
        self.view.c_crop_right.valueChanged.connect(self.update_crop_spinbox)
        self.update_orient_combo()
        self.update_exclude_combo()

        # Create selection models for VIEWER sheet and img_sheet and for
        # FILE_SELECT file_browser_sheet:
        self.logger.debug("creating selection models")
        self.sheet_select_model = self.view.sheet.selectionModel()
        self.img_sheet_select_model = self.view.img_select.selectionModel()
        self.file_select_model = self.view.file_browse_sheet.selectionModel()

        # Set selection model actions for selection changes:
        # (i.e., when a PID/session or a thumbnail in VIEW is clicked)
        self.img_sheet_select_model.selectionChanged.connect(self.show_ida)
        self.sheet_select_model.selectionChanged.connect(
            self.update_session_sheet_model
        )

        # Set FILE_SEARCH actions for "Main Menu," "+/-," and "Explore" buttons
        # and action for when search text is edited:
        # self.view.menuButton2.clicked.connect(self.back_to_menu)
        self.view.add_file.clicked.connect(self.search_file_addition)
        self.view.rm_file.clicked.connect(self.search_file_removal)
        self.view.explore_file.clicked.connect(self.explore_file)
        self.view.f_search.textEdited.connect(self.update_file_search_sheet)

        # Connect FILE_SEARCH signals and sockets:
        self.addSearch.connect(self.data.search_file_addition)
        self.removeSearch.connect(self.data.search_file_removal)
        self.data.searchAdded.connect(self.update_search_additions)
        self.data.searchRemoved.connect(self.update_search_removals)

        # Create an auto-completion model for PIDs and set action for edited
        # PID text:
        self.logger.debug("creating PID auto-completion model")
        self.pid_completer = QCompleter()
        self.view.c_pid.setCompleter(self.pid_completer)
        self.pid_auto_model = QStringListModel()
        self.pid_completer.setModel(self.pid_auto_model)
        self.pid_completer.activated.connect(self.data.autofill_pid)

        # Connect signals and sockets:
        self.setField.connect(self.data.get_input_field)
        self.data.fieldSet.connect(self.update_input_field)
        self.getDataList.connect(self.data.get_data_list)
        self.data.dataListed.connect(self.update_data)
        self.getPhotoAttrs.connect(self.data.get_photo_attrs)
        self.data.photoAttrs.connect(self.set_photo_sheet_model)
        self.getSessionAttrs.connect(self.data.get_session_attrs)
        self.data.sessionAttrs.connect(self.set_session_sheet_model)
        self.createSession.connect(self.data.create_session)
        self.getThumbs.connect(self.data.get_thumbs)
        self.data.addThumb.connect(self.set_sheet_thumbs)
        self.setPIDFields.connect(self.data.set_pid_fields)
        self.setSessionFields.connect(self.data.set_session_fields)
        self.setAttr.connect(self.data.set_attr)
        self.setDataLV.connect(self.data.set_clevel)

        # Connect signals/sockets for take preview image:
        self.getPreviewPhotoAttrs.connect(self.data.get_preview_attrs)
        self.getPreviewTags.connect(self.data.get_preview_tags)
        self.getPreviewThumb.connect(self.data.get_preview_thumb)

        # Create an auto-completion model for dictionary fields:
        self.logger.debug("creating dictionary auto-completion model")
        self.dict_completer = QCompleter()
        self.view.c_gen_sp.setCompleter(self.dict_completer)
        self.dict_auto_model = QStringListModel()
        self.dict_completer.setModel(self.dict_auto_model)
        self.dict_completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.dict_completer.activated.connect(self.autofill_dict)

        # Create an auto-completion model for USDA PLANTS:
        self.logger.debug("creating USDA PLANTS auto-completion model")
        self.plants_completer = QCompleter()
        self.view.c_usda_plants.setCompleter(self.plants_completer)
        self.plants_auto_model = QStringListModel()
        self.plants_completer.setModel(self.plants_auto_model)
        self.plants_completer.activated.connect(self.autofill_plants)

        # Build the USDA PLANTS database and update the auto-completion models:
        self.build_plants_db()
        self.update_dict_auto_model()

        # Create integer validator and use on number of images input:
        self.int_regex = QRegExp("^\d+$")
        self.int_validator = QRegExpValidator(self.int_regex, self.base)
        self.view.c_num_images.setValidator(self.int_validator)

        # Initialize the search dictionaries, list of PID metadata, list of
        # thumbnail images, session create/edit boolean, input field defaults,
        # whether an image is being previewed, and the current image:
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.img_list = []
        self.crop_extents = {'bottom': 0, 'left': 0, 'right': 0, 'top': 0}
        self.crop_on = False           # handles session image batch crop
        self.editing = False           # handles edits from new session
        self.preview = False           # if preview is already set
        self.a_session = None          # analysis session path
        self.attr_dict = {}            # configurable parameter dictionary
        self.tag_dict = {}             # EXIF tag search dictionary
        # self.current_img = None      # not currently used

        # Enable the appropriate input fields:
        # NOTE: needs to be after editing flag is defined
        self.enable_all_attrs()

        # Create configuration menus:
        self.create_menus()
        self.update_menus()

        # Set exit register:
        atexit.register(exit_app, self)

        # Check configuration file version against software:
        use_defaults = self.version_checker(config_filepath)

        # Parse configuration file is version is okay:
        if not use_defaults:
            if my_parser is not None and config_filepath is not None:
                my_parser(self, __name__, config_filepath)

        # End loading splash screen:
        self.set_greeter()

        # Re-Enable Buttons
        self.view.new_hdf.setEnabled(True)
        self.view.open_hdf.setEnabled(True)
        self.view.search_hdf.setEnabled(True)
        self.view.re_configure.setEnabled(True)

        # Update status:
        self.base.statusBar().showMessage("Welcome!", 2000)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def about(self):
        """
        Name:     Prida.about
        Inputs:   None.
        Outputs:  None.
        Features: Produces the file's About popup dialog box
        """
        self.logger.info("Button Clicked")
        self.logger.debug("getting about info")
        overview = self.data.about

        # Set popup values to default string if about fails:
        self.logger.debug("assigning about fields")
        self.about_ui.version_about.setText("%s" % (__version__))
        self.about_ui.author_about.setText(overview.get("Author", "Unknown"))
        self.about_ui.contact_about.setText(overview.get("Contact", "Unknown"))
        self.about_ui.plants_about.setText(overview.get("Plants", "?"))
        self.about_ui.sessions_about.setText(overview.get("Sessions", "?"))
        self.about_ui.photos_about.setText(overview.get("Photos", "?"))
        self.about_ui.summary_about.setText(overview.get("Summary", "?"))
        self.about_ui.fversion_about.setText(overview.get("Version", "None"))
        self.about_dialog.setWindowTitle("About: %s" % (self.data.basename))
        if self.about_dialog.exec_():
            self.about_dialog.update()

    def autofill_dict(self, value=''):
        """
        Name:     Prida.autofill_dict
        Inputs:   [optional] str, Genus species (value)
        Outputs:  None.
        Features: Autofills the USDA PLANTS ID for a given Genus sp.
        Depends:  set_input_field
        """
        for k, v in self.usda_plants.items():
            if value == v:
                try:
                    self.set_input_field("c_usda_plants", "QLineEdit", str(k))
                except:
                    self.logger.exception(
                        "could not set QLineEdit c_usda_plants")

    def autofill_plants(self, key=''):
        """
        Name:     Prida.autofill_plants
        Inputs:   [optional] str, USDA PLANTS ID (key)
        Outputs:  None.
        Features: Autofills the Genus species for a given USDA PLANTS ID
        Depends:  set_input_field
        """
        if key in self.usda_plants.keys():
            try:
                self.set_input_field(
                    "c_gen_sp", "QLineEdit", str(self.usda_plants[key]))
            except:
                self.logger.exception("could not set QLineEdit c_gen_sp")

    def back_to_main(self):
        """
        Name:     Prida.back_to_main
        Inputs:   None
        Outputs:  None
        Features: Returns to the menu screen, closing the HDF5 file handle and
                  clearing session and sheet data.
        Depends:  - clear_session_sheet_model
                  - init_pid_attrs
                  - init_session_attrs
                  - reset_cropbox
                  - update_file_browse_sheet
                  - update_file_search_sheet
                  - update_menus
                  - update_sheet_model
        """
        # Update menu:
        self.update_menus()
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.search_files = {}
        self.search_data = {}
        self.data_list = []
        self.editing = False
        self.reset_cropbox()
        self.image.reset()
        self.user_ui.user.setText("")
        self.user_ui.email.setText("")
        self.user_ui.about.setText("")
        self.update_sheet_model()
        self.update_file_browse_sheet()
        self.update_file_search_sheet()
        self.set_greeter()
        self.view.stackedWidget.setCurrentIndex(MAIN_MENU)
        self.logger.info("view changed to MAIN_MENU")
        self.base.setWindowTitle(self.prida_title)

    def back_to_menu(self):
        """
        Name:     Prida.back_to_menu
        Inputs:   None
        Outputs:  None
        Features: Signals data to close, which triggers back to main
        """
        self.logger.info("Button Clicked")
        self.logger.info("signaling data to close file")
        self.closeFile.emit()

    def back_to_sheet(self):
        """
        Name:     Prida.back_to_sheet
        Inputs:   None
        Outputs:  None
        Features: Clears session and image sheets in VIEWER, clears IDAs,
                  updates the sheet model, and changes current view to VIEWER
        Depends:  - clear_session_sheet_model
                  - update_sheet_model
        """
        self.logger.debug("returning back to sheet")
        self.view.a_preview.clearBaseImage("Image Display Area")
        self.view.c_preview.clearBaseImage()
        self.view.welcome.clearBaseImage('Welcome to Prida')
        self.clear_session_sheet_model()
        self.img_sheet_model.clear()
        self.update_sheet_model()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.logger.info("view changed to SHEET_VIEW")
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.logger.info("view changed to VIEWER")
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def build_plants_db(self):
        """
        Name:     Prida.build_plants_db
        Inputs:   None.
        Outputs:  None.
        Features: Reads the dictionary file and parses contents into the
                  usda_plants dictionary; if no USDA PLANTS IDs found,
                  assumes generic numbering

        @TODO:    add mayday for missing or bad dictionary entries
        """
        unk_idx = 0
        try:
            with open(self.gs_dict) as f:
                my_dict = f.read().splitlines()
        except IOError:
            self.logger.warning("dictionary file not found")
        else:
            self.logger.info("read file contents to dictionary list")
            for item in my_dict:
                my_items = item.split(":")
                if len(my_items) == 2:
                    try:
                        my_keys = eval("%s" % my_items[0])
                    except NameError:
                        self.logger.exception(
                            "bad dictionary file entry '%s', check formatting",
                            item)
                    else:
                        for key in my_keys:
                            self.logger.debug("adding '%s' to database", key)
                            self.usda_plants[str(key)] = str(my_items[1])
                else:
                    self.logger.debug(
                        "USDA PLANTS ID not found, adding '%s' to database",
                        unk_idx)
                    self.usda_plants[str(unk_idx)] = item
                    unk_idx += 1

    def calib_back(self):
        """
        Name:     Prida.calib_back
        Inputs:   None.
        Outputs:  None.
        Features: Moves the stackedWidget in the calib_ui back one page and
                  sets the focus to the input field
        """
        cur_page = self.calib_ui.pages.currentIndex()
        self.calib_ui.pages.setCurrentIndex(cur_page - 1)

    def calib_next(self):
        """
        Name:     Prida.calib_next
        Inputs:   None.
        Outputs:  None.
        Features: Moves the stackedWidget in the calib_ui forward one page and
                  sets the focus to the input field
        """
        cur_page = self.calib_ui.pages.currentIndex()
        self.calib_ui.pages.setCurrentIndex(cur_page + 1)

    def calib_set_preview(self, checked=False):
        """
        Name:     Prida.calib_set_preview
        Inputs:   [optional] bool, checked---required by QRadioButton signal
        Outputs:  None.
        Features: Changes the preview image for distance from camera to tank
                  based on user selection
        """
        if (self.calib_ui.c_to_center.isChecked() and
                self.calib_dist_center is not None):
            self.pix_dist = QPixmap(self.calib_dist_center).scaledToWidth(300)
            self.calib_ui.tank_dist.setPixmap(self.pix_dist)
        elif (self.calib_ui.c_to_wall.isChecked() and
                self.calib_dist_wall is not None):
            self.pix_dist = QPixmap(self.calib_dist_wall).scaledToWidth(300)
            self.calib_ui.tank_dist.setPixmap(self.pix_dist)

    def calibrate(self):
        """
        Name:     Prida.calibrate
        Inputs:   None.
        Outputs:  None.
        Features: Saves the selected session for analysis; signals for analysis
                  label updates; clears ANALYSIS view previews and labels;
                  zeros the progress bar; changes current view to ANALYSIS;
                  starts analysis thread
        Signals:  - forAnalysis
                  - analysis_thread.start
        Depends:  - mayday
                  - update_analysis_progress
        TODO:     _ check if get_sheet_model_selection can be used here
        """
        self.logger.info("Button Clicked")
        if self.sheet_select_model.hasSelection():
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0])

            if my_selection.parent() is not None:
                # Get session path:
                pid = my_selection.parent().text()
                session = my_selection.text()
                s_path = "/%s/%s" % (pid, session)
                self.a_session = s_path

                self.view.a_preview.clearBaseImage("Image Display Area")
                self.view.a_label.clear()
                self.update_analysis_progress(0, 100)
                self.view.tabWidget.setCurrentIndex(0)
                self.start_spinner("Please wait ...")

                self.logger.debug("starting analysis thread")
                self.analysis_thread.start()
                self.logger.debug("requesting analysis info")
                self.forAnalysis.emit(s_path)
            else:
                self.mayday("Please select a session to begin.")
                self.a_session = None
        else:
            self.mayday("Please select a session to begin.")
            self.a_session = None

    def cancel_input(self):
        """
        Name:     Prida.cancel_input
        Inputs:   None
        Outputs:  None
        Features: Exits INPUT_EXP, resetting editing boolean, enabling all
                  QLineEdit fields, and returns to VIEWER
        Depends:  - enable_all_attrs
                  - reset_cropbox
        """
        self.logger.info("Button Clicked")
        self.editing = False
        if not self.crop_on:
            self.reset_cropbox()
        self.enable_all_attrs()
        if self.view.stacked_sheets.currentIndex() == IDA_VIEW:
            self.logger.info("view changed to SHEET_VIEW")
            self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
        self.logger.info("view changed to VIEWER")
        self.view.stackedWidget.setCurrentIndex(VIEWER)

    def check_complete(self):
        """
        Name:     Prida.check_complete
        Inputs:   None.
        Outputs:  None.
        Features: Catched data's file checked signal
        """
        self.stop_spinner()
        pass

    def check_export_opt(self, exp_opt):
        """
        Name:     Prida.check_export_opt
        Inputs:   str, export naming option (exp_opt)
        Outputs:  None.
        Features: Checks the export naming option and inhibits scaled exports
                  for RootReader
        """
        if exp_opt == "RR3D":
            self.export_ui.scaleCombo.setCurrentIndex(0)
            self.export_ui.scaleCombo.setEnabled(False)
        else:
            self.export_ui.scaleCombo.setEnabled(True)

    def clear_photo_sheet_model(self):
        """
        Name:     Prida.clear_photo_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Clears photo sheet model
        Depends:  resize_photo_sheet
        """
        self.logger.debug("clearing photo sheet")
        for i in sorted(list(self.data.photo_attrs.keys())):
            self.photo_sheet_model.setItem(i, 1, QStandardItem(''))

        self.resize_photo_sheet()

    def clear_session_sheet_model(self):
        """
        Name:     Prida.clear_session_sheet_model
        Inputs:   None.
        Outputs:  None.
        Features: Clears session sheet model
        Depends:  resize_session_sheet
        """
        self.logger.debug("clearing session sheet")
        # for i in sorted(list(self.data.session_attrs.keys())):
        for i in range(self.session_sheet_model.rowCount()):
            self.session_sheet_model.setItem(i, 1, QStandardItem(''))

        self.resize_session_sheet()

    def config_clevel(self):
        """
        Name:     Prida.config_clevel
        Inputs:   None.
        Outputs:  None.
        Features: Requests new compression level and signals the change to the
                  data thread
        """
        cur_val = self.data.compression_lv
        if cur_val is None:
            cur_val = 6

        clv, ok = QInputDialog.getInt(
            self.base,
            "HDF5 Compression Level",
            "Set level, 1 (low) to 9 (high):",
            value=cur_val, min=1, max=9, step=1)
        if ok:
            self.setDataLV.emit(clv)

    def config_degrees(self):
        """
        Name:     Prida.config_degrees
        Inputs:   None.
        Outputs:  None.
        Features: Requests a motor degree resolution and assigns it to the
                  hardware
        Depends:  - mayday
                  - notice
        """
        cur_val = self.hardware.motor_degrees
        if cur_val is None:
            cur_val = 0

        val, ok = QInputDialog.getDouble(
            self.base,
            "Motor Resolution",
            "Set step resolution (degrees):",
            value=cur_val, min=0, max=9999, decimals=2)
        if ok:
            try:
                self.hardware.motor_degrees = val
            except:
                self.logger.error("failed to set motor degrees")
                self.mayday("Error! Motor degrees could not be set.")
            else:
                self.logger.info("motor degrees set to %0.2f", val)
                self.notice("Motor degrees set to %0.2f." % (val))

    def config_driver(self):
        """
        @TODO: is this function still needed?
        """
        cur_val = self.hardware.image_motor_driver
        if cur_val is None:
            cur_val = ''

        driver, ok = QInputDialog.getText(
            self.base,
            "Motor Controller Driver",
            "Set driver (i.e., Shield, HAT, Controller):",
            text=cur_val)
        if ok:
            try:
                self.hardware.image_motor_driver = driver
            except:
                self.logger.error("failed to set motor driver; resetting...")
                self.hardware.image_motor_driver = cur_val
                self.mayday("Error! Motor driver could not be set.")
            else:
                self.logger.info("motor driver changed to %s", driver)
                self.notice("Motor driver set to %s." % (driver))

    def config_gear_ratio(self):
        """
        Name:     Prida.config_gear_ratio
        Inputs:   None.
        Outputs:  None.
        Features: Requests a new gear ratio and assigns it to the hardware
        Depends:  - mayday
                  - notice
        """
        cur_val = self.hardware.motor_gear_ratio
        if cur_val is None:
            cur_val = 1.0

        gr, ok = QInputDialog.getDouble(
            self.base, "Imaging Gear Ratio", "Set ratio:",
            value=cur_val, min=0, max=9999, decimals=4)
        if ok:
            try:
                self.hardware.motor_gear_ratio = gr
            except:
                self.logger.error("failed to set gear ratio")
                self.mayday("Error! Imaging gear ratio could not be set.")
            else:
                self.logger.info("gear ratio set to %0.2f", gr)
                self.notice("Gear ratio set to %0.2f." % (gr))

    def config_image_mode(self):
        """
        @TODO: open config_mode.ui QDialog popup
        """
        self.mayday("This function is not currently available.")

    def config_imdir(self):
        """
        Name:     Prida.config_imdir
        Inputs:   None.
        Outputs:  None.
        Features: Requests new camera path and assigns it to the hardware
        """
        cur_val = self.hardware.path
        if cur_val == "":
            cur_val = self.last_dir

        new_path = QFileDialog.getExistingDirectory(
            self.base,
            "Select directory for storing camera images:",
            cur_val,
            QFileDialog.ShowDirsOnly)

        if os.path.isdir(new_path):
            self.hardware.path = new_path

    def config_mspeed(self):
        """
        Name:     Prida.config_mspeed
        Inputs:   None.
        Outputs:  None.
        Features: Requests new motor speed and assigns it to the hardware
        Depends:  - mayday
                  - notice
        """
        cur_val = self.hardware.motor_speed
        if cur_val is None:
            cur_val = 0

        mspeed, ok = QInputDialog.getInt(
            self.base,
            "Motor Speed",
            "Set speed (1-100):",
            value=cur_val, min=0, max=100, step=1)
        if ok:
            try:
                self.hardware.motor_speed = mspeed
            except:
                self.logger.error("failed to set motor speed")
                self.mayday("Error! Motor speed could not be set.")
            else:
                self.logger.info("motor speed set to %d", mspeed)
                self.notice("Motor speed set to %d." % (mspeed))

    def config_mstime(self):
        """
        Name:     Prida.config_mstime
        Inputs:   None.
        Outputs:  None.
        Features: Requests a settling time, seconds, to wait between the end
                  of the motor action and the capture of the image.
        Depends:  - mayday
                  - notice
        """
        cur_val = self.hardware.image_settling_time
        if cur_val is None:
            cur_val = 0.0

        mstime, ok = QInputDialog.getDouble(
            self.base,
            "Image Settling Time",
            "Set time, seconds:",
            value=cur_val, min=0, max=86400, decimals=1)
        if ok:
            try:
                self.hardware.image_settling_time = mstime
            except:
                self.logger.error("failed to set settling time")
                self.mayday("Error! Image settling time could not be set.")
            else:
                self.logger.info("settling time set to %0.1f", mstime)
                self.notice("Settling time set to %0.1f seconds." % (mstime))

    def config_mstype(self):
        """
        Name:     Prida.config_mstype
        Inputs:   None.
        Outputs:  None.
        Features: Requests new motor step type and assigns it to the hardware
        Depends:  - mayday
                  - notice
        """
        cur_val = self.hardware.motor_step_type
        if cur_val is None:
            cur_val = ''

        mstype, ok = QInputDialog.getText(
            self.base,
            "Motor Step Type",
            "Set step type (i.e., micro, single, inter, double):",
            text=cur_val)
        if ok:
            try:
                self.hardware.motor_step_type = mstype
            except:
                self.logger.error("failed to set motor step type")
                self.mayday("Error! Motor step type could not be set.")
            else:
                self.logger.info("motor step type changed to %s", mstype)
                self.notice("Motor step type set to %s." % (mstype))

    def configure_analysis(self):
        """
        Name:     Prida.configure_analysis
        Inputs:   None.
        Outputs:  None.
        Features: Configures the analysis thread signals and sockets and
                  defines the calibration parameters
        Depends:  - update_analysis_combos
        """
        # Create a new thread for image analysis:
        self.analysis_thread = QThread()
        self.image.moveToThread(self.analysis_thread)

        # Define cameras and their sensor info:
        self.supported_cameras = {
            0: "<None Selected>",
            1: "Nikon D200",
            2: "Nikon D300S",
            3: "Nikon D600",
            4: "Nikon D7100",
	    5: "Nikon D7200",
            9999: "Other"}
        # sensor (width, height), mm
        self.camera_info = {
            "Nikon D200": (23.6, 15.8),    # DX-format CCD sensor
            "Nikon D300S": (23.6, 15.8),   # DX-format CMOS sensor
            "Nikon D600": (35.9, 24.0),    # FX-format CMOS sensor
            "Nikon D7100": (23.5, 15.6),   # DX-format CMOS sensor
	    "Nikon D7200": (23.5, 15.6)}   # DX-format CMOS sensor

        # Define calibration parameters:
        self.za = None
        self.zg = None
        self.zw = None
        self.zf = None
        self.xs = None
        self.Xo = None
        self.alpha = None
        self.xi = None
        self.xa = None
        self.xg = None
        self.xw = None

        # Update ANALYSIS QComboBoxes:
        self.update_analysis_combos()

        # Load calibration input pop up:
        self.calib_ui = Ui_CalibInput()
        self.calib_dialog = QDialog(self.base)
        self.calib_ui.setupUi(self.calib_dialog)

        # Create a regex validator for checking valid distances:
        self.dist_regex = QRegExp("^\d{1,3}(\.\d*)?$")
        self.dist_validator = QRegExpValidator(self.dist_regex, self.base)

        # Assign validator to calibration distance input fields:
        self.calib_ui.c_tank_dist.setValidator(self.dist_validator)
        self.calib_ui.c_tank_depth.setValidator(self.dist_validator)
        self.calib_ui.c_tank_wall.setValidator(self.dist_validator)
        self.view.a_lens_size.setValidator(self.dist_validator)

        # Define popup signals and sockets:
        self.calib_ui.next1.clicked.connect(self.calib_next)
        self.calib_ui.next2.clicked.connect(self.calib_next)
        self.calib_ui.back2.clicked.connect(self.calib_back)
        self.calib_ui.back3.clicked.connect(self.calib_back)
        self.calib_ui.exit1.clicked.connect(self.calib_dialog.reject)
        self.calib_ui.exit2.clicked.connect(self.calib_dialog.reject)
        self.calib_ui.buttonBox.rejected.connect(self.calib_dialog.reject)
        self.calib_ui.buttonBox.accepted.connect(self.calib_dialog.accept)
        self.calib_ui.c_to_center.clicked.connect(self.calib_set_preview)
        self.calib_ui.c_to_wall.clicked.connect(self.calib_set_preview)
        # self.calib_dialog.rejected.connect(self.analysis_thread.quit)

        # Connect signals and sockets with main thread:
        self.view.a_lens_size.textEdited.connect(self.update_cam_zf)
        self.view.a_cam_combo.currentIndexChanged.connect(self.update_cam_xs)
        self.view.a_orient_combo.currentIndexChanged.connect(
            self.update_cam_xs)

        # Connect signals and sockets with image thread:
        self.toCalibrate.connect(self.image.calibrate)
        self.image.isCalibrated.connect(self.update_calib_params)
        self.view.calibrate.clicked.connect(self.calibrate)
        self.view.a_rotleft.clicked.connect(self.image.rotate_left)
        self.view.a_rotright.clicked.connect(self.image.rotate_right)
        self.analysis_thread.started.connect(self.image.start)
        self.analysis_thread.finished.connect(self.back_to_sheet)
        self.image.sendAlert.connect(self.receive_alert)
        self.image.isFound.connect(self.update_analysis_found)
        self.image.isMerged.connect(self.update_analysis_merge)
        self.image.isReady.connect(self.show_analysis)
        self.image.setBGColor.connect(self.update_analysis_color)
        self.image.setImgRange.connect(self.update_analysis_range)
        self.image.setImgShape.connect(self.update_analysis_shape)
        self.image.updateProc.connect(self.update_analysis_process)
        self.image.updateProg.connect(self.update_analysis_progress)
        self.getObject.connect(self.image.get_element_array)
        self.removeObject.connect(self.image.remove_element)
        self.toCrop.connect(self.image.crop)
        self.toFilter.connect(self.image.dust_filter)
        self.toSmooth.connect(self.image.median_filter)
        self.toFind.connect(self.image.find_objects)
        self.toFind2.connect(self.image.find_objects2)
        self.toPlot.connect(self.image.plot_centers)
        self.toRefresh.connect(self.image.refresh)
        self.toSave.connect(self.image.export_data)
        self.toStretch.connect(self.image.stretch)
        self.toThresh.connect(self.image.make_binary)
        self.toUndo.connect(self.image.show_proc_img)

        # Connect signals and sockets with data & image threads:
        self.forAnalysis.connect(self.data.get_analysis_vals)
        self.data.toAnalysis.connect(self.update_analysis_view)
        self.data.datasetsListed.connect(self.image.set_datasets)
        self.showFirst.connect(self.image.show_first)
        self.toMerge.connect(self.image.start_merge)
        self.image.getProcImg.connect(self.data.get_process_img)
        self.data.setImg.connect(self.image.set_image)

        # Set ANALYSIS actions:
        self.view.a_exit.clicked.connect(self.analysis_thread.quit)
        self.view.a_run.clicked.connect(self.run_calib)
        self.view.a_save.clicked.connect(self.save_analysis)
        self.view.a_crop.clicked.connect(self.run_crop)
        self.view.a_delete.clicked.connect(self.run_delete)
        self.view.a_filter.clicked.connect(self.run_filter)
        self.view.a_smoother.clicked.connect(self.run_smoother)
        self.view.a_find.clicked.connect(self.run_find)
        self.view.a_find2.clicked.connect(self.run_find2)
        self.view.a_dsone.clicked.connect(self.run_dsone)
        self.view.a_merge.clicked.connect(self.run_merge)
        self.view.a_refresh.clicked.connect(self.run_refresh)
        self.view.a_show.clicked.connect(self.run_show)
        self.view.a_stretch.clicked.connect(self.run_stretch)
        self.view.a_thresh.clicked.connect(self.run_thresh)
        self.view.a_undo.clicked.connect(self.run_undo)
        self.view.a_dist_calc.clicked.connect(self.get_calib_inputs)

    def create_menus(self):
        """
        Name:     Prida.create_menus
        Inputs:   None.
        Outputs:  None.
        Features: Creates menu items and actions

        @TODO: Finish creating menu items for configuring camera, motor, etc.
               Create setter actions for each configurable parameter

        @TODO: Import config_mode.ui for addressing the final five config
               values.

        YES    PRIDA.HDF_ORGANIZER         COMPRESS_LV
        YES    PRIDA.HARDWARE.CAMERA       IMAGE_DIR
        YES    PRIDA.HARDWARE.CONTROLLER   DEGREES_PER_STEP
        YES    PRIDA.HARDWARE.CONTROLLER   SPEED
        YES    PRIDA.HARDWARE.CONTROLLER   STEP_TYPE
        YES    PRIDA.HARDWARE.IMAGING      SETTLING_TIME
        YES    PRIDA.HARDWARE.IMAGING      GEAR_RATIO
        N/A    PRIDA.GLOBAL_CONF           PRIDA_VERSION
        N/A    PRIDA.GLOBAL_CONF           LOG_LEVEL
        N/A    PRIDA.GLOBAL_CONF           LOG_DIR
        N/A    PRIDA.GLOBAL_CONF           LOG_FILENAME
        N/A    PRIDA.GLOBAL_CONF           DICTIONARY_DIR
        N/A    PRIDA.GLOBAL_CONF           DICTIONARY_FILENAME
        NO     PRIDA.HARDWARE.IMAGING      MODE
        NO     PRIDA.HARDWARE.IMAGING      DRIVER
        NO     PRIDA.HARDWARE.IMAGING      HAS_LED
        NO     PRIDA.HARDWARE.IMAGING      HAS_PIEZO
        NO     PRIDA.HARDWARE.CONTROLLER   PORT_NUMBER
        """
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Create new actions:
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # File -> New
        self.fnewAct = QAction("&New", self.base)
        self.fnewAct.setShortcuts(QKeySequence.New)
        self.fnewAct.setStatusTip("Create a new project file")
        self.fnewAct.triggered.connect(self.new_hdf)

        # File -> Open
        self.fopenAct = QAction("&Open", self.base)
        self.fopenAct.setShortcuts(QKeySequence.Open)
        self.fopenAct.setStatusTip("Open existing project file")
        self.fopenAct.triggered.connect(self.open_hdf)

        # File -> Save
        self.fsaveAct = QAction("&Save", self.base)
        self.fsaveAct.setShortcuts(QKeySequence.Save)
        self.fsaveAct.setStatusTip("Save a compressed copy of your file")
        self.fsaveAct.triggered.connect(self.save_compressed_copy)

        # File -> Close
        self.fcloseAct = QAction("&Close", self.base)
        self.fcloseAct.setShortcuts(QKeySequence.Close)
        self.fcloseAct.setStatusTip("Return to the main menu")
        self.fcloseAct.triggered.connect(self.back_to_menu)

        # File -> Quit
        self.fquitAct = QAction("&Quit", self.base)
        self.fquitAct.setShortcuts(QKeySequence.Quit)
        self.fquitAct.setStatusTip("Exit program")
        self.fquitAct.triggered.connect(self.shutdown)

        # Help -> about
        self.haboutAct = QAction("&About", self.base)
        self.haboutAct.setStatusTip("About Prida")
        self.haboutAct.triggered.connect(self.about)

        # Help -> Check file
        self.hcheckAct = QAction("&Check File", self.base)
        self.hcheckAct.setStatusTip("Check HDF5 file for problems")
        self.hcheckAct.triggered.connect(self.file_checker)

        # Help -> reconfigure
        self.hconfigAct = QAction("&Reconfig", self.base)
        self.hconfigAct.setStatusTip(
            "Replace configuration file in Prida directory")
        self.hconfigAct.triggered.connect(self.reconfigure)

        # Config -> Data -> Compression Level
        self.clevelAct = QAction("&Compression Level", self.base)
        self.clevelAct.setStatusTip("Set HDF5 compression level")
        self.clevelAct.triggered.connect(self.config_clevel)

        # Config -> Camera -> Image Directory
        self.imdirAct = QAction("&Image Directory", self.base)
        self.imdirAct.setStatusTip("Assign directory for saving camera images")
        self.imdirAct.triggered.connect(self.config_imdir)

        # Config -> Motor -> Degrees
        self.mdegAct = QAction("&Degrees", self.base)
        self.mdegAct.setStatusTip("Set motor degree resolution")
        self.mdegAct.triggered.connect(self.config_degrees)

        # Config -> Motor -> Speed
        self.mspeedAct = QAction("&Motor Speed", self.base)
        self.mspeedAct.setStatusTip("Set motor speed")
        self.mspeedAct.triggered.connect(self.config_mspeed)

        # Config -> Motor -> Step Type
        self.mstypeAct = QAction("&Step Type", self.base)
        self.mstypeAct.setStatusTip(
            "Set motor step type (e.g., micro, single, inter, double)")
        self.mstypeAct.triggered.connect(self.config_mstype)

        # Config -> Imaging -> Gear Ratio
        self.mgearrAct = QAction("&Gear Ratio", self.base)
        self.mgearrAct.setStatusTip("Set motor gear ratio")
        self.mgearrAct.triggered.connect(self.config_gear_ratio)

        # Config -> Imaging -> Mode
        self.imodeAct = QAction("&Mode", self.base)
        self.imodeAct.setStatusTip("Change Prida imaging mode")
        self.imodeAct.triggered.connect(self.config_image_mode)

        # Config -> Imaging -> Settling Time
        self.mstimeAct = QAction("&Settling Time", self.base)
        self.mstimeAct.setStatusTip("Set pause before camera capture")
        self.mstimeAct.triggered.connect(self.config_mstime)

        # Motor -> Take Steps
        self.mtestAct = QAction("&Take Steps", self.base)
        self.mtestAct.setStatusTip(
            "Run the motor for a prescribed number of steps.")
        self.mtestAct.triggered.connect(self.test_motor)

        # Motor -> Test Gear Ratio
        self.mqrtAct = QAction("&Gear Ratio Test", self.base)
        self.mqrtAct.setStatusTip(
            "Attempt to rotate the bearing based on prescribed gear ratio.")
        self.mqrtAct.triggered.connect(self.test_gear_ratio)

        # Photo -> Take Preview
        self.previewAct = QAction("&Take Preview", self.base)
        self.previewAct.setStatusTip("Take a preview image with camera")
        self.previewAct.triggered.connect(self.take_preview_photo)

        # Photo -> Open Preview
        self.popenAct = QAction("&Open Preview", self.base)
        self.popenAct.setStatusTip("Open an image from file for previewing")
        self.popenAct.triggered.connect(self.open_preview_photo)

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Create menu items for the actions:
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.fileMenu = self.base.menuBar().addMenu("&File")
        self.configMenu = self.base.menuBar().addMenu("&Config")
        self.photoMenu = self.base.menuBar().addMenu("&Photo")
        self.motorMenu = self.base.menuBar().addMenu("&Motor")
        self.helpMenu = self.base.menuBar().addMenu("&Help")

        # File menu:
        self.fileMenu.addAction(self.fnewAct)
        self.fileMenu.addAction(self.fopenAct)
        self.fileMenu.addAction(self.fsaveAct)
        self.fileMenu.addAction(self.fcloseAct)
        self.fileMenu.addAction(self.fquitAct)

        # Help  menu:
        self.helpMenu.addAction(self.haboutAct)
        self.helpMenu.addAction(self.hcheckAct)
        self.helpMenu.addAction(self.hconfigAct)

        # Data submenu:
        self.dataConfigMenu = self.configMenu.addMenu("&Data")
        self.dataConfigMenu.addAction(self.clevelAct)

        # Camera submenu:
        self.cameraConfigMenu = self.configMenu.addMenu("&Camera")
        self.cameraConfigMenu.addAction(self.imdirAct)

        # Imaging submenu:
        self.imageConfigMenu = self.configMenu.addMenu("&Imaging")
        self.imageConfigMenu.addAction(self.imodeAct)
        self.imageConfigMenu.addAction(self.mstimeAct)
        self.imageConfigMenu.addAction(self.mgearrAct)

        # Motor submenu:
        self.motorConfigMenu = self.configMenu.addMenu("&Motor")
        self.motorConfigMenu.addAction(self.mdegAct)
        self.motorConfigMenu.addAction(self.mspeedAct)
        self.motorConfigMenu.addAction(self.mstypeAct)

        # Preview photo submenu:
        self.photoMenu.addAction(self.previewAct)
        self.photoMenu.addAction(self.popenAct)

        # Test motor submenu:
        self.motorMenu.addAction(self.mtestAct)
        self.motorMenu.addAction(self.mqrtAct)

        # Disable unavailable menus/actions based on hardware mode:
        if not self.hardware_mode:
            self.cameraConfigMenu.setEnabled(False)
            self.motorConfigMenu.setEnabled(False)
            self.previewAct.setEnabled(False)
            self.motorMenu.setEnabled(False)
        elif self.hardware.mode != '3D':
            self.motorMenu.setEnabled(False)
            self.motorConfigMenu.setEnabled(False)

    def create_session(self):
        """
        Name:     Prida.create_session
        Inputs:   None
        Outputs:  None
        Features: Zeros cropbox and changes view to INPUT_EXP with a focus on
                  the PID field
        Depends:  reset_cropbox
        """
        self.logger.info("view changed to INPUT_EXP")
        self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
        self.reset_cropbox()
        self.view.c_pid.setFocus()

    def disable_pid_attrs(self):
        """
        Name:     Prida.disable_pid_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with PID attributes
        Depends:  enable_all_attrs
        """
        self.enable_all_attrs()
        if self.editing:
            self.logger.debug("disabling INPUT_EXP fields")
            self.view.c_pid.setEnabled(False)
            self.view.c_num_images.setEnabled(False)
            for k in self.data.pid_attrs:
                exec("self.view.%s.setEnabled(False)" % (
                    self.data.pid_attrs[k]["qt_val"]))

    def disable_session_attrs(self):
        """
        Name:     Prida.disable_session_attrs
        Inputs:   None
        Outputs:  None
        Features: Disables INPUT_EXP fields associated with session attributes
        Depends:  enable_all_attrs
        """
        self.enable_all_attrs()
        if self.editing:
            self.logger.debug("disabling INPUT_EXP fields")
            self.view.c_pid.setEnabled(False)
            for k in self.data.session_attrs:
                if self.data.session_attrs[k]['qt_val'] is not None:
                    exec("self.view.%s.setEnabled(False)" % (
                        self.data.session_attrs[k]["qt_val"]))

    def edit_field(self):
        """
        Name:     Prida.edit_field
        Inputs:   None
        Outputs:  None
        Features: Performs INPUT_EXP field disabling based on the selection in
                  the sheet model, signals data to set the appropriate input
                  fields, sets editing boolean to True and changes view to
                  INPUT_EXP
        Depends:  - disable_pid_attrs
                  - disable_session_attrs
                  - set_defaults
                  - set_input_field
        """
        self.logger.info("Button Clicked")
        if self.sheet_select_model.hasSelection():
            self.logger.debug("set editing to True")
            self.editing = True
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session, disable PID attrs and set focus on user
                self.disable_pid_attrs()
                self.view.c_session_user.setFocus()

                # Get session attrs & set them to INPUT_EXP:
                pid = my_selection.parent().text()
                session = my_selection.text()
                p_path = "/%s" % (pid)
                s_path = "/%s/%s" % (pid, session)

                self.logger.debug("setting INPUT_EXP fields for PID")
                self.set_input_field("c_pid", "QLineEdit", str(pid))
                self.setPIDFields.emit(p_path)

                self.logger.debug("setting INPUT_EXP fields for session")
                self.setSessionFields.emit(s_path)
            else:
                # Selected PID, disable session attrs and set focus to ID
                self.disable_session_attrs()
                self.view.c_usda_plants.setFocus()

                # Get PID attrs & set them to INPUT_EXP:
                pid = my_selection.text()
                p_path = "/%s" % (pid)

                self.set_defaults()
                self.logger.debug("setting INPUT_EXP fields for PID")
                self.set_input_field("c_pid", "QLineEdit", str(pid))
                self.setPIDFields.emit(p_path)
            self.logger.info("view changed to INPUT_EXP")
            self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
        else:
            self.mayday("Nothing selected!")

    def enable_all_attrs(self):
        """
        Name:     Prida.enable_all_attrs
        Inputs:   None
        Outputs:  None
        Features: Enables all INPUT_EXP fields, except image count unless in
                  3D mode
        """
        self.logger.debug("enabling all INPUT_EXP fields")
        self.view.c_pid.setEnabled(True)
        for k in self.data.pid_attrs:
            exec("self.view.%s.setEnabled(True)" % (
                self.data.pid_attrs[k]["qt_val"]))

        for j in self.data.session_attrs:
            if self.data.session_attrs[j]['qt_val'] is not None:
                exec("self.view.%s.setEnabled(True)" % (
                    self.data.session_attrs[j]['qt_val']))

        # NOTE: disable user editing of image count field
        #       unless capturing photos in 3D mode
        self.view.c_num_images.setEnabled(False)
        if self.hardware.mode == '3D' and not self.editing:
            self.logger.debug("Imaging in 3D mode; allow image count edits")
            self.view.c_num_images.setEnabled(True)

    def end_progress(self):
        """
        Name:     Prida.end_progress
        Inputs:   None.
        Outputs:  None.
        Features: Resets progress bar, signals for new data, and displays
                  'saving to file' message
        """
        self.logger.debug("imaging done")
        self.view.c_progress.reset()
        self.preview = False
        self.logger.info("sending get data list signal")
        self.getDataList.emit()
        self.base.statusBar().showMessage("Imaging complete!", 2000)
        self.view.imagingLabel.setText(
            "<html><head/><body><p>"
            "<span style='font-size:28pt; font-weight:600;'>"
            "Saving to File ..."
            "</span></p></body></html>")

    def explore_file(self):
        """
        Name:     Prida.explore_file
        Inputs:   None.
        Outputs:  None.
        Features: Checks for user selection and signals to data for opening
        Depends:  - start_spinner
                  - mayday
        """
        self.logger.info("Button Clicked")
        if self.file_select_model.hasSelection():
            self.logger.debug("gathering selection details")
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0]
            )
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            self.logger.debug("opening file %s", s_name)
            self.start_spinner("Opening file ...")
            self.openFile.emit(f_path)
        else:
            self.mayday("Nothing selected!")

    def export(self):
        """
        Name:     Prida.export
        Inputs:   None.
        Outputs:  None.
        Features: Prompts user for exporting options (e.g., output directory,
                  image format, image color) and signals the data thread to
                  begin exporting the selected PID(s) or session(s)
        Depends:  get_sheet_model_selection
        """
        self.logger.info("Button Clicked")
        self.logger.debug("gathering user selection")
        if self.sheet_select_model.hasSelection():
            s_path = self.get_sheet_model_selection()
            self.logger.debug("user selected %s", s_path)
        else:
            s_path = "/"
            self.logger.debug("no selection, exporting from root")

        # Update UI path labels:
        self.export_ui.u_selection.setText(s_path)
        self.export_ui.u_directory.setText(self.data.directory)

        # Session cropping support:
        self.export_ui.toCrop.setEnabled(False)
        if self.crop_on:
            self.export_ui.toCrop.setEnabled(True)

        if self.export_dialog.exec_():
            path = self.export_ui.u_directory.text()
            pformat = self.export_ui.formatCombo.currentText().lower()
            pcolor = self.export_ui.colorCombo.currentText().lower()
            pscale = self.export_ui.scaleCombo.currentData()
            pname = self.export_ui.namingCombo.currentText().lower()
            tokeep = self.export_ui.toKeep.isChecked()
            tocrop = self.export_ui.toCrop.isChecked()
            self.logger.info("Exporting %s to %s as %s in %s" % (
                s_path, path, pformat, pcolor))

            # Prepare exporting options and emit signal to data thread:
            exp_opts = {}
            exp_opts['color'] = pcolor
            exp_opts['format'] = pformat
            exp_opts['name'] = pname
            exp_opts['scale'] = pscale
            exp_opts['keep'] = tokeep
            exp_opts['crop'] = tocrop
            exp_opts['crop_top'] = self.crop_extents['top']
            exp_opts['crop_left'] = self.crop_extents['left']
            exp_opts['crop_bottom'] = self.crop_extents['bottom']
            exp_opts['crop_right'] = self.crop_extents['right']
            self.toExport.emit(s_path, path, exp_opts)

    def file_checker(self):
        """
        Features: check image counts and attributes
        """
        self.start_spinner("Checking file ...")
        self.checkFile.emit()

    def get_calib_inputs(self):
        """
        Name:     Prida.get_calib_inputs
        Inputs:   None.
        Outputs:  None.
        Features: Opens calib dialog, requests calibration inputs
        Depends:  - set_qlabel_text
                  - update_calib_vals
        Note:     * zd is the full depth of the glass tank (i.e., dimension
                    from outer wall to outer wall); for the Prida 3D hardware,
                    zd is 19 inches (482.6 mm)
                  * zg is the thickness of the tank wall; for Prida 3D harware,
                    zg is 0.5 inches (12.7 mm)
        """
        self.logger.info("Button clicked")
        self.logger.debug("setting up calibration info popup")
        self.calib_ui.pages.setCurrentIndex(0)

        # Set the preview figures:
        self.calib_set_preview()
        if self.calib_tank_depth is not None:
            self.pix_td = QPixmap(self.calib_tank_depth).scaledToWidth(300)
            self.calib_ui.tank_depth.setPixmap(self.pix_td)
        if self.calib_tank_wall is not None:
            self.pix_tw = QPixmap(self.calib_tank_wall).scaledToWidth(300)
            self.calib_ui.tank_wall.setPixmap(self.pix_tw)

        # Open the popup:
        if self.calib_dialog.exec_():
            t_dist = self.calib_ui.c_tank_dist.text()          # meters
            t_depth = self.calib_ui.c_tank_depth.text()        # meters
            t_wall = self.calib_ui.c_tank_wall.text()          # centimeters
            to_center = self.calib_ui.c_to_center.isChecked()
            self.logger.info("Tank distance: %s m", t_dist)
            self.logger.info("Tank depth: %s m", t_depth)
            self.logger.info("Tank wall: %s cm", t_wall)
            try:
                zd = float(t_depth)
                self.zg = float(t_wall)/100.0
                self.zw = 0.5*zd - self.zg
                if to_center:
                    zt = float(t_dist)
                    self.za = zt - 0.5*zd
                else:
                    self.za = float(t_dist)
            except:
                self.logger.exception("something went wrong!")
                self.za = None
                self.zg = None
                self.zw = None
                self.set_qlabel_text("a_za", "N/A")
                self.set_qlabel_text("a_zg", "N/A")
                self.set_qlabel_text("a_zw", "N/A")
                self.set_qlabel_text("a_zt", "N/A")
            else:
                self.set_qlabel_text("a_za", "{:0.2f}".format(self.za*100))
                self.set_qlabel_text("a_zg", "{:0.2f}".format(self.zg*100))
                self.set_qlabel_text("a_zw", "{:0.2f}".format(self.zw*100))
                self.set_qlabel_text("a_zt", "{:0.3f}{}{:0.3f}".format(
                    self.za + self.zg + self.zw, "\u00B1", zt_err))
                self.update_calib_vals()

    def get_export_dir(self):
        """
        Name:     Prida.get_export_dir
        Inputs:   None.
        Outputs:  None.
        Features: Socket for export UI directory button; prompts user for
                  output directory and assigns it to the UI label;
                  updates last directory
        """
        path = QFileDialog.getExistingDirectory(self.base,
                                                "Select an output directory:",
                                                self.last_dir,
                                                QFileDialog.ShowDirsOnly)
        self.export_ui.u_directory.setText(path)
        if os.path.isdir(path):
            self.last_dir = path

    def get_input_field(self, field_name, field_type):
        """
        Name:     Prida.get_input_field
        Inputs:   - str, Qt field name (field_name)
                  - str, Qt field type (field_type)
        Outputs:  str, current input text (field_value)
        Features: Get the current text from a Qt input box
        Raises:   NameError
        """
        err_msg = "could not get %s %s value" % (field_type, field_name)

        if field_type is None:
            # Bypass fields that are set to None.
            field_value = ""
        elif field_type == "QComboBox":
            try:
                field_value = eval("self.view.%s.currentText()" % (field_name))
            except:
                self.logger.exception(err_msg)
                raise NameError(err_msg)
            else:
                self.logger.debug("read %s from %s %s" % (
                    field_value, field_type, field_name))
        elif (field_type == "QDateEdit" or
              field_type == "QLineEdit" or
              field_type == "QSpinBox"):
            # All three fields use text() getter
            try:
                field_value = eval("self.view.%s.text()" % (field_name))
            except:
                self.logger.exception(err_msg)
                raise NameError(err_msg)
            else:
                self.logger.info("read %s from %s %s" % (
                    field_value, field_type, field_name))
        else:
            self.logger.error(err_msg)
            raise NameError(err_msg)

        # Handle some forbidden characters:
        field_value = field_value.replace("\\", "\\\\")
        field_value = field_value.replace("\"", "'")
        return str(field_value)

    def get_sheet_model_selection(self):
        """
        Name:     Prida.get_sheet_model_selection
        Inputs:   None
        Outputs:  str, HDF5 group path based on selection (s_path)
        Features: Returns sheet model selection path for HDF5 file
        """
        if self.sheet_select_model.hasSelection():
            self.logger.debug("gathering user selection")
            my_selection = self.sheet_model.itemFromIndex(
                self.sheet_select_model.selectedIndexes()[0]
            )
            if my_selection.parent() is not None:
                # Selected session
                pid = my_selection.parent().text()
                session = my_selection.text()
                s_path = "/%s/%s" % (pid, session)
                self.logger.debug("returning session %s", s_path)
            else:
                # Selected PID
                pid = my_selection.text()
                s_path = "/%s" % (pid)
                self.logger.debug("returning PID %s", s_path)

            return s_path

    def get_tags(self):
        """
        Name:     Prida.get_tags
        Inputs:   None.
        Outputs:  None.
        Features: Retrieves the thumbnail selection data and signals the data
                  thread for tag info; handles preview images
        Depends:  mayday
        """
        if self.view.img_select.selectedIndexes():
            img_path = self.img_sheet_model.itemFromIndex(
                self.view.img_select.selectedIndexes()[0]).data()
            img_name = os.path.basename(img_path)
            self.tag_dialog.setWindowTitle("Tag Info: %s" % (img_name))

            self.logger.debug("signaling tag info for %s", img_path)
            if os.path.isfile(img_path):
                # For preview photos
                self.getPreviewTags.emit(img_path)
            else:
                self.getTags.emit(img_path)
        else:
            self.mayday("No image selected!")

    def hdf_opened(self, err_val):
        """
        Name:     Prida.hdf_opened
        Inputs:   int, error value (err_val)
                  * 0: new file
                  * 1: error
                  * -1: existing file
        Outputs:  None.
        Features: Catches signal from data after an HDF5 file is opened; reads
                  data, updates PID autofill model, sets window title, and
                  returns to VIEW
        Depends:  - back_to_menu
                  - get_data
                  - update_menus
                  - set_about
                  - set_defaults
                  - mayday
        """
        self.logger.info("caught file opened signal with code %d", err_val)

        # Update menu:
        self.update_menus()

        if err_val == 0:
            self.logger.debug("HDF5 file created")
            self.set_about()

            self.logger.info("sending get data list signal")
            self.getDataList.emit()

            self.logger.debug('updating window title')
            self.base.setWindowTitle(
                "%s: %s" % (self.prida_title, self.data.basename)
            )
            self.set_defaults()
            self.base.statusBar().showMessage("New file created!", 2000)
        elif err_val == -1:
            self.logger.debug("HDF5 file opened")
            self.logger.info("sending get data list signal")
            self.getDataList.emit()

            self.logger.debug('updating window title to %s', self.prida_title)
            self.base.setWindowTitle(
                "%s: %s" % (self.prida_title, self.data.basename))
            self.set_defaults()
            self.stop_spinner()
            self.base.statusBar().showMessage("File opened!", 2000)
        elif err_val == 1:
            self.logger.warning("open file failed")
            self.logger.debug("returning back to menu")
            self.back_to_menu()
            self.mayday("Could not open the selected file.")
        else:
            self.logger.warning("unknown error encountered")
            self.logger.debug("returning back to menu")
            self.back_to_menu()
            self.mayday("An unknown error was encountered during file open!")

    def import_data(self):
        """
        Name:     Prida.import_data
        Inputs:   None.
        Outputs:  None.
        Features: Reads a user-defined directory for images, saves images to
                  search_files dictionary, attempts to extract image exif tags
                  and defines them in INPUT_EXP view; for multi-file import,
                  emits signal for data consistency check
        Depends:  - find_image_files
                  - reset_cropbox
                  - set_defaults
                  - set_input_field
                  - set_session_fields
        """
        self.logger.info("Button Clicked")
        self.reset_cropbox()
        self.logger.debug("requesting directory from user")

        # Ask user if files are in a directory?
        reply = QMessageBox.question(
            self.base,
            "Image Import",
            "Are your images stored in a directory?")
        if reply == QMessageBox.No:
            self.logger.debug("QMessageBox: user selected 'No'")
            my_file = QFileDialog.getOpenFileName(
                self.base, "Select File", self.last_dir, (img_import_str))[0]
            self.logger.debug("user selected file %s", my_file)
            if os.path.isfile(my_file):
                # Update last directory:
                self.last_dir = os.path.dirname(my_file)

                self.logger.debug("importing 1 file")
                self.logger.debug("resetting search file dictionary")
                self.search_files = {}
                self.search_files[my_file] = 0
                self.set_session_fields(my_file, 1)

                # Set keyboard focus to PID and change view to INPUT_EXP:
                self.view.c_pid.setFocus()
                self.logger.info("view changed to INPUT_EXP")
                self.view.stackedWidget.setCurrentIndex(INPUT_EXP)
            else:
                self.mayday("Please select a file for importing.")
                self.logger.warning("could not find file %s", my_file)
        elif reply == QMessageBox.Yes:
            self.logger.debug("QMessageBox: user selected 'Yes'")
            path = QFileDialog.getExistingDirectory(self.base,
                                                    "Select image directory:",
                                                    self.last_dir,
                                                    QFileDialog.ShowDirsOnly)
            if os.path.isdir(path):
                # Update last directory:
                self.last_dir = path

                # Populate default values and overwrite where possible:
                self.set_defaults()

                # Currently filters search for only jpg and tif image files
                self.logger.debug("searching directory for images")
                my_files = find_image_files(path)
                files_found = len(my_files)
                self.logger.info("importing %d files", files_found)

                if files_found > 0:
                    # Check for consistent image shapes:
                    self.start_spinner("Validating import images ...")
                    self.imageCheck.emit(my_files)
                else:
                    self.mayday("No images found in directory.")
                    self.logger.warning("no images found in %s", path)

    def import_images(self, my_files, check_passed):
        """
        Name:     Prida.import_images
        Inputs:   - list, image file list (my_files)
                  - bool, consistency check (check_passed)
        Outputs:  None.
        Features: Catches consistency check signal from data and continues the
                  image import, gathering exif tags where and when possible
        Depends:  - set_session_fields
                  - stop_spinner
        """
        self.stop_spinner()
        answ = QMessageBox.No
        if not check_passed:
            q = ("One or more images from the directory you "
                 "selected are oriented or sized differently. "
                 "Do you want to continue?")
            answ = QMessageBox.question(self.base, "Consistency check!", q)
        if check_passed or answ == QMessageBox.Yes:
            # Reset file names for each processing:
            self.logger.debug("resetting search file dictionary")
            self.search_files = {}

            self.logger.debug("reading images found")
            for my_file in my_files:
                self.search_files[my_file] = 0

            num_imgs = len(my_files)
            self.set_session_fields(my_file, num_imgs)

            # Set keyboard focus to PID and change view to INPUT_EXP
            self.view.c_pid.setFocus()
            self.logger.info("view changed to INPUT_EXP")
            self.view.stackedWidget.setCurrentIndex(INPUT_EXP)

    def load_file_browse_sheet_headers(self):
        """
        Name:     Prida.load_file_browse_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the file_sheet_model
        Depends:  resize_file_browse_sheet
        """
        self.logger.debug('loading headers')
        self.file_sheet_model.setColumnCount(2)
        self.file_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File Name'))
        self.file_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Path'))
        self.resize_file_browse_sheet()

    def load_file_search_sheet_headers(self):
        """
        Name:     Prida.load_file_search_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the search_sheet_model
        Depends:  resize_search_sheet
        """
        self.logger.debug('loading headers')
        self.search_sheet_model.setColumnCount(self.sheet_items + 1)
        self.search_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('File'))
        self.search_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('PID'))
        for i in self.data.pid_attrs:
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.search_sheet_model.setHorizontalHeaderItem", (i + 1),
                self.data.pid_attrs[i]['title']))
        self.resize_search_sheet()

    def load_images(self):
        """
        Name:     Prida.load_images
        Inputs:   None.
        Outputs:  None.
        Features: Searches for image files used in the GUI; sets greeter,
                  spinner and movie
        Depends:  resource_path
        """
        self.logger.debug("checking for required resources")

        img_greeter = resource_path(os.path.join("images", "greeter.jpg"))
        if os.path.isfile(img_greeter):
            self.greeter = img_greeter
        else:
            self.greeter = None
            self.logger.warning("greeter JPG not found")

        img_load = resource_path(os.path.join("images", "loading.gif"))
        if os.path.isfile(img_load):
            self.spinner = img_load
        else:
            self.logger.warning("loading GIF not found")
            self.spinner = None
        self.movie = QMovie(self.spinner)

        img_dist_center = resource_path(
            os.path.join("images", "calib_dist_center.jpg"))
        if os.path.isfile(img_dist_center):
            self.calib_dist_center = img_dist_center
        else:
            self.calib_dist_center = None

        img_dist_wall = resource_path(
            os.path.join("images", "calib_dist_wall.jpg"))
        if os.path.isfile(img_dist_wall):
            self.calib_dist_wall = img_dist_wall
        else:
            self.calib_dist_wall = None

        img_tank_depth = resource_path(
            os.path.join("images", "calib_tank_depth.jpg"))
        if os.path.isfile(img_tank_depth):
            self.calib_tank_depth = img_tank_depth
        else:
            self.calib_tank_depth = None

        img_tank_wall = resource_path(
            os.path.join("images", "calib_tank_wall.jpg"))
        if os.path.isfile(img_tank_wall):
            self.calib_tank_wall = img_tank_wall
        else:
            self.calib_tank_wall = None

        icon_path = resource_path(os.path.join("images", "icon.png"))
        if os.path.isfile(icon_path):
            self.im_icon = QIcon(QPixmap(icon_path))
            self.setWindowIcon(self.im_icon)
        else:
            self.im_icon = None

    def load_photo_sheet_headers(self):
        """
        Name:     Prida.load_photo_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the photo sheet model
        Depends:  resize_photo_sheet
        """
        self.logger.debug("loading headers")
        self.photo_sheet_model.setColumnCount(2)
        self.photo_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem("Property"))
        self.photo_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem("Value"))

        for i in self.data.photo_attrs:
            exec("%s(%d, 0, QStandardItem('%s'))" % (
                "self.photo_sheet_model.setItem", i,
                self.data.photo_attrs[i]['key']))
        self.resize_photo_sheet()

    def load_session_sheet_headers(self):
        """
        Name:     Prida.load_session_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers and property names in session sheet model
        Depends:  resize_session_sheet
        """
        self.logger.debug('loading headers')
        self.session_sheet_model.setColumnCount(2)
        self.session_sheet_model.setHorizontalHeaderItem(
            0, QStandardItem('Property'))
        self.session_sheet_model.setHorizontalHeaderItem(
            1, QStandardItem('Value'))

        # Define session sheet rows in column 1:
        for i in self.data.session_attrs:
            exec("%s(%d, 0, QStandardItem('%s'))" % (
                "self.session_sheet_model.setItem", i,
                self.data.session_attrs[i]['title']))
        self.resize_session_sheet()

    def load_sheet_headers(self):
        """
        Name:     Prida.load_sheet_headers
        Inputs:   None.
        Outputs:  None.
        Features: Sets the headers in the sheet_model
        Depends:  resize_sheet
        """
        self.logger.debug('loading %d headers', self.sheet_items)
        self.sheet_model.setColumnCount(self.sheet_items)
        self.sheet_model.setHorizontalHeaderItem(0, QStandardItem('PID'))
        for i in self.data.pid_attrs:
            exec('%s(%d, QStandardItem("%s"))' % (
                "self.sheet_model.setHorizontalHeaderItem", i,
                self.data.pid_attrs[i]['title']))
        self.resize_sheet()

    def load_ui(self):
        """
        Name:     Prida.load_ui
        Inputs:   None.
        Outputs:  None.
        Features: Assigns UI classes to the MainWindow and other QDialogs
        """
        # Load main window GUI to the QMainWindow:
        self.view = Ui_MainWindow()
        self.view.setupUi(self.base)

        # Load user input QDialog pop up:
        self.user_ui = Ui_Dialog()
        self.user_dialog = QDialog(self.base)
        self.user_ui.setupUi(self.user_dialog)

        # Load about QDialog pop up:
        self.about_ui = Ui_About()
        self.about_dialog = QDialog(self.base)
        self.about_ui.setupUi(self.about_dialog)

        # Connect about UI button signals:
        self.about_ui.editButton.clicked.connect(self.update_about)
        self.about_ui.closeButton.clicked.connect(self.about_dialog.accept)

        # Load export QDialog pop up:
        self.export_ui = Ui_ExportAs()
        self.export_dialog = QDialog(self.base)
        self.export_ui.setupUi(self.export_dialog)

        # Connect export UI buttons:
        self.export_ui.dirButton.clicked.connect(self.get_export_dir)
        self.export_ui.buttonBox.rejected.connect(self.export_dialog.reject)
        self.export_ui.buttonBox.accepted.connect(self.export_dialog.accept)

        # Populate export UI combo boxes:
        self.export_ui.formatCombo.clear()
        self.export_ui.formatCombo.insertItem(1, ".TIFF", 0)
        self.export_ui.formatCombo.insertItem(2, ".JPG", 1)
        self.export_ui.formatCombo.setCurrentIndex(0)

        self.export_ui.colorCombo.clear()
        self.export_ui.colorCombo.insertItem(1, "RGB", 0)
        self.export_ui.colorCombo.insertItem(2, "Gray", 1)
        self.export_ui.colorCombo.setCurrentIndex(0)

        self.export_ui.scaleCombo.clear()
        self.export_ui.scaleCombo.insertItem(1, "100%", 100)
        self.export_ui.scaleCombo.insertItem(2, "75%", 75)
        self.export_ui.scaleCombo.insertItem(3, "50%", 50)
        self.export_ui.scaleCombo.insertItem(4, "25%", 25)
        self.export_ui.scaleCombo.setCurrentIndex(0)

        self.export_ui.namingCombo.clear()
        self.export_ui.namingCombo.insertItem(1, "Prida")
        self.export_ui.namingCombo.insertItem(2, "RR3D")
        self.export_ui.namingCombo.setCurrentIndex(0)

        # Connect current text changed on export type
        self.export_ui.namingCombo.currentTextChanged.connect(
            self.check_export_opt)

        # Load tag info QDialog popup:
        self.tag_ui = Ui_TagInfo()
        self.tag_dialog = QDialog(self.base)
        self.tag_ui.setupUi(self.tag_dialog)

        # Connect tag UI button signals:
        self.tag_ui.buttonBox.accepted.connect(self.tag_dialog.accept)
        self.tag_ui.tagSearch.textEdited.connect(self.update_tag_list)

    def mayday(self, msg):
        """
        Name:     Prida.mayday
        Inputs:   str, warning message (msg)
        Outputs:  None.
        Features: Opens a popup window displaying the warning text
        """
        QMessageBox.warning(self.base, "Warning", msg)

    def new_hdf(self):
        """
        Name:     Prida.new_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Prompts for user information, prompts for new HDF5 file name,
                  and signals to data for opening
        Depends:  mayday
        """
        self.logger.info('Button Clicked')
        self.logger.debug('requesting new file name from user')
        path = QFileDialog.getSaveFileName(
            self.base, "Save File", self.last_dir)[0]
        if path != '':
            if os.path.isdir(os.path.dirname(path)):
                self.last_dir = os.path.dirname(path)
            if os.path.splitext(path)[1] == '':
                path += '.hdf5'
                if os.path.isfile(path):
                    self.mayday(
                        'File already exists. Please select a different name.')
                else:
                    self.logger.debug("sending new file signal")
                    self.newFile.emit(path)
                    self.base.statusBar().showMessage("Opening file ...", 2000)
            elif os.path.splitext(path)[1] != '.hdf5':
                self.mayday('Only HDF5 files are permitted.')
            else:
                if os.path.isfile(path):
                    self.mayday(
                        'File already exists. Please select a different name.')
                else:
                    self.logger.debug("sending new file signal")
                    self.newFile.emit(path)
                    self.base.statusBar().showMessage("Opening file ...", 2000)

    def new_session(self):
        """
        Name:     Prida.new_session
        Inputs:   None.
        Outputs:  None
        Features: Saves attribute edits or creates a new session in hardware
                  mode, resets the progress bar, enters the RUN_EXP screen, and
                  begins the hardware thread
        Depends:  Editing*                  Hardware Mode*
                  - save_edits              - get_input_field
                  - enable_all_attrs        Preview Mode*
                  - set_defaults            - get_input_field
                                            - mayday
        """
        self.logger.info('Button Clicked')
        if self.editing:
            self.logger.info('Saving Edits')
            self.save_edits()
            self.logger.debug('set editing to False')
            self.editing = False
            self.enable_all_attrs()
            self.set_defaults()
            self.logger.info("sending get data list signal")
            self.getDataList.emit()
        else:
            # Get user defined PID and number of images:
            pid = str(self.view.c_pid.text())
            num_img = str(self.view.c_num_images.text())

            # Check that the two required fields are given:
            if pid == '':
                self.view.c_pid.setFocus()
                self.mayday("Please enter a valid PID before imaging.")
            elif num_img == '':
                self.view.c_num_images.setFocus()
                self.mayday(
                    "Please enter the number of images you wish to take.")
            else:
                # Create the PID metadata dictionary:
                self.logger.debug('building PID dictionary')
                pid_dict = {}
                for i in self.data.pid_attrs:
                    f_key = self.data.pid_attrs[i]['key']
                    f_name = self.data.pid_attrs[i]['qt_val']
                    f_type = self.data.pid_attrs[i]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    pid_dict[f_key] = f_val

                # Create the session metadata dictionary:
                self.logger.debug('building session dictionary')
                session_dict = {}
                for j in self.data.session_attrs:
                    f_key = self.data.session_attrs[j]['key']
                    f_name = self.data.session_attrs[j]['qt_val']
                    f_type = self.data.session_attrs[j]['qt_type']
                    f_val = self.get_input_field(f_name, f_type)
                    session_dict[f_key] = f_val

                # Import/2D have no configurable hardware parameters:
                session_dict['motor_driver'] = "None"
                session_dict['gear_ratio'] = "None"
                session_dict['motor_pause'] = "None"
                session_dict['motor_speed'] = "None"
                session_dict['motor_degrees'] = "None"
                session_dict['motor_step'] = "None"
                session_dict['mode'] = self.hardware.mode

                if self.hardware_mode:
                    # Imaging ...
                    self.logger.debug(
                        "Adding configurable parameters to session attributes")
                    if self.hardware.mode == "3D":
                        # Get 3D mode configurable parameters from hardware:
                        session_dict['motor_driver'] = "%s" % (
                            self.hardware.image_motor_driver)
                        session_dict['gear_ratio'] = "%s" % (
                            self.hardware.motor_gear_ratio)
                        session_dict['motor_pause'] = "%s" % (
                            self.hardware.image_settling_time)
                        session_dict['motor_speed'] = "%s" % (
                            self.hardware.motor_speed)
                        session_dict['motor_degrees'] = "%s" % (
                            self.hardware.motor_degrees)
                        session_dict['motor_step'] = "%s" % (
                            self.hardware.motor_step_type)

                    self.logger.info("view changed to RUN_EXP")
                    self.view.imagingLabel.setText(
                        "<html><head/><body><p>"
                        "<span style='font-size:28pt; font-weight:600;'>"
                        "Acquiring Images ...</span></p></body></html>")
                    self.view.stackedWidget.setCurrentIndex(RUN_EXP)
                    self.view.stackedWidget.show()

                    self.logger.info("signaling for new session")
                    self.createSession.emit(pid, pid_dict, session_dict)

                    self.logger.debug("updating hardware")
                    self.hardware.image_number = int(num_img)
                    self.hardware.image_pid = pid

                    self.logger.debug("preparing progress bar")
                    progress_max = self.hardware.get_maximum_value()
                    self.view.c_preview.clearBaseImage()
                    self.view.c_progress.setMinimum(0)
                    self.view.c_progress.setMaximum(progress_max)

                    self.logger.info("starting hardware thread")
                    self.hardware_thread.start(QThread.NormalPriority)
                    self.base.statusBar().showMessage(
                        "Capturing images ...", 2000)
                else:
                    # Importing ...
                    self.logger.debug("calculating files for import")
                    files_found = sorted(list(self.search_files.keys()))
                    num_files = len(files_found)
                    if num_files > 0:
                        self.logger.info("signaling for new session")
                        self.createSession.emit(pid, pid_dict, session_dict)

                        self.logger.debug("preparing progress bar")
                        self.view.c_progress.setMinimum(0)
                        self.view.c_progress.setMaximum(num_files - 1)

                        # Set appropriate data variables for processing:
                        msg = "Click OK to begin importing %d images." % (
                            num_files)
                        resp = QMessageBox.information(
                            self.base,
                            "Import Images",
                            msg,
                            buttons=QMessageBox.Ok | QMessageBox.Cancel,
                            defaultButton=QMessageBox.Ok)
                        if resp == QMessageBox.Ok:
                            self.logger.info("view changed to RUN_EXP")
                            self.view.imagingLabel.setText(
                                "<html><head/><body><p>"
                                "<span style='font-size:28pt; "
                                "font-weight:600;'>"
                                "Importing Images ..."
                                "</span></p></body></html>")
                            self.view.stackedWidget.setCurrentIndex(RUN_EXP)
                            self.view.stackedWidget.show()
                            self.logger.debug("User selected OK")
                            self.toImport.emit(files_found)
                        else:
                            self.logger.debug("User selected cancel")
                    else:
                        self.mayday("No images found for importing.")
                        self.logger.info("sending get data list signal")
                        self.getDataList.emit()

    def notice(self, msg):
        """
        Name:     Prida.notice
        Inputs:   str, notification message (msg)
        Outputs:  None.
        Features: Opens a popup window displaying the notification text
        """
        QMessageBox.information(self.base, "Notice", msg)

    def open_hdf(self):
        """
        Name:     Prida.open_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Prompts user for an existing HDF5 file and signals to data
                  for opening
        Depends:  - mayday
                  - start_spinner
        """
        self.logger.info("Button Clicked")
        self.logger.debug("prompting user for existing file")
        path = QFileDialog.getOpenFileName(
            self.base, "Select File", self.last_dir, filter='*.hdf5')[0]
        self.logger.debug("user selected file %s", path)
        if path != '':
            if os.path.isfile(path):
                # Update last directory:
                self.last_dir = os.path.dirname(path)

                # Change to VIEWER to see spinner:
                self.view.stacked_attrs.setCurrentIndex(SHEET_ATTR)
                self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)
                self.view.stackedWidget.setCurrentIndex(VIEWER)
                self.logger.debug("opening file %s", path)
                self.start_spinner("Opening file ...")
                self.openFile.emit(path)
                self.base.statusBar().showMessage("Opening file ...", 2000)
            else:
                self.mayday("Please select an existing file for opening.")

    def open_preview_photo(self):
        """
        Name:     Prida.open_preview_photo
        Inputs:   None.
        Outputs:  None.
        Features: Requests and opens a photo from file for previewing;
                  provides a utility to handle NEF files via context menu
                  (right click IDA) for saving as other file types
        Depends:  - mayday
                  - to_preview
        """
        my_file = QFileDialog.getOpenFileName(
            self.base, "Select File", self.last_dir, (img_import_str))[0]
        self.logger.debug("user selected file %s", my_file)
        if os.path.isfile(my_file):
            self.to_preview()
            self.start_spinner("Opening preview photo ...")
            self.getPreviewThumb.emit(my_file, VIEWER)
        else:
            self.mayday("Error! Preview image could not be opened.")

    def receive_alert(self, msg):
        """
        Name:     Prida.receive_alert
        Inputs:   str, alert message (msg)
        Outputs:  None.
        Features: Stops analysis spinner and displays alert message
        Depends:  - mayday
                  - stop_spinner
        """
        self.stop_spinner(ANALYSIS)
        self.mayday(msg)

    def reconfigure(self):
        """
        Name:     Prida.reconfigure
        Inputs:   None.
        Outputs:  None.
        Features: Replaces the configuration file in the Prida directory
        Depends:  - create_config_file
                  - notice
        """
        self.logger.info("Button Clicked")
        if self.prida_dir is not None:
            try:
                create_config_file(self.prida_dir, self.prida_conf)
            except IOError:
                self.logger.exception("Failed to create new config file")
            except:
                self.logger.exception("Encountered unexpected error")
            else:
                self.notice(
                    ("A new configuration file was created in %s. "
                     "Please exit this application and relaunch to read "
                     "changes in the configure file.") % (
                        os.path.join(self.prida_dir, self.prida_conf)))

    def reset_cropbox(self):
        """
        Name:     Prida.reset_cropbox
        Inputs:   None.
        Outputs:  None.
        Features: Zeros spinboxes and disables the crop group box
        """
        self.view.cropBox.setChecked(False)
        self.view.c_crop_top.setValue(0)
        self.view.c_crop_left.setValue(0)
        self.view.c_crop_bottom.setValue(100)
        self.view.c_crop_right.setValue(100)

    def resize_file_browse_sheet(self):
        """
        Name:     Prida.resize_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_browse_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.file_browse_sheet.resizeColumnToContents(col)

    def resize_photo_sheet(self):
        """
        Name:     Prida.resize_photo_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the photo_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.photo_sheet.resizeColumnToContents(col)

    def resize_search_sheet(self):
        """
        Name:     Prida.resize_search_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the file_search_sheet
        """
        self.logger.debug('resizing')
        for col in range(self.sheet_items + 2):
            self.view.file_search_sheet.resizeColumnToContents(col)

    def resize_session_sheet(self):
        """
        Name:     Prida.resize_session_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the session_sheet
        """
        self.logger.debug('resizing')
        for col in range(2):
            self.view.session_sheet.resizeColumnToContents(col)

    def resize_sheet(self):
        """
        Name:     Prida.resize_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Resizes columns to contents in the sheet
        """
        self.logger.debug("resizing")
        for col in range(self.sheet_items + 1):
            self.view.sheet.resizeColumnToContents(col)

    def run_calib(self):
        """
        Name:     Prida.run_calib
        Inputs:   None.
        Outputs:  None.
        Features: Signals the image thread to run camera calibration
        Depends:  mayday
        """
        self.logger.info("Button Clicked")
        if self.Xo is None:
            self.mayday("Please load an image!")
        elif self.ir is None:
            self.mayday("Please define all inputs!")
        else:
            calib_vals = (self.ir, self.ir_err, self.za, self.zg, self.zw)
            self.toCalibrate.emit(calib_vals)

    def run_crop(self):
        """
        Name:     Prida.run_crop
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to crop the process image
        Depends:  start_spinner
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Cropping ...", ANALYSIS)
        a_top = self.view.a_crop_top.value()
        a_bot = self.view.a_crop_bottom.value()
        a_left = self.view.a_crop_left.value()
        a_right = self.view.a_crop_right.value()
        self.toCrop.emit(a_top, a_bot, a_left, a_right)

    def run_delete(self):
        """
        Name:     Prida.run_delete
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to delete the selected element
        Depends:  - mayday
                  - start_spinner
        """
        self.logger.info("Button Clicked")

        try:
            obj_data = self.view.a_element_combo.currentData()
            if obj_data is not None:
                self.removeObject.emit(obj_data)
            else:
                self.mayday("Please find elements before deleting!")
        except:
            self.logger.exception("Failed to delete object")
            self.mayday("Are your UI files up-to-date?")
        else:
            if obj_data is not None and obj_data >= 0:
                self.start_spinner("Deleting 1 object ...", ANALYSIS)
            elif obj_data is not None:
                self.start_spinner("Deleting all objects ...", ANALYSIS)

    def run_filter(self):
        """
        Name:     Prida.run_filter
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to filter the process image
        Depends:  start_spinner
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Filtering ...", ANALYSIS)
        a_dust = self.view.a_dust_spin.value()
        self.toFilter.emit(a_dust)

    def run_smoother(self):
        """
        Name:     Prida.run_smoother
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to smooth the process image
        Depends:  start_spinner
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Smoothing ...", ANALYSIS)
        a_kern = self.view.a_kernel_spin.value()
        self.toSmooth.emit(a_kern)

    def run_find(self):
        """
        Name:     Prida.run_find
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to find calibration objects in the preview
                  image using Method 1
        Depends:  - mayday
                  - start_spinner
        """
        self.logger.info("Button Clicked")
        if self.view.a_preview.base_array is not None:
            self.start_spinner("Searching ...", ANALYSIS)
            self.toFind.emit(self.view.a_preview.base_array[:, :, 0])
        else:
            self.mayday("Please first load images!")

    def run_find2(self):
        """
        Name:     Prida.run_find2
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to find calibration objects in the preview
                  image using Method 2
        Depends:  - mayday
                  - start_spinner
        """
        self.logger.info("Button Clicked")
        if self.view.a_preview.base_array is not None:
            self.start_spinner("Searching ...", ANALYSIS)
            self.toFind2.emit(self.view.a_preview.base_array[:, :, 0])
        else:
            self.mayday("Please first load images!")

    def run_dsone(self):
        """
        Name:     Prida.run_dsone
        Inputs:   None.
        Outputs:  None.
        Signals:  showFirst (to ImageWorker)
        Features: Signals analysis thread to show the first session dataset
                  with appropriate scaling
        Depends:  start_spinner
        """
        self.logger.info("Button Clicked")
        self.view.a_preview.clearBaseImage("Image Display Area")

        self.start_spinner("Processing ...", ANALYSIS)
        a_sf = self.view.a_scale_combo.currentData()
        opts = {'scale': a_sf}
        self.showFirst.emit(opts)

    def run_merge(self):
        """
        Name:     Prida.run_merge
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to begin merge for analysis session
        Depends:  start_spinner
        """
        self.logger.info("Button Clicked")
        self.view.a_preview.clearBaseImage("Image Display Area")

        self.start_spinner("Processing ...", ANALYSIS)
        self.logger.debug("gathering process values")
        a_bg = self.view.a_bgcolor_combo.currentData()
        a_sf = self.view.a_scale_combo.currentData()
        a_opts = {
            'background': a_bg,
            'scale': a_sf}
        self.toMerge.emit(a_opts)

    def run_refresh(self):
        """
        Name:     Prida.run_refresh
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to refresh merge image
        Depends:  start_spinner
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Refreshing ...", ANALYSIS)
        self.toRefresh.emit()

    def run_show(self):
        """
        Name:     Prida.run_show
        Input:    None.
        Output:   None.
        Features: Signals to image to show selected object
        Depends:  - mayday
                  - start_spinner
        """
        self.logger.info("Button Clicked")
        try:
            obj_data = self.view.a_element_combo.currentData()
            if obj_data is not None:
                self.getObject.emit(obj_data)
            else:
                self.mayday("Please find elements first!")
        except:
            self.logger.exception("Failed to show object")
            self.mayday("Are your UI files up-to-date?")
        else:
            if obj_data is not None:
                self.start_spinner("Preparing object preview ...", ANALYSIS)

    def run_stretch(self):
        """
        Name:     Prida.run_stretch
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to stretch the grayscale luminosity of the
                  process image
        Depends:  start_spinner
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Stretching ...", ANALYSIS)
        self.toStretch.emit()

    def run_thresh(self):
        """
        Name:     Prida.run_thresh
        Inputs:   None.
        Outputs:  None.
        Features: Signals image to threshold the process image
        Depends:  start_spinner
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Thresholding ...", ANALYSIS)
        self.toThresh.emit()

    def run_undo(self):
        """
        Name:     Prida.run_undo
        Inputs:   None.
        Outputs:  None.
        Features: Signals image thread to show the current process image
        Depends:  start_spinner
        """
        self.logger.info("Button Clicked")
        self.start_spinner("Restoring process image ...", ANALYSIS)
        self.toUndo.emit()

    def save_analysis(self):
        """
        Name:     Prida.save_analysis
        Inputs:   None.
        Outputs:  None.
        Features: Prompts user for output directory, saves current preview
                  image as TIFF and signals for image to save process/element
                  data
        Depends:  - mayday
                  - notice
        """
        self.logger.info('Button Clicked')
        self.logger.debug('requesting new file name from user')
        path = QFileDialog.getExistingDirectory(self.base,
                                                "Select an output directory:",
                                                self.last_dir,
                                                QFileDialog.ShowDirsOnly)
        if path != '':
            # Update last directory:
            self.last_dir = path

            # Create the processed image output file name:
            bname = os.path.splitext(self.data.basename)[0]
            fname = "%s_proc.tiff" % (bname)
            file_path = os.path.join(path, fname)
            is_ready = False
            f_idx = 1
            while not is_ready:
                if os.path.isfile(file_path):
                    fname = "%s_proc-%d.tiff" % (bname, f_idx)
                    file_path = os.path.join(path, fname)
                    is_ready = False
                    f_idx += 1
                    self.logger.debug("updated output filename to %s",
                                      file_path)
                else:
                    is_ready = True

            try:
                self.logger.debug('saving image %s', file_path)
                self.view.a_preview.export_to_file(file_path)
            except:
                self.logger.exception("save failed!")
                self.mayday("Failed to save image.")
            else:
                if os.path.isfile(file_path):
                    self.notice("Image save was successful.")
                else:
                    self.mayday("Failed to save image.")

            self.logger.debug("preparing to export processing parameters")
            export_file = "prida-calib-export_%s.txt" % (__version__)
            export_file_path = os.path.join(path, export_file)
            session_date = self.view.a_sess_date.text()
            my_dict = {'path': export_file_path,
                       'name': self.data.basename,
                       'date': session_date}
            self.logger.debug("sending save signal")
            self.toSave.emit(my_dict)
        else:
            self.mayday("Image save canceled.")
            self.toPlot.emit()

    def save_compressed_copy(self):
        """
        Name:     Prida.save_compressed_copy
        Inputs:   None.
        Outputs:  None.
        Features: Requests a file path for saving a compressed copy of the
                  current HDF5 file and signals the data thread to begin
        """
        self.logger.info("Button Clicked!")
        default_name = "%s_comp%s" % os.path.splitext(self.data.basename)
        path = QFileDialog.getSaveFileName(
            self.base,
            "Save Compressed Copy As ...",
            os.path.join(self.last_dir, default_name),
            "HDF5 files (*.hdf5)")[0]
        self.logger.debug("Compresed file path: %s", path)
        if path != '':
            # Update last directory:
            self.last_dir = os.path.dirname(path)

            # Emit signal to data worker
            self.compressFile.emit(path)

    def save_edits(self):
        """
        Name:     Prida.save_edits
        Inputs:   None
        Outputs:  None
        Features: Signals data to save attributes for all enabled input fields
        Depends:  get_sheet_model_selection
        """
        if self.editing:
            if self.sheet_select_model.hasSelection():
                # Set cropping boolean and values based on user edits:
                if self.view.cropBox.isChecked():
                    self.crop_on = True
                    tval = self.view.c_crop_top.value()
                    bval = self.view.c_crop_bottom.value()
                    lval = self.view.c_crop_left.value()
                    rval = self.view.c_crop_right.value()
                    self.crop_extents['bottom'] = bval
                    self.crop_extents['left'] = lval
                    self.crop_extents['right'] = rval
                    self.crop_extents['top'] = tval
                else:
                    self.crop_on = False

                self.logger.debug("gathering user selection")
                s_path = self.get_sheet_model_selection()

                self.logger.debug(
                    "saving attributes from enabled INPUT_EXP fields")
                for i in self.data.pid_attrs:
                    f_key = self.data.pid_attrs[i]["key"]
                    f_name = self.data.pid_attrs[i]["qt_val"]
                    f_type = self.data.pid_attrs[i]["qt_type"]
                    f_val = self.get_input_field(f_name, f_type)
                    f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                    if f_bool:
                        self.logger.info("signaling data to save %s", f_key)
                        self.setAttr.emit(f_key, f_val, s_path)

                for j in self.data.session_attrs:
                    if self.data.session_attrs[j]['qt_val'] is not None:
                        f_key = self.data.session_attrs[j]["key"]
                        f_name = self.data.session_attrs[j]["qt_val"]
                        f_type = self.data.session_attrs[j]["qt_type"]
                        f_val = self.get_input_field(f_name, f_type)
                        f_bool = eval("self.view.%s.isEnabled()" % (f_name))
                        if f_bool:
                            self.logger.info(
                                "signaling data to save %s", f_key)
                            self.setAttr.emit(f_key, f_val, s_path)

    def search(self, pid_meta, terms):
        """
        Name:     Prida.search
        Inputs:   - list, PID meta data values (pid_meta)
                  - list, search terms (terms)
        Outputs:  bool (found)
        Features: Searches the pid metafields and returns true if all terms are
                  found anywhere within the fields
        """
        found = True
        for term in terms:
            found = found and any(term in m_field for m_field in pid_meta)
        return found

    def search_file_addition(self):
        """
        Name:     Prida.search_file_addition
        Inputs:   None.
        Outputs:  None.
        Features: Adds user-defined HDF5 files to search files dictionary and
                  signals data to read PID and attributes from HDF5 file
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.info("Button Clicked")
        self.logger.debug("requesting search files")
        items = QFileDialog.getOpenFileNames(self.base,
                                             "Select one or more files",
                                             self.last_dir,
                                             "HDF5 files (*.hdf5)")
        paths = items[0]
        num_paths = len(paths)
        if num_paths > 0:
            # Start spinner:
            self.start_spinner("Adding selection to list ...", FILE_SEARCH)
            path_idx = 0
            for path in paths:
                path_idx += 1
                self.logger.debug("processing selection %s", path)
                f_name = os.path.basename(path)
                f_name = os.path.splitext(f_name)[0]
                f_path = os.path.dirname(path)
                if f_name not in self.search_files:
                    # Add file to file_sheet_model:
                    self.logger.debug("adding search file %s to list", f_name)
                    self.search_files[f_name] = f_path
                    f_list = [QStandardItem(f_name), QStandardItem(f_path)]
                    self.file_sheet_model.appendRow(f_list)

                    self.logger.info("signaling data to add search file")
                    self.addSearch.emit(path, path_idx, num_paths)

    def search_file_removal(self):
        """
        Name:     Prida.search_file_removal
        Inputs:   None.
        Outputs:  None.
        Features: Removes a file from the file browse view and its
                  associated PID rows from the file search view
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.info("Button Clicked")
        if self.file_select_model.hasSelection():
            # Get the selection and put together the file name and path:
            self.logger.debug("gathering user selection")
            my_selection = self.file_sheet_model.itemFromIndex(
                self.file_select_model.selectedIndexes()[0])
            s_name = my_selection.text()
            f_name = "%s.hdf5" % (s_name)
            f_path = os.path.join(self.search_files[s_name], f_name)

            # Remove file from search_files:
            self.logger.debug("deleting file %s", f_name)
            del self.search_files[s_name]

            # Remove file's PIDs from file_search_sheet:
            self.removeSearch.emit(f_path)
        else:
            self.logger.warning("nothing selected!")
            err_msg = "No selection made. Please select a file to remove."
            self.mayday(err_msg)

    def search_hdf(self):
        """
        Name:     Prida.search_hdf
        Inputs:   None.
        Outputs:  None.
        Features: Changed view to FILE SEARCH and clears welcome greeter
        """
        self.logger.info("Button Clicked!")
        self.logger.info("View changed to FILE SEARCH")
        self.view.stackedWidget.setCurrentIndex(FILE_SEARCH)
        self.view.welcome.clearBaseImage('Welcome to Prida')

    def set_about(self):
        """
        Name:     Prida.set_about
        Inputs:   None.
        Outputs:  None.
        Features: Opens the user input popup and signals root attributes and
                  session defaults to data
        """
        self.logger.debug('loading input user popup')
        self.user_ui.user.setFocus()
        if self.user_dialog.exec_():
            r_user = str(self.user_ui.user.text())
            r_addr = str(self.user_ui.email.text())
            r_about = str(self.user_ui.about.toPlainText())

            self.logger.debug("signaling root attributes")
            self.setAttr.emit("user", r_user, "/")
            self.setAttr.emit("addr", r_addr, "/")
            self.setAttr.emit("about", r_about, "/")

            self.logger.debug("setting session defaults")
            self.setSessionDefault.emit("user", r_user)
            self.setSessionDefault.emit("addr", r_addr)

            self.logger.debug("updating about popup fields")
            self.about_ui.author_about.setText(r_user)
            self.about_ui.contact_about.setText(r_addr)
            self.about_ui.summary_about.setText(r_about)

    def set_defaults(self):
        """
        Name:     Prida.set_defaults
        Inputs:   None.
        Outputs:  None.
        Features: Sets GUI text default values
        """
        self.logger.debug("resetting INPUT_EXP PID default fields")
        self.set_input_field("c_pid", "QLineEdit", "")
        for i in self.data.pid_attrs:
            f_name = self.data.pid_attrs[i]["qt_val"]
            f_type = self.data.pid_attrs[i]["qt_type"]
            f_val = self.data.pid_attrs[i]["def_val"]
            try:
                self.set_input_field(f_name, f_type, f_val)
            except:
                self.set_input_field(f_name, f_type, '')

        self.logger.debug("resetting INPUT_EXP session default fields")
        for j in self.data.session_attrs:
            if self.data.session_attrs[j]['qt_val'] is not None:
                f_name = self.data.session_attrs[j]["qt_val"]
                f_type = self.data.session_attrs[j]["qt_type"]
                f_val = self.data.session_attrs[j]["def_val"]
                try:
                    self.set_input_field(f_name, f_type, f_val)
                except:
                    self.set_input_field(f_name, f_type, '')

    def set_greeter(self):
        """
        Name:     Prida.set_greeter
        Inputs:   None.
        Outputs:  None.
        Features: Sets greeter image
        Depends:  imopen
        """
        if self.greeter is not None:
            self.logger.debug("setting the greeter image")
            self.view.welcome.setBaseImage(imopen(self.greeter))
        else:
            self.view.welcome.setText(
                '<html><head/><body><p align="center">'
                '<span style="font-size:24pt; font-weight:600; '
                'color:#ffffff;">'
                'Welcome to Prida'
                '</span></p></body></html>')

    def set_input_field(self, field_name, field_type, field_value):
        """
        Name:     Prida.set_input_field
        Inputs:   - str, Qt input widget name (field_name)
                  - str, Qt input widget type (field_type)
                  - str, input value (field_value)
        Outputs:  None
        Features: Sets Qt input field

        @TODO: change empty string to None type
        """
        err_msg = "set_input_field:could not set Qt %s, %s, to value %s" % (
            field_type, field_name, field_value)

        if field_type == "QComboBox":
            # Takes an integer, find it!
            if field_value == '':
                index = 0
            else:
                index = eval('self.view.%s.findText("%s", %s)' % (
                    field_name,
                    field_value,
                    "Qt.MatchFixedString")
                )
            try:
                self.logger.debug("setting QComboBox index to %d", index)
                exec("self.view.%s.setCurrentIndex(%d)" % (
                    field_name,
                    index)
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        elif field_type == "QDateEdit":
            # Takes a QDate object
            if field_value == '':
                try:
                    self.logger.debug("setting QDateEdit to current date")
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        "QDate.fromString",
                        QDate.currentDate().toString("yyyy-MM-dd"))
                    )
                except:
                    self.logger.exception(err_msg)
                    raise ValueError(err_msg)
            else:
                try:
                    self.logger.debug("setting QDate edit to %s", field_value)
                    exec("self.view.%s.setDate(%s('%s', 'yyyy-MM-dd'))" % (
                        field_name,
                        "QDate.fromString",
                        field_value)
                    )
                except:
                    self.logger.exception(err_msg)
                    raise ValueError(err_msg)
        elif field_type == "QLineEdit":
            # Takes a string
            try:
                self.logger.debug("setting QLineEdit to %s", field_value)
                exec('self.view.%s.setText("%s")' % (
                    field_name,
                    field_value)
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        elif field_type == "QSpinBox":
            # Takes an integer
            if field_value == '':
                field_value = 0
            try:
                self.logger.debug("setting QSpinBox to %d", int(field_value))
                exec("self.view.%s.setValue(%d)" % (
                    field_name,
                    int(field_value))
                )
            except:
                self.logger.exception(err_msg)
                raise ValueError(err_msg)
        else:
            self.logger.error(err_msg)
            raise ValueError(err_msg)

    def set_ida(self, my_array, enum):
        """
        Name:     Prida.set_ida
        Inputs:   - ndarray, image dataset array (my_array)
                  - int, view enumerator (enum)
        Output:   None.
        Features: Catches signal from data; sets IDA base image;
                  crops if appropriate
        """
        if enum == VIEWER:
            self.stop_spinner()
            self.logger.debug("setting IDA base image to selection")
            if self.crop_on:
                my_array = rel_crop(my_array,
                                    self.crop_extents['top'],
                                    self.crop_extents['right'],
                                    self.crop_extents['bottom'],
                                    self.crop_extents['left'])
            self.view.ida.setBaseImage(my_array)
        elif enum == ANALYSIS:
            self.logger.debug("clearing IDA base image")
            self.movie.stop()
            self.view.a_spinner.clear()
            self.logger.debug("setting IDA base image to selection")
            self.view.a_preview.setBaseImage(my_array)

    def set_qlabel_text(self, label, text):
        """
        Features: Convenience function for setting QLabel text;
        """
        try:
            eval('self.view.{}.setText("{}")'.format(label, text))
        except:
            self.logger.exception(
                "Failed to set label %s with text %s" % (label, text))
            self.mayday("Have you updated your UI files lately?")

    def set_photo_sheet_model(self, attr_dict):
        """
        Name:     Prida.set_photo_sheet_model
        Inputs:   dict, standard photo attributes (attr_dict)
        Outputs:  None.
        Features: Receives the photo attribute signal from data; sets
                  attribute values to photo sheet
        """
        self.logger.debug("setting photo attributes")
        for i in attr_dict:
            exec('%s(%d, 1, QStandardItem("%s"))' % (
                "self.photo_sheet_model.setItem", i, attr_dict[i]['val'])
            )

    def set_session_fields(self, img_path, img_num):
        """
        Name:     Prida.set_session_fields
        Inputs:   - str, path to image file (img_path)
                  - int, number of images (img_num)
        Outputs:  None.
        Features: Sets the session date and session image number fields in
                  INPUT_EXP based on the creation date of the given image;
                  used during imports
        Depends:  - get_ctime
                  - read_exif_tags
                  - set_input_field
        """
        self.logger.debug("reading tags from %s", img_path)
        tags = read_exif_tags(img_path)
        ctime = tags.get("EXIF DateTimeOriginal", str(get_ctime(img_path)))
        cdatetime = ctime.split(" ")
        if len(cdatetime) == 2:
            cdate = cdatetime[0]
            cdate = cdate.replace(":", "-")
            self.set_input_field("c_session_date", "QDateEdit", cdate)

        self.set_input_field("c_num_images", "QLineEdit", str(img_num))

    def set_session_sheet_model(self, attr_dict):
        """
        Name:     Prida.set_session_sheet_model
        Inputs:   dict, session attributes (attr_dict)
        Outputs:  None.
        Features: Receives the session attribute signal from data; sets
                  attribute values to session sheet
        """
        self.logger.debug("loading session fields")
        for i in attr_dict:
            exec('%s(%d, 1, QStandardItem("%s"))' % (
                "self.session_sheet_model.setItem", i, attr_dict[i]['cur_val'])
            )

    def set_sheet_thumbs(self, img_count, img_tot, img_path, img_array, enum):
        """
        Name:     Prida.set_sheet_thumbs
        Inputs:   - int, image counter index (img_count)
                  - int, total number of images (img_tot)
                  - str, image path (img_path)
                  - ndarray, image dataset array (my_array)
                  - int, view enumerator (enum)
        Outputs:  None.
        Features: Catches signal from data; adds thumbnails to the image sheet
                  model; crops if appropriate
        Depends:  - is_binary
                  - is_grayscale
                  - rel_crop
                  - stop_spinner
        """
        self.logger.info("caught signal for thumbnail %d", img_count)
        if img_array.size != 0:
            if is_grayscale(img_array):
                # Binary image support:
                if is_binary(img_array):
                    self.logger.debug("found binary image!")
                    self.logger.debug("scaling binary pixels for grayscale")
                    if img_array.max() == 1:
                        img_array[numpy.where(img_array == 1)] *= 255
                    self.logger.debug("pixel scaling complete")

                # Grayscale image support:
                try:
                    self.logger.debug("found grayscale image!")
                    temp_array = numpy.zeros(
                        (img_array.shape[0], img_array.shape[1], 3))
                    temp_array[:, :, 0] = numpy.copy(img_array)
                    temp_array[:, :, 1] = numpy.copy(img_array)
                    temp_array[:, :, 2] = numpy.copy(img_array)
                except:
                    self.logger.exception("something is awry")
                    raise
                else:
                    img_array = numpy.copy(temp_array.astype('uint8'))

            # Session cropping support:
            if self.crop_on:
                img_array = rel_crop(img_array,
                                     self.crop_extents['top'],
                                     self.crop_extents['right'],
                                     self.crop_extents['bottom'],
                                     self.crop_extents['left'])

            try:
                self.logger.debug("creating thumbnail %d", img_count)
                image = Image.fromarray(img_array)
                qt_image = ImageQt.ImageQt(image)
                pix = QPixmap.fromImage(qt_image)
            except:
                self.logger.exception("Failed to set thumbnail")
            else:
                self.img_list.append(qt_image)
                if img_count == 9999:
                    # To help distinguish preview image in the thumbnail list
                    item = QStandardItem("Preview")
                else:
                    item = QStandardItem(str(img_count))
                item.setIcon(QIcon(pix))
                item.setData(img_path)
                if enum == VIEWER:
                    self.logger.debug("adding thumbnail %d", img_count)
                    self.img_sheet_model.appendRow(item)
                elif enum == ANALYSIS:
                    self.logger.warning("received deprecated signal!")
                else:
                    self.logger.warning(
                        "received unknown enum %d for updating thumbnails",
                        enum)

        # Stop spinner when last thumb arrives:
        if img_count == img_tot:
            self.stop_spinner()

    def show_analysis(self, img_array):
        """
        Name:     Prida.show_analysis
        Input:    numpy.ndarray, image array (img_array)
        Output:   None.
        Features: Sets preview image to analysis IDA
        """
        self.stop_spinner(ANALYSIS)
        self.logger.debug("caught image with size %d", img_array.size)
        if img_array.size > 1:
            self.logger.debug("setting IDA base image to selection")
            try:
                self.view.a_preview.setBaseImage(img_array)
            except Exception as err:
                self.logger.exception("failed to set analysis base image")
                self.view.a_preview.clearBaseImage()
                self.mayday(
                    ("Error! Could not load preview image. "
                     "Try cropping to smaller extent. {}").format(str(err)))

    def show_context_menu(self, pos=None):
        """
        Name:     Prida.show_context_menu
        Inputs:   [optional] QPoint, where the click occurred (pos)
        Outputs:  None.
        Features: Opens context menu for right clicking on IDA (in view's page)
                  to request the save image as feature
        Depends:  - mayday
                  - notice
        Ref:      http://www.setnode.com/blog/right-click-context-menus-with-qt
        """
        gpos = self.view.page.mapToGlobal(pos)
        my_menu = QMenu()
        my_menu.addAction("Save Image As ...")
        selected_item = my_menu.exec_(gpos)
        if selected_item:
            path = QFileDialog.getSaveFileName(
                self.base, "Save Image As ...", self.last_dir)[0]
            if path != '':
                # Check to see if user selected a supported image format:
                p_ext = os.path.splitext(path)[1]
                if p_ext not in img_types:
                    err_msg = (
                        "Please select one of the following image formats: ")
                    err_msg += ", ".join(img_types)
                    self.mayday(err_msg)
                else:
                    self.logger.debug("Saving image to %s", path)
                    try:
                        self.view.ida.export_to_file(path, False)
                    except:
                        self.logger.exception("save failed!")
                        self.mayday(
                            "Failed to save image. "
                            "Please check your file name and try again.")
                    else:
                        if os.path.isfile(path):
                            self.last_dir = os.path.dirname(path)
                            self.notice("Image save was successful.")
                        else:
                            self.mayday("Failed to save image.")
        else:
            self.logger.debug("User did not make selection")

    def show_ida(self, selection):
        """
        Name:     Prida.show_ida
        Input:    obj (selection)
        Output:   None.
        Features: Switches to IDA page and PHOTO_ATTR page in VIEWER and sets
                  IDA base image when a thumbnail is selected;
                  handles image orientation and preview images
        """
        if selection.indexes():
            self.logger.debug("selection made; changing view to IDA_VIEW")
            self.view.stacked_attrs.setCurrentIndex(PHOTO_ATTR)
            self.view.stacked_sheets.setCurrentIndex(IDA_VIEW)

            self.logger.debug("gathering user selection")
            img_path = self.img_sheet_model.itemFromIndex(
                selection.indexes()[0]).data()

            if os.path.isfile(img_path):
                # For take preview images
                self.getPreviewPhotoAttrs.emit(img_path, VIEWER)
            else:
                # Signal to update photo attribute table and get IDA image:
                self.getPhotoAttrs.emit(img_path)
                self.getIDA.emit(img_path, VIEWER)
            self.start_spinner("Opening preview ...")

    def show_tags(self, tag_dict):
        """
        Name:     Prida.show_tags
        Inputs:   dict, tag dictionary (tag_dict)
        Outputs:  None.
        Features: Socket for data thread's tagsGotten signal; builds the tag
                  list for the selected image
        """
        try:
            self.tag_dict = tag_dict
            self.update_tag_list()
        except:
            self.logger.exception("Failed to load QTableWidget")
        else:
            self.tag_ui.tagSearch.setFocus()
            if self.tag_dialog.exec_():
                # Reset variables:
                self.tag_ui.listWidget.clear()
                self.tag_ui.tagSearch.clear()
                self.tag_dict = {}

    def shutdown(self):
        """
        Name:     Prida.shutdown
        Inputs:   None.
        Outputs:  None.
        Features: Shuts down all open processes and threads
        """
        try:
            self.stopFile.emit()
            self.data.quit()
            self.data_thread.wait()
            if self.hardware_mode:
                self.hardware_thread.quit()
                self.hardware_thread.wait()
            if self.analysis_mode:
                self.analysis_thread.quit()
                self.analysis_thread.wait()
        except:
            self.logger.warning("failed to shutdown")
        else:
            self.logger.info("app is shutting down")
        finally:
            self.quit()

    def start_spinner(self, msg, enum=VIEWER):
        """
        Name:     Prida.start_spinner
        Inputs:   str, spinner's message text (msg)
        Outputs:  None.
        Features: Starts the spinner GIF and with associated text
        """
        if enum == VIEWER:
            self.logger.debug("updated spinner message in VIEWER to %s", msg)
            self.view.spinner_txt.setText(msg)
            if self.movie.state() == QMovie.NotRunning:
                self.logger.debug("starting spinner in VIEWER")
                self.view.spinner_gif.setMovie(self.movie)
                self.view.spinner_gif.show()
                self.movie.start()
                self.logger.debug("spinner started in VIEWER")
        elif enum == ANALYSIS:
            self.logger.debug("updated spinner message in ANALYSIS")
            self.view.a_label.setText(msg)
            if self.movie.state() == QMovie.NotRunning:
                self.logger.debug("starting spinner in ANALYSIS")
                self.view.a_spinner.setMovie(self.movie)
                self.view.a_spinner.show()
                self.movie.start()
                self.logger.debug("spinner started in ANALYSIS")
        elif enum == FILE_SEARCH:
            self.logger.debug("updated spinner message in FILE_SEARCH")
            self.view.fs_label.setText(msg)
            if self.movie.state() == QMovie.NotRunning:
                self.logger.debug("starting spinner in FILE_SEARCH")
                self.view.fs_spinner.setMovie(self.movie)
                self.view.fs_spinner.show()
                self.movie.start()
                self.logger.debug("spinner started in FILE_SEARCH")
        else:
            self.logger.warning("could not process spinner enum %d", enum)

    def stop_spinner(self, enum=VIEWER):
        """
        Name:     Prida.stop_spinner
        Inputs:   None.
        Outputs:  None.
        Features: Stops the spinner GIF and clears the associated text
        """
        if self.movie.state() == QMovie.Running:
            self.logger.debug("stopping spinner")
            self.movie.stop()
        if enum == VIEWER:
            self.logger.debug("clearing spinner message in VIEWER")
            self.view.spinner_txt.setText("")
            self.view.spinner_gif.clear()
        elif enum == ANALYSIS:
            self.logger.debug("clearing spinner message in ANALYSIS")
            self.view.a_spinner.clear()
        elif enum == FILE_SEARCH:
            self.logger.debug("clearing spinner message in FILE_SEARCH")
            self.view.fs_label.setText("")
            self.view.fs_spinner.clear()

    def take_preview_photo(self):
        """
        Name:     Prida.take_preview_photo
        Inputs:   None.
        Outputs:  None.
        Features: Captures and saves a preview image from the camera
        Depends:  - mayday
                  - start_spinner
                  - to_preview
        """
        if self.hardware_mode:
            self.to_preview()
            self.start_spinner("Taking preview photo ...")
            photo_path = self.hardware.take_preview_photo()
            if photo_path is not None:
                if os.path.isfile(photo_path):
                    self.getPreviewThumb.emit(photo_path, VIEWER)
                else:
                    self.mayday(
                        "Error! Could not locate captured preview image.")

    def test_gear_ratio(self):
        """
        Name:     Prida.test_gear_ratio
        Inputs:   None.
        Outputs:  None.
        Features: Attempts a full rotation of the bearing based on the
                  prescribed gear ratio
        Depends:  - mayday
                  - notice
        """
        if self.hardware.mode == '3D':
            resp = QMessageBox.information(
                self.base,
                "Gear Ratio Test",
                ("This will attempt to rotate the bearing a full rotation "
                 "(%d steps). "
                 "Please mark the bearing starting location and click OK to "
                 "begin or Cancel to exit.") % (self.hardware.total_steps),
                buttons=QMessageBox.Ok | QMessageBox.Cancel,
                defaultButton=QMessageBox.Ok)
            if resp == QMessageBox.Ok:
                try:
                    self.hardware.test_gear_ratio()
                except:
                    self.mayday("Error! Gear ratio test failed.")
                else:
                    self.notice(
                        "Gear ratio test complete. "
                        "Please ensure a full rotation has occurred; "
                        "else change the gear ratio and try again.")

    def test_motor(self):
        """
        Name:     Prida.test_motor
        Inputs:   None.
        Outputs:  None.
        Features: Requests a number of steps and tests the motor operation
        Depends:  - mayday
                  - notice
        """
        if self.hardware.mode == '3D':
            nsteps, ok = QInputDialog.getInt(
                self.base,
                "Run Motor Test",
                "Number of steps to take:",
                value=1, min=0, step=1)
            if ok:
                try:
                    self.hardware.test_motor(nsteps)
                except:
                    self.mayday("Error! Motor testing failed.")
                else:
                    self.notice("Signaled motor to take %d steps." % (nsteps))

    def to_preview(self):
        """
        Name:     Prida.to_preview
        Inputs:   None.
        Outputs:  None.
        Features: Sets the stage for a preview image;
                  clears IDA, clears selections, clears thumbnails,
                  changes view to VIEWER of session table and session attrs
        """
        self.view.ida.clearBaseImage("Image Display Area")
        self.view.img_select.clearSelection()
        self.view.sheet.clearSelection()
        self.view.sheet.collapseAll()
        self.img_sheet_model.clear()
        self.img_list = []
        self.logger.info("view changed to VIEWER")
        self.view.stackedWidget.setCurrentIndex(VIEWER)
        self.view.stacked_attrs.setCurrentIndex(SHEET_ATTR)
        self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)

    def to_sheet(self):
        """
        Name:     Prida.to_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Switches from IDA to sheet view; releasing resources
        Depends:  clear_photo_sheet_model
        """
        self.logger.info("Button Clicked")
        self.logger.debug("clearing IDA base image")
        self.view.ida.clearBaseImage("Image Display Area")
        self.logger.debug("clearing img_select")
        self.view.img_select.clearSelection()
        self.logger.debug("clearing photo attributes")
        self.clear_photo_sheet_model()
        self.logger.info("view changed to SHEET_VIEW")
        self.view.stacked_attrs.setCurrentIndex(SHEET_ATTR)
        self.view.stacked_sheets.setCurrentIndex(SHEET_VIEW)

    def tool_not_enabled(self):
        """
        THIS IS A PLACEHOLDER FUNCTION
        """
        self.logger.warning("tool not enabled!")
        err_msg = "This tool has not be enabled yet."
        self.mayday(err_msg)

    def update_about(self):
        """
        Name:     Prida.update_about
        Inputs:   None.
        Outputs:  None.
        Features: Sets user input fields and opens a new user input popup
        Depends:  set_about
        """
        self.logger.debug("setting user input values")
        overview = self.data.about
        self.user_ui.user.setText(overview.get("Author", ""))
        self.user_ui.email.setText(overview.get("Contact", ""))
        self.user_ui.about.setText(overview.get("Summary", ""))

        self.logger.debug("requesting new input values")
        self.set_about()

    def update_analysis_color(self, bg_color):
        """
        Name:     Prida.update_analysis_color
        Inputs:   str, background color (bg_color)
        Outputs:  None.
        Features: Updates the QLabel with current background color
        """
        try:
            self.view.a_background.clear()
            self.view.a_background.setText(bg_color)
        except:
            self.mayday("Have you updated your UI files?")

    def update_analysis_combos(self):
        """
        Name:     Prida.update_analysis_combos
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QComboBox with exclude options
        """
        self.logger.debug("setting the analysis view QComboBox values")
        try:
            self.view.a_bgcolor_combo.clear()
            self.view.a_bgcolor_combo.insertItem(1, "Black", 0)
            self.view.a_bgcolor_combo.insertItem(2, "White", 1)
            self.view.a_bgcolor_combo.setCurrentIndex(0)

            self.view.a_scale_combo.clear()
            for i in range(1, 5):
                s_factor = int(100 - 25*(i-1))
                s_label = "%d%%" % (s_factor)
                self.view.a_scale_combo.insertItem(i, s_label, s_factor)
            self.view.a_scale_combo.setCurrentIndex(0)

            self.view.a_cam_combo.clear()
            cam_idx = 0
            for d in sorted(list(self.supported_cameras.keys())):
                cam = self.supported_cameras[d]
                cam_idx += 1
                self.view.a_cam_combo.insertItem(cam_idx, cam, d)

            self.view.a_orient_combo.clear()
            self.view.a_orient_combo.insertItem(1, "Portrait", 0)
            self.view.a_orient_combo.insertItem(2, "Landscape", 1)
        except AttributeError:
            self.logger.exception("Could not update QComboBoxes")
            self.mayday(
                "An error has been encountered. "
                "Please make certain your UI files are up-to-date.")
        except:
            self.logger.exception("Encountered unexpected error.")
            raise

    def update_analysis_found(self, num_found):
        """
        Name:     Prida.update_analysis_found
        Inputs:   int, number of objects found (num_found)
        Outputs:  None.
        Features: Updates the QCombo with current number of objects found
        """
        try:
            self.view.a_element_combo.clear()
            if num_found > 0:
                for i in range(num_found):
                    j = i + 1
                    self.view.a_element_combo.insertItem(j, str(j), i)
                self.view.a_element_combo.insertItem(0, "All", -1)
                self.view.a_element_combo.setCurrentIndex(0)
        except:
            self.logger.exception("Could not update element combo box")
            self.mayday("Are your UI files up-to-date?")

    def update_analysis_process(self, msg):
        """
        Name:     Prida.update_analysis_process
        Inputs:   str, progress message (msg)
        Outputs:  None.
        Features: Updates the QLabel with current progress
        """
        self.view.a_label.clear()
        self.view.a_label.setText(msg)

    def update_analysis_progress(self, val, tot):
        """
        Name:     Prida.update_analysis_progress
        Inputs:   - int, current progress value (val)
                  - int, maximum progress value (tot)
        Outputs:  None.
        Features: Updates the QProgressBar with current progress
        """
        self.view.a_progress.setMinimum(0)
        self.view.a_progress.setMaximum(tot)
        self.view.a_progress.setValue(val)

    def update_analysis_merge(self, h, w):
        """
        Name:     Prida.update_analysis_merge
        Inputs:   - int, image pixel height (h)
                  - int, image pixel width (w)
        Outputs:  None.
        Features: Updates the cropping QSpinBoxes with current image size
        """
        self.view.a_crop_left.clear()
        self.view.a_crop_left.setRange(0, w)
        self.view.a_crop_left.setValue(0)
        self.view.a_crop_left.setSingleStep(10)

        self.view.a_crop_right.clear()
        self.view.a_crop_right.setRange(0, w)
        self.view.a_crop_right.setValue(w)
        self.view.a_crop_right.setSingleStep(10)

        self.view.a_crop_top.clear()
        self.view.a_crop_top.setRange(0, h)
        self.view.a_crop_top.setValue(0)
        self.view.a_crop_top.setSingleStep(10)

        self.view.a_crop_bottom.clear()
        self.view.a_crop_bottom.setRange(0, h)
        self.view.a_crop_bottom.setValue(h)
        self.view.a_crop_bottom.setSingleStep(10)

    def update_analysis_range(self, img_range):
        """
        Name:     Prida.update_analysis_range
        Inputs:   str, image range (img_range)
        Outputs:  None.
        Features: Updates the QLabel with current image pixel range
        """
        try:
            self.view.a_img_range.clear()
            self.view.a_img_range.setText(img_range)
        except:
            self.mayday("Have you updated your UI files lately?")

    def update_analysis_shape(self, img_shape):
        """
        Name:     Prida.update_analysis_shape
        Inputs:   str, image shape (img_shape)
        Outputs:  None.
        Features: Sets image pixel height calibration parameter, Xo, and
                  updates the QLabel with current image shape.
                  Calibration parameter setter
        Depends:  update_calib_vals
        """
        self.Xo = img_shape[0]   # Either an integer or Nonetype
        self.view.a_img_shape.clear()
        self.view.a_img_shape.setText(str(img_shape))
        self.update_calib_vals()

    def update_analysis_view(self, s_dict, num_img):
        """
        Name:     Prida.update_analysis_view
        Inputs:   - dict, session attributes (s_dict)
                  - int, session image list (num_img)
        Outputs:  None.
        Features: Receives session attribute dictionary and number of images
                  and updates analysis view labels
        """
        # Set session attributes to GUI labels:
        session_title = s_dict[2]['cur_val']
        self.view.a_sess_title.setText(session_title)
        session_date = s_dict[3]['cur_val']
        self.view.a_sess_date.setText(session_date)
        self.view.a_sess_count.setText(str(num_img))

        # Open analysis view
        self.stop_spinner()
        self.view.stackedWidget.setCurrentIndex(ANALYSIS)

    def update_calib_ir(self):
        """
        Name:     Prida.update_calib_ir
        Inputs:   None.
        Outputs:  None.
        Features: Calculates the image resolution conversion factor, Ir, used
                  in the calibration, and sets the QLabel (mm/px)
        Depends:  set_qlabel_text
        """
        if self.xa is not None and self.xg is not None and self.xw is not None:
            xo, xo_err = calc_xo(self.xa, self.xa_err,
                                 self.xg, self.xg_err,
                                 self.xw, self.xw_err)
            if self.Xo is not None:
                self.ir, self.ir_err = calc_ir(xo, xo_err, self.Xo)
                self.set_qlabel_text("a_ir", "{:0.3f}{}{:0.3f}".format(
                    1000*self.ir, "\u00B1", 1000*self.ir_err))
            else:
                self.ir = None
                self.ir_err = None
                self.set_qlabel_text("a_ir", "N/A")
        else:
            self.ir = None
            self.ir_err = None
            self.set_qlabel_text("a_ir", "N/A")

    def update_calib_params(self, calib_params):
        """
        Name:     Prida.update_calib_params
        Inputs:   tuple, calibration parameters (calib_params)
        Outputs:  None.
        Features: Socket for image threads isCalibrated signal; updates QLabels
        """
        (roll, roll_err, yt, yt_err, pitch, pitch_err) = calib_params
        try:
            Yt = int(yt/self.ir + 0.5)
            Yt_err = int(yt_err/self.ir + 0.5)
            self.set_qlabel_text(
                "a_Yt", "{:d}{}{:d}".format(Yt, "\u00B1", Yt_err))
        except:
            self.logger.exception("Failed to calculate Yt")
        self.set_qlabel_text("a_roll", "{:0.3f}{}{:0.3f}".format(
            roll, "\u00B1", roll_err))
        self.set_qlabel_text("a_trans", "{:0.3f}{}{:0.3f}".format(
            1000*yt, "\u00B1", 1000*yt_err))
        self.set_qlabel_text("a_pitch", "{:0.3f}{}{:0.3f}".format(
            pitch, "\u00B1", pitch_err))

    def update_calib_vals(self):
        """
        Name:     Prida.update_calib_vals
        Inputs:   None.
        Outputs:  None.
        Features: Updates calibration values and sets their respective QLabels
        Depends:  - set_qlabel_text
                  - update_cam_alpha    - update_calib_xi
                  - update_calib_xa     - update_calib_xg
                  - update_calib_xw     - update_calib_ir
        """
        self.update_cam_alpha()
        self.update_calib_xi()
        self.update_calib_xa()
        self.update_calib_xg()
        self.update_calib_xw()
        self.update_calib_ir()

    def update_calib_xi(self):
        """
        Name:     Prida.update_calib_xi
        Inputs:   None.
        Outputs:  None.
        Features: Calculates xi, the variable substitute used in the
                  calibration and updates the QLabel
        Depends:  set_qlabel_text
        """
        if self.alpha is not None:
            try:
                self.xi = numpy.sqrt(1.0 - numpy.cos(numpy.radians(
                    self.alpha)))
            except:
                self.logger.exception("Failed to calculate xi")
                self.xi = None
            else:
                self.set_qlabel_text("a_xi", "{:0.2f}".format(self.xi))
        else:
            self.xi = None
            self.set_qlabel_text("a_xi", "N/A")

    def update_calib_xa(self):
        """
        Name:     Prida.update_calib_xa
        Inputs:   None.
        Outputs:  None.
        Features: Calculates xa, the x-distance light travels through air,
                  used in the calibration and updates the QLabel
        Depends:  - calc_xa
                  - set_qlabel_text
        """
        if self.alpha is not None and self.za is not None:
            try:
                self.xa, self.xa_err = calc_xa(self.za, self.alpha)
            except:
                self.logger.exception("Failed to calculate xa")
                self.xa = None
                self.xa_err = None
            else:
                self.set_qlabel_text(
                    "a_xa", "{:0.2f}{}{:0.2f}".format(
                        100*self.xa, "\u00B1", 100*self.xa_err))
        else:
            self.xa = None
            self.xa_err = None
            self.set_qlabel_text("a_xa", "N/A")

    def update_calib_xg(self):
        """
        Name:     Prida.update_calib_xg
        Inputs:   None.
        Outputs:  None.
        Features: Calculates xg, the x-distance light travels through the tank
                  wall, used in the calibration and updates the QLabel
        Depends:  - calc_xg
                  - set_qlabel_text
        """
        if self.xi is not None and self.zg is not None:
            try:
                self.xg, self.xg_err = calc_xg(self.zg, self.xi)
            except:
                self.logger.exception("Failed to calculate xg")
                self.xg = None
                self.xg_err = None
            else:
                self.set_qlabel_text(
                    "a_xg", "{:0.2f}{}{:0.2f}".format(
                        100*self.xg, "\u00B1", 100*self.xg_err))
        else:
            self.xg = None
            self.xg_err = None
            self.set_qlabel_text("a_xg", "N/A")

    def update_calib_xw(self):
        """
        Name:     Prida.update_calib_xw
        Inputs:   None.
        Outputs:  None.
        Features: Calculates xw, the x-distance light travels through water,
                  used in the calibration, and updates the QLabel
        Depends:  set_qlabel_text
        """
        if self.xi is not None and self.zw is not None:
            try:
                self.xw, self.xw_err = calc_xw(self.zw, self.xi)
            except:
                self.logger.exception("Failed to calculate xw")
                self.xw = None
                self.xw_err = None
            else:
                self.set_qlabel_text(
                    "a_xw", "{:0.2f}{}{:0.2f}".format(
                        100*self.xw, "\u00B1", 100*self.xw_err))
        else:
            self.xw = None
            self.xw_err = None
            self.set_qlabel_text("a_xw", "N/A")

    def update_cam_alpha(self):
        """
        Name:     Prida.update_cam_alpha
        Inputs:   None.
        Outputs:  None.
        Features: Calculates alpha, the camera's field of view angle, used in
                  the calibration, and sets the QLabel
        Depends:  - mayday
                  - set_qlabel_text
        """
        if self.xs is not None and self.zf is not None:
            try:
                self.alpha = 2.0*numpy.arctan(self.xs/(2.0*self.zf))
                self.alpha *= 180.0/numpy.pi
            except:
                self.logger.exception("something's gone awry!")
                self.mayday("Failed to update alpha")
            else:
                self.set_qlabel_text("a_alpha", "{:0.2f}".format(self.alpha))
        else:
            self.alpha = None
            self.set_qlabel_text("a_alpha", "N/A")

    def update_cam_xs(self, idx=None):
        """
        Name:     Prida.update_cam_xs
        Inputs:   [optional] int, selection index (idx)
        Outputs:  None.
        Features: Socket for selection change on both camera type and
                  orientation combo boxes; looks up the camera selection from
                  supported cameras list and sets the camera sensor height, xs,
                  based on the orientation; if the camera selection is "other"
                  prompts the user for the appropriate sensor height info
                  and signals to update calibration values
        Depends:  - set_qlabel_text
                  - update_calib_vals
        """
        dcamera = self.view.a_cam_combo.currentData()
        dorient = self.view.a_orient_combo.currentData()
        cam_name = self.supported_cameras.get(dcamera, 0)
        cam_sens = self.camera_info.get(cam_name, (None, None))
        self.xs = cam_sens[dorient]
        if self.xs is not None:
            self.xs /= 1000.0   # mm to m
            self.set_qlabel_text("a_xs", "{:0.2f}".format(100*self.xs))
        elif dcamera == 9999:
            # Request camera sensor width/height from user:
            if dorient == 0:
                xs_temp, ok = QInputDialog.getDouble(
                    self.base,
                    "Camera Sensor Settings",
                    "Camera sensor width (mm):",
                    value=23.5, min=0, max=9999, decimals=1)
                if ok:
                    self.xs = float(xs_temp)
                    self.xs /= 1000.0  # mm to m
                    self.set_qlabel_text("a_xs", "{:0.2f}".format(100*self.xs))
            else:
                xs_temp, ok = QInputDialog.getDouble(
                    self.base,
                    "Camera Sensor Settings",
                    "Camera sensor height (mm):",
                    value=15.6, min=0, max=9999, decimals=1)
                if ok:
                    self.xs = float(xs_temp)
                    self.xs /= 1000.0  # mm to m
                    self.set_qlabel_text("a_xs", "{:0.2f}".format(100*self.xs))
        else:
            self.set_qlabel_text("a_xs", "N/A")
        self.update_calib_vals()

    def update_cam_zf(self, lens_size):
        """
        Name:     Prida.update_cam_zf
        Inputs:   float, camera lens focal length, mm (lens_size)
        Outputs:  None.
        Features: Socket for text edited in the camera lens size input field;
                  sets camera lens focal length, zf, and updates calibration
                  values
        Depends:  - set_qlabel_text
                  - update_calib_vals
        """
        try:
            self.zf = float(lens_size)
            self.zf /= 1000.0  # mm to m
        except:
            self.zf = None
            self.set_qlabel_text("a_zf", "N/A")
            self.logger.exception("something's gone awry!")
        else:
            self.set_qlabel_text("a_zf", "{:0.2f}".format(100*self.zf))
            self.update_calib_vals()

    def update_cropbox(self, checked=False):
        """
        Name:     Prida.update_cropbox
        Inputs:   [optional] bool, whether checkbox is checked (checked)
        Outputs:  None.
        Features: Handles operations when the Image Crop checkbox is clicked;
                  warns when clicked and not editing.
        Depends:  - mayday
                  - reset_cropbox
        """
        if checked and self.editing:
            # We're okay to set the crop values.
            self.view.c_crop_top.setFocus()
        elif checked and not self.editing:
            # Warn against this action during imaging or import
            self.mayday(
                "Please create the new session before setting crop values.")
            self.view.cropBox.setChecked(False)
            self.view.c_pid.setFocus()
        else:
            self.reset_cropbox()

    def update_crop_spinbox(self, val=None):
        """
        Name:     Prida.update_crop_spinbox
        Inputs:   [optional] int, spin box value (val)
        Outputs:  None.
        Features: Applies uniform cropping to preserve the center of image
                  required for calibration; updates/limits spin box values
                  accordingly
        """
        # Current spinbox values:
        tval = self.view.c_crop_top.value()
        bval = self.view.c_crop_bottom.value()
        lval = self.view.c_crop_left.value()
        rval = self.view.c_crop_right.value()

        # Apply uniform cropping based on the current spinbox being updated:
        if self.view.c_crop_left.hasFocus():
            rval = 100-lval
            self.view.c_crop_right.setValue(rval)
        elif self.view.c_crop_right.hasFocus():
            lval = 100-rval
            self.view.c_crop_left.setValue(lval)
        elif self.view.c_crop_top.hasFocus():
            bval = 100-tval
            self.view.c_crop_bottom.setValue(bval)
        elif self.view.c_crop_bottom.hasFocus():
            tval = 100-bval
            self.view.c_crop_top.setValue(tval)

        # Update spinbox limits:
        self.view.c_crop_top.setRange(0, bval)
        self.view.c_crop_bottom.setRange(tval, 100)
        self.view.c_crop_left.setRange(0, rval)
        self.view.c_crop_right.setRange(lval, 100)

    def update_data(self, my_list):
        """
        Name:     Prida.update_data
        Inputs:   list, data list (my_list)
        Outputs:  None.
        Features: Catches signal from data, updates data list with current
                  PIDs and their associated attributes
        Depends:  back_to_sheet
        """
        self.logger.info("caught data listed signal")
        self.data_list = []
        self.data_list = my_list
        self.logger.debug("updating sheet model")
        self.back_to_sheet()

    def update_dict_auto_model(self):
        """
        Name:     Prida.update_dict_auto_model
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QStringListModel for the dictionary file's
                  contents
        """
        self.plants_auto_model.setStringList(list(self.usda_plants.keys()))
        self.dict_auto_model.setStringList(
            list(set(self.usda_plants.values())))

    def update_exclude_combo(self):
        """
        Name:     Prida.update_exclude_combo
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QComboBox with exclude options
        """
        self.logger.debug("setting the QComboBox values for c_exclude")
        my_options = ["False",
                      "True"]
        self.view.c_exclude.insertItems(0, my_options)

    def update_file_browse_sheet(self):
        """
        Name:     Prida.update_file_browse_sheet
        Inputs:   None.
        Outputs:  None.
        Features: Reloads the rows in the file_sheet_model
        Depends:  - load_file_browse_sheet_headers
                  - resize_file_browse_sheet
        """
        self.logger.debug("updating sheet")
        self.file_sheet_model.clear()
        self.load_file_browse_sheet_headers()
        for f_name in self.search_files:
            f_path = self.search_files[f_name]
            f_list = [QStandardItem(f_name), QStandardItem(f_path)]
            self.file_sheet_model.appendRow(f_list)
        self.resize_file_browse_sheet()

    def update_file_search_sheet(self, search=''):
        """
        Name:     Prida.update_file_search_sheet
        Inputs:   [optional] str, search string (search)
        Outputs:  None.
        Features: Reloads the rows in the search_sheet_model, filtered for
                  search terms
        Depends:  - search_data (attr)
                  - load_file_search_sheet_headers (func)
                  - resize_search_sheet (func)
                  - search (func)
        """
        self.logger.debug("updating sheet")
        self.search_sheet_model.clear()
        self.load_file_search_sheet_headers()
        terms = search.lower().split()
        for i in self.search_data:
            pid_meta = []
            for j in self.search_data[i]:
                pid_meta.append(j.lower())
            if self.search(pid_meta, terms):
                d_list = []
                for k in self.search_data[i]:
                    d_list.append(QStandardItem(k))
                self.search_sheet_model.appendRow(d_list)
        self.resize_search_sheet()

    def update_input_field(self, my_vals):
        """
        Name:     Prida.update_input_field
        Inputs:   tuple, Qt field name, type, and value (my_vals)
        Outputs:  None
        Features: Catches data's set field signal and sets the input field with
                  the appropriate info
        Depends:  set_input_field
        """
        (f_name, f_type, f_value) = my_vals
        self.set_input_field(f_name, f_type, f_value)

    def update_menus(self):
        """
        Name:     Prida.update_menus
        Inputs:   None.
        Outputs:  None.
        Features: Enables/disables menu actions based on the HDF5 file status
        TODO:     _ toggle enabled menu actions based on the current view
                    (e.g., when editing, disable take preview photo)
        """
        if self.data.is_open:
            # HDF5 file is open,
            # allow File->save, File->close, Help->Check actions
            # disallow File->new, File->open, Help->reconfigure
            self.fsaveAct.setEnabled(True)
            self.fcloseAct.setEnabled(True)
            self.fnewAct.setEnabled(False)
            self.fopenAct.setEnabled(False)
            self.hcheckAct.setEnabled(True)
            self.hconfigAct.setEnabled(False)
        else:
            # HDF5 file closed.
            # disallow File->save, File->close and Help->Check
            # allow File->new and File->open and Help->reconfigure
            self.fsaveAct.setEnabled(False)
            self.fcloseAct.setEnabled(False)
            self.fnewAct.setEnabled(True)
            self.fopenAct.setEnabled(True)
            self.hcheckAct.setEnabled(False)
            self.hconfigAct.setEnabled(True)

    def update_orient_combo(self):
        """
        Name:     Prida.update_orient_combo
        Inputs:   None.
        Outputs:  None.
        Features: Updates the QComboBox with orientation options
        """
        self.logger.debug("setting the QComboBox values for c_img_orient")
        my_options = ["Original",
                      "Rotated Counter-Clockwise",
                      "Rotated Clockwise"]
        self.view.c_img_orient.insertItems(0, my_options)

    def update_pid_auto_model(self):
        """
        Name:     Prida.update_pid_auto_model
        Inputs:   None
        Outputs:  None
        Features: Updates the QStringListModel with HDF5 file's current PIDs
        """
        self.logger.debug("assigning %d PIDs to list", len(self.data.pid_list))
        self.pid_auto_model.setStringList(self.data.pid_list)

    def update_progress(self, prog_val, t, show_all=False):
        """
        Name:     Prida.update_progress
        Inputs:   - int, progress value (prog_val)
                  - dict, imaging output arguments (t)
                  - bool, show all preview images (show_all)
        Outputs:  None.
        Features: Threading slot.
                  Updates the progress bar during imaging, creates a preview
                  of the first image taken
        Depends:  imopen
        """
        self.logger.debug("updating progress bar")
        self.view.c_progress.setValue(prog_val)

        self.logger.debug("gathering image metadata")
        is_path = t.get('ready', False)
        file_path = t.get('path', None)

        if show_all:
            self.preview = False

        self.logger.debug("setting image preview")
        if is_path and not self.preview:
            try:
                img = imopen(file_path)
            except:
                self.logger.exception("Failed to read file %s", file_path)
                self.preview = False
            else:
                self.logger.debug("image preview set")
                self.view.c_preview.setBaseImage(img)
                self.preview = True
        else:
            self.logger.debug("skipping image, preview already set")

    def update_search_additions(self, search_dict, cur_idx, max_idx):
        """
        Name:     Prida.update_search_additions
        Inputs:   - dict, search file dictionary (search_dict)
                  - int, current index value (cur_idx)
                  - int, maximum index value (max_idx)
        Outputs:  None.
        Features: Catches signal from data and updates file search and file
                  browse sheets
        Depends:  - update_file_browse_sheet
                  - update_file_search_sheet
        """
        self.logger.debug("updating search data")
        for key in search_dict.keys():
            value = search_dict.get(key, "N/A")
            self.search_data[key] = value

        self.logger.debug("clearing FILE_SEARCH search bar")
        self.view.f_search.clear()

        self.logger.debug("updating file search and file browse sheets")
        self.update_file_search_sheet()
        self.update_file_browse_sheet()
        if cur_idx == max_idx:
            self.stop_spinner(FILE_SEARCH)

    def update_search_removals(self, key_list):
        """
        Name:     Prida.update_search_removals
        Inputs:   list, search file PID keys (key_list)
        Outputs:  None.
        Features: Catches signal from data and updates file search and file
                  browse sheets
        Depends:  - search_data (attr)
                  - update_file_browse_sheet (func)
                  - update_file_search_sheet (func)
        """
        self.logger.debug("removing items from file search sheet")
        for key in key_list:
            if key in self.search_data:
                del self.search_data[key]

        self.view.f_search.clear()
        self.update_file_browse_sheet()
        self.update_file_search_sheet()

    def update_session_sheet_model(self, selection):
        """
        Name:     Prida.update_session_sheet_model
        Inputs:   obj (selection)
        Outputs:  None.
        Features: Parses user's sheet selection; clears session and image
                  sheets if PID is selected; signals for session attributes and
                  thumbnails if session is selected
        Depends:  - clear_session_sheet_model
                  - resize_session_sheet
                  - start_spinner
        TODO:     _ create a function that returns a bool for selected_pid or
                    selected_session
                  _ use get_sheet_model_selection function for path
                  _ add a signal to check cropping extents are okay for this
                    selection (e.g., send self.crop_extents and raise a
                    warning if they do not work; give option to cancel cropping
                    or selection)
        """
        if selection.indexes():
            self.logger.debug("gathering user selection")
            my_session = self.sheet_model.itemFromIndex(selection.indexes()[0])

            if (my_session.parent() is not None):
                # Session selection
                self.logger.debug("session selected")
                pid = my_session.parent().text()
                session = my_session.text()
                s_path = "/%s/%s" % (pid, session)

                # Signal data to set session attributes:
                self.logger.info("signaling data for session attributes")
                self.getSessionAttrs.emit(s_path)

                self.logger.info("signaling data for thumbnails")
                self.img_sheet_model.clear()
                self.img_list = []
                self.start_spinner("Loading session ...")
                self.getThumbs.emit(s_path, VIEWER)

            else:
                # PID selection
                self.logger.debug("PID selected")
                self.logger.debug("clearing session and image sheets")
                self.clear_session_sheet_model()
                self.img_sheet_model.clear()
                self.img_list = []
        else:
            # You made a de-selection (ctrl+click):
            self.logger.debug("encountered de-selection")
            self.logger.debug("clearing session and image sheets")
            self.clear_session_sheet_model()
            self.img_sheet_model.clear()
            self.img_list = []
        self.logger.debug("resizing session sheet")
        self.resize_session_sheet()

    def update_sheet_model(self, search=''):
        """
        Name:     Prida.update_sheet_model
        Inputs:   [optional] str (search)
        Features: Displays all PIDs in the sheet or the PIDs that match the
                  search terms if search terms are given
        Depends:  - load_sheet_headers
                  - resize_sheet
                  - search
                  - update_pid_auto_model
        """
        self.logger.debug("updating the sheet model")
        self.update_pid_auto_model()
        self.sheet_model.clear()
        self.load_sheet_headers()
        self.logger.debug("parsing search terms")
        terms = search.lower().split()
        for d in self.data_list:
            pid_meta = []
            for key in d:
                if key != "sessions":
                    pid_meta.append(d[key].lower())
            if self.search(pid_meta, terms):
                pid_item = QStandardItem(d["pid"])

                # Create session rows under each PID:
                for session in d["sessions"]:
                    pid_item.appendRow(QStandardItem(session))

                # Fill the PID row with its ordered attributes:
                pid_list = [pid_item, ]
                for i in sorted(list(self.data.pid_attrs.keys())):
                    f_key = self.data.pid_attrs[i]["key"]
                    my_attr = d[f_key]
                    pid_list.append(QStandardItem(my_attr))
                self.sheet_model.appendRow(pid_list)
        self.resize_sheet()

    def update_tag_list(self, search=''):
        """
        Name:     Prida.update_tag_list
        Inputs:   [optional] str (search)
        Features: Displays all EXIF tags that match the search terms
        """
        self.tag_ui.listWidget.clear()

        terms = search.lower().split()
        nrows = len(self.tag_dict)
        if nrows > 0:
            tag_row = 0
            for tag in self.tag_dict:
                s_list = [str(tag).lower(), ]
                if self.search(s_list, terms):
                    val = self.tag_dict[tag]
                    row_str = ": ".join([tag, val])
                    self.tag_ui.listWidget.addItem(QListWidgetItem(row_str))
                    tag_row += 1
        else:
            self.tag_ui.listWidget.addItem(QListWidgetItem("No tag info."))

    def version_checker(self, config_filepath):
        """
        Name:     Prida.version_checker
        Inputs:   str, configuration file path (config_filepath)
        Outputs:  bool, use defaults flag
        Features: Checks the configuration file version against current
                  software version; if matched, return False, else return True
        """
        # Initialize error string and configuration file version:
        use_defaults = False
        err_str = None
        conf_ver = None

        if config_filepath is not None:
            try:
                my_user = os.path.expanduser('~')
                conf_file = os.path.join(my_user, 'Prida', 'prida.config')
                if os.path.isfile(conf_file):
                    with open(conf_file, 'r') as my_config_file:
                        for line in my_config_file:
                            if 'PRIDA_VERSION' in line:
                                words = line.split()
                                if len(words) == 3:
                                    conf_ver = eval(words[2])
                                else:
                                    err_str = (
                                        'Configuration formatting error!\n'
                                        'Please check your configuration file '
                                        'and restart the program. '
                                        'Assuming program defaults.')
                            else:
                                err_str = (
                                    'Configuration version missing!\n'
                                    'Please check your configuration file and '
                                    'restart the program. '
                                    'Assuming program defaults.')
                else:
                    err_str = (
                        'Configuration file missing!\n'
                        'Please make sure your configuration file is located '
                        'in your local Prida directory. '
                        'Assuming program defaults.')
            except:
                err_str = (
                    'Configuration file parsing error!\n'
                    'Please check your configuration file is in the proper '
                    'format and is located in your local Prida directory. '
                    'Assuming program defaults.')
                self.logger.exception(err_str)
            if conf_ver is not None:
                if conf_ver != __version__:
                    err_str = (
                        'Configuration file version mis-match!\n'
                        'Please update your configuration file version  (%s) '
                        'to match your software version (%s). '
                        'Assuming program defaults.' % (conf_ver, __version__))
                else:
                    err_str = None
        else:
            err_str = (
                'Configuration file parsing error!\n'
                'Please check that your configuration file is in the proper '
                'format and is located in your local Prida directory. '
                'Assuming program defaults.')

        if err_str:
            use_defaults = True
            self.mayday(err_str)

        return use_defaults
