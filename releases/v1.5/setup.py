#!/usr/bin/python
#
# setup.py
#
# VERSION: 1.5.0
#
# LAST EDIT: 2017-06-20
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################

# This script checks for software required dependencies, compiles Qt UI files
# to Python modules, and launches Prida.

###############################################################################
# REQUIRED MODULES:
###############################################################################
import os
import re
import shutil
import subprocess
import sys


###############################################################################
# FUNCTIONS:
###############################################################################
def check_exifread(tot=49):
    """
    Name:     check_exifread
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if exifread package is available
    """
    passed = False
    err = None
    a = "Checking for exifread"
    try:
        import exifread
    except ImportError:
        err = ("Python package exifread is not installed! "
               "Please install exifread (e.g., pip install exifread).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(exifread.__version__)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_gphoto2(tot=49):
    """
    Name:     check_gphoto2
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if gphot2 package is available
    """
    passed = False
    err = None
    a = "Checking for gphoto2"
    try:
        import gphoto2
    except ImportError:
        if os.name == 'nt':
            err = ("Python package gphoto2 is not available on this "
                   "computer. 2D and 3D imaging modes may not be used.")
            print("{} {} N/A".format(a, "."*(tot - len(a))))
        else:
            err = ("Python package gphoto2 is not installed. "
                   "This package is required for (2D/3D) hardware mode. "
                   "For hardware mode, please install gphoto2 and libgphoto2 "
                   "(e.g., pip install gphoto2).")
            print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(gphoto2.__version__)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_h5py(tot=49):
    """
    Name:     check_h5py
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if h5py package is available
    """
    passed = False
    err = None
    a = "Checking for h5py"
    try:
        import h5py
    except ImportError:
        err = ("Python package h5py is not installed! "
               "Please install h5py (e.g., pip install h5py).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(h5py.__version__)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_numpy(tot=49):
    """
    Name:     check_numpy
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if numpy package is available
    """
    passed = False
    err = None
    a = "Checking for numpy"  # 18 chars
    try:
        import numpy
    except ImportError:
        err = ("Python package numpy is not installed! "
               "Please install numpy (e.g., pip install numpy).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(numpy.__version__)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_pil(tot=49):
    """
    Name:     check_pil
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if PIL package is available
    """
    passed = False
    err = None
    a = "Checking for PIL"
    try:
        import PIL.Image
    except ImportError:
        err = ("Python package PIL is not installed! "
               "Please install PIL (e.g., pip install Pillow).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(PIL.Image.VERSION)
        print("{} {} {} OK ".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_prida(tot=49):
    """
    Name:     check_prida
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
              - str, Prida version (ver)
    Features: Checks if prida package is available
    """
    passed = False
    err = None
    ver = None
    a = "Checking for Prida"
    try:
        import prida
    except ImportError:
        err = ("Failed to import prida module! "
               "Please check that this setup.py file is "
               "in the correct directory.")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        ver = prida.__version__
        b = "(found v.{})".format(ver)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err, ver)


def check_pyqt5(tot=49):
    """
    Name:     check_pyqt5
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if PyQt5 package is available
    """
    passed = False
    err = None
    a = "Checking for PyQt5"
    try:
        import PyQt5.QtCore
    except ImportError:
        err = ("Python package PyQt5 is not installed! "
               "Please install PyQt (e.g., http://pyqt.sourceforge.net).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(PyQt5.QtCore.QT_VERSION_STR)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_python3(tot=49):
    """
    Name:     check_python3
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if the main version of Python is version 3
    """
    passed = False
    err = None
    a = "Checking for Python 3"
    if sys.version_info[0] == 3:
        passed = True
        b = "(running v.{}.{}.{})".format(
            sys.version_info[0], sys.version_info[1], sys.version_info[2])
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))
    else:
        err = ("Python 3 not running! "
               "Python 3 is required for running this software.")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))

    return (passed, err)


def check_pyuic():
    """Checks if PyUIC is installed and working"""
    try:
        if sys.version_info >= (3, 5):
            if os.name == 'nt':
                return subprocess.run(
                    ['pyuic5.bat', '-h'], stdout=subprocess.PIPE).returncode
            else:
                return subprocess.run(
                    ['pyuic5', '-h'], stdout=subprocess.PIPE).returncode
        else:
            if os.name == 'nt':
                return subprocess.call(
                    ['pyuic5.bat', '-h'], stdout=subprocess.PIPE)
            else:
                return subprocess.call(
                    ['pyuic5', '-h'], stdout=subprocess.PIPE)
    except:
        return -1


def check_rawpy(tot=49):
    """
    Name:     check_rawpy
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if the rawpy package is available
    """
    passed = False
    err = None
    a = "Checking for rawpy"
    try:
        import rawpy
    except ImportError:
        err = ("Python package rawpy is not installed! "
               "Please install rawpy (e.g., pip install rawpy).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(rawpy.__version__)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_scipy(tot=49):
    """
    Name:     check_scipy
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if the scipy package is available
    """
    passed = False
    err = None
    a = "Checking for scipy"  # 18 chars
    try:
        import scipy
        import scipy.misc
        import scipy.optimize
    except ImportError:
        err = ("Python package scipy is not installed! "
               "This package is required for analysis mode. "
               "For analysis mode, please install scipy "
               "(e.g., pip install scipy).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(scipy.__version__)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def find_ui_files_r(my_dir):
    """
    Name:     find_ui_files_r
    Inputs:   str, directory path (my_dir)
    Outputs:  list, absolute paths to UI files
    Features: Returns list of UI files from a recursive directory search
    """
    ui_file_list = []
    for root, subdirs, files in os.walk(my_dir):
        for my_file in files:
            if re.match("^.*ui$", my_file):
                ui_file_list.append(os.path.join(root, my_file))
    return ui_file_list


def fix_mainwindow(py_file, fline=49):
    """
    Name:     fix_mainwindow
    Inputs:   - str, Python file to fix (py_file)
              - [optional] int, character fill line length (fline)
    Outputs:  tuple, okay boolean and error string
    Features: Fixes the Python file created by pyuic for the relative import
              made in mainwindow.ui in Prida v1.3+
    """
    okay = True
    err_msg = ""
    msg = "> fixing mainwindow.py"
    bak_file = py_file + ".bak"
    if os.path.isfile(py_file):
        try:
            shutil.copyfile(py_file, bak_file)
        except:
            okay = False
            err_msg = "Failed to create copy of %s!" % (
                os.path.basename(py_file))
            print("{} {} FAILED".format(msg, "."*(fline - len(msg))))
        else:
            try:
                f = open(py_file, 'r', encoding="utf-8")
            except:
                okay = False
                err_msg = "Failed to read %s!" % os.path.basename(py_file)
                print("{} {} FAILED".format(msg, "."*(fline - len(msg))))
            else:
                data = f.readlines()
                f.close()

                try:
                    g = open(py_file, 'w', encoding="utf-8")
                except:
                    okay = False
                    err_msg = "Failed to write to %s!" % (
                        os.path.basename(py_file))
                    print("{} {} FAILED".format(msg, "."*(fline - len(msg))))
                else:
                    for line in data:
                        line = re.sub("custom", ".custom", line)
                        g.write(line)
                    g.close()
                    print("{} {} OK".format(msg, "."*(fline - len(msg))))
    else:
        okay = False
        err_msg = "%s does not exist!" % py_file
        print("{} {} FAILED".format(msg, "."*(fline - len(msg))))

    return (okay, err_msg)


def perform_checks(quiet=False, fline=49):
    """
    Name:     perform_checks
    Inputs:   - [optional] bool, do not prompt user for input (quiet)
              - [optional] int, character fill line length (fline)
    Outputs:  - bool, okay to procees flag
              - list, checks passed versus checks performed
              - list, error messages
    Features: Runs a series of checks for Prida software development
    """
    passed = [0, 0]
    to_proceed = True
    err_msgs = []
    conf_ver = None
    prida_ver = None

    # CHECK: Python 3 requirement
    c_py3, e_py3 = check_python3(fline)
    passed[1] += 1
    if c_py3:
        passed[0] += 1
    else:
        to_proceed = False
    if e_py3 is not None:
        err_msgs.append(e_py3)

    # CHECK: pyuic for compiling Qt UI files:
    c_pyuic = "Checking for pyuic"  # 18 chars
    if check_pyuic() == 0:
        passed[0] += 1
        print("{} {} OK".format(c_pyuic, "."*(fline - len(c_pyuic))))
    else:
        to_proceed = False
        err_msgs.append(
            "pyuic is not installed! "
            "Please install pyuic5 (e.g. qt5-dev-tools) to compile UI files.")
        print("{} {} FAILED".format(c_pyuic, "."*(fline - len(c_pyuic))))
    passed[1] += 1

    # CHECK: Python PyQt5
    c_pyqt, e_pyqt = check_pyqt5(fline)
    passed[1] += 1
    if c_pyqt:
        passed[0] += 1
    else:
        to_proceed = False
    if e_pyqt is not None:
        err_msgs.append(e_pyqt)

    # CHECK: Python numpy
    c_numpy, e_numpy = check_numpy(fline)
    passed[1] += 1
    if c_numpy:
        passed[0] += 1
    else:
        to_proceed = False
    if e_numpy is not None:
        err_msgs.append(e_numpy)

    # CHECK: Python scipy
    c_scipy, e_scipy = check_scipy(fline)
    passed[1] += 1
    if c_scipy:
        passed[0] += 1
    if e_numpy is not None:
        err_msgs.append(e_scipy)

    # CHECK: Python Image Library (PIL)
    c_pil, e_pil = check_pil(fline)
    passed[1] += 1
    if c_pil:
        passed[0] += 1
    else:
        to_proceed = False
    if e_pil is not None:
        err_msgs.append(e_pil)

    # CHECK: Python HDF5 API (h5py)
    c_h5py, e_h5py = check_h5py(fline)
    passed[1] += 1
    if c_h5py:
        passed[0] += 1
    else:
        to_proceed = False
    if e_h5py is not None:
        err_msgs.append(e_h5py)

    # CHECK: Python rawpy package
    c_rawpy, e_rawpy = check_rawpy(fline)
    passed[1] += 1
    if c_rawpy:
        passed[0] += 1
    else:
        to_proceed = False
    if e_rawpy is not None:
        err_msgs.append(e_rawpy)

    # CHECK: Python exifread package
    c_exif, e_exif = check_exifread(fline)
    passed[1] += 1
    if c_exif:
        passed[0] += 1
    else:
        to_proceed = False
    if e_exif is not None:
        err_msgs.append(e_exif)

    # CHECK: gphoto2
    c_gphoto, e_gphoto = check_gphoto2(fline)
    passed[1] += 1
    if c_gphoto:
        passed[0] += 1
    if e_gphoto is not None:
        err_msgs.append(e_gphoto)

    # CHECK: Prida module
    c_prida, e_prida, prida_ver = check_prida(fline)
    passed[1] += 1
    if c_prida:
        passed[0] += 1
    else:
        to_proceed = False
    if e_prida is not None:
        err_msgs.append(e_prida)

    # CHECK: main.py existence
    ck10 = "Checking for main.py"
    if os.path.isfile("main.py"):
        passed[0] += 1
        print("{} {} OK".format(ck10, "."*(fline - len(ck10))))
    else:
        to_proceed = False
        err_msgs.append(
            "Prida main.py is missing! "
            "Please check that this setup.py file is "
            "in the correct directory.")
        print("{} {} FAILED".format(ck10, "."*(fline - len(ck10))))
    passed[1] += 1

    # CHECK: UI files:
    ck11 = "Checking for UI files"
    ui_list = find_ui_files_r(os.path.abspath("."))
    if len(ui_list) != 6:
        to_proceed = False
        err_msgs.append(
            "Prida UI files missing! "
            "Please check that this setup.py file is "
            "in the correct directory.")
        print("{} {} FAILED".format(ck11, "."*(fline - len(ck11))))
    else:
        passed[0] += 1
        print("{} {} OK".format(ck11, "."*(fline - len(ck11))))
        if to_proceed:
            print("Compiling UI files ...")
            okay, num_passed, messages = ui2py(ui_list)
            passed[0] += num_passed[0]
            passed[1] += num_passed[1]
            err_msgs += messages
            if not okay:
                to_proceed = False
        else:
            print("*skipping UI compilation")
    passed[1] += 1

    # CHECK: Local Prida directory
    ck8 = "Checking for Prida directory"  # 28 chars
    prida_dir = os.path.join(os.path.expanduser("~"), "Prida")
    if os.path.isdir(prida_dir):
        passed[0] += 1
        print("{} {} OK".format(ck8, "."*(fline - len(ck8))))

        # CHECK: configuraiton file:
        ck81 = "Checking for config file"
        config_file = os.path.join(prida_dir, "prida.config")
        if os.path.isfile(config_file):
            # CHECK: configuration version:
            try:
                with open(config_file, 'r') as fh:
                    for line in fh:
                        if 'PRIDA_VERSION' in line:
                            words = line.split()
                            if len(words) == 3:
                                conf_ver = eval(words[2])
                                assert isinstance(conf_ver, str)
            except:
                err_msgs.append(
                    "Configuration file is unreadable. "
                    "Please click 'Replace' from the Main Menu "
                    "to create a new configuration file.")
            else:
                passed[0] += 1
                if conf_ver is not None:
                    ok81 = "(found v.{})".format(conf_ver)
                    print("{} {} {} OK".format(
                        ck81, ok81, "."*(fline - len(ck81) - len(ok81) - 1)))

                    # CHECK: prida v. config versions:
                    ck82 = "Matching to Prida v.{}".format(prida_ver)
                    if prida_ver == conf_ver:
                        passed[0] += 1
                        print("{} {} OK".format(ck82, "."*(fline - len(ck82))))
                    else:
                        print("{} {} FAILED".format(
                            ck82, "."*(fline - len(ck82))))
                        err_msgs.append(
                            "Configuration file does not match "
                            "the current software! "
                            "Please click 'Replace' from the Main Menu "
                            "to create a new configuration file.")
                    passed[1] += 1
                else:
                    print("{} {} OK".format(ck81, "."*(fline - len(ck81))))
        else:
            print("{} {} FAILED".format(ck81, "."*(fline - len(ck81))))
            err_msgs.append(
                "No configuration file found! "
                "Please click 'Replace' from the Main Menu "
                "to create a new configuration file.")
        passed[1] += 1
    else:
        print("{} {} FAILED".format(ck8, "."*(fline - len(ck8))))

        # Only ask if it is still possible to run the program:
        if to_proceed and not quiet:
            try:
                ans = raw_input("*create local Prida directory now? (Y/N): ")
            except NameError:
                try:
                    ans = input("*create local Prida directory now? (Y/N): ")
                except EOFError:
                    ans = ''
            finally:
                assert isinstance(ans, str)
            if ans.lower() == 'y':
                msg8 = "Creating Prida directory"  # 24 chars
                try:
                    from prida.resources import create_local_prida
                except ImportError:
                    to_proceed = False
                    err_msgs.append(
                        "Failed to import prida module! "
                        "Please check that this setup.py file is "
                        "in the correct directory.")
                    print("{} {} FAILED".format(msg8, "."*(fline - len(msg8))))
                else:
                    rcode = create_local_prida(prida_dir)
                    if rcode != 0:
                        # Still okay to proceed
                        err_msgs.append(
                            "Unable to create a local Prida directory!")
                        print("{} {} FAILED".format(
                            msg8, "."*(fline - len(msg8))))
                    else:
                        passed[0] += 1
                        print("{} {} OK".format(msg8, "."*(fline - len(msg8))))
            else:
                print("*skipping local Prida directory")
                err_msgs.append("Local Prida directory not found.")
        else:
            print("*skipping local Prida directory")
            err_msgs.append("Local Prida directory not found.")
    passed[1] += 1

    return (to_proceed, passed, err_msgs)


def print_messages(msg_list, char_count):
    """
    Name:     print_messages
    Inputs:   - list, error messages (msg_list)
              - int, character fill line length (char_count)
    Outputs:  None.
    Features: Prints formatted error messages
    """
    for i in range(len(msg_list)):
        # Break each error message into individual words:
        msg = msg_list[i].split(" ")

        # Split the error message into separate lines based on each
        # line's length
        out_lines = []
        line_num = 0
        out_lines.append("")
        for j in range(len(msg)):
            out_lines[line_num] += msg[j]
            count = len(out_lines[line_num])
            if count > char_count - 7:
                line_num += 1
                out_lines.append("")
            out_lines[line_num] += " "
        for k in range(len(out_lines)):
            if not out_lines[k].isspace():
                if k == 0:
                    print("{0:2}. {1:}".format(i + 1, out_lines[k]))
                else:
                    print("   {}".format(out_lines[k]))
    print("{}".format('-'*char_count))


def ui2py(my_files, fline=49):
    """
    Name:     ui2py
    Inputs:   - list, list of UI files (my_files)
              - [optional] int, character fill line length (fline)
    Outputs:  tuple, to procees boolean, list of passed versus performed
              operations, and list of error messages
    Features: Runs the Qt development tool on given list of UI files
    """
    passed = [0, 0]
    to_proceed = True
    err_msgs = []
    py35 = sys.version_info >= (3, 5)

    if check_pyuic() == 0:
        if len(my_files) > 0:
            for my_file in my_files:
                if os.name == "nt":
                    cmd_arg1 = "pyuic5.bat"
                else:
                    cmd_arg1 = "pyuic5"
                cmd_arg2 = "-o"
                cmd_arg3 = "%s.py" % (os.path.splitext(my_file)[0])
                cmd_arg4 = my_file
                msg = "> %s" % os.path.basename(my_file)
                try:
                    if py35:
                        subprocess.run(
                            [cmd_arg1, cmd_arg2, cmd_arg3, cmd_arg4],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            check=True)
                    else:
                        subprocess.check_call(
                            [cmd_arg1, cmd_arg2, cmd_arg3, cmd_arg4],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
                except:
                    to_proceed = False
                    err_msgs.append(
                        "pyuic failed to compile file %s "
                        "from directory %s" % (os.path.basename(my_file),
                                               os.path.dirname(my_file)))
                    print("{} {} FAILED".format(msg, "."*(fline - len(msg))))
                else:
                    passed[0] += 1
                    print("{} {} OK".format(msg, "."*(fline - len(msg))))
                    if re.match(".*mainwindow.*$", my_file):
                        passed[1] += 1
                        is_okay, err_msg = fix_mainwindow(cmd_arg3)
                        if not is_okay:
                            err_msgs.append(err_msg)
                        else:
                            passed[0] += 1
                passed[1] += 1
        else:
            passed[1] += 1
            to_proceed = False
            err_msgs.append("No UI files found!")
            print(" {} FAILED".format("."*fline))
    else:
        passed[1] += 1
        to_proceed = False
        err_msgs.append("pyuic not installed!")
        print(" {} FAILED".format("."*fline))

    return (to_proceed, passed, err_msgs)

###############################################################################
# MAIN:
###############################################################################
if __name__ == '__main__':
    # Define the main program module:
    mainfile = "main.py"
    line_len = 57

    greeting = " Prida setup "
    ending = " end setup "
    gdots = int(0.5*(line_len - len(greeting)))
    edots = int(0.5*(line_len - len(ending)))
    print("{}{}{}".format("-"*gdots, greeting, "-"*gdots))

    # Check command-line arguments
    num_args = len(sys.argv)
    if "-quiet" in sys.argv:
        run_quiet = True
    else:
        run_quiet = False

    # Perform system checks:
    to_proceed, num_passed, messages = perform_checks(run_quiet)

    if to_proceed:
        print("{}{}{}".format('-'*edots, ending, '-'*edots))
        print("Passed {}/{}".format(num_passed[0], num_passed[1]))
        print("")

        if num_passed[0] < num_passed[1]:
            print("Encountered the following warnings:")
            print_messages(messages, line_len)
            print("")

        # Allow user to pass "-y" argument to setup.py to run if successful:
        if not run_quiet:
            if "-y" in sys.argv:
                ans = "y"
            elif "-n" in sys.argv:
                ans = 'n'
            else:
                try:
                    ans = raw_input("Start application? (Y/N): ")
                except NameError:
                    try:
                        ans = input("Start application? (Y/N): ")
                    except EOFError:
                        ans = ''
                finally:
                    assert isinstance(ans, str)

            if ans.lower() == "y":
                if os.path.isfile(mainfile):
                    with open(mainfile) as f:
                        code = compile(f.read(), mainfile, 'exec')
                        exec(code)
                else:
                    print("... FAILED! %s not found." % mainfile)
    else:
        print("{}{}{}".format('-'*edots, ending, '-'*edots))
        print("Passed {}/{}".format(num_passed[0], num_passed[1]))
        print("")
        print("Encountered the following errors:")
        print_messages(messages, line_len)
