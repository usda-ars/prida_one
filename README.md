# Plant Root Imaging and Data Acquisition (PRIDA)
-------------------------------------------------

The PRIDA software enables researchers to collect digital images, along with all the relevant experimental details, of a range of hydroponically grown agricultural crop roots for 2D and 3D trait analysis.
The multi-platform software has been released with considerations for both high throughput (e.g., 3D imaging of a single root system in under ten minutes) and high portability (e.g., support for the Raspberry Pi computer).
The software features unified data collection and preservation (i.e., through HDF5 project files) and management and exploration (i.e., through a Qt5 graphical user interface) for continued trait and genetics analysis of root system architecture.
The design makes data acquisition efficient and includes features that address the needs of researchers and technicians, such as reduced imaging time, semi-automated camera calibration with uncertainty characterization, and safe storage of the critical experimental data.

# Repository Details
--------------------

* STATUS: Active
* LATEST RELEASE: v.1.5
* LAST UPDATED: 2017-06-28
* LICENSE: Public Domain (except where otherwise noted)
* TEAM: USDA-ARS
* REPO: https://bitbucket.org/usda-ars/prida_one/src
* WIKI: https://bitbucket.org/usda-ars/prida_one/wiki/Home
* DEVELOPER: https://bitbucket.org/usda-ars/prida_one/wiki/developer
* URL: http://www.plantmineralnutrition.net/software/prida/

# Repository Structure
----------------------

## docs/
This directory holds the mkdocs (http://www.mkdocs.org) files for the PRIDA website.

## releases/
This directory holds the PRIDA software releases.

## utilities/
This directory holds miscellaneous utility scripts.

## working/
This directory contains the PRIDA source code currently under development.

## mkdocs.yml
The YAML data file for the mkdocs website located in the docs/ directory.
