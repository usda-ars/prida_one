#!/usr/bin/env python3
#
# main.py
#
# VERSION: 1.6.0-dev
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import atexit
import logging
import logging.handlers
import os
import sys

from prida import __version__
from prida.prida import Prida
from prida.email_organizer import PridaMail
from prida.global_conf import GlobalConf
from prida.custom_filter import CustomFilter
from prida.resources import create_local_prida
from prida.stream_to_logger import StreamToLogger
from prida.utilities import conf_parser


###############################################################################
# FUNCTIONS:
###############################################################################
def exit_log(file_handler, mail_handler, cred_file):
    """
    Name:     exit_log
    Inputs:   - RotatingFileHandler (file_handler)
              - PridaMail (mail_handler)
    Outputs:  None.
    Features: Roll over a RotatingFileHandler to a new log file and attempt to
              email all archived logs
    """
    # Archive log files (if existing)
    try:
        mail_handler.validate(cred_file)
        file_handler.doRollover()
        mail_handler.archive_log(log_path)
        logging.info('Rolling over to new log file')
    except:
        logging.exception('Email failed to send!')


###############################################################################
# MAIN:
###############################################################################
if __name__ == "__main__":
    # Create a root logger and temporary buffer logger
    root_logger = logging.getLogger()
    temp_logger = logging.getLogger('temp')
    temp_logger.setLevel(logging.DEBUG)

    # Set up a 66 kB memory handler for buffering initial logs
    mem_handler = logging.handlers.MemoryHandler(
        (66 * 1024),
        flushLevel=logging.CRITICAL,
        target=logging.NullHandler())
    temp_logger.addHandler(mem_handler)

    # Configure global variables; if config file doesn't exist, uses
    # GlobalConf default values
    temp_logger.debug('Instantiating global conf...')
    config = GlobalConf()
    temp_logger.debug('Configuring global variables...')
    version = config.version.lower()
    prida_dir = config.prida_dir
    conf_dir = config.conf_dir
    cred_dir = config.cred_dir
    log_dir = config.log_dir
    dict_dir = config.dict_dir   # NOTE potentially move to Prida?
    conf_filename = config.conf_filename
    cred_filename = config.cred_filename
    log_filename = config.log_filename
    dict_filename = config.dict_filename
    log_level = "logging.%s" % (config.log_level.upper())

    # Define the configuration file path:
    conf_filepath = os.path.join(conf_dir, conf_filename)

    # Checking if config file version number matches prida's version number
    my_dev = False
    conf_dev = False
    try:
        my_ver = __version__
        if '-dev' in my_ver:
            my_dev = True
            my_ver = my_ver[0:-4]
        my_ver = [int(x) for x in my_ver.split('.')]

        conf_ver = version
        if '-dev' in conf_ver:
            conf_dev = True
            conf_ver = conf_ver[0:-4]
        conf_ver = [int(x) for x in conf_ver.split('.')]
        if version == '0.0.0-dev':
            temp_logger.warning(
                'Version number missing from configuration file! '
                'Please check that your configuration file is up to date.')
        elif my_ver != conf_ver or my_dev != conf_dev:
            temp_logger.warning(
                'Configuration version mis-match! '
                'Please update your configuration file version to match '
                'the current version of this software.')
            temp_logger.debug('%s != %s' % (__version__, version))
        else:
            temp_logger.debug('Configuration file is up to date.')
    except:
        temp_logger.exception(
            'Failed to compare the configuration version number! '
            'Please check the configuration file for possible read and write. '
            'Ignoring config file.')
        conf_filepath = None

    # Set root logger level
    root_logger.setLevel(eval(log_level))

    # Check for pre-existance of logging directory
    temp_logger.debug('Checking for pre-existance of logging directory...')
    if os.path.isdir(log_dir) and os.access(log_dir, os.W_OK):
        # Logging directory exists and is writeable
        temp_logger.debug("Logging directory found at '%s'" % (log_dir))
        log_path = os.path.join(log_dir, log_filename)
        my_mail = PridaMail(log_dir)

        # Instantiating logging file handler
        temp_logger.debug('Instantiating logging file handler...')
        root_handler = logging.handlers.RotatingFileHandler(
            log_path, backupCount=9)
    elif os.path.isdir(prida_dir) and os.access(prida_dir, os.W_OK):
        # Reassign logging directory
        log_dir = prida_dir
        temp_logger.debug(
            "Writing to logging directory failed, writing to '%s'" % (log_dir))
        log_path = os.path.join(log_dir, log_filename)
        my_mail = PridaMail(log_dir)

        # Instantiating logging file handler
        temp_logger.debug('Instantiating logging file handler...')
        root_handler = logging.handlers.RotatingFileHandler(
            log_path, backupCount=9)
    else:
        # No writeable logging directory found, logging to stream
        temp_logger.info(
            'No writeable logging directory found, logging to stream.')
        root_handler = logging.StreamHandler()
        my_mail = PridaMail()

    # Estblishing record format
    rec_format = (
        "%(asctime)s.%(msecs)03d:%(levelname)s:%(name)s:%(funcName)s:"
        "%(message)s")
    formatter = logging.Formatter(rec_format, datefmt='%Y-%m-%d %H:%M:%S')
    root_handler.setFormatter(formatter)

    # Instantiating logging filter to check the records in the buffer loggers,
    # add to handler, and register rollover at exit
    temp_logger.debug('Instantiating logging filter...')
    root_filter = CustomFilter(eval(log_level))
    temp_logger.debug('Sending filter to root handler...')
    root_handler.addFilter(root_filter)
    if os.path.isdir(log_dir):
        temp_logger.debug('Registering file handler rollover...')
        cred_file = os.path.join(cred_dir, cred_filename)
        atexit.register(exit_log,
                        root_handler,
                        my_mail,
                        cred_file)

    # Send logging handler to root logger
    temp_logger.debug('Sending logging handler to root logger...')
    root_logger.addHandler(root_handler)

    # To preserve record order, collect log records from GlobalConf and
    # temp handler now if logging to file
    if os.path.isdir(log_dir):
        temp_logger.debug('Collecting buffered log records...')
        config.collect_log_records(root_handler)
        mem_handler.setTarget(root_handler)
        mem_handler.flush()

    # Create a standard output logger
    stdout_logger = logging.getLogger("STDOUT")
    sl = StreamToLogger(stdout_logger, logging.INFO)
    sys.stdout = sl

    # Create a standard error logger
    stderr_logger = logging.getLogger("STDERR")
    sl = StreamToLogger(stderr_logger, logging.ERROR)
    sys.stderr = sl

    # Attempt to create a local Prida directory:
    pridadir_code = create_local_prida(prida_dir, conf_filename, dict_filename)
    if pridadir_code == 0:
        # Success, delete any local logs and set new logging directory
        my_mail.set_log_dir(log_dir)
        root_handler.flush()
        root_logger.removeHandler(root_handler)

        log_path = os.path.join(log_dir, log_filename)
        fh = logging.handlers.RotatingFileHandler(log_path, backupCount=9)
        fh.setFormatter(formatter)
        root_logger.addHandler(fh)
        root_logger.info("Created local Prida directory")

        root_logger.debug("Collecting buffered log records...")
        config.collect_log_records(fh)
        mem_handler.setTarget(fh)
        mem_handler.flush()
        root_logger.debug("...finished recording buffered log records")
    elif pridadir_code == 9999:
        root_logger.info("Local Prida directory already exists")
    elif pridadir_code == 1:
        root_logger.warning("Failed to create local Prida directory")
        root_logger.debug('Collecting buffered log records...')
        config.collect_log_records(root_handler)
        mem_handler.setTarget(root_handler)
        mem_handler.flush()
        root_logger.debug("...finished recording buffered log records")
    elif pridadir_code == -1:
        root_logger.warning("Failed to create config and/or dictionary file")
        temp_logger.debug('Collecting buffered log records...')
        config.collect_log_records(root_handler)
        mem_handler.setTarget(root_handler)
        mem_handler.flush()
        root_logger.debug("...finished recording buffered log records")

    root_logger.debug("initializing Prida application")
    app = Prida(os.path.join(dict_dir, dict_filename),
                conf_parser,
                conf_filepath)
    sys.exit(app.exec_())
