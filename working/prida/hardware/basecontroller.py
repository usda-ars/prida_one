#!/usr/bin/python
#
# basecontroller.py
#
# VERSION: 1.6.0-dev
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#
###############################################################################
from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class BaseController(PRIDAPeripheral):
    """
    The base motor controller abstraction class, inherits PRIDAPeripheral,
    ensures the required properties and functions for all PRIDA controllers.

    Name:    BaseController
    History: Version 1.6.0-dev
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Name:     BaseController.__init__
        Inputs:   None.
        Outputs:  None.
        Features: Initializes PRIDAPeripheral, which instantiates the status
                  property and the connect, current_status, and poweroff
                  behaviours; defines the attribute dictionary of configurable
                  parameters
        """
        PRIDAPeripheral.__init__(self)

        # Configurable controller parameters
        self.attr_dict = {'SPEED': 'speed',
                          'DEGREES_PER_STEP': 'degrees',
                          'MICROSTEPS': 'microsteps',
                          'PORT_NUMBER': 'motor_port_num',
                          'STEP_TYPE': 'step_type'}

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def degrees(self):
        raise NotImplementedError('Motor degree not implemented!')

    @degrees.setter
    def degrees(self, val):
        raise NotImplementedError('Motor degree not implemented!')

    @property
    def microsteps(self):
        raise NotImplementedError('Motor microsteps not implemented!')

    @microsteps.setter
    def microsteps(self, val):
        raise NotImplementedError('Motor microsteps not implemented!')

    @property
    def motor_port_num(self):
        raise NotImplementedError('Motor port number not implemented!')

    @motor_port_num.setter
    def motor_port_num(self, val):
        raise NotImplementedError('Motor port number not implemented!')

    @property
    def speed(self):
        raise NotImplementedError('Motor speed not implemented!')

    @speed.setter
    def speed(self, val):
        raise NotImplementedError('Motor speed not implemented!')

    @property
    def step_type(self):
        raise NotImplementedError('Motor step type not implemented!')

    @step_type.setter
    def step_type(self, val):
        raise NotImplementedError('Motor step type not implemented!')

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def calibrate(self):
        """
        Name:     BaseController.calibrate
        Inputs:   None
        Outputs:  None
        Features: Perform a motor calibration to ensure operation.
        """
        raise NotImplementedError('calibrate not implemented')

    def profile(self, steps, vmax, up_ramp_frac=0.25, down_ramp_frac=0.25):
        """
        Name:     BaseController.profile
        Inputs:   - int, number of unit steps (steps)
                  - float, maximum velocity in rpm (vmax)
                  - [optional] float, fraction of total steps over which to
                    ramp up (up_ramp_frac)
                  - [optional] float, fraction of total steps over which to
                    ramp down (down_ramp_frac)
        Outputs:  list, ramped speed profile (speeds)
        Features: Returns a profile of ramped speeds
        """
        up_steps = int(up_ramp_frac*(steps+1))
        down_steps = int(down_ramp_frac*(steps+1))
        steady_steps = steps - up_steps - down_steps
        assert(up_steps+steady_steps+down_steps == steps)

        w = 0.0
        speeds = list()
        for k in range(steps):
            if k < up_steps:
                w += vmax/float(up_steps+1)
            else:
                if k < up_steps + steady_steps:
                    w = vmax
                else:
                    w += -vmax/float(down_steps+1)
            speeds.append(w)

        return speeds

    def single_step(self):
        """
        Name:     BaseController.single_step
        Inputs:   None
        Outputs:  None
        Features: Take one unit step
        """
        raise NotImplementedError('single step not implemented')

    def step(self, steps, to_ramp=False):
        """
        Name:     BaseController.step
        Inputs:   - int, number of unit steps to take (steps)
                  - [optional] bool, whether to ramp speeds (to_ramp)
        Outputs:  None.
        Features: Take a specified number of unit steps
        Depends:  - profile
                  - single_step
        """
        if self.status == 0:
            if self.step_type == 'micro':
                steps *= self.microsteps
            elif self.step_type == 'inter':
                steps *= 2

            if to_ramp:
                max_speed = self.speed
                ramp_speeds = self.profile(steps, max_speed)

            for i in range(steps):
                if to_ramp:
                    self.speed = ramp_speeds[i]
                self.single_step()

            if to_ramp:
                self.speed = max_speed
        else:
            raise PRIDAPeripheralError(msg="Motor status is not ready!")

    def turn_off_motors(self):
        """
        Name:     BaseController.turn_off_motors
        Inputs:   None
        Outputs:  None
        Features: Disable all motors attached to the controller.
        """
        raise NotImplementedError('turn off motors not implemented')
