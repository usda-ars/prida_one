#!/usr/bin/python
#
# camera.py
#
# VERSION: 1.6.0-dev
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os
import os.path
import tempfile
import subprocess
import sys

import gphoto2 as gp

from .pridaperipheral import PRIDAPeripheral
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class Camera(PRIDAPeripheral):
    """
    Camera, a class for communicating with USB digital cameras

    Name:       Camera
    History:    Version 1.6.0-dev
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    logger = logging.getLogger(__name__)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, my_parser=None, config_filepath=None):
        """
        Initialize data members, connect the camera.
        """
        self.logger.debug('start initializing camera.')
        PRIDAPeripheral.__init__(self)

        # initialize data members
        self._path = None
        self._summary = ''
        self.status = 1      # 0 for good; 1 for bad
        self.camera = None
        self.config = None
        self.context = None
        self.label_list = []
        self.name_list = []

        # connect camera and configure from file
        self.attr_dict = {'IMAGE_DIR': 'path', }
        if my_parser is not None and config_filepath is not None:
            my_parser(self, __name__, config_filepath)
        self.logger.debug('complete.')

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def aperture(self):
        """
        Integer representing camera aperture (if determinable)
        """
        self.logger.debug('returning camera aperture')
        apt_at_min = self._attr_getter(
            'Maximum Aperture at Focal Length Maximum')
        apt_at_max = self._attr_getter(
            'Maximum Aperture at Focal Length Minimum')
        if apt_at_min == apt_at_max:
            return apt_at_min
        else:
            val = min([apt_at_min, apt_at_max])
            return 'Unknown. Less than %s.' % (val)

    @property
    def exposure_time(self):
        """
        The camera exposure time or "shutter speed" (float)
        """
        self.logger.debug('returning camera exposure time')
        return self._attr_getter('Shutter Speed').strip('s')

    @property
    def focal_length(self):
        """
        The camera focal length (mm)
        """
        self.logger.debug('returning camera focal length')
        return self._attr_getter('Focal Length')

    @property
    def image_height(self):
        """
        Height dimension of the image resolution (pixels)
        """
        self.logger.debug('returning image height')
        tmp = self.image_size
        if int(self.orientation) in {90, 270}:
            tmp.sort()
            return tmp.pop()
        else:
            tmp.sort(reverse=True)
            return tmp.pop()

    @property
    def image_size(self):
        """
        Camera image pixel resolution (height, width)
        """
        self.logger.debug('returning image_size')
        img_size = self._attr_getter('Image Size')
        img_size = img_size.split('x')
        return [img_size[0], img_size[1]]

    @property
    def image_width(self):
        """
        Width dimension of the image resolution (pixels)
        """
        self.logger.debug('returning image width')
        tmp = self.image_size
        if int(self.orientation) in {0, 180}:
            tmp.sort()
            return tmp.pop()
        else:
            tmp.sort(reverse=True)
            return tmp.pop()

    @property
    def iso_speed(self):
        """
        Integer representing camera iso speed
        """
        self.logger.debug('returning camera ISO speed')
        return self._attr_getter('iso')

    @property
    def make(self):
        """
        Camera manufacturer
        """
        self.logger.debug('returning camera make')
        return self._attr_getter('Camera Manufacturer')

    @property
    def model(self):
        """
        Camera model
        """
        self.logger.debug('returning camera model')
        return self._attr_getter('Camera Model')

    @property
    def orientation(self):
        """
        Integer angle corresponding to camera position (using standard polar
        coordinates)
        """
        self.logger.debug('returning camera orientation')
        return self._attr_getter('orientation').strip("'")

    @property
    def path(self):
        """
        Output directory path for saving camera images
        """
        return self._path

    @path.setter
    def path(self, val):  # lint:ok
        if isinstance(val, str):
            if os.path.isdir(val) and os.access(val, os.W_OK):
                try:
                    with tempfile.TemporaryFile(mode='w', dir=val) as testfile:
                        testfile.write('Hello World!')
                except:
                    self.logger.exception('unexpected exception caught!'
                                          ' check image dir. write'
                                          ' permissions.')
                    raise PRIDAPeripheralError(
                        msg='Camera cannot write to image directory!')
                self._path = val
            else:
                raise ValueError('"%s" is not a valid directory' % (val))
        else:
            raise TypeError('path attribute must be a string')

    @property
    def summary(self):
        """
        Camera summary (make and model)
        """
        self.logger.debug('returning camera summary')
        return ' '.join([self.make, self.model])

    # /////////////////////////////////////////////////////////////////////////
    # Private Functions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def _attr_getter(self, attr):
        """
        Name:     Camera._attr_getter
        Inputs:   str, attribute name or label as defined by gphoto2 (attr)
        Outputs:  Attribute value (val)
        Features: Takes a attribute name or label string and calls the
                  appropriate getter function to return the desired attribute
        """
        if attr.lower() == attr:
            val = self._get_atter_by_name(attr)
        else:
            val = self._get_atter_by_label(attr)
        return val

    def _find_all_children(self, camera_widget):
        """
        Name:     Camera._find_all_children
        Inputs:   CameraWidget*, a gphoto2 Camera Widget (camera_widget)
        Outputs:  None
        Features: Recursive search to accumulate all camera attribute names
                  and labels
        """
        num_children = camera_widget.count_children()
        if num_children == 0:
            self.label_list.append(camera_widget.get_label())
            self.name_list.append(camera_widget.get_name())
        else:
            for i in range(num_children):
                self._find_all_children(camera_widget.get_child(i))

    def _get_atter_by_label(self, label):
        """
        Name:     Camera._get_attr_by_label
        Inputs:   str, the attribute label (label)
        Outputs:  Attribute value
        Features: Returns a gphoto2 camera attribute by its label
        """
        if label in self.label_list:
            return self.config.get_child_by_label(label).get_value()
        else:
            self.logger.error('%s not a valid camera attr label.' % (label))
            raise PRIDAPeripheralError(
                msg='%s not a valid camera attr label.' % (label))

    def _get_atter_by_name(self, name):
        """
        Name:     Camera._get_attr_by_name
        Inputs:   str, attribute name (name)
        Outputs:  Attribute value
        Features: Returns a gphoto2 camera attribute by its name
        """
        if name in self.name_list:
            return self.config.get_child_by_name(name).get_value()
        else:
            self.logger.error('%s not a valid camera attr name.' % (name))
            raise PRIDAPeripheralError(
                msg='%s not a valid camera attr name.' % (name))

    # /////////////////////////////////////////////////////////////////////////
    # Behaviour Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def capture(self, name, verbose=False):
        """
        Name:     Camera.capture
        Inputs:   - str, name of the image to be saved (name)
                  - bool, enable verbose output (verbose)
        Outputs:  String, absolute file path of captured image (filename)
        Features: Captures an image with the connected camera
        """
        self.logger.debug('start.')

        # Take a photo:
        file_path = gp.check_result(
            gp.gp_camera_capture(
                self.camera,
                gp.GP_CAPTURE_IMAGE,
                self.context))

        # Get the image file from the camera:
        camera_file = gp.check_result(
            gp.gp_camera_file_get(
                self.camera,
                file_path.folder,
                file_path.name,
                gp.GP_FILE_TYPE_NORMAL,
                self.context))

        # 'Save image as' to the computer preserving image extension
        f_ext = os.path.splitext(file_path.name)[1]
        f_name = "%s%s" % (str(name), f_ext)
        filename = os.path.join(self.path, f_name)
        self.logger.debug('Copying image to: %s', filename)
        gp.check_result(gp.gp_file_save(camera_file, filename))

        # Close camera:
        gp.check_result(gp.gp_camera_exit(self.camera, self.context))
        self.logger.debug('complete.')

        return filename

    def connect(self):
        """
        Name:     Camera.connect
        Inputs:   None
        Outputs:  None
        Features: Checks that the output directory for saving images exists
                  and is writeable and connects to the camera via gphoto2
        """
        # Initialize bad status:
        self.status = 1

        self.logger.debug('checking image dir write privillages')
        if self.path is None:
            raise PRIDAPeripheralError(msg='No output directory set!')
        if not os.path.isdir(self.path):
            raise PRIDAPeripheralError(msg='Output directory does not exist!')
        if not os.access(self.path, os.W_OK):
            raise PRIDAPeripheralError(msg='Cannot write to output directory!')
        else:
            # Double-check files can be written to output directory:
            try:
                with tempfile.TemporaryFile(mode='w', dir=self.path) as tfile:
                    tfile.write('Hello World!')
            except:
                self.logger.exception(
                    'Writing to camera output directory failed! '
                    'Check write permissions.')
                raise PRIDAPeripheralError(
                    msg='Camera cannot write to image directory!')

        self.logger.debug('connecting to camera')
        try:
            if not self.context:
                self.context = gp.Context()
                self.camera = gp.Camera()
                if self.context.camera_autodetect().count() >= 1:
                    try:
                        self.config = self.camera.get_config(self.context)
                    except:
                        self.logger.warning("Issue claiming USB device")

                        # Try killing PTPCamera and reconnecting:
                        if self.kill_ptp() == 0:
                            try:
                                self.config = self.camera.get_config(
                                    self.context)
                            except:
                                self.logger.exception(
                                    "Failed to claim USB device")
                                raise PRIDAPeripheralError(
                                    msg="Failed to claim USB device.")
                        else:
                            raise PRIDAPeripheralError(
                                msg="Could not claim USB device")
                else:
                    self.logger.error(
                        'No camera detected, please connect a camera.')
                    raise PRIDAPeripheralError(msg='No camera detected!')
                self.logger.debug('camera configured')
                self._find_all_children(self.config)
            else:
                self.logger.warning('Camera already connected.')
        except PRIDAPeripheralError:
            raise
        except:
            self.logger.exception(
                'Caught unknown exception. Could not connect to camera!')
            raise PRIDAPeripheralError(msg="Could not connect to camera.")
        else:
            self.status = 0
            self.logger.info("Camera connected!")

    def current_status(self):
        """
        Name:     Camera.current_status
        Inputs:   None
        Outputs:  int, status indicator (status)
        Features: Identifies the current status of the camera.
                  Required for PRIDAPeripheral
        """
        self.logger.debug('called')
        return self.status

    def kill_ptp(self):
        """
        Name:     Camera.kill_ptp
        Inputs:   None.
        Outputs:  None.
        Features: Runs a system kill command on Unix-based systems to stop
                  the PTPCamera tool that locks the camera USB on Mac OS
        """
        if sys.version_info >= (3, 5):
            if not os.name == 'nt':
                self.logger.warning("Killing PTPCamera processes")
                return subprocess.run(
                    ['killall', 'PTPCamera'],
                    stdout=subprocess.PIPE).returncode
        else:
            if not os.name == 'nt':
                return subprocess.call(
                    ['killall', 'PTPCamera'], stdout=subprocess.PIPE)

    def poweroff(self):
        """
        Name:     Camera.poweroff
        Inputs:   None
        Outputs:  None
        Features: Prepares the camera for manual shutdown.
                  Required for PRIDAPeripheral
        """
        self.logger.debug('start.')
        err = gp.check_result(gp.gp_camera_exit(self.camera, self.context))
        if err >= 1:
            self.logger.error('could not disconnect camera for poweroff.')
            raise PRIDAPeripheralError(
                msg='Could not disconnect camera for poweroff!')
        else:
            self.context = None
            self.camera = None
            self.config = None
            self.status = 1
            self._summary = ''
            self._attr_dict = {}
            self.logger.info('Camera disconnected. Safe to power down.')
        self.logger.debug('complete.')
