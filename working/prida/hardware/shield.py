#!/usr/bin/python
#
# shield.py
#
# VERSION: 1.6.0-dev
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import time
import logging

import serial
from serial import SerialException
import serial.tools.list_ports

from .basecontroller import BaseController
from .pridaperipheral import PRIDAPeripheralError


###############################################################################
# CLASSES:
###############################################################################
class Shield(BaseController):
    """
    Name:    Shield
    History: Version 1.6.0-dev
    """

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, my_parser=None, config_filepath=None):
        """
        Name:    Shield.__init__
        Inputs:   - [optional] object, parser function (my_parser)
                  - [optional] str, configuration file path (config_filepath)
        Feature: Initialize data memebers, inherit attributes and
                 behaviours from the BaseController and PridaPeripheral
                 abstract classes.
        Note:    currently assumes that system is always using microstepping.
        """
        logging.debug('start initializing shield.')
        super(Shield, self).__init__()

        my_name = __name__[:-6] + 'controller'
        self.logger = logging.getLogger(my_name)

        # binary motor serial communication values
        self.CAL = bin(127)       # 01111111 <-- calibrate command
        self.BEACON = bin(63)     # 00111111 <-- reset shield
        self.ECHO = bin(32)       # 00100000 <-- echo command
        self.RELEASE = bin(0)     # 00000000 <-- release command

        self.STEP = bin(64)       # 01000000 <-- step flag
        self.FORWARD = bin(16)    # 00010000 <-- step - direction
        self.BACKWARD = bin(0)    # 00000000 <-- step - direction
        self.SINGLE = bin(1)      # 00000001 <-- step - regime
        self.DOUBLE = bin(2)      # 00000010 <-- step - regime
        self.INTER = bin(4)       # 00000100 <-- step - regime
        self.MICRO = bin(8)       # 00001000 <-- step - regime

        # private attributes for property definitions
        self._speed = 2.0         # Approximate rotational velocity
        self._sleep_val = 0.01    # Time to sleep between steps
        self._step_res = 200      # Number of units steps per rotation
        self.__step_types = {self.SINGLE: 'single',
                             self.DOUBLE: 'double',
                             self.INTER: 'inter',
                             self.MICRO: 'micro',
                             }
        # NOTE: microsteps per step are defined in Adafruit_MotorShield.h
        self._microsteps = 16      # Number of microsteps per unit step
        self._step_type = self.__step_types[self.MICRO]  # Stepping type
        self._motor_port_num = 1   # Motor Shield Port Number In Use
        self._serial_port = None
        self._baud_rate = 9600
        self._my_serial = serial.Serial()

        if my_parser is not None and config_filepath is not None:
            my_parser(self, my_name, config_filepath)
        self.logger.debug('complete.')

        # set property values
        self.timeout = 5
        self.connect()

    # /////////////////////////////////////////////////////////////////////////
    # Property Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @property
    def baud_rate(self):
        """
        The serial baud rate
        """
        return self._baud_rate

    @baud_rate.setter
    def baud_rate(self, val):  # lint:ok
        if isinstance(val, int):
            if val > 0:
                self._baud_rate = val
            else:
                self.logger.error('baud rate must be a positive value')
                raise ValueError('baud rate must be a positive value')
        else:
            self.logger.error('baud rate must be an integer')
            raise TypeError('baud rate must be an integer')

    @property
    def degrees(self):
        """
        The step resolution in terms of degrees per unit step.
        """
        return 360.0 / self.step_res

    @degrees.setter
    def degrees(self, value):  # lint:ok
        self.logger.error('setting the step degree manualy is forbidden')
        raise AttributeError('Setting step degree is forbidden.')

    @property
    def microsteps(self):
        """
        The number of microsteps per unit step
        """
        return self._microsteps

    @microsteps.setter
    def microsteps(self, value):  # lint:ok
        raise AttributeError('setting of the microsteps is forbidden')

    @property
    def motor_port_num(self):
        """
        The motor port number that the shield is currently controlling
        """
        return self._motor_port_num

    @motor_port_num.setter
    def motor_port_num(self, num):  # lint:ok
        """
        Sets stepper motor port number.
        """
        if isinstance(num, int):
            if num in (1, 2):
                self._motor_port_num = num
            else:
                self.logger.error('motor port number restricted to 1 or 2.')
                raise ValueError('motor port number restricted to 1 or 2.')
        else:
            self.logger.error('motor port number must be an integer.')
            raise TypeError('motor port number must be an integer.')

    @property
    def my_serial(self):
        """
        The controller's serial object
        """
        return self._my_serial

    @my_serial.setter
    def my_serial(self, val):  # lint:ok
        raise AttributeError('setting of the serial device atr is forbidden')

    @property
    def serial_port(self):
        """
        The active computer serial port location (str)
        """
        return self._serial_port

    @serial_port.setter
    def serial_port(self, val):  # lint:ok
        """
        Sets the computer serial port location
        """
        if isinstance(val, str):
            ports = serial.tools.list_ports.comports()
            p_list = [com.device for com in ports]
            if val in p_list:
                self._serial_port = val
            else:
                self.logger.error(
                    '%s is not a valid path to a serial device' % (val))
                raise ValueError(
                    '%s is not a valid path to a serial device' % (val))
        else:
            self.logger.error('serial port attribute must be a string')
            raise TypeError('serial port attribute must be a string')

    @property
    def sleep_val(self):
        """
        The shortest delay in seconds between consecutive single steps.
        """
        # check step type for calculating sleep time
        if self.step_type is self.__step_types[self.MICRO]:
            substeps_per_rot = self.step_res * self.microsteps
        elif self.step_type is self.__step_types[self.INTER]:
            substeps_per_rot = self.step_res * 2
        else:
            substeps_per_rot = self.step_res
        substeps_per_min = substeps_per_rot * self.speed
        self._sleep_val = 60.0 / substeps_per_min
        self.logger.debug('sleep val:{}'.format(self._sleep_val))

        return self._sleep_val

    @sleep_val.setter
    def sleep_val(self, val):  # lint:ok
        raise AttributeError('setting of the Shield sleep value is forbidden')

    @property
    def speed(self):
        """
        The approximate speed value at which the motor spins.
        """
        return self._speed

    @speed.setter
    def speed(self, val):  # lint:ok
        """
        Updates the motor speed by affecting the sleep value between
        consecutive single steps loosely based on rotations per minute.
        """
        if isinstance(val, (float, int)):
            if val > 0:
                self._speed = val
            else:
                self.logger.error('speed requires a non-negative value.')
                raise ValueError('Motor speed requires a non-negative value.')
        else:
            self.logger.error('speed must be a number.')
            raise TypeError('Motor speed must be a number.')

    @property
    def step_res(self):
        """
        The number of unit steps to complete a full revolution of the motor
        shaft. A 1.8-deg motor has a step resolution of 200.
        """
        return self._step_res

    @step_res.setter
    def step_res(self, sr):  # lint:ok
        if isinstance(sr, int):
            if sr > 0:
                self._step_res = sr
            else:
                self.logger.error('step resolution must be non-negative.')
                raise ValueError('Step resolution must be non-negative')
        else:
            self.logger.error('step resolution must be an integer.')
            raise TypeError('Step resolution must be an integer')

    @property
    def step_type(self):
        """
        The stepper motor's step type (micro, single, double, interleave).
        """
        return self._step_type

    @step_type.setter
    def step_type(self, val):  # lint:ok
        if isinstance(val, str):
            if val in self.__step_types.values():
                self._step_type = val
            else:
                try:
                    decval = int(val, 2)
                except ValueError:
                    self.logger.error(
                        "'{}' is not a valid stepping regime".format(val))
                    raise ValueError(
                        "'{}' is not a valid stepping regime.".format(val))
                else:
                    if bin(decval) in self.__step_types.keys():
                        self._step_type = self.__step_types[bin(decval)]
                    else:
                        self.logger.error(
                            "binary string '{}' does not ".format(val) +
                            "correspond to a stepping regime")
                        raise ValueError(
                            "Binary string '{}' does not ".format(val) +
                            "correspond to a stepping regime.")

    @property
    def stepstr(self):
        """
        A string representation of the binary step command.
        """
        return self.binadd(self.STEP,
                           self.FORWARD,
                           eval('self.{}'.format(self._step_type.upper())))

    @stepstr.setter
    def stepstr(self, val):  # lint:ok
        raise AttributeError('setting of the Shield stepstr is forbidden')

    @property
    def timeout(self):
        """
        The serial communication timeout in seconds.
        """
        return self.my_serial.timeout

    @timeout.setter
    def timeout(self, val):  # lint:ok
        if isinstance(val, (int, float)):
            if val >= 0:
                self.my_serial.timeout = val
            else:
                self.logger.debug('timeout atr must be a non-negative value')
                raise ValueError('timeout atr must be a non-negative value')
        else:
            self.logger.debug(
                'timeout atr must be a number of type int or float')
            raise TypeError(
                'timeout atr must be a number of type int or float')

    # /////////////////////////////////////////////////////////////////////////
    # Function Definitions:
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def bin_to_bytes(self, binstr):
        """
        Name:     Shield.bin_to_bytes
        Inputs:   str, binary string to be converted
        Outputs:  str, byte string representation (bytestr)
        Features: Converts a binary string to the equivilent ASCII byte string
        """
        bytestr = b''
        try:
            bytestr = chr(int(binstr, 2)).encode('utf-8')
        except:
            self.logger.exception('failed binary to bytestring conversion')
        finally:
            return bytestr

    def binadd(self, *args, **kwargs):
        """
        Name:     Shield.binadd
        Inputs:   - any number of binary string arguments (*args)
                  - any number of additional keyword arguments (**kwargs)
        Outputs:  str, resultant binary string sum (binsum)
        Features: Adds together an arbitary number of python binary strings
        """
        binsum = ''
        try:
            binsum = bin(sum([int(arg, 2) for arg in args]))
        except:
            self.logger.exception('failed binary addition')
        finally:
            return binsum

    def bytes_to_bin(self, bytestr):
        """
        Name:     Shield.bytes_to_bin
        Inputs:   str, byte string to convert (bytestr)
        Outputs:  str, resulting binary string (binstr)
        Features: Converts a byte string ASCII character to its eqivalent
                  binary string value
        """
        binstr = ''
        try:
            binstr = bin(ord(bytestr.decode('utf-8')))
        except:
            self.logger.exception('failed bytestring to binary conversion')
        finally:
            return binstr

    def calibrate(self):
        """
        Name:     Shield.calibrate
        Inputs:   None
        Outputs:  None
        Features: Takes three unit steps to engage coils.
                  * Required for BaseController
        Depends:  - bin_to_bytes
                  - current_status
        """
        self.logger.debug('calibrating motor...')
        max_attempts = 10
        cur_attempt = 0
        keep_trying = True
        if self.my_serial.isOpen():
            if self.current_status() == 0:
                while keep_trying and cur_attempt < max_attempts:
                    cur_attempt += 1
                    self.my_serial.write(self.bin_to_bytes(self.CAL))
                    time.sleep(0.05)
                    mybyte = self.my_serial.read(size=1)
                    keep_trying = mybyte != self.bin_to_bytes(self.CAL)
                if keep_trying:
                    raise PRIDAPeripheralError(msg='Calibration failed!')
            else:
                raise PRIDAPeripheralError(msg="Lost connection with motor!")

    def connect(self):
        """
        Name:     Shield.connect
        Inputs:   None
        Outputs:  None
        Features: Attempts to connect to the shield
                  * Required for PRIDAPeripheral
        Depends:  - current_status
                  - listen
        """
        self.logger.debug('establishing connection...')
        status = 1
        if not self.my_serial.isOpen():
            for port in serial.tools.list_ports.comports():
                self.my_serial.baudrate = self.baud_rate
                self.my_serial.port = port.device
                try:
                    self.my_serial.open()
                except SerialException:
                    self.logger.warning(
                        'Failed to connect to serial port %s', port.device)
                else:
                    status = self.listen()
                    if status == 0:
                        self.logger.info("found potential arduino")
                        self.serial_port = port.device
                        break
                    self.my_serial.close()
        else:
            self.logger.warning('serial port open, may already be connected')
            status = self.current_status()

        if status == 0:
            self.status = 0
            self.logger.info(
                'connection established at port {}'.format(self.serial_port))
        else:
            self.status = 1
            self.logger.error('could not establish connection')
            raise PRIDAPeripheralError(
                msg=('Could not establish connection with Arduino shield. '
                     'Try resetting the Arduino and reconnecting.'))

    def current_status(self):
        """
        Name:     Shield.current_status
        Inputs:   None
        Outputs:  int, status indicator (status)
        Features: Attempts a handshake with the shield
                  * Required for PRIDAPeripheral
        Depends:  - bin_to_bytes
                  - bytes_to_bin
        """
        status = 1
        if self.my_serial.isOpen():
            # flush serial buffers
            self.logger.debug('flushing buffers...')
            self.my_serial.reset_input_buffer()
            self.my_serial.reset_output_buffer()

            # write echo command ascii character to serial port
            self.logger.debug('attempting handshake...')
            self.my_serial.write(self.bin_to_bytes(self.ECHO))
            time.sleep(0.05)
            my_bytes = self.my_serial.read(size=1)
            if my_bytes == b'':
                self.status = 1
                self.logger.warning('Serial read to motor timed out.')
            elif self.bytes_to_bin(my_bytes) == self.ECHO:
                self.status = 0
                status = 0
            else:
                self.status = 1
                self.logger.warning(
                    'Bad return on echo call, received %s' % (str(my_bytes)))
        return status

    def listen(self, force=True):
        """
        Name:     shield.listen
        Inputs:   None
        Outputs:  None
        Features: Listens for Arduino on the current port
        Depends:  - bin_to_bytes
                  - current_status
        """
        status = 1
        if self.my_serial.isOpen():
            try:
                mybyte = self.my_serial.read(size=1)
            except:
                self.logger.exception('exception caught during serial read')
            else:
                if mybyte == self.bin_to_bytes(self.RELEASE):
                    self.logger.debug(
                        'found possible arduino, checking status')
                    self.my_serial.write(self.bin_to_bytes(self.ECHO))
                    time.sleep(0.05)
                    status = self.current_status()
                    if status == 0:
                        self.logger.debug('connection established.')
                elif mybyte == b'':
                    self.logger.warning("Listening to motor timed out!")
                else:
                    self.logger.error("Listening read unexpected byte")
        return status

    def poweroff(self):
        """
        Name:     Shield.poweroff
        Inputs:   None
        Outputs:  None
        Features: Sends the serial command to power down the coils and closes
                  the serial connection
                  * Required for PRIDAPeripheral
        Depends:  turn_off_motors
        """
        self.logger.debug('powering down the controller...')
        self.turn_off_motors()
        if self.my_serial.isOpen():
            self.my_serial.close()
            self.status = 1

    def single_step(self):
        """
        Name:     Shield.single_step
        Inputs:   None
        Outputs:  None
        Features: Take a single step
                  * Required for BaseController
        """
        if self.my_serial.isOpen():
            # write step command to arduino
            time.sleep(self.sleep_val)
            self.my_serial.write(self.bin_to_bytes(self.stepstr))
        else:
            raise PRIDAPeripheralError(msg="Lost motor serial connection!")

    def turn_off_motors(self):
        """
        Name:     Shield.turn_off_motors
        Inputs:   None
        Outputs:  None
        Features: Disables all motors attached to the serial port.
                  * Required for BaseController
        """
        self.logger.debug('powering down the motor...')
        if self.my_serial.isOpen():
            try:
                self.my_serial.write(self.bin_to_bytes(self.RELEASE))
            except SerialException:
                self.logger.error("Failed to write motor release to serial")
            except:
                self.logger.exception("Unknown error during motor release")

###############################################################################
# FOR DEBUG
###############################################################################
if __name__ == '__main__':
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)

    # Instantiating logging handler and record format:
    root_handler = logging.StreamHandler()
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)

    # Create a shield controller:
    my_c = Shield()
    my_c.step_type = 'double'

    # STEP_TYPE  SPEED   SLEEP_VAL
    # double     100     0.003
    # -          200     0.0015

    my_c.speed = 100
    my_c.calibrate()
    my_c.step(100)
