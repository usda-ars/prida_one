#!/usr/bin/python
#
# image_worker.py
#
# VERSION: 1.6.0-dev
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software is freely available to the public for use.      #
# The Department of Agriculture (USDA) and the U.S. Government have not       #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     USDA-Agricultural Research Service                                      #
#     Robert W. Holley Center for Agriculture and Health                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import logging
import os

import numpy
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QObject
try:
    import scipy.optimize
    analysis_mode = True
except ImportError:
    analysis_mode = False

from .calibutil import calc_ha
from .calibutil import calc_hg
from .calibutil import calc_hw
from .calibutil import calc_phi
from .calibutil import calc_theta
from .calibutil import calc_ya
from .calibutil import calc_yg
from .calibutil import calc_yw
from .calibutil import element
from .calibutil import find_feature_extents
from .calibutil import find_foreground
from .calibutil import find_neighbors
from .calibutil import find_particles
from .calibutil import func_line
from .calibutil import func_poly
from .imutil import rgb_to_gray
from .imutil import scale_image


###############################################################################
# CLASS:
###############################################################################
class ImageWorker(QObject):
    """
    Name:     ImageWorker
    Features: This class is a worker object used for image processing.
    History:  Version 1.6.0-dev
    Todo:     _ add session cropping support
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Variable Initialization
    # ////////////////////////////////////////////////////////////////////////
    finished = pyqtSignal()
    getProcImg = pyqtSignal(str, int)
    isCalibrated = pyqtSignal(tuple)
    isFound = pyqtSignal(int)
    isMerged = pyqtSignal(int, int)
    isReady = pyqtSignal(numpy.ndarray)
    setBGColor = pyqtSignal(str)
    setImgRange = pyqtSignal(str)
    setImgShape = pyqtSignal(tuple)
    updateProc = pyqtSignal(str)
    updateProg = pyqtSignal(int, int)
    sendAlert = pyqtSignal(str)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, parent=None, mutex=None, is_enabled=analysis_mode):
        """
        Name:     ImageWorker.__init__
        Inputs:   - [optional] Qt parent object (parent)
                  - [optional] Qt mutex lock (mutex)
        Returns:  None.
        Features: Class initialization
        Depends:  reset_calib_params
        """
        super(ImageWorker, self).__init__(parent)

        # Create a worker logger:
        self.logger = logging.getLogger(__name__)
        self.logger.debug("image worker object initialized")

        # Analysis mode flag:
        self.is_enabled = is_enabled

        # Initialize variables
        self.mutex = mutex         # Qt mutex lock
        self.datasets = []         # list of HDF5 dataset paths
        self.elements = []         # list of element objects
        self.proc_img = None       # processed image array
        #                           (undergoes cropping, stretching and binary)
        self.merge_img = None      # merged image array

        self.is_cropped = False    # boolean for cropped images
        self.is_binary = False     # boolean for binary images
        self.is_merged = False     # boolean for merged images
        self.is_stretched = False  # boolean for stretched images

        self.bin_thresh = 0        # Otsu's threshold value
        self.bg_color = 0          # background color for thresholding
        self.crop_left = 0         # image crop column left
        self.crop_right = 0        # image crop column right
        self.crop_top = 0          # image crop row top
        self.crop_bottom = 0       # image crop tow bottom
        self.kernel = 0            # kernel width
        self.merge_count = 0       # image counter for merging
        self.path = None           # HDF5 path (session or image)
        self.sfactor = 100         # scaling factor

        self.ir = None             # image resolution factor, m/px
        self.ir_err = None         # image resolution factor error, m/px
        self.za = None             # camera to tank wall distance, m
        self.zg = None             # tank wall thickness, m
        self.zw = None             # origin to inner tank wall distance, m

        self.reset_calib_params()

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def calc_pitch(self):
        """
        Name:     ImageWorker.calc_pitch
        Inputs:   None.
        Outputs:  tuple, HF' and its associated std error
        Features: Returns estimate of HF', the pixel coordinate of the observed
                  center of focus above/below the center of image
        Depends:  - check_for_pitch
                  - fit_element_heights

        @TODO: error calculation
        """
        self.fit_element_heights()

        num_e = len(self.elements)
        if num_e > 0:
            # Get coordinates for center of original image:
            h, w = self.merge_img.shape
            h_coi = int(0.5*h + 0.5)

            isokay = self.check_for_pitch()
            if isokay:
                # Calculate HF' by setting derivative of parabola equation
                # equal to zero and solving for the pixel coordinate of the
                # root (minimization)
                # NOTE: HF' is positive above the COI
                try:
                    self.logger.debug("calculating hf prime")
                    hf_prime = h_coi + 0.5*(self.k2/self.k1)
                    hf_prime *= self.ir

                    ddir = h_coi + 0.5*(self.k2/self.k1)
                    ddk1 = -0.5*self.ir*self.k2/numpy.power(self.k1, 2.0)
                    ddk2 = 0.5*self.ir/self.k1

                    ddirx = numpy.power(ddir*self.ir_err, 2.0)
                    ddk1x = numpy.power(ddk1*self.k1_err, 2.0)
                    ddk2x = numpy.power(ddk2*self.k2_err, 2.0)

                    hf_prime_err = numpy.sqrt(ddirx + ddk1x + ddk2x)

                    # Calculate total distance, zt:
                    zt = self.za + self.zg + self.zw

                    # Calculate the intermediate distances:
                    ha, ha_err = calc_ha(self.za, zt, hf_prime, hf_prime_err)
                    hg, hg_err = calc_hg(self.zg, zt, hf_prime, hf_prime_err)
                    hw, hw_err = calc_hw(self.zw, zt, hf_prime, hf_prime_err)

                    self.logger.info(
                        "ha = %0.2f +/- %0.2f" % (ha*100, ha_err*100))
                    self.logger.info(
                        "hg = %0.2f +/- %0.2f" % (hg*100, hg_err*100))
                    self.logger.info(
                        "hw = %0.2f +/- %0.2f" % (hw*100, hw_err*100))

                    self.logger.debug("calculating hf")
                    hf = ha + hg + hw
                    syax = numpy.power(ha_err, 2.0)
                    sygx = numpy.power(hg_err, 2.0)
                    sywx = numpy.power(hw_err, 2.0)
                    hf_err = numpy.sqrt(syax + sygx + sywx)

                    self.logger.info(
                        "hf = %0.2f +/- %0.2f" % (hf*100, hf_err*100))

                    self.logger.debug("calculating pitch")
                    self.pitch, self.pitch_err = calc_phi(hf, hf_err, zt)
                except:
                    self.logger.exception("pitch calculation failed")
                    self.pitch = None
                    self.pitch_err = None
                else:
                    self.logger.info("pitch = %f deg", self.pitch)
        else:
            self.logger.warning("no elements for calculation")
            self.pitch = None
            self.pitch_err = None

    def calc_roll(self):
        """
        Name:     ImageWorker.calc_roll
        Inputs:   None.
        Outputs:  None.
        Features: Calculates roll, theta, based on the slope of the axis of
                  rotation
        Depends:  - calc_theta
                  - fit_aor
        """
        self.fit_aor()
        if self.m is not None:
            try:
                self.logger.debug("calculating roll")
                self.roll, self.roll_err = calc_theta(self.m, self.m_err)
            except:
                self.logger.exception("roll calculation failed")
                self.roll = None
                self.roll_err = None
            else:
                self.logger.info("roll = %f deg", self.roll)
                self.logger.info("roll error = %f deg", self.roll_err)
        else:
            self.logger.warning("no slope available")
            self.roll = None
            self.roll_err = None

    def calc_translation(self):
        """
        Name:     ImageWorker.calc_translation
        Inputs:   None.
        Outputs:  None.
        Features: Calculates translation, yt, of the axis of rotation based on
                  the calibration elements and its associated standard error
        Depends:  - calc_ya       - check_for_translation
                  - calc_yg       - convert_to_e1
                  - calc_yw       - convert_to_e2
                  - fit_aor
        """
        self.logger.debug("calculating translation")

        self.fit_aor()
        num_e = len(self.elements)
        if num_e > 0:
            self.logger.debug("extracting center of image coordinates")
            h, w = self.merge_img.shape
            p2_coi = int(0.5*h + 0.5)
            p1_coi = int(0.5*w + 0.5)

            e1_coi = self.convert_to_e1(p1_coi)
            e2_coi = self.convert_to_e2(p2_coi)

            isok = self.check_for_translation()  # @TODO
            if isok:
                try:
                    # Calculate uncorrected pixel translation, Yt':
                    pYt = e1_coi - (e2_coi - float(self.b)) / self.m

                    # Calculate the error of Yt':
                    ddm = (e2_coi - self.b)/numpy.power(self.m, 2.0)
                    ddb = 1.0/self.m
                    ddmx = numpy.power(ddm*self.m_err, 2.0)
                    ddbx = numpy.power(ddb*self.b_err, 2.0)
                    pYt_err = numpy.sqrt(ddmx + ddbx)

                    # Calculate uncorrected physical translation, yt':
                    ytp = pYt*self.ir

                    # Calculate the error of yt':
                    pyrx = numpy.power(pYt*self.ir_err, 2)
                    piyx = numpy.power(self.ir*pYt_err, 2)
                    ytp_err = numpy.sqrt(pyrx + piyx)

                    # Calculate total distance, zt:
                    zt = self.za + self.zg + self.zw

                    # Calculate the intermediate distances:
                    ya, ya_err = calc_ya(self.za, zt, ytp, ytp_err)
                    yg, yg_err = calc_yg(self.zg, zt, ytp, ytp_err)
                    yw, yw_err = calc_yw(self.zw, zt, ytp, ytp_err)

                    self.logger.info(
                        "ya = %0.2f +/- %0.2f" % (ya*100, ya_err*100))
                    self.logger.info(
                        "yg = %0.2f +/- %0.2f" % (yg*100, yg_err*100))
                    self.logger.info(
                        "yw = %0.2f +/- %0.2f" % (yw*100, yw_err*100))

                    # Calculate corrected translation:
                    self.yt = ya + yg + yw

                    syax = numpy.power(ya_err, 2.0)
                    sygx = numpy.power(yg_err, 2.0)
                    sywx = numpy.power(yw_err, 2.0)
                    self.yt_err = numpy.sqrt(syax + sygx + sywx)
                except:
                    self.logging.exception("Failed to calculate translation!")
                    self.yt = None
                    self.yt_err = None
                else:
                    self.logger.info(
                        "yt = %0.2f +/- %0.2f" % (
                            self.yt*100, self.yt_err*100))
        else:
            self.logger.warning("no elements for calculation")
            self.yt = None
            self.yt_err = None

    def calibrate(self, calib_vals):
        """
        Name:     ImageWorker.calibrate
        Inputs:   tuple, calibration input values (calib_vals)
        Outputs:  None.
        Features: Calculates the three calibration parameters and signals the
                  return values back to main thread
        Depends:  - calc_roll
                  - calc_translation
                  - calc_pitch
        """
        (self.ir, self.ir_err, self.za, self.zg, self.zw) = calib_vals
        self.calc_roll()
        self.calc_translation()
        self.calc_pitch()
        calib_params = (self.roll, self.roll_err,
                        self.yt, self.yt_err,
                        self.pitch, self.pitch_err)
        self.isCalibrated.emit(calib_params)

    def check_for_translation(self):
        """
        Name:     ImageWorker.check_for_translation
        Inputs:   None.
        Outputs:  bool
        Features: Checks calibration parameters necessary for calculating yt
        """
        if self.m is None:
            self.logger.error("AoR slope is not calculated!")
            return False
        elif self.b is None:
            self.logger.error("AoR intercept is not calculated!")
            return False
        elif self.ir is None:
            self.logger.error("Ir is not set!")
            return False
        elif self.za is None or self.zg is None or self.zw is None:
            self.logger.error("Calibration distances not set!")
            return False
        else:
            return True

    def check_for_pitch(self):
        """
        Name:     ImageWorker.check_for_pitch
        Inputs:   None.
        Outputs:  bool
        Features: Checks calibration parameters necessary for calculating pitch
        """
        if self.k1 is None or self.k2 is None or self.k3 is None:
            self.logger.error("Quadratic has not been solved!")
            return False
        elif self.ir is None:
            self.logger.error("Ir is not set!")
            return False
        elif self.za is None or self.zg is None or self.zw is None:
            self.logger.error("Calibration distances not set!")
            return False
        else:
            return True

    def convert_to_e1(self, p1):
        """
        Name:     ImageWorker.convert_to_e1
        Inputs:   int, p1 pixel coordinate (i.e., image columns)
        Outputs:  int, e1 element coordinate
        Features: Converts a coordinate from pixel, p1, to element, e1,
                  coordinate systems
        """
        num_e = len(self.elements)
        if num_e > 0:
            center_points = []
            for i in range(num_e):
                obj = self.elements[i]
                center_points.append(obj.center)

            w_points = numpy.array([point[1] for point in center_points])
            w_min = w_points.min()
            e1 = p1 - w_min
            return e1
        else:
            self.logger.warning("Conversion failed. No elements!")
            return None

    def convert_to_e2(self, p2):
        """
        Name:     ImageWorker.convert_to_e2
        Inputs:   int, p2 pixel coordinate (i.e., image rows)
        Outputs:  int, e2 element coordinate
        Features: Converts a coordinate from pixel, p2, to element, e2,
                  coordinate systems
        """
        num_e = len(self.elements)
        if num_e > 0:
            center_points = []
            for i in range(num_e):
                obj = self.elements[i]
                center_points.append(obj.center)

            h_points = numpy.array([point[0] for point in center_points])
            h_max = h_points.max()
            e2 = h_max - p2
            return e2
        else:
            self.logger.warning("Conversion failed. No elements!")
            return None

    def crop(self, tidx, bidx, lidx, ridx):
        """
        Name:     ImageWorker.crop
        Inputs:   - int, top row index (tidx)
                  - int, bottom row index (bidx)
                  - int, left column index (lidx)
                  - int, right column index (ridx)
        Outputs:  None.
        Features: toCrop socket; crops merge image
        Depends:  update_image_range
        """
        if self.merge_img is not None:
            self.crop_left = lidx
            self.crop_right = ridx
            self.crop_top = tidx
            self.crop_bottom = bidx

            h, w = self.merge_img.shape
            if tidx < 0 or tidx >= bidx or bidx > h:
                self.sendAlert.emit(
                    "Please select a top index less than the bottom index.")
                self.updateProc.emit("Image Processing Ready")
            elif lidx < 0 or lidx >= ridx or ridx > w:
                self.sendAlert.emit(
                    "Please select left index less than right index.")
                self.updateProc.emit("Image Processing Ready")
            else:
                self.proc_img = numpy.copy(self.merge_img)
                self.proc_img = self.proc_img[tidx:bidx, lidx:ridx]
                self.is_cropped = True
                self.updateProc.emit("Cropped merge image")
                self.update_image_range()
                self.isReady.emit(self.proc_img)
        else:
            self.sendAlert.emit("Please first load merge image.")
            self.updateProc.emit("Image Processing Ready")
            self.crop_left = 0
            self.crop_right = 0
            self.crop_top = 0
            self.crop_bottom = 0

    def export_data(self, out_dict):
        """
        Name:     ImageWorker.export_data
        Inputs:   dict, output values (out_dict)
        Outputs:  None.
        Features: Saves output values to file
        Depends:  - calc_hf_prime
                  - calc_roll_translation
        """
        self.logger.debug("preparing export data")
        file_path = out_dict.get('path', '')
        bname = out_dict.get('name', '')
        sdate = out_dict.get('date', '')
        if self.bg_color == 0:
            background = "Black"
        elif self.bg_color == 1:
            background = "White"
        else:
            background = "N/A"

        # Header column names:
        col_names = ['Name', 'Date', 'ImgCount', 'ImgHeight', 'ImgWidth',
                     'Scale', 'Background', 'ROI_top', 'ROI_bottom',
                     'ROI_left', 'ROI_right', 'Otsu', 'KernelWidth',
                     'ElementCount', 'Res_mm_px',
                     'Transl_px', 'TranslErr_px',
                     'Transl_mm', 'TranslErr_mm',
                     'Roll_deg', 'RollErr_deg',
                     'Pitch_deg', 'PitchErr_deg']

        # Column data (NOTE: everything must be a string in order to use join):
        export_dict = {
            'Name': str(bname),
            'Date': str(sdate),
            'ImgCount': str(self.merge_count),
            'ImgHeight': str(self.merge_img.shape[0]),
            'ImgWidth': str(self.merge_img.shape[1]),
            'Scale': str(self.sfactor),
            'Background': background,
            'ROI_top': str(self.crop_top),
            'ROI_bottom': str(self.crop_bottom),
            'ROI_left': str(self.crop_left),
            'ROI_right': str(self.crop_right),
            'Otsu': str(self.bin_thresh),
            'KernelWidth': str(self.kernel),
            'ElementCount': str(len(self.elements)),
        }
        header_line = ';'.join(col_names)
        output_vals = [export_dict.get(i, '') for i in col_names]

        self.logger.debug("preparing to write to %s", file_path)

        if len(self.elements) == 0:
            self.logger.debug("no elements found, exporting ...")
            output_string = ";".join(output_vals)
        else:
            self.logger.debug("found %d elements, exporting ...",
                              len(self.elements))

            # Perform calibration:
            if self.roll is not None and self.yt is not None:
                export_dict['Res_mm_px'] = str(1000*self.ir)
                export_dict["Roll_deg"] = str(self.roll)
                export_dict["RollErr_deg"] = str(self.roll_err)
                export_dict["Transl_mm"] = str(1000*self.yt)
                export_dict["TranslErr_mm"] = str(1000*self.yt_err)
                export_dict["Transl_px"] = str(int(self.yt/self.ir + 0.5))
                export_dict["TranslErr_px"] = str(self.yt_err/self.ir)
                output_vals = [export_dict.get(i, '') for i in col_names]

            if self.pitch is not None:
                export_dict["Pitch_deg"] = str(self.pitch)
                export_dict["PitchErr_deg"] = str(self.pitch_err)
                output_vals = [export_dict.get(i, '') for i in col_names]

            # Update headerline and output values for each element
            element_header = ""
            for i in range(len(self.elements)):
                idx = str(i + 1)
                element_header += "".join([";Element-", idx, "_size"])
                element_header += "".join([";Element-", idx, "_height"])
                element_header += "".join([";Element-", idx, "_width"])

                obj = self.elements[i]
                try:
                    output_vals += [str(obj.size),
                                    str(obj.height),
                                    str(obj.width)]
                except:
                    self.logger.exception("###ELEMENT FAILED###")
            header_line += element_header

            # Create output string:
            try:
                self.logger.debug("creating output string")
                output_string = ";".join(output_vals)
            except:
                self.logger.exception("###FAILED###")
                output_string = ""

            # NOTE: temporary testing function:
            # self.plot_centers()

        # Prepare the output file name (update is already exists)
        if os.path.isfile(file_path):
            self.logger.debug("appending to existing file")
            to_append = True
        else:
            self.logger.debug("creating export file")
            to_append = False

        try:
            self.logger.debug("writing to file")
            my_file = open(file_path, 'a')
            if not to_append:
                my_file.write(header_line)
                my_file.write("\n")
            my_file.write(output_string)
            my_file.write("\n")
        except:
            self.logger.exception(
                "failed to write to file %s", file_path)
        else:
            self.logger.debug("successfully exported data")
        finally:
            my_file.close()

    def find_objects(self, my_array):
        """
        Name:     ImageWorker.find_objects
        Inputs:   numpy.ndarray, image array (my_array)
        Outputs:  None.
        Features: toFind socket; searches the binary process image for
                  calibration objects
        Depends:  - find_neighbors
                  - investigate

        TODO:     _ take advantage of imutil's is_binary function
        """
        self.logger.debug("finding objects ...")
        self.investigate(my_array)
        if self.is_binary:
            # Create a blank copy of the processed image:
            my_img = numpy.ones(my_array.shape)
            my_img *= 255
            self.isReady.emit(my_img)

            # Determine the central width pixel of processed image:
            h, w = my_img.shape
            m = int(0.5 * w)

            # Initialize variables:
            self.elements = []
            visited = set()
            num_found = 0
            for p in range(h):
                loc = (p, m)
                if my_array[loc] == 0 and loc not in visited:
                    # New black pixel found! perform next sweep
                    num_found += 1
                    self.updateProc.emit(
                        "Searching ... found %d objects" % (num_found))
                    self.logger.debug("found new object at %s", str(loc))
                    new_e = element(
                        data=find_neighbors(my_array, loc),
                        shape=(h, w),
                        offset=(self.crop_top, self.crop_left))
                    self.elements.append(new_e)
                    visited.update(new_e._orig)
                    # Update element image:
                    my_img = numpy.minimum(my_img, new_e.img)
            self.updateProc.emit("Found %d objects!" % (num_found))
            self.isReady.emit(my_img)
            self.isFound.emit(num_found)
        else:
            self.logger.warning("objects can only be found in binary images")
            self.sendAlert.emit("Please first threshold the image.")
            self.updateProc.emit("Image Processing Ready")

    def find_objects2(self, my_array):
        """
        Name:     ImageWorker.find_objects2
        Inputs:   numpy.ndarray, binary image (my_array)
        Outputs:  None.
        Signals:  - isFound (to Prida)
                  - isReady (to Prida)
                  - updateProc (to Prida)
                  - sendAlert (to Prida)
        Features: Searches binary image for grouped foreground pixels and sets
                  them equal to elements; has the benefit over nearest
                  neighbor search in that the grouped pixels do not have to be
                  contiguous
        Depends:  - find_feature_extents
                  - find_foreground
                  - investigate

        TODO:     _ take advantage of imutil's is_binary function
        """
        self.logger.debug("finding object by method 2")
        self.investigate(my_array)
        if self.is_binary:
            # Create a blank copy of the processed image:
            my_img = numpy.ones(my_array.shape)
            my_img *= 255
            self.isReady.emit(my_img)

            # Extract shape info:
            h, w = my_array.shape

            # Get feature extents:
            my_extents = find_feature_extents(my_array)
            num_rings = len(my_extents)
            self.logger.debug("counted %d element regions", num_rings)

            # Initialize variables:
            self.elements = []
            for i in range(num_rings):
                self.updateProc.emit("Searching ... found %d objects" % (i))
                new_e = element(
                    data=find_foreground(my_array, my_extents[i]),
                    shape=(h, w),
                    offset=(self.crop_top, self.crop_left))
                self.elements.append(new_e)
                # Update element image:
                my_img = numpy.minimum(my_img, new_e.img)
            self.updateProc.emit("Found %d objects!" % (num_rings))
            self.isReady.emit(my_img)
            self.isFound.emit(num_rings)
        else:
            self.logger.warning("objects can only be found in binary images")
            self.sendAlert.emit("Please first threshold the image.")
            self.updateProc.emit("Image Processing Ready")

    def fit_aor(self):
        """
        Name:     ImageWorker.fit_aor
        Inputs:   None.
        Outputs:  None.
        Features: Calculates the slope and intercept of the axis of rotation,
                  AoR, based on the least squares fitting of the element
                  center points
        """
        self.logger.debug("fitting axis of rotation")

        num_e = len(self.elements)
        if num_e > 0:
            self.logger.debug("extracting center points of %d elements", num_e)
            center_points = []
            for i in range(num_e):
                obj = self.elements[i]
                center_points.append(obj.center)

            self.logger.debug("calculating center max and min")
            h_points = numpy.array([point[0] for point in center_points])
            h_max = h_points.max()
            w_points = numpy.array([point[1] for point in center_points])
            w_min = w_points.min()

            # Convert from pixel to element coordinate system:
            y_array = numpy.array([h_max - h for h in h_points])
            x_array = numpy.array([w - w_min for w in w_points])

            if self.is_enabled:
                try:
                    self.logger.debug("performing curve fitting")
                    p_opt, p_cov = scipy.optimize.curve_fit(
                        func_line, x_array, y_array, (1., 0.))
                except:
                    self.logger.exception("curve fit failed!")
                    self.m = None
                    self.b = None
                    self.m_err = None
                    self.b_err = None
                else:
                    self.m, self.b = p_opt
                    self.logger.info("slope = %f", self.m)
                    self.logger.info("intercept = %f", self.b)

                    self.logger.debug("calculating the standard errors")
                    try:
                        std_err = numpy.sqrt(numpy.diag(p_cov))
                    except:
                        self.logger.exception("failed to calculate errors")
                        self.m_err = None
                        self.b_err = None
                    else:
                        self.m_err, self.b_err = std_err
                        self.logger.info("slope error = %f", self.m_err)
                        self.logger.info("intercept error = %f", self.b_err)
            else:
                self.logger.warning("Not in Analysis Mode!")
        else:
            self.logger.warning("no elements for calculation")
            self.m = None
            self.b = None
            self.m_err = None
            self.b_err = None

    def fit_element_heights(self):
        """
        Name:     ImageWorker.fit_element_heights
        Inputs:   None.
        Outputs:  None.
        Features: Fits quadratic to element heights
        """
        self.logger.debug("fitting element heights")

        num_e = len(self.elements)
        if num_e > 0:
            self.logger.debug("extracting element data")
            center_points = []
            element_heights = []
            for i in range(num_e):
                obj = self.elements[i]
                center_points.append(obj.center)
                element_heights.append(obj.height)
            self.logger.debug("creating x-y arrays")
            x_array = numpy.array([point[0] for point in center_points])
            y_array = numpy.array(element_heights)
            min_y = y_array.min()  # seed value for 'c' parameter in poly

            try:
                self.logger.debug("performing curve fit")
                p_opt, p_cov = scipy.optimize.curve_fit(
                    func_poly, x_array, y_array, (1., 0., min_y))
            except:
                self.logger.exception("curve fit failed")
                self.k1 = None
                self.k2 = None
                self.k3 = None
                self.k1_err = None
                self.k2_err = None
                self.k3_err = None
            else:
                self.k1, self.k2, self.k3 = p_opt
                self.logger.info("poly k1 = %f", self.k1)
                self.logger.info("poly k2 = %f", self.k2)
                self.logger.info("poly k3 = %f", self.k3)

                try:
                    self.logger.debug("calculating estimate standard errors")
                    std_err = numpy.sqrt(numpy.diag(p_cov))
                except:
                    self.logger.debug("failed to calculate standard error")
                    self.k1_err = 0.0
                    self.k2_err = 0.0
                    self.k3_err = 0.0
                else:
                    self.k1_err, self.k2_err, self.k3_err = std_err
                    self.logger.info("k1_err = %f", self.k1_err)
                    self.logger.info("k2_err = %f", self.k2_err)
                    self.logger.info("k3_err = %f", self.k3_err)

    def get_element_array(self, idx):
        """
        Name:     ImageWorker.get_element_array
        Inputs:   int, element list index (idx)
        Outputs:  None.
        Features: Catches Prida's getObject signal to display a given element
                  and signals the array back to the GUI for preview
        """
        self.logger.debug("caught signal %d", idx)
        try:
            if idx >= 0:
                self.updateProc.emit("Showing object %d" % (idx + 1))
                obj = self.elements[idx]
                self.isReady.emit(obj.img)
            else:
                obj = self.elements[0]
                my_img = numpy.copy(obj.img)
                for obj in self.elements:
                    my_img = numpy.minimum(my_img, obj.img)
                self.updateProc.emit(
                    "Showing %d objects" % (len(self.elements)))
                self.isReady.emit(my_img)
        except IndexError:
            self.logger.exception("Failed to get element array")
            self.sendAlert.emit("Please first find objects.")
        except:
            self.logger.exception("Encountered unknown error")
            raise

    def investigate(self, my_array):
        """
        Name:     ImageWorker.investigate
        Inputs:   numpy.ndarray, image array (my_array)
        Outputs:  None.
        Features: Checks an image array for grayscale and binary

        TODO:     _ replace w/ imutil's is_binary
        """
        if isinstance(my_array, numpy.ndarray):
            self.logger.debug("checking if image is grayscale")
            if len(my_array.shape) == 2:
                self.logger.debug("... yes")

                my_hist = numpy.histogram(my_array)[0]
                num_pos_bins = len(numpy.where(my_hist != 0)[0])
                self.logger.debug("checking if image is binary")
                if num_pos_bins == 1 or num_pos_bins == 2:
                    self.logger.debug("... yes")
                    self.is_binary = True
                else:
                    self.logger.debug("... no")
                    self.is_binary = False
            else:
                self.logger.debug("... no")
                self.is_binary = False
        else:
            raise TypeError("Function parameter must be an array.")

    def make_binary(self):
        """
        Name:     ImageWorker.make_binary
        Inputs:   None.
        Outputs:  None.
        Features: Converts process image to binary image
        Depends:  - rgb_to_gray
                  - otsu

        TODO:     _ take advantage of imutil's is_binary function
        """
        try:
            self.investigate(self.proc_img)
        except TypeError:
            self.sendAlert.emit(
                "Please load an image before thresholding!")
            self.updateProc.emit("Image Processing Ready")
        else:
            if not self.is_binary:
                img_dim = len(self.proc_img.shape)
                self.logger.debug("image has dimension %d", img_dim)
                if img_dim == 2:
                    # Check grayscale values:
                    my_array = numpy.clip(
                        self.proc_img.astype("uint8"), 0, 255)
                elif img_dim == 3:
                    self.logger.debug(
                        "converting color image to grayscale")
                    my_array = rgb_to_gray(self.proc_img)
                    my_array = numpy.clip(my_array.astype("uint8"), 0, 255)
                else:
                    raise ValueError("Unhandled array dimension encountered")

                try:
                    self.proc_img = self.otsu(my_array)
                except:
                    self.logger.exception("convert to binary failed")
                    self.isReady.emit(numpy.ndarray([]))
                else:
                    self.updateProc.emit("Binary merged image (Otsu %d)" % (
                        self.bin_thresh))
                    self.isReady.emit(self.proc_img)
            else:
                self.logger.debug(
                    "skipping binary process; already binary")

    def dust_filter(self, part_size):
        """
        Name:     ImageWorker.dust_filter
        Inputs:   int, dust particle size (part_size)
        Outputs:  None.
        Features: toFilter socket; removes dust particles from processed image
        """
        self.logger.info(
            "removing dust particles less than %d pixels", part_size)

        try:
            self.investigate(self.proc_img)
        except TypeError:
            self.sendAlert.emit(
                "Please load an image before dust filtering.")
            self.updateProc.emit("Image Processing Ready")
        else:
            if self.is_binary:
                if part_size == 0:
                    self.updateProc.emit("No dust particle size allocated.")
                    self.isReady.emit(self.proc_img)
                elif part_size > 0:
                    # NOTE: method assumes array is binary
                    temp_array = numpy.copy(self.proc_img).astype('bool')

                    # Find dust particles:
                    self.updateProg.emit(0, 100)   # arbitrary to zero the bar
                    pixel_groups = find_particles(temp_array)
                    num_groups = len(pixel_groups)

                    # Remove dust:
                    for i in range(num_groups):
                        self.updateProg.emit(i, num_groups-1)
                        group = pixel_groups[i]
                        if group.size <= part_size:
                            # Consider this dust; remove it!
                            for point in group.points:
                                temp_array[point] = True

                    self.proc_img = temp_array.astype('uint8')
                    self.updateProc.emit(
                        "Filtered image (particle size = %d)" % (
                            part_size))
                    self.isReady.emit(self.proc_img)
                else:
                    self.sendAlert.emit(
                        "Dust particle size must be a positive integer.")
                    self.isReady.emit(self.proc_img)
            else:
                self.sendAlert.emit(
                    "Please convert image to binary before dust filtering.")
                self.updateProc.emit("Image Processing Ready")

    def median_filter(self, hkernel):
        """
        Name:     ImageWorker.median_filter
        Input:    int, half-kernel width (hkernel)
        Output:   None.
        Features: toSmooth socket; performs median filter
        """
        self.logger.debug("half-kernel size = %d", hkernel)
        if self.proc_img is not None:
            kwidth = int(2*hkernel + 1)
            self.kernel = kwidth
            self.logger.info("filtering with kernel size = %d", kwidth)

            img_dim = len(self.proc_img.shape)
            self.logger.debug("image dimension = %d", img_dim)

            # Work on a copy of the process image:
            temp_array = numpy.copy(self.proc_img)
            if img_dim == 2:
                h, w = self.proc_img.shape
                for y in range(h):
                    # Each column
                    self.updateProc.emit("Smoothing ...")
                    self.updateProg.emit(y, h-1)
                    # Initialize the before and after filtering row data;
                    # NOTE previous row is initialized as all white, such that
                    #      blank rows (i.e., nothing to be filtered) are
                    #      skipped :)
                    prev_row = numpy.zeros((w,))
                    cur_row = numpy.copy(self.proc_img[y, :])
                    # Whiten the half-kernel boundary on both sides:
                    for p in range(hkernel):
                        cur_row[p] = 255
                    for p in range(w - hkernel, w, 1):
                        cur_row[p] = 255
                    while not numpy.array_equal(prev_row, cur_row):
                        prev_row = numpy.copy(temp_array[y, :])
                        for x in range(hkernel, w - hkernel, 1):
                            k_grid = [x + i - hkernel for i in range(kwidth)]
                            k_grid = numpy.array(k_grid)
                            k_vals = prev_row[k_grid]
                            k_median = numpy.median(k_vals)
                            cur_row[x] = k_median
                        temp_array[y, :] = cur_row
                self.updateProc.emit(
                    "Smoothed merged image (kernel size = %d)" % (kwidth))
                self.isReady.emit(temp_array)
            else:
                self.logger.warning("encountered color image!")
                self.sendAlert.emit("Please try loading merge image again.")
                self.updateProc.emit("Image Processing Ready")
        else:
            self.logger.warning("process image is not set!")
            self.sendAlert.emit("Please first load merged image")
            self.updateProc.emit("Image Processing Ready")

    def otsu(self, nd_array):
        """
        Name:     ImageWorker.otsu
        Inputs:   numpy.ndarray, 2D integer array (nd_array)
        Outputs:  numpy.ndarray, two-dimensional integer array (my_binary)
        Features: Returns a binary image based on the Otsu (1979) method
        Ref:      Otsu, N. (1979) A threshold selection method from gray-level
                  histograms, IEEE Transactions on Systems, Man, and
                  Cybernetics, 9(1), 62--66.
        """
        # Otsu's max and min level values:
        if self.bg_color == 0:
            # Black background, make foreground objects black
            self.logger.info("Otsu black background")
            l_max = 0
            l_min = 255
        elif self.bg_color == 1:
            # White background, make forground objects black
            self.logger.info("Otsu white background")
            l_max = 255
            l_min = 0

        # Otsu's number of pixels at each level, n, total number of pixels, N,
        # and normalized probability distribution, p
        n = numpy.bincount(nd_array.astype("uint8").ravel(), minlength=256)
        N = n.sum()
        p = n.astype("f4") / N
        self.logger.debug("total number of pixels: %d", N)

        # Otsu's mean pixel value at each level, mu:
        mu = [i * p[i] for i in range(len(p))]
        mu = numpy.array(mu)

        # Otsu's probabilities of class occurrence, wk [Eq. 6], class mean
        # values, uk [Eq. 7], and the total mean level of the original
        # image, ut [Eq. 8]:
        wk = p.cumsum()
        uk = mu.cumsum()
        ut = mu.sum()

        # Calculate the between-class variances, vb [Eq. 18]:
        vb = ut * wk[:-1]
        vb -= uk[:-1]
        vb = numpy.power(vb, 2.0)
        denom = 1. - wk[:-1]
        denom *= wk[:-1]
        if 0 in denom:
            self.logger.warning(
                "Otsu found zeros in denominator, adjusting")
            denom[numpy.where(denom == 0)] += 1.e-3
        try:
            vb /= denom
        except:
            self.logger.exception("encountered division error")
            raise
        else:
            # Search for maximum between-class variance, i.e., threshold value:
            k = numpy.where(vb == vb.max())[0]
            if len(k) == 0:
                self.logger.error("failed to find maximum value")
                raise ValueError("No maximum value found in Otsu method")
            elif len(k) > 1:
                self.logger.warning(
                    "found %d maximum values", len(k))
                self.logger.warning("using first occurrence")
            thresh = k[0]
            self.bin_thresh = thresh
            self.logger.info(
                "Otsu threshold set at %d", int(thresh))

            # Find the foreground and background pixel values:
            my_binary = numpy.copy(nd_array).astype("f4")
            fore_idx = numpy.where(my_binary > thresh)
            back_idx = numpy.where(my_binary <= thresh)

            # Set foreground and background:
            my_binary[fore_idx] *= 0
            my_binary[fore_idx] += 1
            my_binary[fore_idx] *= l_max

            my_binary[back_idx] *= 0
            my_binary[back_idx] += 1
            my_binary[back_idx] *= l_min

            return my_binary.astype("uint8")

    def plot_centers(self):
        """
        TEMPORARY FUNCTION
        Plots elements with red dots at their centers and a blue dot at the COI
        """
        if len(self.elements) > 0:
            hf_prime, hf_prime_err = self.calc_hf_prime()

            temp_array = numpy.ones(
                (self.elements[0].shape[0],
                 self.elements[0].shape[1], 3)).astype('uint8')
            temp_array *= 255

            # Create merged object image:
            for obj in self.elements:
                min_array = numpy.minimum(temp_array[:, :, 0], obj.img)
                temp_array[:, :, 0] = numpy.copy(min_array)
                temp_array[:, :, 1] = numpy.copy(min_array)
                temp_array[:, :, 2] = numpy.copy(min_array)

            # Color centers red:
            for k in range(len(self.elements)):
                obj = self.elements[k]
                center_y, center_x = obj.center
                self.logger.debug(
                    "Element %d: center: (%d, %d)" % (k, center_y, center_x))
                for i in numpy.arange(-4, 5, 1):
                    for j in numpy.arange(-4, 5, 1):
                        temp_array[center_y + i, center_x + j, 0] = 255
                        temp_array[center_y + i, center_x + j, 1] = 0
                        temp_array[center_y + i, center_x + j, 2] = 0

            # Color COI blue:
            coi_y, coi_x = self.elements[0].coi
            for i in numpy.arange(-4, 5, 1):
                for j in numpy.arange(-4, 5, 1):
                    temp_array[coi_y + i, coi_x + j, 0] = 0
                    temp_array[coi_y + i, coi_x + j, 1] = 0
                    temp_array[coi_y + i, coi_x + j, 2] = 255

            # Color HFprime green:
            if hf_prime is not None:
                hf_star = int(coi_y - hf_prime + 0.5)
                for i in numpy.arange(-4, 5, 1):
                    for j in numpy.arange(-4, 5, 1):
                        temp_array[hf_star + i, coi_x + j, 0] = 0
                        temp_array[hf_star + i, coi_x + j, 1] = 255
                        temp_array[hf_star + i, coi_x + j, 2] = 0

            self.updateProc.emit(
                "Element centers in red; COI in blue; HF' in green")
            self.isReady.emit(temp_array)

    def refresh(self):
        """
        Name:     ImageWorker.refresh
        Inputs:   None
        Returns:  None
        Features: Resets view to original merged image
        Depends:  - reset_calib_params
                  - update_image_range

        @TODO: reset the elements array
        """
        if self.merge_img is not None:
            self.proc_img = numpy.copy(self.merge_img)
            self.is_cropped = False
            self.kernel = 0
            self.crop_left = 0
            self.crop_right = self.merge_img.shape[1]
            self.crop_top = 0
            self.crop_bottom = self.merge_img.shape[0]
            self.reset_calib_params()
            self.isFound.emit(0)
            self.isReady.emit(self.proc_img)
            self.update_image_range()
            self.updateProc.emit("Merged grayscale image")
        else:
            self.sendAlert.emit("Please first load merge image.")
            self.updateProc.emit("Image Processing Ready")

    def remove_element(self, idx):
        """
        Name:     ImageWorker.remove_element
        Inputs:   int, element list index (idx)
        Outputs:  None.
        Features: Catches Prida's removeObject signal to delete a given element
                  and drops the element from the current list
        Depends:  get_element_array
        """
        self.logger.debug("caught signal %d", idx)
        if idx >= 0:
            try:
                self.elements.pop(idx)
            except:
                self.logger.exception("Failed to remove element")
            else:
                num_found = len(self.elements)
                self.isFound.emit(num_found)
                self.get_element_array(-1)
        else:
            self.elements = []
            self.isFound.emit(0)

    def reset(self):
        """
        Name:     ImageWorker.reset
        Inputs:   None
        Returns:  None
        Signals:  - isFound (to Prida)
                  - setBGColor (to Prida)
                  - setImgRange (to Prida)
                  - setImgShape (to Prida)
        Features: Reset all variables except datasets and path
        Depends:  reset_calib_params
        """
        self.logger.info("resetting process values")
        self.reset_calib_params()
        self.elements = []
        self.crop_left = 0
        self.crop_right = 0
        self.crop_top = 0
        self.crop_bottom = 0
        self.kernel = 0
        self.proc_img = None
        self.merge_img = None
        self.merge_count = 0
        self.is_binary = False
        self.is_cropped = False
        self.is_merged = False
        self.isFound.emit(0)
        self.setBGColor.emit("")
        self.setImgRange.emit("")
        self.setImgShape.emit((None, None))

    def reset_calib_params(self):
        """
        Name:     ImageWorker.reset_calib_params
        Inputs:   None.
        Outputs:  None.
        Features: Resets calibration parameters
        """
        self.m = None              # slope of AoR in e-coordinate system
        self.b = None              # intercept of AoR in e-coordinate system
        self.m_err = None          # AoR slope standard error
        self.b_err = None          # AoR intercept standard error

        self.k1 = None             # quadratic coefficient of quadratic
        self.k2 = None             # linear coefficient of quadratic
        self.k3 = None             # constant of quadratic
        self.k1_err = None         # quadratic coefficient error
        self.k2_err = None         # linear coefficient error
        self.k3_err = None         # constant error

        self.roll = None           # roll, degrees
        self.roll_err = None       # roll uncertainty, degrees
        self.pitch = None          # pitch, degrees
        self.pitch_err = None      # pitch uncertainty, degrees
        self.yt = None             # translation, m
        self.yt_err = None         # translation uncertainty, m

    def rotate_left(self):
        """
        Name:     ImageWorker.rotate_left
        Inputs:   None.
        Outputs:  None.
        Features: Rotates the merged and processed images ninety degrees
                  counter-clockwise; signals updated image shape and preview
        """
        if self.proc_img is not None and self.merge_img is not None:
            self.merge_img = numpy.rot90(self.merge_img)
            self.proc_img = numpy.rot90(self.proc_img)
            h, w = self.merge_img.shape
            self.isMerged.emit(h, w)
            self.setImgShape.emit(self.merge_img.shape)
            self.isReady.emit(self.proc_img)

    def rotate_right(self):
        """
        Name:     ImageWorker.rotate_right
        Inputs:   None.
        Outputs:  None.
        Features: Rotates the merged and processed images ninety degrees
                  clockwise; signals updated image shape and preview
        """
        if self.proc_img is not None and self.merge_img is not None:
            self.merge_img = numpy.rot90(self.merge_img, 3)
            self.proc_img = numpy.rot90(self.proc_img, 3)
            h, w = self.merge_img.shape
            self.isMerged.emit(h, w)
            self.setImgShape.emit(self.merge_img.shape)
            self.isReady.emit(self.proc_img)

    def set_datasets(self, s_path, ds_list):
        """
        Name:     ImageWorker.set_datasets
        Inputs:   - str, session path (s_path)
                  - list, list of session dataset paths (ds_list)
        Outputs:  None.
        Features: Receives datasetsListed signal and saves the session path,
                  image list and merge count
        """
        self.logger.info("caught datasets listed signal")
        self.path = s_path
        self.datasets = ds_list

    def set_image(self, my_array, img_count):
        """
        Name:     ImageWorker.set_image
        Inputs:   - numpy.ndarray, image array (my_array)
                  - int, image number (img_count)
        Returns:  None.
        Features: Socket for setImg; performs array scaling, grayscaling and
                  image merging
        Depends:  - rgb_to_gray
                  - investigate
                  - scale_image
                  - update_image_range
        """
        self.logger.debug("caught signal for image %d", img_count)

        # Progress bar:
        if self.merge_count > 1:
            self.updateProc.emit("Merging ...")
            self.updateProg.emit(img_count, self.merge_count - 1)

        if self.sfactor != 100:
            self.logger.debug("scaling image")
            my_array = scale_image(my_array, self.sfactor)

        img_dim = len(my_array.shape)
        if img_dim == 3:
            self.logger.debug("converting image to grayscale")
            gray_img = rgb_to_gray(my_array)
        elif img_dim == 2:
            self.logger.debug("image already grayscale")
            gray_img = my_array
        else:
            self.logger.error("unexpected image array size")
            raise TypeError("Image array is not of expected size!")

        self.logger.debug("merging image")
        if img_count == 0 and img_count < self.merge_count - 1:
            self.logger.info("setting first merge image")
            self.merge_img = gray_img
            self.is_merged = False
            img_count += 1
            dset = self.datasets[img_count]
            self.logger.debug("signaling for image %d", img_count)
            self.getProcImg.emit(dset, img_count)
        elif img_count == 0 and img_count == self.merge_count - 1:
            # There was only one image in the session set
            self.logger.info("setting image")
            self.merge_img = gray_img
            self.is_merged = True
            self.proc_img = numpy.copy(self.merge_img)
            self.setImgShape.emit(gray_img.shape)
            self.update_image_range()
            self.updateProc.emit("Grayscale image")
            h, w = self.proc_img.shape
            self.crop_left = 0
            self.crop_right = w
            self.crop_top = 0
            self.crop_bottom = h
            self.isMerged.emit(h, w)
            self.isReady.emit(self.proc_img)
        elif img_count == self.merge_count - 1:
            if self.bg_color == 0:
                # Black background, merge foreground (white)
                self.logger.info("merging last image with black background")
                self.merge_img = numpy.maximum(self.merge_img, gray_img)
                self.setBGColor.emit("Black")
            elif self.bg_color == 1:
                self.logger.info("merging last image with white background")
                self.merge_img = numpy.minimum(self.merge_img, gray_img)
                self.setBGColor.emit("White")
            self.is_merged = True
            self.proc_img = numpy.copy(self.merge_img)
            self.setImgShape.emit(gray_img.shape)
            self.update_image_range()
            self.updateProc.emit("Merged grayscale image")
            h, w = self.proc_img.shape
            self.crop_left = 0
            self.crop_right = w
            self.crop_top = 0
            self.crop_bottom = h
            self.isMerged.emit(h, w)
            self.isReady.emit(self.proc_img)
        elif img_count < self.merge_count - 1:
            if self.bg_color == 0:
                # Black background, merge foreground (white)
                self.logger.info(
                    "merging image %d with black background",
                    img_count)
                self.merge_img = numpy.maximum(self.merge_img, gray_img)
            elif self.bg_color == 1:
                self.logger.info(
                    "merging image %d with white background",
                    img_count)
                self.merge_img = numpy.minimum(self.merge_img, gray_img)
            self.is_merged = False
            img_count += 1
            dset = self.datasets[img_count]
            self.logger.debug("signaling for image %d", img_count)
            self.getProcImg.emit(dset, img_count)

    def show_first(self, opts):
        """
        Name:     ImageWorker.show_first
        Inputs:   dict, image processing options (opts)
        Outputs:  None.
        Signals:  - getProcImg (to DataWorker)
                  - updateProc (to Prida)
        Features: Loads first image from datasets list
        Depends:  reset
        """
        sfactor = opts.get('scale', 100)
        self.logger.info("processing first image")
        self.reset()
        self.sfactor = sfactor
        self.merge_count = 1

        self.updateProc.emit("Loading image ...")
        dset = self.datasets[0]
        self.logger.debug("signaling for image 0")
        self.getProcImg.emit(dset, 0)

    def show_proc_img(self):
        """
        Name:     ImageWorker.show_proc_img
        Inputs:   None.
        Outputs:  None.
        Signals:  - isReady (to Prida)
                  - updateProc (to Prida)
        Features: Signals the process image back to the GUI
        """
        if self.proc_img is not None:
            self.updateProc.emit("Current process image.")
            self.isReady.emit(self.proc_img)

    def start(self):
        """
        Name:     ImageWorker.start
        Inputs:   None.
        Outputs:  None.
        Signals:  updateProc (to Prida)
        Features: The analysis thread run function
        Depends:  reset
        """
        self.logger.info("start")
        self.reset()
        self.updateProc.emit("Image Processing Ready")

    def start_merge(self, opts):
        """
        Name:     ImageWorker.start_merge
        Inputs:   dict, image processing options (opts)
        Signals:  - updateProc (to Prida)
                  - getProcImg (to DataWorker)
        Features: Saves background color and scaling factor;
                  signals to data for first process image to begin merging;
                  receives data's signal with set_image
        Depends:  reset
        """
        self.reset()

        bg_color = opts.get("background", 0)
        sfactor = opts.get("scale", 100)

        self.merge_count = len(self.datasets)
        self.logger.debug("setting merge count to %d", self.merge_count)
        self.bg_color = bg_color
        self.logger.info("setting background to %d", bg_color)
        self.sfactor = sfactor
        self.logger.info("setting scale factor to %d", sfactor)

        self.updateProc.emit("Beginning merge ...")
        self.logger.debug("starting to gather images from data")
        dset = self.datasets[0]
        self.logger.debug("signaling for image 0")
        self.getProcImg.emit(dset, 0)

    def stretch(self):
        """
        Name:     ImageWorker.stretch
        Inputs:   None
        Returns:  None
        Features: Stretches the grayscale luminosity of the process image
        Depends:  update_image_range
        """
        if self.proc_img is not None:
            img_dim = len(self.proc_img.shape)
            if img_dim == 2:
                h, w = self.proc_img.shape
                max_val = self.proc_img.max()
                min_val = self.proc_img.min()
                if max_val == 255 and min_val == 0:
                    self.logger.debug("image already stretched")
                else:
                    for i in range(h):
                        self.updateProc.emit("Stretching ...")
                        self.updateProg.emit(i, h-1)
                        for j in range(w):
                            my_val = self.proc_img[i, j]
                            s_val = 255. * float(my_val - min_val)
                            s_val /= float(max_val - min_val)
                            s_val = int(s_val)
                            self.proc_img[i, j] = s_val
                self.updateProc.emit("Stretched merged image.")
                self.isReady.emit(self.proc_img)
                self.update_image_range()
            else:
                self.sendAlert.emit("Processed image is not grayscale!")
                self.updateProc.emit("Image Processing Ready")
        else:
            self.sendAlert.emit("Please load merged image first.")
            self.updateProc.emit("Image Processing Ready")

    def update_image_range(self):
        """
        Name:     ImageWorker.update_image_range
        Inputs:   None.
        Returns:  None.
        Features: Signals to GUI the process image's luminosity range.
        """
        if self.proc_img is not None:
            max_val = self.proc_img.max()
            min_val = self.proc_img.min()
            my_range = "(%d, %d)" % (min_val, max_val)
            self.setImgRange.emit(my_range)
        else:
            self.setImgRange.emit("")


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    # Create a root logger:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    # Instantiating logging handler and record format:
    root_handler = logging.FileHandler("workers.log")
    rec_format = "%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s"
    formatter = logging.Formatter(rec_format, datefmt="%Y-%m-%d %H:%M:%S")
    root_handler.setFormatter(formatter)

    # Send logging handler to root logger:
    root_logger.addHandler(root_handler)
