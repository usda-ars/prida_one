#!/usr/bin/python
#
# utilities.py
#
# VERSION: 1.6.0-dev
#
# LAST EDIT: 2017-06-12
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# CHANGELOG:
# $log_start_tag$
# $log_end_tag$
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import datetime
import glob
import logging
import os.path
import time

import exifread

###############################################################################
# PARAMETER DEFINITIONS:
###############################################################################
# Currently support image types:
img_types = (".jpg", ".JPG", ".jpeg", ".tif", ".tiff", ".NEF", ".nef")
img_import_str = 'Images (*.jpg *.JPG *.jpeg *.tif *.tiff *.NEF *.nef)'


###############################################################################
# FUNCTIONS:
###############################################################################
def ctime(file_path):
    """
    Name:     ctime
    Inputs:   str, file path (file_path)
    Outputs:  datetime.datetime object (my_date)
    Features: Returns datetime object of file's *creation* time
    Note:     Creation time is not actual file creation time on *nix systems,
              rather, it refers to the last time the inode data changed
    """
    try:
        my_date = datetime.datetime.strptime(
            time.ctime(os.path.getctime(file_path)),
            "%a %b %d %H:%M:%S %Y")
    except OSError:
        my_date = ""
    finally:
        return my_date


def find_image_files(my_dir):
    """
    Name:     find_image_files
    Inputs:   str, directory name (my_dir)
    Outputs:  list, image paths
    Features: Searches a single directory for image files
    Depends:  img_types (global variable)
    """
    my_files = []
    for img_type in img_types:
        s_str = os.path.join(my_dir, "*%s" % (img_type))
        my_files += glob.glob(s_str)
    my_files = sorted(list(set(my_files)))

    return my_files


def get_ctime(file_path):
    """
    Name:     get_ctime
    Inputs:   str, image file path (file_path)
    Outputs:  str, date and time
    Features: Returns the original/creation datetime for an image file based
              either on its EXIF tags or system data
    Depends:  - ctime
              - read_exif_tags
    """
    fext = os.path.splitext(file_path)[1]
    if fext in img_types:
        tags = read_exif_tags(file_path)
        dto = tags.get("EXIF DateTimeOriginal", str(ctime(file_path)))
        cdatetime = dto.split(" ")
        if len(cdatetime) == 2:
            d, t = cdatetime
            d = d.replace(":", "-")
            dto = " ".join([d, t])
            return dto
        else:
            raise TypeError("Unexpected datetime format (%s)" % dto)
    else:
        raise TypeError("Unexpected file type (%s)" % (fext))


def read_exif_tags(im_path):
    """
    Name:     read_exif_tags
    Inputs:   str, image file with path (im_path)
    Outputs:  dict, exif tag values (my_tags)
    Features: Returns dictionary of image exif tags
    """
    # Initialize tag and exif dictionaries:
    my_tags = {}
    exif = {}

    # Open image file for processing:
    if os.path.isfile(im_path):
        try:
            f = open(im_path, 'rb')
        except:
            logging.warning("failed to open %s for reading", im_path)
        else:
            try:
                exif = exifread.process_file(f)
            except:
                logging.warning("failed to process tags on file %s", im_path)
            else:
                for tag in exif:
                    value = str(exif[tag])
                    assert isinstance(value, str)
                    if not value.isspace() and value not in ("[]", "()"):
                        if tag not in ('JPEGThumbnail',
                                       'TIFFThumbnail',
                                       'Filename',
                                       'EXIF MakerNote',
                                       'MakerNote ShotInfo'):
                            my_tags[tag] = value.rstrip(' ')
            finally:
                f.close()

    # Read through and correct tag types:
    return my_tags


def conf_parser(obj_inst, obj_name, config_file):
    """
    Name:    conf_parser
    Feature: Parses config file for relevant entries and assigns object
             attributes the user defined values found in the file
    Inputs:  - obj, instance of object to configure (obj_inst)
             - str, name of script calling the parser (obj_name)
             - str, absolute path to config file (config_file)
    Outputs: None
    """
    # Track whether we found a matching entry in the config file:
    found_match = False
    try:
        with open(config_file, 'r') as my_conf:
            for line in my_conf.readlines():
                words = line.split()

                # Check config file line formating
                if not len(words) == 3:
                    obj_inst.logger.error(
                        'config file poorly conditioned, check formatting.')
                    raise IOError(
                        'config file poorly conditioned, check formatting.')

                # Only access valid lines that match script/module name:
                if obj_name.upper() == words[0]:
                    found_match = True

                    # Check class attribute dictionary for valid entry:
                    if words[1] in obj_inst.attr_dict.keys():

                        # Double check that class has the attribute variable:
                        if hasattr(obj_inst, obj_inst.attr_dict[words[1]]):
                            try:

                                # Check if OS is windows and
                                # the attribute is a directory:
                                if os.name is 'nt' and '_DIR' in words[1]:

                                    # Replace escape characters:
                                    words[2] = words[2].replace(os.sep, '/')

                                # Set attribute value:
                                exec("obj_inst.%s = %s" % (
                                    obj_inst.attr_dict[words[1]], words[2]))
                            except (ValueError, TypeError, AttributeError):
                                obj_inst.logger.exception(
                                    "failed to set %s.%s to %s" % (
                                        obj_name,
                                        obj_inst.attr_dict[words[1]],
                                        words[2]))
                            else:
                                obj_inst.logger.info(
                                    "%s.%s = %s" % (
                                        obj_name,
                                        obj_inst.attr_dict[words[1]],
                                        words[2]))
                        else:
                            obj_inst.logger.warning(
                                "%s does not have attribute %s" % (
                                    obj_name, obj_inst.attr_dict[words[1]]))
                    else:
                        obj_inst.logger.warning(
                            "%s is not an configurable attribute of %s" % (
                                words[1], obj_name))
                else:
                    obj_inst.logger.debug("ignoring %s", line.rstrip())
    except IOError:
        obj_inst.logger.exception(
            "Could not find config file at '%s'! Using defaults.", config_file)
    else:
        if not found_match:
            obj_inst.logger.warning("%s not found in config file!", obj_name)


def init_pid_attrs():
    """
    Name:     init_pid_attrs
    Inputs:   None.
    Outputs:  None.
    Features: Returns the PID attribute dictionary
              > 'title' .... string used to name the field in the GUI
              > 'key' ...... HDF5 attribute variable name
              > 'qt_val' ... Qt variable name in mainwindow.ui
              > 'def_val' .. the default value (accessed w/ set_defaults)
              > 'cur_val' .. the current value (may be different from default)
              > 'qt_type' .. the UI class type in mainwindow.ui
    """
    pid_attrs = {
        1: {'title': 'USDA PLANTS',
            'key': 'usda_plants',
            'qt_val': 'c_usda_plants',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        2: {'title': 'Genus Species',
            'key': 'gen_sp',
            'qt_val': 'c_gen_sp',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        3: {'title': 'Line',
            'key': 'line',
            'qt_val': 'c_line',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        4: {'title': 'Rep',
            'key': 'rep_num',
            'qt_val': 'c_rep_num',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        5: {'title': 'Tub ID',
            'key': 'tubid',
            'qt_val': 'c_tubid',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        6: {'title': 'Germination Date',
            'key': 'germdate',
            'qt_val': 'c_germdate',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        7: {'title': 'Transplant Date',
            'key': 'transdate',
            'qt_val': 'c_transdate',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        8: {'title': 'Treatment',
            'key': 'treatment',
            'qt_val': 'c_treatment',
            'def_val': 'Control',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        9: {'title': 'Media',
            'key': 'media',
            'qt_val': 'c_media',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        10: {'title': 'Tub Size',
             'key': 'tubsize',
             'qt_val': 'c_tubsize',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        11: {'title': 'Nutrient',
             'key': 'nutrient',
             'qt_val': 'c_nutrient',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        12: {'title': 'Growth Temp (Day)',
             'key': 'growth_temp_day',
             'qt_val': 'c_growth_temp_day',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        13: {'title': 'Growth Temp (Night)',
             'key': 'growth_temp_night',
             'qt_val': 'c_growth_temp_night',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        14: {'title': 'Lighting Conditions',
             'key': 'growth_light',
             'qt_val': 'c_growth_light',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        15: {'title': 'Watering schedule',
             'key': 'water_sched',
             'qt_val': 'c_water_sched',
             'def_val': '',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        16: {'title': 'Damage',
             'key': 'damage',
             'qt_val': 'c_damage',
             'def_val': 'No',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        17: {'title': 'Plant Notes',
             'key': 'notes',
             'qt_val': 'c_plant_notes',
             'def_val': 'N/A',
             'cur_val': '',
             'qt_type': 'QLineEdit'}
    }
    return pid_attrs


def init_photo_attrs():
    """
    Name:     init_photo_attrs
    Inputs:   None.
    Outputs:  None.
    Features: Returns an empty dictionary photo dataset attributes as
              defined in PridaHDF
    """
    photo_attrs = {
        0: {"key": "File", "val": ""},
        1: {"key": "Format", "val": ""},
        2: {"key": "Color", "val": ""},
        3: {"key": "Height", "val": ""},
        4: {"key": "Width", "val": ""},
        5: {"key": "Date", "val": ""},
        6: {"key": "Orientation", "val": ""}
    }
    return photo_attrs


def init_session_attrs():
    """
    Name:     init_session_attrs
    Inputs:   None.
    Outputs:  None.
    Features: Returns the session attribute dictionary
              > 'title' .... string used to name the field in the GUI
              > 'key' ...... HDF5 attribute variable name
              > 'qt_val' ... Qt variable name in mainwindow.ui
                             if Nonetype, it is skipped in load and get
              > 'def_val' .. the default value (accessed w/ set_defaults)
              > 'cur_val' .. the current value (may be different from default)
              > 'qt_type' .. the UI class type in mainwindow.ui
    """
    session_attrs = {
        0: {'title':  'Session User',
            'key': 'user',
            'qt_val': 'c_session_user',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        1: {'title': 'Session Email',
            'key': 'addr',
            'qt_val': 'c_session_addr',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        2: {'title': 'Session Title',
            'key': 'number',
            'qt_val': 'c_session_number',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        3: {'title': 'Session Date',
            'key': 'date',
            'qt_val': 'c_session_date',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QDateEdit'},
        4: {'title': 'Rig Name',
            'key': 'rig',
            'qt_val': 'c_session_rig',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        5: {'title': 'Image Count',
            'key': 'num_img',
            'qt_val': 'c_num_images',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        6: {'title': 'Plant Age',
            'key': 'age_num',
            'qt_val': 'c_plant_age',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        7: {'title': 'Tank Distance',
            'key': 'cam_dist',
            'qt_val': 'c_cam_dist',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QLineEdit'},
        8: {'title': 'Orientation',
            'key': 'img_orient',
            'qt_val': 'c_img_orient',
            'def_val': '',
            'cur_val': '',
            'qt_type': 'QComboBox'},
        9: {'title': 'Exclude',
            'key': 'exclude',
            'qt_val': 'c_exclude',
            'def_val': 'False',
            'cur_val': '',
            'qt_type': 'QComboBox'},
        10: {'title': 'Notes',
             'key': 'notes',
             'qt_val': 'c_session_notes',
             'def_val': 'N/A',
             'cur_val': '',
             'qt_type': 'QLineEdit'},
        11: {'title': 'Mode',
             'key': 'mode',
             'qt_val': None,
             'def_val': 'Explorer',
             'cur_val': '',
             'qt_type': None},
        12: {'title': 'Driver',
             'key': 'motor_driver',
             'qt_val': None,
             'def_val': '',
             'cur_val': '',
             'qt_type': None},
        13: {'title': 'Gear Ratio',
             'key': 'gear_ratio',
             'qt_val': None,
             'def_val': '',
             'cur_val': '',
             'qt_type': None},
        14: {'title': 'Pause',   # aka settling time
             'key': 'motor_pause',
             'qt_val': None,
             'def_val': '',
             'cur_val': '',
             'qt_type': None},
        15: {'title': 'Speed',
             'key': 'motor_speed',
             'qt_val': None,
             'def_val': '',
             'cur_val': '',
             'qt_type': None},
        16: {'title': 'Degrees',
             'key': 'motor_degrees',
             'qt_val': None,
             'def_val': '',
             'cur_val': '',
             'qt_type': None},
        17: {'title': 'Step Type',
             'key': 'motor_step',
             'qt_val': None,
             'def_val': '',
             'cur_val': '',
             'qt_type': None}
    }
    return session_attrs
